<?php

	session_start();
	require_once("../funciones/generales.php");
  	require_once("../funciones/construct.php");
  	require_once("../funciones/utilidades.php");
  	require_once("../funciones/utilidadesProcesos.php");

	$tipoHold = $_REQUEST['i728GeneracionMdo'];	

	if($_SESSION['usuCompania'] ==''){
		$_SESSION['usuCompania'] = $_REQUEST['ciaSesVal'];
	}if ($_REQUEST['i728ModoRdo'] == 'D') {
		detenerUnidades();
	}if ($_REQUEST['i728ModoRdo'] == 'T') {
		liberaUnidad();
	}

	function detenerUnidades($unidades){
	// Variables del REQUEST -------------------------------
		$a = array();
		$f = array();
		$a['success'] = true;
		$a['msjResponse'] = '';
		$a['numError'] = 0;
		$f['faltantes'] = '';
		$_REQUEST['transmision'] = '';
		$vin1 = $_REQUEST['i728VinTxa'];
		$claveHold = $_REQUEST['trap728EstatusCmb'];		
	// -----------------------------------------------------
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);
		$cadena = chunk_split($vin, 17,",");
		$unidades= explode(",",$cadena);

		if($_REQUEST['i728ReprocesoDtm'] != ''){
			
			$fecha = substr($_REQUEST['i728ReprocesoDtm'],0,10);
			$hora = substr($_REQUEST['i728ReprocesoDtm'],11,19);

			$fechaInsert = $fecha." ".$hora;

		}
		else{

			$fechaAct="SELECT NOW() as fechaEvento ";
			$rsfechaAct = fn_ejecuta_query($fechaAct);

			$fechaInsert = $rsfechaAct['root'][0]['fechaEvento'];
		}
		$ff = 0;
		$tt = 0;

		for ($i=0; $i <(count($unidades)-1); $i++) {

					if($_REQUEST['i728GeneracionMdo'] == '1'){

						$sqlGetCenDis = "SELECT centroDistribucion ".
										"FROM alUltimoDetalleTbl ".
										"WHERE vin = '".$unidades[$i]."'; ";

						$rsGetCentro = fn_ejecuta_query($sqlGetCenDis);

						$distUnidad =$rsGetCentro['root'][0]['centroDistribucion'];
							// -----------------------------------------------  REVISAR QUE NO SE DUPLIQUE EL HOLD DE LA UNIDAD EXISTENTE
							$sqlDuplicado = "SELECT hu.vin, ".
	  										"(SELECT count(h1.claveMovimiento) FROM alhistoricounidadestbl h1 WHERE h1.vin = ud.vin AND h1.centroDistribucion='".$distUnidad."' AND h1.claveMovimiento = '".$claveHold."' LIMIT 1) as holdUnidad, ".
	  										"(SELECT count(h1.claveMovimiento) FROM alhistoricounidadestbl h1 WHERE h1.vin = ud.vin AND h1.centroDistribucion='".$distUnidad."' AND h1.claveMovimiento = (SELECT ge.estatus FROM cageneralestbl ge WHERE ge.tabla = 'alHoldsUnidadesTbl' AND ge.columna = 'CDTOL' AND ge.valor = '".$claveHold."')) as libHold ".
	  										"FROM alhistoricounidadestbl hu, alultimodetalletbl ud ".
	  										"WHERE hu.vin = ud.vin ".
	  										"AND ud.claveMovimiento IN(SELECT valor FROM cageneralestbl WHERE TABLA = 'alultimodetalletbl' AND columna = 'INVFIS') ".
	  										"AND hu.vin = '".$unidades[$i]."' ".
	  										"GROUP BY hu.vin;";

	  						$rsDuplicado = fn_ejecuta_query($sqlDuplicado);


  						if($rsDuplicado['root'] != null){

	  						if($rsDuplicado['root'][0]['holdUnidad'] == $rsDuplicado['root'][0]['libHold']){

								$addHoldUnd =	"INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento,claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
												"SELECT h1.centroDistribucion, h1.vin, '".$fechaInsert."' as fechaEvento,'".$claveHold."' as claveMovimiento, h1.distribuidor, h1.idTarifa, h1.localizacionUnidad, h1.claveChofer, 'UNIDAD CON HOLD', '".$_SESSION['idUsuario']."' as usuario, '".getclientip()."' as ip ".
												"FROM alUltimoDetalleTbl h1, alunidadestbl au, casimbolosunidadestbl su ".
												"WHERE h1.vin = au.vin ".
												"AND au.simboloUnidad = su.simboloUnidad ".
												"AND su.marca NOT IN('KI','HY','NS','GM','BM') ".
												"AND au.vin = '".$unidades[$i]."' LIMIT 1; ";

								fn_ejecuta_query($addHoldUnd);

								//echo $claveHold;
							///inserta en alunidadesdetenidastbl para cobro de custodias 

							if ($claveHold =='HH') {
								$insDetencion="INSERT INTO alunidadesdetenidastbl (`vin`, `claveMovimiento`, `centroDetiene`, `fechaInicial`, `numeroMovimiento`)"." VALUES ('".$unidades[$i]."', 'UD', '".$_SESSION['usuCompania'] ."', now(), '1')";
								fn_ejecuta_query($insDetencion);
							}

							    // FOLIO DE LAS UNIDADES
							    $sqlGetFolio = "SELECT LPAD(FOLIO - 1,4,0) AS numFolio, folio - 1 as actualFolio ".
							                    "FROM trfoliostbl ".
							                    "WHERE centroDistribucion = 'TCO' ".
							                    "AND tipoDocumento = 'HD'";

							    $rsGetFolio = fn_ejecuta_query($sqlGetFolio);

								$addTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, folio, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"SELECT '550' AS tipoTransaccion, '".$_SESSION['usuCompania']."' as centroDistribucion, 'D' as detiene,vin,'".$fechaInsert."' as fechaGeneracion, '".$claveHold."' as holdUnidad, '".$fechaInsert."' as fechaMovimiento, null as prodStatus, 'RAC550".$rsGetFolio['root'][0]['actualFolio'].".FAL' as fecha, null as hora ".
																	"FROM alultimodetalletbl ".
																	"WHERE vin = '".$unidades[$i]."';";

								fn_ejecuta_query($addTransaccion);
								
								$_REQUEST['transmision'] .= $unidades[$i].",";
								$tt++;

								}
	  						else
	  						{
	  								$f['faltantes']['root'][$ff]['vin'] = $unidades[$i];
	  								$ff++;
	  						}
  						}
					}
				}
				$f['faltantes']['records'] = $ff;				

				if($_REQUEST['i728GeneracionMdo'] == '1'){
						if($tt > 0)
						{
								$a = generaInterface();
						}
						else
						{
								$a['success'] = false;
								$a['msjResponse'] = '<b>No existen unidades para transmitir.</b>';						
						}						
				}
				else
				{
						$a = generaInterface();
				}
				if($a['numError'] > 0)
				{
						//Cuando hubo error en FTP
						$f['faltantes'] = '';
				}
						
			$a['faltantes'] = $f['faltantes'];
			//var_dump($a['faltanLiberar']);
			
			echo json_encode($a);
	}

	function liberaUnidad(){
		$a = array();
		$f = array();		
		$a['success'] = true;
		$a['msjResponse'] = '';
		$a['numError'] = 0;
		$f['faltantes'] = '';		
		$_REQUEST['transmision'] = '';
	// Variables del REQUEST -------------------------------
		$vin1 = $_REQUEST['i728VinTxa'];
		$claveHold = $_REQUEST['trap728EstatusCmb'];
	// -----------------------------------------------------
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);
		$cadena = chunk_split($vin, 17,",");
		$unidades= explode(",",$cadena);

		if($_REQUEST['i728ReprocesoDtm'] != ''){
			
			$fecha = substr($_REQUEST['i728ReprocesoDtm'],0,10);
			$hora = substr($_REQUEST['i728ReprocesoDtm'],11,19);

			$fechaInsert = $fecha." ".$hora;

		}
		else{

			$fechaAct="SELECT NOW() as fechaEvento ";
			$rsfechaAct = fn_ejecuta_query($fechaAct);

			$fechaInsert = $rsfechaAct['root'][0]['fechaEvento'];
		}

		$sqlClaveHoldFca= "SELECT  columna  FROM cageneralestbl  ".
						  "WHERE tabla = 'i728' AND valor = 'T' AND nombre = '".$claveHold."' ";

		$rssqlClaveHoldFca = fn_ejecuta_query($sqlClaveHoldFca);

		$claveHold =$rssqlClaveHoldFca['root'][0]['columna'];
		
		$ff = 0;
		$tt = 0;		
		for ($i=0; $i <(count($unidades) - 1); $i++){

					$sqlGetCenDis = "SELECT centroDistribucion ".
									"FROM alUltimoDetalleTbl ".
									"WHERE vin = '".$unidades[$i]."'; ";

					$rsGetCentro = fn_ejecuta_query($sqlGetCenDis);

					$distUnidad =$rsGetCentro['root'][0]['centroDistribucion'];

				if($_REQUEST['i728GeneracionMdo'] == '1'){

				// verifica que tenga su antecesor que es un Hold que detiene, dependiendo de su liberado
				$sqlDetUndStr =	"SELECT 1 AS tieneHold,IFNULL((SELECT 1 FROM alhistoricounidadestbl h2, alhistoricounidadestbl hu WHERE h2.vin = hu.vin and h2.fechaEvento= hu.fechaEvento and hu.vin = '".$unidades[$i]."' and hu.idHistorico =(select max(h3.idHistorico) from alhistoricounidadestbl h3 where hu.vin=h3.vin ) AND h2.claveMovimiento = '".$claveHold."' ),0) as liberada ".
								"FROM alHistoricoUnidadesTbl hu ".
								"WHERE hu.vin = '".$unidades[$i]."' ".
								"AND hu.claveMovimiento  = (SELECT  ge.nombre as nombreHold FROM cageneralestbl ge WHERE ge.tabla = 'i728' AND ge.valor = 'T' AND ge.columna = '".$claveHold."') ";

				$rsDetUndRst = fn_ejecuta_query($sqlDetUndStr);

				if($rsDetUndRst['root'][0]['tieneHold'] !== '1' && $rsDetUndRst['root'][0]['liberada'] !== '0'){
					//echo "esta entrando";
					$f['faltantes']['root'][$ff]['vin'] = $unidades[$i];
					$ff++;						
				}else if($rsDetUndRst['root'][0]['tieneHold'] == '1' && $rsDetUndRst['root'][0]['liberada'] == '1' && $_REQUEST['i728GeneracionMdo'] == 1){
					//echo "se esta duplicando";
					$f['faltantes']['root'][$ff]['vin'] = $unidades[$i];
					$ff++;					
				}else{
					
						$addHoldUnd =	"INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento,claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
										"SELECT h1.centroDistribucion, h1.vin, '".$fechaInsert."' as fechaEvento,'".$claveHold."' as claveMovimiento, h1.distribuidor, h1.idTarifa, h1.localizacionUnidad, h1.claveChofer, 'LIBERADA DE HOLD', '".$_SESSION['idUsuario']."' as usuario, '".getclientip()."' as ip ".
										"FROM alUltimoDetalleTbl h1, alunidadestbl au, casimbolosunidadestbl su ".
										"WHERE h1.vin = au.vin ".
										"AND au.simboloUnidad = su.simboloUnidad ".
										"AND su.marca NOT IN('KI','HY','NS','GM','BM') ".
										"AND au.vin = '".$unidades[$i]."' LIMIT 1; ";

						fn_ejecuta_query($addHoldUnd);

						///actualiza en alunidadesdetenidastbl para cobro de custodias 

						//echo $claveHold;

						if ($claveHold =='LH') {
							$insDetencion="UPDATE alunidadesdetenidastbl SET claveMovimiento='UL', centroLibera='".$_SESSION['usuCompania']."', fechaFinal= now() WHERE vin='".$unidades[$i]."'";
							fn_ejecuta_query($insDetencion);
						}

						    $sqlGetFolio = "SELECT LPAD(FOLIO - 1,4,0) AS numFolio, folio - 1 as actualFolio ".
						                    "FROM trfoliostbl ".
						                    "WHERE centroDistribucion = 'TCO' ".
						                    "AND tipoDocumento = 'HD'";

						    $rsGetFolio = fn_ejecuta_query($sqlGetFolio);


						$addTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, folio, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
											"SELECT '550' AS tipoTransaccion, '".$distUnidad."' as centroDistribucion, 'L' as detiene,vin,'".$fechaInsert."' as fechaGeneracion, '".$claveHold."' as holdUnidad, '".$fechaInsert."' as fechaMovimiento, null as prodStatus,'RAC550".$rsGetFolio['root'][0]['actualFolio'].".FAL' as fecha, null as hora ".
												"FROM alultimodetalletbl ".
												"WHERE vin = '".$unidades[$i]."';";

							fn_ejecuta_query($addTransaccion);
							
							$_REQUEST['transmision'] .= $unidades[$i].",";
							$tt++;							
					}
				}
			}
		
			$f['faltantes']['records'] = $ff;				

			if($_REQUEST['i728GeneracionMdo'] == '1'){
					if($tt > 0)
					{
							$a = generaInterface();
					}
					else
					{
							$a['success'] = false;
							$a['msjResponse'] = '<b>No existen unidades para transmitir.</b>';						
					}						
			}
			else
			{
					$a = generaInterface();
			}		
			if($a['numError'] > 0)
			{
					//Cuando hubo error en FTP
					$f['faltantes'] = '';
			}
			$a['faltantes'] = $f['faltantes'];			
		
			echo json_encode($a);			
	}

	function generaInterface(){

		$a = array();
		$a['success'] = true;		
		$a['msjResponse'] = '';
		$a['numError'] = 0;		
		
	// Variables del REQUEST -------------------------------
		$vin1 = $_REQUEST['i728VinTxa'];
		$claveHold = $_REQUEST['trap728EstatusCmb'];
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);
		$cadena = chunk_split($vin, 17,",");
		$unidades= explode(",",$cadena);
		if(isset($_REQUEST['transmision']) && $_REQUEST['transmision'] != '')
		{
				$unidades= explode(",",$_REQUEST['transmision']);
		}

		for ($i=0; $i < 1 ; $i++){

					$sqlGetCenDis = "SELECT centroDistribucion ".
									"FROM alUltimoDetalleTbl ".
									"WHERE vin = '".$unidades[$i]."'; ";

					$rsGetCentro = fn_ejecuta_query($sqlGetCenDis);

			$distUnidad =$rsGetCentro['root'][0]['centroDistribucion'];
		}
	// -----------------------------------------------------
		if($_REQUEST['i728ModoRdo'] == 'D'){
			$mSegType = 'E';
		}else{
			$mSegType = 'T';
		}

		if($_REQUEST['i728ReprocesoDtm'] != ''){

			$anio = substr($_REQUEST['i728ReprocesoDtm'],2,2);
			$mes = substr($_REQUEST['i728ReprocesoDtm'],5,2);
			$dia = substr($_REQUEST['i728ReprocesoDtm'],8,2);
			$hora = substr($_REQUEST['i728ReprocesoDtm'],11,2);
			$minuto = substr($_REQUEST['i728ReprocesoDtm'],14,2);
			$segundo = substr($_REQUEST['i728ReprocesoDtm'],17,2);

			$undFecha = $anio.$mes.$dia;
			$undHora = $hora.$minuto;

		}
		else{

			$fechaAct="SELECT NOW() as fechaEvento ";
			$rsfechaAct = fn_ejecuta_query($fechaAct);


			$anio = substr($rsfechaAct['root'][0]['fechaEvento'],2,2);
			$mes = substr($rsfechaAct['root'][0]['fechaEvento'],5,2);
			$dia = substr($rsfechaAct['root'][0]['fechaEvento'],8,2);
			$hora = substr($rsfechaAct['root'][0]['fechaEvento'],11,2);
			$minuto = substr($rsfechaAct['root'][0]['fechaEvento'],14,2);
			$segundo = substr($rsfechaAct['root'][0]['fechaEvento'],17,2);

			$undFecha = $anio.$mes.$dia;
			$undHora = $hora.$minuto;

		}
	// -----------------------------------------------------

	    // FOLIO DE LAS UNIDADES
	    $sqlGetFolio = "SELECT LPAD(FOLIO - 1,4,0) AS numFolio, folio - 1 as actualFolio ".
	                    "FROM trfoliostbl ".
	                    "WHERE centroDistribucion = 'TCO' ".
	                    "AND tipoDocumento = 'HD'";

	    $rsGetFolio = fn_ejecuta_query($sqlGetFolio);

	    $updFolio = "UPDATE trfoliostbl ".
	                "SET folio = '".$rsGetFolio['root'][0]['actualFolio']."' ".
	                "WHERE centroDistribucion = 'TCO' ".
	                "AND tipoDocumento = 'HD';";

		 fn_ejecuta_query($updFolio);


		$fileDir="E:/carbook/i728/RAC550".$rsGetFolio['root'][0]['numFolio'].".FAL";
		$arch1 = "RAC550".$rsGetFolio['root'][0]['numFolio'].".FAL";
	  	$fileLog = fopen($fileDir, 'a+');

		$segmento = 1;

		$distUnidad= substr($distUnidad,2,3);

		$numTotal =(count($unidades) -1 );

			// Cabecero de las unidades

			$sqlSplc = "SELECT splcCode FROM casplctbl WHERE patioCode = '".$distUnidad."' ";
			$rsSplc = fn_ejecuta_query($sqlSplc);

			  fwrite($fileLog,'ISA*03*RA550     *00*          *ZZ*XTRA           *ZZ*ADIMSDCC       *'.$undFecha.'*'.$undHora.'*U*00200*000000001*0*P*>'.PHP_EOL);
		    fwrite($fileLog,'GS*VI*XTRA*VISTA*'.$undFecha.'*'.$undHora.'*00001*T*1'.PHP_EOL);
		    fwrite($fileLog,'ST*550*00001'.str_pad($segmento,4,"0",STR_PAD_LEFT).PHP_EOL);
		    fwrite($fileLog,'BV5*'.$mSegType.'*XTRA*'.$rsSplc['root'][0]['splcCode'].'*'.str_pad($numTotal,3,"0",STR_PAD_LEFT).'*'.$claveHold.'*'.$undFecha.'***'.$undHora.PHP_EOL);

		    // Por Unidad ...
			for ($i=0; $i <(count($unidades) - 1); $i++){

				fwrite($fileLog,'VI*'.$unidades[$i].'*'.substr($unidades[$i], 10,1).'*'.'M1'.'*****'.PHP_EOL);
		    }

			fwrite($fileLog,'SE*'.str_pad(($numTotal + 1),6,"0",STR_PAD_LEFT).'*000010001'.PHP_EOL);
			fwrite($fileLog,'GE*000001*000001'.PHP_EOL);
			fwrite($fileLog,'IEA*01*000000000'.PHP_EOL);

		fclose($fileLog);

		if (!file_exists('../../respaldos/respaldosHolds/')) {	
				mkdir('../../respaldos/respaldosHolds/', 0777);
		}
		if (file_exists('../../respaldos/respaldosHolds/')) {	
				copy($fileDir, '../../respaldos/respaldosHolds/'.$arch1);
		}	
		
		/*$a = subirFtp($fileDir,$arch1);
		$a['nombreArchivo'] = '../../respaldos/respaldosHolds/'.$arch1;
		return $a;*/
	}

	function subirFtp($fileDir,$nomArch){
		$a = array();
		$a['success'] = true;
		$a['msjResponse'] = '';
		$a['numError'] = 0;
		if(file_exists($fileDir)){
			# Definimos las variables
			$host="ftp.iclfca.com";
			$port=21;
			$user="IG";
			$password="dBJY76ig";
			$ruta="/ig/SC/";				
			
			$file = $fileDir;//tobe uploaded
			$remote_file = $ruta.$nomArch;
			$nuevo_fichero = "E:/carbook/i728/respaldoArchivo/RAC550".date('ymdHis').".FAL";

			# Realizamos la conexion con el servidor
			$conn_id = ftp_connect($host);

			// loggeo
			if($conn_id)
			{
					$login = ftp_login($conn_id, $user, $password); 
			}
			
			// conexión
			if ((!$conn_id) || (!$login))
			{ 
					 $a['success'] = false;
					 $a['numError'] = 1;
					 $a['msjResponse'] = "Conexion fallida al sitio FTP!";
			}
			else
			{
					 $a['msjResponse'] = "Conectado a $host, por el usuario $user";				
			}			
			ftp_pasv($conn_id,true);
			//var_dump($conn_id, $remote_file, $file);
			
			$descarga = ftp_put($conn_id, $remote_file, $file, FTP_ASCII);

			if (!$descarga)
			{ 
					 $a['success'] = false;
					 $a['numError'] = 2;
					 $a['msjResponse'] = "Error al transmitir el archivo por FTP.";
			}			
			ftp_close($conn_id);		
		}else{
			$a['msjResponse'] = "No existe el archivo.";
		}if(!copy($file, $nuevo_fichero)){
			//echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			//echo "si se copio el archivo";
		}
		return $a;
	}
?>
