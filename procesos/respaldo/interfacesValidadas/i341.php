<?php
	// REVISADA POR CARLOS SIERRA MAYORAL.
	// FUNCIONALIDAD 100%, GENERA ARCHIVOS: RA3R.txt Y TOT3R.log
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");
   
    $startDate = date('Y-m-d', strtotime("-5 days")); //- 5 días
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $today = date('Y-m-d');
    $todayDateTime = date('Y-m-d H:i:s');
    $tipoTransaccion = 'RA3';

    $rsUnidadesCentros = getGroupUnidadesByCentros(getHistoricoUnidades341($startDate, $today));
    echo json_encode($rsUnidadesCentros);
    generarReporte341($rsUnidadesCentros);
    //insertarTransacciones($rsUnidadesCentros, $tipoTransaccion, $todayDateTime);
    generarLog341($rsUnidadesCentros, $todayDateTime);

    function getHistoricoUnidades341($startDate, $endDate) {

		

		/*$sqlHistoricoUnidades341="select * from alhistoricounidadestbl h, capuertoshktbl dc,(SELECT a6.*, MAX(a6.vuptime)FROM al660tbl 
								a6,(SELECT a6.vin, MAX(a6.vupdate) AS maxVupDate FROM al660tbl a6,
								 alhistoricounidadestbl h 
								 WHERE a6.vin = h.vin) AS tmp) AS filtered660	
								 WHERE h.vin = filtered660.vin 
								 AND h.centroDistribucion = dc.clavePuerto
								 AND DATE(h.fechaEvento) BETWEEN '2016-07-17' AND '2016-07-22'
								 AND h.vin NOT IN (SELECT im.vin FROM alInstruccionesMercedesTbl im) 
								 AND (h.idTarifa <> (SELECT t.idTarifa FROM caTarifasTbl t WHERE t.tarifa = '13') 
								 OR (h.idTarifa = (SELECT t.idTarifa FROM caTarifasTbl t WHERE t.tarifa = '13')))
								 AND DATE(h.fechaEvento) = '2016-07-22' 
								 AND (SELECT (SELECT 1 FROM capuertoshktbl ex WHERE h.distribuidor = ex.tipoDistribuidor) IS NOT NULL) = 0
								 AND (SELECT (SELECT 1 FROM alTransaccionUnidadTbl tu WHERE tu.tipoTransaccion = 'H10' AND tu.vin = h.vin 
															AND tu.centroDistribucion = h.centroDistribucion AND tu.claveMovimiento = h.claveMovimiento) IS NOT NULL) = 0 
								 AND (SELECT (SELECT 1 FROM alTransaccionUnidadTbl tu WHERE tu.tipoTransaccion = 'RA3' AND tu.vin = h.vin 
															AND tu.centroDistribucion = h.centroDistribucion AND DATE(tu.fechaGeneracionUnidad) = '2016-07-22') IS NOT NULL) = 0
								 AND (h.centroDistribucion IN ('CDTOL', 'CDAGS') 
								 AND h.claveMovimiento IN ('AM', 'AS', 'ER', 'SO', 'SV', 'ST', 'SP', 'SC', 'EF') 
								 OR (h.centroDistribucion = 'CDSAL' 
								 AND h.claveMovimiento IN ('AM', 'AS', 'ER', 'SO', 'SV', 'ST', 'SC', 'EF', 'SJ', '')) 
								 OR (h.centroDistribucion IN ('CDANG', 'CDMAZ', 'CDVER', 'CDCUA', 'CDSFE', 'CDLZC') 
								 AND h.claveMovimiento IN ('AM', 'AS', 'ER')))";
		
		 $rsUnidadesCentros=fn_ejecuta_query($sqlHistoricoUnidades341);
		 return($rsUnidadesCentros);*/

		$sqlHistoricoUnidades341 = "SELECT DISTINCT hu.centroDistribucion,hu.vin,hu.fechaEvento,hu.claveMovimiento,hu.distribuidor,t660.scacCode ".
									  "FROM alhistoricounidadestbl hu, al660tbl t660 ".
									  "WHERE hu.vin = t660.vin ".
									  "AND CAST(hu.fechaEvento as date) >= CAST('".$startDate."' AS DATE) ".
									  "AND hu.vin NOT IN(SELECT tu.vin FROM altransaccionunidadtbl tu WHERE tu.vin = hu.vin AND tu.tipoTransaccion IN('H10','RA3')) ".
									  "AND (hu.centroDistribucion  IN ('CDTOL','CDAGS') AND hu.claveMovimiento IN ('AM', 'AS', 'ER', 'SO', 'SV', 'ST', 'SP', 'SC', 'EF') ".
									      "OR hu.centroDistribucion = 'CDSAL' AND hu.claveMovimiento IN ('AM', 'AS', 'ER', 'SO', 'SV', 'ST', 'SC', 'EF', 'SJ') ".
									      "OR  hu.centroDistribucion IN ('CDANG', 'CDVER', 'CDSFE', 'CDLZC') AND hu.claveMovimiento IN ('AM', 'AS', 'ER')) ".									      
									      "AND t660.vupdate = (SELECT MAX(A6.vUpdate)FROM al660tbl a6 WHERE hu.vin = a6.vin) ".
									      "AND hu.idTarifa NOT IN (SELECT ta.idTarifa FROM caTarifasTbl ta WHERE hu.idTarifa = ta.idTarifa AND ta.tipoTarifa != 'E') ".
									      "GROUP BY hu.vin;";

		$rsUnidadesCentros=fn_ejecuta_query($sqlHistoricoUnidades341);
		return($rsUnidadesCentros);


    }
    
    function getGroupUnidadesByCentros($rsUnidades341) {
    	
    	if(!isset($rsUnidades341['root']))
    		return;

    	$rsCentrosDistribucion = array();
    	foreach ($rsUnidades341['root'] as $unidad) {
	    	
	    	$unidadRecord = $unidad;
	    	
	    	if(preg_match('/^CD+/', $unidad['localizacionUnidad']))
	    		$patio = substr($unidad['localizacionUnidad'], 2);
	    	else
	    		$patio = $unidad['localizacionUnidad'];

	    	$unidadRecord = array('patio' => $patio, 'vin' => $unidad['vin'], 'centroDistribucion' => $unidad['centroDistribucion'],
			    				  'claveMovimiento' => $unidad['claveMovimiento'], 'scaccode' => $unidad['scaccode'],
			    				  'prodstatus' => $unidad['prodstatus'], 'vupdate' => $unidad['vupdate'], 
			    				  'vuptime' => $unidad['vuptime'], 'fechaEvento' => $unidad['fechaEvento'],
			    				  'localizacionUnidad' => $unidad['localizacionUnidad']);
	    	
	    	if (!isset($rsCentrosDistribucion[$unidad['centroDistribucion']])) {
	    		$rsCentrosDistribucion[$unidad['centroDistribucion']] = array();
	    		$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']] = array();
	    		$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
	    	} else {
	    		if (!isset($rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']])) {
	    			$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']] = array();
	    			$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
	    		} else{
	    			$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
	    		}
	    	}
    	}

    	return $rsCentrosDistribucion;    	
    }


    function generarReporte341($rsCentrosDistribucion) {

    	if (count($rsCentrosDistribucion) < 1)
    		return;

    	//$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA3R.txt";
    	$fileDir = "E:/carbook/i341/RA3R.txt";
    	$flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
    	$recordsPositionValue = array();
    	foreach ($rsCentrosDistribucion as $nombreCentro => $centroDistribucion) {    			
    	    	if($nombreCentro == 'CDSAL'){
    				$rampaArr = 'ADIMSRAMPMX806';
    				$ptoUnd = 'X6';
    			}elseif($nombreCentro == 'CDTOL'){
    				$rampaArr = 'ADIMSRAMPMX807';
    				$ptoUnd = 'X7';
    			}elseif($nombreCentro =='CDANG'){
    				$rampaArr = 'ADIMSRAMPMX808';
    				$ptoUnd = 'X8';
    			}elseif($nombreCentro == 'CDAGS'){
    				$rampaArr = 'ADIMSRAMPMX011';
    				$ptoUnd = 'X4';
    			}elseif($nombreCentro == 'CDMAZ'){
    				$rampaArr = 'ADIMSRAMPMX809';
    				$ptoUnd = 'X9';
    			}elseif($nombreCentro == 'CDSFE'){
    				$rampaArr = 'ADIMSRAMPMX988';
    				$ptoUnd = 'XO';
    			}elseif($nombreCentro == 'CDCUA'){
    				$rampaArr = 'ADIMSRAMPMX888';
    				$ptoUnd = 'XU';
    			}elseif($nombreCentro == 'CDLZC'){
    				$rampaArr = 'ADIMSRAMPMX816';
    				$ptoUnd = 'PT';
    			}	
    		foreach ($centroDistribucion as $nombrePatio => $patios) {    									
    			
    		//A) ENCABEZADO
    		//Posiciones - Valores
    		$record = array( 0 =>  'ISA*03*RA3R      *00*          *ZZ*',
						    35 => $rampaArr." ",
						    50 => "*ZZ*ADIMS          *",
						    70 => date_format(date_create($today), 'y'),
						    72 => date_format(date_create($today), 'm'),
						    74 => date_format(date_create($today), 'd'),
						    76 => '*',
						    77 => date_format(date_create(date('H:i:s')), 'H'),
						    79 => date_format(date_create(date('H:i:s')), 'i'),
						    81 => '*U*00300*',
						    90 => sprintf('%09d', count($patios)),
						    99 => '*0*P*'
						   );
    		    			echo $nombrePatio;


    		$recordsPositionValue[] = $record;
    		$headerTxt = getTxt($record, array_keys($record));
    		fwrite($flReporte660, $headerTxt.out('n', 1));
    			//B) DETALLE UNIDADES
    			foreach ($patios as $unidad) {
					//Posiciones - Valores
					$record = array( 0 =>  '3R',
								     2 => $ptoUnd,
								     4 => date('mdy'),
								    10 => $unidad['vin'],
								    27 => 'AA111',
								    32 => out('s', 17),
								    49 => date_format(date_create($todayDateTime), 'h'),
								    51 => date_format(date_create($todayDateTime), 'i'),
								    53 => substr(date_format(date_create($todayDateTime), 'A'), 0, 1), 
								    54 => out('s', 25),
								    79 => lws_marca($unidad['vin'][0], $unidad['scaccode']),
							   	   );

					$detalleTxt = getTxt($record, array_keys($record));
    				fwrite($flReporte660, $detalleTxt.out('n', 1));
    			}
    			//C) TRAILER
    			//Posiciones - Valores
    			$record = array(0 => 'IEA*01*', 7 => sprintf('%09d', count($patios)));
    			$recordsPositionValue[] = $record;
    			$trailerTxt = getTxt($record, array_keys($record));
    			fwrite($flReporte660, $trailerTxt.out('n', 1));
    		}
		}
		fclose($flReporte660);
    }
    function insertarTransacciones($rsCentrosDistribucion, $tipoTransaccion, $dateTime) {
    	
    	if (!isset($rsCentrosDistribucion) || count($rsCentrosDistribucion) < 1)
    		return;

    	/*$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl".
    							"(tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, ".
								"claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) VALUES";*/

		foreach ($rsCentrosDistribucion as $nombreCentro => $rsCentroDistribucion) {
			foreach ($rsCentroDistribucion as $nombrePatio => $rsPatio) {
				$i = 0;
				echo $rsPatio;
				echo $unidad;
				foreach ($rsPatio as $unidad) {
					if($i++ > 0)
						// $sqlInsertTransaccion .= ', ';

					$vupdate = DateTime::createFromFormat('d/m/Y', $unidad['vupdate']);
					$vupdate = date_format($vupdate, 'Y-m-d');
					
					$vuptime = $unidad['vuptime'];
					if(strlen($unidad['vuptime']) > 5)
						$vuptime = substr($unidad['vuptime'], 0, 5);

					$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl ".
    											"(tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, ".
												"claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) VALUES ".												
											 	"('$tipoTransaccion', '$unidad[centroDistribucion]', '$unidad[vin]', '$dateTime', ".
											 	"'$unidad[claveMovimiento]', '$unidad[fechaEvento]', '$unidad[prodstatus]', ".
											 	"'$vupdate', '$vuptime');";
					
					fn_ejecuta_query($sqlInsertTransaccion);											 	

				}
			}
		}
		
		
		echo json_encode($sqlInsertTransaccion);

    }
    function generarLog341($rsCentrosDistribucion, $dateTime) {

    	$unidadesR3Suma = 0;
    	if (isset($rsCentrosDistribucion) && count($rsCentrosDistribucion) > 1) {
	    	foreach ($rsCentrosDistribucion as $nombreCentro => $rsCentroDistribucion) {
				foreach ($rsCentroDistribucion as $nombrePatio => $rsPatio) {
					$unidadesR3Suma += count($rsPatio);
				}
			}
		}

    	$record1 = array( 0 => 'TOTAL DE UNDS EN R3',
					     20 => date_format(date_create($dateTime), 'd/m/Y'),
					     30 => '-',
					     31 => date_format(date_create($dateTime), 'H:i:s'),
					     40 => ':',
						);

    	$record2 = array(0 =>  $unidadesR3Suma);

    	//$fileDir = $_SERVER['DOCUMENT_ROOT'].'/TOT3R.log';
    	$fileDir = "E:/carbook/i341/TOT3R.log";
    	$log341 = fopen($fileDir, 'a+') or die("$dateTime .- No se pudo generar archivo de LOG");
    	fwrite($log341, getTxt($record1, array_keys($record1)).out('n', 1));
    	fwrite($log341, getTxt($record2, array_keys($record2)).out('n', 1));
    	fclose($log341);
    }
    

?>