<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");

	$tipoHold = $_REQUEST['trap728ModoRdo'];

	if ($tipoHold == 'D') {
		detenerUnidades();
	}else{
		liberaUnidad();
	}
	
	function detenerUnidades($unidades){	

	// Variables del REQUEST -------------------------------
		$vin1 = $_REQUEST['unidades'];
		$claveHold = $_REQUEST['trap728EstatusCmb'];		
		$ciaSesVal = 'CDTOL';	
	// -----------------------------------------------------
		
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);
		$cadena = chunk_split($vin, 17,",");
		$unidades= explode(",",$cadena);		
		

		for ($i=0; $i <(count($unidades) - 1); $i++) {	

			$sqlGetExiste = "SELECT 1 as existe ".
							"FROM alUnidadesTbl ".
							"WHERE vin = '".$unidades[$i]."' ";

			$rsSqlGetExiste = fn_ejecuta_query($sqlGetExiste);

			if ($rsSqlGetExiste['root'][0]['existe'] != '1') {
			#############################################
			##  no existen en el cabecero de  unidades ##
			#############################################				

				$sqlHoldStr =	"SELECT 1 as existe FROM alHoldsUnidadesTbl ".
								"WHERE distribuidorCentro ='".$ciaSesVal."' ".
								"AND claveHold = '".$claveHold. "' ".
								"AND vin = '".$unidades[$i]."';";

				$reHoldsRst = fn_ejecuta_query($sqlHoldStr);

				if ($reHoldsRst['root'][0]['existe'] != '1') {
					$addHoldStr = 	"INSERT INTO alHoldsUnidadesTbl VALUES(".
									"'".$unidades[$i]."', ".
									"'".$claveHold."', ".
									"'".$ciaSesVal."', ".
									"'".date("Y-m-d H:m:s") ."');";

					fn_ejecuta_query($addHoldStr);
				}
				else{
					echo " La unidad:".$unidades[$i]."ya tiene el hold pendiente";
				}				
			}else{
			##########################################
			##  existen en el cabecero de  unidades ##
			##########################################					
			
					$sqlDetUndStr =	"SELECT 1 AS existe, claveMovimiento FROM alUltimoDetalleTbl hu ".
									"WHERE hu.vin = '".$unidades[$i]."'".
									"AND hu.claveMovimiento  = '".$claveHold. "' ".
									"AND vin NOT IN(SELECT vin FROM alTransaccionUnidadTbl tu WHERE tu.vin = hu.vin AND tu.tipoTransaccion = 'H10');";

					$rsDetUndRst = fn_ejecuta_query($sqlDetUndStr);

				if($rsDetUndRst['root'][0]['existe'] != '1'){

					$sqlGetCenDis = "SELECT centroDistribucion ".
									"FROM alUltimoDetalleTbl ".
									"WHERE vin = '".$unidades[$i]."';";

					$rsGetCentro = fn_ejecuta_query($sqlGetCenDis);

					if ($rsGetCentro['root'][0]['centroDistribucion'] == 'CDSAL'){
						$splcUnd = '922786801';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDTOL'){
						$splcUnd = '958770807';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDANG'){
						$splcUnd = '958770808';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDAGS'){
						$splcUnd = '940606000';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDMAZ'){
						$splcUnd = '958770809';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDSFE' || $rsGetCentro['root'][0]['centroDistribucion'] == 'CDMAT' ){
						$splcUnd = '978442999';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDLZC'){
						$splcUnd = '999990907';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDVER'){
						$splcUnd = '978442999';
					}

					$addHoldUnd =	"INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento,claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
									"SELECT centroDistribucion, vin, now() as fechaEvento,'".$claveHold."' as claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ".
									"FROM alUltimoDetalleTbl ".
									"WHERE VIN = '".$unidades[$i]."'; ";																				
					
					fn_ejecuta_query($addHoldUnd);					
					
					$addHoldUndTmp =	"INSERT INTO alinterfasechryslertbl (claveScacCode,origenSplc, centroDistribucion,vin,  distribuidor, documentoVista, fechaTransmision) ".
										"SELECT (SELECT  DISTINCT a1.scaccode FROM al660tbl a1 WHERE a1.vin = ud.vin AND a1.scaccode in ('DCC','XTRA','MITS','XKSM') AND a1.vupdate = (SELECT MAX(a2.vupdate) FROM al660tbl a2 WHERE a2.vin = ud.vin) LIMIT 1 ) as claveScacCode, ".
										"'".$splcUnd."' AS orSplc,ud.centroDistribucion,ud.vin, ud.distribuidor, 'i728' as documentoVista, now() as fechaTransmision ".
										"FROM alUltimoDetalleTbl ud ".
										"WHERE VIN = '".$unidades[$i]."'; ";																				
					
					fn_ejecuta_query($addHoldUndTmp);

					$updScacCode = "UPDATE alinterfasechryslertbl ".
									"SET claveScacCode = 'DCC' ".
									"WHERE claveScacCode IN ('XTRA','XKSM');";

					fn_ejecuta_query($updScacCode);																			
				}else{
					echo "La unidad: ".$unidades[$i].", ya cuenta con Hold";
				}								
			}			
		}
		generaInterface();
	}

	function liberaUnidad(){

	// Variables del REQUEST -------------------------------
		$vin1 = $_REQUEST['unidades'];
		$claveHold = $_REQUEST['trap728EstatusCmb'];		
		$ciaSesVal = 'CDTOL';	
	// -----------------------------------------------------
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);
		$cadena = chunk_split($vin, 17,",");
		$unidades= explode(",",$cadena);

		echo $_REQUEST['trap728Estatus1Cmb'];

		for ($i=0; $i <(count($unidades) - 1); $i++){
			$sqlGetExiste = "SELECT 1 as existe ".
							"FROM alUnidadesTbl ".
							"WHERE vin = '".$unidades[$i]."' ";

			$rsSqlGetExiste = fn_ejecuta_query($sqlGetExiste);

			if ($rsSqlGetExiste['root'][0]['existe'] != '1') {

			#############################################
			##  no existen en el cabecero de  unidades ##
			#############################################				

				$sqlHoldStr =	"SELECT 1 as existe FROM alHoldsUnidadesTbl ".
								"WHERE distribuidorCentro ='".$ciaSesVal."' ".
								"AND claveHold = '".$claveHold. "' ".
								"AND vin = '".$unidades[$i]."';";

				$reHoldsRst = fn_ejecuta_query($sqlHoldStr);

				if ($reHoldsRst['root'][0]['existe'] != '1') {
					$addHoldStr = 	"INSERT INTO alHoldsUnidadesTbl VALUES(".
									"'".$unidades[$i]."', ".
									"'".$claveHold."', ".
									"'".$ciaSesVal."', ".
									"'".date("Y-m-d H:m:s") ."');";

					fn_ejecuta_query($addHoldStr);
				}
				else{
					echo " La unidad:".$unidades[$i]."ya tiene el hold pendiente";
				}				
			}else{
			##########################################
			##  existen en el cabecero de  unidades ##
			##########################################
			
				// verifica que tenga su antecesor que es un Hold que detiene, dependiendo de su liberado	
				$sqlDetUndStr =	"SELECT 1 AS tieneHold,(SELECT 1 FROM alhistoricounidadestbl h2 WHERE h2.vin = hu.vin AND h2.claveMovimiento = 'LC' ) as liberada ".
								"FROM alHistoricoUnidadesTbl hu ".
								"WHERE hu.vin = '".$unidades[$i]."' ".
								"AND hu.claveMovimiento  = (SELECT  ge.nombre as nombreHold FROM cageneralestbl ge WHERE ge.tabla = 'i728' AND ge.valor = 'T' AND ge.columna = '".$claveHold."') ".
								"AND vin NOT IN(SELECT vin FROM alTransaccionUnidadTbl tu WHERE tu.vin = hu.vin AND tu.tipoTransaccion = 'H10');";

				$rsDetUndRst = fn_ejecuta_query($sqlDetUndStr);

				if($rsDetUndRst['root'][0]['tieneHold'] == '1' && $rsDetUndRst['root'][0]['tieneHold'] != '1'){

				}else{
					$sqlGetCenDis = "SELECT centroDistribucion ".
									"FROM alUltimoDetalleTbl ".
									"WHERE vin = '".$unidades[$i]."';";

					$rsGetCentro = fn_ejecuta_query($sqlGetCenDis);

					if ($rsGetCentro['root'][0]['centroDistribucion'] = 'CDSAL'){
						$splcUnd = '922786801';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDTOL'){
						$splcUnd = '958770807';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDANG'){
						$splcUnd = '958770808';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDAGS'){
						$splcUnd = '940606000';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDMAZ'){
						$splcUnd = '958770809';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDSFE' || $rsGetCentro['root'][0]['centroDistribucion'] == 'CDMAT' ){
						$splcUnd = '978442999';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDLZC'){
						$splcUnd = '999990907';
					}elseif($rsGetCentro['root'][0]['centroDistribucion'] == 'CDVER'){
						$splcUnd = '978442999';
					}

					$addHoldUnd =	"INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento,claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
									"SELECT centroDistribucion, vin, now() as fechaEvento,'".$claveHold."' as claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ".
									"FROM alUltimoDetalleTbl ".
									"WHERE VIN = '".$unidades[$i]."'; ";																				
					
					fn_ejecuta_query($addHoldUnd);					

					$addHoldUndTmp =	"INSERT INTO alinterfasechryslertbl (claveScacCode,origenSplc, centroDistribucion,vin,  distribuidor, documentoVista, fechaTransmision) ".
										"SELECT (SELECT  DISTINCT a1.scaccode FROM al660tbl a1 WHERE a1.vin = ud.vin AND a1.scaccode in ('DCC','XTRA','MITS','XKSM') AND a1.vupdate = (SELECT MAX(a2.vupdate) FROM al660tbl a2 WHERE a2.vin = ud.vin) LIMIT 1 ) as claveScacCode, ".
										"'".$splcUnd."' AS orSplc,ud.centroDistribucion,ud.vin, ud.distribuidor, 'i728' as documentoVista, now() as fechaTransmision ".
										"FROM alUltimoDetalleTbl ud ".
										"WHERE VIN = '".$unidades[$i]."'; ";																				
					
					fn_ejecuta_query($addHoldUndTmp);

					$updScacCode = "UPDATE alinterfasechryslertbl ".
									"SET claveScacCode = 'DCC' ".
									"WHERE claveScacCode IN ('XTRA','XKSM');";

					fn_ejecuta_query($updScacCode);						
				}
			}			
		}
		generaInterface();	
	}

	function generaInterface(){

	// Variables del REQUEST -------------------------------
		$vin1 = $_REQUEST['unidades'];
		$claveHold = $_REQUEST['trap728EstatusCmb'];		
		$ciaSesVal = 'CDTOL';
	// -----------------------------------------------------			

		$sqlGetSplc = "SELECT distinct(origenSplc) as oriSplc ".
					  "FROM alinterfasechryslertbl;";

		$rsSqlGetSplc = fn_ejecuta_query($sqlGetSplc);		

		$fileDir="E:/carbook/i728/RAC550.FAL";
	    $fileLog = fopen($fileDir, 'a+');
		
		$segmento = 1;
		$countUnds = 1;
		$countSegSc = 1;			

		for ($s=0; $s < sizeof($rsSqlGetSplc['root']) ; $s++) {
			
			$sqlGetUnds = "SELECT ic.vin, '".$claveHold."' as claveMovimiento, ic.fechaTransmision, ic.claveScacCode, ic.origenSplc, ".
			"(SELECT valor FROM cageneralestbl WHERE tabla = 'i728' AND columna = '".$claveHold."' ) as segType, ".
			"(SELECT SUBSTRING(a6.routedes, 1, 2) FROM al660tbl a6 WHERE a6.vin = ic.vin  AND a6.vupdate = (SELECT MAX(a1.vupdate) FROM al660tbl a1 WHERE a1.vin = a6.vin) limit 1) as routeDes ".
			"FROM alinterfasechryslertbl ic ".
			"WHERE documentoVista = 'i728' ".
			"AND origenSplc = '".$rsSqlGetSplc['root'][$s]['oriSplc']."'";

			$rsSqlGetUnds = fn_ejecuta_query($sqlGetUnds);

			$mSegType = $rsSqlGetUnds['root'][0]['segType'];
		    $mSplc = $rsSqlGetUnds['root'][0]['origenSplc'];
			

			$numTotal =sizeof($rsSqlGetUnds['root']);		

			
			// Cabecero de las unidades

			
			fwrite($fileLog,'ISA*03*RA550     *00*          *ZZ*XTRA           *ZZ*ADMISDCC       *'.date("ymd*Hi").'*U*00200*000000001*0*P>'.PHP_EOL); 			
		    fwrite($fileLog,'GS*VI*XTRA*VISTA*'.date("ymd*Hi").'*00001*T*1'.PHP_EOL);
		    fwrite($fileLog,'ST*550*00001'.str_pad($segmento,4,"0",STR_PAD_LEFT).PHP_EOL);
		    fwrite($fileLog,'BV5*'.$mSegType.'*XTRA*'.$mSplc.'*'.str_pad($numTotal,3,"0",STR_PAD_LEFT).'*'.$claveHold.'*'.date("ymd").'***'.date("Hi").PHP_EOL);

echo "entro:".$s;

		    // Por Unidad ...
		    for ($e=0; $e < sizeof($rsSqlGetUnds['root']); $e++) {		    	

		    	//fwrite($fileLog,'ST*550*000010001'.PHP_EOL);
		    	//fwrite($fileLog, 'BV5*E*XTRA*000000000*'.str_pad($numTotal,3,"0",STR_PAD_LEFT).'*'.$cMovimiento.'*'.date("ymd***Hi",strtotime($fMovimiento[$i])).PHP_EOL);
				fwrite($fileLog,'VI*'.$rsSqlGetUnds['root'][$e]['vin'].'*'.$rsSqlGetUnds['root'][$e]['segType'].'*'.$rsSqlGetUnds['root'][$e]['routeDes'].'*****'.PHP_EOL);
				//fwrite($fileLog,'SE*'.str_pad(count($numTotal),6,"0",STR_PAD_LEFT).'*00001'.str_pad($numTotal,4,"0",STR_PAD_LEFT).PHP_EOL);
		    	//$e ++;
		    }
							 				
			fwrite($fileLog,'SE*'.str_pad(($numTotal + 1),6,"0",STR_PAD_LEFT).'*000010001'.PHP_EOL);
			fwrite($fileLog,'GE*000001*000001'.PHP_EOL);
			fwrite($fileLog,'IEA*01*000000000'.PHP_EOL);
	 			 		
	 		//subirFtp();				
		}			
	fclose($fileLog);
	}
?>
