<?php
    session_start();
	
	//include 'iVerificacionRepuve.php';
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");

	
    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["interfacesLzcRepuveArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["interfacesLzcRepuveArchivoFld"]["error"];

		} else {
			$temp_file_name = $_FILES['interfacesLzcRepuveArchivoFld']['tmp_name']; 
			$original_file_name = $_FILES['interfacesLzcRepuveArchivoFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/genArchRepuve/'. $file_name . $ext;
		  
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					//$a['root'] = genArchivoTxt($new_name);
					$a['root'] = genArchivoTxt();
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);


	function genArchivoTxt(){

        date_default_timezone_set('America/Mexico_City');

		$ruta = "C:\\servidores\\apache\\htdocs\\genArchRepuve\\";
		$ruta1 = "C:\\servidores\\apache\\htdocs\\respaldogenArchRepuve\\";

		$nombres = scandir($ruta,1);        
		$files2 = count($nombres);
		$files3 = $files2 -1;
		$files4 = $files2 -2;

		unset($nombres[$files3]);
		unset($nombres[$files4]);

		//echo json_encode(sizeof($nombresAr));
		for ($i=0; $i < sizeof($nombres); $i++){ 
			$lineas1 = $ruta.$nombres[$i];			
			$lineas = fopen($lineas1,"r");
			
			while(!feof($lineas)){							
				$lin = fgets($lineas);
				
				$datos = explode("|",$lin);
				$vin = trim($datos[0]);
				$fRepuve = trim($datos[1]);
				$tid = trim($datos[2]);
				$date = date_create($datos[3]);
				$date1= date_format($date, 'Y-m-d H:i:s');

		        $SqlFolioRepuve= "SELECT folioRepuve ".
		        				 "from alrepuvetbl ".
		        				 "WHERE folioRepuve=".$fRepuve." ";
		        $rsSqlFolioRepuve= fn_ejecuta_query($SqlFolioRepuve);
		       // echo json_encode($SqlFolioRepuve);

				if(sizeof($rsSqlFolioRepuve['root']) != 0){

						echo"archivos ya existentes";
					
				}else{
				   $sqlAddRepuve= "INSERT INTO alrepuvetbl (folioRepuve,marca,centroDistribucion,vin,tid,fechaEvento,estatus,observacion) ".
		                        "VALUES (".$fRepuve.",'CD','LZC02','".$vin."','".$tid."','".$date1."','VR',NULL)";

					//echo json_encode($sqlAddRepuve);

				  $rsSqlAddRepuve= fn_ejecuta_query($sqlAddRepuve);
				}  	
			}
			fclose($lineas);
			copy($lineas1, $ruta1.'\\verificacion'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
			
		}
			unlink($lineas1);	
			

		 $sqlRep = 	"SELECT dy.cveStatus ".
					"FROM alultimodetalletbl al, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = dy.vin ".
					"AND al.centroDistribucion='LZC02' ".
					"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='REP') ";

		$rsRep= fn_ejecuta_query($sqlRep);


		for ($i=0; $i <sizeof($rsRep['root']) ; $i++) { 

			if (substr($rsRep['root'][$i]['cveStatus'], -1) == 'K') {				
				$arrK[] = $rsRep['root'][$i];				
			}
		/////////////////////////////////////////////////////////
			else if (substr($rsRep['root'][$i]['cveStatus'], -1) == 'H') {
				$arrH[] = $rsRep['root'][$i];
			}
			else{
				//echo "string";
			}
		}

	if (count($arrK) != 0) {
		$varEstatus = 'DK';
		$nomArchivo = 'K';
	}if (count($arrH) != 0) {
		$varEstatus = 'DH';
		$nomArchivo = 'H';		
	}

	$sqlGenerarDatos= "SELECT r.vin,r.tid,r.folioRepuve, m.cveDisFac,r.fechaEvento ".
	  					"from alrepuvetbl r,alinstruccionesmercedestbl m ".
	  					"where r.estatus='VR' and r.vin=m.vin ".
	  					"and r.vin not in (select vin from altransaccionunidadtbl where tipoTransaccion='REP') ".
	  					"and m.cveStatus='".$varEstatus."' ";

	$rssqlGenerarDatos= fn_ejecuta_query($sqlGenerarDatos);


	if(sizeof($rssqlGenerarDatos['root']) != NULL){

		$fecha = date("YmdHis");
	    $fileDir = "C:/carbook/archivosInterfaces/".$nomArchivo."MM_REP_"."$fecha".".txt";
	    $fileRepuve = fopen($fileDir, "a") or die("No se pudo abrir archivo");
	    $recordsPositionValue = array();
	    //for ($i = 0; $i <= $patioGroup; i++) {       
	        //foreach ($patioGroup as $conteo => $patioGroup) {
	        
	        //A) ENCABEZADO
	    fwrite($fileRepuve, 'REPH APS  GMX  REP'."$fecha".PHP_EOL);
	            
	            //B) DETALLE UNIDADES

	    for ($i=0; $i < sizeof($rssqlGenerarDatos['root']); $i++){ 

	        fwrite($fileRepuve,'REP  '.'FT16 '.$rssqlGenerarDatos['root'][$i]['vin'].$rssqlGenerarDatos['root'][$i]['folioRepuve'].$rssqlGenerarDatos['root'][$i]['tid'].substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],0,4).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],5,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],8,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],11,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],14,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],17,2).PHP_EOL);
	    }
		//C) TRAILER
	    fwrite($fileRepuve, 'REPT '.sprintf('%06d', (sizeof($rssqlGenerarDatos['root'])+2)).PHP_EOL);
	                  
	    fclose($fileRepuve);

		$SqlSelVin= "SELECT vin FROM alrepuvetbl WHERE estatus='VR'";
		$rsSqlSelVin= fn_ejecuta_query($SqlSelVin);


		for ($i=0; $i <sizeof($rsSqlSelVin['root']) ; $i++){ 

			$vines[]=$rsSqlSelVin['root'][$i]['vin'];
		}

		$cadena = implode(",", $vines);
		$contador= count($rsSqlSelVin['root']);
		//echo json_encode($contador);

		$SqlSelfecha= "SELECT fechaEvento FROM alrepuvetbl WHERE estatus='VR'";
		$rsSqlSelfecha= fn_ejecuta_query($SqlSelfecha);

		$sqlTransaccion = "SELECT vin from altransaccionunidadtbl ".
						   "where vin in ('".$cadena."') and tipoTransaccion='REP' and claveMovimiento='GR' ";
		$rssqlTransaccion= fn_ejecuta_query($sqlTransaccion);

							
		if (sizeof($rssqlTransaccion['root']) == 0){

			for ($i=0; $i< ($contador); $i++){ 
				$vin = $rsSqlSelVin['root'][$i]['vin'];
				$fecha1 = $rsSqlSelfecha['root'][$i]['fechaEvento'];
				$today = date("Y-m-d H:i:s");
				$fecha = substr($today,0,10);
				$hora=substr($today,11,8);

				$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin, ".
		             				"fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".
									"VALUES ('REP','LZC02',NULL,'".$rsSqlSelVin['root'][$i]['vin']."','".$rsSqlSelfecha['root'][$i]['fechaEvento']."','GR',NOW(),NULL,'".$fecha."','".$hora."') ";

				//echo json_encode($sqlAddTransaccion);

				fn_ejecuta_query($sqlAddTransaccion);

				$sqlUpdrepuve= "UPDATE alrepuvetbl SET estatus='GR' Where vin='".$vin."' AND estatus='VR' ";
				fn_ejecuta_query($sqlUpdrepuve);

				//echo json_encode($sqlUpdrepuve);
			}
		}	      
	}else{
		echo "No se genero archivo Repuve  ";
	}					
}

?>