<?php
    session_start();

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");

	RA3Rcmdat();

	function RA3Rcmdat(){

    // Variables del REQUEST -------------------------------
        $vin1 = $_REQUEST['intCrymexUndsTxa'];
        $clavePatio = $_REQUEST['intCrymexPatioCmb'];        
    // -----------------------------------------------------
        $buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar = array("", "", "", "");
        $vin = str_ireplace($buscar,$reemplazar,$vin1);
        $cadena = chunk_split($vin, 17,"','");
        $unidades= explode(",",$cadena);  

		$sqlGeneraDatos = "SELECT h.vin, h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%m%d%y')AS fechaFormato, claveMovimiento ".
	      					"FROM alHistoricoUnidadesTbl h ".
	      					"WHERE h.claveMovimiento IN (SELECT valor FROM cageneralestbl WHERE tabla = 'i341' AND columna = 'outGate') ".
                            " AND h.centroDistribucion = '".$_REQUEST['intCrymexPatioCmb']."' ".	        				
	        				" AND h.vin IN('".substr($cadena,0,-2).") ".
                            "AND h.fechaEvento in (SELECT MIN(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.vin = h.vin AND h2.centroDistribucion = h.centroDistribucion)";   

	    $rsGeneraDatos = fn_ejecuta_query($sqlGeneraDatos);

		$patioGroup = $rsTotalUnidades['root'][0]['numeroUnidad'];

		$cveMovimiento = $rsGeneraDatos['root'][0]['claveMoviomiento'];

        $sqlGetSplc = "SELECT valor,nombre FROM cageneralestbl ".
                        "WHERE tabla = 'caSplcTbl' ".
                        "AND columna = '".$_REQUEST['intCrymexPatioCmb']."';";

        $rsSqlGetSplc = fn_ejecuta_query($sqlGetSplc);

        $cveSplc = $rsSqlGetSplc['root'][0]['nombre'];
        $cveCod = $rsSqlGetSplc['root'][0]['valor'];		

		if(sizeof($rsGeneraDatos['root']) != null){
			//$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";

        	$sqlGetFolio = "SELECT LPAD(FOLIO + 1,5,0) AS numFolio, folio + 1 as actualFolio ".
            	            "FROM trfoliostbl ".
                        	"WHERE centroDistribucion = 'CMDAT' ".
                        	"AND tipoDocumento = '3R'";

        	$rsGetFolio = fn_ejecuta_query($sqlGetFolio);

        	echo json_encode($rsGetFolio);

        	$updFolio = "UPDATE trfoliostbl ".
            	        "SET folio = '".$rsGetFolio['root'][0]['actualFolio']."' ".
                    	"WHERE centroDistribucion = 'CMDAT' ".
                    	"AND tipoDocumento = '3R';";

        	$rsUpdFolio = fn_ejecuta_query($updFolio);

			$nombreArchivo = "RA3R".$rsGetFolio['root'][0]['numFolio'].".txt";        

	        $fileDir = "E:/carbook/i341/RA3R".$rsGetFolio['root'][0]['numFolio'].".txt";
	        $nomArch = "RA3R";
	        $dirResp = "E:/carbook/i341/respArchivo/";
	        $rampaFija = '807';
	        $lw_ramp = 'ADIMSRAMPMX';
	        $flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
	        $recordsPositionValue = array();
		        //for ($i = 0; $i <= $patioGroup; i++) {
		            //foreach ($patioGroup as $conteo => $patioGroup) {
		            //A) ENCABEZADO
		    fwrite($flReporte660, 'ISA*03*RA3R      *00*          *ZZ*ADIMSRAMPMX'.$cveSplc.' *ZZ*ADIMS          *'.date_format(date_create($today), 'ymd').'*'.date_format(date_create(date('H:i:s')), 'H').''.date_format(date_create(date('H:i:s')), 'i').'*U*00300*'.sprintf('%09d', sizeof($rsGeneraDatos['root'])).'*0*P*'.PHP_EOL);

            //B) DETALLE UNIDADES
            for ($i=0; $i < sizeof($rsGeneraDatos['root']); $i++){
                fwrite($flReporte660,'3R'.$cveCod.$rsGeneraDatos['root'][$i]['fechaFormato'].$rsGeneraDatos['root'][$i]['vin'].$clavePatio.out('s', 17).date_format(date_create($rsGeneraDatos['root'][$i]['fechaEvento']),'Hi').out('s', 26).PHP_EOL);
                $sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".                     
                                    "VALUES ('RA3', ".
                                    "'CMDAT', ".
                                    "'1', ".
                                    "'".$rsGeneraDatos['root'][$i]['vin']."', ".
                                    "NOW(), ".
                                    "'".$cveMovimiento."', ".
                                    "'".$rsGeneraDatos['root'][$i]['fechaFormato']."', ".
                                    "'1', ".
                                    "'".$nombreArchivo."', ".
                                    "'1');"; 

                fn_ejecuta_query($sqlAddTransaccion);		                
            }
		                
		    fwrite($flReporte660, 'IEA*01*'.sprintf('%09d', sizeof($rsGeneraDatos['root'])).PHP_EOL);
		    fclose($flReporte660);
		}else{
		    	echo "No se genero Archivo RA3R  ";
		}

		echo "Termino RA3R Comodato: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
		//subirFtp_RA3R($nombreArchivo);
	}

    function subirFtp_RA3R($nombreArchivo){
        $fileDir = "E:/carbook/".$nombreArchivo;
        if(file_exists($fileDir)){
            # Definimos las variables
            $host = "ftp.iclfca.com";
            $port = 21;
            $user = "IG";
            $password = "dBJY76ig";
            $ruta = "/ig/SC/";
            $file = $fileDir;//tobe uploaded
            $remote_file = $nombreArchivo;
            $nuevo_fichero = "E:/carbook/i340/respArchivo/".$nombreArchivo;

            # Realizamos la conexion con el servidor
            $conn_id = @ftp_connect($host);//,$port);
            if($conn_id){
                    # Realizamos el login con nuestro usuario y contraseña
                if(@ftp_login($conn_id,$user,$password)){
                    # Canviamos al directorio especificado
                    if(@ftp_chdir($conn_id,$ruta)){
                        # Subimos el fichero
                        if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
                        echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
                        }else{
                            echo "No ha sido posible subir el fichero";
                        }
                    }else
                        echo "No existe el directorio especificado";
                }else
                    echo "El usuario o la contraseña son incorrectos";
                    # Cerramos la conexion ftp
                    ftp_close($conn_id);
            }else
                echo "No ha sido posible conectar con el servidor";
        }else{
            echo "no existe el archivo";
        }if(!copy($file, $nuevo_fichero)){
            echo "Error al copiar $fichero...\n";
        }else{
            unlink($file);
            echo "si se copio el archivo";
        }
    }		

?>
