<?php 
	require("../funciones/generales.php");
    require("../funciones/construct.php");
    require_once("../funciones/utilidadesProcesos.php");
    require_once("../funciones/utilidades.php");    

	$fechaAnt = strtotime ( '-5 day' , strtotime (date("Y-m-d")) ) ;
	$fechaAnt = date ( 'Y-m-d' , $fechaAnt );

	$vin1= $_REQUEST['vines'];

	$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
	$reemplazar=array("", "", "", "");
	$vin=str_ireplace($buscar,$reemplazar,$vin1);

	$cadena = chunk_split($vin, 17,"','");
	
	$vines=substr($cadena,0,-2);
   
    //$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
    $fileDir = "E:/carbook/i343a/HA510.txt";
    $flReporte660 = fopen($fileDir, "a") or die("No se pudo generar ,interfaz");

   	//A) ENCABEZADO
	fwrite($flReporte660,'ISA*03*HA510            *00*          *ZZ*XTRA        *ZZ*ADIMSDCC    *'.date('ymd').'*'.date('hi').'*U*00200*'.sprintf('%09d','1').'*0*P*<'.PHP_EOL);
	fwrite($flReporte660,'GS*VI*XTRA*INNI*'.date('ymd').'*'.date('hi').'*'.date('hi').'*1*T*1'.PHP_EOL);

	$sqlGetHora = "SELECT DISTINCT(h1.fechaEvento), centroDistribucion
					FROM alhistoricounidadestbl h1, al660tbl a1
					WHERE h1.vin = a1.vin
					AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')
					AND h1.claveMovimiento IN ('ED','OM','ER','RO','PT','OK','ET','CS','DP')
					AND a1.vupdate = (SELECT MAX(vupdate) FROM al660tbl a2 WHERE a2.vin = h1.vin)
					AND h1.vin IN ('".$vines.")
					ORDER BY h1.fechaEvento;";

	$rsSqlGetHora = fn_ejecuta_query($sqlGetHora);

	for ($e=0; $e <sizeof($rsSqlGetHora['root']) ; $e++) { 
		
		$incremento = $incremento + 1;
		
		$sqlHstUndStr =	"SELECT DISTINCT h1.centroDistribucion, h1.vin, h1.fechaEvento, h1.claveMovimiento, h1.distribuidor,a1.scaccode ,
						(SELECT h2.fechaEvento FROM alhistoricounidadestbl h2 WHERE h2.vin =  h1.vin AND h2.claveMovimiento = 'AM' LIMIT 1) AS fechaAM
						FROM alhistoricounidadestbl h1, al660tbl a1
						WHERE h1.vin = a1.vin
						AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')
						AND h1.claveMovimiento IN ('ED','OM','ER','RO','PT','OK','ET','CS','DP')
						AND a1.vupdate = (SELECT MAX(vupdate) FROM al660tbl a2 WHERE a2.vin = h1.vin)
						AND h1.fechaEvento = '".$rsSqlGetHora['root'][$e]['fechaEvento']."'
						AND h1.vin IN ('".$vines.")
						ORDER BY h1.fechaEvento;";		

		$HstUndRst = fn_ejecuta_query($sqlHstUndStr);

		if ($rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDSAL'){
			$splcUnd = '922786801';
		}elseif($rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDTOL'){
			$splcUnd = '958770807';
		}elseif($rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDANG'){
			$splcUnd = '958770808';
		}elseif($rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDAGS'){
			$splcUnd = '940606000';
		}elseif($rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDMAZ'){
			$splcUnd = '958770809';
		}elseif($rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDSFE' || $rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDMAT' ){
			$splcUnd = '978442999';
		}elseif($rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDLZC'){
			$splcUnd = '999990907';
		}elseif($rsSqlGetHora['root'][$e]['centroDistribucion'] == 'CDVER'){
			$splcUnd = '978442999';
		}				
		
		fwrite($flReporte660,'ST*510*1'.sprintf('%04d',$incremento).PHP_EOL);
		fwrite($flReporte660,'BV1*XTRA*'.$splcUnd.'*'.sprintf('%04d', sizeof($HstUndRst['root'])).PHP_EOL);
		
		for ($i=0; $i < sizeof($HstUndRst['root']); $i++) {						

			fwrite($flReporte660,'VI*'.$HstUndRst['root'][$i]['vin'].'****'.$HstUndRst['root'][$i]['distribuidor'].PHP_EOL);
			fwrite($flReporte660,'P1**'.date("ymd",strtotime($HstUndRst['root'][$i]['fechaAM'])).'*A'.PHP_EOL);
			fwrite($flReporte660,'P2**'.date("ymd",strtotime($HstUndRst['root'][$i]['fechaEvento'])).'*A*'.date("hi",strtotime($HstUndRst['root'][$i]['fechaEvento'])).PHP_EOL);			
		}
		fwrite($flReporte660,'SE*'.sprintf('%02d', sizeof($HstUndRst['root'])).'*1'.$incremento.PHP_EOL);		
	}

	fwrite($flReporte660,'GE*'.$incremento.'*1'.PHP_EOL);
	fwrite($flReporte660,'IEA*01*'.sprintf('%09d','1').PHP_EOL);

?>	              