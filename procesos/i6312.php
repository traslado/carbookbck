<?php
   
  /* 
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    date_default_timezone_set('America/Mexico_City');

     i6312(); 
	*/	
	function i6312(){	

		echo "Inicio i6312: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

        $sqlUnidadesbmw= 	"SELECT bm.*,tu.idTalon,tv.folio  ".
							"FROM alhistoricoUnidadesTbl hu, alinstruccionesbmwTbl bm, trunidadesdetallestalonestbl tu, trtalonesviajestbl tv ".
							"WHERE hu.vin = bm.vin  ".
							"and bm.vin=tu.vin ".
							"and tu.idTalon=tv.idTalon ".
							"and tu.estatus != 'X' ".
							"AND hu.centroDistribucion='CDSLP' ".
							"AND hu.claveMovimiento='AM' ".
							"AND bm.cveStatus ='BC' ".
							"and bm.vin in (select vin from altransaccionunidadtbl where tipoTransaccion='6310') ".
							"and bm.vin not in (select vin from altransaccionunidadtbl where tipoTransaccion='6312'); ";

		$rssqlUnidadesbmw= fn_ejecuta_query($sqlUnidadesbmw);

		//echo json_encode($rssqlUnidadesbmw);
 		//var_dump($rssqlUnidadesbmw);
		if ( sizeof($rssqlUnidadesbmw['root']) > 0) {


			for ($i=0; $i <sizeof($rssqlUnidadesbmw['root']) ; $i++) { 


				$talon[]=$rssqlUnidadesbmw['root'][$i]['idTalon']; 	

			}

			$distictTalones= array_unique($talon);
			sort($distictTalones);

			for ($i=0; $i < sizeof($distictTalones) ; $i++) {

				        $sqlUnidadesbmwporTalon= "SELECT bm.*,hu.*,tu.idTalon,tv.folio ".
							"FROM alhistoricoUnidadesTbl hu, alinstruccionesbmwTbl bm, trunidadesdetallestalonestbl tu, trtalonesviajestbl tv ".
							"WHERE hu.vin = bm.vin ".
							"and bm.vin=tu.vin ".
							"and tu.idTalon=tv.idTalon ".
							"and tu.estatus != 'X' ".
							"AND hu.centroDistribucion='CDSLP' ".
							"AND hu.claveMovimiento='AM' ".
							"AND bm.cveStatus ='BC' ".
							"AND tu.idTalon= ".$distictTalones[$i]." ".
							"and bm.vin in (select vin from altransaccionunidadtbl where tipoTransaccion='6310') ".
							"and bm.vin not in (select vin from altransaccionunidadtbl where tipoTransaccion='6312'); ";	

						$rssqlUnidadesbmwporTalon= fn_ejecuta_query($sqlUnidadesbmwporTalon);

				   if ( sizeof($rssqlUnidadesbmwporTalon['root']) > 0) {

				   		$fecha = date('YmdHis',(strtotime("+1$i second")));
						generaArchivo6312($rssqlUnidadesbmwporTalon,$fecha);

				   }

			}
	}

	echo "Fin i6312: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
}		

	function generaArchivo6312($rssqlUnidadesbmwporTalon,$fecha){

		echo json_encode($fecha);
	    $fechaSinSegundos = date('ymdHi');
	    $fileDir = "E:/carbook/i631/instrucciones/".$fecha."0001_TRC1.VDA631.AFG.txt";
	    $nombreRespaldo = "E:/carbook/i631/respaldo631-2/".$fecha."0001_TRC1.VDA631.AFG.txt";
	    $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
	    $recordsPositionValue = array();
	    $nombreBusqueda= $fecha."0001_TRC1.VDA631.AFG.txt";

	    //A) ENCABEZADO


	    $folioBMW="SELECT folio FROM trfoliosTbl WHERE centroDistribucion='CDSLP' and compania='BM' and tipoDocumento='BM'";
	    $rsfolioBMW = fn_ejecuta_query($folioBMW);  
	    $folioAnterior= $rsfolioBMW['root'][0]['folio'] ;
	    $folio= $folioAnterior + 1 ;

	    fwrite($fileRec, '63102AFGSTAT  TRC1     '.sprintf('%05d',($folioAnterior)).sprintf('%05d',($folio))."$fechaSinSegundos                                                                                     ".PHP_EOL);
	        

	    $contador=sizeof($rssqlUnidadesbmwporTalon['root']);

	

	        //B) DETALLE UNIDADES

	        for ($i=0; $i<$contador ; $i++) 
	        { 

	           fwrite($fileRec,'632'.sprintf('%-2s',$rssqlUnidadesbmwporTalon['root'][$i]['numeroVersion']).'2'.sprintf('%-2s',$rssqlUnidadesbmwporTalon['root'][$i]['tipoUbicacionAlmacenamientoSalida']).sprintf('%-7s',$rssqlUnidadesbmwporTalon['root'][$i]['codigoUbicacionAlmacenamientoSalida']).sprintf('%-2s',$rssqlUnidadesbmwporTalon['root'][$i]['tipoUbicacionAlmacenamientoEntrega']).sprintf('%-7s',$rssqlUnidadesbmwporTalon['root'][$i]['codigoUbicacionAlmacenamientoEntrega']).substr($rssqlUnidadesbmwporTalon['root'][$i]['fechaEvento'],2,2).substr($rssqlUnidadesbmwporTalon['root'][$i]['fechaEvento'],5,2).substr($rssqlUnidadesbmwporTalon['root'][$i]['fechaEvento'],8,2).substr($rssqlUnidadesbmwporTalon['root'][$i]['fechaEvento'],11,2).substr($rssqlUnidadesbmwporTalon['root'][$i]['fechaEvento'],14,2).$rssqlUnidadesbmwporTalon['root'][$i]['vin'].'01'.sprintf('%-25s',$rssqlUnidadesbmwporTalon['root'][$i]['numeroCarga'].'-0003606').'                0003606                                '.PHP_EOL);


				$today = date("Y-m-d H:i:s");
				$fecha1 = substr($today,0,10);
				$hora=substr($today,11,8);

			$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin, ".
			 				"fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".
							"VALUES ('6312','CDSLP',NULL,'".$rssqlUnidadesbmwporTalon['root'][$i]['vin']."','".$rssqlUnidadesbmwporTalon['root'][$i]['fechaEvento']."','AM','".$today."','BC','".$fecha1."','".$hora."') ";    

			fn_ejecuta_query($sqlAddTransaccion);


		$UpdStatus= "UPDATE alinstruccionesbmwTbl set cveStatus='BA' WHERE vin='".$rssqlUnidadesbmwporTalon['root'][$i]['vin']."' and centroDistribucion='CDSLP'";

		fn_ejecuta_query($UpdStatus);

		
	 }

	 		$UpdFolio= "UPDATE trfoliosTbl set folio='".$folio."' WHERE centroDistribucion='CDSLP'  and compania='BM' and tipoDocumento='BM'";

			fn_ejecuta_query($UpdFolio);
			//C) TRAILER
			fwrite($fileRec,'639020000001'.sprintf('%07d',($contador)).'000000100000000000000                                                                                        ');
	       	fclose($fileRec);
	       	copy($fileDir, $nombreRespaldo);

	}
?> 