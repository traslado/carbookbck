<?php
   /* require_once("C:/Servidores/apache/htdocs/carbookbck/funciones/generales.php");
    require_once("C:/Servidores/apache/htdocs/carbookbck/funciones/construct.php");
    require_once("C:/Servidores/apache/htdocs/carbookbck/funciones/utilidades.php");
    require_once("C:/Servidores/apache/htdocs/carbookbck/funciones/utilidadesProcesos.php");*/

    //i341_CB();

    function i341_CB(){

        echo "inicio 341 ";

        $startDate = date('Y-m-d', strtotime("-7 days")); //- 5 días
        //$startDate = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime("-1 days"));
        $today = date('Y-m-d');
        $todayDateTime = date('Y-m-d H:i:s');
        $tipoTransaccion = 'RA3';


        $rsUnidadesCentros = getGroupUnidadesByCentros(getHistoricoUnidades341($startDate, $today));
        //echo json_encode($rsUnidadesCentros);
        generarReporte341($rsUnidadesCentros);
        insertarTransacciones($rsUnidadesCentros, $tipoTransaccion, $todayDateTime);
        generarLog341($rsUnidadesCentros, $todayDateTime);
    }
    function getHistoricoUnidades341($startDate, $endDate) {

        $sqlHistoricoUnidades341 = "SELECT DISTINCT hu.centroDistribucion,hu.vin,hu.fechaEvento,hu.claveMovimiento,hu.distribuidor, ".
                                "(SELECT ra.descripcionRampa FROM caRampasTbl ra WHERE ra.centroDistribucion = hu.localizacionUnidad) as rampa, ".
                                "(SELECT ra.codigoRampa FROM caRampasTbl ra WHERE ra.centroDistribucion = hu.localizacionUnidad) as codigoRampa, ".
                                "(SELECT ra.codigoLocalizacion FROM caRampasTbl ra WHERE ra.centroDistribucion = hu.localizacionUnidad) as codigoLocalizacion ".
                                "FROM alhistoricounidadestbl hu, alunidadestbl au, casimbolosunidadestbl su ".
                                "WHERE hu.vin = au.vin ".
                                "AND au.simboloUnidad = su.simboloUnidad ".
                                "AND su.marca not in ('HY','KI') ".
                                "AND CAST(hu.fechaEvento as date) >= CAST('".$startDate."' AS DATE) ".
                                "AND hu.vin NOT IN(SELECT tu.vin FROM altransaccionunidadtbl tu WHERE tu.vin = hu.vin AND tu.tipoTransaccion IN('H10')) ".
                                "AND hu.vin NOT IN(SELECT tu.vin FROM altransaccionunidadtbl tu WHERE tu.vin = hu.vin AND tu.tipoTransaccion IN('RA3')AND tu.centroDistribucion = hu.centroDistribucion AND tu.fechaMovimiento = hu.fechaEvento)  ".
                                "AND hu.centroDistribucion  IN ('CDTOL','CDAGS','CDANG', 'CDSFE','CDSAL') ".
                                //"AND hu.centroDistribucion  IN ('CDSAL') ".
                                "AND substring(hu.distribuidor,1,1) !='H' ".
                                "AND hu.vin not in ('3C6JRAAGXLG169350',
'3C6JRAAGXLG183474',
'3C6JRAAGXLG186777',
'3C6JRAAGXLG186889',
'3C6JRAAGXLG190134',
'3C6JRAAGXLG197956',
'3C6JRAAGXLG197990',
'3C6JRAAG0LG179272',
'3C6JRAAG0LG165078',
'3C6JRAAG0LG169499',
'3C6JRAAG0LG183435',
'3C6JRAAG0LG183578',
'3C6JRAAG0LG186853',
'3C6JRAAG0LG190188',
'3C6JRAAG1LG167731',
'3C6JRAAG1LG167888',
'3C6JRAAG1LG179250',
'3C6JRAAG0LG186884',
'3C6JRAAG0LG190174',
'3C6JRAAG1LG165378',
'3C6JRAAG1LG190300',
'3C6JRAAG2LG165230',
'3C6JRAAG2LG169424',
'3C6JRAAG2LG186899',
'3C6JRAAG2LG197952',
'3C6JRAAG3LG164863',
'3C6JRAAG3LG169254',
'3C6JRAAG3LG190167',
'3C6JRAAG3LG190217',
'3C6JRAAG4LG183549',
'3C6JRAAG4LG186838',
'3C6JRAAG4LG186936',
'3C6JRAAG5LG165397',
'3C6JRAAG5LG167764',
'3C6JRAAG5LG179249',
'3C6JRAAG5LG183477',
'3C6JRAAG5LG186878',
'3C6JRAAG5LG190137',
'3C6JRAAG5LG190154',
'3C6JRAAG5LG190204',
'3C6JRAAG6LG165151',
'3C6JRAAG6LG169202',
'3C6JRAAG6LG183553',
'3C6JRAAG6LG186789',
'3C6JRAAG6LG190163',
'3C6JRAAG6LG190308',
'3C6JRAAG7LG165109',
'3C6JRAAG7LG165207',
'3C6JRAAG7LG167801',
'3C6JRAAG7LG169290',
'3C6JRAAG7LG175929',
'3C6JRAAG7LG190222',
'3C6JRAAG8LG165264',
'3C6JRAAG8LG167659',
'3C6JRAAG8LG169427',
'3C6JRAAG8LG169458',
'3C6JRAAG8LG179259',
'3C6JRAAG8LG197972',
'3C6JRAAG9LG164897',
'3C6JRAAG9LG165130',
'3C6JRAAG9LG186785',
'3C6JRAAG9LG186902',
'3C6JRAAG9LG197950',
'3C6JRAAG9LG197964',
'3C6JRAAGXLG164746',
'3C6JRAAGXLG165301',
'3C6JRAAGXLG165427',
'3C6JRAAGXLG169378',
'3C6JRAAGXLG175911',
'3C6JRAAGXLG186780',
'3C6JRAAGXLG186813',
'3C6JRAAGXLG190165',
'3C6JRAAGXLG190215',
'3C6JRAAG0LG165338',
'3C6JRAAG0LG190210',
'3C6JRAAG1LG167633',
'3C6JRAAG1LG167907',
'3C6JRAAG1LG190149',
'3C6JRAAG1LG197974',
'3C6JRAAG2LG165227',
'3C6JRAAG2LG183436',
'3C6JRAAG2LG186773',
'3C6JRAAG2LG186904',
'3C6JRAAG2LG190161',
'3C6JRAAG3LG165091',
'3C6JRAAG3LG169433',
'3C6JRAAG3LG175944',
'3C6JRAAG3LG183431',
'3C6JRAAG3LG186832',
'3C6JRAAG4LG164810',
'3C6JRAAG4LG165066',
'3C6JRAAG4LG179260',
'3C6JRAAG4LG183406',
'3C6JRAAG4LG190212',
'3C6JRAAG5LG169384',
'3C6JRAAG0LG169440',
'3C6JRAAG0LG175965',
'3C6JRAAG4LG186791',
'3C6JRAAG4LG186841',
'3C6JRAAG4LG190307',
'3C6JRAAG4LG197970',
'3C6JRAAG5LG164752',
'3C6JRAAG5LG165125',
'3C6JRAAG5LG169191',
'3C6JRAAG5LG169322',
'3C6JRAAG5LG169417',
'3C6JRAAG5LG179235',
'3C6JRAAG5LG183527',
'3C6JRAAG5LG186802',
'3C6JRAAG5LG186850',
'3C6JRAAG5LG186931',
'3C6JRAAG5LG190140',
'3C6JRAAG3LG183400',
'3C6JRAAG3LG186751',
'3C6JRAAG3LG186880',
'3C6JRAAG3LG186927',
'3C6JRAAG3LG190136',
'3C6JRAAG3LG190153',
'3C6JRAAG3LG190184',
'3C6JRAAG3LG190203',
'3C6JRAAG3LG190234',
'3C6JRAAG3LG190301',
'3C6JRAAG4LG165309',
'3C6JRAAG4LG186757',
'3C6JRAAG7LG164851',
'3C6JRAAG7LG164901',
'3C6JRAAG7LG165143',
'3C6JRAAG7LG165191',
'3C6JRAAG7LG165420',
'3C6JRAAG7LG167720',
'3C6JRAAG7LG186770',
'3C6JRAAG7LG186834',
'3C6JRAAG7LG186882',
'3C6JRAAG7LG186929',
'3C6JRAAG7LG190169',
'3C6JRAAG7LG190186',
'3C6JRAAG7LG190205',
'3C6JRAAG7LG197946',
'3C6JRAAG8LG164812',
'3C6JRAAG8LG167693',
'3C6JRAAG8LG197986',
'3C6JRAAG9LG169498',
'3C6JRAAG9LG186849',
'3C6JRAAG0LG164884',
'3C6JRAAG0LG165114',
'3C6JRAAG4LG197886',
'3C6JRAAG5LG165304',
'3C6JRAAG5LG179266',
'3C6JRAAG6LG186744',
'3C6JRAAG6LG190146',
'3C6JRAAG7LG186784',
'3C6JRAAG8LG165068',
'3C6JRAAG8LG165278',
'3C6JRAAG8LG183473',
'3C6JRAAG8LG186809',
'3C6JRAAG4LG190193',
'3C6JRAAG5LG179252',
'3C6JRAAG5LG183463',
'3C6JRAAG5LG186797',
'3C6JRAAG5LG197959',
'3C6JRAAG6LG164792',
'3C6JRAAG6LG175954',
'3C6JRAAG6LG190177',
'3C6JRAAG7LG167636',
'3C6JRAAG7LG169368',
'3C6JRAAG7LG186865',
'3C6JRAAG7LG190141',
'3C6JRAAG7LG197977',
'3C6JRAAG8LG190178',
'3C6JRAAG8LG190214',
'3C6JRAAG9LG164978',
'3C6JRAAG9LG169324',
'3C6JRAAG9LG169338',
'3C6JRAAGXLG164925',
'3C6JRAAGXLG190151',
'3C6JRAAGXLG190201',
'3C6JRAAG1LG186814',
'3C6JRAAG1LG186862',
'3C6JRAAG1LG186926',
'3C6JRAAG1LG190135',
'3C6JRAAG1LG190183',
'3C6JRAAG1LG197957',
'3C6JRAAG2LG164885',
'3C6JRAAG2LG164949',
'3C6JRAAG2LG165213',
'3C6JRAAG2LG186756',
'3C6JRAAG2LG186806',
'3C6JRAAG2LG186854',
'3C6JRAAG2LG186868',
'3C6JRAAG2LG186921',
'3C6JRAAG2LG190175',
'3C6JRAAG2LG190189',
'3C6JRAAG2LG190225',
'3C6JRAAG3LG164782',
'3C6JRAAG3LG165253',
'3C6JRAAG3LG169366',
'3C6JRAAG3LG175961',
'3C6JRAAG3LG179251',
'3C6JRAAG7LG190172',
'3C6JRAAG7LG190219',
'3C6JRAAG8LG169508',
'3C6JRAAG8LG186888',
'3C6JRAAG8LG190133',
'3C6JRAAG8LG190164',
'3C6JRAAG8LG190200',
'3C6JRAAG9LG164964',
'3C6JRAAG9LG165208',
'3C6JRAAG9LG186799',
'3C6JRAAG9LG186821',
'3C6JRAAG9LG190156',
'3C6JRAAG5LG186864',
'3C6JRAAG5LG190168',
'3C6JRAAG5LG197976',
'3C6JRAAG5LG197993',
'3C6JRAAG6LG197923',
'3C6JRAAG7LG169273',
'3C6JRAAG7LG186848',
'3C6JRAAG0LG167770',
'3C6JRAAG0LG169261',
'3C6JRAAG0LG183516',
'3C6JRAAG5LG190185',
'3C6JRAAG5LG190221',
'3C6JRAAG5LG190235',
'3C6JRAAG6LG164789',
'3C6JRAAG6LG165053',
'3C6JRAAG6LG165280',
'3C6JRAAG6LG165361',
'3C6JRAAG6LG167806',
'3C6JRAAG6LG169233',
'3C6JRAAG6LG169443',
'3C6JRAAG6LG169488',
'3C6JRAAG6LG186758',
'3C6JRAAG6LG186761',
'3C6JRAAG6LG186811',
'3C6JRAAG6LG186825',
'3C6JRAAG6LG186856',
'3C6JRAAG6LG186923',
'3C6JRAAG6LG186937',
'3C6JRAAGXLG190229',
'3C6JRAAG0LG165369',
'3C6JRAAG0LG183483',
'3C6JRAAG0LG186898',
'3C6JRAAG0LG197951',
'3C6JRAAG2LG165129',
'3C6JRAAG2LG167768',
'3C6JRAAG0LG179269',
'3C6JRAAG2LG183548',
'3C6JRAAG5LG165352',
'3C6JRAAG6LG183486',
'3C6JRAAG9LG186835',
'3C6JRAAGXLG165346',
'3C6JRAAGXLG169204',
'3C6JRAAG0LG167817',
'3C6JRAAG0LG186870',
'3C6JRAAG1LG183511',
'3C6JRAAG1LG186876',
'3C6JRAAG1LG190202',
'3C6JRAAG2LG169360',
'3C6JRAAG2LG186885',
'3C6JRAAG2LG190306',
'3C6JRAAG3LG167634',
'3C6JRAAG3LG183512',
'3C6JRAAG4LG186869',
'3C6JRAAG4LG186886',
'3C6JRAAG4LG190159',
'3C6JRAAG0LG186786',
'3C6JRAAG0LG186867',
'3C6JRAAG0LG190207',
'3C6JRAAG0LG190224',
'3C6JRAAG1LG164909',
'3C6JRAAG1LG165008',
'3C6JRAAG1LG165235',
'3C6JRAAG1LG165431',
'3C6JRAAG1LG167860',
'3C6JRAAG1LG169429',
'3C6JRAAG1LG183413',
'3C6JRAAGXLG164892',
'3C6JRAAGXLG165315',
'3C6JRAAGXLG167632',
'3C6JRAAGXLG169333',
'3C6JRAAGXLG179277',
'3C6JRAAGXLG186763',
'3C6JRAAGXLG186861',
'3C6JRAAGXLG186892',
'3C6JRAAG1LG183475',
'3C6JRAAG2LG165132',
'3C6JRAAG2LG165373',
'3C6JRAAG2LG169441',
'3C6JRAAG2LG169505',
'3C6JRAAG2LG190208',
'3C6JRAAG3LG164748',
'3C6JRAAG3LG165284',
'3C6JRAAG3LG167911',
'3C6JRAAG3LG186796',
'3C6JRAAG3LG190220',
'3C6JRAAG3LG197894',
'3C6JRAAG4LG164774',
'3C6JRAAG4LG164872',
'3C6JRAAG4LG167903',
'3C6JRAAG4LG186872',
'3C6JRAAG4LG186919',
'3C6JRAAG4LG190176',
'3C6JRAAG8LG169234',
'3C6JRAAG8LG169329',
'3C6JRAAG8LG169430',
'3C6JRAAG8LG183408',
'3C6JRAAG8LG186762',
'3C6JRAAG8LG186910',
'3C6JRAAG8LG190150',
'3C6JRAAG8LG197938',
'3C6JRAAG8LG197955',
'3C6JRAAG9LG164866',
'3C6JRAAG9LG186768',
'3C6JRAAG9LG186804',
'3C6JRAAG9LG190142',
'3C6JRAAG9LG190173',
'3C6JRAAG9LG190190',
'3C6JRAAG9LG190206',
'3C6JRAAGXLG167744',
'3C6JRAAGXLG169249',
'3C6JRAAG0LG164836',
'3C6JRAAG0LG164979',
'3C6JRAAG0LG165162',
'3C6JRAAG0LG167686',
'3C6JRAAG3LG165043',
'3C6JRAAG3LG165057',
'3C6JRAAG3LG165267',
'3C6JRAAG3LG169352',
'3C6JRAAG3LG186765',
'3C6JRAAG3LG186829',
'3C6JRAAG3LG186846',
'3C6JRAAG3LG190198',
'3C6JRAAG3LG197958',
'3C6JRAAG3LG197961',
'3C6JRAAG4LG164855',
'3C6JRAAG4LG164886',
'3C6JRAAG4LG164953',
'3C6JRAAG4LG165049',
'3C6JRAAG4LG165391',
'3C6JRAAG4LG175967',
'3C6JRAAG4LG186788',
'3C6JRAAG4LG186807',
'3C6JRAAG4LG186810',
'3C6JRAAG4LG190145',
'3C6JRAAG4LG190226',
'3C6JRAAG5LG164847',
'3C6JRAAG5LG165402',
'3C6JRAAG5LG183446',
'3C6JRAAG5LG186900',
'3C6JRAAG0LG164741',
'3C6JRAAG0LG165095',
'3C6JRAAG0LG167736',
'3C6JRAAG0LG169258',
'3C6JRAAG0LG190238',
'3C6JRAAG0LG197965',
'3C6JRAAG7LG190236',
'3C6JRAAG7LG197932',
'3C6JRAAG8LG165135',
'3C6JRAAG8LG165281',
'3C6JRAAG5LG186928',
'3C6JRAAG5LG190218',
'3C6JRAAG6LG167773',
'3C6JRAAG6LG169362',
'3C6JRAAG6LG169457',
'3C6JRAAG6LG190227',
'3C6JRAAG5LG165156',
'3C6JRAAG5LG165271',
'3C6JRAAG7LG186901',
'3C6JRAAG7LG186915',
'3C6JRAAG7LG186932',
'3C6JRAAG1LG186781',
'3C6JRAAG1LG186795',
'3C6JRAAG1LG186800',
'3C6JRAAG1LG186831',
'3C6JRAAG2LG165325',
'3C6JRAAG2LG167835',
'3C6JRAAG2LG169200',
'3C6JRAAG2LG169245',
'3C6JRAAG2LG183517',
'3C6JRAAG2LG186837',
'3C6JRAAG2LG186918',
'3C6JRAAG2LG190211',
'3C6JRAAG2LG190239',
'3C6JRAAG1LG164943',
'3C6JRAAG1LG164988',
'3C6JRAAG1LG165039',
'3C6JRAAG7LG164753',
'3C6JRAAG7LG164932',
'3C6JRAAG7LG164977',
'3C6JRAAG7LG169337',
'3C6JRAAG7LG175946',
'3C6JRAAG7LG183402',
'3C6JRAAG7LG183559',
'3C6JRAAG1LG165168',
'3C6JRAAG1LG165347',
'3C6JRAAG1LG169379',
'3C6JRAAG1LG169415',
'3C6JRAAG1LG175909',
'3C6JRAAG1LG175960',
'3C6JRAAG1LG186750',
'3C6JRAAG8LG165331',
'3C6JRAAG8LG169413',
'3C6JRAAG8LG183540',
'3C6JRAAG8LG183571',
'3C6JRAAG8LG186759',
'3C6JRAAG8LG190147',
'3C6JRAAG8LG190231',
'3C6JRAAG9LG164950',
'3C6JRAAG9LG165029',
'3C6JRAAG9LG165161',
'3C6JRAAG9LG175933',
'3C6JRAAG9LG186771',
'3C6JRAAG9LG186852',
'3C6JRAAG9LG186883',
'3C6JRAAG9LG190187',
'3C6JRAAG9LG190223',
'3C6JRAAG9LG197981',
'3C6JRAAGXLG164827',
'3C6JRAAGXLG164844',
'3C6JRAAGXLG167761',
'3C6JRAAGXLG169252',
'3C6JRAAGXLG186746',
'3C6JRAAGXLG186844',
'3C6JRAAGXLG186908',
'3C6JRAAGXLG186939') ".
                                "AND hu.claveMovimiento IN ('AM','MM','FS','AS','ER','SO','SI','SV','ST','SC','EF','SJ','EC','SH','SF','SN','SP','FE') ".
                                "AND hu.fechaEvento =(select max(hu1.fechaEvento) from alHistoricoUnidadesTbl hu1 where hu1.vin=hu.vin AND hu1.claveMovimiento in ('AM','MM','FS','AS','ER','SO','SI','SV','ST','SC','EF','SJ','EC','SH','SF','SN','SP','FE')) ".
                                "AND hu.vin NOT IN(SELECT a.vin FROM alhistoricounidadestbl a WHERE a.idHistorico=(hu.idHistorico-1) AND a.claveMovimiento in
                                ('US','SI','EO')) ".
                                "AND hu.idTarifa NOT IN (SELECT ta.idTarifa FROM caTarifasTbl ta WHERE hu.idTarifa = ta.idTarifa AND ta.tarifa = '13') ".
                                "GROUP BY hu.vin;";

        $rsUnidadesCentros = fn_ejecuta_query($sqlHistoricoUnidades341);
        return($rsUnidadesCentros);
    }


    function getGroupUnidadesByCentros($rsUnidades341) {

        if(!isset($rsUnidades341['root']))
            return;

        $rsCentrosDistribucion = array();
        foreach ($rsUnidades341['root'] as $unidad) {
            $unidadRecord = $unidad;

            if(preg_match('/^CD+/', $unidad['localizacionUnidad']))
                $patio = substr($unidad['localizacionUnidad'], 2);
            else
                $patio = $unidad['localizacionUnidad'];

            $unidadRecord = array('patio' => $patio, 'vin' => $unidad['vin'], 'centroDistribucion' => $unidad['centroDistribucion'],
                                  'claveMovimiento' => $unidad['claveMovimiento'], 'codigoLocalizacion' => $unidad['codigoLocalizacion'],
                                  'prodstatus' => $unidad['prodstatus'], 'vupdate' => $unidad['vupdate'],
                                  'vuptime' => $unidad['vuptime'], 'fechaEvento' => $unidad['fechaEvento'],
                                  'localizacionUnidad' => $unidad['localizacionUnidad']);

            if (!isset($rsCentrosDistribucion[$unidad['centroDistribucion']])) {
                $rsCentrosDistribucion[$unidad['centroDistribucion']] = array();
                $rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']] = array();
                $rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
            } else {
                if (!isset($rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']])) {
                    $rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']] = array();
                    $rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
                } else{
                    $rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
                }
            }
        }

        return $rsCentrosDistribucion;
    }


    function generarReporte341($rsCentrosDistribucion) {

        if (count($rsCentrosDistribucion) < 1)
            return;

        $sqlGetFolio = "SELECT LPAD(FOLIO + 1,5,0) AS numFolio, folio + 1 as actualFolio ".
                      "FROM trfoliostbl ".
                      "WHERE centroDistribucion = 'CDTOL' ".
                      "AND tipoDocumento = '3R'";

      $rsGetFolio = fn_ejecuta_query($sqlGetFolio);

      $updFolio = "UPDATE trfoliostbl ".
                  "SET folio = '".$rsGetFolio['root'][0]['actualFolio']."' ".
                  "WHERE centroDistribucion = 'CDTOL' ".
                  "AND tipoDocumento = '3R';";

      $rsUpdFolio = fn_ejecuta_query($updFolio);

        //$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA3R.txt";
        $fileDir = "E:/carbook/i341_CB/RA3R".$rsGetFolio['root'][0]['numFolio'].".txt";
        $flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
        $recordsPositionValue = array();
        foreach ($rsCentrosDistribucion as $nombreCentro => $centroDistribucion) {
                if($nombreCentro == 'CDSAL'){
                    $rampaArr = 'ADIMSRAMPMX806';
                    $ptoUnd = 'X6';
                }elseif($nombreCentro == 'CDTOL'){
                    $rampaArr = 'ADIMSRAMPMX807';
                    $ptoUnd = 'X7';
                }elseif($nombreCentro =='CDANG'){
                    $rampaArr = 'ADIMSRAMPMX808';
                    $ptoUnd = 'X8';
                }elseif($nombreCentro == 'CDAGS'){
                    $rampaArr = 'ADIMSRAMPMX011';
                    $ptoUnd = 'X4';
                }elseif($nombreCentro == 'CDMAZ'){
                    $rampaArr = 'ADIMSRAMPMX809';
                    $ptoUnd = 'X9';
                }elseif($nombreCentro == 'CDSFE'){
                    $rampaArr = 'ADIMSRAMPMX988';
                    $ptoUnd = 'XO';
                }elseif($nombreCentro == 'CDCUA'){
                    $rampaArr = 'ADIMSRAMPMX888';
                    $ptoUnd = 'XU';
                }elseif($nombreCentro == 'CDLZC'){
                    $rampaArr = 'ADIMSRAMPMX816';
                    $ptoUnd = 'PT';
                }
            foreach ($centroDistribucion as $nombrePatio => $patios) {

            //A) ENCABEZADO
            //Posiciones - Valores
            $record = array( 0 =>  'ISA*03*RA3R      *00*          *ZZ*',
                            35 => $rampaArr." ",
                            50 => "*ZZ*ADIMS          *",
                            70 => date_format(date_create($today), 'y'),
                            72 => date_format(date_create($today), 'm'),
                            74 => date_format(date_create($today), 'd'),
                            76 => '*',
                            77 => date_format(date_create(date('H:i:s')), 'H'),
                            79 => date_format(date_create(date('H:i:s')), 'i'),
                            81 => '*U*00300*',
                            90 => sprintf('%09d', count($patios)),
                            99 => '*0*P*'
                           );
                            //echo $nombrePatio;


            $recordsPositionValue[] = $record;
            $headerTxt = getTxt($record, array_keys($record));
            fwrite($flReporte660, $headerTxt.out('n', 1));
                //B) DETALLE UNIDADES
                foreach ($patios as $unidad) {

                   /*$sqlGetFecha = "SELECT h.vin, h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%m%d%y')AS fechaFormato,h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%H%i')AS horaFormato ".
                                            "FROM alultimodetalletbl h ".
                                            "WHERE h.vin = '".$unidad['vin']."'; ";

                    $rsGetFecha = fn_ejecuta_query($sqlGetFecha);*/

                    $sqlGetFecha = "SELECT h.vin, h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%m%d%y')AS fechaFormato,h.fechaEvento,DATE_FORMAT(ADDDATE(h.fechaEvento, INTERVAL 2 minute),'%H%i')AS horaFormato ".
                                            "FROM alhistoricounidadestbl h ".
                                            "WHERE h.fechaEvento =(SELECT MAX(hu1.fechaEvento) from alHistoricoUnidadesTbl hu1 where hu1.vin=h.vin AND hu1.claveMovimiento in ('AM','MM','FS','AS','ER','SO','SI','SV','ST','SC','EF','SJ','EC','SH','SF','SN','SP','FE')) ".
                                            "AND h.vin = '".$unidad['vin']."'; ";

                    $rsGetFecha = fn_ejecuta_query($sqlGetFecha);

                    //Posiciones - Valores
                    $record = array( 0 =>  '3R',
                                     2 => $ptoUnd,
                                     //4 => date('mdy'),
                                     4 => $rsGetFecha['root'][0]['fechaFormato'],
                                    10 => $unidad['vin'],
                                    27 => 'AA111',
                                    32 => out('s', 17),
                                    //49 => date_format(date_create($todayDateTime), 'h'),
                                    //51 => date_format(date_create($todayDateTime), 'i'),
                                    49=> $rsGetFecha['root'][0]['horaFormato'],
                                    //53 => substr(date_format(date_create($rsGetFecha['root'][0]['horaFormato']), 'A'), 0, 1),
                                    54 => out('s', 25),
                                    79 => lws_marca($unidad['vin'][0], $unidad['codigoLocalizacion']),
                                   );

                    $detalleTxt = getTxt($record, array_keys($record));
                    fwrite($flReporte660, $detalleTxt.out('n', 1));
                }
                //C) TRAILER
                //Posiciones - Valores
                $record = array(0 => 'IEA*01*', 7 => sprintf('%09d', count($patios)));
                $recordsPositionValue[] = $record;
                $trailerTxt = getTxt($record, array_keys($record));
                fwrite($flReporte660, $trailerTxt.out('n', 1));
            }
        }
        fclose($flReporte660);
    }
    function insertarTransacciones($rsCentrosDistribucion, $tipoTransaccion, $dateTime) {

        if (!isset($rsCentrosDistribucion) || count($rsCentrosDistribucion) < 1)
            return;

        /*$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl".
                                "(tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, ".
                                "claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) VALUES";*/

        foreach ($rsCentrosDistribucion as $nombreCentro => $rsCentroDistribucion) {
            foreach ($rsCentroDistribucion as $nombrePatio => $rsPatio) {
                $i = 0;
        
                foreach ($rsPatio as $unidad) {
                    if($i++ > 0)
                        // $sqlInsertTransaccion .= ', ';

                    $vupdate = DateTime::createFromFormat('d/m/Y', $unidad['vupdate']);
                    $vupdate = date_format($vupdate, 'Y-m-d');

                    $vuptime = $unidad['vuptime'];
                    if(strlen($unidad['vuptime']) > 5)
                        $vuptime = substr($unidad['vuptime'], 0, 5);

                    $sqlGetFolio = "SELECT LPAD(FOLIO + 1,5,0) AS numFolio, folio  as actualFolio ".
                      "FROM trfoliostbl ".
                      "WHERE centroDistribucion = 'CDTOL' ".
                      "AND tipoDocumento = '3R'";

                    $rsGetFolio = fn_ejecuta_query($sqlGetFolio);


                    $sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl ".
                                                "(tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, ".
                                                "claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) VALUES ".
                                                "('$tipoTransaccion', '$unidad[centroDistribucion]', '$unidad[vin]', '$dateTime', ".
                                                "'$unidad[claveMovimiento]', '$unidad[fechaEvento]', 'RA3R".$rsGetFolio['root'][0]['numFolio']."', ".
                                                "'$vupdate', '$vuptime');";

                    fn_ejecuta_query($sqlInsertTransaccion);

                }
            }
        }


        //echo json_encode($sqlInsertTransaccion);

    }
    function generarLog341($rsCentrosDistribucion, $dateTime) {

        $unidadesR3Suma = 0;
        if (isset($rsCentrosDistribucion) && count($rsCentrosDistribucion) > 1) {
            foreach ($rsCentrosDistribucion as $nombreCentro => $rsCentroDistribucion) {
                foreach ($rsCentroDistribucion as $nombrePatio => $rsPatio) {
                    $unidadesR3Suma += count($rsPatio);
                }
            }
        }

        $record1 = array( 0 => 'TOTAL DE UNDS EN R3',
                         20 => date_format(date_create($dateTime), 'd/m/Y'),
                         30 => '-',
                         31 => date_format(date_create($dateTime), 'H:i:s'),
                         40 => ':',
                        );

        $record2 = array(0 =>  $unidadesR3Suma);

        //$fileDir = $_SERVER['DOCUMENT_ROOT'].'/TOT3R.log';

        /*$fileDir = "E:/carbook/i341_CB/TOT3R.log";
        $log341 = fopen($fileDir, 'a+') or die("$dateTime .- No se pudo generar archivo de LOG");
        fwrite($log341, getTxt($record1, array_keys($record1)).out('n', 1));
        fwrite($log341, getTxt($record2, array_keys($record2)).out('n', 1));
        fclose($log341);*/
    }

?>    