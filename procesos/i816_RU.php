<?php
	 // INTERFAZ 816_RU
	 // Archivos a generar
	 // este proceso genera movimientos de los eventos de asignacion (AM) para unidades marca Hyundai y Kia.


	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");

	date_default_timezone_set('America/Mexico_City');
	$hoy = getdate();
	$fecha = date('Y-m-d', $hoy[0]);

    $fechaAnt = strtotime ( '-15 day' , strtotime (date("Y-m-d")) ) ;
	$fechaAnt = date ( 'Y-m-d' , $fechaAnt );


  	global $dirPath;
	global $inicioFile;
	global $fileName;

	date_default_timezone_set('America/Mexico_City');

	//$dirPath = "C:carbook/i816_RU/";
	$$dirPath = "E:\\carbook\\i816_RU\\";
	$filePath = $dirPath.$inicioFile;
	$ejecutaProceso = true;

	//ejecutaTLP();
	ejecuta816_RU();


  	/*while(true){
		if(date("i")%1 == 0){
			if($ejecutaProceso == "S"){
				echo "Inicio: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
				ejecutaTLP();
					if(file_exists($filePath)){
						$fichero = "C:carbook/i816_RU/".$inicioFile.".txt";
						$nuevo_fichero = "C:carbook/i816_RU/".str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).".txt";
						if (!copy($fichero, $nuevo_fichero)) {
							echo "Error al copiar $fichero...\n";
						}
					} else {
					echo "El archivo no existe\r\n";
				}
				echo "Termino: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
				$ejecutaProceso = "N";
			}
		}
		else{
			$ejecutaProceso = "S";
		}
	}	*/
	//ejecuta816_RU();

   	function ejecuta816_RU(){

   		echo "inicio i816_RU";

   		//date_default_timezone_set('America/Mexico_City');
		//$hoy = getdate();
		//$fecha = date('Y-m-d', $hoy[0]);


		$fechaAnt = strtotime ( '-300 day' , strtotime (date("Y-m-d")) ) ;
	    $fechaAnt = date ( 'Y-m-d' , $fechaAnt );

	    $fecha = date('Y-m-j');
		$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m-j' , $nuevafecha );

	    //echo $nuevafecha;

   		$sqlTLR = "SELECT * FROM alunidadestbl al, alhistoricounidadestbl ud, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = ud.vin ".
					"AND ud.vin = dy.vin ".
					"AND ud.claveMovimiento ='PR' ".
					//"AND ud.fechaEvento BETWEEN '2016-08-10' AND '2016-08-20' ".
					"AND ud.fechaEvento BETWEEN '".$fechaAnt."' AND '".$nuevafecha."' ".
					"AND dy.cveStatus in('1K','1H') ";
					/*"AND dy.cveDisFac = ud.distribuidor ".
					"AND ud.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='ALR') ".
					"AND ud.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='TLR')";*/
		$rsTLR = fn_ejecuta_query($sqlTLR);
			//echo json_encode($sqlTLR);

		for ($i=0; $i <sizeof($rsTLR['root']) ; $i++) {
			echo json_encode($rsTLR['root'][$i]['cveStatus']);
			if ($rsTLR['root'][$i]['cveStatus'] == '1K') {
				$arrKia[] = $rsTLR['root'][$i];
			}
		/////////////////////////////////////////////////////////
			else if ($rsTLR['root'][$i]['cveStatus'] == '1H') {
				$arrHyu[] = $rsTLR['root'][$i];
			}
			else{
				echo "string";
			}
		}
		//echo json_encode($arrKia);
		if (count($arrKia) != 0) {
			generaEXTKIA($arrKia);
			//generaALRKIA($arrKia);
		}
		else{
			//echo "no hay";
		}
		if (count($arrHyu) != 0) {
			//generaTLRHYU($arrHyu);
			//generaALRHYU($arrHyu);
			generaEXTHYU($arrHyu);
		}
		//echo "no hay";

	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function generaEXTKIA($arrKia){
		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');

		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816_RU\\";

		$inicioFile = "KMM_EXT_".$fecha.$hora.".txt";
		$genArchivo = $directorio.$inicioFile;
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"EXTH"." "."TRA"."  "."GMX"."  "."EXT".$fecha.$hora.PHP_EOL);

		//detalle



		for ($i=0; $i <sizeof($arrKia) ; $i++) {

			if ($arrKia[$i]['centroDistribucion'] == 'CDTOL') {
				$origin = 'FT15';
			}
			if ($arrKia[$i]['centroDistribucion'] == 'CDSFE') {
				$origin = 'FT18';
			}



			fwrite($archivo,"EXT  RU".$arrKia[$i]['vin'].substr($arrKia[$i]['fechaEvento'],0,4).substr($arrKia[$i]['fechaEvento'],5,2).
				substr($arrKia[$i]['fechaEvento'],8,2).substr($arrKia[$i]['fechaEvento'],11,2).substr($arrKia[$i]['fechaEvento'],14,2).
				substr($arrKia[$i]['fechaEvento'],17,2).$origin."                "."TRA  T".PHP_EOL);

				$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='3K' WHERE vin='".$arrKia[$i]['vin']."'"." AND cveStatus ='1K'" ;
				$rstUpt=fn_ejecuta_query($uptdy);

			/*$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('EXT', '".
										$arrKia[$i]['centroDistribucion']."', '".
										$arrKia[$i]['vin']."', '".
										$arrKia[$i]['fechaEvento']."', '".
										$arrKia[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
			echo json_encode($insTransaccion);*/

		}
		//fin de archivo
		$long=(sizeof($arrKia)+2);
		fwrite($archivo,"EXTT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);
	}

/////////////////////////////////////////////////////EXT HYUNDAI////////////////////////
function generaEXTHYU($arrHyu){
	$fecha = date('Ymd');
		$hora = date("His");
		$today =  date('Y-m-d H:i:s');

	//$directorio ="C:carbook/i816/";
	$directorio = "E:\\carbook\\i816_RU\\";

	$inicioFile = "HMM_EXT_".$fecha.$hora.".txt";
	$genArchivo = $directorio.$inicioFile;
	$archivo = fopen($directorio.$inicioFile,"w");

	//encabezado
	fwrite($archivo,"EXTH"." "."TRA"."  "."GMX"."  "."EXT".$fecha.$hora.PHP_EOL);

	//detalle



	for ($i=0; $i <sizeof($arrHyu) ; $i++) {

		if ($arrHyu[$i]['centroDistribucion'] == 'CDTOL') {
			$origin = '1150';
		}
		if ($arrHyu[$i]['centroDistribucion'] == 'CDSFE') {
			$origin = '1140';
		}



		fwrite($archivo,"EXT  RU".$arrHyu[$i]['vin'].substr($arrHyu[$i]['fechaEvento'],0,4).substr($arrHyu[$i]['fechaEvento'],5,2).
			substr($arrHyu[$i]['fechaEvento'],8,2).substr($arrHyu[$i]['fechaEvento'],11,2).substr($arrHyu[$i]['fechaEvento'],14,2).
			substr($arrHyu[$i]['fechaEvento'],17,2).$origin."                "."TRA  T".PHP_EOL);


			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='3H' WHERE vin='".$arrHyu[$i]['vin']."'"." AND cveStatus ='1H'" ;
			$rstUpt=fn_ejecuta_query($uptdy);

		/*$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
							"VALUES ('EXT', '".
									$arrHyu[$i]['centroDistribucion']."', '".
									$arrHyu[$i]['vin']."', '".
									$arrHyu[$i]['fechaEvento']."', '".
									$arrHyu[$i]['claveMovimiento']."', '".
									$today."', 'I', '".
									substr($today,0,10)."', '".
									substr($today,11,8)."')";
		fn_ejecuta_query($insTransaccion);
		echo json_encode($insTransaccion);*/

	}
	//fin de archivo
	$long=(sizeof($arrHyu)+2);
	fwrite($archivo,"EXTT ".sprintf('%06d',($long)).PHP_EOL);
	fclose($archivo);
}

	//////////////////////////////////////////////////////ALT///TLR/////////////////////
	function generaALRKIA($arrKia){
		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');

		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816_RU\\";
		$inicioFile = "KMM_ALB_".$fecha.$hora.".txt";
		$genArchivo = $directorio.$inicioFile;
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"ALBH"." "."TRA"."  "."GMX"."  "."ALB".$fecha.$hora.PHP_EOL);

		//detalle

		$sqlFolioStr = "SELECT * FROM trfoliostbl WHERE tipoDocumento='TH' AND compania='"./*$KIA[$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrKia[0]['centroDistribucion']."'";
		$FolioRst = fn_ejecuta_query($sqlFolioStr);

		$folio = $FolioRst['root'][0]['folio'] + 1;

		$sqlActualizaFolioStr="UPDATE trfoliostbl SET folio =". $folio." where tipoDocumento='TH' and compania='"./*$rsTLR['root'][$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrKia[0]['centroDistribucion']."'";
		$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

		//echo json_encode($FolioRst['root'][0]['folio']);

		for ($i=0; $i <sizeof($arrKia) ; $i++) {

			$sqlCveDisFat ="SELECT * FROM capuertoshktbl ".
							"WHERE origen ='".$arrKia[$i]['cveDisFac']."'";
			$rsCve = fn_ejecuta_query($sqlCveDisFat);
			//echo json_encode($rsCve);

			if ($rsCve['records'] != 0) {
				$cveDis ='C';
			}
			else{
				$cveDis ='D';
			}

			$sqlTractor ="SELECT  ca.tractor ".
							"FROM trunidadesdetallestalonestbl ud , trtalonesviajestbl tl, trviajestractorestbl vi, catractorestbl ca ".
							"WHERE ud.vin ='".$arrKia[$i]['vin']."' ".
							"AND ud.estatus =1 ".
							"AND ud.idTalon = tl.idTalon ".
							"AND tl.idViajeTractor = vi.idViajeTractor ".
							"AND vi.idTractor = ca.idTractor ";
			$rsTractor =fn_ejecuta_query($sqlTractor);



			fwrite($archivo,"ALB"."  ".substr($arrKia[$i]['fechaEvento'],0,4).substr($arrKia[$i]['fechaEvento'],5,2).
				substr($arrKia[$i]['fechaEvento'],8,2).substr($arrKia[$i]['fechaEvento'],11,2).substr($arrKia[$i]['fechaEvento'],14,2).substr($arrKia[$i]['fechaEvento'],17,2));
			fwrite($archivo,$arrKia[$i]['vin']."P".sprintf("%-5s",$arrKia[$i]['cveLoc']).$cveDis.sprintf("%-5s",$arrKia[$i]['cveDisFac'])."D".sprintf("%-12s",$arrKia[$i]['chipNum'])."000000000000000");
			fwrite($archivo,substr($arrKia[$i]['fechaEvento'],0,4).substr($arrKia[$i]['fechaEvento'],5,2).substr($arrKia[$i]['fechaEvento'],8,2));
			fwrite($archivo,/*$arrKia[$i]['folio']*/sprintf('%06d',$FolioRst['root'][0]['folio']).sprintf('%06d',$rsTractor['root'][0]['tractor'])."09".sprintF('%02d',sizeof($arrKia)).$arrKia[$i]['distribuidor'].PHP_EOL);

			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='3K' WHERE vin='".$arrKia[$i]['vin']."'"." AND cveStatus ='1K'" ;
			$rstUpt=fn_ejecuta_query($uptdy);


			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('ALR', '".
										$arrKia[$i]['centroDistribucion']."', '".
										$arrKia[$i]['vin']."', '".
										$arrKia[$i]['fechaEvento']."', '".
										$arrKia[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('TLR', '".
										$arrKia[$i]['centroDistribucion']."', '".
										$arrKia[$i]['vin']."', '".
										$arrKia[$i]['fechaEvento']."', '".
										$arrKia[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);


			/*$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='EK' WHERE vin='".$arrKia[$i]['vin']."'"." AND cveStatus ='AK'" ;
			$rstUpt=fn_ejecuta_query($uptdy);*/


		}


		//fin de archivo
		$long=(sizeof($arrKia)+2);
		fwrite($archivo,"ALBT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);
		//subirArchivo($genArchivo,$inicioFile);
	}

	////////////////////////////////////////////ALR HYUNDAI//////////////////////////////////

	function generaALRHYU($arrHyu){
		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');

		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816_RU\\";
		$inicioFile = "HMM_ALB_".$fecha.$hora.".txt";
		$genArchivo = $directorio.$inicioFile;
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"ALBH"." "."TRA"."  "."GMX"."  "."ALB".$fecha.$hora.PHP_EOL);

		//detalle

		$sqlFolioStr = "SELECT * FROM trfoliostbl WHERE tipoDocumento='TH' AND compania='"./*$KIA[$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrHyu[0]['centroDistribucion']."'";
		$FolioRst = fn_ejecuta_query($sqlFolioStr);

		$folio = $FolioRst['root'][0]['folio'] + 1;

		$sqlActualizaFolioStr="UPDATE trfoliostbl SET folio =". $folio." where tipoDocumento='TH' and compania='"./*$rsTLR['root'][$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrHyu[0]['centroDistribucion']."'";
		$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

		//echo json_encode($FolioRst['root'][0]['folio']);

		for ($i=0; $i <sizeof($arrHyu) ; $i++) {

			$sqlCveDisFat ="SELECT * FROM capuertoshktbl ".
							"WHERE origen ='".$arrHyu[$i]['cveDisFac']."'";
			$rsCve = fn_ejecuta_query($sqlCveDisFat);
			//echo json_encode($rsCve);

			if ($rsCve['records'] != 0) {
				$cveDis ='C';
			}
			else{
				$cveDis ='D';
			}

			$sqlTractor ="SELECT  ca.tractor ".
							"FROM trunidadesdetallestalonestbl ud , trtalonesviajestbl tl, trviajestractorestbl vi, catractorestbl ca ".
							"WHERE ud.vin ='".$arrHyu[$i]['vin']."' ".
							"AND ud.estatus =1 ".
							"AND ud.idTalon = tl.idTalon ".
							"AND tl.idViajeTractor = vi.idViajeTractor ".
							"AND vi.idTractor = ca.idTractor ";
			$rsTractor =fn_ejecuta_query($sqlTractor);



			fwrite($archivo,"ALB"."  ".substr($arrHyu[$i]['fechaEvento'],0,4).substr($arrHyu[$i]['fechaEvento'],5,2).
				substr($arrHyu[$i]['fechaEvento'],8,2).substr($arrHyu[$i]['fechaEvento'],11,2).substr($arrHyu[$i]['fechaEvento'],14,2).substr($arrHyu[$i]['fechaEvento'],17,2));
			fwrite($archivo,$arrHyu[$i]['vin']."P".sprintf("%-5s",$arrHyu[$i]['cveLoc']).$cveDis.sprintf("%-5s",$arrHyu[$i]['cveDisFac'])."D".sprintf("%-12s",$arrHyu[$i]['chipNum'])."000000000000000");
			fwrite($archivo,substr($arrHyu[$i]['fechaEvento'],0,4).substr($arrHyu[$i]['fechaEvento'],5,2).substr($arrHyu[$i]['fechaEvento'],8,2));
			fwrite($archivo,/*$arrHyu[$i]['folio']*/sprintf('%06d',$FolioRst['root'][0]['folio']).sprintf('%06d',$rsTractor['root'][0]['tractor'])."09".sprintF('%02d',sizeof($arrHyu)).$arrHyu[$i]['distribuidor'].PHP_EOL);

			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='3H' WHERE vin='".$arrKia[$i]['vin']."'"." AND cveStatus ='1H'" ;
			$rstUpt=fn_ejecuta_query($uptdy);


			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('ALR', '".
										$arrHyu[$i]['centroDistribucion']."', '".
										$arrHyu[$i]['vin']."', '".
										$arrHyu[$i]['fechaEvento']."', '".
										$arrHyu[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('TLR', '".
										$arrHyu[$i]['centroDistribucion']."', '".
										$arrHyu[$i]['vin']."', '".
										$arrHyu[$i]['fechaEvento']."', '".
										$arrHyu[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
		}


		//fin de archivo
		$long=(sizeof($arrHyu)+2);
		fwrite($archivo,"ALBT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);
		//subirArchivo($genArchivo,$inicioFile);
	}

    function subirArchivo($genArchivo){
		# Definimos las variables
		$host="ftp.glovismx.com";
		$port=21;
		$user="GMX_TRA";
		$password="art180Xmg!";
		$ruta="/OUT";
		$local_file = "E:/carbook/i814/hyundai_kia/instrucciones/";//tobe uploaded
		$remote_file = "".$nombreBusqueda;
		$nuevo_fichero = "E:/carbook/i814/hyundai_kia/instrucciones/".$nombreBusqueda;

		# Realizamos la conexion con el servidor
		$conn_id=@ftp_connect($host);//,$port);
		if($conn_id){
			# Realizamos el login con nuestro usuario y contraseña
			if(@ftp_login($conn_id,$user,$password)){
				# Canviamos al directorio especificado
				if(@ftp_chdir($conn_id,$ruta)){
					# Subimos el fichero
					/*if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
						echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
					}*/
					$contents = ftp_nlist($conn_id, ".");
					//$arr_01 = var_dump($contents);
					$arr=$contents;

					for ($i=0; $i <sizeof($arr) ; $i++) {
						echo json_encode(substr($arr[$i],4,3));
						$encabezado = substr($arr[$i],4,3);
						if (substr($arr[$i],4,3) == 'ASN') {
							if(@ftp_get($conn_id, $local_file.$arr[$i], $arr[$i], FTP_BINARY)){
								//echo "si bajo el archivo: ".$arr[$i];
							}else{
								echo "no bajo el archivo";
							}
						}if (substr($arr[$i],4,3) == 'CAN') {
							if(@ftp_get($conn_id, $local_file.$arr[$i], $arr[$i], FTP_BINARY)){
								//echo "si bajo el archivo: ".$arr[$i];
							}else{
								echo "no bajo el archivo";
							}
						}if (substr($arr[$i],4,3) == 'DPR') {
							if(@ftp_get($conn_id, $local_file.$arr[$i], $arr[$i], FTP_BINARY)){
								//echo "si bajo el archivo: ".$arr[$i];
							}else{
								echo "no bajo el archivo";
							}
						}
					}

					i814();
				}else
					echo "No existe el directorio especificado";
			}else
				echo "El usuario o la contraseña son incorrectos";
			# Cerramos la conexion ftp
			ftp_close($conn_id);
		}else{
			echo "No ha sido posible conectar con el servidor";
		}
    }
?>
