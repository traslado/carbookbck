<?php
	require("../funciones/generales.php");
	require("../funciones/utilidades.php");
  require_once("../funciones/utilidadesProcesos.php");
 
	date_default_timezone_set('America/Mexico_City');

    //echo "Generacion de Archivo Interface 349: ";//.date("Y-m-d H:i", strtotime("now"))."\r\n";
    $sqlFechaInicialConsulta = "SELECT date_add(CURDATE(), INTERVAL - 5 DAY) as FechaConsulta";
    $rs = fn_ejecuta_query($sqlFechaInicialConsulta);
    //echo json_encode($rs);
    //$fechaInicio = $rs['root'][0]['FechaConsulta'];
    $sqlFechaFinalConsulta   = "SELECT date_add(CURDATE(), INTERVAL - 0 DAY) as FechaFinalConsulta";
    $rs = fn_ejecuta_query($sqlFechaFinalConsulta);
    //$fechaFinal = $rs['root'][0]['FechaFinalConsulta'];
    // $Borra = 'DELETE FROM al510tmp';
    //$rs = fn_ejecuta_query($Borra);

    $fechaInicio = date('Y-m-d', strtotime("-7 days")); //- 7 días
    $fechaFinal = date('Y-m-d');
   

  
    $sqlUnidades ="INSERT INTO al510tmp  (centroDistribucion, vin, fechaEvento, horaEvento, claveMovimiento, scacCode, idTarifa, ".
                  "distribuidor,fechaMovimientoEP,horaMovimientoEp ,oriSplc) ".
                  "SELECT hu.centroDistribucion,hu.vin,CAST(hu.fechaEvento AS DATE) as fechaHistorico ,substring(hu.fechaEvento,11,10) as horaHistorico, ".
                  "hu.claveMovimiento,'XTRA' AS scacCode,hu.idTarifa, hu.distribuidor, ".
                  "(SELECT max(cast(hu2.fechaEvento as DATE)) FROM alhistoricounidadestbl  hu2 WHERE hu2.vin = hu.vin AND hu2.claveMovimiento = 'AM' ) as fechaMovimientoEP, ".
                  "(SELECT max(substring(hu2.fechaEvento,11,10)) FROM alhistoricounidadestbl  hu2 WHERE hu2.vin = hu.vin AND hu2.claveMovimiento = 'AM' ) as horaMovimientoEP, ".
                  "(SELECT ge.nombre FROM cageneralestbl ge WHERE tabla = 'interfaces' AND columna = 'splc' AND hu.centroDistribucion = ge.valor ) as oriSplc ". 
                  "FROM alhistoricounidadestbl hu, alunidadestbl au, casimbolosunidadestbl su ".
                  "WHERE hu.centroDistribucion IN('CDTOL','CDSAL','CDAGS','CDANG','CDSFE','CDVER','CDLZC') ".
                  "AND hu.vin = au.vin ". 
                  "AND au.simboloUnidad = su.simboloUnidad ".
                  "AND su.marca NOT IN('HI','KI') ".
                  "AND hu.claveMovimiento IN ('ED','OM','ER','OK','RO','PT','ET') ".
                  "AND hu.distribuidor IN (SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE di.tipoDistribuidor != 'DX') ". 
                  "AND hu.idTarifa IN (SELECT idTarifa FROM catarifastbl WHERE tarifa != '13') ".
                  "AND CAST(hu.fechaEvento AS DATE) BETWEEN CAST('2019-02-19' AS DATE) AND CAST('2019-02-25' AS DATE) ".
                  "AND substring(hu.distribuidor,1,1) != 'H' ".
                  "AND au.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE rv.tipoTransaccion = 'V10' AND rv.vin = au.vin ) ".
                  "group by oriSplc,hu.fechaEvento;";

    $rssqlUnidades = fn_ejecuta_query($sqlUnidades);    
      //selecciona los ecabezados         
      //se Crea Archivo 
        $fileDir = "E:/carbook/i349/VA510.txt"; 
        echo "esta es la direccion".$fileDir;                                                  
        
        $fileLog = fopen($fileDir, 'a+');
       //print_r($rst['root']);
        $sqlEncabezado1 =  "SELECT scacCode FROM al510tmp where scacCode in ('XTRA','MITS') group by scacCode" ;
        $rst = fn_ejecuta_query($sqlEncabezado1); // CONSULTA 1 
        //echo json_encode($sqlEncabezado1);   
    if($rst['root']>0){
       //for ($i=0;$i < sizeof($rst['root']);$i++){
      if ($rst['root'][$i]['scacCode'] ='XTRA'){ 
            $marca='DCC';
      }else{
             $marca=$rst['root'][$i]['scacCode'];
      }
      $sqlEncabezado1 = array(1 => 'ISA*03*VA510     *00*          *ZZ*XTRA           *ZZ*ADMIS', 
           60 => $marca,
           64 => '      ',
           70 => '*',
           71 => date("y"),
           73 => date("m"),
           75 => date("d"),
           77 => '*',
           78 => date("h"),
           80 => date("i"),
           72 => '*U*00200*',
           89 => '00000000',
           90 =>  $i+1 ,
           100 => '*0*P*<');
      $text = getTxt($sqlEncabezado1, array_keys($sqlEncabezado1)).out('n', 1);  // PRIMER RENGLON
      fwrite($fileLog, $text);  
      $sqlEncabezado2 = array(1 => 'GS*VI*XTRA*INNI*', 
                         17 => date("y"),
                         19 => date("m"),
                         21 => date("d"),
                         23 => '*',
                         24 => date("h"),
                         26 => date("i"),
                         28 => '*1*T*1');
      $text2 = getTxt($sqlEncabezado2, array_keys($sqlEncabezado2)).out('n', 1);  // SEGUNDO RENGLON
      fwrite($fileLog, $text2);
      $sqlEncabezado3 = "SELECT  DISTINCT centroDistribucion,count(*) AS NoCdistribuidor FROM al510tmp  WHERE scacCode='".$rst['root'][$i]['scacCode'] ."' GROUP BY centroDistribucion";
      $rst3 = fn_ejecuta_query($sqlEncabezado3);
      //echo json_encode($sqlEncabezado3); // CONSULTA 2
      $incremento = 0;
      for ($j=0;$j < sizeof($rst3['root']);$j++){                  
        if ( $rst3['root'][$j]['centroDistribucion'] == "CDTOL")
          {$SPLC='958770807';}
            //echo $SPLC;
      $sqlEncabezado4 = "SELECT t.centroDistribucion,t.fechaMovimientoEp,count(t.vin) AS NoVinxfecha, t.oriSplc ". 
                        "FROM al510tmp t ".
                        "WHERE t.scacCode = '".$rst['root'][$i]['scacCode']."' ".
                        "AND t.centroDistribucion = '". $rst3['root'][$j]['centroDistribucion'] ."' ".
                        "GROUP BY T.fechaMovimientoEp;";

      $rst4 = fn_ejecuta_query($sqlEncabezado4);

      //echo json_encode($sqlEncabezado4); // CONSULTA 3      
      for ($m=0;$m < sizeof($rst4['root']);$m++){ 
        $sqlEncabezado4 = array(1 => 'ST*510*1', 
                                9 => str_pad(($incremento+1),4,'0',STR_PAD_LEFT));
        $text2 = getTxt($sqlEncabezado4, array_keys($sqlEncabezado4)).out('n', 1); // TERCERO RENGLON
        fwrite($fileLog, $text2);
        $sqlEncabezado4 = array(1 => 'BV1*XTRA*', 
                                10 => $rst4['root'][$m]['oriSplc'],
                                11 => '*',
                                20 => $rst4['root'][$m]['NoVinxfecha']);
        $text2 = getTxt($sqlEncabezado4, array_keys($sqlEncabezado4)).out('n', 1); // CUARTO RENGLON
        fwrite($fileLog, $text2);
        $fechaEvento = date_create($rst4['root'][$m]['fechaMovimientoEp']);
        // echo $fechaEvento; aqui finaliza el cilco
        $sqlEncabezado5 =  "SELECT centroDistribucion,vin,fechaEvento,horaEvento,fechaMovimientoEp,horaMovimientoEp,distribuidor, oriSplc FROM al510tmp WHERE scacCode = '".$rst['root'][$i]['scacCode']."'AND centroDistribucion = '". $rst3['root'][$j]['centroDistribucion'] ."' AND fechaMovimientoEp = '". date_format($fechaEvento,'Y-m-d') . "'"  ; 
        $rst5 = fn_ejecuta_query($sqlEncabezado5); //CONSULTA 4
                //echo json_encode($sqlEncabezado5);
        for ($k=0;$k < sizeof($rst5['root']);$k++){                                
          $sqlcadena1 ="SELECT tl.folio as talon, hu.fechaEvento as FechaEvento2, ". 
                        "hu.centroDistribucion as centroDistribucion,tl.companiaRemitente as compania, tr.tractor,substring(hu.fechaEvento,11,10) as horaEvento ".
                        "FROM alhistoricounidadestbl hu, trunidadesdetallestalonestbl ts, trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr ".
                        "WHERE hu.vin = ts.vin ".
                        "AND vt.idViajeTractor = tl.idViajeTractor ". 
                        "AND tl.idTalon = ts.idTalon ".
                        "AND vt.idTractor = tr.idTractor ". 
                        "AND hu.claveMovimiento ='AM' ". 
                        "AND hu.claveChofer = vt.claveChofer ".
                        "AND hu.fechaEvento = (SELECT max(hu2.fechaEvento) FROM alhistoricounidadestbl  hu2 WHERE hu2.vin = hu.vin AND hu2.claveMovimiento = hu.claveMovimiento) ".
                        "AND hu.vin = '".$rst5['root'][$k]['vin']."';";

          $rst6 = fn_ejecuta_query($sqlcadena1); //CONSULTA 5
          //echo json_encode($rst6);
          for ($l=0;$l < sizeof($rst6['root']);$l++){  
            $date_evento= date_create($rst6['root'][$l]['fechaEvento']);
            $cadena = $rst6['root'][$l]['centroDistribucion'].$rst6['root'][$l]['compania'].$rst6['root'][$l]['tractor'].$rst6['root'][$l]['talon'].date_format($fechaEvento,"d/m/Y");
            //echo $cadena; 
            $hrUnidad = substr($rst6['root'][$l]['horaEvento'],1,2).substr($rst6['root'][$l]['horaEvento'],4,2);                                    
                         
            $sqlEncabezado5 = array(1 => 'VI*', 
                                    4 => $rst5['root'][$k]['vin'],
                                    21 => '****',
                                    25 => $rst5['root'][$k]['distribuidor'],
                                    30 => '*',
                                    31 => $cadena);
            $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
            fwrite($fileLog, $text2);

            $date_movi = date_create($rst5['root'][$k]['fechaMovimientoEp']);
            $hora_Eve  = date_create($rst5['root'][$k]['horaEvento']);
            $date_Eve  = date_create($rst5['root'][$k]['fechaEvento']);
            $sqlEncabezado5 = array(1 => 'P1**', 
                                    5 =>  date_format($date_movi, 'y'),
                                    7 =>  date_format($date_movi, "m"),
                                    9 =>  date_format($date_movi, "d"),
                                    11 => '*A');
            $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
            fwrite($fileLog, $text2);
            $sqlEncabezado5 = array(1 => 'P2**', 
                                    5 =>  date_format($date_Eve,'y'),
                                    7 =>  date_format($date_Eve,"m"), 
                                    9 =>  date_format($date_Eve,"d"), 
                                    11 => '*A*',
                                    14 => $hrUnidad);                                                               
            //15 => date_format($rst6['root'][$k]['horaEvento'],"i"));
            $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
            fwrite($fileLog, $text2);
          }                                                                    
        }
         
        $numrendetalle = ($rst4['root'][$m]['NoVinxfecha']*3)+1;
        $numrendetalle =str_pad($numrendetalle,4,'0',STR_PAD_LEFT);
        $sqlEncabezado5 = array(1 => 'SE*', 
                      4 => $numrendetalle,                      
                      7 => '*1',
                      9 => str_pad(($incremento+1),4,'0',STR_PAD_LEFT));
        $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
        fwrite($fileLog, $text2);
        $incremento = $incremento + 1;               
      }                 
    }
    $sqlEncabezado5 = array(1 => 'GE*', 
                            4 => $incremento,
                            7 => '*',
                            9 => $i+1);
    $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
    fwrite($fileLog, $text2);

    echo "Fin de la Interfaz, numero de Bloques".$incremento;
//}
    $sqlEncabezado5 = array(1 => 'IEA*01', 
                            4 => '*000000001');
    $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
    fwrite($fileLog, $text2);
  }else{
    echo "No Existen Unidades de Interfaz";
  }      
  fclose($fileLog);
    
?>
 