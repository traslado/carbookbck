<?php
	require_once("../funciones/generales.php");
	//require_once("../funciones/utilidades.php");
    //require_once("../funciones/utilidadesProcesos.php");



	switch ($_REQUEST['actionHdn']) {
		case 'obtenFolio':
			obtenFolio();
			break;
		
		default:
			# code...
			break;
	}

	function obtenFolio(){
		$selFolio="SELECT estatus as folio FROM cageneralestbl WHERE tabla= 'PFACKIA' AND valor='trap832'";
		$rstFolio=fn_ejecuta_query($selFolio);
		echo json_encode($rstFolio);

		if ($rstFolio['records']>0) {
			$folio=$rstFolio['root'][0]['folio']+1;

			$update="UPDATE cageneralestbl SET estatus='".$folio."' WHERE tabla='PFACKIA' AND valor='trap832'";
			$rstUpdate=fn_ejecuta_query($update);
			//echo json_encode($update);
		}
	}

	switch ($_REQUEST['centroDistribucionTxt']) {
		case 'CDTOL':
			switch ($_REQUEST['marcaTxt']) {
				case 'KIA':
					switch ($_REQUEST['origenTxt']) {
						case 'T':
							$puerto='TOL';
							$destino='D';
							$statEnt='ALB';
							$statDy='EK';
							break;
						case 'W':
							$puerto='TRA';
							$destino='D';
							$statEnt='ALB';
							$statDy='EK';
						break;
						default:
							# code...
							break;
					}
				break;
				case 'HYU':
					$puerto='TOL';
					$destino='D';
					$statEnt='ALB';
					$statDy='EH';
					$origen='TOL';
					break;

				default:
					# code...
					break;
			}
		break;
		default:
			break;
		case 'CDVER':
			switch ($_REQUEST['marcaTxt']) {
				case 'KIA':
					$origen='VRS';
					$puerto='VRS';
					$destino='D';
					$statEnt='ALB';
					$statDy='EK';
			break;
				case 'HYU':
					$origen='VRZ';
					$puerto='VRZ';
					$destino='D';
					$statEnt='ALB';
					$statDy='EH';
					break;
			}	
		break;
		case 'CDMTY':
			switch ($_REQUEST['marcaTxt']) {
				case 'KIA':
					$origen='MTY';
					$puerto='MTY';
					$destino='D';
					$statEnt='ALB';
					$statDy='EK';
					break;
			}	
		break;
		case 'CDLZC':
			switch ($_REQUEST['marcaTxt']) {
				case 'KIA':
				switch ($_REQUEST['destinoTxt']) {
					case 'D':
						$origen='LZC';
						$puerto='LZC';
						$statEnt='ALB';
						$statDy='EK';	
						break;
					case 'P':
						$origen='LZC';
						$puerto='LZC';
						$statEnt='ALR';
						$statDy='EK';	
						break;
					
					default:
						# code...
						break;
				}
			}	
		break;
		case 'CDSFE':
			switch ($_REQUEST['marcaTxt']) {
				case 'KIA':
					$origen='SFE';
					$puerto='TRB';
					$destino='D';
					$statEnt='ALB';
					$statDy='EK';
					break;
			}	
		break;
	}

	if ($_REQUEST['marcaTxt']=='KIA') {
		$tipoDis='K';
	}
	if ($_REQUEST['marcaTxt']=='HYU') {
		$tipoDis='H';
	}

	switch ($_REQUEST['opcionRdoGpo']) {
		case 'P':
			switch ($_REQUEST['procesoRdoGpo']) {
				case 'A':
					automatico($puerto,$statDy,$destino);
					break;
				case 'M':
					manual($puerto,$statDy,$destino);
					break;
				default:
					# code...
					break;
			}
		break;

		case 'F':
			facturacion();
		break;	

		case 'C':
			confirmaFactura();
		break;	

		case 'D':
			cancelaPreFactura()	;
		break;	

		default:
			# code...
			break;
	}
	

	function automatico($puerto,$statDy,$destino){

		$selPuerto="SELECT distinct origen FROM catarifastar"
	               ." WHERE marca='".$_REQUEST['marcaTxt']."'"
	               ." AND puerto='".$puerto."'";
	    $rstSelPuerto=fn_ejecuta_query($selPuerto);
	    //echo json_encode($rstSelPuerto);

		$selCon="SELECT  dy.vin, dy.natType, dy.cveLoc,dy.cveDisEnt, dy.chipNum, rv.centroDistribucion,"
			 	." rv.fechaMovimiento,ts.idTalon,ca.idTractor, dt.fechaEvento"
				." FROM alinstruccionesmercedestbl dy , altransaccionunidadtbl rv  ,trunidadesdetallestalonestbl ts ,trtalonesviajestbl tr,"
				." trviajestractorestbl tv, catractorestbl ca , alhistoricounidadestbl dt"
				." WHERE dy.cveStatus='".$statDy."'"
				." AND dy.cveLoc='".$rstSelPuerto['root'][0]['origen']."'"
				." AND EXISTS (SELECT * FROM cadistribuidorescentrostbl di WHERE di.distribuidorCentro=dy.cveDisEnt)"
				." AND dy.vin=rv.vin"
				." AND rv.tipoTransaccion in ('ALB','TLP')"
				." AND rv.claveMovimiento in ('OM','AM')"
				." AND rv.vin= ts.vin"
				." AND ts.idtalon=tr.idTalon"
				." AND ts.estatus != 'X'"
				." AND tr.tipoDocumento='R'"
				." AND tr.idViajeTractor=tv.idViajeTractor"
				." AND tv.idTractor=ca.idTractor"
				." AND ca.compania IN ('TR','CR','ST')"
				." AND dt.vin=dy.vin"
				." AND rv.centroDistribucion=dt.centroDistribucion"
				." AND dt.claveMovimiento in ('OM','AM')"
				." AND dt.fechaEvento=rv.fechaMovimiento";
		$rstSelCon=fn_ejecuta_query($selCon);
		//echo json_encode($rstSelCon);

		datos($rstSelCon,$destino);
	}

	function manual($puerto,$statDy,$destino){

		$datos=$_REQUEST['datos'];

		$array = str_split($datos,53);		

		for ($i=0; $i <sizeof($array) ; $i++) { 

			$customer[]=substr($array[$i],0,3);
			$vendor[]=substr($array[$i],4,3);
			$departure[]=substr($array[$i],8,3);
			$dealer[]=substr($array[$i],12,5);
			$vin[]=substr($array[$i],18,17);
			$ALBdate[]=substr($array[$i],36,8);
			$TLPdate[]=substr($array[$i],45,8);

		}


		$cadena=implode(' ', $vin);
		//echo $cadena;
		$cadena1 = chunk_split($cadena, 18,"','");
		$vines=substr($cadena1,0,-2);

		//echo json_encode($vines);

		$selPuerto="SELECT distinct origen FROM catarifastar"
	               ." WHERE marca='".$_REQUEST['marcaTxt']."'"
	               ." AND puerto='".$puerto."'";
	    $rstSelPuerto=fn_ejecuta_query($selPuerto);
	    //echo json_encode($rstSelPuerto);

		$selCon="SELECT  dy.vin, dy.natType, dy.cveLoc,dy.cveDisEnt, dy.chipNum, rv.centroDistribucion,"
			 	." rv.fechaMovimiento,ts.idTalon,ca.idTractor, dt.fechaEvento"
				." FROM alinstruccionesmercedestbl dy , altransaccionunidadtbl rv  ,trunidadesdetallestalonestbl ts ,trtalonesviajestbl tr,"
				." trviajestractorestbl tv, catractorestbl ca , alhistoricounidadestbl dt"
				." WHERE dy.cveStatus='".$statDy."'"
				." AND dy.cveLoc='".$rstSelPuerto['root'][0]['origen']."'"
				." AND EXISTS (SELECT * FROM cadistribuidorescentrostbl di WHERE di.distribuidorCentro=dy.cveDisEnt)"
				." AND dy.vin in ('".$vines.")"
				." AND dy.vin=rv.vin"
				." AND rv.tipoTransaccion in ('ALB','TLP')"
				." AND rv.claveMovimiento in ('OM','AM')"
				." AND rv.vin= ts.vin"
				." AND ts.idtalon=tr.idTalon"
				." AND ts.estatus != 'X'"
				." AND tr.tipoDocumento='R'"
				." AND tr.idViajeTractor=tv.idViajeTractor"
				." AND tv.idTractor=ca.idTractor"
				." AND ca.compania IN ('TR','CR','ST')"
				." AND dt.vin=dy.vin"
				." AND rv.centroDistribucion=dt.centroDistribucion"
				." AND dt.claveMovimiento in ('OM','AM')"
				." AND dt.fechaEvento=rv.fechaMovimiento";
		$rstSelCon=fn_ejecuta_query($selCon);
		//echo json_encode($rstSelCon);

		datos($rstSelCon,$destino);
	}

	function datos($rstSelCon,$destino){
		
		$importe='1';
		$wkms='1';



		for ($i=0; $i <sizeof($rstSelCon['root']) ; $i++) { 

			if ($rstSelPuerto['root'][0]['origen']=='FT11' or $rstSelPuerto['root'][0]['origen']=='FT18' or $rstSelPuerto['root'][0]['origen']=='1140') {
				$selPlaza="SELECT *  FROM caplazastbl p,cadistribuidorescentrostbl d"
							." WHERE d.distribuidorCentro='".$rstSelCon['root'][$i]['cveDisEnt']."'"
							." AND p.idplaza=d.idplaza";
			$rstSelPlaza=fn_ejecuta_query($selPlaza);
			//echo json_encode($rstSelPlaza);

			$selPlazaDestino="SELECT idPlaza from cadistribuidorescentrostbl  where distribuidorCentro='".$_REQUEST['centroDistribucionTxt']."'";
			$rstSelPlazaDestino=fn_ejecuta_query($selPlazaDestino);
			}

			if ($rstSelPlaza['records']!=0) {
				$selKms="SELECT kilometros from caKilometrosPlazaTbl  where idPlazaDestino='".$rstSelPlaza['root'][$i]['idPlaza']."'"
				." and idPlazaOrigen='".$rstSelPlazaDestino['root'][$i]['idPlaza']."'";
				$rstSelKms=fn_ejecuta_query($selKms);
				$wkms=$rstSelKms['root'][$i]['kilometros'];
			}

			$selTarifa="SELECT * from catarifastar where origen='".$rstSelPuerto['root'][0]['origen']."'"
						." AND fechaHasta >='".$rstSelCon['root'][$i]['fechaEvento']."'"
						." AND kilometros='".$wkms."'"
						." order by fechaHasta,kilometros";
			$rstSelTarifa=fn_ejecuta_query($selTarifa);

			if ($rstSelTarifa['records'] !=0) {
				if ($rstSelTarifa['root'][$i]['tipo']=='U') {
					$importe=$rstSelTarifa['root'][$i]['impRate'];
				}
				else{
					$importe=1;
				}
			}

			$tarifa1=($importe*$wkms)/9.5;

		}
		insert($rstSelCon,$destino,$tarifa1);
	}

	function insert($rstSelCon,$destino,$tarifa1){

		for ($i=0; $i <sizeof($rstSelCon['root']) ; $i++) { 
			
			$insertDat="INSERT INTO alfhktmp (concepto, vin, folio, marca, cveTran, puertoOrigen, destino, locCod, folioFactura," 
								." avanzada, delEnt, claveMovimiento, fechaStatus, horaStatus, costCod, workCode, importe, fechaAsignacion, horaAsignacion, "
								." diasalm, diaslib, diascus, costo, tractor, chipNum)"
								." VALUES ('T', '".
									$rstSelCon['root'][$i]['vin']."','".
									$_REQUEST['folioTxt']."','".
									$_REQUEST['marcaTxt']."',"
									."'ALI','".
									$_REQUEST['centroDistribucionTxt']."','"
									.$destino."','".
									$rstSelCon['root'][$i]['cveLoc']."',"
									." '...',"
									." '".substr($rstSelCon['root'][$i]['vin'],9,8)."','" 
									.$rstSelCon['root'][$i]['cveDisEnt']."',"
									."'OM'," 
									." '".$rstSelCon['root'][$i]['fechaMovimiento']."',"
									." '".$rstSelCon['root'][$i]['fechaEvento']."',"
									." 'TP',"
									."'...','"
									.$tarifa1."'," 
									." '".$rstSelCon['root'][$i]['fechaMovimiento']."',"
									." '".$rstSelCon['root'][$i]['fechaEvento']."',"
									."'0'," 
									."'0'," 
									."'0','" 
									.$tarifa1."'," 
									." '".$rstSelCon['root'][$i]['idTractor']."',"
									." '".$rstSelCon['root'][$i]['chipNum']."')";
			$rstInsertDat=fn_ejecuta_query($insertDat);
		}
		generaArchivoConta();	
	}

	function generaArchivoConta(){
	
		$fechaArchivo=date("YmdHms");

		$basic="SELECT * FROM alfhktmp WHERE concepto='T'";
		$rstBasic=fn_ejecuta_query($basic);

		$folio=$rstBasic['root'][0]['folio'];
		$marca=$rstBasic['root'][0]['marca'];

		//ARCHIVO DE SOPORTE PARA CONTABILIDAD
		$fileDir="C:/carbook/TRAP818ftp/TOL".$marca."000".$folio."_".$fechaArchivo.".csv";
		$fileLog = fopen($fileDir, 'a+');	


		for ($i=0;$i < sizeof($rstBasic['root']);$i++) {

	    		$encabezado2 = array(1  => "000".$rstBasic['root'][$i]['folio'].",",
					    			7  => $rstBasic['root'][$i]['marca'].",",
					    			11  =>$rstBasic['root'][$i]['puertoOrigen'].",",
					    			16 =>$rstBasic['root'][$i]['vin'].",",
					    			29  => date('YmdHms',strtotime($rstBasic['root'][$i]['fechaStatus'])) .",",
					    			40  => $rstBasic['root'][$i]['delEnt'].",",
					    			50  => $rstBasic['root'][$i]['costCod'].",",
					    			54 =>$rstBasic['root'][$i]['importe']);
	    			
	    		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
		}
	}

	function getTxt($texts, $positions){
    	$text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = ($positions[$i]);
                $antLength = strlen($texts[$positions[$i]]);
            }
            $text .= out('s', ($positions[$i]-1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }
    function out($char, $times){
        $validChar = array(
                    "n" => PHP_EOL,
                    "t" => "\t",
                    "s" => " ");
        $chars = '';
        for ($i=0; $i < $times ; $i++) { 
            $chars .= $validChar[$char];
        }
        fseek($myfile, -20);   
        return $chars;
    }

    function facturacion(){
    	
    	$folio=$_REQUEST['folioTxt'];

    	$selDatos="SELECT * FROM alfhktmp WHERE concepto='T' AND folio=".$folio;
    	$rstSelDatos=fn_ejecuta_query($selDatos);
    	//echo json_encode($rstSelDatos);

    	$fechaArchivo=date("YmdHms");

		
		//ARCHIVO DE INTERFASE ALI
		$fileDir="C:/carbook/TRAP818ftp/".$rstSelDatos['root'][0]['marca']."_ALI_".$fechaArchivo.".txt";
		$fileLog = fopen($fileDir, 'a+');	

	    		$encabezado1 = array(1  => $rstSelDatos['root'][0]['cveTran']."H ",
					    			7  => "TRA ",
					    			11  => "GMX ",
					    			16 => $rstSelDatos['root'][0]['cveTran'],
					    			29  => $fechaArchivo);
	    			
		$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);


	   		$cons=0;

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 

   	        $factura=$_REQUEST['folioFacturaTxt'];

	    	$consec=$cons+1;
	    	$avanzada=substr($rstSelDatos['root'][$i]['vin'],9,8);
	    	$netImp='0.00';
	    	$fscImp='0.00';
	    	$oth1Imp='0.00';
	    	$oth2Imp='0.00';
	    	$netImp=$rstSelDatos['root'][$i]['importe'];
	    	$tofImp=($netImp+$fscImp)+($oth1Imp+$oth2Imp);
	    	$retImp=$tofImp*0.04;
	    	$ivaImp=$tofImp*0.16;
	    	$grossImp=$tofImp+$ivaImp-$retImp;

		    	$detalle = array(1  => $rstSelDatos['root'][$i]['cveTran']." ",
				    			7  => date('YmdHms',strtotime($rstSelDatos['root'][$i]['fechaStatus'])),
				    			10 => "TRA ",
				    			16 => $rstSelDatos['root'][$i]['vin'],
				    			22 => "P",
				    			25 => $rstSelDatos['root'][$i]['locCod'],
				    			28 => $rstSelDatos['root'][0]['destino'],
				    			30 => $rstSelDatos['root'][0]['delEnt'],
				    			35 => "D",
				    			40 => $rstSelDatos['root'][$i]['chipNum']." ",
				    			45 => "000000000000000",
				    			50 => $rstSelDatos['root'][$i]['tractor'],
				    			60 => $factura,
				    			70 => $consec,
				    			75 => "MXN",
				    			80 => substr($fechaArchivo,0,8),
				    			90 => $grossImp*10000,
				    			100 => $netImp*10000,
				    			110 => $fscImp*10000,
				    			120 => $oth1Imp*10000,
				    			130 => $oth2Imp*10000,
				    			140 => $tofImp*10000,
				    			150 => $retImp*10000,
				    			160 => $ivaImp*10000);
		    			
				$text = getTxt($detalle, array_keys($detalle)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			 	$cons++;

		}

		$trailer = array(1  => $rstSelDatos['root'][$i]['cveTran']."T ",
		    			7  => "000",
		    			10 =>$cons+2);
		    			
		$text = getTxt($trailer, array_keys($trailer)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 
	    	if ($rstSelDatos['root'][$i]['marca']=='KIA') {
	    		$claveMarca='FK';
	    		$estatus='EK';
	    	}
	    	if ($rstSelDatos['root'][$i]['marca']=='HYU') {
	    		$claveMarca='FH';
	    		$estatus='EH';
	    	}
	    	


		    	$insRv1="INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion,folio, vin, fechaGeneracionUnidad, claveMovimiento,"
		    		." fechaMovimiento, prodStatus) "
					." VALUES ('"
					.$rstSelDatos['root'][$i]['cveTran']."','"
					.$rstSelDatos['root'][$i]['puertoOrigen']."','"
					.$factura."','"
					.$rstSelDatos['root'][$i]['vin']."','"
					.$rstSelDatos['root'][$i]['fechaStatus']."','"
					.$claveMarca."','"
					.$fechaArchivo."','"
					.$rstSelDatos['root'][$i]['claveMovimiento']."')";
				$rstInsRv1=fn_ejecuta_query($insRv1);

				$updDy="UPDATE alinstruccionesmercedestbl SET pedNum='".$factura."', cveStatus='".$claveMarca
						."' WHERE vin='".$rstSelDatos['root'][$i]['vin']."' AND cveStatus='".$estatus."'";
				$rstUpdDy=fn_ejecuta_query($updDy);
	    }    
    }

    function confirmaFactura(){
    	$folio=$_REQUEST['folioTxt'];
        $factura=$_REQUEST['folioFacturaTxt'];


    	$selDatos="SELECT * FROM alfhktmp WHERE concepto='T' AND folio=".$folio;
    	$rstSelDatos=fn_ejecuta_query($selDatos);

    	//echo json_encode($rstSelDatos);
	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 
	    	$cont=$cont+1;
	    	if ($rstSelDatos['root'][$i]['marca']=='KIA') {
		    		$estatus='FK';
		    		$nvoEstatus='TK';
		    		$estaPat='3K';
		    		$nvoEstp='8K';
		    	}
		    	if ($rstSelDatos['root'][$i]['marca']=='HYU') {
		    		$estatus='FH';
		    		$nvoEstatus='TH';
		    		$estaPat='3H';
		    		$nvoEstp='8H';
		    	}
		    $selDy="SELECT * FROM alinstruccionesmercedestbl  where vin='".$rstSelDatos['root'][$i]['vin']."' AND cveStatus='".$estatus."'";
		    $rstSelDy=fn_ejecuta_query($selDy);

		    if ($rstSelDy['records'] !=0) {
		    	$tratdy=$tratdy+1;
		    }

		    $selRv1="SELECT * FROM altransaccionunidadtbl WHERE tipoTransaccion='ALI' AND vin='".$rstSelDatos['root'][$i]['vin']."'";
		    $rstSelRv1=fn_ejecuta_query($selRv1);

		    if ($rstSelRv1['records'] !=0) {
		    	$tratrv1=$tratrv1+1;
		    }
		}

		if ($cont !=0) {
			if ($cont=$tratdy AND $cont=$tratrv1) {
				$selDatos="SELECT * FROM alfhktmp WHERE concepto='T' AND folio=".$folio;
    			$rstSelDatos=fn_ejecuta_query($selDatos);
			}
		}

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 
	    	$updDy1="UPDATE alinstruccionesmercedestbl SET cveStatus='".$nvoEstatus
						."' WHERE vin='".$rstSelDatos['root'][$i]['vin']."' AND cveStatus='".$estatus."'";
			$rstUpdDy1=fn_ejecuta_query($updDy1);

			$selDy1="SELECT * FROM alinstruccionesmercedestbl WHERE vin='".$rstSelDatos['root'][$i]['vin']."' AND cveStatus='".$estaPat."'";
			$rstSelDy1=fn_ejecuta_query($selDy1);
			if ($rstSelDy1['records'] !=0) {
				$updDy2="UPDATE alinstruccionesmercedestbl SET pedNum='".$factura."', cveStatus='".$nvoEstp
						."' WHERE vin='".$rstSelDatos['root'][$i]['vin']."' AND cveStatus='".$estaPat."'";
				$rstUpdDy2=fn_ejecuta_query($updDy2);
			}
	    }

	    $delTemp="DELETE FROM alfhktmp WHERE concepto='T' AND marca='".$_REQUEST['marcaTxt']."'" ." AND folio='".$folio."'";
	    $rstDelTemp=fn_ejecuta_query($delTemp);
    }

    function cancelaPreFactura(){
    	$folio=$_REQUEST['folioTxt'];

		$selTra="SELECT * FROM alfhktmp WHERE folio=".$folio." AND concepto='T'";
		$rstSelTra=fn_ejecuta_query($selTra);


		if ($rstSelTra['records'] != '0') {
			$delPreFac="DELETE FROM alfhktmp WHERE folio=".$folio." AND concepto='T'";
			$rstDelPreFac=fn_ejecuta_query($delPreFac);
		}
    }

    


	
	     
			
?>