<?php
	require_once("../funciones/generales.php");
	//require_once("../funciones/utilidades.php");
    //require_once("../funciones/utilidadesProcesos.php");



	switch ($_REQUEST['actionHdn']) {
		case 'obtenFolio':
			obtenFolio();
			break;
		
		default:
			# code...
			break;
	}

	function obtenFolio(){
		$selFolio="SELECT estatus as folio FROM cageneralestbl WHERE tabla= 'PFACKIA' AND valor='trap832'";
		$rstFolio=fn_ejecuta_query($selFolio);
		echo json_encode($rstFolio);

		if ($rstFolio['records']>0) {
			$folio=$rstFolio['root'][0]['folio']+1;

			$update="UPDATE cageneralestbl SET estatus='".$folio."' WHERE tabla='PFACKIA' AND valor='trap832'";
			$rstUpdate=fn_ejecuta_query($update);
			//echo json_encode($update);
		}
	}

	switch ($_REQUEST['conceptoTxt']) {
		case 'S':
			switch ($_REQUEST['opcionTxt']) {
				case 'P':
					switch ($_REQUEST['seleccionTxt']) {
						case 'A':
							storageAuto();
							break;
						case 'M':
								storageManual();
							break;	

						default:
							# code...
							break;
					}
					break;
				case 'F':
					facturacionStor();
					break;
				case 'C':
					confirmacionStorage();
					break;
				case 'D':
					cancelaPreFacST();
					break;				
				default:
					# code...
					break;
			}
			break;
		case 'B':
			switch ($_REQUEST['opcionTxt']) {
				case 'P':
					switch ($_REQUEST['seleccionTxt']) {
						case 'A':
							basicAuto();
							break;
						case 'M':
							basicManual();
							break;	

						default:
							# code...
							break;
					}
					break;
				case 'F':
					facturacionBasic();
					break;
				case 'C':
					confirmacionBasic();
					break;
				case 'D':
					cancelaPreFacBS();
					break;					
				default:
					# code...
					break;
			}
			break;
		case 'P':
			switch ($_REQUEST['opcionTxt']) {
				case 'P':
					switch ($_REQUEST['seleccionTxt']) {
						case 'A':
							PDIAuto();
							break;
						case 'M':
							PDIManual();
							break;	

						default:
							# code...
							break;
					}
					break;
				case 'F':
					facturacionPDI();
					break;
				case 'C':
					confirmacionPDI();
					break;		
				case 'D':
					cancelaPreFacPDI();
					break;			
				default:
					# code...
					break;
			}
			break;
		
	default:
		
	break;
	}

	function storageAuto(){

		$fechaHasta=substr($_REQUEST['fechaTxt'],0,10);

		$fechaDesde=strtotime ( '-21 day' , strtotime ( $fechaHasta ) );
		$fechaDesde = date ( 'Y-m-j' , $fechaDesde );

		$selStor=" SELECT rv.vin,rv.claveMovimiento,dy.vin,rv.fechaMovimiento, dt.fechaEvento" 
				 ." FROM altransaccionunidadtbl rv, alinstruccionesmercedestbl dy, alhistoricounidadestbl dt"
				 ." WHERE rv.tipoTransaccion='TLP'"
				 ." AND rv.centroDistribucion='CDTOL'"
				 ." AND rv.claveMovimiento='AM'"
				 ." AND rv.fechaMovimiento BETWEEN '".$fechaDesde."' AND '".$fechaHasta."'" //." AND rv.fechaMovimiento BETWEEN '2016-06-13' AND '2016-08-08'"
				 ." AND rv.vin = dy.vin"
				 ." AND rv.vin=dt.vin"
 				 ." AND dt.centroDistribucion='CDTOL'"
 				 ." AND dt.claveMovimiento='AM'"
				 ." AND dy.cveStatus in ('PK','AK','SK','FK','EK','TK','QK')"
				 ." AND  EXISTS (SELECT * FROM altransaccionunidadtbl al "
								." WHERE al.centroDistribucion=al.centroDistribucion "
				                ." AND al.tipoTransaccion='ALR'"
				                ." AND al.vin=rv.vin"
				                ." AND al.claveMovimiento in ('RP','PR'))"
				." AND NOT EXISTS (SELECT * FROM altransaccionunidadtbl ab"
								." WHERE ab.centroDistribucion='TRA'"
				                ." AND ab.tipoTransaccion='INS'"
				                ." AND ab.vin=rv.vin"
				                ." AND ab.claveMovimiento='ST'"
				                ." AND ab.prodStatus='AM')";
		$rstSelStor=fn_ejecuta_query($selStor);
		//echo json_encode($rstSelStor);

		insertStorage($rstSelStor);
	}

	function storageManual(){

		$fechaHasta=substr($_REQUEST['fechaTxt'],0,10);

		$fechaDesde=strtotime ( '-21 day' , strtotime ( $fechaHasta ) );
		$fechaDesde = date ( 'Y-m-j' , $fechaDesde );


		$datos=$_REQUEST['datos'];
		//echo $datos;

		$array = str_split($datos,39);
		//echo json_encode($array);  

		for ($i=0; $i <sizeof($array) ; $i++) { 
			$departure[]=substr($datos,0,3);
			$vin[]=substr($datos,4,17);
			$portinDate[]=substr($datos,22,8);
			$ShipementDate[]=substr($datos,31,8);
		}

		$cadena=implode(' ', $vin);
		$cadena1 = chunk_split($cadena, 18,"','");
		$vines=substr($cadena1,0,-2);


		$selStor="  SELECT rv.vin,rv.claveMovimiento,dy.vin,rv.fechaMovimiento, dt.fechaEvento"
				 ." FROM altransaccionunidadtbl rv, alinstruccionesmercedestbl dy, alhistoricounidadestbl dt "
				 ." WHERE rv.vin in('".$vines.")" 
				 ." AND rv.tipoTransaccion='TLP'" 
				 ." AND rv.centroDistribucion='CDTOL' "
				 ." AND rv.claveMovimiento='AM' "
				 ." AND rv.fechaMovimiento BETWEEN '".$fechaDesde."' AND '".$fechaHasta."'" //." AND rv.fechaMovimiento BETWEEN '2016-06-13' AND '2016-08-08'"
				 ." AND rv.vin = dy.vin"
				 ." AND rv.vin=dt.vin"
 				 ." AND dt.centroDistribucion='CDTOL'"
                 ." AND dt.claveMovimiento='AM'"
				 ." AND dy.cveStatus in ('PK','AK','SK','FK','EK','TK','QK') "
				 ." AND EXISTS (SELECT * FROM altransaccionunidadtbl al "
								." WHERE al.centroDistribucion=al.centroDistribucion "
				                ." AND al.tipoTransaccion='ALR' "
				                ." AND al.vin=rv.vin "
				                ." AND al.claveMovimiento in ('RP','PR')) "
				." AND NOT EXISTS (SELECT * FROM altransaccionunidadtbl ab "
								." WHERE ab.centroDistribucion='TRA' "
				                ." AND ab.tipoTransaccion='INS' "
				                ." AND ab.vin=rv.vin "
				                ." AND ab.claveMovimiento='ST' "
				                ." AND ab.prodStatus='AM')";
		$rstSelStor=fn_ejecuta_query($selStor);
		//echo json_encode($selStor);
		
		insertStorage($rstSelStor);
	}

	function basicAuto(){

		$fechaHasta=substr($_REQUEST['fechaTxt'],0,10);

		$fechaDesde=strtotime ( '-21 day' , strtotime ( $fechaHasta ) );
		$fechaDesde = date ( 'Y-m-j' , $fechaDesde );


		$selBas="SELECT dt.vin, dt.fechaEvento, dy.vin" 
				." FROM alhistoricounidadestbl dt, alinstruccionesmercedestbl dy "
				." WHERE dt.centroDistribucion='CDTOL'"
				." AND dt.claveMovimiento in ('RP','PR')"
				." AND dt.fechaEvento BETWEEN '".$fechaDesde."' AND '".$fechaHasta."'" //." AND rv.fechaMovimiento BETWEEN '2016-06-13' AND '2016-08-08'"
				." AND dt.vin=dy.vin"
				." AND EXISTS (SELECT * FROM alinstruccionesmercedestbl dt"
							." WHERE dt.vin=dy.vin"
				            ." AND dt.cveStatus in ('3K','8K'))"
				." AND NOT EXISTS (SELECT * FROM altransaccionunidadtbl rv "
							." WHERE rv.tipoTransaccion='INV'"
				            ." AND rv.centroDistribucion='TRA'"
				            ." AND rv.vin = dt.vin"
				            ." AND rv.claveMovimiento='BS')";
		$rstSelBas=fn_ejecuta_query($selBas);
		//echo json_encode($selBas);

		insertBasic($rstSelBas);
	}

	function basicManual(){

		$fechaHasta=substr($_REQUEST['fechaTxt'],0,10);

		$fechaDesde=strtotime ( '-21 day' , strtotime ( $fechaHasta ) );
		$fechaDesde = date ( 'Y-m-j' , $fechaDesde );


		$datos=$_REQUEST['datos'];
		//echo $datos;

		$array = str_split($datos,39);
		//echo json_encode($array);  

		for ($i=0; $i <sizeof($array) ; $i++) { 
			$departure[]=substr($array[$i],0,3);
			$vin[]=substr($array[$i],4,17);
			$portinDate[]=substr($array[$i],22,8);
			$code[]=substr($array[$i],31,3);
		}

		$cadena=implode(' ', $vin);
		$cadena1 = chunk_split($cadena, 18,"','");
		$vines=substr($cadena1,0,-2);

		$selBas="SELECT dt.vin, dt.fechaEvento, dy.vin" 
				." FROM alhistoricounidadestbl dt, alinstruccionesmercedestbl dy "
				." WHERE dt.vin in('".$vines.")"
				." AND dt.centroDistribucion='CDTOL'"
				." AND dt.claveMovimiento in ('RP','PR')"
				." AND dt.fechaEvento BETWEEN '".$fechaDesde."' AND '".$fechaHasta."'" //." AND rv.fechaMovimiento BETWEEN '2016-06-13' AND '2016-08-08'"
				." AND dt.vin=dy.vin"
				." AND EXISTS (SELECT * FROM alinstruccionesmercedestbl dt"
							." WHERE dt.vin=dy.vin"
				            ." AND dt.cveStatus in ('3K','8K'))"
				." AND NOT EXISTS (SELECT * FROM altransaccionunidadtbl rv "
							." WHERE rv.tipoTransaccion='INV'"
				            ." AND rv.centroDistribucion='TRA'"
				            ." AND rv.vin = dt.vin"
				            ." AND rv.claveMovimiento='BS')";
		$rstSelBas=fn_ejecuta_query($selBas);
		//echo json_encode($selBas);
		insertBasic($rstSelBas);

	}

	function PDIAuto(){

		$fechaHasta=substr($_REQUEST['fechaTxt'],0,10);

		$fechaDesde=strtotime ( '-21 day' , strtotime ( $fechaHasta ) );
		$fechaDesde = date ( 'Y-m-j' , $fechaDesde );


		$selPDI="SELECT dt.vin, dt.fechaEvento,dy.vin, rv.hora"
				." FROM alhistoricounidadestbl dt, alinstruccionesmercedestbl dy, altransaccionunidadtbl rv"
				." WHERE dt.centroDistribucion='CDTOL'"
				." AND dt.claveMovimiento='PI'"
				." AND dt.fechaEvento BETWEEN '".$fechaDesde."' AND '".$fechaHasta."'" //." AND rv.fechaMovimiento BETWEEN '2016-06-13' AND '2016-08-08'"
				." AND dt.vin=dy.vin"
				." AND dy.cveLoc='FT15'"
				." AND rv.tipoTransaccion='ACN'"              
				." AND dt.centroDistribucion= rv.centroDistribucion" 
				." AND dt.vin = rv.vin"
				." AND rv.claveMovimiento='PI'"
				." AND NOT EXISTS (SELECT *FROM altransaccionunidadtbl ab"
								." WHERE ab.tipoTransaccion='INV'"
				                ." AND ab.centroDistribucion='TRA'"
				                ." AND ab.vin = dt.vin"
				                ." AND ab.claveMovimiento='WK'"
				                ." AND ab.prodStatus='PI')";
		$rstSelPDI=fn_ejecuta_query($selPDI);
		//echo json_encode($rstSelPDI);
		
		insertPDI($rstSelPDI);

	}

	function PDIManual(){

		$fechaHasta=substr($_REQUEST['fechaTxt'],0,10);

		$fechaDesde=strtotime ( '-21 day' , strtotime ( $fechaHasta ) );
		$fechaDesde = date ( 'Y-m-j' , $fechaDesde );

		
		$datos=$_REQUEST['datos'];
		//echo $datos;

		$array = str_split($datos,39);
		//echo json_encode($array);  

		for ($i=0; $i <sizeof($array) ; $i++) { 
			$departure[]=substr($datos,0,3);
			$vin[]=substr($datos,4,17);
			$portinDate[]=substr($datos,22,8);
			$code[]=substr($datos,31,3);
		}

		$cadena=implode(' ', $vin);
		$cadena1 = chunk_split($cadena, 18,"','");
		$vines=substr($cadena1,0,-2);


		$selPDI="SELECT dt.vin, dt.fechaEvento,dy.vin, rv.hora"
				." FROM alhistoricounidadestbl dt, alinstruccionesmercedestbl dy, altransaccionunidadtbl rv"
				." WHERE dt.vin in('".$vines.")" 
				." AND dt.centroDistribucion='CDTOL'"
				." AND dt.claveMovimiento='PI'"
				." AND dt.fechaEvento BETWEEN '".$fechaDesde."' AND '".$fechaHasta."'" //." AND rv.fechaMovimiento BETWEEN '2016-06-13' AND '2016-08-08'"
				." AND dt.vin=dy.vin"
				." AND dy.cveLoc='FT15'"
				." AND rv.tipoTransaccion='ACN'"              
				." AND dt.centroDistribucion= rv.centroDistribucion" 
				." AND dt.vin = rv.vin"
				." AND rv.claveMovimiento='PI'"
				." AND NOT EXISTS (SELECT *FROM altransaccionunidadtbl ab"
								." WHERE ab.tipoTransaccion='INV'"
				                ." AND ab.centroDistribucion='TRA'"
				                ." AND ab.vin = dt.vin"
				                ." AND ab.claveMovimiento='WK'"
				                ." AND ab.prodStatus='PI')";
		$rstSelPDI=fn_ejecuta_query($selPDI);
		//echo json_encode($selPDI);

		insertPDI($rstSelPDI);

	}

	function insertStorage($datos){

		echo json_encode($datos);

		$diasLib="SELECT nombre as diasLibres FROM cageneralestbl WHERE tabla='trap832' AND valor='1'";
		$rstDiasLib=fn_ejecuta_query($diasLib);

		$selTar3="SELECT nombre as tarifa3 FROM cageneralestbl WHERE tabla='trap832' AND valor='6'";
		$rstSelTar3=fn_ejecuta_query($selTar3);

		for ($i=0; $i <sizeof($datos['root']) ; $i++) { 
			$calculo1=substr($datos['root'][$i]['fechaMovimiento'],8,2)-substr($datos['root'][$i]['fechaEvento'],8,2);
			$calculo2=$calculo1-$rstDiasLib['root'][0]['diasLibres'];
			$calculo3=$calculo2*$rstSelTar3['root'][0]['tarifa3'];
			//echo $calculo3;

			
			$insertStor="INSERT INTO alfhktmp (concepto, vin, folio, marca, cveTran, puertoOrigen, destino, locCod, folioFactura," 
						." avanzada, delEnt, claveMovimiento, fechaStatus, horaStatus, costCod, workCode, importe, fechaAsignacion, horaAsignacion, "
						." diasalm, diaslib, diascus, costo, tractor, chipNum)"
						." VALUES ('S', '".
							$datos['root'][$i]['vin']."', '".
							$_REQUEST['folioTxt']."',"
							."'KMM',"
							." 'INS',"
							." 'TRA'," 
							."'...',"
							." '...',"
							." '...',"
							." '".substr($datos['root'][$i]['vin'],9,8)."'," 
							." '...',"
							."'AM'," 
							." '".$datos['root'][$i]['fechaEvento']."',"
							." '120000',"
							." 'ST',"
							."'STO',"
							." '".$rstSelTar3['root'][0]['tarifa3']."',"
							."'".$datos['root'][$i]['fechaMovimiento']."',"
							."'120000'," 
							."'".$calculo1."'," 
							."'".$rstDiasLib['root'][0]['diasLibres']."', "
							."'".$calculo2."', '".$calculo3."'"
							.",'0',"
							."'...')";
			$rstInsertStor=fn_ejecuta_query($insertStor);
		}

		$fechaArchivo=date("YmdHms");

		$storage="SELECT * FROM alfhktmp WHERE concepto='S'";
		$rstStorage=fn_ejecuta_query($storage);
		//echo json_encode($rstStorage);

		$folio=$rstStorage['root'][0]['folio'];

		//ARCHIVO DE SOPORTE PARA CONTABILIDAD
		$fileDir="C:/carbook/TRAP832shp/storage/STO_000".$folio."_".$fechaArchivo.".csv";
		$fileLog = fopen($fileDir, 'a+');	

		for ($i=0;$i < sizeof($rstStorage['root']);$i++) {

			if ($rstStorage['root'][$i]['diasalm']>3) {

	    		$encabezado2 = array(1  => "000".$rstStorage['root'][$i]['folio'].",",
					    			7  => $rstStorage['root'][$i]['marca'].",",
					    			11  =>$rstStorage['root'][$i]['puertoOrigen'].",",
					    			16 =>$rstStorage['root'][$i]['vin'].",",
					    			29  => date('YmdHms',strtotime($rstStorage['root'][$i]['fechaStatus'])) .",",
					    			38  => date('YmdHms',strtotime($rstStorage['root'][$i]['fechaAsignacion'])) .",",
					    			50  => $rstStorage['root'][$i]['costCod'].",",
					    			51 =>"00".$rstStorage['root'][$i]['diasalm'].",",
					    			52 =>"000".$rstStorage['root'][$i]['diaslib'].",",
					    			53 =>"00".$rstStorage['root'][$i]['diascus'].",  ",
					    			54 =>$rstStorage['root'][$i]['importe'].",  ",
					    			55 =>$rstStorage['root'][$i]['costo']);
					    			
	    		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			}
		}		
	} 
	
	function insertBasic($datos){

		$selTar2="SELECT nombre as tarifa2 FROM cageneralestbl WHERE tabla='trap832' AND valor='4'";
		$rstSelTar2=fn_ejecuta_query($selTar2);

		for ($i=0; $i <sizeof($datos['root']) ; $i++) { 
			$insertBas="INSERT INTO alfhktmp (concepto, vin, folio, marca, cveTran, puertoOrigen, destino, locCod, folioFactura," 
						." avanzada, delEnt, claveMovimiento, fechaStatus, horaStatus, costCod, workCode, importe, fechaAsignacion, horaAsignacion, "
						." diasalm, diaslib, diascus, costo, tractor, chipNum)"
						." VALUES ('B', '".
							$datos['root'][$i]['vin']."', '".
							$_REQUEST['folioTxt']."',"
							."'KMM',"
							." 'INV',"
							." 'TRA'," 
							."'...',"
							." '...',"
							." '...',"
							." '".substr($datos['root'][$i]['vin'],9,8)."'," 
							." '...',"
							."'PR'," 
							." '".$datos['root'][$i]['fechaEvento']."',"
							." '120000',"
							." 'BS',"
							."'101',"
							." '".$rstSelTar2['root'][0]['tarifa2']."',"
							."0,"
							."'120000'," 
							."0,"
							."0,"
							."0,"
							."0,"
							."0,"
							."'...')";
			$rstInsertBas=fn_ejecuta_query($insertBas);


		}

		$fechaArchivo=date("YmdHms");


		$basic="SELECT * FROM alfhktmp WHERE concepto='B'";
		$rstBasic=fn_ejecuta_query($basic);

		$folio=$rstBasic['root'][0]['folio'];

		//ARCHIVO DE SOPORTE PARA CONTABILIDAD
		$fileDir="C:/carbook/TRAP832shp/basic/BAS_000".$folio."_".$fechaArchivo.".csv";
		$fileLog = fopen($fileDir, 'a+');	


		for ($i=0;$i < sizeof($rstBasic['root']);$i++) {

	    		$encabezado2 = array(1  => "000".$rstBasic['root'][$i]['folio'].",",
					    			7  => $rstBasic['root'][$i]['marca'].",",
					    			11  =>$rstBasic['root'][$i]['puertoOrigen'].",",
					    			16 =>$rstBasic['root'][$i]['vin'].",",
					    			29  => date('YmdHms',strtotime($rstBasic['root'][$i]['fechaStatus'])) .",",
					    			50  => $rstBasic['root'][$i]['costCod'].",",
					    			54 =>$rstBasic['root'][$i]['importe']);
	    			
	    		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
		}
	}

	function insertPDI($datos){

		$selTar1="SELECT nombre as tarifa1 FROM cageneralestbl WHERE tabla='trap832' AND valor='5'";
		$rstSelTar1=fn_ejecuta_query($selTar1);

		for ($i=0; $i <sizeof($datos['root']) ; $i++) { 
			$insertPdi="INSERT INTO alfhktmp (concepto, vin, folio, marca, cveTran, puertoOrigen, destino, locCod, folioFactura," 
						." avanzada, delEnt, claveMovimiento, fechaStatus, horaStatus, costCod, workCode, importe, fechaAsignacion, horaAsignacion, "
						." diasalm, diaslib, diascus, costo, tractor, chipNum)"
						." VALUES ('P', '".
							$datos['root'][$i]['vin']."', '".
							$_REQUEST['folioTxt']."',"
							."'KMM',"
							." 'INV',"
							." 'TRA'," 
							."'...',"
							." '...',"
							." '...',"
							." '".substr($datos['root'][$i]['vin'],9,8)."'," 
							." '...',"
							."'PI'," 
							." '".$datos['root'][$i]['fechaEvento']."',"
							." '120000',"
							." 'WK',"
							." '".$datos['root'][$i]['costCod']."',"
							." '".$rstSelTar1['root'][0]['tarifa1']."',"
							."0,"
							."'120000'," 
							."0,"
							."0,"
							."0,"
							."0,"
							."0,"
							."'...')";

			$rstInsertPDI=fn_ejecuta_query($insertPdi);

		}

			$fechaArchivo=date("YmdHms");


		$PDI="SELECT * FROM alfhktmp WHERE concepto='P'";
		$rstPDI=fn_ejecuta_query($PDI);

		$folio=$rstPDI['root'][0]['folio'];

		//ARCHIVO DE SOPORTE PARA CONTABILIDAD
		$fileDir="C:/carbook/TRAP832shp/PDI/PIO_000".$folio."_".$fechaArchivo.".csv";
		$fileLog = fopen($fileDir, 'a+');	


		for ($i=0;$i < sizeof($rstPDI['root']);$i++) {

	    		$encabezado2 = array(1  => "000".$rstPDI['root'][$i]['folio'].",",
					    			7  => $rstPDI['root'][$i]['marca'].",",
					    			11  =>$rstPDI['root'][$i]['puertoOrigen'].",",
					    			16 =>$rstPDI['root'][$i]['vin'].",",
					    			29  => date('YmdHms',strtotime($rstPDI['root'][$i]['fechaStatus'])) .",",
					    			50  => $rstPDI['root'][$i]['costCod'].",",
					    			54 =>$rstPDI['root'][$i]['importe']);
	    			
	    		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			
		}
	}

	function getTxt($texts, $positions){
    	$text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = ($positions[$i]);
                $antLength = strlen($texts[$positions[$i]]);
            }
            $text .= out('s', ($positions[$i]-1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }
    function out($char, $times){
        $validChar = array(
                    "n" => PHP_EOL,
                    "t" => "\t",
                    "s" => " ");
        $chars = '';
        for ($i=0; $i < $times ; $i++) { 
            $chars .= $validChar[$char];
        }
        fseek($myfile, -20);   
        return $chars;
    }

    function facturacionStor(){

    	$folio=$_REQUEST['folioTxt'];

    	$selDatos="SELECT * FROM alfhktmp WHERE concepto='S' AND folio=".$folio;
    	$rstSelDatos=fn_ejecuta_query($selDatos);


    	$fechaArchivo=date("YmdHms");

		
		//ARCHIVO DE INTERFASE ALI
		$fileDir="C:/carbook/TRAP832shp/storage/".$rstSelDatos['root'][0]['marca']."_INS_".$fechaArchivo.".txt";
		$fileLog = fopen($fileDir, 'a+');	

	    		$encabezado1 = array(1  => "INSH ",
					    			7  => "TRA  ",
					    			11  => "GMX  ",
					    			16 => "INS",
					    			29  => $fechaArchivo);
	    			
		$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

		$selTotReg="SELECT COUNT(*) as totreg FROM alfhktmp WHERE folio =".$folio;
		$rstSelTotReg=fn_ejecuta_query($selTotReg);

        $selLastDate="SELECT MAX(fechaAsignacion) as lastdate FROM alfhktmp WHERE folio =".$folio;
        $rstSelLastDate=fn_ejecuta_query($selLastDate);
      
        $selTotCto="SELECT SUM(costo) as totcto FROM alfhktmp WHERE folio =".$folio;
        $rstSelTotCto=fn_ejecuta_query($selTotCto);	

        $totIVA=$rstSelTotCto['root'][0]['totcto']*0.16;
        $totIns=$rstSelTotCto['root'][0]['totcto']+$totIVA;
        $cveLoc='FT15';//TOL
        $factura=$_REQUEST['folioFacturaTxt'];

        if (strlen($factura)==1) {
	   			$ceros1='00000';
	   		}
	   		if (strlen($factura)==2) {
	   			$ceros1='0000';
	   		}
	   		if (strlen($factura)==3) {
	   			$ceros1='000';
	   		}
	   		if (strlen($factura)==4) {
	   			$ceros1='00';
	   		}
	   		if (strlen($factura)==5) {
	   			$ceros1='0';
	   		}

        $encabezado2 = array(1  => "INSB1",
			    			7  => $ceros1.$factura,
			    			11 => '              ',
			    			16 => substr($fechaArchivo,0,8),
			    			18 => $cveLoc." ",
			    			22 => "TRA  ",
			    			25 => "KMM  ",
			    			28 => "ST",
			    			30 => "            ",
			    			35 => date('YmdHms',strtotime($rstSelLastDate['root'][0]['lastdate'])),
			    			40 => "               ",
			    			45 => "MXN+",
			    			50 => $totIns*100,
			    			60 => $rstSelTotCto['root'][0]['totcto']*100,
			    			70 => $totIVA*100,
			    			80 => "STORAGE FEE-PORT",
			    			95 => "                                 ");
	    			
		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

	   		$cons=1;

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 

	    	if (strlen($cons)==1) {
	   			$ceros='00000';
	   		}
	   		if (strlen($cons)==2) {
	   			$ceros='0000';
	   		}
	   		if (strlen($cons)==3) {
	   			$ceros='000';
	   		}


   		    if ($rstSelDatos['root'][$i]['diasalm']>3) {

   		    	$fechastat=date('YmdHms',strtotime($rstSelDatos['root'][$i]['fechaStatus']));
   		    	$fechaasig=date('YmdHms',strtotime($rstSelDatos['root'][$i]['fechaAsignacion']));

		    	$detalle = array(1  => "INSB2",
				    			7  => $ceros1.$factura,
				    			11 => '              ',
				    			16 => $ceros,
				    			17 => $cons,
				    			18 => $rstSelDatos['root'][$i]['vin'],
				    			22 => substr($fechastat,0,8),
				    			25 => '        ',
				    			28 => substr($fechastat,0,8),
				    			30 => substr($fechaasig,0,8),
				    			35 => "000".$rstSelDatos['root'][$i]['diasalm'],
				    			40 => "000".$rstSelDatos['root'][$i]['diaslib'],
				    			45 => "000".$rstSelDatos['root'][$i]['diascus'],
				    			50 => "000".$rstSelDatos['root'][$i]['importe']*100,
				    			60 => "00000".$rstSelDatos['root'][$i]['costo']*100000,
				    			70 => "0000000");
		    			
				$text = getTxt($detalle, array_keys($detalle)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			    $cons++;

			}

		}

		$trailer = array(1  => "INST ",
		    			7  => "000",
		    			10 =>$cons+2);
		    			
		$text = getTxt($trailer, array_keys($trailer)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 
  		    if ($rstSelDatos['root'][$i]['diasalm']>3) {

		    	$insRv1="INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion,folio, vin, fechaGeneracionUnidad, claveMovimiento,"
		    		." fechaMovimiento, prodStatus) "
					." VALUES ('"
					.$rstSelDatos['root'][$i]['cveTran']."','"
					.$rstSelDatos['root'][$i]['puertoOrigen']."','"
					.$factura."','"
					.$rstSelDatos['root'][$i]['vin']."','"
					.$rstSelDatos['root'][$i]['fechaStatus']."','"
					.$rstSelDatos['root'][$i]['costCod']."','"
					.$fechaArchivo."','"
					.$rstSelDatos['root'][$i]['claveMovimiento']."')";
				$rstInsRv1=fn_ejecuta_query($insRv1);
			}
	    }
    }

    function facturacionBasic(){

    	$folio=$_REQUEST['folioTxt'];

    	$selDatos="SELECT * FROM alfhktmp WHERE concepto='B' AND folio=".$folio;
    	$rstSelDatos=fn_ejecuta_query($selDatos);

    	$fechaArchivo=date("YmdHms");

		
		//ARCHIVO DE INTERFASE ALI
		$fileDir="C:/carbook/TRAP832shp/basic/".$rstSelDatos['root'][0]['marca']."_INV_".$fechaArchivo.".txt";
		$fileLog = fopen($fileDir, 'a+');	

	    		$encabezado1 = array(1  => "INVH ",
					    			7  => "TRA ",
					    			11  => "GMX ",
					    			16 => "INV",
					    			29  => $fechaArchivo);
	    			
		$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

        $factura=$_REQUEST['folioFacturaTxt'];

	   		$cons=1;

	   		

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 

	    	if (strlen($cons)==1) {
	   			$ceros='00000';
	   		}
	   		if (strlen($cons)==2) {
	   			$ceros='0000';
	   		}
	   		if (strlen($cons)==3) {
	   			$ceros='000';
	   		}


		    	$detalle = array(1  => "000".$factura,
				    			7  => "              ",
				    			10 => $ceros.$cons,
				    			16 => substr($fechaArchivo,0,8),
				    			22 => $rstSelDatos['root'][$i]['puertoOrigen'],
				    			25 => "     ",
				    			28 => "TRA  ",
				    			30 => "KMM  ",
				    			35 => $rstSelDatos['root'][$i]['vin'],
				    			40 => $rstSelDatos['root'][$i]['costCod']."    ",
				    			45 => $rstSelDatos['root'][$i]['workCode']."   ",
				    			50 => date('YmdHms',strtotime($rstSelDatos['root'][$i]['fechaStatus'])),
				    			60 => " MXN",
				    			70 => "0000".$rstSelDatos['root'][$i]['importe']*10000,
				    			75 => "00100+",
				    			77 => "0000".$rstSelDatos['root'][$i]['importe']*10000,
				    			80 => "00000".$rstSelDatos['root'][$i]['importe']*0.16*10000,
				    			90 => substr($fechaArchivo,0,8));
		    			
				$text = getTxt($detalle, array_keys($detalle)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			 	$cons++;

		}

		$trailer = array(1  => "INVT ",
		    			7  => "000",
		    			10 =>$cons+1);
		    			
		$text = getTxt($trailer, array_keys($trailer)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 

		    	$insRv1="INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion,folio, vin, fechaGeneracionUnidad, claveMovimiento,"
		    		." fechaMovimiento, prodStatus) "
					." VALUES ('"
					.$rstSelDatos['root'][$i]['cveTran']."','"
					.$rstSelDatos['root'][$i]['puertoOrigen']."','"
					.$factura."','"
					.$rstSelDatos['root'][$i]['vin']."','"
					.$rstSelDatos['root'][$i]['fechaStatus']."','"
					.$rstSelDatos['root'][$i]['costCod']."','"
					.$fechaArchivo."','"
					.$rstSelDatos['root'][$i]['claveMovimiento']."')";
				$rstInsRv1=fn_ejecuta_query($insRv1);
	    }
    }

    function facturacionPDI(){

    	$folio=$_REQUEST['folioTxt'];

    	$selDatos="SELECT * FROM alfhktmp WHERE concepto='P' AND folio=".$folio;
    	$rstSelDatos=fn_ejecuta_query($selDatos);

    	$fechaArchivo=date("YmdHms");

		
		//ARCHIVO DE INTERFASE ALI
		$fileDir="C:/carbook/TRAP832shp/PDI/".$rstSelDatos['root'][0]['marca']."_INV_".$fechaArchivo.".txt";
		$fileLog = fopen($fileDir, 'a+');	

	    		$encabezado1 = array(1  => "INVH ",
					    			7  => "TRA ",
					    			11  => "GMX ",
					    			16 => "INV",
					    			29  => $fechaArchivo);
	    			
		$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

        $factura=$_REQUEST['folioFacturaTxt'];

	   		$cons=1;

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 

	    	if (strlen($cons)==1) {
	   			$ceros='00000';
	   		}
	   		if (strlen($cons)==2) {
	   			$ceros='0000';
	   		}
	   		if (strlen($cons)==3) {
	   			$ceros='000';
	   		}
		    	$detalle = array(1  => "000".$factura,
				    			7  => "              ",
				    			10 => $ceros.$cons,
				    			16 => substr($fechaArchivo,0,8),
				    			22 => $rstSelDatos['root'][$i]['puertoOrigen'],
				    			25 => "     ",
				    			28 => "TRA  ",
				    			30 => "KMM  ",
				    			35 => $rstSelDatos['root'][$i]['vin'],
				    			40 => $rstSelDatos['root'][$i]['costCod']."    ",
				    			45 => $rstSelDatos['root'][$i]['workCode']."   ",
				    			50 => date('YmdHms',strtotime($rstSelDatos['root'][$i]['fechaStatus'])),
				    			60 => " MXN",
				    			70 => "000".$rstSelDatos['root'][$i]['importe']*10000,
				    			75 => "00100+",
				    			77 => "000".$rstSelDatos['root'][$i]['importe']*10000,
				    			80 => "0000".$rstSelDatos['root'][$i]['importe']*0.16*10000,
				    			90 => substr($fechaArchivo,0,8));
		    			
				$text = getTxt($detalle, array_keys($detalle)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			 	$cons++;

		}

		$trailer = array(1  => "INVT ",
		    			7  => "000",
		    			10 =>$cons+1);
		    			
		$text = getTxt($trailer, array_keys($trailer)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

	    for ($i=0; $i <sizeof($rstSelDatos['root']) ; $i++) { 

		    	$insRv1="INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion,folio, vin, fechaGeneracionUnidad, claveMovimiento,"
		    		." fechaMovimiento, prodStatus) "
					." VALUES ('"
					.$rstSelDatos['root'][$i]['cveTran']."','"
					.$rstSelDatos['root'][$i]['puertoOrigen']."','"
					.$factura."','"
					.$rstSelDatos['root'][$i]['vin']."','"
					.$rstSelDatos['root'][$i]['fechaStatus']."','"
					.$rstSelDatos['root'][$i]['costCod']."','"
					.$fechaArchivo."','"
					.$rstSelDatos['root'][$i]['claveMovimiento']."')";
				$rstInsRv1=fn_ejecuta_query($insRv1);
	    }
    }

    function confirmacionStorage(){

    	$folio=$_REQUEST['folioTxt'];
        $factura=$_REQUEST['folioFacturaTxt'];


    	$selDatos="SELECT * FROM alfhktmp WHERE concepto='S' AND folio=".$folio;
    	$rstSelDatos=fn_ejecuta_query($selDatos);
    	//echo json_encode($rstSelDatos);

    	$selRv1="SELECT * FROM altransaccionunidadtbl WHERE folio ='".$factura."' AND tipoTransaccion='INS'";
    	$rstSelRv1=fn_ejecuta_query($selRv1);
    	echo json_encode($rstSelRv1);

    	$selCon="SELECT COUNT(*) as contfhk FROM alfhktmp WHERE folio =".$folio
				." AND concepto = 'S'"
				." AND diasalm >3";
		$rstSelCon=fn_ejecuta_query($selCon);
		//echo json_encode($rstSelCon);

		$selCon1="SELECT COUNT(*) as contrv1 FROM altransaccionunidadtbl WHERE tipoTransaccion ='".$rstSelDatos['root'][0]['cveTran']."'"
				." AND claveMovimiento   ='ST'"
				." AND folio='".$factura."'";
		$rstSelCon1=fn_ejecuta_query($selCon1);
		//echo json_encode($rstSelCon1);

		if ($rstSelCon['root'][0]['contfhk']==$rstSelCon1['root'][0]['contrv1']) {
			$delStorage="DELETE FROM alfhktmp WHERE folio='".$folio."'"." AND concepto='S'";
			$rstDelStorage=fn_ejecuta_query($delStorage);
		}
    }

    function confirmacionBasic(){
		$folio=$_REQUEST['folioTxt'];
        $factura=$_REQUEST['folioFacturaTxt'];


    	$selDatos="SELECT * FROM alfhktmp WHERE concepto='B' AND folio=".$folio;
    	$rstSelDatos=fn_ejecuta_query($selDatos);
    	//echo json_encode($rstSelDatos);

    	$selRv1="SELECT * FROM altransaccionunidadtbl WHERE folio ='".$factura."' AND tipoTransaccion='INV' AND prodstatus='PR'";
    	$rstSelRv1=fn_ejecuta_query($selRv1);
    	//echo json_encode($rstSelRv1);

    	$selCon="SELECT COUNT(*) as contfhk FROM alfhktmp WHERE folio =".$folio
				." AND concepto = 'B'";
		$rstSelCon=fn_ejecuta_query($selCon);
		//echo json_encode($rstSelCon);

		$selCon1="SELECT COUNT(*) as contrv1 FROM altransaccionunidadtbl WHERE tipoTransaccion ='".$rstSelDatos['root'][0]['cveTran']."'"
				." AND claveMovimiento   ='BS'"
				." AND folio='".$factura."'";
		$rstSelCon1=fn_ejecuta_query($selCon1);
		//echo json_encode($rstSelCon1);

		if ($rstSelCon['root'][0]['contfhk']==$rstSelCon1['root'][0]['contrv1']) {
			$delStorage="DELETE FROM alfhktmp WHERE folio='".$folio."'"." AND concepto='B'";
			$rstDelStorage=fn_ejecuta_query($delStorage);
		}   
    }

    function confirmacionPDI(){
		$folio=$_REQUEST['folioTxt'];
        $factura=$_REQUEST['folioFacturaTxt'];


    	$selDatos="SELECT * FROM alfhktmp WHERE concepto='P' AND folio=".$folio;
    	$rstSelDatos=fn_ejecuta_query($selDatos);
    	//echo json_encode($rstSelDatos);

    	$selRv1="SELECT * FROM altransaccionunidadtbl WHERE folio ='".$factura."' AND tipoTransaccion='INV' AND prodstatus='PI'";
    	$rstSelRv1=fn_ejecuta_query($selRv1);
    	//echo json_encode($rstSelRv1);

    	$selCon="SELECT COUNT(*) as contfhk FROM alfhktmp WHERE folio =".$folio
				." AND concepto = 'P'";
		$rstSelCon=fn_ejecuta_query($selCon);
		echo json_encode($rstSelCon);

		$selCon1="SELECT COUNT(*) as contrv1 FROM altransaccionunidadtbl WHERE tipoTransaccion ='".$rstSelDatos['root'][0]['cveTran']."'"
				." AND claveMovimiento   ='WK'"
				." AND folio='".$factura."'";
		$rstSelCon1=fn_ejecuta_query($selCon1);

		if ($rstSelCon['root'][0]['contfhk']==$rstSelCon1['root'][0]['contrv1']) {
			$delStorage="DELETE FROM alfhktmp WHERE folio='".$folio."'"." AND concepto='P'";
			$rstDelStorage=fn_ejecuta_query($delStorage);
		}   
    }

    function cancelaPreFacST(){
		$folio=$_REQUEST['folioTxt'];

		$selTra="SELECT * FROM altransaccionunidadtbl WHERE folio=".$folio." AND tipoTransaccion='INS'";
		$rstSelTra=fn_ejecuta_query($selTra);

		if ($rstSelTra['records'] == '0') {
			$delPreFac="DELETE FROM alfhktmp WHERE folio=".$folio." AND concepto='S'";
			$rstDelPreFac=fn_ejecuta_query($delPreFac);
		}
    }

    function cancelaPreFacBS(){
		$folio=$_REQUEST['folioTxt'];

		$selTra="SELECT * FROM altransaccionunidadtbl WHERE folio=".$folio." AND tipoTransaccion='INV'";
		$rstSelTra=fn_ejecuta_query($selTra);

		if ($rstSelTra['records'] != '0') {
			$delPreFac="DELETE FROM alfhktmp WHERE folio=".$folio." AND concepto='B'";
			$rstDelPreFac=fn_ejecuta_query($delPreFac);
		}
    }

     function cancelaPreFacPDI(){
		$folio=$_REQUEST['folioTxt'];

		$selTra="SELECT * FROM altransaccionunidadtbl WHERE folio=".$folio." AND tipoTransaccion='INV'";
		$rstSelTra=fn_ejecuta_query($selTra);

		if ($rstSelTra['records'] == '0') {
			$delPreFac="DELETE FROM alfhktmp WHERE folio=".$folio." AND concepto='P'";
			$rstDelPreFac=fn_ejecuta_query($delPreFac);
		}
    }

?>