<?php
	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");

		date_default_timezone_set('America/Mexico_City');
		$hoy = getdate();
		$fecha = date('Y-m-d', $hoy[0]);

	    $fechaAnt = strtotime ( '-15 day' , strtotime (date("Y-m-d")) ) ;
  	    $fechaAnt = date ( 'Y-m-d' , $fechaAnt );

  	    $selALB="SELECT   au.avanzada, rv.hora"
				." FROM alunidadestbl au, alhistoricounidadestbl dt , alinstruccionesmercedestbl dy, altransaccionunidadtbl rv, catarifastbl ta"
				." WHERE dt.vin = au.vin"
				." AND dt.centroDistribucion IN ('CDTOL','CDAGS','CDSAL','CDVER','CDLZC','CDANG','CDSFE')"
				." AND dt.fechaEvento BETWEEN '2015-07-20' AND '2015-07-21'" //es un mes pero es necesario parametrizar el numero de dias en caGenralesTbl
				." AND dt.claveMovimiento = 'OM'"
				." AND ta.tarifa IN('01','00')"
				." AND dt.vin = dy.vin"
				." AND  dy.cveStatus = 'AK'"
				." AND rv.vin = dy.vin"
				." AND rv.tipoTransaccion = 'TLP'"
				." ORDER BY hora";
		$rstALB=fn_ejecuta_query($selALB);
		//echo json_encode($rstALB);

		$selALBTLP="INSERT INTO altransaccionUnidadestmp (centroDistribucion,folio, vin, avanzada, fechaEvento, claveMovimiento, distribuidor, tarifa, viaje,compania,tractor,fechaTalon, llave,chipNum,cveLoc )"
				." SELECT   rv.centroDistribucion, rv.folio, rv.vin, au.avanzada, dt.fechaEvento, rv.claveMovimiento,au.distribuidor,ta.tarifa,vt.viaje,tr.compania,tr.tractor,tl.fechaEvento as fechaTalon, 'i816' as llave,dy.chipNum,dy.cveLoc"
				." FROM alunidadestbl au, alhistoricounidadestbl dt ,catarifastbl ta ,alinstruccionesmercedestbl dy ,"
				."   altransaccionunidadtbl rv,trviajestractorestbl vt ,trunidadesdetallestalonestbl ts, trtalonesviajestbl tl ,catractorestbl tr"
				." WHERE au.vin = dt.vin"
				." AND dt.centroDistribucion  IN('CDVER','CDSFE','CDAGS','CDTOL','CDLZC','CDSAL','CDANG')"
				." AND dt.claveMovimiento IN('OM','OK','ER')"
				." AND dt.idTarifa=ta.idTarifa"
				." AND ta.tarifa IN('01','00')"
				." AND dt.fechaEvento BETWEEN '2016-06-01' AND '2016-06-30'" 
				." AND dt.vin = dy.vin"
				." AND dy.cveStatus = 'AK'"
				." AND dt.vin = rv.vin"
				." AND rv.tipoTransaccion ='TLP'"
				." AND rv.centroDistribucion = vt.centroDistribucion"
				." AND dt.vin = ts.vin"
				." AND ts.estatus = 'R'"
				." AND tl.idTalon = ts.idTalon"
				." AND tl.idViajeTractor = vt.idViajeTractor" 
				." AND vt.idTractor = tr.idTractor ";
				//." AND tl.fechaEvento = rv.fechaGeneracionUnidad";
		$rstALBTLP=fn_ejecuta_query($selALBTLP);
		//echo json_encode($rstALBTLP);

			$sqlDatosStr = "select distinct * from altransaccionUnidadestmp where llave='i816'";
			$DatosRst = fn_ejecuta_query($sqlDatosStr);

			$sqlInsertStr="INSERT INTO altransaccionunidadtbl (tipoTransaccion,centroDistribucion,folio,VIN, fechaGeneracionUnidad, claveMovimiento,fechaMovimiento, prodstatus, fecha, hora)"
						." SELECT 'ALB' as tipoTransaccion,centroDistribucion,folio,VIN, NOW() as fechaGeneracionUnidad, claveMovimiento,fechaEvento, NULL AS prodStatus,null as fecha, ' ' as hora"
						." FROM altransaccionunidadestmp"
						." WHERE LLAVE = 'i816'";
			$InsertRst=fn_ejecuta_query($sqlInsertStr);

			for ($i=0; $i <sizeof($DatosRst['root']) ; $i++) { 
				
					$sqlMarcaStr = "SELECT  ge.nombre"
								 ." FROM  casimbolosunidadestbl su, cageneralestbl ge, alunidadestbl au"
								." WHERE au.vin ='".$DatosRst['root'][$i]['vin']."'" 
								." AND au.simboloUnidad = su.simboloUnidad"
								." AND su.marca = ge.valor"
								." AND ge.tabla = 'caSimboloUnidadesTbl'"
								." AND ge.columna = 'marca'";
					$MarcaRst = fn_ejecuta_query($sqlMarcaStr);
					//echo json_encode($MarcaRst);



					switch ($MarcaRst['root'][$i]['nombre'] ) {
						case 'HMM' :
		     				$txt =$MarcaRst['root'][$i]['nombre'];
							generatxt($DatosRst,$rstALB,$rstALBTLP,$txt);
							break;
						case 'KMM':
							$txt =$MarcaRst['root'][$i]['nombre'];
							generatxt($DatosRst,$rstALB,$rstALBTLP,$txt);
							break;
						default:
							//echo "ESTE VIN NO PERTENECE A KIA NI HYUNDAI";
							break;
					}
			}



		function generatxt($DatosRst,$rstALB,$rstALBTLP,$txt){

	    	$fecha = date('Ymd');
	    	$hora = date("His");
			
			//$directorio = '../../carbookBck/generacion_550/';
			$directorio ="C:carbook/i816/";
			$inicioFile = $txt."_ALB_".$fecha.$hora.".txt";
			$archivo = fopen($directorio.$inicioFile,"w");

			//encabezado
			fwrite($archivo,"ALBH"." "."TRA"."  "."GMX"."  "."ALB".$fecha.$hora.PHP_EOL);

			//detalle
			for ($i=0; $i <sizeof($DatosRst['root']) ; $i++) { 
				

				fwrite($archivo,"ALB"."  ".$fecha.substr($DatosRst['root'][$i]['fechaEvento'],11,2).substr($DatosRst['root'][$i]['fechaEvento'],14,2).substr($DatosRst['root'][$i]['fechaEvento'],17,2));
				fwrite($archivo,$DatosRst['root'][$i]['vin']."P".$DatosRst['root'][$i]['cveLoc']." "."D".$DatosRst['root'][$i]['distribuidor'].$DatosRst['root'][$i]['chipNum']."  "."000000000000000");
				fwrite($archivo,substr($DatosRst['root'][$i]['fechaEvento'],0,4).substr($DatosRst['root'][$i]['fechaEvento'],5,2).substr($DatosRst['root'][$i]['fechaEvento'],8,2)."00");
				fwrite($archivo,$DatosRst['root'][$i]['folio']."000".$DatosRst['root'][$i]['tractor']."0906".$DatosRst['root'][$i]['distribuidor'].PHP_EOL);
		

				$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='EK' WHERE vin='".$DatosRst['root'][$i]['vin']."'"." AND cveStatus='AK'";
				$rstUpt=fn_ejecuta_query($uptdy);	

				$uptdy1="UPDATE alinstruccionesmercedestbl SET cveStatus='3K' WHERE vin='".$DatosRst['root'][$i]['vin']."'"." AND cveStatus in ('1K,2K')";
				$rstUpt1=fn_ejecuta_query($uptdy1);	
			}


			//fin de archivo
			$long=(sizeof($DatosRst['root'])+2);
			fwrite($archivo,"ALBT"."00000".$long.PHP_EOL);

			fclose($archivo);

			

	    	}


?>