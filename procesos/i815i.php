<?php
	 // INTERFAZ 815
	 // Archivos a generar 
	 // este proceso genera movimientos de los eventos de asignacion (AM) para unidades marca Hyundai y Kia.

	

	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");

		date_default_timezone_set('America/Mexico_City');
		$hoy = getdate();
		$fecha = date('Y-m-d', $hoy[0]);

	    $fechaAnt = strtotime ( '-15 day' , strtotime (date("Y-m-d")) ) ;
  	    $fechaAnt = date ( 'Y-m-d' , $fechaAnt );

		
  	   

  	    //PRIMERA APLICACION
		$sqlDetalleUNidadesStr =/*"INSERT INTO altransaccionUnidadestmp ( centroDistribucion, vin, avanzada, fechaEvento, claveMovimiento, distribuidor, tarifa, viaje, compania, tractor, fechaTalon, llave ) "*/
								" SELECT  hu.centroDistribucion, hu.vin, au.avanzada, hu.fechaEvento, hu.claveMovimiento, hu.distribuidor, ta.tarifa, vt.viaje,tr.compania,tr.tractor,tl.fechaEvento as fechaTalon, 'i815' as llave "
								." FROM alhistoricounidadestbl hu, alunidadestbl au, catarifastbl ta, trviajestractorestbl vt, trtalonesviajestbl tl,trunidadesdetallestalonestbl ts, catractorestbl tr "
								." WHERE hu.vin = au.vin "
								." AND hu.distribuidor= au.distribuidor"
								." AND hu.fechaEvento between '2015-12-10 17:30:00' AND '2015-12-10 17:31:00' "
								." AND hu.idtarifa = ta.idtarifa "
								." AND hu.claveMovimiento = 'AM'"
								." AND hu.centroDistribucion IN ('CDVER','CDLZC','CDTOL') "
								." AND ta.tarifa IN('01','00') "
								." AND hu.vin IN (SELECT dy.vin FROM alinstruccionesmercedestbl dy"
														 ." WHERE dy.vin = hu.vin "
															 ." AND dy.cveStatus in('1H','1K')"
															 ." AND dy.cveDisEnt NOT IN  (SELECT hk.clavePuerto FROM capuertoshktbl hk WHERE dy.cveDisEnt = hk.clavePuerto)) "
								." AND hu.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='TLR') "
								." AND au.vin NOT IN (SELECT tmp.vin FROM altransaccionunidadestmp tmp WHERE tmp.llave = 'i815' AND au.vin = tmp.vin) "
								." AND ts.idTalon = tl.idTalon"
								." AND tl.idViajeTractor = vt.idViajeTractor "
								." AND vt.idTractor = tr.idTractor"
								." AND hu.vin = ts.vin " 
								." GROUP BY vt.viaje,tr.compania,tr.tractor"
								." ORDER BY au.avanzada, hu.fechaEvento";
		 $DetalleUnidadesRst = fn_ejecuta_query($sqlDetalleUNidadesStr);
		 	//echo json_encode($DetalleUNidadesStr);
		 


		 //SEGUNDA APLICACION 

		 $sqlSegundoDetalleStr = //"INSERT INTO altransaccionUnidadestmp (centroDistribucion, vin, avanzada, fechaEvento, claveMovimiento, distribuidor, tarifa, viaje, compania, tractor, fechaTalon, llave ) "
								" SELECT hu.centroDistribucion, hu.vin, au.avanzada, hu.fechaEvento, hu.claveMovimiento, hu.distribuidor, ta.tarifa, vt.viaje,tr.compania,tr.tractor,tl.fechaEvento as fechaTalon, '2' as llave "
								." FROM alhistoricounidadestbl hu, alunidadestbl au, catarifastbl ta, trviajestractorestbl vt, trtalonesviajestbl tl,trunidadesdetallestalonestbl ts, catractorestbl tr "
								." WHERE hu.vin = au.vin"
								." AND hu.fechaEvento between '2015-01-20' AND '2015-01-24' "
								." AND hu.idtarifa = ta.idtarifa "
								." AND hu.claveMovimiento = 'AM' "
								." AND hu.centroDistribucion IN ('CDVER','CDLZC') "
								." AND ta.tarifa IN('01','00') "
								." AND hu.vin IN (SELECT dy.vin FROM alinstruccionesmercedestbl dy "
								 	 				." WHERE dy.vin = hu.vin "
								 		 				." AND dy.cveStatus ='PH'"
								                        ." AND dy.cveDisEnt NOT IN  (SELECT hk.clavePuerto FROM capuertoshktbl hk WHERE dy.cveDisEnt = hk.clavePuerto))"
								." AND hu.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht "
								  							." WHERE ht.tipoTransaccion ='TLP') "
								." AND ts.idTalon = tl.idTalon "
								." AND tl.idViajeTractor = vt.idViajeTractor "
								." AND vt.idTractor = tr.idTractor "
								." AND hu.vin = ts.vin "
								." AND tr.compania in ('CR','TR','ST','TC')"
								." AND tl.tipoDocumento = 'R'  AND tl.tipoDocumento != 'X'"
								." AND  hu.vin  in  (SELECT dy.vin FROM alinstruccionesmercedestbl dy "
								  	 				." WHERE dy.vin = hu.vin "
								  		 				." AND dy.cveStatus IN('1H','2H'))"
								." AND hu.claveMovimiento=tl.claveMovimiento"
								." AND hu.fechaEvento = tl.fechaEvento"
								." GROUP BY vt.viaje,tr.compania,tr.tractor"
								." ORDER BY au.avanzada, hu.fechaEvento";
			$SegundoUnidadesRst = fn_ejecuta_query($sqlSegundoDetalleStr);

		
			//TERCERA APLICACION

			$sqlTercerDetalleStr="INSERT INTO altransaccionUnidadestmp (centroDistribucion, vin, avanzada, fechaEvento, claveMovimiento, distribuidor, tarifa, viaje, compania, tractor, fechaTalon, llave ) "
								." SELECT hu.centroDistribucion, hu.vin, au.avanzada, hu.fechaEvento, hu.claveMovimiento, hu.distribuidor,ta.tarifa, vt.viaje,tr.compania,tr.tractor,tl.fechaEvento as fechaTalon, 'i815' as llave"
								." FROM alhistoricounidadestbl hu,alunidadestbl au, catarifastbl ta ,trunidadesdetallestalonestbl ts ,trtalonesviajestbl tl,trviajestractorestbl vt, catractorestbl tr"
								." WHERE hu.vin = au.vin "
								." AND hu.fechaEvento between '2016-06-01' AND '2016-06-30' "
								." AND hu.idtarifa = ta.idtarifa "
								." AND hu.claveMovimiento = 'AM' "
								." AND hu.centroDistribucion IN ('CDVER','CDLZC','CDTOL') "
								." AND ta.tarifa IN('01','00') "
								." AND hu.vin IN (SELECT dy.vin FROM alinstruccionesmercedestbl dy "
												." WHERE dy.vin = hu.vin AND dy.cveStatus ='PK'"
								                ." AND dy.cveDisEnt NOT IN (SELECT hk.clavePuerto FROM capuertoshktbl hk "
																			." WHERE dy.cveDisEnt = hk.clavePuerto)) "
								." AND hu.vin = ts.vin "
								." AND ts.idTalon = tl.idTalon "
								." AND tl.idViajeTractor = vt.idViajeTractor "
								." AND vt.idTractor = tr.idTractor "
								." AND tr.compania in ('CR','TR','ST','TC') "
								." AND tl.tipoDocumento = 'R' "
								." AND tl.tipoDocumento != 'X'"
								." AND hu.vin  in (SELECT ht.vin FROM altransaccionunidadtbl ht "
													." WHERE ht.tipoTransaccion ='TLP')"
								                    ." AND NOT exists (SELECT dy.vin FROM alinstruccionesmercedestbl dy "
																		." WHERE dy.vin = hu.vin "
								                                        ." AND dy.cveStatus IN('1K','2K')) "
								." ORDER BY au.avanzada, hu.fechaEvento";
			$TerceraUnidadesRst = fn_ejecuta_query($sqlTercerDetalleStr);
			echo json_encode($TerceraUnidadesRst);

			$sqlDatosStr = "select distinct * from altransaccionUnidadestmp where llave='i815'";
			$DatosRst = fn_ejecuta_query($sqlDatosStr);
			//echo json_encode($DatosRst);
						
			
			$sqlInsertStr="INSERT INTO altransaccionunidadtbl (tipoTransaccion,centroDistribucion,folio,VIN, fechaGeneracionUnidad, claveMovimiento,fechaMovimiento, prodstatus, fecha, hora)"
						." SELECT tipoTransaccion,centroDistribucion,folio,VIN, NOW() as fechaGeneracionUnidad, claveMovimiento,fechaEvento, NULL AS prodStatus,null as fecha, ' ' as hora"
						." FROM altransaccionunidadestmp"
						." WHERE LLAVE = 'i815'";
			$InsertRst=fn_ejecuta_query($sqlInsertStr);
			

			$sqlPatiosStr = "SELECT * FROM capuertoshktbl";
			$PatiosRst = fn_ejecuta_query($sqlPatiosStr);

			

			


	    	function generatxt($DatosRst,$FolioRst,$PatiosRst,$txt){

	    

			for ($i=0; $i <count($DatosRst['root']) ; $i++) { 

				$sqlFolioStr = "select * from trfoliostbl where tipoDocumento='H' and compania='".$DatosRst['root'][0]['compania'] ."'"." ". "and centroDistribucion ='".$DatosRst['root'][0]['centroDistribucion']."'";
				$FolioRst = fn_ejecuta_query($sqlFolioStr);

				$folio = $FolioRst['root'][0]['folio'] +1;

				$sqlActualizaFolioStr="UPDATE trfoliostbl set folio =". $folio." WHERE centroDistribucion ='".$DatosRst['root'][0]['centroDistribucion']."'";
				$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);


				$sqlMarcaStr = "SELECT  ge.nombre"
						 ." FROM  casimbolosunidadestbl su, cageneralestbl ge, alunidadestbl au"
						." WHERE au.vin ='".$DatosRst['root'][$i]['vin']."'" 
						." AND au.simboloUnidad = su.simboloUnidad"
						." AND su.marca = ge.valor"
						." AND ge.tabla = 'caSimboloUnidadesTbl'"
						." AND ge.columna = 'marca'";
				$MarcaRst = fn_ejecuta_query($sqlMarcaStr);
				//echo json_encode($MarcaRst);s

				switch ($MarcaRst['root'][$i]['nombre'] ) {
					case 'HMM' :
	     				$txt =$MarcaRst['root'][$i]['nombre'];
						generatxt($DatosRst,$FolioRst,$PatiosRst,$txt);
						break;
					case 'KMM':
						$txt =$MarcaRst['root'][$i]['nombre'];
						generatxt($DatosRst,$FolioRst,$PatiosRst,$txt);

						break;
					
					default:
						//echo "ESTE VIN NO PERTENECE A KIA NI HYUNDAI";
						break;
				}

					$fecha = date('Ymd');
					$hora = date("His");
					
					//$directorio = '//10.1.2.122/servidores/apache/htdocs/archivos/i815/';
					$directorio ="C:carbook/i816/";
					$inicioFile = $txt."_TLP_".$fecha.$hora.".txt";
					$archivo = fopen($directorio.$inicioFile,"w");



					//ARCHIVO TLP REGISTRO 1
					fwrite($archivo,"TLPH"." "."TRA"." "."GMX"." "."TLP".$fecha.$hora.PHP_EOL);
					//REGISTRO 2
					

					fwrite($archivo,"TLP"."  ".$PatiosRst['root'][$i]['origen']."  ".$PatiosRst['root'][$i]['clavePuerto'].$DatosRst['root'][$i]['vin'].$DatosRst['root'][$i]['centroDistribucion'].substr($DatosRst['root'][$i]['fechaevento'],0,4).substr($DatosRst['root'][$i]['fechaevento'],5,2).substr($DatosRst['root'][$i]['fechaevento'],8,2).$FolioRst['root'][$i]['folio']);
					fwrite($archivo,"09"."N"."0000000000".PHP_EOL);
			}

			$lineas = count($DatosRst['root'])+2;
			//REGISTRO 3
			fwrite($archivo,"TLPT"." "."00000".$lineas.PHP_EOL); 

	    	fclose($archivo);
	    	}


?>