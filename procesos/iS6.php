<?php

	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");


	date_default_timezone_set('America/Mexico_City');

   	ejecutaS6();

    function ejecutaS6(){	

		$sqlUnidS6=	"SELECT case when substr(dy.cveDisFac,-5) in('77831','77828','77827','77848','77829','77830','72148','31112') then 'G' else 'M' end as shipper, dy.vin, '' as orderNuzmber, 'S6' as eventCode , substring(hu.fechaEvento,1,10) as  vehicleDate, ".
							"substring(dy.cveDisFac,1,2) as dealer,'LC' as portCode, '' as portDischarge, dy.currency,'0' as volume ,substring(dy.cveDisFac,3) as ship,(SELECT substring(hu1.fechaEvento,1,10) ".
					        "FROM alhistoricounidadestbl hu1 WHERE hu1.vin = hu.vin AND hu1.claveMovimiento in ('L3')) as dateVehicle, ".
					        "case when da.vin is not null then 'HR' else '' end as HRoHQ,case when da.vin is not null then concat(cat.areaDano,cat.tipoDano,cat.severidadDano)  else '' end  as holdCodes, ".
					        "case when da.vin is not null then da.observacion else '' end as   holdAdditional,ca.descripcion,'' as markModCode,'' as vehicleTrans, '' as engineNumber, '' as modelPackcage, ".
					        "'' as carrierCut,  case when da.vin is not null then dy.fped else dy.fped end as vesselName,'' as vesselNumber,'' as voyageNumber, '' as containerNumber,'' as containerSeal,'' as timerQ, ".
					        "concat(substring(hu.fechaEvento,12,2),substring(hu.fechaEvento,15,2),substring(hu.fechaEvento,18,2)) as eventTime,'TRAM' as sender, ".
					        "(SELECT folio +1 FROM trfoliostbl WHERE centroDistribucion='LZC02' AND compania ='GM' AND tipoDocumento ='GM') as sequence ".        
					"from alinstruccionesmercedestbl dy inner join alhistoricounidadestbl hu on hu.vin = dy.vin inner join casimbolosunidadestbl ca on ca.simboloUnidad = dy.modelDesc ".
					"left join  aldanosunidadestbl da on  da.vin = dy.vin  left join cadanostbl cat on da.iddano = cat.iddano left join cadistribuidorescentrostbl di on di.distribuidorCentro = dy.cveDisEnt ".
					"where dy.cveStatus in('3G')	".
					"and dy.cveLoc='LZC02' ".
					"AND di.tipoDistribuidor='DX' ".
					"AND hu.claveMovimiento='D5' ".					
					"AND hu.vin not in (SELECT ti.vin FROM altransaccionunidadtbl ti WHERE ti.vin = hu.vin AND ti.tipoTransaccion in('IS6')) ".
					"order by hu.vin;";

		$rsSqlUnidS6= fn_ejecuta_query($sqlUnidS6);
		
		echo json_encode($rsSqlUnidS6);



		if (count($rsSqlUnidS6['root']) != 0) {


			$fecha = date("YmdHis");
			$directorio = "E:/carbook/interfacesGM/S6/";
			$inicioFile = "DFYP301X";
			$archivo = fopen($directorio.$inicioFile,"w");
			$today = date("Y-m-d H:i:s");
			//$fecha = substr($today,0,10);
			$nombreBusqueda = "DFYP301X";


			//encabezado
			fwrite($archivo,"GESGEIS6.000  TRAM             DFYDETROIT$".$fecha.sprintf('%09s',$rsSqlUnidS6['root'][0]['sequence'])."EST".PHP_EOL);

			//detalle

			for ($i=0; $i <sizeof($rsSqlUnidS6['root']) ; $i++) {
					
				echo json_encode($rsSqlUnidS6['root'][$i]['vehicleDate']);	

				fwrite($archivo,sprintf('%-1s',$rsSqlUnidS6['root'][$i]['shipper']).sprintf('%-17s',$rsSqlUnidS6['root'][$i]['vin']).sprintf('%-6s',$rsSqlUnidS6['root'][$i]['orderNumber']).sprintf('%-2s',$rsSqlUnidS6['root'][$i]['eventCode']).sprintf('%-10s',$rsSqlUnidS6['root'][$i]['vehicleDate']).sprintf('%-9s',$rsSqlUnidS6['root'][$i]['dealer']).sprintf('%-3s',$rsSqlUnidS6['root'][$i]['portCode']).sprintf('%-5s',$rsSqlUnidS6['root'][$i]['portDischarge']).sprintf('%-5s',$rsSqlUnidS6['root'][$i]['currency']).sprintf('%-7s',$rsSqlUnidS6['root'][$i]['volume']).sprintf('%-25s',$rsSqlUnidS6['root'][$i]['vesselName']).sprintf('%-10s',$rsSqlUnidS6['root'][$i]['dateVehicle']).sprintf('%-30s',$rsSqlUnidS6['root'][$i]['storageLocation']).sprintf('%-15s',$rsSqlUnidS6['root'][$i]['storageNumber']).sprintf('%-14s',$rsSqlUnidS6['root'][$i]['futureUse']).sprintf('%-3s',$rsSqlUnidS6['root'][$i]['timerQ']).sprintf('%-6s',$rsSqlUnidS6['root'][$i]['eventTime']).sprintf('%-12s',$rsSqlUnidS6['root'][$i]['containerNumber']).sprintf('%-11s',$rsSqlUnidS6['root'][$i]['containerSealNumber']).sprintf('%-109s',$rsSqlUnidS6['root'][$i]['reservedForFutureUse']).PHP_EOL);

				$today = date("Y-m-d H:i:s");
				$hora1=substr($today,11,8);
				$fecha = substr($today,0,10);
				$fecha1 = $rsSqlUnidS6['root'][$i]['vehicleDate']." ".$hora1;

				
				$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
									"VALUES ('IS6', '".
											"LZC02"."', '".
											$rsSqlUnidS6['root'][$i]['vin']."', '".
											$fecha1."', '".
											"S6"."', '".
											$today."','S6', '".
											substr($today,0,10)."', '".
											substr($today,11,8)."')";
				fn_ejecuta_query($insTransaccion);

				$updateEstatusS6 = "UPDATE alinstruccionesmercedestbl set cveStatus='6G' where vin ='".$rsSqlUnidS6['root'][$i]['vin']."' ";
				fn_ejecuta_query($updateEstatusS6);
				
				
			}


			//fin de archivo
			$long=(sizeof($rsSqlUnidS6['root']));
			fwrite($archivo,"GEE".sprintf('%06d',($long)).sprintf('%09s',$rsSqlUnidS6['root'][0]['sequence']));

			fclose($archivo);	
			//ftpArchivo ($nombreBusqueda);

			$updateFolioGM = "UPDATE trfoliostbl set folio=".$rsSqlUnidS6['root'][0]['sequence']." where centroDistribucion='LZC02' and compania='GM' and tipoDocumento='GM'";
			fn_ejecuta_query($updateFolioGM);


		}
	}

	/*function ftpArchivo($nombreBusqueda){
			if(file_exists("E:/carbook/archivosInterfaces/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/archivosInterfaces/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/archivosInterfaces/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}*/

?>