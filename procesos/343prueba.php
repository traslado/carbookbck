<?php
require("../funciones/generales.php");
require("../funciones/construct.php");
require_once("../funciones/utilidadesProcesos.php");
require_once("../funciones/utilidades.php");

        i343();

        function i343(){

        echo "inicio i343";

        $fechaAnt = strtotime ( '-10 day' , strtotime (date("Y-m-d")) ) ;
        $fechaAnt = date ( 'Y-m-d' , $fechaAnt );


        $sqlGetUnidades1 = "SELECT distinct c.nombre ".
                "FROM alhistoricounidadestbl h1, alunidadestbl al, casimbolosunidadestbl ca, catarifastbl tb, cageneralestbl c ".
                "WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
                "AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
                "AND h1.claveMovimiento IN ('ED','OM','ER','RO','PT','OK','CS') ".
                "AND h1.vin = al.vin ".
                "AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX') ".
                "AND substring(al.distribuidor,1,1) !='H' ".
                "AND al.simboloUnidad = ca.simboloUnidad ".
                "AND ca.marca not in ('HY','KI') ".
                "AND h1.idTarifa = tb.idTarifa ".
                "AND tb.tarifa !='13' ".
                "AND c.tabla = 'interfaces' ".
                "AND c.columna = 'splc' ".
                "AND c.valor = h1.centroDistribucion ".
                "AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
                "AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ".
                "UNION ".
                "SELECT distinct c.nombre ".
                "FROM alhistoricounidadestbl h1, alunidadestbl al, casimbolosunidadestbl ca,catarifastbl tb, cageneralestbl c ".
                "WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
                "AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
                "AND h1.claveMovimiento IN ('EP') ".
                "AND h1.vin = al.vin ".
                "AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX')  ".
                "AND al.distribuidor IN ('M8220', 'M8221', 'M8270', 'M8271') ".
                "AND al.simboloUnidad = ca.simboloUnidad ".
                "AND ca.marca not in ('HY','KI') ".
                "AND h1.idTarifa = tb.idTarifa ".
                "AND tb.tarifa !='13' ".
                "AND c.tabla = 'interfaces' ".
                "AND c.columna = 'splc' ".
                "AND c.valor = h1.centroDistribucion ".
                "AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
                "AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ";

        $rsGetUnidades1 = fn_ejecuta_query($sqlGetUnidades1);

        //echo json_encode($rsGetUnidades1['root']);

        for ($i=0; $i <sizeof($rsGetUnidades1['root']) ; $i++) { 

          $sqlGetUnidades = "SELECT h1.centroDistribucion, h1.vin, h1.fechaEvento, h1.claveMovimiento, tb.tarifa,al.distribuidor, ".
                "(SELECT ca.nombre FROM cageneralestbl ca WHERE ca.tabla = 'interfaces' AND ca.columna = 'splc' AND ca.valor = h1.centroDistribucion) as oriSplc ".
                "FROM alhistoricounidadestbl h1, alunidadestbl al, casimbolosunidadestbl ca, catarifastbl tb, cageneralestbl c ".
                "WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
                "AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
                "AND h1.claveMovimiento IN ('ED','OM','ER','RO','PT','OK','CS') ".
                "AND h1.vin = al.vin ".
                "AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX') ".
                "AND substring(al.distribuidor,1,1) !='H' ".
                "AND al.simboloUnidad = ca.simboloUnidad ".
                "AND ca.marca not in ('HY','KI') ".
                "AND h1.idTarifa = tb.idTarifa ".
                "AND tb.tarifa !='13' ".
                "AND c.tabla = 'interfaces' ".
                "AND c.columna = 'splc' ".
                "AND c.valor = h1.centroDistribucion ".
                "AND c.nombre='".$rsGetUnidades1['root'][$i]['nombre']."' ".
                "AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
                "AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ".
                "UNION ".
                "SELECT h1.centroDistribucion, h1.vin, h1.fechaEvento, h1.claveMovimiento, tb.tarifa,al.distribuidor, ".
                "(SELECT ca.nombre FROM cageneralestbl ca WHERE ca.tabla = 'interfaces' AND ca.columna = 'splc' AND ca.valor = h1.centroDistribucion) as oriSplc ".
                "FROM alhistoricounidadestbl h1, alunidadestbl al, casimbolosunidadestbl ca,catarifastbl tb, cageneralestbl c ".
                "WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
                "AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
                "AND h1.claveMovimiento IN ('EP') ".
                "AND h1.vin = al.vin ".
                "AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX')  ".
                "AND al.distribuidor IN ('M8220', 'M8221', 'M8270', 'M8271') ".
                "AND al.simboloUnidad = ca.simboloUnidad ".
                "AND ca.marca not in ('HY','KI') ".
                "AND h1.idTarifa = tb.idTarifa ".
                "AND tb.tarifa !='13' ".
                "AND c.tabla = 'interfaces' ".
                "AND c.columna = 'splc' ".
                "AND c.valor = h1.centroDistribucion ".
                "AND c.nombre='".$rsGetUnidades1['root'][$i]['nombre']."' ".
                "AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
                "AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ".
                "group by fechaEvento ".
                "LIMIT 100; ";

          $rsGetUnidades = fn_ejecuta_query($sqlGetUnidades);


          echo json_encode($rsGetUnidades);

          if (sizeof($rsGetUnidades['root']) !='0') {
              generaArchivo($rsGetUnidades);
          }else{
            echo "no existen unidades por transmitir";
          }
        }   
    }

    function generaArchivo($rsGetUnidades){

            $selFolio="SELECT * FROM trfoliostbl ".
                  "WHERE compania='H10' ".          
                  "AND centroDistribucion='TCO';";
            $rsFolio=fn_ejecuta_query($selFolio);         

            //$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
            $fileDir = "C:/carbook/i343/HA510".$rsFolio['root'][0]['folio'].".txt";
            $flReporte660 = fopen($fileDir, "a") or die("No se pudo generar ,interfaz");

            //A) ENCABEZADO
            fwrite($flReporte660,'ISA*03*HA510     *00*          *ZZ*XTRA           *ZZ*ADMISDCC       *'.date('ymd').'*'.date('hi').'*U*00200*'.sprintf('%09d','1').'*0*P*<'.PHP_EOL);
            fwrite($flReporte660,'GS*VI*XTRA*INNI*'.date('ymd').'*'.date('hi').'*1*T*1'.PHP_EOL);

            $incremento = 0;

         for ($m=0;$m < sizeof($rst4['root']);$m++){ 
            $sqlEncabezado4 = array(1 => 'ST*510*1', 
                                    9 => str_pad(($incremento+1),4,'0',STR_PAD_LEFT));
            $text2 = getTxt($sqlEncabezado4, array_keys($sqlEncabezado4)).out('n', 1); // TERCER RENGLON
            fwrite($fileLog, $text2);
            $sqlEncabezado4 = array(1 => 'BV1*XTRA*', 
                                    10 => $rst4['root'][$m]['oriSplc'],
                                    11 => '*',
                                    20 => $rst4['root'][$m]['NoVinxfecha']);
            $text2 = getTxt($sqlEncabezado4, array_keys($sqlEncabezado4)).out('n', 1); // CUARTO RENGLON
            fwrite($fileLog, $text2);
            $fechaEvento = date_create($rst4['root'][$m]['fechaMovimientoEp']);
            // echo $fechaEvento; aqui finaliza el cilco
            $sqlEncabezado5 =  "SELECT centroDistribucion,vin,fechaEvento,horaEvento,fechaMovimientoEp,horaMovimientoEp,distribuidor, oriSplc FROM al510tmp WHERE scacCode = '".$rst['root'][$i]['scacCode']."'AND centroDistribucion = '". $rst3['root'][$j]['centroDistribucion'] ."' AND fechaMovimientoEp = '". date_format($fechaEvento,'Y-m-d') . "'"  ; 
            $rst5 = fn_ejecuta_query($sqlEncabezado5); //CONSULTA 4
                    //echo json_encode($sqlEncabezado5);
            for ($k=0;$k < sizeof($rst5['root']);$k++){                                
              $sqlcadena1 ="SELECT tl.folio as talon, hu.fechaEvento as FechaEvento2, ". 
                            "hu.centroDistribucion as centroDistribucion,tl.companiaRemitente as compania, tr.tractor,substring(hu.fechaEvento,11,10) as horaEvento ".
                            "FROM alhistoricounidadestbl hu, trunidadesdetallestalonestbl ts, trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr ".
                            "WHERE hu.vin = ts.vin ".
                            "AND vt.idViajeTractor = tl.idViajeTractor ". 
                            "AND tl.idTalon = ts.idTalon ".
                            "AND vt.idTractor = tr.idTractor ". 
                            "AND hu.claveMovimiento ='AM' ". 
                            "AND hu.claveChofer = vt.claveChofer ".
                            "AND hu.fechaEvento = (SELECT max(hu2.fechaEvento) FROM alhistoricounidadestbl  hu2 WHERE hu2.vin = hu.vin AND hu2.claveMovimiento = hu.claveMovimiento) ".
                            "AND hu.vin = '".$rst5['root'][$k]['vin']."';";

              $rst6 = fn_ejecuta_query($sqlcadena1); //CONSULTA 5
              //echo json_encode($rst6);
              for ($l=0;$l < sizeof($rst6['root']);$l++){  
                $date_evento= date_create($rst6['root'][$l]['fechaEvento']);
                $cadena = $rst6['root'][$l]['centroDistribucion'].$rst6['root'][$l]['compania'].$rst6['root'][$l]['tractor'].$rst6['root'][$l]['talon'].date_format($fechaEvento,"d/m/Y");
                //echo $cadena; 
                $hrUnidad = substr($rst6['root'][$l]['horaEvento'],1,2).substr($rst6['root'][$l]['horaEvento'],4,2);                                    
                             
                $sqlEncabezado5 = array(1 => 'VI*', 
                                        4 => $rst5['root'][$k]['vin'],
                                        21 => '****',
                                        25 => $rst5['root'][$k]['distribuidor'],
                                        30 => '*',
                                        31 => $cadena);
                $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                fwrite($fileLog, $text2);

                $date_movi = date_create($rst5['root'][$k]['fechaMovimientoEp']);
                $hora_Eve  = date_create($rst5['root'][$k]['horaEvento']);
                $date_Eve  = date_create($rst5['root'][$k]['fechaEvento']);
                $sqlEncabezado5 = array(1 => 'P1**', 
                                        5 =>  date_format($date_movi, 'y'),
                                        7 =>  date_format($date_movi, "m"),
                                        9 =>  date_format($date_movi, "d"),
                                        11 => '*A');
                $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                fwrite($fileLog, $text2);
                $sqlEncabezado5 = array(1 => 'P2**', 
                                        5 =>  date_format($date_Eve,'y'),
                                        7 =>  date_format($date_Eve,"m"), 
                                        9 =>  date_format($date_Eve,"d"), 
                                        11 => '*A*',
                                        14 => $hrUnidad);                                                               
                //15 => date_format($rst6['root'][$k]['horaEvento'],"i"));
                $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                fwrite($fileLog, $text2);
              }                                                                    
            }
             
            $numrendetalle = ($rst4['root'][$m]['NoVinxfecha']*3)+1;
            $numrendetalle =str_pad($numrendetalle,4,'0',STR_PAD_LEFT);
            $sqlEncabezado5 = array(1 => 'SE*', 
                          4 => $numrendetalle,                      
                          7 => '*1',
                          9 => str_pad(($incremento+1),4,'0',STR_PAD_LEFT));
            $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
            fwrite($fileLog, $text2);
            $incremento = $incremento + 1;               
          }
    }

/*    $rssqlUnidades = fn_ejecuta_query($sqlUnidades);    
      //selecciona los ecabezados         
      //se Crea Archivo 
        $fileDir = "E:/carbook/i349/VA510.txt"; 
        echo "esta es la direccion".$fileDir;                                                  
        
        $fileLog = fopen($fileDir, 'a+');
       //print_r($rst['root']);
        $sqlEncabezado1 =  "SELECT scacCode FROM al510tmp where scacCode in ('XTRA','MITS') group by scacCode" ;
        $rst = fn_ejecuta_query($sqlEncabezado1); // CONSULTA 1 
        //echo json_encode($sqlEncabezado1);   
    if($rst['root']>0){
       //for ($i=0;$i < sizeof($rst['root']);$i++){
      if ($rst['root'][$i]['scacCode'] ='XTRA'){ 
            $marca='DCC';
      }else{
             $marca=$rst['root'][$i]['scacCode'];
      }
      $sqlEncabezado1 = array(1 => 'ISA*03*VA510     *00*          *ZZ*XTRA           *ZZ*ADMIS', 
           60 => $marca,
           64 => '      ',
           70 => '*',
           71 => date("y"),
           73 => date("m"),
           75 => date("d"),
           77 => '*',
           78 => date("h"),
           80 => date("i"),
           72 => '*U*00200*',
           89 => '00000000',
           90 =>  $i+1 ,
           100 => '*0*P*<');
      $text = getTxt($sqlEncabezado1, array_keys($sqlEncabezado1)).out('n', 1);  // PRIMER RENGLON
      fwrite($fileLog, $text);  
      $sqlEncabezado2 = array(1 => 'GS*VI*XTRA*INNI*', 
                         17 => date("y"),
                         19 => date("m"),
                         21 => date("d"),
                         23 => '*',
                         24 => date("h"),
                         26 => date("i"),
                         28 => '*1*T*1');
      $text2 = getTxt($sqlEncabezado2, array_keys($sqlEncabezado2)).out('n', 1);  // SEGUNDO RENGLON
      fwrite($fileLog, $text2);
      $sqlEncabezado3 = "SELECT  DISTINCT centroDistribucion,count(*) AS NoCdistribuidor FROM al510tmp  WHERE scacCode='".$rst['root'][$i]['scacCode'] ."' GROUP BY centroDistribucion";
      $rst3 = fn_ejecuta_query($sqlEncabezado3);
      //echo json_encode($sqlEncabezado3); // CONSULTA 2
      $incremento = 0;
      for ($j=0;$j < sizeof($rst3['root']);$j++){                  
        if ( $rst3['root'][$j]['centroDistribucion'] == "CDTOL")
          {$SPLC='958770807';}
            //echo $SPLC;
      $sqlEncabezado4 = "SELECT t.centroDistribucion,t.fechaMovimientoEp,count(t.vin) AS NoVinxfecha, t.oriSplc ". 
                        "FROM al510tmp t ".
                        "WHERE t.scacCode = '".$rst['root'][$i]['scacCode']."' ".
                        "AND t.centroDistribucion = '". $rst3['root'][$j]['centroDistribucion'] ."' ".
                        "GROUP BY T.fechaMovimientoEp;";

      $rst4 = fn_ejecuta_query($sqlEncabezado4);

      //echo json_encode($sqlEncabezado4); // CONSULTA 3      
      for ($m=0;$m < sizeof($rst4['root']);$m++){ 
        $sqlEncabezado4 = array(1 => 'ST*510*1', 
                                9 => str_pad(($incremento+1),4,'0',STR_PAD_LEFT));
        $text2 = getTxt($sqlEncabezado4, array_keys($sqlEncabezado4)).out('n', 1); // TERCERO RENGLON
        fwrite($fileLog, $text2);
        $sqlEncabezado4 = array(1 => 'BV1*XTRA*', 
                                10 => $rst4['root'][$m]['oriSplc'],
                                11 => '*',
                                20 => $rst4['root'][$m]['NoVinxfecha']);
        $text2 = getTxt($sqlEncabezado4, array_keys($sqlEncabezado4)).out('n', 1); // CUARTO RENGLON
        fwrite($fileLog, $text2);
        $fechaEvento = date_create($rst4['root'][$m]['fechaMovimientoEp']);
        // echo $fechaEvento; aqui finaliza el cilco
        $sqlEncabezado5 =  "SELECT centroDistribucion,vin,fechaEvento,horaEvento,fechaMovimientoEp,horaMovimientoEp,distribuidor, oriSplc FROM al510tmp WHERE scacCode = '".$rst['root'][$i]['scacCode']."'AND centroDistribucion = '". $rst3['root'][$j]['centroDistribucion'] ."' AND fechaMovimientoEp = '". date_format($fechaEvento,'Y-m-d') . "'"  ; 
        $rst5 = fn_ejecuta_query($sqlEncabezado5); //CONSULTA 4
                //echo json_encode($sqlEncabezado5);
        for ($k=0;$k < sizeof($rst5['root']);$k++){                                
          $sqlcadena1 ="SELECT tl.folio as talon, hu.fechaEvento as FechaEvento2, ". 
                        "hu.centroDistribucion as centroDistribucion,tl.companiaRemitente as compania, tr.tractor,substring(hu.fechaEvento,11,10) as horaEvento ".
                        "FROM alhistoricounidadestbl hu, trunidadesdetallestalonestbl ts, trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr ".
                        "WHERE hu.vin = ts.vin ".
                        "AND vt.idViajeTractor = tl.idViajeTractor ". 
                        "AND tl.idTalon = ts.idTalon ".
                        "AND vt.idTractor = tr.idTractor ". 
                        "AND hu.claveMovimiento ='AM' ". 
                        "AND hu.claveChofer = vt.claveChofer ".
                        "AND hu.fechaEvento = (SELECT max(hu2.fechaEvento) FROM alhistoricounidadestbl  hu2 WHERE hu2.vin = hu.vin AND hu2.claveMovimiento = hu.claveMovimiento) ".
                        "AND hu.vin = '".$rst5['root'][$k]['vin']."';";

          $rst6 = fn_ejecuta_query($sqlcadena1); //CONSULTA 5
          //echo json_encode($rst6);
          for ($l=0;$l < sizeof($rst6['root']);$l++){  
            $date_evento= date_create($rst6['root'][$l]['fechaEvento']);
            $cadena = $rst6['root'][$l]['centroDistribucion'].$rst6['root'][$l]['compania'].$rst6['root'][$l]['tractor'].$rst6['root'][$l]['talon'].date_format($fechaEvento,"d/m/Y");
            //echo $cadena; 
            $hrUnidad = substr($rst6['root'][$l]['horaEvento'],1,2).substr($rst6['root'][$l]['horaEvento'],4,2);                                    
                         
            $sqlEncabezado5 = array(1 => 'VI*', 
                                    4 => $rst5['root'][$k]['vin'],
                                    21 => '****',
                                    25 => $rst5['root'][$k]['distribuidor'],
                                    30 => '*',
                                    31 => $cadena);
            $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
            fwrite($fileLog, $text2);

            $date_movi = date_create($rst5['root'][$k]['fechaMovimientoEp']);
            $hora_Eve  = date_create($rst5['root'][$k]['horaEvento']);
            $date_Eve  = date_create($rst5['root'][$k]['fechaEvento']);
            $sqlEncabezado5 = array(1 => 'P1**', 
                                    5 =>  date_format($date_movi, 'y'),
                                    7 =>  date_format($date_movi, "m"),
                                    9 =>  date_format($date_movi, "d"),
                                    11 => '*A');
            $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
            fwrite($fileLog, $text2);
            $sqlEncabezado5 = array(1 => 'P2**', 
                                    5 =>  date_format($date_Eve,'y'),
                                    7 =>  date_format($date_Eve,"m"), 
                                    9 =>  date_format($date_Eve,"d"), 
                                    11 => '*A*',
                                    14 => $hrUnidad);                                                               
            //15 => date_format($rst6['root'][$k]['horaEvento'],"i"));
            $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
            fwrite($fileLog, $text2);
          }                                                                    
        }
         
        $numrendetalle = ($rst4['root'][$m]['NoVinxfecha']*3)+1;
        $numrendetalle =str_pad($numrendetalle,4,'0',STR_PAD_LEFT);
        $sqlEncabezado5 = array(1 => 'SE*', 
                      4 => $numrendetalle,                      
                      7 => '*1',
                      9 => str_pad(($incremento+1),4,'0',STR_PAD_LEFT));
        $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
        fwrite($fileLog, $text2);
        $incremento = $incremento + 1;               
      }                 
    }
    $sqlEncabezado5 = array(1 => 'GE*', 
                            4 => $incremento,
                            7 => '*',
                            9 => $i+1);
    $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
    fwrite($fileLog, $text2);

    echo "Fin de la Interfaz, numero de Bloques".$incremento;
//}
    $sqlEncabezado5 = array(1 => 'IEA*01', 
                            4 => '*000000001');
    $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
    fwrite($fileLog, $text2);
  }else{
    echo "No Existen Unidades de Interfaz";
  }      
  fclose($fileLog);*/
    
?>
 