<?php
	require("../funciones/generales.php");
	require("../funciones/utilidades.php");
  require_once("../funciones/utilidadesProcesos.php");
  
	date_default_timezone_set('America/Mexico_City');

    echo "Generacion de Archivo Interface 349: ";//.date("Y-m-d H:i", strtotime("now"))."\r\n";
    $sqlFechaInicialConsulta = "SELECT date_add(CURDATE(), INTERVAL - 5 DAY) as FechaConsulta";
    $rs = fn_ejecuta_query($sqlFechaInicialConsulta);
    //$fechaInicio = $rs['root'][0]['FechaConsulta'];
    $sqlFechaFinalConsulta   = "SELECT date_add(CURDATE(), INTERVAL - 0 DAY) as FechaFinalConsulta";
    $rs = fn_ejecuta_query($sqlFechaFinalConsulta);
    //$fechaFinal = $rs['root'][0]['FechaFinalConsulta'];
    // $Borra = 'DELETE FROM al510tmp';
    //$rs = fn_ejecuta_query($Borra);
    $fechaInicio = '20130501';
    $fechaFinal  =  '20151210';  
    $sqlUnidades =  "INSERT INTO al510tmp  (`centroDistribucion`, `vin`, `fechaEvento`, `horaEvento`, `fechaMovimientoEp`, `horaMovimientoEp`, `claveMovimiento`, `scacCode`, `idTarifa`, `distribuidor`) ".
                    "SELECT his.centroDistribucion, his.vin,  ".
                          "substring(his.fechaEvento,1,10) as FechaEvento,  ".
                          "substring(his.fechaEvento,12,8) as HorasEvento,  ".
                          "SUBSTRING((SELECT fechaEvento ".
                          "           FROM alhistoricounidadestbl ".
                          "           WHERE alhistoricounidadestbl.vin = t660.vin ".
                          "           AND alhistoricounidadestbl.claveMovimiento = 'EP'),1,10) as fechaEntradaPatio, ".
                          "SUBSTRING((SELECT fechaEvento ".
                          "           FROM alhistoricounidadestbl ".
                          "           WHERE alhistoricounidadestbl.vin =  t660.vin ".
                          "             AND alhistoricounidadestbl.claveMovimiento = 'EP'),12,8) as horaEP, ".
                          "his.claveMovimiento,t660.scacCode,his.idTarifa,his.distribuidor ".
                          "FROM    alhistoricounidadestbl his ,al660tbl t660 ".
                          "WHERE   his.claveMovimiento in ('ED','OM','ER','OK','RO','PT','ET','LA') ".
                          "AND  his.distribuidor NOT LIKE 'H%'   AND his.vin not in (SELECT vin FROM alinstruccionesmercedestbl) ".
                          "AND  his.vin=t660.vin AND  his.vin NOT in (SELECT vin FROM altransaccionunidadtbl where tipotransaccion not in ('V10')) ".
                          "AND  his.distribuidor NOT IN (SELECT distribuidorCentro FROM cadistribuidorescentrostbl where tipoDistribuidor='DX') ".
                          "AND  idtarifa <> 6  AND  fechaEvento BETWEEN ".$fechaInicio." AND ".$fechaFinal." group by his.vin " ;
       $rs = fn_ejecuta_query($sqlUnidades);
      //selecciona los ecabezados         
       //se Crea Archivo 
        $fileDir = 'C:\\carbook\\349\\VA510.txt';
        $fileLog = fopen($fileDir, 'a+');
       //print_r($rst['root']);
        $sqlEncabezado1 =  "SELECT scacCode FROM al510tmp where scacCode in ('XTRA','MITS') group by scacCode" ;
        $rst = fn_ejecuta_query($sqlEncabezado1); // CONSULTA 1    
    if($rst['root']>0){
       for ($i=0;$i < sizeof($rst['root']);$i++){
               if ($rst['root'][$i]['scacCode']='XTRA') 
                  { 
                    $marca='DCC ';
                  }
                else
                  {
                     $marca=$rst['root'][$i]['scacCode'];
                  }
              $sqlEncabezado1 = array(1 => 'ISA*03*VA510     *00*          *ZZ*XTRA           *ZZ*ADMIS', 
                   60 => $marca,
                   64 => '      ',
                   70 => '*',
                   71 => date("y"),
                   73 => date("m"),
                   75 => date("d"),
                   77 => '*',
                   78 => date("h"),
                   80 => date("i"),
                   72 => '*U*00200*',
                   89 => '00000000',
                   90 =>  $i+1 ,
                   100 => '*0*P>');
              $text = getTxt($sqlEncabezado1, array_keys($sqlEncabezado1)).out('n', 1);  // PRIMER RENGLON
              fwrite($fileLog, $text);  
              $sqlEncabezado2 = array(1 => 'GS*VI*XTRA*INNI*', 
                                 17 => date("y"),
                                 19 => date("m"),
                                 21 => date("d"),
                                 23 => '*',
                                 24 => date("h"),
                                 26 => date("i"),
                                 28 => '*1*T*1');
              $text2 = getTxt($sqlEncabezado2, array_keys($sqlEncabezado2)).out('n', 1);  // SEGUNDO RENGLON
              fwrite($fileLog, $text2);
              $sqlEncabezado3 = "SELECT  centroDistribucion,count(*) AS NoCdistribuidor FROM al510tmp  WHERE scacCode='".$rst['root'][$i]['scacCode'] ."' GROUP BY centroDistribucion";
              $rst3 = fn_ejecuta_query($sqlEncabezado3); // CONSULTA 2
                for ($j=0;$j < sizeof($rst3['root']);$j++){
                    
                     if ( $rst3['root'][$j]['centroDistribucion'] = 'CDTOL')
                          {$SPLC='958770807';}
                     $sqlEncabezado4 = "SELECT centroDistribucion,fechaMovimientoEp,count(*) AS NoVinxfecha FROM al510tmp t, trunidadesdetallestalonestbl tu WHERE t.vin = tu.vin and t.scacCode = '" .$rst['root'][$i]['scacCode']."'  AND T.centroDistribucion = '". $rst3['root'][$j]['centroDistribucion'] ."' GROUP BY T.fechaMovimientoEp";
                     $rst4 = fn_ejecuta_query($sqlEncabezado4); // CONSULTA 3
                     for ($m=0;$m < sizeof($rst4['root']);$m++){ 

                               $sqlEncabezado4 = array(1 => 'ST*510*1', 
                                                     9 => str_pad(($m+1),4,'0',STR_PAD_LEFT));
                               $text2 = getTxt($sqlEncabezado4, array_keys($sqlEncabezado4)).out('n', 1); // TERCERO RENGLON
                               fwrite($fileLog, $text2);
                              $sqlEncabezado4 = array(1 => 'BV1*XTRA*', 
                                                     10 => $SPLC,
                                                     11 => '*',
                                                     20 => $rst4['root'][$m]['NoVinxfecha']);
                             $text2 = getTxt($sqlEncabezado4, array_keys($sqlEncabezado4)).out('n', 1); // CUARTO RENGLON
                             fwrite($fileLog, $text2);
                             $fechaEvento = date_create($rst4['root'][$m]['fechaMovimientoEp']);
                             $sqlEncabezado5 =  "SELECT centroDistribucion,vin,fechaEvento,horaEvento,fechaMovimientoEp,horaMovimientoEp,distribuidor FROM al510tmp WHERE scacCode = '".$rst['root'][$i]['scacCode']."'AND centroDistribucion = '". $rst3['root'][$j]['centroDistribucion'] ."' AND fechaMovimientoEp = '". date_format($fechaEvento,'Y-m-d') . "'"  ; 
                             $rst5 = fn_ejecuta_query($sqlEncabezado5); //CONSULTA 4
                             for ($k=0;$k < sizeof($rst5['root']);$k++){   
                             
                                $sqlcadena1 = "SELECT  tta.folio AS talon,tta.fechaEvento as FechaEvento2,tta.centroDistribucion as centroDistribucion,tta.companiaRemitente as compania,cat.tractor as tractor ".
                                                "FROM   trunidadesdetallestalonestbl tud,trviajestractorestbl trvt , trtalonesviajestbl tta,".
                                                "trviajestractorestbl tv, caTractoresTbl cat         ".
                                                "where  tud.vin = '".$rst5['root'][$k]['vin']."' and ".
                                                "tta.idTalon=tud.idTalon                         and ".
                                                "trvt.idViajeTractor = tta.idViajeTractor        and ".
                                                "tta.idViajeTractor= tv.idViajeTractor           and ".
                                                "tv.idTractor=cat.idTractor";
                                  $rst6 = fn_ejecuta_query($sqlcadena1); //CONSULTA 5
                                  for ($l=0;$l < sizeof($rst6['root']);$l++){  
                                    $date_evento= date_create($rst6['root'][$l]['fechaEvento']);
                                     $cadena = $rst6['root'][$l]['centroDistribucion'].$rst6['root'][$l]['compania'].$rst6['root'][$l]['tractor'].$rst6['root'][$l]['talon'].date_format($fechaEvento,"d/m/Y");
                                       
                                       
                                       $sqlEncabezado5 = array(1 => 'VIN*', 
                                                               4 => $rst5['root'][$k]['vin'],
                                                               21 => '****',
                                                               25 => $rst5['root'][$k]['distribuidor'],
                                                               30 => '*',
                                                               31 => $cadena);
                                       $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                                       fwrite($fileLog, $text2);

                                       $date_movi = date_create($rst5['root'][$k]['fechaMovimientoEp']);
                                       $hora_Eve  = date_create($rst5['root'][$k]['horaEven']);
                                       $date_Eve  = date_create($rst5['root'][$k]['fechaEvento']);
                                       $sqlEncabezado5 = array(1 => 'P1**', 
                                                               5 =>  date_format($date_movi, 'y'),
                                                               7 =>  date_format($date_movi, "m"),
                                                               9 =>  date_format($date_movi, "d"),
                                                               11 => '*A');
                                       $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                                       fwrite($fileLog, $text2);
                                       $sqlEncabezado5 = array(1 => 'P2**', 
                                                               5 =>  date_format($date_Eve,'y'),
                                                               7 =>  date_format($date_Eve,"m"), 
                                                               9 =>  date_format($date_Eve,"d"), 
                                                               11 => '*A*',
                                                               14 => date_format($hora_Eve,"h"), 
                                                               16 => date_format($hora_Eve,"i"));
                                       $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                                       fwrite($fileLog, $text2);
                                  }
                                  
                                  
                              } 
                                            $numrendetalle = ($rst4['root'][$m]['NoVinxfecha']*3)+1;
                                            $numrendetalle =str_pad($numrendetalle,4,'0',STR_PAD_LEFT);
                                            $sqlEncabezado5 = array(1 => 'SE*', 
                                                          4 => $numrendetalle,
                                                          7 => '*1',
                                                          9 => str_pad(($m+1),4,'0',STR_PAD_LEFT));
                                         $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                                           fwrite($fileLog, $text2);
                              
                             
                           }  
                           
                            
                 }
                                  $sqlEncabezado5 = array(1 => 'GE*', 
                                                          4 => $m,
                                                          7 => '*',
                                                          9 => $i+1);
                                  $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                                   fwrite($fileLog, $text2);

          echo "Existen Unidades de Interfaz";
        }
                                  $sqlEncabezado5 = array(1 => 'IE*01*', 
                                                          4 => $rst4['root'][$m]['NoVinxfecha'],
                                                          7 => '*00000000',
                                                          9 => $i);
                                  $text2 = getTxt($sqlEncabezado5, array_keys($sqlEncabezado5)).out('n', 1); //out es un salto de linea
                                  fwrite($fileLog, $text2);

     }
    else
    {
        echo "No Existen Unidades de Interfaz";
    }      
    fclose($fileLog);





















/*

function fn_crear_archivo($array){
    //echo sizeof($array);
    $keys = array_keys($array);
    for ($i=0;$i < sizeof($keys);$i++) {
      $encabezado1 = array(1 => 'ISA*03*RA550     *00*          *ZZ*', 
                 36 => 'XTRA           *ZZ*ADMIS',
                 60 => $keys[$i],
                 64 => '      ',
                 70 => '*',
                 71 => date("ymd*Hi"),
                 82 => '*U*00200*',
                 91 => '000000001',
                 100 => '*0*P>');
        $text = '';
        $encabezado2 = array(1 => 'GS*VI*XTRA*VISTA*', 
                 18 => date("ymd*Hi"),
                 29 => '*00001*T*1');
        //se Crea Archivo 
        $fileDir = '../generacion_550/'.$keys[$i] .'.txt';
        $fileLog = fopen($fileDir, 'a+');
        //Por cada unidad...         
          $text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
          fwrite($fileLog, $text);
          $text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
          fwrite($fileLog, $text);
          $contSeg = 1;
          $keysSplc = array_keys($array[$keys[$i]]);
          for ($j=0;$j < sizeof($keysSplc);$j++) {
            $keysStatus = array_keys($array[$keys[$i]][$keysSplc[$j]]);
            for ($k=0;$k < sizeof($keysStatus);$k++) {
              $keysFecha = array_keys($array[$keys[$i]][$keysSplc[$j]][$keysStatus[$k]]);
              for ($l=0;$l < sizeof($keysFecha);$l++) {
                $rows = $array[$keys[$i]][$keysSplc[$j]][$keysStatus[$k]][$keysFecha[$l]];
                    $encabezado3 = array(1 => 'ST*550*00001', 13 => str_pad($contSeg,4,"0",STR_PAD_LEFT));
                $text = getTxt($encabezado3, array_keys($encabezado3)).out('n', 1); //out es un salto de linea
                fwrite($fileLog, $text);
                $movto = $keysStatus[$k];
                $segType="";
                if($movto == "CT")
                {
                  $segType="T";
                }
                else{
                  $segType="E";
                }
                $encabezado4 = array(1 => 'BV5*', 
                           5 => $segType,
                           6 => '*XTRA*',
                           12 => $keysSplc[$j],
                           21 => '*',
                           22 => str_pad(sizeof($rows),3,"0",STR_PAD_LEFT),
                           25 => '*',
                           26 => 'HH',
                           28 => '*',
                           29=> date("ymd***Hi",strtotime($keysFecha[$l])));
                $text = getTxt($encabezado4, array_keys($encabezado4)).out('n', 1); //out es un salto de linea
               fwrite($fileLog, $text);
                foreach ($rows as $index => $row) {
                  $detalle = array(1 => 'VI*', 
                             4 => $row['vin'],
                             21 => '*',
                             22 => substr($row['vin'], 10,11),
                             23 => '*',
                             24 => substr($row['routedes'], 0,1),
                             26 => '*****');
                  $text = getTxt($detalle, array_keys($detalle)).out('n', 1); //out es un salto de linea
                  fwrite($fileLog, $text);
                }
                $contSeg ++;
             }
              
           }

          }
      
      fclose($fileLog);
    }
  } */


?>
 