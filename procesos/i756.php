<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    /*$dirArchStr="C:carbook/i756/";
    $nomArchStr = "PRESHI";
    $extStr = "TXT";
    //$dirlog = "../reptra/log758-".date("Y-m-d").".log";
    $logDir="C:carbook/i756/";
    $dirlog="C:carbook/i756/";
    function fn_lee_archivo($dirArchStr,$nomArchStr,$extStr,$logDir){
    	$archStr = $dirArchStr.$nomArchStr.".".$extStr;
    	$newArchStr = $dirArchStr.$nomArchStr.".".$extStr;/*."_".date("Y-m-d_H:i:s").".".$extStr;*/
	fn_lee_archivo();
    function fn_lee_archivo(){
    	$archivoArr = array();
    	$cabceroStr = "";
    	$numRow = 1;


		$lines = file("C:/carbook/i756/PRESHI.txt");

		foreach($lines as $line)
		{
		    //echo($line). "<br />";
			if(strlen(trim($line))==28){
		    	$cabceroStr=$line;	 
		    }
		    if (strlen(trim($cabceroStr)) ==28) {
		    	$cabceroStr = $cabceroStr."-".$numRow;
				$archivoArr[$cabceroStr] = [];	
		   		validaEncabezado($cabceroStr);
		    }elseif (strlen(trim($cabceroStr)) <28) {
		    	$text =  $numRow."|Registro: Invalido|Encabezado/Detalle no identificado\n";
				crea_log($text);
		    }
		    if(strlen(trim($line))>28){
		    	$ArrDetalle []= array('serie' =>substr($line,0,8),
		    						'vin' =>substr($line,9,17),
		    						'simbolo' =>substr($line,27,6),
		    						'desc-esp' =>substr($line,34,30),
		    						'desc-ing' =>substr($line,65,31),
		    						'unid-basica' =>substr($line,96,10),
		    						'unid-basica-s' =>substr($line,106,1),
		    						'flete' =>substr($line,107,9),
		    						'flete-s' =>substr($line,116,1),
		    						'totvin-pesos' =>substr($line,117,12),
		    						'totvin-pesos-s' =>substr($line,129,1),
		    						'von' =>substr($line,131,8),
		    						'distribuidor' =>substr($line,140,7),
		    						'cves-opciones' =>substr($line,147,160),
		    						'imp-opciones' =>substr($line,307,10),
		    						'imp-opciones-s' =>substr($line,317,1),
		    						'imp-aduana' =>substr($line,318,9),
		    						'imp-aduana-s' =>substr($line,327,1),
		    						'tsinflete-us' =>substr($line,328,12),
		    						'tsinflete-us-s' =>substr($line,340,1),
		    						'fol-fac-normal' =>substr($line,362,9),
		    						'fecha-factura' =>substr($line,393,10),
		    						'tipo-cambio' =>substr($line,417,11),
		    						'imp-seguro' =>substr($line,438,10),
		    						'imp-seguro-s' =>substr($line,448,1),
		    						'imp-flete-usa' =>substr($line,449,9),
		    						'imp-flete-usa-s' =>substr($line,458,1),
		    						'total-unidad' =>substr($line,459,12),
		    						'total-unidad-s' =>substr($line,471,1),
		    						'facturas_mux' =>substr($line,472,3),
		    						'imp-import' =>substr($line,475,12),
		    						'imp-import-s' =>substr($line,487,1));
		    }

		   
   		$numRow ++;				
		}

		$resultArr = array('succcess'=>true,'encabezado'=> array(
    		'planta' => limpia_espacios(substr($cabceroStr,0,11)),
    		'clave' => limpia_espacios(substr($cabceroStr,11,3)),
    		"fecha" => limpia_espacios(substr($cabceroStr,14,8)),
    		"hora" => limpia_espacios(substr($cabceroStr,22,6))
    		));
		validaDetalle($ArrDetalle,$resultArr);

	}
	
    function validaEncabezado($cabceroStr){
    	$auxArr = explode("-", $cabceroStr);
    	$cabceroStr = $auxArr[0];
    	$numRegInt = $auxArr[1];

    	$resultArr = array('succcess'=>true,'encabezado'=> array(
    		'planta' => limpia_espacios(substr($cabceroStr,0,11)),
    		'clave' => limpia_espacios(substr($cabceroStr,11,3)),
    		"fecha" => limpia_espacios(substr($cabceroStr,14,8)),
    		"hora" => limpia_espacios(substr($cabceroStr,22,6))
    		));
    	foreach (array_keys($resultArr['encabezado']) as $key) {
    		if(strlen($resultArr['encabezado']['planta']) != 11){
				$text =  $numRegInt."|". "Planta" ." Invalida:\n";
				crea_log($text);	
			}elseif(strlen($resultArr['encabezado']['clave']) != 3){
				$text =  $numRegInt."|". "Clave" ." Invalida:\n";
				crea_log($text);	
			}elseif(strlen($resultArr['encabezado']['fecha']) != 8){
				$text =  $numRegInt."|". "Fecha" ." Invalida:\n";
				crea_log($text);	
			}elseif(strlen($resultArr['encabezado']['hora']) != 6){
				$text =  $numRegInt."|". "Hora" ." Invalida:\n";
				crea_log($text);	
			}
			 //validaDetalle($detalleStr,$dirlog);

		}
    }

    function validaDetalle($ArrDetalle,$resultArr){
    	for ($i=0; $i <sizeof($ArrDetalle) ; $i++) { 
    		if(strlen($ArrDetalle[$i]['serie']) != 8){
				$text = "| num Serie  Invalida:\n";
				crea_log($text);	
			}
			if(strlen($ArrDetalle[$i]['vin']) != 17){
				$text = "| vin  Invalido:\n";
				crea_log($text);	
			}
			if(strlen($ArrDetalle[$i]['simbolo']) != 6){
				$text = "| simbolo  Invalido:\n";
				crea_log($text);	
			}
			if(strlen($ArrDetalle[$i]['desc-esp']) < 1){
				$text = "| desc-esp Invalido:\n";
				crea_log($text);	
			}
			if(strlen($ArrDetalle[$i]['unid-basica']) < 1){
				$text = "| unid-basica  Invalida:\n";
				crea_log($text);	
			}
			if(strlen($ArrDetalle[$i]['flete']) < 1){
				$text = "| flete  Invalido:\n";
				crea_log($text);	
			}
			if(strlen($ArrDetalle[$i]['totvin-pesos']) < 1){
				$text = "| totvin-pesos  Invalido:\n";
				crea_log($text);	
			}
			if(strlen($ArrDetalle[$i]['von']) < 1){
				$text = "| von  Invalido:\n";
				crea_log($text);	
			}
			if(strlen($ArrDetalle[$i]['distribuidor']) < 1){
				$text = "| distribuidor  Invalido:\n";
				crea_log($text);	
			}
			$cambio=str_replace('$', " ", $ArrDetalle[$i]['tipo-cambio']);
    	}
    	insertaDatos($resultArr,$ArrDetalle,$cambio);
    	
    }
    function crea_log($text){
    	$fecha = date("YmdHis");
		$dir="C:/carbook/i756/log/";
   	    $nomArchStr = "log_756".$fecha;
    	$fileLog = fopen($dir.$nomArchStr, 'w');
    	if($fileLog){
		    fwrite($fileLog, $text);
		    fclose($fileLog);
		}
		
    }
    function limpia_espacios($cadena){
		$cadena = str_replace(' ', '', $cadena);
		return $cadena;
	}
	function insertaDatos($resultArr,$ArrDetalle,$cambio){    	   
    	for ($i=0; $i <count($ArrDetalle) ; $i++) { 
			$sqlInstStr =	"INSERT INTO alPreciosShippersTbl (clavePlanta, nombrePlanta, fechaPrecion, tiempoPrecio,".
							"vin, avanzada, simboloUnidad, descripcionEspanol, descripcionIngles, unidadBasica, unidadBasicaS, ".
							"flete, fleteS, totVinPesos, totVinPesosS, von, distribuidor, claveOpciones, impOpciones, impOpcionesS,".
							"impAduana, impAduanaS, tSinFleteUs, tSinFleteUsS, folioFacturaNormal, fechaFactura, tipoCambio, impSeguro, ".
							"impSeguroS, impFleteUsa, impFleteUsaS, totalUnidad, totalUnidadS, facturasMux, impImport, impImportS, fechaOrigen, claveEstatus, fechaEstatus)".
							"VALUES (".
							"'". $resultArr['encabezado']['clave'] ."',".
							"'". $resultArr['encabezado']['planta'] ."',".
							"'". $resultArr['encabezado']['fecha'] ."',".
							"'". $resultArr['encabezado']['hora'] ."',".
							"'". $ArrDetalle[$i]['vin'] ."',".
							"'". $ArrDetalle[$i]['serie'] ."',".
							"'". $ArrDetalle[$i]['simbolo'] ."',".
							"'". $ArrDetalle[$i]['desc-esp'] ."',".
							"'". $ArrDetalle[$i]['desc-ing'] ."',".
							"'". $ArrDetalle[$i]['unid-basica'] ."',".
							"'". $ArrDetalle[$i]['unid-basica-s'] ."',".
							"'". $ArrDetalle[$i]['flete'] ."',".
							"'". $ArrDetalle[$i]['flete-s'] ."',".
							"'". $ArrDetalle[$i]['totvin-pesos'] ."',".
							"'". $ArrDetalle[$i]['totvin-pesos-s'] ."',".
							"'". $ArrDetalle[$i]['von'] ."',".
							"'". $ArrDetalle[$i]['distribuidor'] ."',".
							"'". $ArrDetalle[$i]['cves-opciones'] ."',".
							"'". $ArrDetalle[$i]['imp-opciones'] ."',".
							"'". $ArrDetalle[$i]['imp-opciones-s'] ."',".
							"'". $ArrDetalle[$i]['imp-aduana'] ."',".
							"'". $ArrDetalle[$i]['imp-aduana-s'] ."',".
							"'". $ArrDetalle[$i]['tsinflete-us'] ."',".
							"'". $ArrDetalle[$i]['tsinflete-us-s'] ."',".
							"'". $ArrDetalle[$i]['fol-fac-normal'] ."',".
							"'". $ArrDetalle[$i]['fecha-factura'] ."',".
							"'". $cambio."',".
							"'". $ArrDetalle[$i]['imp-seguro'] ."',".
							"'". $ArrDetalle[$i]['imp-seguro-s'] ."',".
							"'". $ArrDetalle[$i]['imp-flete-usa'] ."',".
							"'". $ArrDetalle[$i]['imp-flete-usa-s'] ."',".
							"'". $ArrDetalle[$i]['total-unidad'] ."',".
							"'". $ArrDetalle[$i]['total-unidad-s'] ."',".
							"'". $ArrDetalle[$i]['facturas_mux'] ."',".
							"'". $ArrDetalle[$i]['imp-import'] ."',".
							"'". $ArrDetalle[$i]['imp-import-s'] ."',".
							"'". date("Y-m-d") ."',".
							"'IN',".
							"'". date("Y-m-d") ."'".
							")";
			fn_ejecuta_query($sqlInstStr);
		}	    			
    }
?>