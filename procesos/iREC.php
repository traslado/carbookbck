<?php

			require_once("../funciones/generales.php");
	        require_once("../funciones/construct.php");
	        require_once("../funciones/utilidades.php");

	        date_default_timezone_set('America/Mexico_City');

	       	switch($_REQUEST['alejecutaInterfacePortInHdn']){
			case 'ejecutaREC':
				ejecutaREC();
				break;
		    default:
		        echo '';
		}


	        function ejecutaREC(){	
	        echo "Inicio iREC: ".date("Y-m-d H:i", strtotime("now"))."\r\n";	

	        $sqlRec = 	"SELECT dy.vin, dy.cveStatus ".
						"FROM alhistoricounidadesTbl al, alinstruccionesmercedestbl dy ".
						"WHERE al.vin = dy.vin ".
						"AND dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
						"AND al.centroDistribucion='LZC02' ".
						"AND al.claveMovimiento = 'L1' ".
						"AND al.observaciones = 'UNIDAD INGRESO L1 LC' ".
						"AND dy.cveStatus in ('DH','DK') ".
						"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='REC') ";

			$rsRec= fn_ejecuta_query($sqlRec);


			for ($i=0; $i <sizeof($rsRec['root']) ; $i++) { 

				if (substr($rsRec['root'][$i]['cveStatus'], -2) == 'DK' ) {	

					$arrK[] = $rsRec['root'][$i];				
				}
			/////////////////////////////////////////////////////////
				else if (substr($rsRec['root'][$i]['cveStatus'], -2) == 'DH') {
					$arrH[] = $rsRec['root'][$i];
				}

				else{
					//echo "string";
				}
			}
			if (count($arrK) != 0) {
				$varEstatus = 'DK';
				$nomArchivo = 'K';
				$portCode=	'FT16';
				generaREC($varEstatus,$nomArchivo,$portCode);
			}	
			if (count($arrH) != 0) {
				$varEstatus = 'DH';
				$nomArchivo = 'H';
				$portCode=	'1145';
				generaREC($varEstatus,$nomArchivo,$portCode);		
			}
				
			echo "Fin iREC: ".date("Y-m-d H:i", strtotime("now"))."\r\n";			
		} 

		function generaREC($varEstatus,$nomArchivo,$portCode){			

			$sqlGenerarDatos= "SELECT dy.vin,dy.fPed,hu.fechaEvento ".
							  "FROM alinstruccionesMercedestbl dy, alhistoricounidadesTbl hu ".
							  "WHERE dy.vin=hu.vin ". 
							  "AND dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
							  "AND hu.centroDistribucion='LZC02' ".
							  "AND hu.claveMovimiento='L1' ".
							  "AND hu.observaciones = 'UNIDAD INGRESO L1 LC' ".
							  "AND dy.vin not in (SELECT vin from altransaccionUnidadTbl WHERE tipoTransaccion='REC')  ".
							  "AND dy.cveStatus ='".$varEstatus."' ";

	       	$rssqlGenerarDatos= fn_ejecuta_query($sqlGenerarDatos);

	          //if(sizeof($rssqlGeneraDatos['root']) != null)
	          //{
				if(sizeof($rssqlGenerarDatos['root']) != NULL){

				$fecha = date("YmdHis");
		        $fileDir = "E:/carbook/archivosInterfacesGLOVIS/respREC/".$nomArchivo."MM_REC_".$fecha.".txt";
		        $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
		        $recordsPositionValue = array();

		        $nombreBusqueda = $nomArchivo."MM_REC_".$fecha.".txt";

	        //for ($i = 0; $i <= $patioGroup; i++) {       
	            //foreach ($patioGroup as $conteo => $patioGroup) {
	            
	            //A) ENCABEZADO
	            fwrite($fileRec, 'RECH APS  GMX  REC'."$fecha".PHP_EOL);
	                
	                //B) DETALLE UNIDADES

	                for ($i=0; $i < sizeof($rssqlGenerarDatos['root']); $i++) 
	                { 
	                    fwrite($fileRec,'REC  '.$portCode.'  '.$rssqlGenerarDatos['root'][$i]['vin'].substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],0,4).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],5,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],8,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],11,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],14,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],17,2).sprintf('%-20s',$rssqlGenerarDatos['root'][$i]['fPed']).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],0,4).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],5,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],8,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],11,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],14,2).substr($rssqlGenerarDatos['root'][$i]['fechaEvento'],17,2).PHP_EOL);


						$fecha1 = $rssqlGenerarDatos['root'][$i]['fechaEvento'];
						$today = date("Y-m-d H:i:s");
						$fecha2 = substr($today,0,10);
						$hora=substr($today,11,8);


							$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin, ".
							 				"fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".
											"VALUES ('REC','LZC02',NULL,'".$rssqlGenerarDatos['root'][$i]['vin']."','".$today."','L2','".$fecha1."','".$varEstatus."','".$fecha2."','".$hora."') ";    

							fn_ejecuta_query($sqlAddTransaccion);
					}

	           
					//C) TRAILER
	                fwrite($fileRec, 'RECT '.sprintf('%06d', (sizeof($rssqlGenerarDatos['root'])+2)));
	               	fclose($fileRec);
	               	ftpArchivo_01($nombreBusqueda);
	           }

		else {

			echo "No hay unidades pendientes por procesar";
		}
	}

	function ftpArchivo_01($nombreBusqueda){
			if(file_exists("E:/carbook/archivosInterfacesGLOVIS/respREC/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/archivosInterfacesGLOVIS/respREC/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/archivosInterfacesGLOVIS/respREC/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}

?>  

