<?php
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    date_default_timezone_set('America/Mexico_City');

    ejecuta904();

    function ejecuta904()
    {

    	echo "inicio VTIMS 904";

    	//unlink("C:/servidores/DFYP301X");


		$sqlUnidadesGM= "SELECT  case when substr(dy.cveDisFac,-5) in('77831','77828','77827','77848','77829','77830','72148','31112') then 'G' else 'M' end as shipper, dy.vin, '' as orderNumber, case when da.vin is not null then 'S2' else 'S3' end  as eventCode , substring(hu.fechaEvento,1,10) as  vehicleDate, ".
									"'  ' as dealer,'LC' as portCode, '' as portDischarge, dy.currency,'' as volume ,substring(dy.cveDisFac,3) as ship,substring(hu.fechaEvento,1,10) as dateVehicle, ".
							        "case when da.vin is not null then 'HR' else '' end as HRoHQ,case when da.vin is not null then concat(cat.areaDano,cat.tipoDano,cat.severidadDano)  else '' end  as holdCodes, ".
							        "case when da.vin is not null then da.observacion else '' end as   holdAdditional,ca.descripcion,'' as markModCode,'' as vehicleTrans, '' as engineNumber, '' as modelPackcage, ".
							        "'' as carrierCut,  case when da.vin is not null then dy.fped else '' end as vesselName,'' as vesselNumber,'' as voyageNumber, '' as containerNumber,'' as containerSeal,'' as timerQ, ".
							        "concat(substring(hu.fechaEvento,12,2),substring(hu.fechaEvento,15,2),substring(hu.fechaEvento,18,2)) as eventTime,'TRAM' as sender, ".
							        "concat('',(SELECT folio +1 FROM trfoliostbl WHERE centroDistribucion='LZC02' AND compania ='GM' AND tipoDocumento ='VT')) as sequence ".
							"FROM alinstruccionesmercedestbl dy inner join alhistoricounidadestbl hu on hu.vin = dy.vin inner join casimbolosunidadestbl ca on ca.simboloUnidad = dy.modelDesc ".
							"left join  aldanosunidadestbl da on  da.vin = dy.vin  left join cadanostbl cat on da.iddano = cat.iddano left join cadistribuidorescentrostbl di on di.distribuidorCentro = dy.cveDisEnt ".
							"WHERE  dy.cveLoc='LZC02' ".	
							"AND di.tipoDistribuidor='DX' ".
							"AND hu.claveMovimiento in ('D3','D4') ".
							"AND hu.vin not in (SELECT ti.vin FROM altransaccionunidadtbl ti WHERE ti.vin = hu.vin AND ti.tipoTransaccion in('904')) ".
							//"AND hu.vin  in (SELECT ti.vin FROM altransaccionunidadtbl ti WHERE ti.vin = hu.vin AND ti.tipoTransaccion in('iS1'))  ". 
							"AND hu.vin not in ('1234567PRUEBAGM01',
												'1234567PRUEBAGM02',
												'1234567PRUEBAGM03',
												'1234567PRUEBAGM04',
												'1234567PRUEBAGM05',
												'1234567PRUEBAGM06',
												'1234567PRUEBAGM07',
												'1234567PRUEBAGM08',
												'1234567PRUEBAGM09',
												'1234567PRUEBAGM10') ".
							"order by hu.vin ".
							"LIMIT 30;"; 

	    $rstSqlUnidadesGM= fn_ejecuta_query($sqlUnidadesGM);

	    


		if(sizeof($rstSqlUnidadesGM['root']) != NULL){

			for ($i=0; $i <sizeof($rstSqlUnidadesGM['root']) ; $i++) { 
				
				if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S2') {				
					//$arrS2[] = $rstSqlUnidadesGM['root'][$i];
					$sqlHOLD="SELECT * FROM aldanosunidadestbl al, cadanostbl ca 
								WHERE al.vin ='".$rstSqlUnidadesGM['root'][$i]['vin']."'
								AND al.idDano=ca.idDano
								AND ca.areaDano in ('98','52','40')
								AND ca.tipoDano in ('08','19','08')
								AND ca.severidadDano in ('06'); ";
					$rsSqlHOLD=fn_ejecuta_query($sqlHOLD);

					if ($rsSqlHOLD['root']!= NULL) {
					$arr904[] = $rstSqlUnidadesGM['root'][$i];
					}
				}				
			/////////////////////////////////////////////////////////
			
				if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S3') {
					$arr904[] = $rstSqlUnidadesGM['root'][$i];
				}
				else{
					//echo "string";
				}
			}/////////////////////////////////////////
		}

		/*if ($arrS2 != null) {
			generaS2($arrS2);			
		}*/

		//echo count($arr904);
		if ($arr904 != null) {
			genera904($arr904);			
		}
	}


	function genera904($arr904){	


		$fecha = date("YmdHis");
	    $fileDir = "C:/servidores/XTRA4LU8";

	    $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
	    $recordsPositionValue = array();

	            
	    //A) ENCABEZADO
	    
		fwrite($fileRec,"***XTRA4LU8         VTIMS            VTREQUEST    ".$fecha."      ".sprintf("%'.010d\n",$arr904[0]['sequence']).PHP_EOL);
		fwrite($fileRec,"LVVUL904                                                                        ".PHP_EOL);
		fwrite($fileRec,"LVVUL900                                                                        ".PHP_EOL);

	        
	    //B) DETALLE UNIDADES
	   	for ($i=0; $i <sizeof($arr904) ; $i++) {

	   		$fecha = date("YmdHis");

			fwrite($fileRec,sprintf('%-1s','A').sprintf('%-17s',$arr904[$i]['vin']).sprintf('%-6s','AJXTRA').$fecha."RA01".PHP_EOL);

			$vin = $arr904[$i]['vin'];
			$today = date("Y-m-d H:i:s");
			$fecha = substr($today,0,10);
			$hora=substr($today,11,8);
			$fecha1 = $arr904[$i]['vehicleDate']." ".$hora;


			$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".					 					
								"VALUES ('904','LZC02','".$arr904[$i]['sequence']."','".
								$arr904[$i]['vin'].
								"','".$fecha1.
								"','904','".
								$today."','".
								$varEstatus."','".
								$fecha."','".
								$hora."') ";    

			//fn_ejecuta_query($sqlAddTransaccion);

			//$sqlUpd ="UPDATE alinstruccionesmercedestbl set cveStatus='3G' WHERE vin ='".$arr904[$i]['vin']."' ";
			//fn_ejecuta_query($sqlUpd);

	    }
		//C) TRAILER
		/*$long=(sizeof($arr904));
		fwrite($fileRec,"GEE".sprintf('%06d',($long)).sprintf('%09s',$arr904[0]['sequence']));*/
	   	fclose($fileRec);
	   	//ftpArchivo ($nombreBusqueda);

	   	$fechaCopia = date("Y-m-d His");


		$updateFolioGM = "UPDATE trfoliostbl set folio=".$arr904[0]['sequence']." where centroDistribucion='LZC02' and compania='GM' and tipoDocumento='VT'";
		//fn_ejecuta_query($updateFolioGM);

		
	//	copy( "C:/servidores/DFYP301X", "E:/carbook/interfacesGM/respaldoS3/DFYP301X".$fechaCopia);

	//	exec("C:/Paso/ejecutaFtp.bat");

		echo "Fin VTIMS 904";
	}   	
?>  