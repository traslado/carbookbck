<?php
	require("../funciones/generales.php");
	require("../funciones/utilidades.php");

	global $dirPath;
	global $filePath;
	global $fileName;
	date_default_timezone_set('America/Mexico_City');

	$dirPath = "E:\\carbook\\informacionCP\\";
	$fileName= "CCPIMPORTACION.txt";
	$filePath = $dirPath.$fileName;
	$ejecutaProceso = true;
	$resPath = "E:\\carbook\\informacionCP\\respaldo\\";
    
	cargaInformacion();
	
	function cargaInformacion(){
		echo "Inicio carga: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

		global $dirPath;
		global $filePath;
		global $fileName;
		global $lineaStr;
		global $cont;

		$informacion = fopen($filePath, "r");//abres el archivo para lectura
		$fecha = date("d-m-Y_H-i-s");


		if(file($filePath)) {
			$cont = 1;


			while (!feof($informacion)) {	
				$lineaStr = fgets($informacion);


				$cabeceroStr = substr($lineaStr,0,3);			

				if ($cabeceroStr !='VIN') {
					//$lineaNueva=str_replace("", "|", $lineaStr);
					$sep=explode("|", $lineaStr);	
					
					$insert="INSERT INTO alinformacioncptbl (vin, descripcion, peso, unidadMedida, precioFactu, claveSat, pedimento, fracAran, puerto, rfcOrigen, razonSocialOrigen, codigoOrigen, direccionOrigen, distribuidor, codigoDestino, rfcDestino, razonSocialDestino, direccionDestino) VALUES ('".$sep[0]."', '".$sep[1]."', '".$sep[2]."', '".$sep[3]."', '".$sep[4]."', '".$sep[5]."', '".$sep[6]."', '".$sep[7]."', '".$sep[8]."', '".$sep[9]."', '".$sep[10]."', '".$sep[11]."', '".$sep[12]."', '".$sep[13]."', '".$sep[14]."', '".$sep[15]."', '".$sep[16]."', '".$sep[17]."');";
					$rs=fn_ejecuta_query($insert);	

				}

				

				$cont = $cont + 1;
			}
		}else {
        	echo "No se pudo abrir el archivo\r\n";
      	}

      	if(!is_dir($dirPath.'respaldo')) {
      		mkdir($dirPath.'respaldo');
      	}

      	copy($filePath, $dirPath.'respaldo\\CCPIMPORTACION'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
      	fclose($informacion);
     	unlink($filePath);
    
		
		echo "Termino carga: ".date("Y-m-d H:i", strtotime("now"))."\r\n";		     	
	}


?>