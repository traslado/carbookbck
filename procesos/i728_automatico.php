	<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");
	
	// Insercion de Unidadades en la temporal ordenado por HOLD y VIN -------------------------------
	$sqlGetUnds = "INSERT INTO alinterfasechryslertbl (claveScacCode,origenSplc, centroDistribucion,vin,  distribuidor, documentoVista, fechaTransmision, tipoMoneda)
					SELECT distinct a1.scaccode, a1.orisplc, h2.centroDistribucion, h2.vin, h2.distribuidor,'i728' as documentoVista, now() as fechaTransmision, h2.claveMovimiento
					FROM alhistoricounidadestbl h2, al660tbl a1
					WHERE h2.vin = a1.vin
						AND a1.vupdate = (SELECT MAX(a2.vupdate) FROM al660tbl a2 WHERE a2.vin = a1.vin)
						AND h2.claveMovimiento IN (SELECT ge.columna FROM cageneralestbl ge WHERE ge.tabla = 'i728')
						AND h2.vin NOT IN (SELECT VIN FROM altransaccionunidadtbl)
						AND h2.vin NOT IN (SELECT VIN FROM alinterfasechryslertbl)
					ORDER BY h2.claveMovimiento,vin;";

	$rsSqlGetUnds = fn_ejecuta_query($sqlGetUnds);

	echo json_encode($rsSqlGetUnds);
	// -----------------------------------------------------
						
	
	$sqlGetCentroDis = "SELECT distinct centroDistribucion
						FROM alinterfasechryslertbl;";

	$rsSqlGetCentroDis = fn_ejecuta_query($sqlGetCentroDis);

	echo json_encode($rsSqlGetCentroDis);
	
	// Actualizar OriSplc -------------------------------
	for ($i=0; $i < sizeof($rsSqlGetCentroDis['root']) ; $i++) { 
		if ($rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDSAL'){

			$splcUnd = '922786801';

			$sqlUpdSpl = "UPDATE alinterfasechryslertbl
							SET origenSplc = 922786801
							WHERE centroDistribucion = 'CDSAL'
							AND documentoVista = 'i728';";

			fn_ejecuta_query($sqlUpdSpl);				

			echo "saltillo";

		}elseif($rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDTOL'){
			
			$splcUnd = '958770807';

			$sqlUpdSpl = "UPDATE alinterfasechryslertbl
							SET origenSplc = 958770807
							WHERE centroDistribucion = 'CDTOL'
							AND documentoVista = 'i728';";

			fn_ejecuta_query($sqlUpdSpl);				
			echo "toluca";

		}elseif($rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDANG'){

			$splcUnd = '958770808';

			$sqlUpdSpl = "UPDATE alinterfasechryslertbl
							SET origenSplc = 958770808
							WHERE centroDistribucion = 'CDANG'
							AND documentoVista = 'i728';";

			fn_ejecuta_query($sqlUpdSpl);				

			echo "angeles";

		}elseif($rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDAGS'){
			$splcUnd = '940606000';

			$sqlUpdSpl = "UPDATE alinterfasechryslertbl
							SET origenSplc = 940606000
							WHERE centroDistribucion = 'CDAGS'
							AND documentoVista = 'i728';";

			fn_ejecuta_query($sqlUpdSpl);				

			echo "aguasCalientes";

		}elseif($rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDMAZ'){
			$splcUnd = '958770809';

			$sqlUpdSpl = "UPDATE alinterfasechryslertbl
							SET origenSplc = 958770809
							WHERE centroDistribucion = 'CDMAZ'
							AND documentoVista = 'i728';";

			fn_ejecuta_query($sqlUpdSpl);

			echo "mazatlan";				


		}elseif($rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDSFE' || $rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDMAT' ){
			$splcUnd = '978442999';

			$sqlUpdSpl = "UPDATE alinterfasechryslertbl
							SET origenSplc = 978442999
							WHERE centroDistribucion IN ('CDMAT','CDSFE')
							AND documentoVista = 'i728';";	

			fn_ejecuta_query($sqlUpdSpl);

			echo "santaFE";

		}elseif($rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDLZC'){
			$splcUnd = '999990907';

			$sqlUpdSpl = "UPDATE alinterfasechryslertbl
							SET origenSplc = 999990907
							WHERE centroDistribucion = 'CDLZC'
							AND documentoVista = 'i728';";

			fn_ejecuta_query($sqlUpdSpl);

			echo "lazaro";

		}elseif($rsSqlGetCentroDis['root'][$i]['centroDistribucion'] == 'CDVER'){
			$splcUnd = '978442999';

			$sqlUpdSpl = "UPDATE alinterfasechryslertbl
							SET origenSplc = 978442999
							WHERE centroDistribucion = 'CDVER'
							AND documentoVista = 'i728';";

			fn_ejecuta_query($sqlUpdSpl);

			echo "veracruz";				
		}						
	}
	// -----------------------------------------------------

		$fileDir="E:/carbook/i728/RAC550.FAL";
	    $fileLog = fopen($fileDir, 'a+');
		
		$segmento = 1;
		$countUnds = 1;
		$countSegSc = 1;

		$sqlGetHoldUnd = "SELECT distinct ic.origenSplc, ic.tipoMoneda, (SELECT ge.NOMBRE FROM cageneralestbl ge WHERE ge.columna = ic.tipoMoneda AND tabla = 'i728' ) as holdUnidad
							FROM alinterfasechryslertbl ic;";

		$rsGetHoldUnd = fn_ejecuta_query($sqlGetHoldUnd);

		for ($a=0; $a < sizeof($rsGetHoldUnd['root']) ; $a++) {

			$sqlGetUnds = "SELECT ic.vin, '".$rsGetHoldUnd['root'][$a]['tipoMoneda']."' as claveMovimiento, ic.fechaTransmision, ic.claveScacCode, ic.origenSplc, ".
							"(SELECT valor FROM cageneralestbl WHERE tabla = 'i728' AND columna = '".$rsGetHoldUnd['root'][$a]['tipoMoneda']."' ) as segType, ".
							"(SELECT SUBSTRING(a6.routedes, 1, 2) FROM al660tbl a6 WHERE a6.vin = ic.vin  AND a6.vupdate = (SELECT MAX(a1.vupdate) FROM al660tbl a1 WHERE a1.vin = a6.vin) limit 1) as routeDes ".
							"FROM alinterfasechryslertbl ic ".
							"WHERE documentoVista = 'i728' ".
							"AND origenSplc = '".$rsGetHoldUnd['root'][$a]['origenSplc']."' ".
							"AND tipoMoneda = '".$rsGetHoldUnd['root'][$a]['tipoMoneda']."'; ";

			$rsSqlGetUnds = fn_ejecuta_query($sqlGetUnds);

			$mSegType = $rsSqlGetUnds['root'][0]['segType'];
		    $mSplc = $rsSqlGetUnds['root'][0]['origenSplc'];
			

			$numTotal =sizeof($rsSqlGetUnds['root']);		

			
			// Cabecero de las unidades

			
			fwrite($fileLog,'ISA*03*RA550     *00*          *ZZ*XTRA           *ZZ*ADMISDCC       *'.date("ymd*Hi").'*U*00200*000000001*0*P>'.PHP_EOL); 			
		    fwrite($fileLog,'GS*VI*XTRA*VISTA*'.date("ymd*Hi").'*00001*T*1'.PHP_EOL);
		    fwrite($fileLog,'ST*550*00001'.str_pad($segmento,4,"0",STR_PAD_LEFT).PHP_EOL);
		    fwrite($fileLog,'BV5*'.$mSegType.'*XTRA*'.$rsGetHoldUnd['root'][$a]['origenSplc'].'*'.str_pad($numTotal,3,"0",STR_PAD_LEFT).'*'.$rsGetHoldUnd['root'][$a]['holdUnidad'].'*'.date("ymd").'***'.date("Hi").PHP_EOL);

		    // Por Unidad ...
		    for ($e=0; $e < sizeof($rsSqlGetUnds['root']); $e++) {		    	

		    	//fwrite($fileLog,'ST*550*000010001'.PHP_EOL);
		    	//fwrite($fileLog, 'BV5*E*XTRA*000000000*'.str_pad($numTotal,3,"0",STR_PAD_LEFT).'*'.$cMovimiento.'*'.date("ymd***Hi",strtotime($fMovimiento[$i])).PHP_EOL);
				fwrite($fileLog,'VI*'.$rsSqlGetUnds['root'][$e]['vin'].'*'.substr($rsSqlGetUnds['root'][$e]['vin'], 10,1).'*'.$rsSqlGetUnds['root'][$e]['routeDes'].'*****'.PHP_EOL);
				//fwrite($fileLog,'SE*'.str_pad(count($numTotal),6,"0",STR_PAD_LEFT).'*00001'.str_pad($numTotal,4,"0",STR_PAD_LEFT).PHP_EOL);
		    	//$e ++;
		    }
							 				
			fwrite($fileLog,'SE*'.str_pad(($numTotal + 1),6,"0",STR_PAD_LEFT).'*000010001'.PHP_EOL);
			fwrite($fileLog,'GE*000001*000001'.PHP_EOL);
			fwrite($fileLog,'IEA*01*000000000'.PHP_EOL);	 			 					
		}

		fclose($fileLog); 
		//subirFtp();

		$sqlAddH10 = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, folio, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento) ".
					"SELECT DISTINCT 'H10' AS tipoTransaccion, centroDistribucion, null AS folio, vin, fechaTransmision,'AA' as claveMovimiento, fechaTransmision ".
					"FROM alinterfasechryslertbl ".
					"WHERE documentoVista = 'i728';";

		fn_ejecuta_query($sqlAddH10);

		$sqlDelTmp = "DELETE FROM alinterfasechryslertbl ".
						"WHERE documentoVista = 'i728';";

		fn_ejecuta_query($sqlDelTmp);


