<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");	
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");
   	require_once("../funciones/phpexcel/PHPExcel.php");


    genera_436();

    function genera_436(){
    	
    	$sqlConsulta="SELECT a6.scacCode, a6.oriSplc, hu.vin, hu.fechaEvento, a6.routeOri, a6.routeDes, a6.desSplc ".
						"FROM alhistoricounidadestbl hu, alunidadestbl al, casimbolosunidadestbl ca, cadistribuidorescentrostbl di, al660tbl a6 ".
						"WHERE hu.centroDistribucion IN ('CDTOL','CDSAL','CDAGS','CDLZC','CDVER','CDSFE','CDANG') ".
						"AND hu.vin = al.vin ". 
						"AND ca.simboloUnidad=al.simboloUnidad ".
						"AND di.distribuidorCentro = al.distribuidor ".
						"AND di.tipoDistribuidor ='DX' ".
						"AND ca.marca not in('KI','HY') ".
						"AND hu.claveMovimiento IN ('OM','OK') ".
						"AND hu.fechaEvento between cast('2018-06-01' as date) and cast('2018-11-30' as date) ".
						"AND hu.idTarifa != '13' ".
						"AND hu.vin=a6.vin ".
						"AND a6.vupdate= (SELECT max(a66.vupdate) from al660tbl a66 where a66.vin=a6.vin) ".
						"AND a6.vuptime= (SELECT max(a660.vuptime) from al660tbl a660 where a660.vin=a6.vin) ".
						"AND a6.id660= (SELECT max(al660.id660) from al660tbl al660 where al660.vin=a6.vin) ".
						"AND a6.prodStatus!='SA' ".
						"AND hu.vin not in (SELECT tr.vin FROM altransaccionunidadtbl tr where tr.vin=hu.vin AND tipoTransaccion='540');"; 
		$rs=fn_ejecuta_query($sqlConsulta);

		if ($rs['records']!='0') {
			//fn_genera_archivo($rs['root'],$rs['records'],$countSegSc);
			generaEXCEL($rs);
			$countSeg[]=sizeof($rs['root']);

			$countSegSc=array_sum($countSeg);

			//echo $countSegSc;
		}
		else{
			echo "no existen unidades por transmitir";
		}	
	}			

	function generaEXCEL($rs){

		$Nombre = "540obt.xlsx";
		$formato = 'Excel2007';
		$Excel = new PHPExcel();

		date_default_timezone_set('America/Mexico_City');


		$fileDir="C:/carbook/i437obtexp/";

		$Excel->getProperties()->setCreator("CARBOOK")
		->setTitle("540obt");

		
		$Excel->setActiveSheetIndex(0);
		$Excel->getActiveSheet()->setTitle('Hoja1')
			->setCellValue('A1', "SCAC *")
			->setCellValue('B1', "SPLC ORIGIN *")
			->setCellValue('C1', "VIN *")
			->setCellValue('D1', "PICKUP DATE *")
			->setCellValue('E1', "ROUTE ORIGIN *")
			->setCellValue('F1', "ROUTE DESTINATION *")
			->setCellValue('G1', "DESTINATION SPLC *")
			->setCellValue('H1', "DELIVERY DATE *")
			->setCellValue('I1', "INVOICE NUMBER *");
			

		for ($i=0;  $i < count($rs['root']) ; $i++) {

		$Excel->setActiveSheetIndex(0);
		$Excel->getActiveSheet()->setTitle('Hoja1')		

			->setCellValue('A'. ($i+2), $rs['root'][$i]['oriSplc'])

			->setCellValue('B'. ($i+2), $rs['root'][$i]['oriSplc'])

			->setCellValue('C'. ($i+2), $rs['root'][$i]['vin'])

			->setCellValue('D'. ($i+2), $rs['root'][$i]['fechaEvento'])
  
			->setCellValue('E'. ($i+2), $rs['root'][$i]['routeOri'])

			->setCellValue('F'. ($i+2), $rs['root'][$i]['routeDes'])

			->setCellValue('G'. ($i+2), $rs['root'][0]['desSplc'])
			
			->setCellValue('H'. ($i+2), date("Y-m-d H:i:s"))
			
			->setCellValue('I'. ($i+2), 'consecutivo');
		}

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename="540obt.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($Excel, 'Excel2007');
		$objWriter->save($fileDir.$Nombre);	

	}

	echo json_encode(array('succes'=>true,'msjResponse'=>"Operacion completada correctamente"));           

?>  