<?php
 /*
      session_start();
	
	
	//include 'iVerificacionRepuve.php';
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");
	
		ejecutaRRL(); 
	*/

	
		function ejecutaRRL(){	

		echo "Inicio iRRL: ".date("Y-m-d H:i", strtotime("now"))."\r\n";  

        $sqlGTE = 	"SELECT dy.cveStatus ".
					"FROM alultimodetalletbl al, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = dy.vin ".
					"AND dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
					"AND SUBSTR(dy.nomFac,-1) !='M' ".
					"AND al.centroDistribucion='LZC02' ".
					"AND dy.cveStatus in ('TK','TY') ".
					"AND dy.trimCode is not null ".
					"AND dy.trimDesc is not null ".
					"AND dy.invPrice is not null ".
					"AND dy.fechaMovimiento is not null ".
					"AND dy.currency is not null ".	
					"AND dy.descColor is not null ".														
					"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='RRL') ".
					"AND dy.vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='RLS') ".
					"AND dy.vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='REP') ".
					"AND dy.vin in (select vin from alHistoricoUnidadesTbl where claveMovimiento='L1') ";

		$rsGTE= fn_ejecuta_query($sqlGTE);


		for ($i=0; $i <sizeof($rsGTE['root']) ; $i++) { 

			if (substr($rsGTE['root'][$i]['cveStatus'], -2) == 'TK') {				
				$arrK[] = $rsGTE['root'][$i];				
			}

			if (substr($rsGTE['root'][$i]['cveStatus'], -2) == 'TY') {				
				$arrH[] = $rsGTE['root'][$i];				
			}
		/////////////////////////////////////////////////////////
			else{
				
				//echo "string";
			}
		}
		if (count($arrK) != 0) {
			$varEstatus = 'TK';
			$nomArchivo = 'K';
			$portCodeIni = 'FT16';
			$portCodeFin = 'FT14';
			ejecutaRRL1($varEstatus,$nomArchivo,$portCodeIni,$portCodeFin);
			
		}		

		if (count($arrH) != 0) {
			$varEstatus = 'TY';
			$nomArchivo = 'H';
			$portCodeIni = '1145';
			$portCodeFin = '1150';
			ejecutaRRL1($varEstatus,$nomArchivo,$portCodeIni,$portCodeFin);
			
		}	

	echo "FIN iRRL: ".date("Y-m-d H:i", strtotime("now"))."\r\n";	
	
	} 


	function ejecutaRRL1($varEstatus,$nomArchivo,$portCodeIni,$portCodeFin){

		$sqlGenerarDatos= "SELECT im.vin,im.nomFac,im.trimCode,im.trimDesc,im.fEvento,im.hEvento,im.descColor as fechaEvento,im.fechaMovimiento,im.currency ".
						  "FROM alinstruccionesMercedestbl im, alHistoricoUnidadesTbl hu ".
						  "WHERE im.vin not in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='RRL')  ".
						  "AND im.vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='RLS') ".
						  "AND im.vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='REP') ".
						  "AND SUBSTR(im.nomFac,-1) !='M' ".
						  "AND im.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
						  "AND im.cveLoc='LZC02' ".
						  "AND im.vin=hu.vin ".
						  "AND hu.claveMovimiento='L1' ".
						  "AND im.trimCode is not null ".
						  "AND im.trimDesc is not null ".
						  "AND im.invPrice is not null ".
						  "AND im.descColor is not null ".	
						  "AND im.fechaMovimiento is not null ".
						  "AND im.currency is not null ".		
						  "AND im.cveStatus='".$varEstatus."' ".
						  "AND im.invPrice='P' ";

       	$rssqlGenerarDatos= fn_ejecuta_query($sqlGenerarDatos);
		
       	for ($i=0; $i <sizeof($rssqlGenerarDatos['root']) ; $i++) { 


			if (substr($rssqlGenerarDatos['root'][$i]['nomFac'], -1) !='M') {	
				$arrUnidades[] = $rssqlGenerarDatos['root'][$i];	

				}
			else {

				}
		}

		if (count($arrUnidades) != 0) {
		generaRRL1($arrUnidades,$varEstatus,$nomArchivo,$portCodeIni,$portCodeFin);
		
		}


	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function generaRRL1($arrUnidades,$varEstatus,$nomArchivo,$portCodeIni,$portCodeFin){

		$fecha = date('YmdHis');
	   	$today =  date('Y-m-d H:i:s');				
			
		//$directorio ="C:carbook/i816/";

		$directorio = "E:\\carbook\\archivosInterfacesGLOVIS\\respRRL\\";

		$inicioFile = $nomArchivo."MM_RRL_".$fecha.".txt";

		$nombreBusqueda = $nomArchivo."MM_RRL_".$fecha.".txt";

		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"RRLH APS  GMX  RRL".$fecha.PHP_EOL);

		//detalle

		for ($i=0; $i <sizeof($arrUnidades) ; $i++) {

			fwrite($archivo,"RRL  ".$portCodeIni." ".$arrUnidades[$i]['vin']."KCSM    ".$portCodeFin." ".substr($arrUnidades[$i]['fechaEvento'],0,4).substr($arrUnidades[$i]['fechaEvento'],5,2).substr($arrUnidades[$i]['fechaEvento'],8,2).substr($arrUnidades[$i]['fechaEvento'],11,2).substr($arrUnidades[$i]['fechaEvento'],14,2).substr($arrUnidades[$i]['fechaEvento'],17,2)."0000000000".sprintf('%-10s',$arrUnidades[$i]['trimCode'])."  ".sprintf('%-1s',$arrUnidades[$i]['currency']).sprintf('%-12s',$arrUnidades[$i]['trimDesc']).substr($arrUnidades[$i]['fechaMovimiento'],0,4).substr($arrUnidades[$i]['fechaMovimiento'],5,2).substr($arrUnidades[$i]['fechaMovimiento'],8,2).PHP_EOL);
			
				$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('RRL','LZC02','".$arrUnidades[$i]['vin']."', '".
										$arrUnidades[$i]['fechaEvento']."','L3', '".
										$today."', 'SO', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')" ;
			fn_ejecuta_query($insTransaccion);

			$UpdStatus= "UPDATE alinstruccionesmercedestbl set invPrice='G',cveStatus='SO' WHERE invPrice='P' and vin='".$arrUnidades[$i]['vin']."' and cveLoc='LZC02' ";

			fn_ejecuta_query($UpdStatus);
			//echo json_encode($insTransaccion);
		}
		//fin de archivo
		$long=(sizeof($arrUnidades)+2);
		fwrite($archivo,"RRLT ".sprintf('%06d',($long)));
		fclose($archivo);

		ftpArchivo($nombreBusqueda);
	}

		function ftpArchivo($nombreBusqueda){
			if(file_exists("E:/carbook/archivosInterfacesGLOVIS/respRRL/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/archivosInterfacesGLOVIS/respRRL/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/archivosInterfacesGLOVIS/respRRL/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							//echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			//echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}
       
?>