<?php
	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");


	date_default_timezone_set('America/Mexico_City');

	ejecutaEXT();

	function ejecutaEXT(){

		
	    $fechaAnt = strtotime ( '-15 day' , strtotime (date("Y-m-d")) ) ;
		$fechaAnt = date ( 'Y-m-d' , $fechaAnt );

		$fecha = date('Y-m-j');
		$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m-j' , $nuevafecha );

		$sqlEXT = "SELECT * FROM alunidadestbl al, alhistoricounidadestbl ud , alinstruccionesmercedestbl dy ".
					"WHERE al.vin = ud.vin ".
					"AND ud.vin = dy.vin ".
					"AND dy.cveStatus in('DK','DH') ".
					"AND ud.claveMovimiento ='L1' ".
					"AND ud.fechaEvento BETWEEN '".$fechaAnt."' AND '".$nuevafecha."' ".
					"AND ud.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='EL1') ";
		$rsEXT= fn_ejecuta_query($sqlEXT);

		//echo json_encode($rsEXT);	
		
		if (count($rsEXT['root']) != 0) {
			generaEXTKIA1($rsEXT);
			generaALBHYU($rsEXT);
		}			
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function generaEXTKIA1($arrKia){

		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');		


			
		

		if ($arrKia['root'][0]['cveStatus'] =='DK') {
			$nombre='KMM';			
		}
		if ($arrKia['root'][0]['cveStatus'] =='DH') {
			$nombre='HMM';
		}

		$directorio = "E:\\carbook\\i816Portin\\";

		$inicioFile = $nombre."_EXT_".$fecha.$hora.".txt";
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"EXTH"." "."TRA"."  "."GMX"."  "."EXT".$fecha.$hora.PHP_EOL);

		//detalle
		for ($i=0; $i <sizeof($arrKia['root']) ; $i++) { 

			
	 ///ft16 origen ft14 destino    
			$origin= 'FT16';
			$destino='FT14';

				

			fwrite($archivo,"EXT  RU".$arrKia['root'][$i]['vin'].substr($arrKia['root'][$i]['fechaEvento'],0,4).substr($arrKia['root'][$i]['fechaEvento'],5,2).
				substr($arrKia['root'][$i]['fechaEvento'],8,2).substr($arrKia['root'][$i]['fechaEvento'],11,2).substr($arrKia['root'][$i]['fechaEvento'],14,2).
				substr($arrKia['root'][$i]['fechaEvento'],17,2).sprintf('%-5s',($origin)).sprintf('%-5s',($arrKia['root'][$i]['distribuidor']))."          "."TRA  T                            ".PHP_EOL);
			
			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('EL1', '".
										$arrKia['root'][$i]['centroDistribucion']."', '".
										$arrKia['root'][$i]['vin']."', '".
										$arrKia['root'][$i]['fechaEvento']."', '".
										$arrKia['root'][$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			//fn_ejecuta_query($insTransaccion);
			//echo json_encode($insTransaccion);
			
		}
		//fin de archivo
		$long=(sizeof($arrKia['root'])+2);
		fwrite($archivo,"EXTT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);	
	}

	function generaALBHYU($arrHyu){

		//echo json_encode($arrHyu['root'][0]['cveStatus']);

		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');	

	   
		if ($arrHyu['root'][0]['cveStatus'] =='DK') {
			$nombre='KMM';			
			$chipNum='9';
		}
		if ($arrHyu['root'][0]['cveStatus'] =='DH') {
			$nombre='HMM';
			$chipNum='0';
		}
			
			
		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816Portin\\";

		$inicioFile = $nombre."_ALB_".$fecha.$hora.".txt";
		$archivo = fopen($directorio.$inicioFile,"w");

		echo $inicioFile;

		//encabezado
		fwrite($archivo,"ALBH"." "."TRA"."  "."GMX"."  "."ALB".$fecha.$hora.PHP_EOL);

		//detalle
		$sqlFolioStr = "SELECT * FROM trfoliostbl WHERE tipoDocumento='TH' AND compania='"./*$KIA[$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrHyu['root'][0]['centroDistribucion']."'";
		$FolioRst = fn_ejecuta_query($sqlFolioStr);
		//echo json_encode($FolioRst);

		$folio = $FolioRst['root'][0]['folio'] + 1;

		$sqlActualizaFolioStr="UPDATE trfoliostbl SET folio =". $folio." where tipoDocumento='TH' and compania='"./*$rsTLR['root'][$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrHyu['root'][0]['centroDistribucion']."'";
		$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

		//echo json_encode($FolioRst['root'][0]['folio']);
		$contador==0;

		for ($i=0; $i <sizeof($arrHyu['root']) ; $i++) {

			$contador++;

			$sqlCveDisFat ="SELECT * FROM cadistribuidorescentrostbl ".
							"WHERE distribuidorCentro ='".$arrHyu['root'][$i]['cveDisFac']."'";
			$rsCve = fn_ejecuta_query($sqlCveDisFat);
			//echo json_encode($rsCve);

			if ($rsCve['records'] != 0) {
				$cveDis ='C';
			}
			else{
				$cveDis ='D';
			}
			

			/*$sqlTractor ="SELECT  ca.tractor ".
							"FROM trunidadesdetallestalonestbl ud , trtalonesviajestbl tl, trviajestractorestbl vi, catractorestbl ca ".
							"WHERE ud.vin ='".$arrHyu['root'][$i]['vin']."' ".
							"AND ud.estatus =1 ".
							"AND ud.idTalon = tl.idTalon ".
							"AND tl.idViajeTractor = vi.idViajeTractor ".
							"AND vi.idTractor = ca.idTractor ";
			$rsTractor =fn_ejecuta_query($sqlTractor);*/
			//echo json_encode($rsTractor);

			$origin= 'FT16';
			$destino='FT14';				

				fwrite($archivo,"ALB"."  ".substr($arrHyu['root'][$i]['fechaEvento'],0,4).substr($arrHyu['root'][$i]['fechaEvento'],5,2).
				substr($arrHyu['root'][$i]['fechaEvento'],8,2).substr($arrHyu['root'][$i]['fechaEvento'],11,2).substr($arrHyu['root'][$i]['fechaEvento'],14,2).substr($arrHyu['root'][$i]['fechaEvento'],17,2));
				fwrite($archivo,$arrHyu['root'][$i]['vin']."P".sprintf("%-5s",$origin).$cveDis.sprintf("%-5s",$arrHyu['root'][$i]['cveDisFac'])."D".$chipNum.sprintf('%09d',$contador)."  000000000000000");
				fwrite($archivo,substr($arrHyu['root'][$i]['fechaEvento'],0,4).substr($arrHyu['root'][$i]['fechaEvento'],5,2).substr($arrHyu['root'][$i]['fechaEvento'],8,2));
				fwrite($archivo,$arrHyu['root'][$i]['folio'].sprintf('%06d',$FolioRst['root'][0]['folio']).sprintf('%06d',$rsTractor['root'][0]['tractor'])."09".sprintF('%02d',sizeof($arrHyu['root'])).$arrHyu['root'][$i]['distribuidor'].PHP_EOL);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('AL1', '".
										$arrHyu['root'][$i]['centroDistribucion']."', '".
										$arrHyu['root'][$i]['vin']."', '".
										$arrHyu['root'][$i]['fechaEvento']."', '".
										$arrHyu['root'][$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			//fn_ejecuta_query($insTransaccion);
	

			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='EH' WHERE vin='".$arrHyu['root'][$i]['vin']."'"." AND cveStatus ='AH'" ;
			$rstUpt=fn_ejecuta_query($uptdy);
			
			
			}
			//fin de archivo
			$long=(sizeof($arrHyu['root'])+2);
			fwrite($archivo,"ALBT ".sprintf('%06d',($long)).PHP_EOL);
			fclose($archivo);
	}
?>