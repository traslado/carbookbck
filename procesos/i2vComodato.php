<?php
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");


    $startDate = date('Y-m-d', strtotime("-5 days")); //- 5 días
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $today = date('Y-m-d');
    $todayDateTime = date('Y-m-d H:i:s');


    $entraReporte = false;

    $rsUnidadesUnion = getHistoricoUnionUltimoDetalle340($startDate, $today);
    
    //$rsUnidadesUnion = getHistoricoUnidades340($startDate, $today);
    //echo getHistoricoUnidades340($startDate, $today, true);
    //echo json_encode($rsUnidadesUnion);

    $unidadesCon660 = array();

    $unidadesSin660 = array();

    echo json_encode($rsUnidadesUnion['root']);


    foreach ($rsUnidadesUnion['root'] as $unidad) {

        $ciaLoc = getCiaLocFromLocalidad($unidad['localizacionUnidad']);

        if (isDuplicatedTransaccionByCiaLoc($unidad, $ciaLoc, $today) == true)
            continue;

        //Si la Unidad tiene 660
        if ($unidad['en660'] == '1'){

            $fieldsName = array(
                            substr($unidad['localizacionUnidad'], 2),
                            $unidad['vin'],
                            $unidad['scaccode'], 
                            $unidad['simboloUnidad'], '|', 
                            $unidad['tipoOrigenUnidad'], '|'
                            );

            if (!isset($unidadesCon660[$unidad['localizacionUnidad']]) ){ // Si no existe crea el arreglo
                
                $unidadesCon660[$unidad['localizacionUnidad']] = array();
                $unidadesCon660[$unidad['localizacionUnidad']][$fieldsName[0]] = array();
                $unidadesCon660[$unidad['localizacionUnidad']][$fieldsName[0]][] = $fieldsName;

            } else {
                if (!isset($unidadesCon660[$unidad['localizacionUnidad']][$fieldsName[0]])){

                    $unidadesCon660[$unidad['localizacionUnidad']][$fieldsName[0]] = array();
                    $unidadesCon660[$unidad['localizacionUnidad']][$fieldsName[0]][] = $fieldsName;

                } else {
                    $unidadesCon660[$unidad['localizacionUnidad']][$fieldsName[0]][] = $fieldsName;
                }
            }
            
        } else {
            //GENERA REGISTRO EN EL REPORTE ASN2V(LOG)
            $generaReporte = true;
            //DETERMINAR lw_scac_code lw_anio en base al scaccode

            $fieldsName = array(
                            0 => substr($unidad['centroDistribucion'], 2),   3 => '|', 
                            //4 => isset($lws_anio($unidad['vin'][0])) ? $lws_anio($unidad['vin'][0]) : $unidad['vin'][0],
                            5 => $unidad['vin'],                             12 => '|',
                            13 =>'R2V',                                      16 => '|', 
                            17 => date_format(date_create($today), 'd/m/Y'), 27 => '|',
                            28 => date('H:i:s'),                             36 => '|', 
                            //37 => isset($lws_scac_code($unidad['vin'][0])) ? $lw_scac_code($unidad['vin'][0]) : 'XTRA' , 41 => '|',
                            42 => $unidad['simboloUnidad'],                  48 => '|', 
                            49 => $unidad['tipoOrigenUnidad'],               50 => '|'
                            );

            $unidadesSin660[] = $fieldsName;
        }
    }

    if ($generaReporte){
        
        //$fileDir = $_SERVER['DOCUMENT_ROOT']."/ASN2V.txt";
        $fileDir = "E:/carbook/i340/ASN2V.txt";
        $reporteSin660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");

        foreach ($unidadesSin660 as $unidadDatos) {
            fwrite($reporteSin660, getTxt($unidadDatos, $unidades).out('n', 1));
        }

        fclose($reporteSin660);
    }

        generarReporte660($unidadesCon660);


    function generarReporte660($unidadesGroupedByCD){

        $centrosDistribucion = $unidadesGroupedByCD;

        //DEFINE VARIABLES lw_ramp, lw_cod, lw_marca
        // lw_ramp = 'ADIMSRAMPMX' + ### + ' '(un blanco)
        $lw_ramp = 'ADIMSRAMPMX';
        $lws_ramp = array('SAL'   => '806', 'TOL'   => '807',   'TRAC1' => '807',
                          'TRAC2' => '890', 'TRAC3' => '891',   'TRAC4' => '892',
                          'TRAC5' => '893', 'TRAC6' => '894',   'TRAC7' => '895',
                          'ANG'   => '808', 'MAZ'   => '809',   'AGS'   => '011',
                          'VER'   => '805', 'SFE'   => '988',   'CUA'   => '888', 'LZC' => '816');
        //--------------------------------------------------------------------------------------------
        // lw_cod
        $lws_cod = array('SAL'   => 'X6', 'TOL'   => 'X7',   'TRAC1' => 'X7',
                          'TRAC2' => 'YB', 'TRAC3' => 'YC',   'TRAC4' => 'YD',
                          'TRAC5' => 'YE', 'TRAC6' => 'YF',   'TRAC7' => 'YG',
                          'ANG'   => 'X8', 'MAZ'   => 'X9',   'AGS'   => 'X4',
                          'VER'   => 'MX', 'SFE'   => 'XO',   'CUA'   => 'XU', 'LZC' => 'PT');
        //--------------------------------------------------------------------------------------------
        
        //$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
        $fileDir = "E:/carbook/i340/RA2V.txt";
        $flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
        $recordsPositionValue = array();
        foreach ($centrosDistribucion as $keyCD => $centroDistribucion) {       
            foreach ($centroDistribucion as $nombrePatio => $patioGroup) {
                //A) ENCABEZADO
                $record = array( 0 =>  'ISA*03*RA2VE     *00*          *ZZ*',
                                35 => $lw_ramp . $lws_ramp[$nombrePatio].' ',
                                50 => "*ZZ*ADIMS          *",
                                70 => date_format(date_create($today), 'y'),
                                72 => date_format(date_create($today), 'm'),
                                74 => date_format(date_create($today), 'd'),
                                76 => '*',
                                77 => date_format(date_create(date('H:i:s')), 'H'),
                                79 => date_format(date_create(date('H:i:s')), 'i'),
                                81 => '*U*00300*',
                                90 => sprintf('%09d', count($patioGroup)),
                                99 => '*0*P*'
                               );

                $recordsPositionValue[] = $record;
                $headerTxt = getTxt($record, array_keys($record));
                fwrite($flReporte660, $headerTxt.out('n', 1));

                //B) DETALLE UNIDADES
                foreach ($patioGroup as $unidad) {
                    $lw_marca = 'C';
                    if(ord(strtolower($unidad[0][0])) >= 97 && ord(strtolower($unidad[0][0])) <= 122) // Rango A - Z
                        $lw_marca = 'M'; //MITSUBISHI
                    if($unidad[2] == 'CHMO')
                        $lw_marca = '3'; // HYUNDAI

                    $record = array( 0 =>  '2V',
                                     2 => $lws_cod[$nombrePatio],
                                     4 => date('mdy'),
                                    10 => $unidad[1],
                                    27 => $nombrePatio,
                                    32 => out('s', 27),
                                    59 => 'A',
                                    60 => out('s', 14),
                                    74 => date_format(date_create($todayDateTime), 'Hi'), 
                                    78 => 'N',
                                    79 => $lw_marca,
                                    80 => out('s', 5)
                                   );

                    $detalleTxt = getTxt($record, array_keys($record));
                    fwrite($flReporte660, $detalleTxt.out('n', 1));
                }
                //C) TRAILER
                $record = array(0 => 'IEA*01*', 7 => sprintf('%09d', count($patioGroup)));
                $recordsPositionValue[] = $record;
                $trailerTxt = getTxt($record, array_keys($record));
                fwrite($flReporte660, $trailerTxt);
            }
        }
        fclose($flReporte660);
        //echo json_encode($recordsPositionValue);
    }

    function insertarTransacciones($rsCentrosDistribucion, $tipoTransaccion, $dateTime) {
        
        if (!isset($rsCentrosDistribucion) || count($rsCentrosDistribucion) < 1)
            return;

        $sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl".
                                "(tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, ".
                                "claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) VALUES";

        foreach ($rsCentrosDistribucion as $nombreCentro => $rsCentroDistribucion) {
            foreach ($rsCentroDistribucion as $nombrePatio => $rsPatio) {
                $i = 0;
                foreach ($rsPatio as $unidad) {
                    if($i++ > 0)
                        $sqlInsertTransaccion .= ', ';

                    $vupdate = DateTime::createFromFormat('d/m/Y', $unidad['vupdate']);
                    $vupdate = date_format($vupdate, 'Y-m-d');
                    
                    $vuptime = $unidad['vuptime'];
                    if(strlen($unidad['vuptime']) > 5)
                        $vuptime = substr($unidad['vuptime'], 0, 5);

                    $sqlInsertTransaccion .= "('$tipoTransaccion', '$unidad[centroDistribucion]', '$unidad[vin]', '$dateTime', ".
                                             "'$unidad[claveMovimiento]', '$unidad[fechaEvento]', '$unidad[prodstatus]', ".
                                             "'$vupdate', '$vuptime')";

                }
            }
        }
        
        fn_ejecuta_query($sqlInsertTransaccion.';');

    }
    
    function getHistoricoUnidades340($startDate, $endDate, $sendAsString = false){ //sendAsString variable Opcional

        $sqlHistoricoUnidades = "SELECT h.centroDistribucion, h.vin, h.fechaEvento, h.claveMovimiento, h.distribuidor, h.idTarifa, h.localizacionUnidad, dc.sucursalDe,'HISTORICO' AS tipoTabla ".
                                "FROM alHistoricoUnidadesTbl h, caDistribuidoresCentrosTbl dc ".
                                "WHERE h.centroDistribucion = dc.distribuidorCentro ".
                                "  AND (h.centroDistribucion IN ('CMDAT') ".
                                "  AND h.claveMovimiento IN ('IC'))";
                                
        return $sendAsString ? $sqlHistoricoUnidades : fn_ejecuta_query($sqlHistoricoUnidades.";");

    }


    function getUltimoDetalleUnidades340($startDate, $endDate, $sendAsString = false){  //sendAsString variable Opcional

        $sqlIdTarifa13 = "(SELECT t.idTarifa FROM caTarifasTbl t WHERE t.tarifa = '13')";

        $sqlUltimoDetalleUnidades = "SELECT ud.centroDistribucion, ud.vin, ud.fechaEvento, ud.claveMovimiento, ".
                                    "ud.distribuidor, ud.idTarifa, ud.localizacionUnidad, dc.sucursalDe, ".
                                    "'ULTIMODETALLE' AS tipoTabla ".
                                    "FROM alUltimoDetalleTbl ud, caDistribuidoresCentrosTbl dc ".
                                    "WHERE ud.centroDistribucion = dc.distribuidorCentro ".
                                    "AND ud.idTarifa <> $sqlIdTarifa13 ".
                                    "AND ud.centroDistribucion IN ('CDTOL', 'CDSAL', 'CDAGS', 'CDANG', 'CDCUA', 'CDLZC', 'CDVER', 'CDSFE') ".
                                    "AND ud.claveMovimiento = 'TC' ";

        return $sendAsString ? $sqlUltimoDetalleUnidades : fn_ejecuta_query($sqlUltimoDetalleUnidades.";");
    }


    function getHistoricoUnionUltimoDetalle340($startDate, $endDate){

         $sqlIdTarifa13 = "(SELECT t.idTarifa FROM caTarifasTbl t WHERE t.tarifa = '13')";

         $sqlExcluirDY = "(SELECT im.vin FROM alInstruccionesMercedesTbl im)";

         $sqlExistePuerto = "(SELECT (SELECT 1 FROM caDistribuidoresCentrosTbl ex WHERE hud.distribuidor = ex.distribuidorCentro AND ex.tipoDistribuidor = 'DX') IS NOT NULL)";

         $sqlEstaEn660 = "(SELECT (SELECT 1 FROM al660Tbl a6 WHERE hud.vin = a6.vin) IS NOT NULL)";

         $sqlCabeceroUnidad = "(SELECT u.distribuidor FROM alUnidadesTbl u WHERE hud.vin = u.vin)";

         $sqlScacCode660 = "(SELECT a6.scaccode FROM al660Tbl a6 WHERE a6.vin = hud.vin)";

         $sqlScacCode660 =   "(SELECT a6.scaccode, MAX(a6.vuptime) FROM al660tbl a6, ".
                             "(SELECT a6.vin, MAX(a6.vupdate) AS maxVupDate FROM al660tbl a6, alhistoricounidadestbl h ".
                             "WHERE a6.vin = h.vin ".
                             "GROUP BY a6.vin) AS tmp ".
                             "WHERE a6.vin = tmp.vin ".
                             "AND a6.vupdate = tmp.maxVupDate ".
                             "GROUP BY a6.vin) AS filtered660 ".

         $sqlExcluirMercedes = "($sqlEstaEn660 = 1 AND $sqlScacCode660 IS NOT NULL AND $sqlCabeceroUnidad NOT LIKE 'H%')";

         $sqlTransaccionDuplicada = "(SELECT (SELECT 1 FROM alTransaccionUnidadTbl tu ".
                                    "WHERE tu.tipoTransaccion = 'H10' ".
                                    "AND tu.vin = hud.vin ".
                                    "AND tu.centroDistribucion = hud.centroDistribucion ".
                                    "AND tu.claveMovimiento = hud.claveMovimiento) IS NOT NULL)";

         $sqlRA3Existente = "(SELECT (SELECT 1 FROM alTransaccionUnidadTbl tu ".
                                    "WHERE tu.tipoTransaccion = 'RA3' ".
                                    "AND tu.vin = hud.vin ".
                                    "AND tu.centroDistribucion = hud.centroDistribucion ".
                                    "AND DATE(tu.fechaGeneracionUnidad) = $endDate) IS NOT NULL)";

         $sqlHistorico = getHistoricoUnidades340($startDate, $endDate, true);
         $sqlUltimoDetalle = getUltimoDetalleUnidades340($startDate, $endDate, true);
         $sqlUnidadesUnionStr = "(".$sqlHistorico." UNION ".$sqlUltimoDetalle.")";

         $sqlUnidadesUnion = "SELECT hud.centroDistribucion, hud.vin, u.simboloUnidad, su.tipoOrigenUnidad, ".
                             "hud.fechaEvento, hud.claveMovimiento, hud.idTarifa, hud.distribuidor, ".
                             "hud.localizacionUnidad, hud.sucursalDe, hud.tieneTarifa13, ".
                             "hud.en660, hud.esDiaActual, hud.claveMovimientoTalon, hud.scacode, ".
                             "hud.distribuidorCabecero, hud.tipoTabla, hud.patioTransaccion, hud.tipoTransaccion ".
                             "FROM $sqlUnidadesUnionStr AS hud, alUnidadesTbl u, caSimbolosUnidadesTbl su ".
                             "WHERE hud.vin = u.vin ".
                             "AND u.simboloUnidad = su.simboloUnidad ".
                             "AND hud.distribuidor NOT LIKE 'H%' ".
                             "AND hud.vin NOT IN $sqlExcluirDY ".
                             "AND (hud.idTarifa <> $sqlIdTarifa13 ".
                             "OR  (hud.idTarifa = $sqlIdTarifa13 AND DATE(hud.fechaEvento) = '$endDate' AND $sqlExistePuerto = 0)) ".
                             "AND $sqlTransaccionDuplicada = 0 ".
                             "AND $sqlExcluirMercedes ".
                             "AND $sqlRA3Existente = 0 ;";
         
         return fn_ejecuta_query($sqlUnidadesUnion);
    }


    function getTxt($texts, $positions){
        $text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = $positions[$i - 1];
                $antLength = strlen($texts[$positions[$i - 1]]);
            }
            $text .= out('s', $positions[$i] - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }

?>