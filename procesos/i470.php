<?php
	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	include_once("../funciones/utilidades.php");	
	require_once("../funciones/utilidadesProcesos.php");

	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['int470Hdn']){
        case 'valida':
            valida();
            break;
        case 'inserta':
            generaInterfase();
            break;
        default:
            echo '';
    }

	function valida(){
		$a = array('success' => true, 'msjResponse' => '', 'fechaInicio'=>'', 'fechaFinal'=>'');
		
		/*
		$sql = "SELECT * FROM trInterfacesFechaTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					 " AND tipoInterface = 'A'".    				 # Anticipos de Gastos
					 " AND SUBSTRING(fechaInicio,1,10) = '".$_REQUEST['fechaInicio']."'".
					 " AND SUBSTRING(fechaFinal,1,10) = '".$_REQUEST['fechaInicio']."'";
		//echo "$sql<br>";
		$rsFecha = fn_ejecuta_query($sql);
		//var_dump($rsFecha);
		*/

		if($a['success'])
		{
				setlocale(LC_TIME, 'spanish');
				$a['fechaInicio'] = $_REQUEST['anio']."-".$_REQUEST['mes']."-01";
				$dia 		= ultimoDiaMes($_REQUEST['anio'], $_REQUEST['mes']);
				$a['fechaFinal'] = $_REQUEST['anio']."-".$_REQUEST['mes']."-".$dia;
		}
		
		echo json_encode($a);
	}
		
	function generaInterfase(){
		$a = array('success' => true, 'msjResponse' => 'Interfase generada correctamente.', 'nombreArchivo'=>'', 'existeTmp'=>0);
		
		$dir 	= "E:/carbook/i470/respArchivo/";
		$arch = strtoupper($_REQUEST['nombreArchivo']);
		
		$sql = "SELECT valor AS centroDistribucion FROM caGeneralesTbl".
					 " WHERE tabla = 'trpolizainterfacetbl' ".
					 //" and valor = 'CDTOL'".		//CHK del		s�lo para probar
					 " ORDER BY idGenerales";
		//echo "$sql<br>";
		$rsCentro = fn_ejecuta_query($sql);
		//var_dump($rsCtas);		
		
		$a['nombreArchivo'] = $dir.$arch;
		$fp = fopen($a['nombreArchivo'], 'w');
		$cont = 0;
		foreach($rsCentro['root'] AS $index => $row)
		{
				$sql = "SELECT b.centroDistribucion,d.claveChofer,b.folio,c.nombre, SUBSTRING(c.apellidoPaterno,1,12) AS apellidoPaterno,b.concepto,b.subTotal".
							 " FROM trPolizaInterfaceTbl a, trGastosViajeTractorTbl b, caChoferesTbl c, trViajesTractoresTbl d".
							 " WHERE a.centroDistribucion = '".$row['centroDistribucion']."'".
							 " AND SUBSTRING(a.fechaMovimiento,1,10) BETWEEN '".$_REQUEST['fechaInicial']."' AND '".$_REQUEST['fechaFinal']."'".
							 " AND b.centroDistribucion = '".$row['centroDistribucion']."'".
							 " AND b.folio = a.folio".
							 " AND SUBSTRING(b.fechaEvento,1,10) = SUBSTRING(a.fechaPoliza,1,10)".
							 " AND (b.concepto IN (SELECT valor FROM cageneralestbl WHERE tabla = 'trPolizaGastosTbl' AND columna = 'deducciones' AND valor != '2233')".
							 " OR b.concepto IN ('138','136','149'))".
							 " AND d.idViajeTractor = a.idViajeTractor".
							 " AND d.claveChofer = c.claveChofer".
							 " AND b.claveMovimiento != 'X'";
				//echo "$sql<br>";		
				$rsSueldos = fn_ejecuta_query($sql);
				//var_dump($rsGastos);
				
				$idx = 0;			
				foreach($rsSueldos['root'] AS $index2 => $row2)
				{						
						fputs($fp, $row2['centroDistribucion']. '|'. $row2['claveChofer']. '|'. $row2['folio']. '|'. $row2['nombre']. '|'. $row2['apellidoPaterno']. '|'. $row2['concepto']. '|'. $row2['subTotal']. '|');
						fputs($fp,"\n");
						$cont++;
				}				
		}
		
		fclose($fp);
		
		//Verifica si existe el directorio tmp, sino lo crea
		if (!file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				mkdir('../../carbookv1.2/modules/interfacesContables/tmp/', 0777);
		}
		if (file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				copy($a['nombreArchivo'], '../../carbookv1.2/modules/interfacesContables/tmp/'.$arch);
				$a['existeTmp'] = 1;
		}		
		
		if($cont == 0)
		{
				$a['msjResponse'] = 'No existe informaci&oacuten a transmitir para sueldos entre las fechas.';
				$a['success'] = false;
		}

    echo json_encode($a);
	}
	
	function ultimoDiaMes($anio, $mes) {
	  return str_pad(date("d",(mktime(0,0,0,$mes+1,1,$anio)-1)), 2, "0", STR_PAD_LEFT);
	}	
?>
