<?php

	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");


	date_default_timezone_set('America/Mexico_City');
	

   	ejecutaS6();

    function ejecutaS6(){	

		$sqlUnidS9=	"SELECT  case when substr(dy.cveDisFac,-5) in('77831','77828','77827','77848','77829','77830','72148','31112') then 'G' else 'M' end as shipper, dy.vin, '' as orderNumber, 'S9' as eventCode , substring(hu.fechaEvento,1,10) as  vehicleDate, ".
								"substring(dy.cveDisFac,1,2) as dealer,'LC' as portCode, '' as portDischarge, dy.currency,'' as volume ,substring(dy.cveDisFac,3) as ship,substring(hu.fechaEvento,1,10) as dateVehicle, ".
						        "case when da.vin is not null then 'HR' else '' end as HRoHQ,case when da.vin is not null then concat(cat.areaDano,cat.tipoDano,cat.severidadDano)  else '' end  as holdCodes, ".
						        "case when da.vin is not null then da.observacion else '' end as   holdAdditional,ca.descripcion,'' as markModCode,'' as vehicleTrans, '' as engineNumber, '' as modelPackcage, ".
						        "'' as carrierCut,  case when da.vin is not null then dy.fped else dy.fped end as vesselName,'' as vesselNumber,'' as voyageNumber, '' as containerNumber,'' as containerSeal,'' as timerQ, ".
						        "concat(substring(hu.fechaEvento,12,2),substring(hu.fechaEvento,15,2),substring(hu.fechaEvento,18,2)) as eventTime,'TRAM' as sender, ".
						        "(SELECT folio +1 FROM trfoliostbl WHERE centroDistribucion='LZC02' AND compania ='GM' AND tipoDocumento ='GM') as sequence ".
						"FROM alinstruccionesmercedestbl dy inner join alhistoricounidadestbl hu on hu.vin = dy.vin inner join casimbolosunidadestbl ca on ca.simboloUnidad = dy.modelDesc ".
						"left join  aldanosunidadestbl da on  da.vin = dy.vin  left join cadanostbl cat on da.iddano = cat.iddano left join cadistribuidorescentrostbl di on di.distribuidorCentro = dy.cveDisEnt ".
						"WHERE dy.cveStatus='6G' ".
						"AND dy.cveLoc='LZC02' ".
						"AND di.tipoDistribuidor='DX' ".
						"AND hu.claveMovimiento='L3' ".
						"AND hu.vin not in (SELECT ti.vin FROM altransaccionunidadtbl ti WHERE ti.vin = hu.vin AND ti.tipoTransaccion in('IS9')) ".
						"AND hu.vin  in (SELECT ti.vin FROM altransaccionunidadtbl ti WHERE ti.vin = hu.vin AND ti.tipoTransaccion in('IS6')) ".
						"order by hu.vin" ;
		$rsSqlUnidS9= fn_ejecuta_query($sqlUnidS9);

		echo json_encode($rsSqlUnidS9);


		if (count($rsSqlUnidS9['root']) != 0) {

			//echo json_encode($rsSqlUnidS9);

			$fecha = date("YmdHis");
			$directorio = "E:/carbook/interfacesGM/S9/";
			$inicioFile = "DFYP301X";
			$archivo = fopen($directorio.$inicioFile,"w");
			$nombreBusqueda = "DFYP301X-S6";
			
			$today = date("Y-m-d H:i:s");
			//$fecha = substr($today,0,10);
			$nombreBusqueda = "DFYP301X";

			//encabezado
			fwrite($archivo,"GESGEIS9.000  TRAM             DFYDETROIT$".$fecha.sprintf('%09s',$rsSqlUnidS9['root'][0]['sequence'])."EST".PHP_EOL);

			//detalle

			for ($i=0; $i <sizeof($rsSqlUnidS9['root']) ; $i++) {
					

				fwrite($archivo,sprintf('%-1s',$rsSqlUnidS9['root'][$i]['shipper']).sprintf('%-17s',$rsSqlUnidS9['root'][$i]['vin']).sprintf('%-6s',$rsSqlUnidS9['root'][$i]['orderNumber']).sprintf('%-2s',$rsSqlUnidS9['root'][$i]['eventCode']).sprintf('%-10s',$rsSqlUnidS9['root'][$i]['vehicleDate']).sprintf('%-9s',$rsSqlUnidS9['root'][$i]['dealer']).sprintf('%-3s',$rsSqlUnidS9['root'][$i]['portCode']).sprintf('%-5s',$rsSqlUnidS9['root'][$i]['portDischarge']).sprintf('%-5s',$rsSqlUnidS9['root'][$i]['currency']).sprintf('%-7s',$rsSqlUnidS9['root'][$i]['volume']).sprintf('%-25s',$rsSqlUnidS9['root'][$i]['vesselName']).sprintf('%-10s',$rsSqlUnidS9['root'][$i]['dateVehicle']).sprintf('%-30s',$rsSqlUnidS9['root'][$i]['storageLocation']).sprintf('%-15s',$rsSqlUnidS9['root'][$i]['storageNumber']).sprintf('%-14s',$rsSqlUnidS9['root'][$i]['futureUse']).sprintf('%-3s',$rsSqlUnidS9['root'][$i]['timerQ']).sprintf('%-6s',$rsSqlUnidS9['root'][$i]['eventTime']).sprintf('%-12s',$rsSqlUnidS9['root'][$i]['containerNumber']).sprintf('%-11s',$rsSqlUnidS9['root'][$i]['containerSealNumber']).sprintf('%-109s',$rsSqlUnidS9['root'][$i]['reservedForFutureUse']).PHP_EOL);

				$today = date("Y-m-d H:i:s");
				$hora1=substr($today,11,8);
				$fecha = substr($today,0,10);
				$fecha1 = $rsSqlUnidS9['root'][$i]['vehicleDate']." ".$hora1;

				
				$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".					 				
										"VALUES ('IS9','LZC02','".$rsSqlUnidS9['root'][$i]['sequence']."','".
										$rsSqlUnidS9['root'][$i]['vin'].
										"','".$fecha1.
										"','S9','".
										$today."','".
										$varEstatus."','".
										$fecha."','".
										$hora1."') ";    

				fn_ejecuta_query($sqlAddTransaccion);

				$sqlUpd ="UPDATE alinstruccionesmercedestbl set cveStatus='9G' WHERE vin ='".$rsSqlUnidS9['root'][$i]['vin']."' ";
				fn_ejecuta_query($sqlUpd);

					
				
			}


			//fin de archivo
			$long=(sizeof($rsSqlUnidS9['root']));
			fwrite($archivo,"GEE".sprintf('%06d',($long)).sprintf('%09s',$rsSqlUnidS9['root'][0]['sequence']));

			fclose($archivo);	
			//ftpArchivo ($nombreBusqueda);

			$updateFolioGM = "UPDATE trfoliostbl set folio=".$rsSqlUnidS9['root'][0]['sequence']." where centroDistribucion='LZC02' and compania='GM' and tipoDocumento='GM'";
			fn_ejecuta_query($updateFolioGM);


		}
	}

	/*function ftpArchivo($nombreBusqueda){
		if(file_exists("E:/carbook/archivosInterfaces/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/archivosInterfaces/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/archivosInterfaces/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}*/

?>