<?php
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    custodiaLZC();

    function custodiaLZC(){
        echo "Inicio Custodias: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
        $fechaAnt = strtotime ( '-1 day' , strtotime (date("Y-m-d")) ) ;
        $fechaAnt = date ( 'Y-m-d' , $fechaAnt );
        //$fechaMod = $fechaAnt.' 23:59:59';

        //INGRESO CUSTODIA - IU
        // UNIDADES DE EXPOTACION
        
        $sqlGetCusExp = "INSERT INTO cocustodiastbl(centroDistribucion, marcaUnidad, vin, claveConcepto, claveGrupo, fechaEvento, fechaCobro, cantidad, importe, claveMovimiento, numeroFactura)  ".
                        "SELECT ud.centroDistribucion,mu.marca,ud.vin,'9103' as claveConcepto, '1' as claveGrupo, now() as fechaEvento,null as fechaCobro,'0' as cantidad,'1' as importe ,'IU' as claveMovimiento, null as numeroFactura ".
                        "FROM alultimodetalletbl ud, alunidadestbl un, camarcasunidadestbl mu, casimbolosunidadestbl su, alinstruccionesmercedestbl im ".
                        "WHERE ud.vin = un.vin ".
                        "AND un.simboloUnidad = su.simboloUnidad ".
                        "AND su.marca = mu.marca ".
                        "AND ud.vin = im.vin ".                        
                        "AND ud.centroDistribucion = 'LZC02' ".
                        "AND ud.claveMovimiento IN ('L2','PI');";

        $rsGetCusExp = fn_ejecuta_query($sqlGetCusExp);

        $sqlGetNsCus = "INSERT INTO cocustodiastbl(centroDistribucion, marcaUnidad, vin, claveConcepto, claveGrupo, fechaEvento, fechaCobro, cantidad, importe, claveMovimiento, numeroFactura) ".
						"SELECT hu.centroDistribucion, su.marca, au.vin, '0091' as claveConcepto, '1' as claveGrupo, NOW() as fechaEvento, null as fechaCobro, '0' as cantidad, '1' as importe, 'IU' as claveMovimiento, null as numeroFactura ".
						"FROM alunidadestbl au, alhistoricounidadestbl hu, casimbolosunidadestbl su ".                    
						"WHERE au.vin = hu.vin ".
						"AND au.simboloUnidad = su.simboloUnidad ".
						"AND hu.centroDistribucion = 'CDAGS' ".
						"AND su.marca = 'NS' ".
						"AND hu.claveMovimiento = 'L2';";

		$rsGetCusExp = fn_ejecuta_query($sqlGetNsCus);						

        $contador = sizeof($rsGetCusExp['root']);
        
        echo "Fin Custodias: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
    }
?>