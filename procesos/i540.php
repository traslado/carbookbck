<?php
	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/utilidadesProcesos.php");

	$a = array();
	$e = array();
	$a['success'] = true;
	//echo (substr($_REQUEST['i507DiaDtm'],0,10));

	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['int504Hdn']){
        case 'validaRegistros':
            validaRegistros();
            break;
        case 'addInterface':
            addInterface();
            break;
        case 'genInterfaces':
        	  genInterfaces();
        	  break;
        default:
            echo '';
    }



	function validaRegistros(){
		$sqlGetUnidades = "SELECT count(gc.idViajeTractor) as BANDERA ".
											"FROM trgastosviajetractortbl gc ".
											"WHERE gc.centroDistribucion = '".$_REQUEST['i504RemesaCmb']."' ".
											"AND cast(gc.fechaEvento as date) =  cast('".substr($_REQUEST['i504FechaDtm'],0,10)."' as date) ".
											"AND gc.concepto IN ('7001','7022') ".
											"AND gc.fechaEvento NOT IN (SELECT ic.fechaInicio ".
                          												"FROM trinterfacesfechatbl ic ".
                          												"WHERE ic.centroDistribucion = gc.centroDistribucion ".
                            											"AND cast(ic.fechaInicio as date) = cast(gc.fechaEvento as date) ) ".
											"AND gc.claveMovimiento NOT IN('DX','DT');";

											$rsGetPolizas = fn_ejecuta_query($sqlGetUnidades);
											echo json_encode($rsGetPolizas);
	}

	function addInterface(){
		// centroDistribucion, folio, idViajeTractor, fechaMovimiento, fechaPoliza, concepto, tipoDocumento, importe, observaciones, claveMovimiento
		$sqlAddInt = "SELECT gc.idViajeTractor, gc.centroDistribucion, ".
									"CONCAT(SUBSTRING((SELECT cc.cuentaContable FROM caconceptoscentrostbl cc WHERE cc.concepto = gc.concepto AND cc.centroDistribucion = vt.centroDistribucion ),1, 14),lpad(ch.cuentaContable,4,0)) as cuentaContable ".
									"FROM trviajestractorestbl vt, cachoferestbl ch, trgastosviajetractortbl gc ".
									"WHERE vt.claveChofer = ch.claveChofer ".
									"and vt.centroDistribucion = gc.centroDistribucion ".
									"AND vt.idViajeTractor = gc.idViajeTractor ".
									"AND gc.concepto IN ('7001','7022') ".
									"AND vt.centroDistribucion = '".$_REQUEST['i504RemesaCmb']."' ".
									"AND CAST(gc.fechaEvento AS DATE) = CAST('".substr($_REQUEST['i504FechaDtm'],0,10)."' AS DATE) ;";

									$rsAddInt = fn_ejecuta_query($sqlAddInt);
									echo json_encode($rsAddInt);

									for ($i=0; $i < $rsAddInt['records'] ; $i++) {
										$sqlUpd = "UPDATE trgastosviajetractortbl ".
															"SET cuentaContable = '".$rsAddInt['root'][$i]['cuentaContable']."', ".
															"claveMovimiento = 'GT' ".
															"WHERE centroDistribucion = '".$rsAddInt['root'][$i]['centroDistribucion']."' ".
															"AND concepto IN ('7001','7022') ".
															"AND idViajeTractor = '".$rsAddInt['root'][$i]['idViajeTractor']."';";

										fn_ejecuta_query($sqlUpd);
									}
	}

	function genInterfaces(){
		$sqlGetGenera = "SELECT '4' as compania, year(gc.fechaEvento) as anio,month(gc.fechaEvento) as mes,LPAD(gc.folio,6,0) as folio,DATE_FORMAT(gc.fechaEvento,'%d/%m/%Y') as fechaEvento, ".
										"(SELECT ge.columna FROM cageneralestbl ge WHERE ge.tabla = 'trpolizainterfacetbl' AND ge.valor = gc.centroDistribucion) as numPoliza, ".
										"(SELECT concat(substring(cc.cuentaContable,1,10),LPAD(ch.cuentaContable,4,0))  FROM caconceptoscentrostbl cc WHERE cc.centroDistribucion = gc.centroDistribucion AND cc.concepto = gc.concepto ) AS cuentaContable,'0' as numeroCheque, ".
										"gc.importe as cargo,'0.00' as abono, CONCAT('FOLIO DE RECIBO ',LPAD(gc.folio,6,0)) as observaciones ".
										"FROM trgastosviajetractortbl gc, trviajestractorestbl vt, cachoferestbl ch ".
										"WHERE gc.idViajeTractor = vt.idViajeTractor ".
										"AND vt.claveChofer = ch.claveChofer ".
										"AND gc.centroDistribucion = '".$_REQUEST['i504RemesaCmb']."' ".
										"AND gc.concepto IN('7001','7022') ".
										"AND CAST(gc.fechaEvento as date) = cast('".substr($_REQUEST['i504FechaDtm'],0,10)."' as date);";

		$rsGeneraDatos = fn_ejecuta_query($sqlGetGenera);
		echo json_encode($rsGeneraDatos);



		if(sizeof($rsGeneraDatos['root']) != '0'){
			$fileDir = "E:/carbook/i504/01".date("d").strtoupper(date("M")).date("Y").".RDI";
			$dirResp = "E:/carbook/i504/respArchivo/";
			$flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
			$recordsPositionValue = array();
			$e=1;

			for ($i=0; $i < sizeof($rsGeneraDatos['root']); $i++) {
				fwrite($flReporte660,$rsGeneraDatos['root'][$i]['compania'].'|'.$rsGeneraDatos['root'][$i]['anio'].'|'.$rsGeneraDatos['root'][$i]['mes'].'|'.$rsGeneraDatos['root'][$i]['folio'].'|'.$rsGeneraDatos['root'][$i]['fechaEvento'].'|'.
														$rsGeneraDatos['root'][$i]['numPoliza'].'|'.$rsGeneraDatos['root'][$i]['cuentaContable'].'|'.$rsGeneraDatos['root'][$i]['numeroCheque'].'|'.$rsGeneraDatos['root'][$i]['cargo'].'|'.
														$rsGeneraDatos['root'][$i]['abono'].'|'.$rsGeneraDatos['root'][$i]['observaciones'].'|0|'.$e.'|'.PHP_EOL);
				$e= $e+1;
			}
			fclose($flReporte660);
		}

	}
?>
