<?php
	/*require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

 	i611(); */
	
	function i611(){

		echo "Inicio i611: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

	 	$_REQUEST['cargaZidisFechaTxt'] = date('Y-m-d');

	 	$a = array();
	    $e = array();
	    $a['success'] = true; 
			
		if($_REQUEST['cargaZidisFechaTxt'] == ""){
	        $e[] = array('id'=>'cargaZidisFechaTxt','msg'=>getRequerido());
	        $a['errorMessage'] = getErrorRequeridos();
	        $a['success'] = 'false';  

	     }

	    if($a['success']){

	    	//DIR
			$servidor = __DIR__."\\..\\..";


			$pathArchivo = "E:\\carbook\\i611\\instrucciones\\";
			$pathRespaldo = "E:\\carbook\\i611\\respaldo611\\";
			//VALIDACION DE DIRECTORIOS				
			if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {	
			    $pathArchivo = str_replace("\\", "/", $pathArchivo);

			}

			if(!is_dir($pathArchivo)){
				mkdir($pathArchivo, 0777, true);
			}

	        $a = generarProceso611($pathArchivo, $pathRespaldo, $_REQUEST['cargaZidisFechaTxt']);
		
	    } else {
	    	$a['errors'] = $e;
	    }

	    //echo json_encode($a);

	    echo "Fin i611: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

	}

	function generarProceso611($pathArchivo, $pathRespaldo, $pathLog, $pathError){

		$data = array('success'=>true);		

		$errorArchivo = 0;

		$archivos = scandir($pathArchivo);
		
		//$archivos = preg_grep("/AFG.VDA611.TRC1/", $archivos);
		//echo json_encode($archivos);
		
		if(sizeof($archivos) > 0){	
			sort($archivos);			

			for ($i=0; $i < sizeof($archivos) ; $i++) { 

				$nombreRespaldo = $pathRespaldo.$archivos[$i];	
				$nombreFile = $pathArchivo.$archivos[$i];
				$file = fopen($nombreFile, "r");	

				if(!$file){
				
						//echo "Error al leer archivo ".$archivos[$i];
				}
				else
				{
						$a = array('success'=>true);

						while (($line = fgets($file)) !== FALSE) {
							
							$success = true;

					    //PROCESAR ENESTE IF TODAS LAS LINEAS QUE COMIENZEN EN 612							


					   if(substr($line, 0,3) == "612"){
					    	
					    $tipoRegistro=substr($line,0,3);
						$numeroVersion=substr($line,3,2);
						$estadoFais=substr($line,5,2);
						$fechaEstadoFais=substr($line,7,6);
						$tipoAutomovil=substr($line,13,1);
						$numDistEnt=substr($line,14,9);
						$numOrden=substr($line,23,9);
						$vin=substr($line,32,17);
						$avanzada=substr($vin,9,8);
						$codigoModelo=substr($line,49,9);
						$codigoColor=substr($line,58,4);
						$horaEstado60=substr($line,62,4);
						$opcsignificativas=substr($line,66,8);
						$peso=substr($line,74,5);
						$precioAuto=substr($line,93,9);
						$numDisSolicitoAuto=substr($line,108,9);
						$fechaEstado60=substr($line,117,6);
						$usoPlanificadoAutomovil=substr($line,124,1);
						$codigoPlanta=substr($line,125,3);
						$banderaAduana=substr($line,184,1);
						$tipoProducto=substr($line,185,1);


						$rsSqlInsertInstruccionesBMW612 ="INSERT INTO alinstruccionesbmwTbl (vin,centroDistribucion,tipoRegistro, avanzada, numeroVersion, estadoFais, fechaEstadoFais, tipoAutomovil,distribuidorEntrega, numeroOrden, simboloUnidad, color, fechaEstado60, horaEstado60, opcionSignificativa, peso, precioAuto, distribuidorOrden, usoPlanificado, codigoPlanta, banderaAduanas, tipoProducto, cveStatus) VALUES ('".trim($vin)."','CDSLP','".trim($tipoRegistro)."','".trim($avanzada)."','".trim($numeroVersion)."','".trim($estadoFais)."','".trim($fechaEstadoFais)."','".trim($tipoAutomovil)."','".trim($numDistEnt)."','".trim($numOrden)."','".trim($codigoModelo)."','".trim($codigoColor)."','".trim($fechaEstado60)."','".trim($horaEstado60)."','".trim($opcsignificativas)."','".trim($peso)."','".trim($precioAuto)."','".trim($numDisSolicitoAuto)."','".trim($usoPlanificadoAutomovil)."','".trim($codigoPlanta)."','".trim($banderaAduana)."','".trim($tipoProducto)."','BD'); ";

						fn_ejecuta_query($rsSqlInsertInstruccionesBMW612);

					    }
						   //PROCESAR ENESTE IF TODAS LAS LINEAS QUE COMIENZEN EN 613



					    if (substr($line,16,1)=='V') {
					    	$distribuidor='CPV';
					    }else if (substr($line,16,1)=='L') {
					    	$distribuidor='SSABM';
					    }else if (substr($line,16,1)=='M') {
					    	$distribuidor='PTOAL';
					    }

					  
						
						if(substr($line, 0,3) == "613"){


						$tipoRegistro=substr($line,0,3);
						$numeroVersion=substr($line,3,2);
						$tipoUbicacionSalida=substr($line,5,2);
						$codigoUbicacionSalida=substr($line,7,7);
						$tipoUbicacionEntregada=substr($line,14,2);
						$codigoUbicacionEntrega=substr($line,16,7);
						$codigoDistribucion=substr($line,23,3);
						$marcaTiempoDespacho=substr($line,26,4);
						$mediosDeTransporte=substr($line,30,2);
						$numeroCarga=substr($line,32,25);
						$servEspDemandados2=substr($line,63,2);
						$servEspDemandados3=substr($line,65,6);
						$portadorPrevisto=substr($line,73,7);
						$direccionesEspeciales=substr($line,80,2);
						$vin=substr($line,191,10).substr($line,82,7);
						$avanzada=substr($vin,9,8);
						$servEspExigido=substr($line,89,2);
						$agentePortuario=substr($line,91,4);
						$puertoDestino=substr($line,95,3);
						$servicioBateria=substr($line,98,2);
						$tiempoApisonarDespacho=substr($line,114,6);
						$prioridadDistribucion=substr($line,126,1);
						$gradoTransporte=substr($line,128,1);
						$mercadoEntrega=substr($line, 169,3);
						$areaEntrega=substr($line, 172,3);
						$precioAutomovil=substr($line,175,13);
						

						$rsSqlInsertInstruccionesBMW613 ="UPDATE alinstruccionesbmwTbl SET tipoRegistro='".trim($tipoRegistro)."',numeroVersion='".trim($numeroVersion)."', precioAuto='".trim($precioAutomovil)."',tipoUbicacionAlmacenamientoSalida='".trim($tipoUbicacionSalida)."', tipoUbicacionAlmacenamientoEntrega='".trim($tipoUbicacionEntregada)."', codigoUbicacionAlmacenamientoSalida='".trim($codigoUbicacionSalida)."', codigoUbicacionAlmacenamientoEntrega='".trim($codigoUbicacionEntrega)."', codigoDistribucion='".trim($codigoDistribucion)."', marcaTiempoDespacho='".trim($marcaTiempoDespacho)."', medioTransporte='".trim($mediosDeTransporte)."', numeroCarga='".trim($numeroCarga)."', serviciosEspeciales1='".trim($servEspDemandados2)."', serviciosEspeciales2='".trim($servEspDemandados3)."', portadorPrevisto='".trim($portadorPrevisto)."', direcionEspecial='".trim($direccionesEspeciales)."', servicioEspecialExigido='".trim($servEspExigido)."', agentePortuario='".trim($agentePortuario)."', puertoDestino='".trim($puertoDestino)."', servicioBateria='".trim($servicioBateria)."', fechaApisonar='".trim($tiempoApisonarDespacho)."', prioridadDistribucion='".trim($prioridadDistribucion)."', gradoTransporte='".trim($gradoTransporte)."', mercadoEntrega='".trim($mercadoEntrega)."', areaEntrega='".trim($areaEntrega)."', cveStatus='BO' WHERE vin ='".$vin."' and cveStatus='BD' and tipoRegistro='612';";

					fn_ejecuta_query($rsSqlInsertInstruccionesBMW613);


					$SelVinUD="SELECT * from alultimodetalletbl where vin='".trim($vin)."' ";
					$rsSelVinUD=fn_ejecuta_query($SelVinUD);


					if ($rsSelVinUD['records'] == 0) {
					
				

	     			$rsSqlInsertAlunidadesTbl="INSERT INTO alunidadesTbl (vin,avanzada,distribuidor,simboloUnidad,color) ".
											  "VALUES('".trim($vin)."','".trim($avanzada)."','".$distribuidor."','1R51','PW7'); ";
					fn_ejecuta_query($rsSqlInsertAlunidadesTbl);

					$rsSqlInsertalHistoricoUnidadesTbl="INSERT INTO alhistoricoUnidadesTbl (centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,".
 														"idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
											  			"VALUES('CDSLP','".trim($vin)."',now(),'PR','".$distribuidor."',1,'CDSLP',null,null,'3','127.0.0.1');" ;
					fn_ejecuta_query($rsSqlInsertalHistoricoUnidadesTbl);

					$rsSqlInsertalHistoricoUnidadesEPTbl="INSERT INTO alhistoricoUnidadesTbl (centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,".
 														"idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
											  			"VALUES('CDSLP','".trim($vin)."',DATE_ADD(NOW(), INTERVAL 5 SECOND),'EP','".$distribuidor."',1,'CDSLP',null,null,'3','127.0.0.1');" ;
					fn_ejecuta_query($rsSqlInsertalHistoricoUnidadesEPTbl);

					$rsSqlInsertaUD = "INSERT INTO alultimodetalletbl (centroDistribucion,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) 
						SELECT centroDistribucion,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip 
						FROM alhistoricounidadestbl hu 
						WHERE  hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) 
                      							FROM alhistoricounidadestbl hu2 
                      							WHERE hu2.vin = hu.vin 
                      							AND hu2.claveMovimiento not in (SELECT ge.valor 
                                                  FROM cageneralestbl ge 
                                                  WHERE ge.tabla = 'alHistoricoUnidadesTbl' 
                                                  AND ge.columna = 'nvalidos')) 
												AND  hu.vin = '".trim($vin)."'  ";

			          fn_ejecuta_query($rsSqlInsertaUD);
			      }


				}


			
			}
			copy($nombreFile, $nombreRespaldo);
			fclose($file);	
			unlink($nombreFile);
			}	

		}

	}
}

?>