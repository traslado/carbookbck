<?php
	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	include_once("../funciones/utilidades.php");	
	require_once("../funciones/utilidadesProcesos.php");

	switch($_SESSION['idioma']){
      case 'ES':
          include("../funciones/idiomas/mensajesES.php");
          break;
      case 'EN':
          include("../funciones/idiomas/mensajesEN.php");
          break;
      default:
          include("../funciones/idiomas/mensajesES.php");
  }

  switch($_REQUEST['int507Hdn']){
      case 'valida':
          valida();
          break;
      case 'inserta':
          generaInterfase();
          break;
      default:
          echo '';
  }

	function valida(){
		$a = array('success' => true, 'msjResponse' => '', 'nombreArchivo'=>'', 'insertaInterface'=>0);
		
		$sql = "SELECT * FROM trInterfacesFechaTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					 " AND tipoInterface = 'D'".    				 # Recibos Diesel
					 " AND SUBSTRING(fechaInicio,1,10) = '".$_REQUEST['fechaInicio']."'".
					 " AND SUBSTRING(fechaFinal,1,10) = '".$_REQUEST['fechaInicio']."'";
		//echo "$sql<br>";
		$rsFecha = fn_ejecuta_query($sql);
		//var_dump($rsFecha);
		
		
		$sql = "SELECT COUNT(*) AS totalReg FROM trGastosViajeTractorTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					 " AND concepto = '2314'".
					 " AND SUBSTRING(fechaEvento,1,10) = '".$_REQUEST['fechaInicio']."'".
					 " AND claveMovimiento NOT IN ('GX','GT')";    			# GX=Cancelado GT=Transmitido"
		//echo "$sql<br>";
		$rsGastos = fn_ejecuta_query($sql);
		//var_dump($rsGastos);
		
		$falta = (empty($rsGastos['root'][0]['totalReg']))?0:$rsGastos['root'][0]['totalReg'];
		//var_dump($falta);
		
		if(($rsFecha['root'][0]['estatus'] == 'G' || $rsFecha['root'][0]['estatus'] == 'T') && $falta == 0)		# Generado o Trasmitido a Contabilidad
		{
				$a['success'] = false;
				$a['msjResponse'] = 'Interfase ya generada para esa fecha.';
		}
		
		if($a['success'])
		{
				if(($rsFecha['root'][0]['estatus'] == 'G' || $rsFecha['root'][0]['estatus'] == 'T') && $falta > 0)		# Generado o Trasmitido a Contabilidad
				{
						//$a['success'] = false;
						$a['insertaInterface'] = 0;
						$a['msjResponse'] = 'Interfase para esa fecha ya generada, pero hay registros sin acumular.';
				}				
		}
		
		if($a['success'])
		{
				if($falta == 0)					# Generado
				{
						$a['success'] = false;
						//$a['msjResponse'] = 'No existen datos con ese criterio.';
						$a['msjResponse'] = 'No existe informaci&oacuten a transmitir para esa fecha.';
				}
				else
				{
						$a['insertaInterface'] = 1;
				}
		}

		if($a['success'])
		{
				setlocale(LC_TIME, 'spanish');
				$mes = (date("n", strtotime($_REQUEST['fechaInicio'])));
				$nombreMes = strftime("%B",mktime(0, 0, 0, $mes));
				
				$aux = explode('-', $_REQUEST['fechaInicio']);
				$anio = $aux[0];				
				$mes = $aux[1];
				$dia = $aux[2];
				
				$a['nombreArchivo'] = str_pad($_REQUEST['tipo'], 2, "0", STR_PAD_LEFT).str_pad($dia, 2, "0", STR_PAD_LEFT).strtoupper(substr($nombreMes,0,3)).str_pad($anio, 4, " ", STR_PAD_LEFT).'.RDI';
		}
		
		echo json_encode($a);
	}
		
	function generaInterfase(){
		$a = array('success' => true, 'msjResponse' => 'Interfase generada correctamente.', 'nombreArchivo'=>'', 'existeTmp'=>0);
		
		$dir 	= "E:/carbook/i507/respArchivo/";
		$arch = $_REQUEST['nombreArchivo'].'.nvo';
		
		$sql = "SELECT cuentaContable, tipoCuenta FROM caConceptosCentrosTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['i507RemesaCmb']."'".
					 " AND concepto = '2314'";
		//echo "$sql<br>";
		$rsCtas = fn_ejecuta_query($sql);
		//var_dump($rsCtas);
		
		$sql = "SELECT cuentaContable, tipoCuenta FROM caConceptosCentrosTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['i507RemesaCmb']."'".
					 " AND concepto = '2316'";
		//echo "$sql<br>";
		$rsCtasAbo = fn_ejecuta_query($sql);
		//var_dump($rsCtasAbo);
		
		$ind = 0;
		$totabo = 0;
		
		$sql = "SELECT * FROM trGastosViajeTractorTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['i507RemesaCmb']."'".
					 " AND concepto = '2314'".
					 " AND SUBSTRING(fechaEvento,1,10) = '".$_REQUEST['i507DiaDtm']."'".
					 " AND claveMovimiento NOT IN ('GX','GT')";    			# GX=Cancelado GT=Transmitido"
		//echo "$sql<br>";		
		$rsGastos = fn_ejecuta_query($sql);
		//var_dump($rsGastos);		
		
		$idx = 0;
		$a['nombreArchivo'] = $dir.$arch;
		$fp = fopen($a['nombreArchivo'], 'w');
		$z = 0;
		foreach($rsGastos['root'] AS $index => $row)
		{
				# Recupera los litros				
				$sql = "SELECT * FROM trGastosViajeTractorTbl".
							 " WHERE centroDistribucion = '".$_REQUEST['i507RemesaCmb']."'".
							 " AND usuario = '".$row['usuario']."'".
							 " AND idViajeTractor = ".$row['idViajeTractor'].
							 " AND concepto = '2312'".							 
							 " AND SUBSTRING(fechaEvento,1,10) = '".substr($row['fechaEvento'],0,10)."'".
							 " AND mesAfectacion = '".$row['mesAfectacion']."'".
							 " AND folio = '".$row['folio']."'".
							 " AND claveMovimiento = '".$row['claveMovimiento']."'";
				//echo "$sql<br>";		
				$rsLitros = fn_ejecuta_query($sql);
				//var_dump($rsLitros);
				
				if($rsLitros['records'] == 0)
				{
						$rsLitros['root'][0]['subTotal'] = 1;
				}
				
				#Inserta cuenta de Cargo
				$sql = "SELECT DISTINCT ca.observaciones, ca.tractor FROM trViajesTractoresTbl tr, caTractoresTbl ca".
							 " WHERE tr.idViajeTractor = ".$row['idViajeTractor'].
							 " AND ca.idTractor = tr.idTractor";
				//echo "$sql<br>";		
				$rsObs = fn_ejecuta_query($sql);
				//var_dump($rsObs);
							 
				$ctacon = rtrim($rsCtas['root'][0]['cuentaContable']);
				$rsGastos['root'][$z]['cuentaContable'] = $ctacon;
				
				$ctacon = substr($rsGastos['root'][$z]['cuentaContable'],0,10).str_pad($rsObs['root'][0]['tractor'], 3, "0", STR_PAD_LEFT).substr($rsGastos['root'][$z]['cuentaContable'],13,strlen($rsGastos['root'][$z]['cuentaContable']));
				$rsGastos['root'][$z]['cuentaContable'] = $ctacon;
				
				$aux = explode('-',substr($row['fechaEvento'],0,10));
				$anio = $aux[0];				
				$mes = $aux[1];
				$dia = $aux[2];
				$fecha = $dia.'/'.$mes.'/'.$anio;
				
				$ctacon = rtrim($rsGastos['root'][$z]['cuentaContable']);
				$cvecia = 4;
				$anio 	= substr($row['fechaEvento'],0,4);
				$mes 		= substr($row['fechaEvento'],5,2);
				$fecpol	= $fecha;				
				$numpol = str_pad('1', 6, "0", STR_PAD_LEFT);
				$tippol = $_REQUEST['tipo'];
				$numche = 0;
				$cargo = '0.00';
				$abono = $row['importe'];				
				if(rtrim($rsCtas['root'][0]['tipoCuenta']) == 'C')
				{
						$cargo = $row['importe'];
						$abono = '0.00';
				}				
				$desmov = 'R-'.str_pad($row['folio'], 4, "0", STR_PAD_LEFT).' EDER '.str_pad($rsObs['root'][0]['tractor'], 3, "0", STR_PAD_LEFT).' '.str_pad(substr($rsObs['root'][0]['observaciones'],12,4), 4, "0", STR_PAD_LEFT).' '.str_pad($rsLitros['root'][0]['subTotal'], 4, "0", STR_PAD_LEFT);
				$tippro = 0;
				$ind++;
				$orden = $ind;
				
				fputs($fp, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
				$idx++;
      	fputs($fp,"\n");
      	
      	# Inserta cuenta de Abono
      	$ind++;
      	$orden = $ind;
      	
      	$ctacon = rtrim($rsCtasAbo['root'][0]['cuentaContable']);      	
      	$ctacon = substr($ctacon,0,10).str_pad($rsObs['root'][0]['tractor'], 3, "0", STR_PAD_LEFT).substr($ctacon,13,strlen($ctacon));      	      	
      	$cargo = 0;
				$abono = $row['importe'];
				if(rtrim($rsCtasAbo['root'][0]['tipoCuenta']) == 'C')
				{
						$cargo = $row['importe'];
						$abono = 0;
				}
				fputs($fp, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
				fputs($fp,"\n");
				$z++;
		}		
		fclose($fp);
		
		//Verifica si existe el directorio tmp, sino lo crea
		if (!file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				mkdir('../../carbookv1.2/modules/interfacesContables/tmp/', 0777);
		}
		if (file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				copy($a['nombreArchivo'], '../../carbookv1.2/modules/interfacesContables/tmp/'.$arch);
				$a['existeTmp'] = 1;
		}			

		//Se realiza la actualización de la interfase contable		
		foreach($rsGastos['root'] AS $index => $row)
		{
				$sql = "UPDATE trGastosViajeTractorTbl".
							 " SET claveMovimiento = 'GT'".					# Transmitido
							 " WHERE centroDistribucion = '".$row['centroDistribucion']."'".
							 " AND idViajeTractor = ".$row['idViajeTractor'].
							 " AND SUBSTRING(fechaEvento,1,10) = '".substr($row['fechaEvento'],0,10)."'".
							 " AND concepto = '".$row['concepto']."'".
							 " AND folio = '".$row['folio']."'".
							 " AND claveMovimiento = '".$row['claveMovimiento']."'";
				//echo "$sql<br>";
				fn_ejecuta_query($sql);	 		//CHK
				
				if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
	      {
	         $a['success']		 = false;
	         $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten del Viaje.";
	         $a['sql'] = $sql;
	         break;
	      }
		}
		
		if($_REQUEST['insertaInterface'] == 1)
		{
				setlocale(LC_TIME, 'spanish');
				$horaActual = date('H:i:s');
				$sql = "INSERT INTO trInterfacesFechaTbl (centroDistribucion, tipoInterface, fechaInicio, fechaFinal, estatus) VALUES (".
								"'".$_REQUEST['i507RemesaCmb']."', ".
								"'D', ".
								//"NULL, ".
								"'".($_REQUEST['i507DiaDtm'].' '.$horaActual)."', ".
								"'".($_REQUEST['i507DiaDtm'].' '.$horaActual)."', ".
								"'D')";
				//echo "$sql<br>";
				fn_ejecuta_query($sql);	 		//CHK
				if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
	      {
	         $a['success']		 = false;
	         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
	         $a['sql'] = $sql;
	      }								
		}

    echo json_encode($a);
	}
?>
