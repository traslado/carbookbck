<?php
	/*require("../funciones/generales.php");
	require("../funciones/utilidades.php");*/

	global $dirPath;
	global $filePath;
	global $fileName;
	date_default_timezone_set('America/Mexico_City');

	//$dirPath = "/Users/poncho/www/carbookBck/";
	$dirPath = "E:\\carbook\\i660\\";
	$fileName= "chr660.txt";
	$filePath = $dirPath.$fileName;
	$ejecutaProceso = true;
	
	//carga660();		

	function carga660(){
		echo "Inicio i660: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
		global $dirPath;
		global $filePath;
		global $fileName;
		global $lineaStr;
		global $cont;

		$_660File = fopen($filePath, "r");//abres el archivo para lectura
		$fecha = date("d-m-Y_H-i-s");

		if(file($filePath)) {
			//fn_ejecuta_query("LOAD DATA LOCAL INFILE '$filePath' INTO TABLE alcarga660tmptbl ");
			$cont = 1;
			while (!feof($_660File)) {
				$lineaStr = fgets($_660File);

				if($lineaStr!=''){
					$cabeceroStr = substr($lineaStr,0,2);

					if ($cabeceroStr == 'HD'){
						datosHD($lineaStr);

					}else{
						insertarDatos($lineaStr);
					}
				}
				$cont = $cont + 1;
			}
			echo "FIN i660: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
		}else {
			
        	echo "No se pudo abrir el archivo\r\n";
        	echo "FIN i660: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

      	}

      	if(!is_dir($dirPath.'respaldo_660')) {
      		mkdir($dirPath.'respaldo_660');
      	}

      	copy($filePath, $dirPath.'respaldo_660\\chr660_'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
      	fclose($_660File);
     	unlink($filePath);
      	//echo "Termino: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
	}

	function datosHD($lineaStr){
	global $scacCodeStr;
    global $marcaStr;
    global $transStr;
    global $cobradoStr;
    global $cont;
	global $estatusBol;
	global $oriSplcStr;
    global $desSplcStr;

	$estatusBol= substr($lineaStr,6,2);
	$oriSplcStr= substr($lineaStr,8,9);
	$desSplcStr= substr($lineaStr,17,9);
	$scacCodeStr= substr($lineaStr,2,4);

	if($scacCodeStr == 'XCLC')
	{
		$scacCodeStr = 'XTRA';
	}
	  //$estatusBol = substr($lineaStr,7,2);
        $marcaStr = 'DC';
        $transStr = '100';
        $cobradoStr = 'N';

        //echo"estatus: $estatusBol,origen: $oriSplcStr, destino: $desSplcStr, scacCode:  $scacCodeStr";
	}

	function insertarDatos($lineaStr){
    	global $scacCodeStr;
    	global $marcaStr;
    	global $transStr;
    	global $cobradoStr;
    	global $cont;
		global $estatusBol;
		global $oriSplcStr;
    	global $desSplcStr;

    	global $marcaStr;
      	global $transStr;
      	global $cobradoStr;
      	global $filePath;
      	global $cont;
      	global $routeori;
      	global $routedes;

//echo $lineaStr;


        $avanzadaStr= substr($lineaStr,12,8);
	    $vinStr = substr($lineaStr,3,17);
	    $distribuidorStr = substr($lineaStr,62,5);
	    $simboloStr = substr($lineaStr,109,6);
	    $updateStr = "20".substr($lineaStr,84,2)."/".substr($lineaStr,86,2)."/".substr($lineaStr,88,2);
		$vuptimeStr = substr($lineaStr,90,2).":".substr($lineaStr,92,2).":".substr($lineaStr,94,2);
	    $colorStr = substr($lineaStr,136,3);
		$routeoriStr= substr($lineaStr,20,13);
		$routedesStr= substr($lineaStr,33,13);
		$vonStr= substr($lineaStr,46,15);

		$estimdate= "20".substr($lineaStr,98,6)."";
		$expedite= substr($lineaStr,104,1);
		$merlcode= substr($lineaStr,105,3);
		$city= substr($lineaStr,115,18);
		$state= substr($lineaStr,134,2);
		$ladingdes= substr($lineaStr,139,34);
		$authoriza= substr($lineaStr,174,5);
		$weight= substr($lineaStr,182,7);
		$height= substr($lineaStr,191,5);
		$lenght= substr($lineaStr,196,5);
		$width= substr($lineaStr,202,7);
		$volume= substr($lineaStr,210,7);

		/*if($distribuidorStr != '')
			validarDistribuidor($estatusDistribuidor);
			validarColor($colorStr);
			validarSimbolo($simboloStr);*/

		$sel660tbl="SELECT * FROM al660tbl
						WHERE vin ='".$vinStr."'
						AND numavanzada='".$avanzadaStr."'
						AND scacCode='".$scacCodeStr."'
						AND prodStatus='".$estatusBol."'
						AND oriSplc='".$oriSplcStr."'
						AND dessplc='".$desSplcStr."'
						AND dealerID='".$distribuidorStr."'
						AND vupdate='".$vuptimeStr."'
						AND vuptime='".$estimdate."'
						AND model='".$simboloStr."';"
		$rs660tbl=fn_ejecuta_query($sel660tbl);

		if ($rs660tbl['root'][0]['records'] !='0') {
				$sqlAdd = "INSERT INTO al660tbl (vin,numavanzada,scaccode,prodstatus,orisplc,dessplc,routeori,routedes,von,dealerid,startdate,enddate,
								dealerid2,vupdate,vuptime,estimdate,expedite,merlcode,model,city,state,colorcode,ladingdes,authoriza,weight,
								height,lenght,width,volume,tipoOrigen, estatus)".
								"VALUES (".
								"'".$vinStr."', ".
								"'".$avanzadaStr."', ".
								"'".$scacCodeStr."', ".
								"'".$estatusBol."', ".
								"'".$oriSplcStr."', ".
								"'".$desSplcStr."', ".
								"'".$routeoriStr."', ".//routeori
								"'".$routedesStr."', ".
								"'".$vonStr."', ".//von
								"'".$distribuidorStr."', ".
								"'', ".//startdate
								"'', ".//enddate
								"'', ".//iddealer2
								"'".$updateStr."', ".
								"'".$vuptimeStr."', ".
								"'".$estimdate."', ".//estimdate
								"'".$expedite."', ".//expedite
								"'".$merlcode."', ".//merlcode
								"'".$simboloStr."', ".
								"'".$city."', ".//city
								"'".$state."', ".//state
								"'".$colorStr."', ".
								"'".$ladingdes."', ".//ladingdes
								"'".$authoriza."', ".//authoriza
								"'".$weight."', ".//weight
								"'".$height."', ".//height
								"'".$lenght."', ".//lenght
								"'".$width."', ".//width
								"'".$volume."', ".//volume
								"'".$estatus."', ".//estatu de la unidad
								"'')";//tipoOrigen

				fn_ejecuta_query($sqlAdd);
			}else{
				
			}

		
	}

	function validarDistribuidor($dist){
		$rs = fn_ejecuta_query("SELECT distribuidorCentro FROM cadistribuidorescentrostbl WHERE distribuidorCentro ='".$dist."'");
		if (sizeof($rs['root']) == 0){ 
			$estatusDistribuidor = 1;
		}
	}

	function validarColor($col){
		$rs = fn_ejecuta_query("SELECT color FROM cacolorunidadestbl WHERE color ='".$col."'");
		if (sizeof($rs['root']) == 0){
			$estatusColor = 1;
		}
	}

	function validarSimbolo($simb){
		$rs = fn_ejecuta_query("SELECT simboloUnidad FROM caSimbolosUnidadesTbl WHERE simboloUnidad ='".$simb."'");
		if (sizeof($rs['root']) == 0){
			$estatusSimbolo = 1;
		}
	}

	/*function cargaFTP(){
		$nombreArchivos = 'chr660.txt';
		$servidor    = "10.2.2.3";
		$usuario     = "emilio";
		$password    = "choncho00";
		//$local = "../../tratDy/";
		$local = "E:\\carbook\\carga_660\\chr660.txt";
		$remoto = "/users/tra/reptra/chr660.txt";

		// conexión
		$conexion = ftp_connect($servidor);
		  
		// loggeo
		$login = ftp_login($conexion, $usuario, $password); 
		//echo $login;		
		// conexión
		if ((!$conexion) || (!$login))
		{ 
		    $error = 1;
		    $msg  = "Conexión fallida al sitio FTP!";		    
		    //echo "Intentando conectar a $servidor for user $usuario"; 
		    exit; 
		}
		else{
		    $msg = "Conectado a $servidor, for user $usuario";		    
		    //ftp_get($conexion, $local, $remoto, $nombreArchivos,FTP_BINARY);
		    if (ftp_get($conexion, $local, $remoto,FTP_BINARY)) {
   				echo "Se ha guardado satisfactoriamente en $local\n";
   				carga660();
   				copy($filePath, $resPath.'chr660Path\\chr660_'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
      			fclose($_660File);
     			unlink($fileRespFile);
			} 
			else {
    			echo "Ha ocurrido un problema $local\n";
			}
		}				
		//$descarga = ftp_put($conexion, $archivoDestino, $archivoFuente, FTP_BINARY);
		ftp_close($conexion);
	}*/

	function limpiarVacios(){
		$sqlUpd =   "update al660Tbl ".
					"Set vin = ltrim(rtrim(VIN)), ".
					"numavanzada = ltrim(rtrim(numavanzada)), ".
					"orisplc = ltrim(rtrim(orisplc)), ".
					"dessplc = ltrim(rtrim(dessplc)), ".
					"routeori = ltrim(rtrim(routeori)), ".
					"routedes = ltrim(rtrim(routedes)), ".
					"von = ltrim(rtrim(von)), ".
					"dealerid = ltrim(rtrim(dealerid)), ".
					"startdate = ltrim(rtrim(startdate)), ".
					"enddate = ltrim(rtrim(enddate)), ".
					"dealerid2 = ltrim(rtrim(dealerid2)), ".
					"vupdate = ltrim(rtrim(vupdate)), ".
					"vuptime = ltrim(rtrim(vuptime)), ".
					"estimdate = ltrim(rtrim(estimdate)), ".
					"expedite = ltrim(rtrim(expedite)), ".
					"merlcode = ltrim(rtrim(merlcode)), ".
					"model = ltrim(rtrim(model)), ".
					"city = ltrim(rtrim(city)), ".
					"state = ltrim(rtrim(state)), ".
					"ladingdes = ltrim(rtrim(ladingdes)), ".
					"authoriza = ltrim(rtrim(authoriza)), ".
					"weight = ltrim(rtrim(weight)), ".
					"height = ltrim(rtrim(height)), ".
					"lenght = ltrim(rtrim(lenght)), ".
					"width = ltrim(rtrim(width)), ".
					"volume = ltrim(rtrim(volume)), ".
					"estatus = ltrim(rtrim(estatus)) ";

		fn_ejecuta_query($sqlUpd);
	}
?>