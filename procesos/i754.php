<?php
	// INTERFAZ 754
	// Archivos a generar 
	// CHRRECSHPER.txt - Interface
	// CHRRECSHPER.man - Archivo de respaldo CHECAR ESTO .man ES PARA MANUALES EN LINUX
	// log_754.ddmmaaaahhiiss - log

	setlocale(LC_TIME, "");
	setlocale(LC_TIME, 'es_MX.utf-8');

	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");

	DIR
	$servidor = __DIR__."\\..\\..";

	$pathArchivo = $servidor.'\\archivos\\shipper\\files\\';
	$pathRespaldo = $servidor.'\\archivos\\shipper\\enviados\\';
	$pathLog = $servidor.'\\archivos\\shipper\\log\\';

	/*$pathArchivo="C:carbook/i754/files/";
	$pathRespaldo="C:carbook/i754/enviados/";
	$pathLog="C:carbook/i754/log/";*/

	//VALIDACION DE DIRECTORIOS
	if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
	    $pathArchivo = str_replace("\\", "/", $pathArchivo);
	    $pathRespaldo = str_replace("\\", "/", $pathRespaldo);
	    $pathLog = str_replace("\\", "/", $pathLog);
	}

	if(!is_dir($pathArchivo)){
		mkdir($pathArchivo, 0777, true);
	}
	if (!is_dir($pathRespaldo)){
		mkdir($pathRespaldo, 0777, true);
	}
	if (!is_dir($pathLog)){
		mkdir($pathLog, 0777, true);
	}

	$fileArchivo = $pathArchivo.'CHRRECSHPER.txt';
	$fileRespaldo = $pathRespaldo.'CHRRECSHPER.man';
	$fileLog = $pathLog.'log_754.'.date("dmYhi").'.log';

	//REVISAR QUE NO SE HAYA GENERADO YA OTRO ARCHIVO EN EL MISMO MINUTO
	if(file_exists($fileLog)){
		echo json_encode(array("success"=>false, 
							   "errorMessage"=>"Ya se genero un archivo en este momento, espere un minuto, por favor."
							   ));
	} else {
		generar754($fileArchivo, $fileRespaldo, $fileLog);

	}

	function generar754($fileArchivo, $fileRespaldo, $fileLog){
		$a = array('success'=>true);

		$log = fopen($fileLog, 'w');
		if($log){
			$fechaInicio = date('d/m/Y', strtotime('-2 days'));

			fwrite($log, 'Empieza proceso para generar shipper <..........>>>>>'.PHP_EOL);
			fwrite($log, 'Desde: '.$fechaInicio.' Hasta: '.date("d/m/Y").PHP_EOL);
			fwrite($log, 'Inicio: '.date("d/m/Y").' '.date("H:i:s").PHP_EOL);

			//OBTENER TALONES
			$sqlGetTalones = "SELECT tv.centroDistribucion, tr.tractor, tr.compania AS companiaTractor, tv.folio, tv.fechaEvento,".
							"ut.vin, hu.idTarifa, hu.distribuidor, un.simboloUnidad, dc.tipoDistribuidor, re.region, ".
							"(SELECT COUNT(tv2.folio) FROM trTalonesViajesTbl tv2 WHERE tv2.centroDistribucion = 'CDTOL' ".
								"AND tv2.tipoDocumento = 'TF' AND (tv2.claveMovimiento ='TF' OR tv2.claveMovimiento = 'TE') ".
								"AND tv2.fechaEvento >= '".date('Y-m-d', strtotime('-2 days'))."' ".
								"AND tv2.fechaEvento <= '".date("Y-m-d 23:59:59")."' ".
							") AS numeroTalones ".
							"FROM trTalonesViajesTbl tv, trViajesTractoresTbl vt, caTractoresTbl tr, trUnidadesDetallesTalonesTbl ut, ".
								"alHistoricoUnidadesTbl hu, alUnidadesTbl un, caDistribuidoresCentrosTbl dc, caRegionesTbl re ".
							"WHERE tv.idViajeTractor = vt.idViajeTractor ".
							"AND vt.idTractor = tr.idTractor ".
							"AND tv.idTalon = ut.idTalon ".
							"AND hu.vin = ut.vin ".
							"AND un.vin = hu.vin ".
							"AND dc.distribuidorCentro = hu.distribuidor ".
							"AND re.idRegion = dc.idRegion ".
							"AND tv.centroDistribucion = 'CDTOL' ".
							"AND (tr.compania = 'CR' OR tr.compania = 'TR') ".
							"AND tv.tipoDocumento = 'TF' ".
							"AND (tv.claveMovimiento ='TF' OR tv.claveMovimiento = 'TE') ".
							"AND tv.fechaEvento >= '".date('Y-m-d', strtotime('-2 days'))."' ".
							"AND tv.fechaEvento <= '".date("Y-m-d 23:59:59")."' ".
							"AND hu.fechaEvento = ( ".
								"SELECT MAX(hu2.fechaEvento) FROM alHistoricoUnidadesTbl hu2 ".
	            				"WHERE hu2.vin = hu.vin) ".
	        				"AND hu.idTarifa != ( ".
								"SELECT tf.idTarifa FROM caTarifasTbl tf ".
					            "WHERE tf.tarifa = '13') ".
							"AND dc.tipoDistribuidor = 'DX' ".
							"ORDER BY tv.folio ";

			$rs = fn_ejecuta_query($sqlGetTalones);
			//echo json_encode($rs);

			if(sizeof($rs['root']) > 0){
				$unidades = $rs['root'];

				$numeroTalones = $unidades[0]['numeroTalones'];
				$sinApertura = 0;
				$sinPrecio = 0;
				$totalSeries = 0;
				$registrosExistentes = 0;
				$unidadesInsertadas = 0;

				$fechaApertura = date("Ym");
				$tipoTransporte = 'O';

				$sqlCheckPedimentosApertura = "SELECT * FROM alPedimentosConsolidadosTbl";
				$sqlCheckPrecioUnidad = "SELECT * FROM alPreciosShippersTbl WHERE vin =";
				$sqlCheckRegistroShipper = "SELECT 1 FROM alShippersPdfTbl WHERE vin =";


				$sqlAddShipper = "INSERT INTO alShippersPdfTbl (folioFactura,numFurgon,fddFactura,fmmFactura,faaFactura,".
								 "planta,numSerie,vin,von,distribuidor,simbolo,opciones,descEspaniol,descIngles,impUndbasS,impUndbas,".
								 "impSeguroS,impSeguro,impFleldoS,impFleldo,impGtosaduS,impGtosadu,impFledistS,impFledist,impSfledistS,".
								 "impSfledist,impUnidadS,impUnidad,tipoCambio,codigoReg,val807UndbasS,val807Undbas,val807OpcsS,".
								 "val807Opcs,destinatario,direccion,localidad,tfacUndbasS,tfacUndbas,tfacOpcsS,tfacOpcs,tfacSeguroS,".
								 "tfacSeguro,tfacFleldoS,tfacFleldo,tfacGtosaduS,tfacGtosadu,tfacFledistS,tfacFledist,tfacSfledistS,".
								 "tfacSfledist,tfacUnidadS,tfacUnidad,tfacConletra,lugarYfecha,numUnd,impMonnalS,impMonnal,tipoCambios2,".
								 "routeKing,patenteDesc,patenteCont,rfcDesc,rfcCont,paisDestDesc,paisDestCont,cvePedDesc,cvePedCont,".
								 "numpedDesc,numpedCont,acuseRboDesc,acuseRboCont,shippedTo,xworkPriceS,xworkPrice,numConsec,numEmb,".
								 "fddEmb,fmmEmb,faaEmb,tshpUndbasS,tshpUndbas,tshpOpcsS,tshpOpcs,tshpSeguroS,tshpSeguro,tshpFleldoS,".
								 "tshpFleldo,tshpGtosaduS,tshpGtosadu,tshpFledistS,tshpFledist,tshpSfledistS,tshpSfledist,tshpEmbdS,".
								 "tshpEmbd,tshpNumUnd,cancelacion,filler,fOrig,cEstatus,fEstatus) VALUES (%V)";

				foreach($unidades as &$unidad){

					$whereStr = "";

					if($unidad['distribuidor'] == 'GCML' && $unidad['region'] == 'R8'){
						// USA PTO LZC
						$whereStr = " WHERE paisDestino = 'G8' AND filler LIKE 'LZC%' ";
					} else if($unidad['region'] == 'R9' || $unidad['region'] == 'R2') {
						//CHINA
						$whereStr = " WHERE paisDestino = 'Z3' ";
					} else if(($unidad['distribuidor'] = 'CPV' && $unidad['region'] == 'R8') || 
							  ($unidad['region'] == 'R8' && $unidad['distribuidor'] != 'GCML')){
						//USA PTO VER
						$whereStr = " WHERE paisDestino = 'G8' AND filler LIKE 'VER%' ";
					} else if($unidad['region'] == 'R3'){
						//ALEMANIA
						$whereStr = " WHERE paisDestino = 'A4' ";
					} else if($unidad['region'] == 'R1' || $unidad['region'] == 'R4' || $unidad['region'] == 'R7'){
						//BRASIL
						$whereStr = " WHERE paisDestino = 'C8' ";
					}
					
					$rs = fn_ejecuta_query($sqlCheckPedimentosApertura.$whereStr);
					//echo json_encode($rs);

					// REVISAR PARA PEDIMENTOS DE APERTURA (CONSOLIDADOS)
					if(sizeof($rs['root']) > 0){
						$pedimentos = $rs['root'][0];
						$lugarFecha = 'MEXICO, D.F. A '.
								  substr($rs['root'][0]['fechaAprobacion'], -2).
								  ' DE '.
								  strtoupper(strftime("%B",strtotime($rs['fechaAprobacion']))).
								  ' DE '.
								  substr($rs['root'][0]['fechaAprobacion'], 0, 4);
						$unidad['lugarFecha'] = $lugarFecha;
					} else {
						$sinApertura += 1;
						fwrite($log, "NO EXISTE APERTURA alPedimentosConsolidadosTbl   ".
									 $unidad['centroDistribucion']." ".$unidad['vin']." ".$unidad['folio']." ".
									 $unidad['simboloUnidad']." ".$sinApertura.PHP_EOL);

						$unidad['invalida'] = 1;
					}

					//PRECIOS DE LA UNIDAD
					if($unidad['invalida'] != 1){
						$rs = fn_ejecuta_query($sqlCheckPrecioUnidad."'".$unidad['vin']."'");
						//echo json_encode($rs);

						if(sizeof($rs['root']) > 0){
							$precios = $rs['root'][0];
							$totalSeries += 1;
						} else {
							$sinPrecio += 1;
							fwrite($log, "NO EXISTE PRECIOS REPORTADOS   ".
										 $unidad['centroDistribucion']." ".$unidad['vin']." ".$unidad['folio']." ".
										 $unidad['simboloUnidad']." ".$sinPrecio.PHP_EOL);

							$unidad['invalida'] = 1;
						}
					}

					//REGISTRO DE SHIPPER
					if($unidad['invalida'] != 1){
						$rs = fn_ejecuta_query($sqlCheckRegistroShipper."'".$unidad['vin']."'");
						//echo json_encode($rs);

						if(sizeof($rs['root']) > 0){
							$registrosExistentes += 1;
							fwrite($log, "YA EXISTE REGISTRO alHistoricoShiperstbl   ".
										 $unidad['centroDistribucion']." ".$unidad['vin']." ".$unidad['folio']." ".
										 $unidad['simboloUnidad']." ".$registrosExistentes.PHP_EOL);

							$unidad['invalida'] = 1;
						}
					}

					//SOLO QUEDAN UNIDADES VALIDAS
					if($unidad['invalida'] != 1){
						$fechaTalon = explode('-', substr($unidad['fechaEvento'], 0, 10));
						$fechaFactura = explode('-', $precios['fechaFactura']);

						//CALCULO DE tfacSfledist SE NECESITARA MAS ADELANTE
						$tfacSfledist = 0.00;

						if($precios['unidadBasicaS'] == '-'){
							$tfacSfledist -= floatval($precios['unidadBasica']);
						} else {
							$tfacSfledist += floatval($precios['unidadBasica']);
						}

						if($precios['impSeguroS'] == '-'){
							$tfacSfledist -= floatval($precios['impSeguro']);
						} else {
							$tfacSfledist += floatval($precios['impSeguro']);
						}

						if($precios['fleteS'] == '-'){
							$tfacSfledist -= floatval($precios['flete']);
						} else {
							$tfacSfledist += floatval($precios['flete']);
						}

						if($precios['impAduanaS'] == '-'){
							$tfacSfledist -= floatval($precios['impAduana']);
						} else {
							$tfacSfledist += floatval($precios['impAduana']);
						}

						if($precios['impFleteUsaS'] == '-'){
							$tfacSfledist -= floatval($precios['impFleteUsa']);
						} else {
							$tfacSfledist += floatval($precios['impFleteUsa']);
						}

						//FOLIO EMBARQUE
						$sqlGetFolio = "SELECT folio FROM trFoliosTbl WHERE centroDistribucion = 'CDTOL' ".
								"AND compania = 'DT'";

						$rs = fn_ejecuta_query($sqlGetFolio);
						//echo json_encode($rs);

						if(sizeof($rs['root']) > 0){
							$folio = intval($rs['root'][0]['folio'])+1;
						} else {
							$a['success']  = false;
							$a['errorMessage'] = "No existen folios para embarques";
						}

						if($a['success']){
							$sqlValues =	"'".$precios['folioFacturaNormal']."','". //folioFactura
											$unidad['tractor']."',". //numFurgon
											"'".$fechaFactura[2]."',". //fddFactura
											"'".$fechaFactura[1]."',". //fmmFactura
											"'".$fechaFactura[0]."',". //faaFactura
											"'".$pedimentos['codigoPlanta']."',". //planta
											"'".substr($unidad['vin'], -8)."',". //numSerie
											"'".$unidad['vin']."',". //vin
											"'".$precios['von']."',". //von
											"'".$precios['distribuidor']."',". //distribuidor
											"'".$precios['simboloUnidad']."',". //simbolo
											"'".substr(str_replace(" ", "",$precios['claveOpciones']),0,120)."',". //opciones
											"'".$precios['descripcionEspanol']."',". //descEspaniol
											"'".$precios['descripcionIngles']."',". //descIngles
											"'".$precios['unidadBasicaS']."','". //impUndbasS	
											replaceEmptyFloat($precios['unidadBasica'])."',". //impUndbas
											"'".$precios['impSeguroS']."','". //impSeguroS	
											replaceEmptyFloat($precios['impSeguro'])."',". //impSeguro
											"'".$precios['fleteS']."','". //impFleldoS
											replaceEmptyFloat($precios['flete'])."',". //impFleldo
											"'".$precios['impAduanaS']."','". //impGtosaduS	
											replaceEmptyFloat($precios['impAduana'])."',". //impGtosadu
											"'".$precios['impFleteUsaS']."','". //impFledistS
											replaceEmptyFloat($precios['impFleteUsa'])."',". //impFledist
											"' ','". //impSfledistS
											"0.00',". //impSfledist
											"'".$precios['totalUnidadS']."','". //impUnidadS
											replaceEmptyFloat($precios['totalUnidad'])."','". //impUnidad
											replaceEmptyFloat($precios['tipoCambio'])."',". //tipoCambio
											"'1',". //codigoReg
											"'".$precios['unidadBasicaS']."','". //val807UndbasS
											replaceEmptyFloat($precios['unidadBasica'])."',". //val807Undbas
											"'".$precios['impOpcionesS']."','". //val807OpcsS
											replaceEmptyFloat($precios['impOpciones'])."',". //val807Opcs
											"'FCA US LLC',". //destinatario
											"'CIMS  485-11-25, 1000 CHRYSLER DRIVE ',". //direccion
											"'AUBURN HILLS, MI 48326-2766 U.S.A. 4502',". //localidad
											"'".$precios['totalUnidadS']."','".  //tfacUndbasS
											 "0.00',".//tfacUndbas
											"'".$precios['impOpcionesS']."','". //tfacOpcsS
											replaceEmptyFloat($precios['impOpciones'])."',".//tfacOpcs
											"'".$precios['fleteS']."','". //tfacSeguroS
											replaceEmptyFloat($precios['flete'])."',".//tfacSeguro
											"'".$precios['impAduanaS']."','". //tfacFleldoS
											replaceEmptyFloat($precios['impAduana'])."',". //tfacFleldo
											"'".$precios['tSinFleteUsS']."','". //tfacGtosaduS
											replaceEmptyFloat($precios['tSinFleteUs'])."',". //tfacGtosadu
											"'".$precios['impAduanaS']."','". //tfacFledistS
											replaceEmptyFloat($precios['tSinFleteUs'])."',". //tfacFledist
											"' ','". //tfacSfledistS
											strval($tfacSfledist)."',". //tfacSfledist
											"' ','". //tfacUnidadS
											strval($tfacSfledist)."',". //tfacUnidad
											"'".strtoupper(
												str_replace("M.N.", "", str_replace("PESOS", "U.S. DLLS.", numtoletras($tfacSfledist)))
												)."',". //tfacConletra
											"'".$unidad['lugarFecha']."',". //lugarYfecha
											"'1',". //numUnd
											"'".$precios['totVinPesosS']."','". //impMonnalS
											$precios['totVinPesos']."','". //impMonnal
											replaceEmptyFloat($precios['tipoCambio'])."',".//tipoCambios2
											"'VZ',". //routeKing
											"'PATENTE',". //patenteDesc
											"'".$pedimentos['patente']."',". //patenteCont
											"'R.F.C.: ',". //rfcDesc
											"'".$pedimentos['rfc']."',". //rfcCont
											"'XCLX DESTINO: ',". //paisDestDesc
											"'".$pedimentos['paisDestino']."',". //paisDestCont
											"'CLAVE PEDIMENTO: ',". //cvePedDesc
											"'".$pedimentos['tipoPedimento']."',". //cvePedCont
											"' ".$pedimentos['codigoPuerto']." PEDIMENTO: ',". //numpedDesc
											"'".$pedimentos['numeroPedimento']."',". //numpedCont
											"'ACUSE DE RECIBO: ',". //acuseRboDesc
											"'".$pedimentos['acuseRecibo']."',". //acuseRboCont
											"' ',". //shippedTo
											"' ',". //xworkPriceS
											"0.00,". //xworkPrice
											"1,". //numConsec
											"'FTA".str_pad($folio, 6, '0', STR_PAD_LEFT)."',". //numEmb
											"'".$fechaTalon[2]."',". //fddEmb
											"'".$fechaTalon[1]."',". //fmmEmb
											"'".$fechaTalon[0]."',". //faaEmb
											"'".$precios['unidadBasicaS']."','". //tshpUndbasS
											replaceEmptyFloat($precios['unidadBasica'])."',".//tshpUndbas
											"'".$precios['impSeguroS']."','".//tshpOpcsS
											replaceEmptyFloat($precios['impSeguro'])."',".//tshpOpcs
											"'".$precios['fleteS']."','".  //tshpSeguroS
											replaceEmptyFloat($precios['flete'])."',".//tshpSeguro
											"'".$precios['impAduanaS']."','". //tshpFleldoS
											replaceEmptyFloat($precios['impAduana'])."',".  //tshpFleldo
											"'".$precios['impFleteUsaS']."','". //tshpGtosaduS
											replaceEmptyFloat($precios['impFleteUsa'])."',". //tshpGtosadu
											"' ','". //tshpFledistS
											"0.00',". //tshpFledist
											"' ',".  //tshpSfledistS
											strval($tfacSfledist).",". //tshpSfledist
											"' ',". //tshpEmbdS
											strval($tfacSfledist).",". //tshpEmbd
											"1,".//tshpNumUnd
											"'A',".
											"' ',".
											"'".$precios['fechaOrigen']."',".
											"'EN',".
											"'".date("d/m/Y")."'";

							fn_ejecuta_query(str_replace("%V", $sqlValues, $sqlAddShipper));
							//echo json_encode($sqlValues, $sqlAddShipper);


							if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
								$sqlGetShipper = "SELECT * FROM alShippersPdfTbl WHERE vin = '".$unidad['vin']."'";
								$rs = fn_ejecuta_query($sqlGetShipper);
								if(sizeof($rs['root']) > 0){
									//SE GENERA EL ARCHIVO TXT
									$archivoSuccess = generarArchivo($fileArchivo, $rs['root'][0]);

									if($archivoSuccess){
										$unidadesInsertadas += 1;

										//UPDATE FOLIO
						                $sqlUpdFolio = "UPDATE trFoliosTbl SET folio = '".str_pad($folio, 2, '0', STR_PAD_LEFT)."' ".
						                				"WHERE centroDistribucion = 'CDTOL' AND compania = 'DT' ";

						                fn_ejecuta_query($sqlUpdFolio);

						                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
							                //UPDATE REGISTRO DE PRECIOS
							                $sqlUpdPrecios = "UPDATE alPreciosShippersTbl SET ".
							                				 "claveEstatus = 'EN', ".
							                				 "fechaEstatus = '".date("Y-m-d")."' ".
							                				 "WHERE idPreciosShipper = ".$precios['idPreciosShipper'];

							                fn_ejecuta_query($sqlUpdPrecios);

							                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
							                	//SE ACTUALIZA EL CONSECUTIVO DEL PEDIMENTO
							                	$sqlUpdPedimentos = "UPDATE alPedimentosConsolidadosTbl SET ".
							                						"consecutivo = ".(intval($pedimentos['consecutivo'])+1)." ".
							                						"WHERE fechaAprobacion = '".$pedimentos['fechaAprobacion']."' ".
							                						"AND tipoTransporte = '".$pedimentos['tipoTransporte']."' ".
							                						"AND paisDestino = '".$pedimentos['paisDestino']."' ".
							                						"AND filler = '".$pedimentos['filler']."'";

							                	fn_ejecuta_query($sqlUpdPedimentos);

							                	if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
							                		$a['successMessage'] = "Shipper Generado Correctamente";
							                	} else {
							                		$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdPedimentos;
							                	}
							                } else {
							                	$a['success'] = false;
							                	$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdPrecios;
							                }
							            } else {
							                $a['success'] = false;
							                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdFolio;
							            }
									} else {
										$a['success'] = false;
										$a['errorMessage'] = "Error al generar el archivo TXT de shippers";

										fn_ejecuta_query("DELETE FROM alShippersPdfTbl WHERE vin = '".$unidad['vin']."'");
									}
								} else {
									$a['success'] = false;
									$a['errorMessage'] = "Error al obtener los datos de la tabla de Shippers";

									fn_ejecuta_query("DELETE FROM alShippersPdfTbl WHERE vin = '".$unidad['vin']."'");
								}
				            } else {
				                $a['success'] = false;
				                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . str_replace("%V", $sqlValues, $sqlAddShipper);
				            }
						}
					}		
				}

				if($a['success'] && $a['successMessage'] == "")
					$a['successMessage'] = "Shipper Generado Correctamente";

				//LOG INSERTS
        		fwrite($log, 'Registros Talones Recuperados: '.$numeroTalones.PHP_EOL);
        		fwrite($log, 'Registros sin apertura de mes: '.$sinApertura.PHP_EOL);
        		fwrite($log, 'Registros sin precio ya regis: '.$sinPrecio.PHP_EOL);
        		fwrite($log, 'Registros series recuperados : '.$totalSeries.PHP_EOL);
        		fwrite($log, 'Registros con shipper ya regi: '.$registrosExistentes.PHP_EOL);
        		fwrite($log, 'Registros shipper generados  : '.$unidadesInsertadas.PHP_EOL);
        		fwrite($log, 'Termino:  '.date("d/m/Y").' <a> '.date("H:i:s"));

        		//COPIA DE ARCHIVO
        		if($archivoSuccess){
        			if(!copy($fileArchivo, str_replace('.txt', '.man', $fileArchivo))){
        				$a['successMessage'] .= " Error al generar el archivo .man .";
        			}
        			if(!copy($fileArchivo, str_replace('.txt',date("dmYHis").".txt",str_replace('files', 'enviados', $fileArchivo)))){
        				$a['successMessage'] .= " Error al copiar el archivo de Interfaz.";
        			}
        		}
			} else {
				$a['success'] = false;
				$a['errorMessage'] = "No existen talones en el periodo ".$fechaInicio." - ".date("d/m/Y");
			}
		} else {
			$a['success'] = false;
			$a['errorMessage'] = "Error al crear el archivo de LOG";
		}

		echo json_encode($a, JSON_UNESCAPED_SLASHES);
	}

	function generarArchivo($fileArchivo, $shipper){
		$archivo = fopen($fileArchivo, 'w');

		if($archivo){
			fwrite($archivo, str_pad($shipper['folioFactura'], 9, ' '));
			fwrite($archivo, str_pad($shipper['numFurgon'], 10, ' '));
			fwrite($archivo, str_pad($shipper['fddFactura'], 2, ' '));
			fwrite($archivo, str_pad($shipper['fmmFactura'], 2, ' '));
			fwrite($archivo, str_pad($shipper['faaFactura'], 4, ' '));
			fwrite($archivo, str_pad($shipper['planta'], 5, ' '));
			fwrite($archivo, str_pad($shipper['numSerie'], 8, ' '));
			fwrite($archivo, str_pad($shipper['vin'], 17, ' '));
			fwrite($archivo, str_pad($shipper['von'], 8, ' '));
			fwrite($archivo, str_pad($shipper['distribuidor'], 7, ' '));
			fwrite($archivo, str_pad($shipper['simbolo'], 6, ' '));
			fwrite($archivo, str_pad($shipper['opciones'], 120, ' '));
			fwrite($archivo, str_pad($shipper['descEspaniol'], 31, ' '));
			fwrite($archivo, str_pad($shipper['descIngles'], 31, ' '));
			fwrite($archivo, str_pad($shipper['impUndbasS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impUndbas'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['impSeguroS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impSeguro'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['impFleldoS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impFleldo'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['impGtosaduS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impGtosadu'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['impFledistS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impFledist'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['impSfledistS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impSfledist'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['impUnidadS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impUnidad'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['impUnidadS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impUnidad'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad(number_format($shipper['tipoCambio'], 4, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['codigoReg'], 2, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['val807UndbasS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['val807Undbas'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['val807OpcsS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['val807Opcs'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['destinatario'], 40, ' '));
			fwrite($archivo, str_pad($shipper['direccion'], 40, ' '));
			fwrite($archivo, str_pad($shipper['localidad'], 40, ' '));
			fwrite($archivo, str_pad($shipper['tfacUndbasS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tfacUndbas'],2, '.', ''), 9, '0', STR_PAD_LEFT));			
			fwrite($archivo, str_pad($shipper['tfacOpcsS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tfacOpcs'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tfacSeguroS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tfacSeguro'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tfacFleldoS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tfacFleldo'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tfacGtosaduS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tfacGtosadu'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tfacFledistS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tfacFledist'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tfacSfledistS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tfacSfledist'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tfacUnidadS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tfacUnidad'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tfacConletra'], 100, ' '));
			fwrite($archivo, str_pad($shipper['lugarYfecha'], 40, ' '));
			fwrite($archivo, str_pad($shipper['numUnd'], 2, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['impMonnalS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['impMonnal'],2, '.', ''), 13, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad(number_format($shipper['tipoCambios2'], 4, '.', ''), 10, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['routeKing'], 2, ' '));
			fwrite($archivo, str_pad($shipper['patenteDesc'], 9, ' '));
			fwrite($archivo, str_pad($shipper['patenteCont'], 2, ' '));
			fwrite($archivo, str_pad($shipper['rfcDesc'], 8, ' '));
			fwrite($archivo, str_pad($shipper['rfcCont'], 13, ' '));
			fwrite($archivo, str_pad($shipper['paisDestDesc'], 14, ' '));
			fwrite($archivo, str_pad($shipper['paisDestCont'], 2, ' '));
			fwrite($archivo, str_pad($shipper['cvePedDesc'], 17, ' '));
			fwrite($archivo, str_pad($shipper['cvePedCont'], 2, ' '));
			fwrite($archivo, str_pad($shipper['numpedDesc'], 16, ' '));
			fwrite($archivo, str_pad($shipper['numpedCont'], 7, ' '));
			fwrite($archivo, str_pad($shipper['acuseRboDesc'], 16, ' '));
			fwrite($archivo, str_pad($shipper['acuseRboCont'], 8, ' '));
			fwrite($archivo, str_pad($shipper['shippedTo'], 28, ' '));
			fwrite($archivo, str_pad($shipper['xworkPriceS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['xworkPrice'],2, '.', ''), 8, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['numConsec'], 2, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['fddEmb'], 2, ' '));
			fwrite($archivo, str_pad($shipper['fmmEmb'], 2, ' '));
			fwrite($archivo, str_pad($shipper['faaEmb'], 4, ' '));
			fwrite($archivo, str_pad($shipper['numEmb'], 9, ' '));
			fwrite($archivo, str_pad($shipper['tshpUndbasS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tshpUndbas'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tshpOpcsS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tshpOpcs'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tshpSeguroS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tshpSeguro'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tshpFleldoS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tshpFleldo'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tshpGtosaduS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tshpGtosadu'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tshpFledistS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tshpFledist'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tshpSfledistS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tshpSfledist'],2, '.', ''), 9, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tshpEmbdS'], 1, ' '));
			fwrite($archivo, str_pad(number_format($shipper['tshpEmbd'],2, '.', ''), 12, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['tshpNumUnd'], 2, '0', STR_PAD_LEFT));
			fwrite($archivo, str_pad($shipper['cancelacion'], 1, ' '));
			fwrite($archivo, str_pad($shipper['filler'], 2, ' '));


			return true;
		} else{ 
			return false;
		}
	}

?>