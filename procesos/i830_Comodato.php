<?php
	/*require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    i830_Comodato();*/

	function i830_Comodato(){
	    $startDate = date('Y-m-d', strtotime("-20 days")); //- 7 días
	    $today = date('Y-m-d');
	    $todayDateTime = date('Y/m/d H:i:s');
			echo "inicio i830".$todayDateTime;

	    $p01Count = 0; //Contador de unidades con clave de movimiento 'AM'
	    $d04Count = 0; //Contador de unidades con clave de movimiento 'OM'

	    //$unidadesSin660 = getUnidadesSin660($startDate, $today);
	    $unidadesCon660 = getUnidadesCon660($startDate, $today);

	    //Conteo de unidades AM-P01, OM-D04
	    foreach ($unidadesCon660 as $unidad) {
	    	if ($unidad['claveMovimiento'] == 'AM')
		    	$p01Count++;
		    else //OM
		    	$d04Count++;
	    }

	    //Se genera archivo de unidades que no están en la 660
	    //createLogSin660($unidadesSin660, "RAT660.FAL");

	    //Ordena las Unidades con 660 por Fecha y Tractor para creación de Logs *No sé si sea mejor ordenar en el query;
	    //usort($unidadesCon660, 'sortByFechaYTractor');

	    //Se genera archivo de unidades que están en la 660
	    createArchivoCon660($unidadesCon660);

	    //Se genera Log de Avisos de Proceso y Cifras de Control, éste siempre se ejecuta...
	    //createLogProceso($p01Count, $d04Count, $todayDateTime);

	    //Se genera Log de Cifras de Control, sólo se ejecuta si hubo movimientos...
	    //createLogProceso2($p01Count, $d04Count, $todayDateTime);

	    //Se Afecta la BD insertando las unidades en alTransaccionUnidadTbl
	    insertTransacciones($unidadesCon660);

	    if (isset($unidadesCon660) && count($unidadesCon660) > 0)
	    	updFolio();
	}

//-----FIN TRAP830------------------------------------------------------------------------------------------------------
    /*
    * DECLARACIÓN Y DEFINICION DE FUNCIONES PARA LA INTERFAZ TRAP830
    *
    */
    //Obtiene un folio de parámetros de Sistema...
    function getFolio() {
    	//Obtiene el Folio de parametros del sistem
    	$sqlGetFolio = "SELECT f.folio FROM trFoliosTbl f ".
    				   "WHERE f.centroDistribucion = 'TCO' ".
    				   "AND f.compania = '830' ".
    				   "AND f.tipoDocumento = 'T';";

    	$rsFolio = fn_ejecuta_query($sqlGetFolio);

    	$folio = $rsFolio['root'][0]['folio'];

    	return $folio;
    }
    //Actualiza(autoincrementa) el Folio de parámetros de Sistema...
    function updFolio() {
    	//Si no existió ningún tipo de error actualiza el folio
    	if (!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {

    		$folio = getFolio();
	    	$sqlUpdFolio = "UPDATE trFoliosTbl ".
	    				   "SET folio = '".(intval($folio) - 1)."' ".
	    				   "WHERE centroDistribucion = 'TCO' ".
	    				   "AND compania = '830' ".
	    				   "AND tipoDocumento = 'T';";

	    	fn_ejecuta_query($sqlUpdFolio);
    	}
    }

    function getUnidadesSin660($startDate, $endDate) {

    	$sqlExcluir660 = "(SELECT a6.vin FROM al660Tbl a6)";

    	$sqlHistoricoSin660 = "SELECT hu2.* FROM alHistoricoUnidadesTbl hu2 ".
			    				"WHERE hu2.idTarifa <> (SELECT t.idTarifa FROM caTarifasTbl t WHERE t.tarifa = '13') ".
			    				"AND hu2.claveMovimiento IN ('AM', 'OM') ".
			    				"AND hu2.centroDistribucion IN ('CDTOL', 'CDSAL', 'CDAGS', 'CDSFE', 'CDANG') ".
			    				"AND DATE(hu2.fechaEvento) BETWEEN '$startDate' AND '$endDate' ".
			    				//"AND DATE(hu2.fechaEvento) BETWEEN '2017-10-01' AND '2017-10-31' ".
			                    "AND hu2.vin NOT IN $sqlExcluir660;";

	    $rsUnidadesSin660 = fn_ejecuta_query($sqlHistoricoSin660);

	    return $rsUnidadesSin660['root'];
    }

	function getUnidadesCon660($startDate, $endDate) {

		$sqlExcluirDY = "(SELECT im.vin FROM al660tbl im)";

		$sqlTransaccionExistente = " hu.vin in (SELECT tu.VIN FROM altransaccionunidadtbl tu
													WHERE hu.vin=tu.vin and tu.tipoTransaccion in ('D04','P01'))";



						$sqlHistorico660 = "SELECT hu.vin, adddate(hu.fechaevento , interval 1 minute) as fechaEvento,hu.claveMovimiento, hu.distribuidor as disUnidad, tl.distribuidor as disTalon, tl.folio, ta.tractor, tl.folio as talon ".
																"FROM alhistoricounidadestbl hu, trviajestractorestbl vt, trtalonesviajestbl tl, trunidadesdetallestalonestbl ts, alunidadestbl tr, casimbolosunidadestbl su, catractorestbl ta ".
																"WHERE  hu.claveMovimiento = 'AM' ".																
																"AND DATE(hu.fechaEvento) BETWEEN cast('".$startDate."' as date) AND cast('".$endDate."' as date) ".
																//"AND DATE(hu.fechaEvento) BETWEEN cast('2019-01-01' as date) AND cast('2019-07-22' as date)  ".
																"AND hu.centroDistribucion IN ('CMDAT') ".
																"AND ts.vin = hu.vin ".
																///"AND hu.vin='3C4NJCBHXKT831521' ".
																"AND    ts.estatus != 'CA' ".
																"AND tr.vin = hu.vin ".
																"AND vt.idTractor = ta.idTractor ".
																"AND cast(hu.fechaEvento as date) = cast(tl.fechaEvento as date) ".
																"AND tr.simboloUnidad = su.simboloUnidad ".
																"AND hu.idTarifa not in (SELECT tar.idTarifa  FROM catarifastbl tar WHERE tar.tarifa = '13') ".
																"AND su.marca not in('KI','HY') ".
																//"AND vt.centroDistribucion=hu.centroDistribucion ".
																"AND vt.idViajetractor = tl.idViajeTractor ".
																"AND tl.idTalon = ts.idTalon ".
																"AND    tl.claveMovimiento !='TX' ".
																"AND tr.vin not in(SELECT rv.vin FROM altransaccionunidadtbl rv WHERE rv.tipoTransaccion = 'P01' AND cast(rv.fechaMovimiento as date)= cast(hu.fechaEvento as date)) ".
																"UNION ".
																"SELECT hu.vin, adddate(hu.fechaevento , interval -1 minute) as fechaEvento,hu.claveMovimiento, hu.distribuidor as disUnidad, tl.distribuidor as disTalon, tl.folio, ta.tractor, tl.folio as talon ".
																"FROM alhistoricounidadestbl hu, trviajestractorestbl vt, trtalonesviajestbl tl, trunidadesdetallestalonestbl ts, alunidadestbl tr, casimbolosunidadestbl su, catractorestbl ta ".
																"WHERE  hu.claveMovimiento = 'OM' ".																
																"AND DATE(hu.fechaEvento) BETWEEN cast('".$startDate."' as date) AND cast('".$endDate."' as date) ".
																//"AND DATE(hu.fechaEvento) BETWEEN cast('2019-01-01' as date) AND cast('2019-07-22' as date)  ".
																"AND hu.centroDistribucion IN ('CMDAT') ".
																"AND ts.vin = hu.vin ".
																///"AND hu.vin='3C4NJCBHXKT831521' ".
																"AND    ts.estatus != 'CA' ".
																"AND tr.vin = hu.vin ".
																"AND vt.idTractor = ta.idTractor ".
																//"AND cast(hu.fechaEvento as date) = cast(tl.fechaEvento as date) ".
																"AND tr.simboloUnidad = su.simboloUnidad ".
																"AND hu.idTarifa not in (SELECT tar.idTarifa  FROM catarifastbl tar WHERE tar.tarifa = '13') ".
																"AND su.marca not in('KI','HY') ".
																//"AND vt.centroDistribucion = hu.centroDistribucion ".
																"AND vt.idViajetractor = tl.idViajeTractor ".
																"AND tl.idTalon = ts.idTalon ".
																"AND    tl.claveMovimiento !='TX' ".
																"AND tr.vin not in(SELECT rv.vin FROM altransaccionunidadtbl rv WHERE rv.tipoTransaccion = 'D04' AND cast(rv.fechaMovimiento as date)= cast(hu.fechaEvento as date)) ".
																"group by 1,2,3,4;";

		$rsUnidades660 = fn_ejecuta_query($sqlHistorico660);
		//echo $sqlHistorico660."<br><br><br>";
		//echo json_encode($rsUnidades);
		return $rsUnidades660['root'];
	}
	function createArchivoCon660($unidades) {
		if (!isset($unidades) || count($unidades) < 1)
			return;
		/*El nombre del archivo es un texto de 12 caracteres
    	 *pos 0 valor fijo : 'IGSC'  												Longitud: 4 caracteres
    	 *pos 4 folio : USING(&&&&) *Valor autoincremental y     					Longitud: 4 caracteres
    	 *							es actualizado cada que sea utilizado
    	 *							siempre y cuando existan movimentos
    	 *							Viene de caGeneralesTbl
    	 *pos 8 valor fijo : '.R41'													Longitud: 4 caracteres
    	 */
		$folio = getFolio();
    	$fileName = "IGSC".sprintf("%04d", $folio).".R41";
		//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir = "E:/carbook/i830_Comodato/".$fileName;
		$fileDir = fopen("E:/carbook/i830_Comodato//".$fileName, 'a') or die('No se pudo generar Reporte');
		$todayDateTime = date_create(date('Y/m/d H:i:s'));

		$encabezado =  array(
							 1 => 'IGR41',
							 6 => date_format($todayDateTime, 'm'),
							 8 => date_format($todayDateTime, 'd'),
							10 => date_format($todayDateTime, 'y'),
							12 => date_format($todayDateTime, 'H'),
							14 => date_format($todayDateTime, 'i'),
							16 => sprintf('%06d', (count($unidades) + 2)),
							22 => 'IGSC',
							26 => sprintf('%04d', $folio),
							30 => '.R41'
							);

		fwrite($fileDir, getTxt2($encabezado).out('n', 1));

		foreach ($unidades as $unidad) {
			$fechaEvento = date_create($unidad['fechaEvento']);

			//Incrementa los contadores ya sea AM o OM
			if ($unidad['claveMovimiento'] == 'AM') {
				$p01Count++;
			} else if($unidad['claveMovimiento'] == 'OM') {
				$d04Count++;
			}

			$sqlGetSplc = "SELECT splcCode ".
									"FROM casplctbl ".
									"WHERE patioCode = '".$unidad['disTalon']."';";

		 $rsGetSplc = fn_ejecuta_query($sqlGetSplc);

		 if($rsGetSplc['records'] == '1'){
			 $splcUnd = $rsGetSplc['root'][0]['splcCode'];
		 }else{
			 $splcUnd= $unidad['disUnidad'];
		 }

			$registro =  array(
							 1 => $unidad['talon'],
							 9 => out('s', 7),
							16 => $unidad['vin'],
							33 => date_format($fechaEvento, 'm'),
							35 => date_format($fechaEvento, 'd'),
							37 => date_format($fechaEvento, 'y'),
							39 => date_format($fechaEvento, 'H'),
							41 => date_format($fechaEvento, 'i'),
							43 => getTransaccion($unidad['claveMovimiento']),
							46 => out('s', 19),
							65 => sprintf('%-9s',$splcUnd),
							70 => out('s', 23),
							93 => $unidad['tractor'],
						   101 => out('s', 12)
							);

			fwrite($fileDir, getTxt2($registro).out('n', 1));
		}
		$trailer = array(
						 1 => 'EOF',
						 2 => sprintf('%06d', (count($unidades) + 2))
						);
		fwrite($fileDir, getTxt2($trailer).out('n', 1));

		fclose($fileDir);

		subirFtp_Comodato($fileName);
	}
	function createLogSin660($unidades, $fileName) {

		if (!isset($unidades) || count($unidades) < 1)
			return;

		//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir = "E:/carbook/i830_Comodato//".$fileName;
		$fileDir = fopen("E:/carbook/i830_Comodato//".$fileName, 'a') or die('No se pudo generar Reporte');
		foreach ($unidades as $unidad) {
			fwrite($fileDir, "NO EXISTE trat660 |".$unidad['vin']." |   |". substr($unidad['vin'], 9).out('n', 1));
		}
		fclose($fileFir);

	}
	//Log de Avisos de Proceso y Cifras de Control, éste siempre se ejecuta...
	function createLogProceso($p01Count, $d04Count, $todayDateTime) {
		/* NOMBRE DE ARCHIVO DE 12 CARACTERES + año mes dia hora minuto segundo .log
		*  IGSCffff.R41aaaammddhhmmss.log
		*  Se crea haya o no movimientos
		*/
		$todayDateTime = date_create($todayDateTime);
		$fechaInicio = date_format($todayDateTime, 'd/m/Y');
		$tiempoInicio = date_format($todayDateTime, 'H:i:s');
		$fileName =  "IGSCffff.R41".date("Ymdhis");

		//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir = "E:/carbook/i830_Comodato//".$fileName;
		$fileDir = fopen("E:/carbook/i830_Comodato//".$fileName, 'a') or die('No se pudo generar Reporte');

		fwrite($fileDir, "=============================================================>".out('n',1));
		fwrite($fileDir, "Inicio...........|".$fechaInicio."|<a>|".$tiempoInicio."|".out('n',1));
		fwrite($fileDir, "Archivo generado:|IGSCffff.R41 |||".out('n',1));
		fwrite($fileDir, "Movtos P01:  |".$p01Count."   |||".out('n',1));
		fwrite($fileDir, "Movtos D04:  |".$d04Count."   |||".out('n',1));
		fwrite($fileDir, "Movtos generados:|".($p01Count + $d04Count)."   |||".out('n',1));
		$tiempoFin = date('H:i:s');
		fwrite($fileDir, "Termino..........|".date('d/m/Y')."|<a>|".$tiempoFin."|".out('n',1));
		fwrite($fileDir, "=============================================================>".out('n',1));

		fclose($fileDir);

	}
	//Log de Cifras de Control, sólo se ejecuta si hubo movimientos...
	function createLogProceso2($p01Count, $d04Count, $todayDateTime) {
		/* NOMBRE DE ARCHIVO DE 12 CARACTERES
		*  IGSCffff.R41.log
		*  Solo se ejecuta si hay movimientos
		*/

		if ($p01Count + $d04Count == 0)
			return;

		$todayDateTime = date_create($todayDateTime);
		$fechaInicio = date_format($todayDateTime, 'd/m/Y');
		$tiempoInicio = date_format($todayDateTime, 'H:i:s');
		$fileName =  "IGSCffff.R41.log";

		//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir = "E:/carbook/i830_Comodato//".$fileName;
		$fileDir = fopen("E:/carbook/i830_Comodato//".$fileName, 'a') or die('No se pudo generar Reporte');

		fwrite($fileDir, "=============================================================>".out('n',1));
		fwrite($fileDir, "Inicio...........|".$fechaInicio."|<a>|".$tiempoInicio."|".out('n',1));
		fwrite($fileDir, "Movtos generados:|".($p01Count + $d04Count)."   |||".out('n',1));
		$tiempoFin = date('H:i:s');
		fwrite($fileDir, "Termino..........|".date('d/m/Y')."|<a>|".$tiempoFin."|".out('n',1));
		fwrite($fileDir, "=============================================================>".out('n',1));

		fclose($fileDir);

	}
	function insertTransacciones($unidades) {

		if (!isset($unidades) || count($unidades) == 0)
			return;

		$folio = getFolio();

		$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl ".
								"(tipoTransaccion, centroDistribucion, folio, vin, ".
								"fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, ".
								"prodStatus, fecha, hora) ".
								"VALUES";

		for ($i = 0; $i < count($unidades); $i++) {
			if ($i > 0)
				$sqlInsertTransaccion .= ", ";

			$unidad = $unidades[$i];
			//Fomatea la fecha y hora proveniente de la 660
			$vupdateParse = date_format(DateTime::createFromFormat('d/m/Y', $unidad['vupdate']), 'Y/m/d');
			$vuptimeParse = substr($unidad['vuptime'], 0, 5);
			echo $vupdateParse;
			echo $vuptimeParse;

			$sqlInsertTransaccion .= "('".getTransaccion($unidad['claveMovimiento'])."', ".
									 "'CMDAT', ".
									 "'".$folio."', ".
									 "'".$unidad['vin']."', ".
									 "'".date('Y-m-d H:i:s')."', ".
									 "'".$unidad['claveMovimiento']."', ".
									 "'".$unidad['fechaEvento']."', ".
									 "'".$unidad['prodStatus']."', ".
									 "'".$vupdateParse."', ".
									 "'".$vuptimeParse."')";
		}
		fn_ejecuta_query($sqlInsertTransaccion);
	}

	function sortByFechaYTractor($a, $b) {
	    if (($a['fechaEvento'] == $b['fechaEvento'])) {

	    	return ($a['tractor'] - $b['tractor']);
	    }
	    return (date_create($a['fechaEvento']) < date_create($b['fechaEvento'])) ? -1 : 1;
	}

	function getTransaccion($claveMovimiento) {

		return $claveMovimiento == 'AM' ? 'P01' : 'D04';
	}


	function getTxt2($texts){
        $positions = array_keys($texts);
        $text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = $positions[$i - 1] - 1;
                $antLength = strlen($texts[$positions[$i - 1]]);
            }
            $text .= out('s', ($positions[$i] - 1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }

     function subirFtp_Comodato($nombreArchivo){
        $fileDir = "E:/carbook/i830_Comodato/".$nombreArchivo;
        if(file_exists($fileDir)){
            # Definimos las variables
            $host = "ftp.iclfca.com";
            //$port = 21;
            $user = "IG";
            $password = "dBJY76ig";
            $ruta = "/ig/SC/";
            $file = $fileDir;//tobe uploaded
            $remote_file = $nombreArchivo;
            $nuevo_fichero = "E:/carbook/i830_Comodato/respaldo/".$nombreArchivo;

            # Realizamos la conexion con el servidor
            $conn_id = @ftp_connect($host);//,$port);
            if($conn_id){
                    # Realizamos el login con nuestro usuario y contraseña
                if(@ftp_login($conn_id,$user,$password)){
                    # Canviamos al directorio especificado
                    if(@ftp_chdir($conn_id,$ruta)){
                        # Subimos el fichero
                        if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
                        echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
                        }else{
                            echo "No ha sido posible subir el fichero";
                        }
                    }else
                        echo "No existe el directorio especificado";
                }else
                    echo "El usuario o la contraseña son incorrectos";
                    # Cerramos la conexion ftp
                    ftp_close($conn_id);
            }else
                echo "No ha sido posible conectar con el servidor";
        }else{
            echo "no existe el archivo";
        }if(!copy($file, $nuevo_fichero)){
            echo "Error al copiar $fichero...\n";
        }else{
            unlink($file);
            echo "si se copio el archivo";
        }
    }

?>
