<?php

	require_once("../funciones/generales.php");
	//require("../funciones/utilidades.php");
    //require("../funciones/utilidadesProcesos.php");


	$sqlDatos="SELECT dt.centroDistribucion, dt.vin, dt.claveMovimiento, dt.fechaEvento, dy.vin, dy.cveDisEnt,"
			." dy.cveLoc, tl.idViajeTractor, tl.companiaRemitente, rv.fechaGeneracionUnidad, tl.tipoDocumento"
			." FROM alhistoricounidadestbl dt, alinstruccionesmercedestbl dy,  altransaccionunidadtbl rv,trtalonesviajestbl tl "
			." WHERE dt.centroDistribucion in ('CDVER','CDLZC','CDTOL','CDSFE')"
			." AND dt.claveMovimiento='AM'"
			." AND dt.idTarifa='01'"
			." AND dt.fechaEvento BETWEEN '2016-09-21' AND '2016-09-26'"
			." AND dt.vin = dy.vin"
			." AND dy.cveStatus='QK'"
			." AND dy.cveDisEnt =(SELECT dc.distribuidorCentro FROM cadistribuidorescentrostbl dc "
								." WHERE dy.cveDisEnt = dc.distribuidorCentro)"
			." AND tl.tipoDocumento ='R'"
			." AND dt.distribuidor = tl.distribuidor"
			." AND tl.claveMovimiento != 'X'"
			." AND tl.idViajeTractor ='12'"
			." AND dt.vin = rv.vin"
			." AND rv.tipoTransaccion ='TLR'"
			." AND EXISTS (SELECT * FROM altransaccionunidadtbl tr "
						  ." WHERE tr.tipoTransaccion='ALR' AND tr.vin = dt.vin)"
			." ORDER BY tl.companiaRemitente, tl.idViajeTractor, dt.fechaEvento,dt.vin";
	$rstDatos=fn_ejecuta_query($sqlDatos);
	//echo json_encode($rstDatos);

	$sqlFolioStr = "select * from trfoliostbl where tipoDocumento='".$rstDatos['root'][0]['tipoDocumento']."' and centroDistribucion ='".$rstDatos['root'][0]['centroDistribucion']."'";
	$FolioRst = fn_ejecuta_query($sqlFolioStr);

	$folio = $FolioRst['root'][0]['folio'] +1;

	$sqlActualizaFolioStr="UPDATE trfoliostbl set folio =". $folio." where tipoDocumento='".$rstDatos['root'][0]['tipoDocumento']."' and centroDistribucion ='".$rstDatos['root'][0]['centroDistribucion']."'";
	$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

	generaYE($rstDatos,$folio);

	for ($i=0; $i <sizeof($rstDatos['root']) ; $i++) { 
		$sqlRC="SELECT * FROM altransaccionunidadtbl WHERE tipoTransaccion='EXT' AND vin='".$rstDatos['root'][$i]['vin']."'";
		$rstRC=fn_ejecuta_query($sqlRC);

		if ($rstRC['root'] != 0) {
			echo "unidad con EXT";
		}else{
			$RC[]=$rstDatos['root'][$i];
		}
	}
	
	generaRC($RC,$folio);

	function generaYE($rstDatos,$folio){
		$fecha=date('YmdHms');

		$directorio ="C:carbook/TRAP815extqk/";
		$inicioFile ="KMM_".$fecha.$hora.".txt";
		$archivo = fopen($directorio.$inicioFile,"w");
		

		//ARCHIVO RC
		$encabezado1 = array(1  => 'EXTH ',
	    			4  => 'TRA  ',
	    			19  =>'GMX  ',
	    			20=>'EXT'  ,
	    			29  => $fecha);
		$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
				    fwrite($archivo, $text);

		for ($i=0; $i <sizeof($RC) ; $i++) { 
					$encabezado1 = array(1  => 'EXT  ',
				    			4   => 'RC',
				    			19  => $RC['vin'],
				    			20  => date('YmdHms',strtotime($RC['fechaEvento'] )),
				    			29  => $RC['cveLoc']);
					$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
				    fwrite($archivo, $text);

		}
	



	}

	function generaRC($RC,$folio){
		echo json_encode($RC);

	}

	function getTxt($texts, $positions){
    	$text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = ($positions[$i]);
                $antLength = strlen($texts[$positions[$i]]);
            }
            $text .= out('s', ($positions[$i]-1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }
    function out($char, $times){
        $validChar = array(
                    "n" => PHP_EOL,
                    "t" => "\t",
                    "s" => " ");
        $chars = '';
        for ($i=0; $i < $times ; $i++) { 
            $chars .= $validChar[$char];
        }
        fseek($myfile, -20);   
        return $chars;
    }

?>