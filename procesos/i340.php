<?php
    session_start();
    /*require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");
    
    RA2Vcmdat();*/
    
    function RA2Vcmdat(){
    	echo "Inicio RA2V Comodato: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
    	$startDate = date('Y-m-d', strtotime("-15 days")); //- 5 días        
        $yesterday = date('Y-m-d', strtotime("-1 days"));
        $today = date('Y-m-d');
        $todayDateTime = date('Y-m-d H:i:s');


    	$sqlGeneraDatos = "SELECT h.vin, h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%m%d%y')AS fechaFormato ".
                        "FROM alHistoricoUnidadesTbl h ".
                        "WHERE h.centroDistribucion IN ('CMDAT') ".
                        " AND h.claveMovimiento IN ('IC') ".
                        " AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'R2V' AND rv.fechaMovimiento = H.fechaEvento) ".
                        " AND h.fechaEvento >=  CAST('".$startDate."' AS DATE) ";

        $rsGeneraDatos = fn_ejecuta_query($sqlGeneraDatos);        


    	$sqlTotalUnidades = "SELECT COUNT(H.VIN) AS numeroUnidad ".
                          "  FROM alHistoricoUnidadesTbl h ".
                          "  WHERE h.centroDistribucion IN ('CMDAT') ".
                          "    AND h.claveMovimiento IN ('IC') ".
                          "    AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'R2V'  AND rv.fechaMovimiento = H.fechaEvento) ".
                          " AND h.fechaEvento BETWEEN CAST('".$startDate."' AS DATE) AND CAST('".$today."' AS DATE) ";

        $rsTotalUnidades = fn_ejecuta_query($sqlTotalUnidades);

        $patioGroup = $rsTotalUnidades['root'][0]['numeroUnidad'];
    	
        if(sizeof($rsGeneraDatos['root']) != null){
    		//$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
        $sqlGetFolio = "SELECT LPAD(FOLIO + 1,5,0) AS numFolio, folio + 1 as actualFolio ".
                        "FROM trfoliostbl ".
                        "WHERE centroDistribucion = 'CMDAT' ".
                        "AND tipoDocumento = '2V'";

        $rsGetFolio = fn_ejecuta_query($sqlGetFolio);

        echo json_encode($rsGetFolio);

        $updFolio = "UPDATE trfoliostbl ".
                    "SET folio = '".$rsGetFolio['root'][0]['actualFolio']."' ".
                    "WHERE centroDistribucion = 'CMDAT' ".
                    "AND tipoDocumento = '2V';";

        $rsUpdFolio = fn_ejecuta_query($updFolio);

        $nombreArchivo = 'RA2V'.$rsGetFolio['root'][0]['numFolio'].'.txt';

            $fileDir = "E:/carbook/i340/RA2V".$rsGetFolio['root'][0]['numFolio'].".txt";
            $nomArch = "RA2V";
            $dirResp = "E:/carbook/i340/respArchivo/";
            $rampaFija = '807';
            $lw_ramp = 'ADIMSRAMPMX';
            $flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
            $recordsPositionValue = array();
            //for ($i = 0; $i <= $patioGroup; i++) {
                //foreach ($patioGroup as $conteo => $patioGroup) {
                //A) ENCABEZADO
                fwrite($flReporte660, 'ISA*03*RA2VE     *00*          *ZZ*ADIMSRAMPMX991 *ZZ*ADIMS          *'.date_format(date_create($today), 'ymd').'*'.date_format(date_create(date('H:i:s')), 'H').''.date_format(date_create(date('H:i:s')), 'i').'*U*00300*'.sprintf('%09d', sizeof($rsGeneraDatos['root'])).'*0*P*'.PHP_EOL);
                    //B) DETALLE UNIDADES
                    for ($i=0; $i < sizeof($rsGeneraDatos['root']); $i++) {
                        fwrite($flReporte660,'2VCM'.$rsGeneraDatos['root'][$i]['fechaFormato'].$rsGeneraDatos['root'][$i]['vin'].'CMDAT'.out('s', 27).'A'.out('s', 14).date_format(date_create($rsGeneraDatos['root'][$i]['fechaEvento']),'Hi').'NC     '.PHP_EOL);

                        $sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".                     
                                            "VALUES ('R2V', ".
                                            "'CMDAT', ".
                                            "'1', ".
                                            "'".$rsGeneraDatos['root'][$i]['vin']."', ".
                                            "NOW(), ".
                                            "'IC', ".
                                            "'".$rsGeneraDatos['root'][$i]['fechaEvento']."', ".
                                            "'1', ".
                                            "'".$nombreArchivo."', ".
                                            "'1');"; 

                        fn_ejecuta_query($sqlAddTransaccion);
                    }
                    fwrite($flReporte660, 'IEA*01*'.sprintf('%09d', sizeof($rsGeneraDatos['root'])).PHP_EOL);
                    //C) TRAILER
                    /*$record = array(0 => 'IEA*01*', 7 => sprintf('%09d', count($patioGroup)));
                    $recordsPositionValue[] = $record;
                    $trailerTxt = getTxt($record, array_keys($record));
                    fwrite($flReporte660, $trailerTxt);*/
                //}
            //}
            fclose($flReporte660);
            //echo json_encode($recordsPositionValue);            

    	  	/*$sqlInsTransaccion =	"INSERT INTO altransaccionunidadtbl  (tipoTransaccion, centroDistribucion, folio, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus,fecha,hora)".
    	        					"SELECT 'R2V','CMDAT' as centroDistribucion,null as folio,h.vin, now() as fechaMovimiento,h.claveMovimiento,h.fechaEvento,a660.prodstatus, '".$nombreArchivo."','A' ".
    	                            "FROM alHistoricoUnidadesTbl h, al660tbl a660 ".
    	                            "WHERE h.vin = a660.vin ".
                                    "AND a660.scaccode = 'COMO' ".
    	                            "AND h.centroDistribucion IN ('CMDAT') ".
    	                            "AND h.claveMovimiento IN ('IC') ".
    	                            "AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'R2V') ".
            						"AND h.fechaEvento >=  CAST('".$startDate."' AS DATE) ";

    		$rsAddTransaccion = fn_ejecuta_query($sqlInsTransaccion);*/
    	}else{
    		echo "No se genero archivo RA2V  ";
    	}
    	echo "termino RA2V Comodato: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
    	//subirFtp($nombreArchivo);
    }

    function subirFtp($nombreArchivo){
        $fileDir = "E:/carbook/i340/".$nombreArchivo;
        if(file_exists($fileDir)){
            # Definimos las variables
            $host = "ftp.iclfca.com";
            $port = 21;
            $user = "IG";
            $password = "dBJY76ig";
            $ruta = "/ig/SC/";
            $file = $fileDir;//tobe uploaded
            $remote_file = $nombreArchivo;
            $nuevo_fichero = "E:/carbook/i340/respArchivo/".$nombreArchivo;

            # Realizamos la conexion con el servidor
            $conn_id = @ftp_connect($host);//,$port);
            if($conn_id){
                    # Realizamos el login con nuestro usuario y contraseña
                if(@ftp_login($conn_id,$user,$password)){
                    # Canviamos al directorio especificado
                    if(@ftp_chdir($conn_id,$ruta)){
                        # Subimos el fichero
                        if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
                        echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
                        }else{
                            echo "No ha sido posible subir el fichero";
                        }
                    }else
                        echo "No existe el directorio especificado";
                }else
                    echo "El usuario o la contraseña son incorrectos";
                    # Cerramos la conexion ftp
                    ftp_close($conn_id);
            }else
                echo "No ha sido posible conectar con el servidor";
        }else{
            echo "no existe el archivo";
        }if(!copy($file, $nuevo_fichero)){
            echo "Error al copiar $fichero...\n";
        }else{
            unlink($file);
            echo "si se copio el archivo";
        }
    }
?>
