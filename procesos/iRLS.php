<?php
/*
	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");


	date_default_timezone_set('America/Mexico_City');
*/
	
	ejecutaRLS(); 
					
    function ejecutaRLS(){
		
		echo "Inicio iRLS: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

        $sqlRec = 	"SELECT dy.vin, dy.cveStatus ".
					"FROM  alinstruccionesmercedestbl dy ".
					"WHERE dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
					"AND dy.cveLoc='LZC02' ".
					"AND dy.cveStatus in ('TK','TY') ".
					"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='RLS') ";

		$rsRec= fn_ejecuta_query($sqlRec);


		for ($i=0; $i <sizeof($rsRec['root']) ; $i++) { 

			if (substr($rsRec['root'][$i]['cveStatus'], -2) == 'TK') {				
				$arrK[] = $rsRec['root'][$i];				
			}
		/////////////////////////////////////////////////////////
			else if (substr($rsRec['root'][$i]['cveStatus'], -2) == 'TY') {
				$arrH[] = $rsRec['root'][$i];
			}
			else{
				//echo "string";
			}
		}

		if (count($arrK) != 0) {
			$varEstatus = 'TK';
			$nomArchivo = 'K';
			$portCode = 'FT16';
			$portCodeFin = 'FT14';
			generaRLSKIA($varEstatus,$nomArchivo,$portCode,$portCodeFin);			
		}	
		if (count($arrH) != 0)  {
			$varEstatus = 'TY';
			$nomArchivo = 'H';
			$portCode = '1145';
			$portCodeFin = '1150';
			generaRLSHY($varEstatus,$nomArchivo,$portCode,$portCodeFin);		
		}		
		echo "FIN iRLS: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
	} 



	function generaRLSKIA($varEstatus,$nomArchivo,$portCode,$portCodeFin){		


		$sqlRLS  = "SELECT dy.vin, al.distribuidor, dy.cveStatus, dy.nomFac, dy.cveDisFac,dy.dirEnt,date_sub(dy.cveColor, interval 3 hour) as fechaEvento ". 
						"FROM alhistoricoUnidadesTbl al, alinstruccionesmercedestbl dy ".
						"WHERE al.vin = dy.vin ".
						"AND al.claveMovimiento='L2' ".
						"AND dy.cveColor is not null ".
						"AND dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
						"AND dy.cveLoc='LZC02' ".
						//"AND dy.vin in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='ACN') ".
						"AND dy.vin in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='REC')  ".
						"AND dy.cveStatus='".$varEstatus."' ".
						"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='RLS') ";

		$rsRLS= fn_ejecuta_query($sqlRLS);		
			
		for ($i=0; $i <sizeof($rsRLS['root']) ; $i++) { 

			if (substr($rsRLS['root'][$i]['nomFac'], -1) == 'M') {

				$arrMadrina[] = $rsRLS['root'][$i];				
			}
		/////////////////////////////////////////////////////////
		
			 if (substr($rsRLS['root'][$i]['nomFac'], -1) != 'M') {

				$arrTren[] = $rsRLS['root'][$i];
			}
			else{
				//echo "string";
			}
		}
		if (count($arrMadrina) != 0) {

			generaRLSMadrina($arrMadrina,$nomArchivo,$varEstatus,$portCode);
		}	
		if (count($arrTren) != 0) {

			generaRLSTren($arrTren,$nomArchivo,$varEstatus,$portCode,$portCodeFin);		
		}			
	} 


		function generaRLSHY($varEstatus,$nomArchivo,$portCode,$portCodeFin){		

			$sqlRLS  =  "SELECT dy.vin, al.distribuidor, dy.cveStatus, dy.nomFac, dy.cveDisFac,dy.dirEnt,date_sub(dy.cveColor, interval 3 hour) as fechaEvento ".  
						"FROM alhistoricoUnidadesTbl al, alinstruccionesmercedestbl dy ".
						"WHERE al.vin = dy.vin ".
						"AND al.claveMovimiento='L2' ".
						"AND dy.cveColor is not null ".
						"AND dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
						"AND dy.cveLoc='LZC02' ".
						"AND dy.vin in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='REC')  ".
						"AND dy.cveStatus='".$varEstatus."'  ".
						"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='RLS') ";

		$rsRLS= fn_ejecuta_query($sqlRLS);		
			
		for ($i=0; $i <sizeof($rsRLS['root']) ; $i++) { 

			if (substr($rsRLS['root'][$i]['nomFac'], -1) == 'M') {				
				$arrMadrina[] = $rsRLS['root'][$i];				
			}
		/////////////////////////////////////////////////////////
		
			 if (substr($rsRLS['root'][$i]['nomFac'], -1) != 'M') {
				$arrTren[] = $rsRLS['root'][$i];
			}
			else{
				//echo "string";
			}
		}
		if (count($arrMadrina) != 0) {
			
			generaRLSMadrina($arrMadrina,$nomArchivo,$varEstatus,$portCode);
		
		}	
		if (count($arrTren) != 0) {
			generaRLSTren($arrTren,$nomArchivo,$varEstatus,$portCode,$portCodeFin);		
		}			
	} 



	function generaRLSMadrina($arrMadrina, $nomArchivo,$varEstatus,$portCode){

		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');				
		$hora1= $hora+1;

		

		$directorio = "E:\\carbook\\archivosInterfacesGLOVIS\\respRLS\\";
		$inicioFile = $nomArchivo."MM_RLS_".$fecha.$hora1;
		$archivo = fopen($directorio.$inicioFile,"w");

		$nombreBusqueda = $nomArchivo."MM_RLS_".$fecha.$hora1;

		//encabezado
		fwrite($archivo,"RLSH"." "."APS"."  "."GMX"."  "."RLS".$fecha.$hora.PHP_EOL);

		//detalle

		for ($i=0; $i <sizeof($arrMadrina) ; $i++) {
				


			fwrite($archivo,"RLS  ".$arrMadrina[$i]['vin'].sprintf('%-4s',$arrMadrina[$i]['dirEnt']).$portCode." ".sprintf('%-5s',($arrMadrina[$i]['cveDisFac'])).substr($arrMadrina[$i]['fechaEvento'],0,4).substr($arrMadrina[$i]['fechaEvento'],5,2).substr($arrMadrina[$i]['fechaEvento'],8,2).substr($arrMadrina[$i]['fechaEvento'],11,2).substr($arrMadrina[$i]['fechaEvento'],14,2).substr($arrMadrina[$i]['fechaEvento'],17,2)."          0000000000000000".PHP_EOL);
			
			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('RLS', '".
										"LZC02"."', '".
										$arrMadrina[$i]['vin']."','".
										$arrMadrina[$i]['fechaEvento']."', '".
										"L2"."', '".
										$today."','".$varEstatus."','".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);



			
			//echo json_encode($insTransaccion);
			
		}
		//fin de archivo
		$long=(sizeof($arrMadrina)+2);
		fwrite($archivo,"RLST ".sprintf('%06d',($long)));
		fclose($archivo);	
		ftpArchivo_03($nombreBusqueda);

	}

	function generaRLSTren($arrTren, $nomArchivo,$varEstatus,$portCode,$portCodeFin){

		$fecha = date('Ymd');
		$hora = date("His");
	   	$hora1 = date('YmdHis',(strtotime("+2 second")));
	   	$today =  date('Y-m-d H:i:s');
	   	$nuevaHora= substr($hora1,8,6);	
			

		//echo $nombre;
		$inicioFile = $nomArchivo."MM_RLS_".$fecha.$nuevaHora;
		$directorio = "E:/carbook/archivosInterfacesGLOVIS/respRLS/";		
		$archivo = fopen($directorio.$inicioFile,"w");

		$nombreBusqueda = $nomArchivo."MM_RLS_".$fecha.$nuevaHora;

		//encabezado
		fwrite($archivo,"RLSH"." "."APS"."  "."GMX"."  "."RLS".$fecha.$hora.PHP_EOL);

		//detalle

		for ($i=0; $i <sizeof($arrTren) ; $i++) {
				

			fwrite($archivo,"RLS  ".$arrTren[$i]['vin'].""."KCSM".$portCode." ".$portCodeFin." ".substr($arrTren[$i]['fechaEvento'],0,4).substr($arrTren[$i]['fechaEvento'],5,2).substr($arrTren[$i]['fechaEvento'],8,2).substr($arrTren[$i]['fechaEvento'],11,2).substr($arrTren[$i]['fechaEvento'],14,2).substr($arrTren[$i]['fechaEvento'],17,2)."          0000000000000000".PHP_EOL);
			
			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('RLS', '".
										"LZC02"."', '".
										$arrTren[$i]['vin']."', '".
										$arrTren[$i]['fechaEvento']."', '".
										"L2"."', '".
										$today."','".$varEstatus."','".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
			
		}
		//fin de archivo
		$long=(sizeof($arrTren)+2);
		fwrite($archivo,"RLST ".sprintf('%06d',($long)));
		fclose($archivo);
		ftpArchivo_03($nombreBusqueda);

		//subirFtp($inicioFile,$inicioFile);	
	}

	function ftpArchivo_03($nombreBusqueda){
			if(file_exists("E:/carbook/archivosInterfacesGLOVIS/respRLS/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/archivosInterfacesGLOVIS/respRLS/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/archivosInterfacesGLOVIS/respRLS/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}

?>  