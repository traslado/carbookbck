<?php
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $startDate = date('Y-m-d', strtotime("-10 days")); //- 10 días
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $today = date('Y-m-d');

    //1° al 5° PASO
    $unidadesSin660 = getUnidadesSin660($startDate, $today);
    $unidadesCon660 = getUnidadesCon660($startDate, $today);

    //Se genera archivo de unidades que están en la 660
    createArchivoSin660($unidadesSin660);

    //Se genera archivo de unidades que están en la 660
    createArchivoCon660($unidadesCon660);
	
    //Inserta las transacciones en alTransaccionUnidadTbl
    insertTransacciones($unidadesCon660);
    //-----------------------------------------------------------------------

	function getUnidadesCon660($startDate, $endDate) {

		$sqlTransaccionDuplicada =  "(SELECT tu.vin FROM alTransaccionUnidadTbl tu ".
								"WHERE dv.vin = tu.vin ".
								"AND dv.centroDistribucion = tu.centroDistribucion ".
								"AND tu.tipoTransaccion = '928' ".
								"AND dv.numeroDanos = tu.claveMovimiento ".
								"AND dv.fechaEvento = tu.fechaMovimiento)";

		$sql_splc = "CASE dv.centroDistribucion WHEN 'CDTOL' THEN '958770703' ".
											   "WHEN 'CDSAL' THEN '922786703' ".
											   "WHEN 'CDAGS' THEN '940600000' ".
					"END AS splc";

	   
	   	//Subquery muuuy pesado...
	   	//$sqlEn660 = "(SELECT a6.vin FROM al660Tbl WHERE dv.vin = a6.vin AND a6.scacCode = 'XTRA' GROUP BY a6.vin)";
	    
	    $sqlGetDanosVics660 = "SELECT dv.centroDistribucion, dv.vin, dv.numeroDanos, a6.vupdate, a6.vuptime, a6.prodStatus, ".
	     				   "d.tipoDano, dv.idDano, d.areaDano, d.severidadDano, dv.fechaEvento, ".
	     				   //"$sqlEn660 AS en660, ".
	     				   "$sql_splc ".
	        			   "FROM alDanosVicsTbl dv, al660Tbl a6, caDanosTbl d ".
	        			   "WHERE DATE(dv.fechaEvento) BETWEEN '$startDate' AND '$endDate' ".
	        			   "AND dv.idDano = d.idDano ".
	        			   "AND dv.centroDistribucion IN ('CDTOL', 'CDSAL', 'CDAGS') ".
	        			   "AND dv.tipoDano IN ('OR', 'DR') ".
	        			   "AND $sqlTransaccionDuplicada IS NULL ".
	        			   "AND dv.vin = a6.vin ".
	        			   "AND a6.vupdate = (SELECT MAX(a2.vupdate) FROM al660tbl a2 ".
	        			   					 "WHERE a2.vin = a6.vin AND a2.vin = dv.vin ".
	        			   					 "AND a2.prodStatus <> 'SA' AND scacCode = 'XTRA') ".
						   "AND a6.vuptime = (select max(a6_3.vuptime) from al660tbl a6_3 ".
										  "where a6_3.vin  = dv.vin ".
										  "and a6_3.vupdate = (select max(a6_4.vupdate) from al660tbl a6_4 ".
										  					  "where a6_4.vin  = dv.vin ".
															  "and a6_4.scacCode IN ('MITS', 'XTRA') ".
															  "group by a6_4.vin) ".
										  "and a6_3.scacCode = 'XTRA' ".
										  "group by a6_3.vin) ".
	        			   "GROUP BY splc, dv.fechaEvento ".
	        			   "ORDER BY splc, dv.fechaEvento;";

	    $rsUnidades = fn_ejecuta_query($sqlGetDanosVics660);
	    //echo json_encode($rsUnidades);

	    return $rsUnidades['root'];
	}

	function getUnidadesSin660($startDate, $endDate) {

		$sqlExcluir660 = "(SELECT a6.vin FROM al660Tbl a6)";

		$sqlTransaccionDuplicada =  "(SELECT tu.vin FROM alTransaccionUnidadTbl tu ".
								"WHERE dv.vin = tu.vin ".
								"AND dv.centroDistribucion = tu.centroDistribucion ".
								"AND tu.tipoTransaccion = '928' ".
								"AND dv.numeroDanos = tu.claveMovimiento ".
								"AND dv.fechaEvento = tu.fechaMovimiento)";

	    $sqlGetDanosVics660 =  "SELECT dv.centroDistribucion, dv.vin ".
		        			   "FROMs alDanosVicsTbl dv, caDanosTbl d ".
		        			   "WHERE DATE(dv.fechaEvento) BETWEEN '$startDate' AND '$endDate' ".
		        			   "AND dv.idDano = d.idDano ".
		        			   "AND dv.centroDistribucion IN ('CDTOL', 'CDSAL', 'CDAGS') ".
		        			   "AND dv.tipoDano IN ('OR', 'DR') ".
		        			   "AND $sqlTransaccionDuplicada IS NULL ".
		        			   "AND dv.vin NOT IN $sqlExcluir660;";

		$rsUnidades = fn_ejecuta_query($sqlGetDanosVics660);
		//echo json_encode($sqlGetDanosVics660);


		return $rsUnidades['root'];
	}

	//Se Afecta la BD insertando las unidades en alTransaccionUnidadTbl
    function insertTransacciones($unidades) {
    	if (!isset($unidades) || count($unidades) == 0)
			return;
		
		$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl ".
								"(tipoTransaccion, centroDistribucion, vin, ".
								"fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, ".
								"prodStatus, fecha, hora) ".
								"VALUES";
		
		for ($i = 0; $i < count($unidades); $i++) {
			if ($i > 0)
				$sqlInsertTransaccion .= ", ";

			$unidad = $unidades[$i];
			//Fomatea la fecha y hora proveniente de la 660
			$vupdateParse = date_format(DateTime::createFromFormat('d/m/Y', $unidad['vupdate']), 'Y/m/d');
			$vuptimeParse = substr($unidad['vuptime'], 0, 5);

			$sqlInsertTransaccion .= "('928', ".
									 "'".$unidad['centroDistribucion']."', ".
									 "'".$unidad['vin']."', ".
									 "'".date('Y-m-d H:i:s')."', ".
									 "'".$unidad['numeroDanos']."', ".
									 "'".$unidad['fechaEvento']."', ".
									 "'".$unidad['prodStatus']."', ".
									 "'".$vupdateParse."', ".
									 "'".$vuptimeParse."')";			
		}
		fn_ejecuta_query($sqlInsertTransaccion);
		//echo json_encode($sqlInsertTransaccion);
    }
    
	function createArchivoCon660($unidades) {
		if (!isset($unidades) || count($unidades) < 1)

			return;
		
		$fileName = 'chr928v.txt';
		$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir="C:/carbook/i783/".$fileName;
	    $fileLog = fopen($fileDir, 'a+');


		$encabezado = array(1 => '928');

		for ($i = 0; $i < count($unidades); $i++) { 
			if ($i%99 == 0) {
				fwrite($fileLog, getTxt2($encabezado).out('n', 1));
			}
			$unidad = $unidades[$i];
			
			$fechaEvento = date_create($unidad['fechaEvento']);
			$registro1 = array( 1 =>  'HDRVDICS02XTRA',
						    15 => date_format($fechaEvento, 'y'),
						    17 => date_format($fechaEvento, 'm'),
						    19 => date_format($fechaEvento, 'd'),
						    21 => '2',
						    22 => out('s', 9),
						    31 => 'A20',
						    34 => $unidad['splc'],
						    43 => out('s', 8),
						    51 => 'XTRA',
						    55 => out('s', 14),
						    69 => date_format($fechaEvento, 'y'),
						    71 => date_format($fechaEvento, 'm'),
						    73 => date_format($fechaEvento, 'd'),
						    75 => out('s', 6)
							);

			fwrite($fileLog, getTxt2($registro1).out('n', 1));

			$registro2 = array( 1 =>  'DT1',
						     4 => $unidad['vin'],
						    21 => out('s', 31),
						    52 => '03Y',
						    55 => out('s', 23)
							);

			fwrite($fileLog, getTxt2($registro2).out('n', 1));
			
			$registro3 = array(1 =>  'DT2', 4 => $unidad['tipoDano'].$unidad['areaDano'].$unidad['severidadDano']);

			fwrite($fileLog, getTxt2($registro3).out('n', 1));	
		}
		fclose($fileDir);
	}

	function createArchivoSin660($unidades) {
		if (!isset($unidades) || count($unidades) < 1)
			return;
		
		$fileName = '928dv.fal660';
		$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir="C:/carbook/i783/".$fileName;
		$fileLog = fopen($fileDir, 'a+');
		foreach ($unidades as $unidad) {
			$registro = array(1 => $unidad['vin'], 22 => 'No existe trat660');
			fwrite($fileLog, getTxt2($registro).out('n', 1));
		}
		fclose($fileDir);
	}

	function getTxt2($texts){
        $positions = array_keys($texts);
        $text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = $positions[$i - 1] - 1;
                $antLength = strlen($texts[$positions[$i - 1]]);
            }
            $text .= out('s', ($positions[$i] - 1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }
?>