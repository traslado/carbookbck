<?php
    session_start();
	
	//include 'iVerificacionRepuve.php';
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");


	/*ejecutaPDI();
	
  		function ejecutaPDI(){	

        $sqlRec = 	"SELECT dy.vin, dy.cveStatus ".
					"FROM alultimodetalletbl al, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = dy.vin ".
					"AND al.centroDistribucion='LZC02' ".
					"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='ACN') ";

		$rsRec= fn_ejecuta_query($sqlRec);


		for ($i=0; $i <sizeof($rsRec['root']) ; $i++) { 

			if (substr($rsRec['root'][$i]['cveStatus'], -1) == 'K') {				
				$arrK[] = $rsRec['root'][$i];				
			}
		/////////////////////////////////////////////////////////
			else if (substr($rsRec['root'][$i]['cveStatus'], -1) == 'H') {
				$arrH[] = $rsRec['root'][$i];
			}
			else{
				//echo "string";
			}
		}
		if (count($arrK) != 0) {
			$varEstatus = 'DK';
			$nomArchivo = 'K';
			genInterface($varEstatus,$nomArchivo);
		}	
		if (count($arrH) != 0) {
			$varEstatus = 'DH';
			$nomArchivo = 'H';
			genInterface($varEstatus,$nomArchivo);	
		}			
	} */

	genInterface();

	function genInterface(){
			$errorMsg = '';
			//echo "Inicio RA2V Comodato: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
			$startDate = date('Y-m-d', strtotime("-5 days")); //- 5 días
		    $yesterday = date('Y-m-d', strtotime("-1 days"));
		    $today = date('Y-m-d');
		    $todayDateTime = date('Y-m-d H:i:s');

			/*$sqlGeneraDatos = "SELECT ap.vin, ap.workCode, hu.fechaEvento ".
								"from alpiotbl ap, alinstruccionesmercedestbl m,alHistoricoUnidadesTbl hu ".
								"where ap.vin not in (select vin from altransaccionunidadtbl where tipoTransaccion='ACN') ".
								"and ap.vin in (select vin from altransaccionunidadtbl where tipoTransaccion='REC') ".
								"and ap.estatus='PD' ".
								"and ap.vin=m.vin ".
								"and ap.vin=hu.vin ".
								"and claveMovimiento='PI' ".
								"and m.cveStatus='".$varEstatus."' ";*/

			$sqlGeneraDatos= "SELECT  distinct distinct hu.* FROM alhistoricounidadestbl hu,alunidadestbl al, casimbolosunidadestbl ca,alinstruccionesmercedestbl im ".
								"WHERE hu.centroDistribucion='LZC02' ".
								"AND hu.claveMovimiento in ('D3','D2') ".
								"AND hu.vin = al.vin ".
								"AND im.vin = al.vin ".
								"AND im.cveStatus='RK' ".
								"AND al.simboloUnidad = ca.simboloUnidad ".
								"AND ca.marca='KI' ".
								"AND hu.vin not in (SELECT tr.vin FROM altransaccionunidadtbl tr where tr.vin = hu.vin AND tr.tipoTransaccion in ('RU1','TU1') ) ";

	    	$rsGeneraDatos = fn_ejecuta_query($sqlGeneraDatos);	   

    	if(sizeof($rsGeneraDatos['root']) != null){
			//$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
			$fecha = date("YmdHis");
			$genArchivo = "KMM_ACN_"."$fecha";
			$nombreBusqueda = "KMM_ACN_"."$fecha";
	        $fileDir = "E:/carbook/interfacesExportacion/".$genArchivo;
	       	$flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
        	$today = date("Y-m-d H:i:s");
			//$fecha = substr($today,0,10);
			$hora=substr($today,11,8);




	      	        
	            //A) ENCABEZADO
	            fwrite($flReporte660,'ACNH APS  GMX  ACN'."$fecha".PHP_EOL);
	                

	                //B) DETALLE UNIDADES
                
                for ($i=0; $i < sizeof($rsGeneraDatos['root']); $i++) { 

                	if ($rsGeneraDatos['root'][$i]['claveMovimiento'] == 'D2') {
                		$codigo='RU01'; 
                		$transaccion='RU1';
                	}

                	if ($rsGeneraDatos['root'][$i]['claveMovimiento'] == 'D3') {
                		$codigo='TU01'; 
                		$transaccion ='TU1';
                	}


                    fwrite($flReporte660,'ACN  FT16 '.$rsGeneraDatos['root'][$i]['vin'].sprintf('%-6s',$codigo).substr($rsGeneraDatos['root'][$i]['fechaEvento'],0,4).substr($rsGeneraDatos['root'][$i]['fechaEvento'],5,2).substr($rsGeneraDatos['root'][$i]['fechaEvento'],8,2).substr($rsGeneraDatos['root'][$i]['fechaEvento'],11,2).substr($rsGeneraDatos['root'][$i]['fechaEvento'],14,2).substr($rsGeneraDatos['root'][$i]['fechaEvento'],17,2).'                         '.PHP_EOL);


                 	$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('".$transaccion."', '".
										"LZC02"."', '".
										$rsGeneraDatos['root'][$i]['vin']."', '".
										$today."', '".
										"RL"."', '".
										$today."','DK', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
					fn_ejecuta_query($insTransaccion);

					$sqlUpd ="UPDATE alinstruccionesmercedestbl set cveStatus='NK' WHERE vin ='".$rsGeneraDatos['root'][$i]['vin']."' ";
					fn_ejecuta_query($sqlUpd);
                }

                //C) TRAILER
                fwrite($flReporte660, 'ACNT '.sprintf('%06d', (sizeof($rsGeneraDatos['root'])+2)));
	                
	               
	        //}
	        fclose($flReporte660);
	        //echo json_encode($recordsPositionValue);
	        ftpArchivo($nombreBusqueda);

		 }
	}
	
		function ftpArchivo($nombreBusqueda){
			if(file_exists("E:/carbook/interfacesExportacion/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/interfacesExportacion/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/interfacesExportacion/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}
?>	