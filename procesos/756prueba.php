<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
//    $dirArchStr = "../reptra/";
    $dirArchStr="C:carbook/i756/";
    $nomArchStr = "PRESHI";
    $extStr = "TXT";
    //$dirlog = "../reptra/log758-".date("Y-m-d").".log";
    $dirlog="C:carbook/i756/";
    fn_lee_archivo($dirArchStr,$nomArchStr,$extStr,$dirlog);
    function fn_lee_archivo($dirArchStr,$nomArchStr,$extStr,$logDir){
    	$archStr = $dirArchStr.$nomArchStr.".".$extStr;
    	$newArchStr = $dirArchStr.$nomArchStr."_".date("Y-m-d_H:i:s").".".$extStr;
    	$fp = fopen($archStr, "r");
    	$archivoArr = array();
    	$cabceroStr = "";
    	$numRow = 1;
    	if($fp){
			while(!feof($fp)) {
				$linea = fgets($fp);
				if(strlen(trim($linea))==28){
					$cabceroStr = trim($linea);
					$cabceroStr = $cabceroStr."-".$numRow;
					$archivoArr[$cabceroStr] = [];
				}
				else{
					if(strlen(str_replace("\r\n","",$linea)) == 490){
						if(strlen(trim(str_replace("\r\n","",$linea)))>0){
							$linea = str_replace("\r\n","",$linea).
							$archivoArr[$cabceroStr][] = str_replace("\r\n","",$linea."-".$numRow);
						}
					}
					else{
						if(strlen(str_replace("\r\n","",$linea)) < 28){
							$text =  $numRow."|Registro: Invalido|Encabezado/Detalle no identificado\n";
							crea_log($logDir,$text);
						}
					}
				}
				$numRow ++;				
			}

			fclose($fp);
		}

		
		fn_valida_datos($archivoArr,$archStr,$newArchStr,$logDir);
    }
    function fn_valida_datos($array,$archStr,$newArchStr,$dirlog){
    	if($array){
    		$keysArr = array_keys($array);
    		for($i = 0;$i < sizeof($keysArr);$i++){
    			$valEcabArr = validaEncabezado($keysArr[$i],$dirlog);
    			if($valEcabArr['succcess'] == true){
    				$valEcabArr = $valEcabArr['encabezado'];
		    		foreach ($array[$keysArr[$i]] as $key => $row) {
						$valDetArr = validaDetalle	($row,$dirlog);
						if($valDetArr['succcess'] == true){
							$valDetArr = $valDetArr['detalle'];
							$sqlValShipStr = " SELECT * FROM alPreciosShippersTbl WHERE vin ='". $valDetArr['vin'] ."'";
			    			$valShipRst = fn_ejecuta_query($sqlValShipStr);
			    			if($valShipRst['records'] == 0){

			    				$sqlInstStr =	"INSERT INTO alPreciosShippersTbl (clavePlanta, nombrePlanta, fechaPrecion, tiempoPrecio,".
			    								"vin, avanzada, simboloUnidad, descripcionEspanol, descripcionIngles, unidadBasica, unidadBasicaS, ".
			    								"flete, fleteS, totVinPesos, totVinPesosS, von, distribuidor, claveOpciones, impOpciones, impOpcionesS,".
			    								"impAduana, impAduanaS, tSinFleteUs, tSinFleteUsS, folioFacturaNormal, fechaFactura, tipoCambio, impSeguro, ".
			    								"impSeguroS, impFleteUsa, impFleteUsaS, totalUnidad, totalUnidadS, facturasMux, impImport, impImportS, fechaOrigen, claveEstatus, fechaEstatus)".
			    								"VALUES (".
			    								"'". $valEcabArr['clave'] ."',".
			    								"'". $valEcabArr['planta'] ."',".
			    								"'". $valEcabArr['fecha'] ."',".
			    								"'". $valEcabArr['hora'] ."',".
			    								"'". $valDetArr['vin'] ."',".
			    								"'". $valDetArr['serie'] ."',".
			    								"'". $valDetArr['simbolo'] ."',".
			    								"'". $valDetArr['descEsp'] ."',".
			    								"'". $valDetArr['descIng'] ."',".
			    								"'". $valDetArr['unidadBasica'] ."',".
			    								"'". $valDetArr['unidadBasica_S'] ."',".
			    								"'". $valDetArr['flete'] ."',".
			    								"'". $valDetArr['flete_S'] ."',".
			    								"'". $valDetArr['totVinPesos'] ."',".
			    								"'". $valDetArr['totVinPesos_S'] ."',".
			    								"'". $valDetArr['von'] ."',".
			    								"'". $valDetArr['distribuidor'] ."',".
			    								"'". $valDetArr['cvesOpciones'] ."',".
												"'". $valDetArr['impOpciones'] ."',".
												"'". $valDetArr['impOpciones_S'] ."',".
												"'". $valDetArr['impAduana'] ."',".
												"'". $valDetArr['impAduana_S'] ."',".
												"'". $valDetArr['tsinFleteUs'] ."',".
												"'". $valDetArr['tsinFleteUs_S'] ."',".
												"'". $valDetArr['folFacNormal'] ."',".
												"'". $valDetArr['fechaFactura'] ."',".
												"'". $valDetArr['tipoCambio'] ."',".
												"'". $valDetArr['impSeguro'] ."',".
												"'". $valDetArr['impSeguro_S'] ."',".
												"'". $valDetArr['impFleteUsa'] ."',".
												"'". $valDetArr['impFleteUs_S'] ."',".
												"'". $valDetArr['totalUnidad'] ."',".
												"'". $valDetArr['totalUnidad_S'] ."',".
												"'". $valDetArr['facturasMux'] ."',".
												"'". $valDetArr['impImport'] ."',".
												"'". $valDetArr['impImport_S'] ."',".
												"'". date("Y-m-d") ."',".
												"'IN',".
												"'". date("Y-m-d") ."'".
			    								")";
								fn_ejecuta_query($sqlInstStr);
			    			}
			    		}
		    		}
		    	}
	    	}
	    	rename($archStr, $newArchStr);
    	}
    }
    function validaEncabezado($cabeceroStr,$dirlog){
    	$auxArr = explode("-", $cabeceroStr);
    	$cabeceroStr = $auxArr[0];
    	$numRegInt = $auxArr[1];
    	$resultArr = array('succcess'=>true,'encabezado'=> array(
    		'planta' => limpia_espacios(substr($cabeceroStr,0,11)),
    		'clave' => limpia_espacios(substr($cabeceroStr,11,3)),
    		"fecha" => limpia_espacios(substr($cabeceroStr,14,8)),
    		"hora" => limpia_espacios(substr($cabeceroStr,22,6))
    		));
    	foreach (array_keys($resultArr['encabezado']) as $key) {
    		if(stripos($resultArr['encabezado'][$key]," ") !== false){
				$resultArr['succcess'] = false;
				$text =  $numRegInt."|". $key ." Invalida:\n";
				crea_log($dirlog,$text);	
			}
			else{
				if($key == 'fecha' || $key == "hora"){
					$validaFechArr = validarFechaHora($resultArr['encabezado']['fecha'], $resultArr['encabezado']['hora']);
					if($validaFechArr['fechaValida'] == false){
						$resultArr['succcess'] = false;
						$text =  $numRegInt."|". $key ." Invalida:\n";
						crea_log($dirlog,$text);	
					}
					if($validaFechArr['horaValida'] == 0){
						$resultArr['succcess'] = false;
						$text =  $numRegInt."|". $key ." Invalida:\n";
						crea_log($dirlog,$text);	
					}
				}
			}
		}
    	return $resultArr;
    }

    function validaDetalle($detalleStr,$dirlog){
    	$auxArr = explode("-", $detalleStr);
    	$cabeceroStr = $auxArr[0];
    	$numRegInt = $auxArr[1];
    	$resultArr = array('succcess'=>true,"detalle"=>array());
    	$resultArr["serie"] = array(substr($detalleStr,0,8),"!=8");
    	$resultArr["vin"] = array(substr($detalleStr,9,17),"!=17");
    	$resultArr["simbolo"] = array(substr($detalleStr,27,6),"!=6");
    	$resultArr["descEsp"] = array(substr($detalleStr,34,30),"<=0");
    	$resultArr["descIng"] = array(substr($detalleStr,65,31),"NO");
    	$resultArr["unidadBasica"] = array(substr($detalleStr,96,10),"<=0");
    	$resultArr["unidadBasica_S"] = array(substr($detalleStr,106,1),"NO");
    	$resultArr["flete"] = array(substr($detalleStr,107,9),"<=0");
    	$resultArr["flete_S"] = array(substr($detalleStr,116,1),"NO");
    	$resultArr["totVinPesos"] = array(substr($detalleStr,117,12),"<=0");
    	$resultArr["totVinPesos_S"] = array(substr($detalleStr,129,1),"NO");
    	$resultArr["von"] = array(substr($detalleStr,131,8),"<=0");
    	$resultArr["distribuidor"] = array(substr($detalleStr,140,7),"<=0");
    	$resultArr["cvesOpciones"] = array(substr($detalleStr,147,160),"NO");
    	$resultArr["impOpciones"] = array(substr($detalleStr,307,10),"NO");
    	$resultArr["impOpciones_S"] = array(substr($detalleStr,317,1),"NO");
    	$resultArr["impAduana"] = array(substr($detalleStr,318,9),"NO");
    	$resultArr["impAduana_S"] = array(substr($detalleStr,327,1),"NO");
    	$resultArr["tsinFleteUs"] = array(substr($detalleStr,328,12),"NO");
    	$resultArr["tsinFleteUs_S"] = array(substr($detalleStr,340,1),"NO");
    	$resultArr["folFacNormal"] = array(substr($detalleStr,362,9),"NO");
    	$resultArr["fechaFactura"] = array(substr($detalleStr,393,10),"NO");
    	$resultArr["tipoCambio"] = array(substr($detalleStr,417,11),"NO");
    	$resultArr["impSeguro"] = array(substr($detalleStr,438,10),"NO");
    	$resultArr["impSeguro_S"] = array(substr($detalleStr,448,1),"NO");
    	$resultArr["impFleteUsa"] = array(substr($detalleStr,449,9),"NO");
    	$resultArr["impFleteUs_S"] = array(substr($detalleStr,458,1),"NO");
    	$resultArr["totalUnidad"] = array(substr($detalleStr,459,12),"NO");
    	$resultArr["totalUnidad_S"] = array(substr($detalleStr,471,1),"NO");
    	$resultArr["facturasMux"] = array(substr($detalleStr,472,3),"NO");
    	$resultArr["impImport"] = array(substr($detalleStr,475,12),"NO");
    	$resultArr["impImport_S"] = array(substr($detalleStr,488,1),"NO");
    	foreach (array_keys($resultArr) as $key) {
    		$value = "";
    		$ValidaStr = "";
    		if(stripos($key,"_") === false && $key != "succcess"){
    			if($resultArr[$key][1]!="NO"){
    				$value = limpia_espacios(trim($resultArr[$key][0]));
	    			$ValidaStr = (strlen($value).$resultArr[$key][1]);
	    			$ValidaStr = eval("return $ValidaStr;");
					if($ValidaStr){
	    				$resultArr["succcess"] = false;
						$text =  $numRegInt."|". $key ." Invalida:\n";
						crea_log($dirlog,$text);
	    			}
    			}
    			$resultArr['detalle'][$key] = $resultArr[$key][0];
    		}
    	}
    	return $resultArr ;
    }
    function crea_log($dir,$text){
    	$fileLog = fopen($dir, 'a');
    	if($fileLog){
		    fwrite($fileLog, $text);
		    fclose($fileLog);
		}
		
    }
    function limpia_espacios($cadena){
		$cadena = str_replace(' ', '', $cadena);
		return $cadena;
	}
?>