<?php
	require("../funciones/generales.php");
    require("../funciones/construct.php");
    require_once("../funciones/utilidadesProcesos.php");
    require_once("../funciones/utilidades.php");
    ##################################################################
    ###  NO SE GENERARA LOG YA QUE OBTIENE LOS REGISTROS CORRECTOS ###
    ##################################################################
    trap342();	
	function trap342(){
		echo "inicio trap342 ";
		$fechaAnt = strtotime ( '-5 day' , strtotime (date("Y-m-d")) ) ;
		$fechaAnt = date ( 'Y-m-d' , $fechaAnt );
		$sqlHstUndStr =	"SELECT c.centroDistribucion,c.localizacionUnidad,c.vin,c.fechaEvento,c.claveMovimiento,c.idTarifa,t.tarifa, c.distribuidor ,a6.scaccode, a6.orisplc,a6.routedes,'E' AS segtype,a6.vupdate,a6.vuptime ".
						"FROM alHistoricoUnidadesTbl c, caTarifasTbl  t , al660Tbl a6  ".
						"WHERE c.idTarifa = t.idTarifa ".
						"AND c.vin = a6.vin ".
						"AND a6.scaccode = 'XTRA' ".
						"AND CAST(c.fechaEvento AS DATE) >= CAST('".$fechaAnt."' AS DATE)  ".
						"AND c.centroDistribucion IN ('CDTOL','CDSAL','CDAGS','CDANG','CDCUA','CDSFE','CDLZC') ".
						"AND c.claveMovimiento = 'DA' ".
						"AND a6.vupdate = (SELECT max(a.vupdate) FROM al660Tbl a where a.vin = c.vin) ".
						"AND a6.vuptime = (SELECT max(a.vuptime) FROM al660Tbl a where a.vin = c.vin) ".
						"AND c.vin NOT IN (SELECT tr.vin FROM alTransaccionUnidadTbl tr ".
										"WHERE (tr.tipoTransaccion = 'H10' OR tr.tipoTransaccion = 'R50' ) ".
										 "AND (tr.claveMovimiento = c.claveMovimiento AND tr.fechaMovimiento = c.fechaEvento) ".
						")" .
						"GROUP BY c.vin ".
						"UNION ".
						"SELECT c.centroDistribucion,c.localizacionUnidad,c.vin,c.fechaEvento,c.claveMovimiento,c.idTarifa,t.tarifa, c.distribuidor ,a6.scaccode, a6.orisplc,a6.routedes,'T' AS segtype,a6.vupdate,a6.vuptime ".
						"FROM alHistoricoUnidadesTbl c, caTarifasTbl  t , al660Tbl a6 ".
						"WHERE c.idTarifa = t.idTarifa ".
						"AND c.vin = a6.vin ".
						"AND a6.scaccode = 'XTRA' ".
						"AND  c.claveMovimiento = 'RU' ".
						"AND CAST(c.fechaEvento AS DATE) >= CAST('".$fechaAnt."' AS DATE) ".
						"AND c.centroDistribucion IN ('CDTOL','CDSAL','CDAGS','CDANG','CDCUA','CDSFE','CDLZC') ".
						"AND a6.vupdate = (SELECT max(a.vupdate) FROM al660Tbl a where a.vin = c.vin) ".
						"AND a6.vuptime = (SELECT max(a.vuptime) FROM al660Tbl a where a.vin = c.vin) ".
						"AND c.vin IN (SELECT tr.vin FROM alTransaccionUnidadTbl tr ".
									"WHERE tr.tipoTransaccion = 'R50' ".
									 "AND (tr.claveMovimiento = 'DA' AND tr.fechaMovimiento <= c.fechaEvento) ".
						") ".
						"AND c.vin IN (SELECT tr.vin FROM alHistoricoUnidadesTbl tr ".
									"WHERE tr.claveMovimiento = 'TA' ".
						") ".
						"GROUP BY c.vin";
		$hstUndRst = fn_ejecuta_query($sqlHstUndStr);
		if($hstUndRst['records'] > 0){
			foreach ($hstUndRst['root'] as $i => $row) {
				$hstUndRst['root'][$i]["ori_splc"] = fn_ori_splc($row['centroDistribucion']);
				if($row['scaccode'] = 'XTRA' ){
					$hstUndRst['root'][$i]["scaccode"] = "DCC";
				}
			}
		}
		
		#agrupamos arreglo
		$agupaCamposArr = array('scaccode','orisplc','claveMovimiento','fechaEvento'); 
		$array2 = getGroupedArray($hstUndRst['root'], $agupaCamposArr);
		#asiganamos arreglo a principal
		fn_crear_archivo($array2,$hstUndRst['records']);
		
		//echo json_encode($array2);
	}

	function fn_array_msort($array, $cols)
	{
		$colarr = array();
	    foreach ($cols as $col => $order) {
	        $colarr[$col] = array();
	        foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
	    }
	    $eval = 'array_multisort(';
	    foreach ($cols as $col => $order) {
	        $eval .= '$colarr[\''.$col.'\'],'.$order.',';
	    }
	    $eval = substr($eval,0,-1).');';
	    eval($eval);
	    $ret = array();
	    foreach ($colarr as $col => $arr) {
	        foreach ($arr as $k => $v) {
	            $k = substr($k,1);
	             if (!isset($ret[$k])) $ret[$k] = $array[$k];
            	$ret[$k][$col] = $array[$k][$col];
	        }
	    }
	    $retA = array();
	    foreach ($ret as $key => $value) {
	    	array_push($retA, $value);
	    }
	    return $retA;

	}

	
	function fn_crear_archivo($array,$totalRegistros){
		//se Crea Archivo 
		// $fileDir="E:/carbook/i342/RADC550.txt";
		$fileDir="C:/carbook/i342/RADC550.txt";
	    //$fileDir = '../generacion_550/RADC550.txt';
	    $fileLog = fopen($fileDir, 'a+');
		$keys = array_keys($array);
		$segmento = 1;
		$countUnds =1;
		for ($i=0;$i < sizeof($keys);$i++) {
			$countSegSc= 1; 
			$encabezado1 = array(1 => 'ISA*03*RA550     *00*          *ZZ*', 
			 				   36 => 'XTRA           *ZZ*ADMIS',
			 				   60 => $keys[$i],
			 				   64 => '      ',
			 				   70 => '*',
			 				   71 => date("ymd*Hi"),
			 				   82 => '*U*00200*',
			 				   91 => '000000001',
			 				   100 => '*0*P>');

		    $encabezado2 = array(1 => 'GS*VI*XTRA*VISTA*', 
			 				   18 => date("ymd*Hi"),
			 				   29 => '*00001*T*1');

		    
		    //Por cada unidad...
		    	
		    	$text = getTxt($encabezado1, array_keys($encabezado1)); //out es un salto de linea
			    fwrite($fileLog, $text);
			    $text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
		    	$keysSplc = array_keys($array[$keys[$i]]);
			    for ($j=0;$j < sizeof($keysSplc);$j++) {
			    	$keysStatus = array_keys($array[$keys[$i]][$keysSplc[$j]]);
			    	for ($k=0;$k < sizeof($keysStatus);$k++) {
			    		$keysFecha = array_keys($array[$keys[$i]][$keysSplc[$j]][$keysStatus[$k]]);
			    		for ($l=0;$l < sizeof($keysFecha);$l++) {
			    			$rows = $array[$keys[$i]][$keysSplc[$j]][$keysStatus[$k]][$keysFecha[$l]];
			    			
			    			$encabezado3 = array(1 => 'ST*550*00001', 
				 				   13 => str_pad($segmento,4,"0",STR_PAD_LEFT));
			    			$text = getTxt($encabezado3, array_keys($encabezado3)).out('n', 1); //out es un salto de linea
			    			fwrite($fileLog, $text);

			    			$encabezado4 = array(1 => 'BV5*', 
								 				   5 => 'TA',
								 				   7 => '*XTRA*',
								 				   13 => $keysSplc[$j],
								 				   22 => '*',
								 				   23 => str_pad($totalRegistros,3,"0",STR_PAD_LEFT),
								 				   26 => '*',
								 				   27 => $rows[0]['segtype'],
								 				   28 => '*',
								 				   39=> date("ymd***Hi",strtotime($keysFecha[$l])));
			    			$text = getTxt($encabezado4, array_keys($encabezado4)).out('n', 1); //out es un salto de linea
			    			
			    			fwrite($fileLog, $text);
			    			foreach ($rows as $index => $row) {
				    			$detalle1 = array(1 => 'VI*', 
									 				   4 => $row['vin'],
									 				   21 => '*',
									 				   22 => substr($row['vin'], 10,11),
									 				   23 => '*',
									 				   24 => substr($row['routedes'], 0,1),
									 				   26 => '*****');
				    			$text = getTxt($detalle1, array_keys($detalle1)).out('n', 1); //out es un salto de linea
				    			fwrite($fileLog, $text);
    			    			fn_afectacion_db($row);
			    			}

			    			$detalle2 = array(1 => 'SE*', 
							 				   4 => str_pad(count($rows),6,"0",STR_PAD_LEFT),
							 				   10 => '*00001',
							 				   16 => str_pad($segmento,4,"0",STR_PAD_LEFT));
			    			$text = getTxt($detalle2, array_keys($detalle2)).out('n', 1); //out es un salto de linea
			    			fwrite($fileLog, $text);
			    			$segmento ++;
			    			
			    		}
			    		
			    	}

			    }
		 	
 			fclose($fileLog);
		}
		//$fileDir="E:/carbook/i342/RADT660.txt";
		$fileDir="E:/carbook/i342/RADC550.txt";
		//$fileDir = '../generacion_550/RADT660.txt';
	    $fileLog = fopen($fileDir, 'a+');
	    $log1 = array(1 => 'TOTAL DE UNDS EN R50 ', 
 				   22 => date('d/m/Y-H:m:s') .":",
 				   23=> "\n".$totalRegistros);
		$text = getTxt($log1, array_keys($log1)).out('n', 1); //out es un salto de linea
		fwrite($fileLog, $text);
		fclose($fileLog);
	}

	function fn_afectacion_db($row){
		if($row){

			$whereStr = "WHERE centroDistribucion = '". $row['centroDistribucion'] ."' ". 
						"AND VIN = '". $row['vin'] ."' ". 
						"AND claveMovimiento = '". $row['claveMovimiento'] ."' ";
						$vupdate=str_replace('/', '-', $row['vupdate']);
			$insv1Str = "INSERT INTO alTransaccionUnidadTbl VALUES("."null,".
						"'R50',".
						"'". $row['centroDistribucion'] ."',' ',".
						"'". $row['vin'] ."',".
						"'". $row['fechaEvento'] ."',".
						"'". $row['claveMovimiento'] ."',".
						"'". date("Y-m-d H:i:s") ."',".
						"'". $row['estatus'] ."',".
						"'". $vupdate."',".
						"'". $row['vuptime'] ."')";
			fn_ejecuta_query($insv1Str);
			$claveMovimientoStr = ($row['claveMovimiento'] == "DA")?"TA":"LA";
			$insertHUStr = "INSERT INTO alHistoricoUnidadesTbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
							"SELECT centroDistribucion,vin,DATE_ADD(fechaEvento, INTERVAL +1 SECOND),'". $claveMovimientoStr ."',distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip ".
							"FROM alhistoricounidadestbl ". $whereStr ." ".
							"AND fechaEvento = (SELECT max(fechaEvento) ".
												"FROM alhistoricounidadestbl ".
												$whereStr .
							                    ")";
			$rs=fn_ejecuta_query($insertHUStr);



		}
	}	
?>