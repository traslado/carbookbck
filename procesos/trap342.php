<?php
	require("../funciones/generales.php");
    require("../funciones/construct.php");
    require_once("../funciones/utilidadesProcesos.php");
    require_once("../funciones/utilidades.php");
    
    trap342();	
	function trap342(){
		$fechaAnt = strtotime ( '-1 day' , strtotime (date("Y-m-d")) ) ;
		$fechaAnt = date ( 'Y-m-d' , $fechaAnt );
		//DT
		$sqlHstUndStr =	"SELECT c.centroDistribucion,c.localizacionUnidad,c.vin,c.fechaEvento,c.claveMovimiento,c.idTarifa,t.tarifa, c.distribuidor,(SELECT (SELECT DISTINCT 1 FROM al660Tbl a6 WHERE c.vin = a6.vin) IS NOT NULL) AS bandera ".
						"FROM alHistoricoUnidadesTbl c, caTarifasTbl  t ".
						"WHERE c.idTarifa = t.idTarifa ".
						"AND c.fechaEvento BETWEEN '". $fechaAnt ."' AND  '". date ( 'Y-m-d')."' ".
						"AND c.centroDistribucion IN ('CDTOL','CDSAL','CDAgs','CDANG','CDCUA','CDSFE','CDLZC') ".
						"AND c.claveMovimiento = 'DT'".
						"AND c.vin <> (select vin ".
							"FROM alTransaccionUnidadTbl ".
							"WHERE (tipoTransaccion = 'H10' OR centroDistribucion = 'CDAGS' ) ".
							"OR (tipoTransaccion = 'R50' AND claveMovimiento = c.claveMovimiento) ".
		                ")";
		$hstUndRst = fn_ejecuta_query($sqlHstUndStr);
		$hstUndRst['sin660'] = array();
		$hstUndRst['con660'] = array();
		if($hstUndRst['records'] > 0){
			foreach ($hstUndRst['root'] as $i => $row) {
				if(isset($row['bandera'])){
					unset($row['bandera']);
				}
				## por el momento este no se ocupara pero se deja preparado en algun momento se ocupe 
				$row['aux'] = "  ";
				##
				$row["ori_splc"] = fn_ori_splc($row['centroDistribucion']);
				if($hstUndRst['root'][$i]['bandera'] == 1){
					$arregloN = fn_scac_code($row['vin']);
					$row['scaccode'] = $arregloN['scaccode'];
					$row['routedes'] = $arregloN['routedes'];
					array_push($hstUndRst['con660'], $row);
				}
				else{
					array_push($hstUndRst['sin660'], $row);
				}
			}
		}
		//CT
		$sqlHstUndCTStr =	"SELECT c.centroDistribucion,c.localizacionUnidad,c.vin,c.fechaEvento,c.claveMovimiento,c.idTarifa,t.tarifa, c.distribuidor,(SELECT (SELECT DISTINCT 1 FROM al660Tbl a6 WHERE c.vin = a6.vin) IS NOT NULL) AS bandera ".
							"FROM alHistoricoUnidadesTbl c, caTarifasTbl  t ".
							"WHERE c.idTarifa = t.idTarifa ".
							"AND c.fechaEvento BETWEEN '". $fechaAnt ."' AND  '". date ( 'Y-m-d')."' ".
							"AND c.centroDistribucion IN ('CDTOL','CDSAL','CDAgs','CDANG','CDCUA','CDSFE','CDLZC') ".
							"AND c.claveMovimiento = 'CT'".
							"AND t.tarifa IN('01','06','05','08') ".
							"AND NOT EXISTS(SELECT vin ".
								"FROM alTransaccionUnidadTbl ".
								"WHERE (tipoTransaccion = 'H10') ".
								"OR (tipoTransaccion = 'R50' AND claveMovimiento = c.claveMovimiento) ".
								"AND vin = c.vin". 
			                ")".
							"and EXISTS (SELECT vin ".
										"FROM alTransaccionUnidadTbl ".
										"WHERE (tipoTransaccion = 'R50' ".
										"AND claveMovimiento = 'DT'  ".
										"AND vin = c.vin ".
										"AND fechaMovimiento <= c.fechaEvento ))";
		$hstUndCTRst = fn_ejecuta_query($sqlHstUndCTStr);
		if($hstUndCTRst['records'] > 0){
			foreach ($hstUndCTRst['root'] as $i => $row) {
				if(isset($row['bandera'])){
					unset($row['bandera']);
				}
				## por el momento este no se ocupara pero se deja preparado en algun momento se ocupe 
				$row['aux'] = "DT";
				##
				$row["ori_splc"] = fn_ori_splc($row['centroDistribucion']);
				if($hstUndCTRst['root'][$i]['bandera'] == 1){
					$arregloN = fn_scac_code($row['vin']);
					$row['scaccode'] = $arregloN['scaccode'];
					$row['routedes'] = $arregloN['routedes'];
					array_push($hstUndRst['con660'], $row);
				}
				else{
					array_push($hstUndRst['sin660'], $row);
				}
			}
		}
		#INICIA Generamos log de inexistentes
		if($hstUndRst['sin660']){
		    
		    $dirPath = "../generacion_550/logInexistentes/";
		    $fileName= "logInexistentes_".date("YmdHi").".log";
		    $filePath = $dirPath.$fileName;        

		   /* $logInexistentes=fopen($filePath,"a") or die("Problemas Forest");
		    //fputs($logInexistentes,"aqui inserte una linea poderosa");
		    foreach ($hstUndRst['sin660'] as $idx => $row) {
		    	fputs($logInexistentes,$row['localizacionUnidad'].",".$row['vin'].",". $row['aux'] ."'R50',".$row['fechaEvento']."\n" );
		    }
		    fclose($logInexistentes);
		    */
		}
		#TERMINA LOG INEXISTENTES
		#ordenamos arreglo
		$array2 = fn_array_msort($hstUndRst['con660'],array('scaccode'=>SORT_ASC, 'ori_splc'=>SORT_ASC,'claveMovimiento'=>SORT_ASC,'fechaEvento'=>SORT_ASC));
		#agrupamos arreglo
		$array2 = fn_agrupar_arreglo($array2);
		#asiganamos arreglo a principal
		$hstUndRst['con660'] = $array2;
		fn_crear_archivo($hstUndRst['con660']);
		//echo json_encode($hstUndRst['con660']);
	}

	function fn_ori_splc($centroDistribucion){
		$ori_splc = array('CDSAL' => '922786801',
						  'CDTOL' => '958770807',
						  'CDANG' => '958770808',
						  'CDAGS' => '940606000',
						  'CDMAZ' => '958770809',
						  'CDSFE' => '978442999',
						  'CDMAT' => '978442999',
						  'CDCUA' => '957692999',
						  'CDLZC' => '999990907');
		return  $ori_splc[$centroDistribucion];
	}

	function fn_scac_code($vin){
		$array = array();
		$whereStr  = 	"WHERE vin = '". $vin ."' ".
						"AND scaccode IN ('DCC','MITS') ";
		$sqlScacStr = 	"SELECT scaccode ,routedes FROM al660tbl ". $whereStr.
						"AND (vupdate = (select MAX(vupdate) ".
							"FROM al660tbl ".
							$whereStr .
			                ")".
							"AND vuptime = (select MAX(vuptime) ".
							"FROM al660tbl ".
							$whereStr .
			                ")".
						")";
		$scaccodeRst = fn_ejecuta_query($sqlScacStr);
		$array['scaccode'] = $scaccodeRst['root'][0]['scaccode'];
		$array['routedes'] = $scaccodeRst['root'][0]['routedes'];
		return  $array;
	}

	function fn_array_msort($array, $cols)
	{
		$colarr = array();
	    foreach ($cols as $col => $order) {
	        $colarr[$col] = array();
	        foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
	    }
	    $eval = 'array_multisort(';
	    foreach ($cols as $col => $order) {
	        $eval .= '$colarr[\''.$col.'\'],'.$order.',';
	    }
	    $eval = substr($eval,0,-1).');';
	    eval($eval);
	    $ret = array();
	    foreach ($colarr as $col => $arr) {
	        foreach ($arr as $k => $v) {
	            $k = substr($k,1);
	             if (!isset($ret[$k])) $ret[$k] = $array[$k];
            	$ret[$k][$col] = $array[$k][$col];
	        }
	    }
	    $retA = array();
	    foreach ($ret as $key => $value) {
	    	array_push($retA, $value);
	    }
	    return $retA;

	}

	function fn_agrupar_arreglo($arreglo){
		$arrayCode = array();
		foreach ($arreglo as $key => $row) {
			
			$info = array("centroDistribucion" =>$row['centroDistribucion'],
							"vin" =>$row['vin'],
							"idTarifa" =>$row['idTarifa'],
							"tarifa" =>$row['tarifa'],
							"distribuidor" =>$row['distribuidor'],
							'routedes'=>$row['routedes']);
			if(!array_key_exists($row['scaccode'],$arrayCode)){
				$arrayCode[$row['scaccode']] = array();
				$arrayCode[$row['scaccode']][$row['ori_splc']] = array();
				$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']] = array();
				$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']][$row['fechaEvento']] = array();
				$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']][$row['fechaEvento']][] = $info;
			}
			else{
				if(!array_key_exists($row['ori_splc'], $arrayCode[$row['scaccode']])){
						$arrayCode[$row['scaccode']][$row['ori_splc']] = array();
						$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']] = array();
						$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']][$row['fechaEvento']] = array();				
						$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']][$row['fechaEvento']][] = $info;

				}
				else{
					if(!array_key_exists($row['claveMovimiento'], $arrayCode[$row['scaccode']][$row['ori_splc']])){
						$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']] = array();
						$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']][$row['fechaEvento']] = array();
						$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']][$row['fechaEvento']][] = $info;				
					}
					else{
						if(!array_key_exists($row['fechaEvento'], $arrayCode[$row['scaccode']][$row['ori_splc']]['claveMovimiento'])){
							$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']] = array();
							
							$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']][$row['fechaEvento']][] = $info;
						}
						else{
							$arrayCode[$row['scaccode']][$row['ori_splc']][$row['claveMovimiento']][$row['fechaEvento']][] = $info;
						}	
					}	
				}

			}

		}

		return $arrayCode;
	}
	function fn_crear_archivo($array){
		//echo sizeof($array);
		$keys = array_keys($array);
		for ($i=0;$i < sizeof($keys);$i++) {
			$encabezado1 = array(1 => 'ISA*03*RA550     *00*          *ZZ*', 
			 				   36 => 'XTRA           *ZZ*ADMIS',
			 				   60 => $keys[$i],
			 				   64 => '      ',
			 				   70 => '*',
			 				   71 => date("ymd*Hi"),
			 				   82 => '*U*00200*',
			 				   91 => '000000001',
			 				   100 => '*0*P>');

		    $text = '';

		    $encabezado2 = array(1 => 'GS*VI*XTRA*VISTA*', 
			 				   18 => date("ymd*Hi"),
			 				   29 => '*00001*T*1');

		    //se Crea Archivo 
		    $fileDir = '../generacion_550/'.$keys[$i] .'.txt';
		    $fileLog = fopen($fileDir, 'a+');
		    //Por cada unidad...
		    	
		    	$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			    $text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
		    	$contSeg = 1;
			    $keysSplc = array_keys($array[$keys[$i]]);
			    for ($j=0;$j < sizeof($keysSplc);$j++) {
			    	$keysStatus = array_keys($array[$keys[$i]][$keysSplc[$j]]);
			    	for ($k=0;$k < sizeof($keysStatus);$k++) {
			    		$keysFecha = array_keys($array[$keys[$i]][$keysSplc[$j]][$keysStatus[$k]]);
			    		for ($l=0;$l < sizeof($keysFecha);$l++) {
			    			$rows = $array[$keys[$i]][$keysSplc[$j]][$keysStatus[$k]][$keysFecha[$l]];
			    			
			    			$encabezado3 = array(1 => 'ST*550*00001', 
				 				   13 => str_pad($contSeg,4,"0",STR_PAD_LEFT));
			    			$text = getTxt($encabezado3, array_keys($encabezado3)).out('n', 1); //out es un salto de linea
			    			fwrite($fileLog, $text);

			    			$movto = $keysStatus[$k];
			    			$segType="";
			    			if($movto == "CT"){
			    				$segType="T";
			    			}
			    			else{
			    				$segType="E";
			    			}
			    			$encabezado4 = array(1 => 'BV5*', 
								 				   5 => $segType,
								 				   6 => '*XTRA*',
								 				   12 => $keysSplc[$j],
								 				   21 => '*',
								 				   22 => str_pad(sizeof($rows),3,"0",STR_PAD_LEFT),
								 				   25 => '*',
								 				   26 => 'HH',
								 				   28 => '*',
								 				   29=> date("ymd***Hi",strtotime($keysFecha[$l])));
			    			$text = getTxt($encabezado4, array_keys($encabezado4)).out('n', 1); //out es un salto de linea
			    			
			    			fwrite($fileLog, $text);
			    			foreach ($rows as $index => $row) {
				    			$detalle = array(1 => 'VI*', 
									 				   4 => $row['vin'],
									 				   21 => '*',
									 				   22 => substr($row['vin'], 10,11),
									 				   23 => '*',
									 				   24 => substr($row['routedes'], 0,1),
									 				   26 => '*****');
				    			$text = getTxt($detalle, array_keys($detalle)).out('n', 1); //out es un salto de linea
				    			fwrite($fileLog, $text);
			    			}
			    			$contSeg ++;
			    			
			    		}
			    		
			    	}

			    }
		 	
 			fclose($fileLog);
		}
	}	
?>