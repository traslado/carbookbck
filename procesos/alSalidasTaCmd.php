<?php 
	require("../funciones/generales.php");
    require("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    

	$fechaAnt = strtotime ( '-5 day' , strtotime (date("Y-m-d")) ) ;
	$fechaAnt = date ( 'Y-m-d' , $fechaAnt );
	$vin1= $_REQUEST['vines'];
	
	$vin1 = $_REQUEST['vines'];
	$cveMovimiento = $_REQUEST['cveMovimiento'];

	$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
	$reemplazar = array("", "", "", "");
	$vin = str_ireplace($buscar,$reemplazar,$vin1);
	$cadena = chunk_split($vin, 17,",");
	$arregloCadena= explode(",",$cadena);	

	echo json_encode($arregloCadena);


	for ($i=0; $i < sizeof($arregloCadena); $i++) {		

		$sqlHoldUnd = "SELECT count(hu.claveMovimiento) as undHold
						FROM alhistoricounidadestbl hu
						WHERE hu.vin = '".$arregloCadena[$i]."'
						AND (claveMovimiento = (SELECT g2.valor FROM cageneralestbl g2 WHERE g2.tabla = 'alHoldsUnidadesTbl' AND g2.columna = 'CDTOL' AND g2.valor = hu.claveMovimiento ))
						GROUP BY VIN;";

		$rsHold = fn_ejecuta_query($sqlHoldUnd);                        

		$sqlLiberado = "SELECT count(hu.claveMovimiento) as unLiberado
						FROM alhistoricounidadestbl hu
						WHERE hu.vin = '".$arregloCadena[$i]."'
						AND (claveMovimiento = (SELECT g4.estatus FROM cageneralestbl g4 WHERE g4.tabla = 'alHoldsUnidadesTbl' AND g4.columna = 'CDTOL' AND g4.estatus = hu.claveMovimiento))
						GROUP BY VIN;";

		$rsLiberado = fn_ejecuta_query($sqlLiberado);


		if($rsHold['root'][0]['undHold'] == $rsLiberado['root'][0]['unLiberado']){
			
			$sqlValidaUnd = "SELECT 1 as bandera FROM alultimodetalletbl
							WHERE vin = '".$arregloCadena[$i]."'
							AND claveMovimiento IN('IC','RU');";

			$rsValidaUnd = fn_ejecuta_query($sqlValidaUnd);

			if($rsValidaUnd['root'][0]['bandera'] == '1'){
				echo "si se puede insertrar ";

				$sqlUpdLugares = "UPDATE allocalizacionpatiostbl ".
						"SET VIN = null, ".
						"estatus = 'DI' ".
						"WHERE patio = 'CMDAT' ".						
						"AND vin = '".$arregloCadena[$i]."'";

				fn_ejecuta_query($sqlUpdLugares);

				$sqlDltPmp = "DELETE FROM alactividadespmptbl ".
								 "WHERE claveEstatus ='PE' ".
								 "AND vin = '".$arregloCadena[$i]."'";

				fn_ejecuta_query($sqlDltPmp);

				$sqlAddUnds = "INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
								"SELECT centroDistribucion, vin, NOW(), '".$cveMovimiento."' as claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ".
								"FROM alUltimoDetalleTbl ".
								"WHERE (claveMovimiento = 'IC' OR claveMovimiento = 'RU') ".
								"AND centroDistribucion = 'CMDAT' ".
								"AND vin = '".$arregloCadena[$i]."'";

				fn_ejecuta_query($sqlAddUnds);

				$sqlUpdUnds = "UPDATE alUltimoDetalleTbl ".
						"SET claveMovimiento = '".$cveMovimiento."', ".
						"fechaEvento = now() ".
						"WHERE centroDistribucion = 'CMDAT' ".					
						"AND vin = '".$arregloCadena[$i]."'";					

				fn_ejecuta_query($sqlUpdUnds);
			}else{
				echo "no se puede dar salida a la unidad";
			}												
		}else{
			echo "no se inserta la salida de la unidad";
		}		
	}					
?>						