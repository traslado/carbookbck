<?php
	require("../funciones/generales.php");
	require("../funciones/utilidades.php");

	date_default_timezone_set('America/Mexico_City');

    echo "Generacion de Archivo 510: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

    $sqlFechaConsulta = "SELECT date_add(CURDATE(), INTERVAL -20 DAY) as FechaConsulta";
    
    $rs = fn_ejecuta_query($sqlFechaConsulta);
    $fechaConsulta = $rs['root'][0]['FechaConsulta'];
    						

    $sqlUnidades =  "INSERT INTO alHistoricoUnidadesTmp (idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
    				"SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
                    " distribuidor, idTarifa, localizacionUnidad, claveChofer, 'PROCESO 510' as observaciones, usuario, ip ".
					"FROM alhistoricounidadestbl ".
					"WHERE centroDistribucion IN ('CDSAL','CDAGS','CDTOL','CDANG','CDSFE','CDVER')  ".
					"AND claveMovimiento = 'EP' ".
    				"AND distribuidor IN ( SELECT nombre FROM caGeneralesTbl WHERE tabla = 'funcion510' AND columna = 'DISTRIBUIDORES' ) ".
    				"AND DATE(fechaEvento) BETWEEN '".$fechaConsulta."' AND curdate() ".
    				"AND vin NOT IN (SELECT vin ".
					"				  FROM alTransaccionUnidadTbl ".
                    "				  WHERE tipoTransaccion = '510' ) ".
                    "AND vin NOT IN (SELECT vin FROM alHistoricoUnidadesTmp WHERE alhistoricounidadestmp.observaciones = 'PROCESO 510')".
                    "AND VIN NOT IN (SELECT vin FROM alinstruccionesmercedestbl)";
    
    $rs = fn_ejecuta_query($sqlUnidades);
    //echo json_encode($rs);

    $sqlUnidades01 ="INSERT INTO alHistoricoUnidadesTmp (idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
    				" distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
    				"SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
                    " distribuidor, idTarifa, localizacionUnidad, claveChofer, 'PROCESO 510' as observaciones, usuario, ip ".
					"FROM alhistoricounidadestbl ".
					"WHERE centroDistribucion IN ('CDTOL','CDANG')  ".
					"AND claveMovimiento IN (SELECT nombre FROM caGeneralesTbl WHERE tabla = 'funcion510' AND columna = 'CDTOLCDANG') ".    				 
    				"AND DATE(fechaEvento) BETWEEN curdate() AND '".$fechaConsulta."' ".
    				"AND vin NOT IN (SELECT vin ".
					"				  FROM alTransaccionUnidadTbl ".
                    "				  WHERE tipoTransaccion = '510' ) ".
                    "AND vin NOT IN (SELECT vin FROM alHistoricoUnidadesTmp WHERE alhistoricounidadestmp.observaciones = 'PROCESO 510')".
                    "AND VIN NOT IN (SELECT vin FROM alinstruccionesmercedestbl)";
    
    $rs = fn_ejecuta_query($sqlUnidades01);
    //echo json_encode($rs);

    $sqlUnidades02 ="INSERT INTO alHistoricoUnidadesTmp (idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
    				" distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
    				"SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
                    " distribuidor, idTarifa, localizacionUnidad, claveChofer,'PROCESO 510' as observaciones, usuario, ip ".
					"FROM alhistoricounidadestbl ".
					"WHERE centroDistribucion IN ('CDSAL','CDANG')  ".
					"AND claveMovimiento IN (SELECT nombre FROM caGeneralesTbl WHERE tabla = 'funcion510' AND columna = 'CDSALCDAGS') ".    				 
    				"AND DAQTE(fechaEvento) BETWEEN curdate() AND '".$fechaConsulta."' ".
    				"AND vin NOT IN (SELECT vin ".
					"				  FROM alTransaccionUnidadTbl ".
                    "				  WHERE tipoTransaccion = '510' ) ".
                    "AND vin NOT IN (SELECT vin FROM alHistoricoUnidadesTmp WHERE alhistoricounidadestmp.observaciones = 'PROCESO 510')".
                    "AND VIN NOT IN (SELECT vin FROM alinstruccionesmercedestbl)";
    
    $rs = fn_ejecuta_query($sqlUnidades02);
    //echo json_encode($rs);

    $sqlUnidades03 ="INSERT INTO alHistoricoUnidadesTmp (idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
    				" distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
    				"SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
                    " distribuidor, idTarifa, localizacionUnidad, claveChofer,'PROCESO 510' as observaciones, usuario, ip ".
					"FROM alhistoricounidadestbl ".
					"WHERE centroDistribucion IN ('CDSFE','CDVER')  ".
					"AND claveMovimiento IN (SELECT nombre FROM caGeneralesTbl WHERE tabla = 'funcion510' AND columna = 'CDSFECDVER') ".    				 
    				"AND DATE(fechaEvento) BETWEEN curdate() AND '".$fechaConsulta."' ".
    				"AND vin NOT IN (SELECT vin ".
					"				  FROM alTransaccionUnidadTbl ".
                    "				  WHERE tipoTransaccion = '510' ) ".
                    "AND vin NOT IN (SELECT vin FROM alHistoricoUnidadesTmp)".
                    "AND VIN NOT IN (SELECT vin FROM alinstruccionesmercedestbl)".
                    "AND id";
    

    $rs = fn_ejecuta_query($sqlUnidades03);
    //echo json_encode($rs);    

    $sqlExistencia660 = "SELECT centroDistribucion, vin, distribuidor, 'Unidad no Existe en la 660' as observaciones ".
                        "FROM alhistoricounidadestmp ".
                        "WHERE vin NOT IN (SELECT vin FROM al660tbl WHERE scaccode = 'XTRA' ".
                        "  AND (dealerid <>'% %' OR dealerid IS NOT NULL) ".
                        "  AND (orisplc <>'% %' OR orisplc IS NOT NULL) ".
                        "  AND (dessplc <>'% %' OR dessplc IS NOT NULL) ".
                        "GROUP BY al660tbl.vin) ";

    $rs = fn_ejecuta_query($sqlExistencia660);
    
    if($rs['root']){
       
        $CentroDistribucion = $rs['root'][0]['centroDistribucion'];
        $UnidadVin = $rs['root'][0]['vin'];
        $distribuidor = $rs['root'][0]['distribuidor'];
        $observaciones = $rs['root'][0]['observaciones'];        

        //echo date("Y-m-d H:i:s");

        $dirPath = "C:\\carbook\\generacion_510\\logInexistentes\\";
        $fileName= "logInexistentes_".date("YmdHi").".txt";
        $filePath = $dirPath.$fileName;        

        $logInexistentes=fopen($filePath,"a") or die("Problemas Forest");
        //fputs($logInexistentes,"aqui inserte una linea poderosa");

        for($a=0; $a < count($rs[root]); $a++){
            $CentroDistribucion = $rs['root'][$a]['centroDistribucion'];
            $UnidadVin = $rs['root'][$a]['vin'];
            $distribuidor = $rs['root'][$a]['distribuidor'];
            $observaciones = $rs['root'][$a]['observaciones'];

            fputs($logInexistentes, $CentroDistribucion.",".$UnidadVin.",".$distribuidor.",".$observaciones."\n" );

            //$eliminarTemporal = "DELETE FROM alhistoricounidadestmp WHERE vin = '".$UnidadVin."'";                                    
        }
       
        fclose($logInexistentes);
        echo "Termino Log Unidades Inexistentes en
         660: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
    }
    else{

        echo"No Existen Unidades Inexistentes en la 660 \n";
        $consultaImportacion = "SELECT A.simboloUnidad, C.tipoOrigenUnidad, B.distribuidor, B.vin ".
                           "  FROM alunidadestbl A , alhistoricounidadestmp B, casimbolosunidadestbl C ".
                           "  WHERE A.vin = B.vin ".
                           "    AND B.observaciones = 'PROCESO 510' ".
                           "    AND A.simboloUnidad = C.simboloUnidad "; 

        $rs = fn_ejecuta_query($consultaImportacion);
        echo json_encode($rs);

        $simboloUnidad = $rs['root'][0]['simboloUnidad'];
        $tipoOrigen = $rs['root'][0]['tipoOrigenUnidad'];
        $distribuidor = $rs['root'][0]['distribuidor'];
        $extraeDistribuidor = substr($distribuidor,0,2);
        $unVin = $rs['root'][0]['vin'];  

        for($b=0; $b < count($rs[root]); $b++){
            $simboloUnidad = $rs['root'][$b]['simboloUnidad'];
            $tipoOrigen = $rs['root'][$b]['tipoOrigenUnidad'];
            $distribuidor = $rs['root'][$b]['distribuidor'];            
            if($extraeDistribuidor == 'M8'){
                echo($simboloUnidad.",");
                $sqlActualizaTipo = "UPDATE casimbolosunidadestbl SET tipoOrigenUnidad = 'I' WHERE simboloUnidad = '".$simboloUnidad."'" ;  
                $rs = fn_ejecuta_query($sqlActualizaTipo);
                echo json_encode($rs);
            }
            elseif($tipoOrigen == 'E'){
                echo($tipoOrigen.",");
                $eliminarExportacion = "DELETE FROM alHistoricoUnidadesTmp WHERE vin '".$unVin."'";   
                $rs = fn_ejecuta_query($eliminarExportacion);
                echo json_encode($rs);
            }
        } 
    }                            
?>
