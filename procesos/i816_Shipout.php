<?php
	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");


	date_default_timezone_set('America/Mexico_City');

	ejecutaEXT();

	function ejecutaEXT(){

		
	    $fechaAnt = strtotime ( '-15 day' , strtotime (date("Y-m-d")) ) ;
		$fechaAnt = date ( 'Y-m-d' , $fechaAnt );

		$fecha = date('Y-m-j');
		$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m-j' , $nuevafecha );

		$sqlEXT = "SELECT * FROM alunidadestbl al, alhistoricounidadestbl ud /*, alinstruccionesmercedestbl dy */".
					"WHERE al.vin = ud.vin ".
					//"-- AND ud.vin = dy.vin ".
					"AND ud.claveMovimiento ='L3' ".
					"AND ud.fechaEvento BETWEEN '2017-04-24' AND '2017-04-26' ".
					"AND ud.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='EL3') ";
		$rsEXT= fn_ejecuta_query($sqlEXT);

		//echo json_encode($rsEXT);	
		
		if (count($rsEXT['root']) != 0) {
			generaEXTKIA1($rsEXT);
		}			
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function generaEXTKIA1($arrKia){

		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');				
			
		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816Shipout\\";

		$inicioFile = "HMM_EXT_".$fecha.$hora.".txt";
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"EXTH"." "."TRA"."  "."GMX"."  "."EXT".$fecha.$hora.PHP_EOL);

		//detalle

		//echo json_encode($arrKia);

		//echo json_encode(sizeof($arrKia['root']))		;

		for ($i=0; $i <sizeof($arrKia['root']) ; $i++) { 

			/*if ($arrKia['root'][$i]['centroDistribucion'] == 'CDTOL') {
				$origin = 'FT15';
			}
			if ($arrKia['root'][$i]['centroDistribucion'] == 'CDSFE') {
				$origin = 'FT18';
			}*/

				

			fwrite($archivo,"EXT  YE".$arrKia['root'][$i]['vin'].substr($arrKia['root'][$i]['fechaEvento'],0,4).substr($arrKia['root'][$i]['fechaEvento'],5,2).
				substr($arrKia['root'][$i]['fechaEvento'],8,2).substr($arrKia['root'][$i]['fechaEvento'],11,2).substr($arrKia['root'][$i]['fechaEvento'],14,2).
				substr($arrKia['root'][$i]['fechaEvento'],17,2).$origin."                "."TRA  T".PHP_EOL);
			
			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('EL3', '".
										$arrKia['root'][$i]['centroDistribucion']."', '".
										$arrKia['root'][$i]['vin']."', '".
										$arrKia['root'][$i]['fechaEvento']."', '".
										$arrKia['root'][$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
			//echo json_encode($insTransaccion);
			
		}
		//fin de archivo
		$long=(sizeof($arrKia['root'])+2);
		fwrite($archivo,"EXTT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);	
	}
?>