<?php
	//require_once("../funciones/generales.php");
    //require_once("../funciones/construct.php");
    //require_once("../funciones/utilidades.php");

    date_default_timezone_set('America/Mexico_City');

    //ejecutaIS2();
    function ejecutaIS2(){
    	 echo "inicio GM S2";

	    unlink("C:/servidores/DFYP301X");


		$sqlUnidadesGM= "SELECT  case when substr(dy.cveDisFac,-5) in('77831','77828','77827','77848','77829','77830','72148','31112') then 'G' else 'M' end as shipper, dy.vin, '' as orderNumber, case when da.vin is not null then 'S2' else 'S3' end  as eventCode , substring(hu.fechaEvento,1,10) as  vehicleDate, ".
									"'  ' as dealer, 'LC' as portCode, '' as portDischarge, dy.currency,'' as volume ,substring(dy.cveDisFac,3) as ship,substring(hu.fechaEvento,1,10) as dateVehicle, ".
							        "case when da.vin is not null then 'HR' else '' end as HRoHQ,case when da.vin is not null then concat(cat.areaDano,cat.tipoDano,cat.severidadDano)  else '' end  as holdCodes, ".
							        "case when da.vin is not null then da.observacion else '' end as   holdAdditional,ca.descripcion,'' as markModCode,'' as vehicleTrans, '' as engineNumber, '' as modelPackcage, ".
							        "'' as carrierCut,  case when da.vin is not null then dy.fped else '' end as vesselName,'' as vesselNumber,'' as voyageNumber, '' as containerNumber,'' as containerSeal,'' as timerQ, ".
							        "concat(substring(hu.fechaEvento,12,2),substring(hu.fechaEvento,15,2),substring(hu.fechaEvento,18,2)) as eventTime,'TRAM' as sender, ".
							        "concat('0000',(SELECT folio +1 FROM trfoliostbl WHERE centroDistribucion='LZC02' AND compania ='GM' AND tipoDocumento ='GM')) as sequence ".
							"FROM alinstruccionesmercedestbl dy inner join alhistoricounidadestbl hu on hu.vin = dy.vin inner join casimbolosunidadestbl ca on ca.simboloUnidad = dy.modelDesc ".
							"left join  aldanosunidadestbl da on  da.vin = dy.vin  left join cadanostbl cat on da.iddano = cat.iddano left join cadistribuidorescentrostbl di on di.distribuidorCentro = dy.cveDisEnt ".
							"WHERE dy.cveStatus='DG' ".
							 "AND da.tipoDano in ('D2','D3','D4') ".
							"and dy.cveLoc='LZC02' ".
							"AND di.tipoDistribuidor='DX' ".
							"AND hu.claveMovimiento='L2' ".
							"AND hu.vin not in (SELECT ti.vin FROM altransaccionunidadtbl ti WHERE ti.vin = hu.vin AND ti.tipoTransaccion in('iS2')) ".
							"AND hu.vin  in (SELECT ti.vin FROM altransaccionunidadtbl ti WHERE ti.vin = hu.vin AND ti.tipoTransaccion in('iS1'))  ".
							"AND hu.vin not in ('1234567PRUEBAGM01',
												'1234567PRUEBAGM02',
												'1234567PRUEBAGM03',
												'1234567PRUEBAGM04',
												'1234567PRUEBAGM05',
												'1234567PRUEBAGM06',
												'1234567PRUEBAGM07',
												'1234567PRUEBAGM08',
												'1234567PRUEBAGM09',
												'1234567PRUEBAGM10') ".
							"order by hu.vin"; 

	    $rstSqlUnidadesGM= fn_ejecuta_query($sqlUnidadesGM);
			


		if(sizeof($rstSqlUnidadesGM['root']) != NULL){

			for ($i=0; $i <sizeof($rstSqlUnidadesGM['root']) ; $i++) { 

				if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S2') {				
					//$arrS2[] = $rstSqlUnidadesGM['root'][$i];
					$sqlHOLD="SELECT * FROM aldanosunidadestbl al, cadanostbl ca 
								WHERE al.vin ='".$rstSqlUnidadesGM['root'][$i]['vin']."'
								AND al.idDano=ca.idDano";
								//AND ca.areaDano not in ('98','52','40')
								//AND ca.tipoDano not in ('08','19','08')
								//AND ca.severidadDano not in ('06'); ";*/
					$rsSqlHOLD=fn_ejecuta_query($sqlHOLD);

					if (($rsSqlHOLD['root']['areaDano'] != '52' || $rsSqlHOLD['root']['areaDano'] != '98' || $rsSqlHOLD['root']['areaDano'] != '40') && ($rsSqlHOLD['root']['tipoDano'] != '08' || $rsSqlHOLD['root']['tipoDano'] != '19') && ($rsSqlHOLD['root']['severidadDano'] != '06')) {
					$arrS2[] = $rstSqlUnidadesGM['root'][$i];
					}
				}		

				/*if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S2') {				
					$arrS2[] = $rstSqlUnidadesGM['root'][$i];
				}*/
			}
		}

		if ($arrS2 != null) {
			generaS2($arrS2);			
		}	
	}

	function generaS2($arrS2){

		$fecha = date("YmdHis");
	    $fileDir = "C:/servidores/DFYP301X";


	    $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
	    $recordsPositionValue = array();
	    
	            
	    //A) ENCABEZADO	    
		fwrite($fileRec,"GESGEIS2.000  TRAM             DFYDETROIT$".$fecha.sprintf('%09s',$arrS2[0]['sequence'])."EST".PHP_EOL);
	        
	    //B) DETALLE UNIDADES
	   	for ($i=0; $i <sizeof($arrS2) ; $i++) {
				

			fwrite($fileRec,sprintf('%-1s',$arrS2[$i]['shipper']).sprintf('%-17s',$arrS2[$i]['vin']).sprintf('%-6s',$arrS2[$i]['orderNumber']).sprintf('%-2s','S2').sprintf('%-10s',$arrS2[$i]['vehicleDate']).
							sprintf('%-9s',$arrS2[$i]['dealer']).sprintf('%-3s',$arrS2[$i]['portCode']).sprintf('%-5s',$arrS2[$i]['portDischarge']).sprintf('%-5s',$arrS2[$i]['currency']).sprintf('%-7s',$arrS2[$i]['volume']).
							sprintf('%-25s',$arrS2[$i]['reserved']).sprintf('%-10s',$arrS2[$i]['dateVehicle']).sprintf('%-30s',$arrS2[$i]['storageLocation']).sprintf('%-15s',$arrS2[$i]['storageNumber']).sprintf('%-14s',$arrS2[$i]['futureUse']).
							sprintf('%-3s',$arrS2[$i]['timerQ']).sprintf('%-6s',$arrS2[$i]['eventTime']).sprintf('%-32s',$arrS2[$i]['reservedForFutureUse']).sprintf('%-100s',$arrS2[$i]['holdAdditional']).PHP_EOL);

			$vin = $arrS2[$i]['vin'];
			$today = date("Y-m-d H:i:s");
			$fecha = substr($today,0,10);
			$hora=substr($today,11,8);
			$fecha1 = $arrS2[$i]['vehicleDate']." ".$hora;


			$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".					 					
								"VALUES ('IS2','LZC02','".$arrS2[$i]['sequence']."','".
								$arrS2[$i]['vin'].
								"','".$fecha1.
								"','S2','".
								$today."','".
								$varEstatus."','".
								$fecha."','".
								$hora."') ";    

			fn_ejecuta_query($sqlAddTransaccion);

			$sqlUpd ="UPDATE alinstruccionesmercedestbl set cveStatus='3G' WHERE vin ='".$arrS2[$i]['vin']."' ";
			fn_ejecuta_query($sqlUpd);

	    }
		//C) TRAILER
		$long=(sizeof($arrS2));
		fwrite($fileRec,"GEE".sprintf('%06d',($long)).sprintf('%09s',$arrS2[0]['sequence']));
	   	fclose($fileRec);	   	

	   	$fechaCopia = date("Y-m-d His");



		$updateFolioGM = "UPDATE trfoliostbl set folio=".$arrS2[0]['sequence']." where centroDistribucion='LZC02' and compania='GM' and tipoDocumento='GM'";
		fn_ejecuta_query($updateFolioGM);

		copy( "C:/servidores/DFYP301X", "E:/carbook/interfacesGM/respaldoS2/DFYP301X".$fechaCopia);		

		exec("C:/Paso/ejecutaFtp.bat");


		echo "Termino IS2";		
	}			
?>  
