<?php
	require_once("../funciones/generales.php");

	$fechaAnt = strtotime ( '-1 day' , strtotime (date("Y-m-d H:i:s")) ) ;
    $fechaAnt = date ( 'Y-m-d H:i:s', $fechaAnt );

    $selBusGen="SElECT ud.centroDistribucion,ud.vin, ud.claveMovimiento, ud.fechaEvento, ud. distribuidor,al.simboloUnidad"
			." FROM alunidadestbl al, alultimodetalletbl ud"
			." WHERE ud.centroDistribucion in ('CDTOL','CDSAL','CDAGS')"
			." AND ud.claveMovimiento in ('PR','CO','EO','DA','RU','CT','RP','EP')"
			." AND ud.vin=al.vin"
			." AND ud.distribuidor=al.distribuidor"
			." AND ud.fechaEvento BETWEEN '2015-01-01' AND '".$fechaAnt."'"
			." AND EXISTS(SELECT * FROM cadistribuidorescentrostbl dx"
					   ." WHERE dx.distribuidorCentro = ud.distribuidor)"
			." AND NOT EXISTS (SELECT * FROM cocuentascobrartbl co Where co.vin=ud.vin)";

	$rstBusGen=fn_ejecuta_query($selBusGen);

	if ($rstBusGen['records']>0) {
		proceso($rstBusGen);
	}
	else{
		echo "no existen unidades";
	}

	function proceso($rstBusGen){

		for ($i=0; $i <sizeof($rstBusGen['root']) ; $i++) { 
			if ($rstBusGen['root'][$i]['simboloUnidad']=='140CBZ' or $rstBusGen['root'][$i]['simboloUnidad']=='140CB5' or $rstBusGen['root'][$i]['simboloUnidad']=='145S36'
				or $rstBusGen['root'][$i]['simboloUnidad']=='191C54' or $rstBusGen['root'][$i]['simboloUnidad']=='145Q36') {
				$marca='CH';
			}
			else{
				$selMarca="SELECT *  FROM casimbolosunidadestbl  WHERE simboloUnidad =".$rstBusGen['root'][$i]['simboloUnidad'];
				$rstMarca=fn_ejecuta_query($selMarca);

				$selClas="SELECT marca FROM caclasificacionmarcatbl WHERE clasificacion=".$rstMarca['root'][0]['clasificacion'];
				$rstClas=fn_ejecuta_query($selClas);

				$marca=substr($rstClas['root'][0]['marca'],0,2);
			}

			$selCxC="SELECT * FROM cocuentascobrartbl"
					." WHERE centroDistribucion='".$rstBusGen['root'][$i]['centroDistribucion']."'"
					." AND vin='".$rstBusGen['root'][$i]['vin']."'"
					." AND concepto='9103'"
					." AND claveGrupo='1'"
					." AND fechaMovimiento='".$rstBusGen['root'][$i]['fechaEvento']."'"
					." AND fechaCobro=' '"
					." AND cantidad='1'"
					." AND importe='1.00'"
					." AND estatus='".$rstBusGen['root'][$i]['claveMovimiento']."'"
					." AND numeroFactura='0'"
					." AND usuario='trap837'";
			$rstCxC=fn_ejecuta_query($selCxC);

			if ($rstCxC['records']==0) {
				$insCxC="INSERT INTO cocuentascobrartbl (cuenta,centroDistribucion, vin, concepto, claveGrupo, fechaMovimiento, fechaCobro, "
						." cantidad, importe, estatus, numeroFactura, usuario)"
						." VALUES (null,'"
						.$rstBusGen['root'][$i]['centroDistribucion']
						."','".$rstBusGen['root'][$i]['vin']
						."','9103','1','"
						.$rstBusGen['root'][$i]['fechaEvento']
						."',null, '1', '1.00', '"
						.$rstBusGen['root'][$i]['claveMovimiento']
						."', '0', 'trap837')";
				$rstIns=fn_ejecuta_query($insCxC);
				//echo json_encode($rstIns);
			}
		}
	}
?>

