<?php
	require("../funciones/generales.php");
	require("../funciones/utilidades.php");

	global $dirPath;
	global $filePath;
	global $fileName;
	date_default_timezone_set('America/Mexico_City');

	//$dirPath = "/Users/poncho/www/carbookBck/";
	$dirPath = "E:\\carbook\\generacionShiper\\";
	$fileName= "shiper.txt";
	$filePath = $dirPath.$fileName;
	$ejecutaProceso = "S";	

 			
	while(true)
		{
			if(date("i")%1 == 0)
			{
				if($ejecutaProceso == "S")
				{

					echo "Inicio: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
					cargaFTP();	
					echo "Termino: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
					$ejecutaProceso = "N";
				}
			}
			else
				$ejecutaProceso = "S";
		}	

	function cargaShiper(){
		global $dirPath;
		global $filePath;
		global $fileName;
		global $lineaStr;
		global $cont;

		$_shiperFile = fopen($filePath, "r");//abres el archivo para lectura
		$fecha = date("d-m-Y_H-i-s");

		if(file($filePath)) {
			
			$cont = 1;
			while (!feof($_shiperFile)) {
				$lineaStr = fgets($_shiperFile);
				$arr = explode("|",$lineaStr);
				insertarDatos($arr);					

				$cont = $cont + 1;
			}
		}else {
        	echo "No se pudo abrir el archivo\r\n";
      	}
      	

      // 	if(!is_dir($dirPath.'respShiper')) {
      // 		mkdir($dirPath.'respShiper');
      // 	}

      // 	copy($filePath, $dirPath.'respShiper\\shiper_'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
      // 	fclose($_shiperFile);
     	// unlink($filePath);
      // 	echo "Termino: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
	}


	function insertarDatos($arr){     

			$sqlAdd = "INSERT INTO alShippersPdfTbl(folioFactura, numFurgon, fddFactura, fmmFactura, faaFactura, planta, numSerie, vin, von, distribuidor,
						simbolo, opciones, descEspaniol, descIngles, impUndbasS, impUndbas, impSeguroS, impSeguro, impFleldoS, impFleldo, impGtosaduS, impGtosadu, 
						impFledistS, impFledist, impSfledistS, impSfledist, impUnidadS, impUnidad, tipoCambio, codigoReg, val807UndbasS, val807Undbas, val807OpcsS, 
						val807Opcs, destinatario, direccion, localidad, tfacUndbasS, tfacUndbas, tfacOpcsS, tfacOpcs, tfacSeguroS, tfacSeguro, tfacFleldoS, 
						tfacFleldo, tfacGtosaduS, tfacGtosadu, tfacFledistS, tfacFledist, tfacSfledistS, tfacSfledist, tfacUnidadS, tfacUnidad, tfacConletra, 
						lugarYfecha, numUnd, impMonnalS, impMonnal, tipoCambios2, routeKing, patenteDesc, patenteCont, rfcDesc, rfcCont, paisDestDesc, paisDestCont, 
						cvePedDesc, cvePedCont, numpedDesc, numpedCont, acuseRboDesc, acuseRboCont, shippedTo, xworkPriceS, xworkPrice, numConsec, numEmb, fddEmb, 
						fmmEmb, faaEmb, tshpUndbasS, tshpUndbas, tshpOpcsS, tshpOpcs, tshpSeguroS, tshpSeguro, tshpFleldoS, tshpFleldo, tshpGtosaduS, tshpGtosadu, 
						tshpFledistS, tshpFledist, tshpSfledistS, tshpSfledist, tshpEmbdS, tshpEmbd, tshpNumUnd, cancelacion, filler, fOrig, cEstatus, fEstatus)".
							"VALUES (".
							"'".$arr[0]."', ".
							"'".$arr[1]."', ".
							"'".$arr[2]."', ".
							"'".$arr[3]."', ".
							"'".$arr[4]."', ".
							"'".$arr[5]."', ".
							"'".$arr[6]."', ".//routeori
							"'".$arr[7]."', ".
							"'".$arr[8]."', ".//von
							"'".$arr[9]."', ".
							"'".$arr[10]."', ".
							"'".$arr[11]."', ".
							"'".$arr[12]."', ".
							"'".$arr[13]."', ".
							"'".$arr[14]."', ".
							"'".$arr[15]."', ".
							"'".$arr[16]."', ".
							"'".$arr[17]."', ".
							"'".$arr[18]."', ".
							"'".$arr[19]."', ".
							"'".$arr[20]."', ".
							"'".$arr[21]."', ".
							"'".$arr[22]."', ".
							"'".$arr[23]."', ".
							"'".$arr[24]."', ".
							"'".$arr[25]."', ".
							"'".$arr[26]."', ".
							"'".$arr[27]."', ".
							"'".$arr[28]."', ".
							"'".$arr[29]."', ".
							"'".$arr[30]."', ".
							"'".$arr[31]."', ".
							"'".$arr[32]."', ".
							"'".$arr[33]."', ".
							"'".$arr[34]."', ".
							"'".$arr[35]."', ".
							"'".$arr[36]."', ".
							"'".$arr[37]."', ".
							"'".$arr[38]."', ".
							"'".$arr[39]."', ".
							"'".$arr[40]."', ".
							"'".$arr[41]."', ".
							"'".$arr[42]."', ".
							"'".$arr[43]."', ".
							"'".$arr[44]."', ".
							"'".$arr[45]."', ".
							"'".$arr[46]."', ".
							"'".$arr[47]."', ".
							"'".$arr[48]."', ".
							"'".$arr[49]."', ".
							"'".$arr[50]."', ".
							"'".$arr[51]."', ".
							"'".$arr[52]."', ".
							"'".$arr[53]."', ".
							"'".$arr[54]."', ".
							"'".$arr[55]."', ".
							"'".$arr[56]."', ".
							"'".$arr[57]."', ".
							"'".$arr[58]."', ".
							"'".$arr[59]."', ".
							"'".$arr[60]."', ".
							"'".$arr[61]."', ".
							"'".$arr[62]."', ".
							"'".$arr[63]."', ".
							"'".$arr[64]."', ".
							"'".$arr[65]."', ".
							"'".$arr[66]."', ".
							"'".$arr[67]."', ".
							"'".$arr[68]."', ".
							"'".$arr[69]."', ".
							"'".$arr[70]."', ".
							"'".$arr[71]."', ".
							"'".$arr[72]."', ".
							"'".$arr[73]."', ".
							"'".$arr[74]."', ".
							"'".$arr[75]."', ".
							"'".$arr[76]."', ".
							"'".$arr[77]."', ".
							"'".$arr[78]."', ".
							"'".$arr[79]."', ".
							"'".$arr[80]."', ".
							"'".$arr[81]."', ".
							"'".$arr[82]."', ".
							"'".$arr[83]."', ".
							"'".$arr[84]."', ".
							"'".$arr[84]."', ".
							"'".$arr[86]."', ".
							"'".$arr[87]."', ".
							"'".$arr[88]."', ".
							"'".$arr[89]."', ".
							"'".$arr[90]."', ".
							"'".$arr[91]."', ".
							"'".$arr[92]."', ".
							"'".$arr[93]."', ".
							"'".$arr[94]."', ".
							"'".$arr[95]."', ".
							"'".$arr[96]."', ".
							"'".$arr[97]."', ".
							"'".$arr[98]."', ".
							"'".$arr[99]."', ".
							"'".$arr[100]."', ".
							"'".$arr[101]."') ";

		fn_ejecuta_query($sqlAdd);
	}	
	

	function cargaFTP(){
		$nombreArchivos = 'shiper.txt';
		$servidor    = "10.2.2.3";
		$usuario     = "emilio";
		$password    = "choncho00";
		//$local = "../../tratDy/";
		$local = "E:\\carbook\\generacionShiper\\shiper.txt";
		$remoto = "/respaldo/shiper/txt/shiper.txt";

		// conexión
		$conexion = ftp_connect($servidor);
		  
		// loggeo
		$login = ftp_login($conexion, $usuario, $password); 
		echo $login;
		//porq no me aparece tu directorio en el localhosto?
		// conexión
		if ((!$conexion) || (!$login))
		{ 
		    $error = 1;
		    $msg  = "Conexión fallida al sitio FTP!";		    
		    //echo "Intentando conectar a $servidor for user $usuario"; 
		    exit; 
		}
		else{
		    $msg = "Conectado a $servidor, for user $usuario";		    
		    //ftp_get($conexion, $local, $remoto, $nombreArchivos,FTP_BINARY);
		    if (ftp_get($conexion, $local, $remoto,FTP_BINARY)) {
   				echo "Se ha guardado satisfactoriamente en $local\n";
   				cargaShiper();
   				copy($filePath, $resPath.'shiperPath\\shiper_'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
      			fclose($_shiperFile);
     			unlink($fileRespFile);
			} 
			else {
    			echo "Ha ocurrido un problema $local\n";
			}
		}				
		
		ftp_close($conexion);
	}		
?>