<?php
	require("../funciones/generales.php");
    require("../funciones/construct.php");
    require_once("../funciones/utilidadesProcesos.php");
    require_once("../funciones/utilidades.php");

    i343();

	function i343(){

		echo "inicio i343";
		setlocale(LC_TIME, 'spanish');

		$fechaAnt = strtotime ( '-10 day' , strtotime (date("Y-m-d")) ) ;
		$fechaAnt = date ( 'Y-m-d' , $fechaAnt );

		$sqlGetUnidades1 = "SELECT distinct c.nombre ".
											"FROM alHistoricoUnidadesTbl h1, alUnidadesTbl al, caSimbolosUnidadesTbl ca, caTarifasTbl tb, caGeneralesTbl c ".
											"WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
											"AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
											"AND h1.claveMovimiento IN ('ED','OM','ER','RO','PT','OK','CS') ".
											"AND h1.vin = al.vin ".
											"AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX') ".
											"AND substring(al.distribuidor,1,1) !='H' ".
											"AND al.simboloUnidad = ca.simboloUnidad ".
											"AND ca.marca not in ('HY','KI') ".
											"AND h1.idTarifa = tb.idTarifa ".
											"AND tb.tarifa !='13' ".
											"AND c.tabla = 'interfaces' ".
											"AND c.columna = 'splc' ".
											"AND c.valor = h1.centroDistribucion ".
											"AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
											"AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ".
											"UNION ".
											"SELECT distinct c.nombre ".
											"FROM alHistoricoUnidadesTbl h1, alUnidadesTbl al, caSimbolosUnidadesTbl ca,caTarifasTbl tb, caGeneralesTbl c ".
											"WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
											"AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
											"AND h1.claveMovimiento IN ('EP') ".
											"AND h1.vin = al.vin ".
											"AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX')  ".
											"AND al.distribuidor IN ('M8220', 'M8221', 'M8270', 'M8271') ".
											"AND al.simboloUnidad = ca.simboloUnidad ".
											"AND ca.marca not in ('HY','KI') ".
											"AND h1.idTarifa = tb.idTarifa ".
											"AND tb.tarifa !='13' ".
											"AND c.tabla = 'interfaces' ".
											"AND c.columna = 'splc' ".
											"AND c.valor = h1.centroDistribucion ".
											"AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
											"AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ";
		//echo "<br>$sqlGetUnidades1<br>";
		$rsGetUnidades1 = fn_ejecuta_query($sqlGetUnidades1);

		//echo json_encode($rsGetUnidades1);

		for ($i=0; $i <sizeof($rsGetUnidades1['root']) ; $i++) { 

			$sqlGetUnidades = "SELECT h1.centroDistribucion, h1.vin, h1.fechaEvento, h1.claveMovimiento, tb.tarifa,al.distribuidor, ".
												"(SELECT ca.nombre FROM cageneralestbl ca WHERE ca.tabla = 'interfaces' AND ca.columna = 'splc' AND ca.valor = h1.centroDistribucion) as oriSplc ".
												"FROM alHistoricoUnidadesTbl h1, alUnidadesTbl al, caSimbolosUnidadesTbl ca, caTarifasTbl tb, caGeneralesTbl c ".
												"WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
												"AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
												"AND h1.claveMovimiento IN ('ED','OM','ER','RO','PT','OK','CS') ".
												"AND h1.vin = al.vin ".
												"AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX') ".
												"AND substring(al.distribuidor,1,1) !='H' ".
												"AND al.simboloUnidad = ca.simboloUnidad ".
												"AND ca.marca not in ('HY','KI') ".
												"AND h1.idTarifa = tb.idTarifa ".
												"AND tb.tarifa !='13' ".
												"AND c.tabla = 'interfaces' ".
												"AND c.columna = 'splc' ".
												"AND c.valor = h1.centroDistribucion ".
												"AND c.nombre='".$rsGetUnidades1['root'][$i]['nombre']."' ".
												"AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
												"AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ".
												"UNION ".
												"SELECT h1.centroDistribucion, h1.vin, h1.fechaEvento, h1.claveMovimiento, tb.tarifa,al.distribuidor, ".
												"(SELECT ca.nombre FROM cageneralestbl ca WHERE ca.tabla = 'interfaces' AND ca.columna = 'splc' AND ca.valor = h1.centroDistribucion) as oriSplc ".
												"FROM alHistoricoUnidadesTbl h1, alUnidadesTbl al, caSimbolosUnidadesTbl ca,caTarifasTbl tb, caGeneralesTbl c ".
												"WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
												"AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
												"AND h1.claveMovimiento IN ('EP') ".
												"AND h1.vin = al.vin ".
												"AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX')  ".
												"AND al.distribuidor IN ('M8220', 'M8221', 'M8270', 'M8271') ".
												"AND al.simboloUnidad = ca.simboloUnidad ".
												"AND ca.marca not in ('HY','KI') ".
												"AND h1.idTarifa = tb.idTarifa ".
												"AND tb.tarifa !='13' ".
												"AND c.tabla = 'interfaces' ".
												"AND c.columna = 'splc' ".
												"AND c.valor = h1.centroDistribucion ".
												"AND c.nombre='".$rsGetUnidades1['root'][$i]['nombre']."' ".
												"AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
												"AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ".
												"order by centrodistribucion ".
												"LIMIT 100; ";
			//echo "<br>$sqlGetUnidades<br>";
			$rsGetUnidades = fn_ejecuta_query($sqlGetUnidades);

			//echo json_encode($rsGetUnidades);

			if (sizeof($rsGetUnidades['root']) !='0') {
				generaArchivo($rsGetUnidades);
			}else{
				echo "no existen unidades por transmitir";
			}
		}		
	}

	function generaArchivo($rsGetUnidades){		

		$selFolio="SELECT * FROM trFoliosTbl ".
							"WHERE compania='H10' ".					
							"AND centroDistribucion='TCO';";
		$rsFolio=fn_ejecuta_query($selFolio);					

  	$dir = opendir("E:/carbook/i343");				
		//Se leen todos los archivos
		while ($archivo = readdir($dir))
		{
		    if (!is_dir($archivo))
		    {				    
				    $aux = explode('.',$archivo);
				    if(isset($aux[1]))
						{
								if(strtoupper($aux[1]) == 'TXT' && substr($aux[0],0,5) == 'HA510')
								{
										$arch = "E:/carbook/i343/".$aux[0].'.'.$aux[1];
										unlink($arch);
								}
						}
				 }
		}
		
  	//$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
    $fileDir = "E:/carbook/i343/HA510".str_pad($rsFolio['root'][0]['folio'], 5, "0", STR_PAD_LEFT).".txt";
    $flReporte660 = fopen($fileDir, "a") or die("No se pudo generar ,interfaz");

	 	//A) ENCABEZADO
	 											 
		fwrite($flReporte660,'ISA*03*HA510     *00*          *ZZ*XTRA                                          *'.'ZZ*ADIMS'.'DCC '.'      *'.date('ymd').'*'.date('hi').'*U*00200*'.sprintf('%09d','1').'*0*P*<'.PHP_EOL);												 
		fwrite($flReporte660,'GS*VI*XTRA*INNI*'.date('ymd').'*'.date('hi').'*1*T*1'.PHP_EOL);

		fwrite($flReporte660,'ST*510*1'.sprintf('%04d',"1").PHP_EOL);
		fwrite($flReporte660,'BV1*XTRA*'.$rsGetUnidades['root'][0]['oriSplc'].'*'.sprintf('%04d', sizeof($rsGetUnidades['root'])).PHP_EOL);

		//echo json_encode($rsGetUnidades);

		for ($e=0; $e <sizeof($rsGetUnidades['root']) ; $e++) {			
			$incremento = $incremento + 1;

			if ($rsGetUnidades['root'][$e]['claveMovimiento'] =='DT' || $rsGetUnidades['root'][$e]['claveMovimiento'] =='EP' || $rsGetUnidades['root'][$e]['claveMovimiento'] =='CS' || $rsGetUnidades['root'][$e]['claveMovimiento'] =='PT' || $rsGetUnidades['root'][$e]['claveMovimiento'] =='ER') {
				$fechaInterfaz=$rsGetUnidades['root'][$e]['fechaEvento'];
			}else{
				$selFechaEP="SELECT * FROM alHistoricoUnidadesTbl WHERE vin ='".$rsGetUnidades['root'][$e]['vin']."' AND claveMovimiento='EP'";
				$rsFechaEP=fn_ejecuta_query($selFechaEP);

				$fechaInterfaz=$rsFechaEP['root'][0]['fechaEvento'];
			}

			/*$sqlGetSplc = "SELECT nombre as oriSplc FROM cageneralestbl ".
											"WHERE tabla = 'interfaces' ".
											"AND columna = 'splc' ".
											"AND valor = '".$rsGetUnidades['root'][$e]['centroDistribucion']."'";
			$rsGetSplc = fn_ejecuta_query($sqlGetSplc);*/			

			//for ($i=0; $i < sizeof($rsGetUnidades['root']); $i++) {

			fwrite($flReporte660,'VI*'.$rsGetUnidades['root'][$e]['vin'].'****'.$rsGetUnidades['root'][$e]['distribuidor'].PHP_EOL);
			fwrite($flReporte660,'P1**'.date("ymd",strtotime($fechaInterfaz)).'*A*'.date("hi",strtotime($fechaInterfaz)).PHP_EOL);
			fwrite($flReporte660,'P2**'.date("ymd",strtotime($rsGetUnidades['root'][$e]['fechaEvento'])).'*A*'.date("hi",strtotime($rsGetUnidades['root'][$e]['fechaEvento'])).PHP_EOL);
			//}
			fwrite($flReporte660,'SE*'.sprintf('%02d', (sizeof($rsGetUnidades['root']) * 3) + 1).'*1'.sprintf('%04d', $incremento).PHP_EOL);

			$today = date("Y-m-d H:i:s");
			$fecha = substr($today,0,10);
			$hora=substr($today,11,8);
			$fecha1 = $fechaInterfaz;    	

			$sqlAddTransaccion= "INSERT INTO alTransaccionUnidadTbl(tipoTransaccion,centroDistribucion,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,fecha,hora) ".					 					
													"VALUES ('H10','".$rsGetUnidades['root'][$e]['centroDistribucion']."','".
													$rsGetUnidades['root'][$e]['vin'].
													"','".$fecha1."','".
													$rsGetUnidades['root'][$e]['claveMovimiento']."','".
													$today."','".								
													$fecha."','".
													$hora."')";
			fn_ejecuta_query($sqlAddTransaccion);
		}

		fwrite($flReporte660,'GE*'.$incremento.'*'.'1'.PHP_EOL);
		fwrite($flReporte660,'IEA*01*'.sprintf('%09d','1').PHP_EOL);

		//validaRow($rsFolio['root'][0]['folio']);

		$updateFolio="UPDATE trFoliosTbl SET folio='".($rsFolio['root'][0]['folio']+1)."' ".
								"WHERE compania='H10' ".					
								"AND centroDistribucion='TCO';";
		fn_ejecuta_query($updateFolio);

		echo "termino i343";			
	}

	/*function validaRow($folio){

		$fechaAnt = strtotime ( '-10 day' , strtotime (date("Y-m-d")) ) ;
		$fechaAnt = date ( 'Y-m-d' , $fechaAnt );

		$sqlGetUnidades1 = "SELECT distinct c.nombre ".
						"FROM alhistoricounidadestbl h1, alunidadestbl al, casimbolosunidadestbl ca, catarifastbl tb, cageneralestbl c ".
						"WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
						"AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
						"AND h1.claveMovimiento IN ('ED','OM','ER','RO','PT','OK','CS') ".
						"AND h1.vin = al.vin ".
						"AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX') ".
						"AND substring(al.distribuidor,1,1) !='H' ".
						"AND al.simboloUnidad = ca.simboloUnidad ".
						"AND ca.marca not in ('HY','KI') ".
						"AND h1.idTarifa = tb.idTarifa ".
						"AND tb.tarifa !='13' ".
						"AND c.tabla = 'interfaces' ".
						"AND c.columna = 'splc' ".
						"AND c.valor = h1.centroDistribucion ".
						"AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
						"AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ".
						"UNION ".
						"SELECT distinct c.nombre ".
						"FROM alhistoricounidadestbl h1, alunidadestbl al, casimbolosunidadestbl ca,catarifastbl tb, cageneralestbl c ".
						"WHERE CAST(h1.fechaEvento AS DATE) >= CAST('".$fechaAnt."' as DATE)  ".
						"AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC')  ".
						"AND h1.claveMovimiento IN ('EP') ".
						"AND h1.vin = al.vin ".
						"AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX')  ".
						"AND al.distribuidor IN ('M8220', 'M8221', 'M8270', 'M8271') ".
						"AND al.simboloUnidad = ca.simboloUnidad ".
						"AND ca.marca not in ('HY','KI') ".
						"AND h1.idTarifa = tb.idTarifa ".
						"AND tb.tarifa !='13' ".
						"AND c.tabla = 'interfaces' ".
						"AND c.columna = 'splc' ".
						"AND c.valor = h1.centroDistribucion ".
						"AND h1.vin IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='RA3') ".
						"AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ";

		$rsGetUnidades1 = fn_ejecuta_query($sqlGetUnidades1);

		if (sizeof($rsGetUnidades1['root']) =='0') {
			$updateFolio="UPDATE trfoliostbl SET FOLIO='".($folio +1)."' ".
						"WHERE compania='H10' ".					
						"AND centroDistribucion='TCO';";
			fn_ejecuta_query($updateFolio);

			echo "termino i343";			
		}else{
			i343();	
		}		
	}*/
?>
