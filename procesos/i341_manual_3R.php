<?php
    require_once("C:/Servidores/apache/htdocs/carbookbck/funciones/generales.php");
    require_once("C:/Servidores/apache/htdocs/carbookbck/funciones/construct.php");
    require_once("C:/Servidores/apache/htdocs/carbookbck/funciones/utilidades.php");
    require_once("C:/Servidores/apache/htdocs/carbookbck/funciones/utilidadesProcesos.php");

    i341_CB();

    function i341_CB(){

    	echo "inicio 341 ";

	    $startDate = date('Y-m-d', strtotime("-3000 days")); //- 5 días
        //$startDate = date('Y-m-d');
	    $yesterday = date('Y-m-d', strtotime("-1 days"));
	    $today = date('Y-m-d');
	    $todayDateTime = date('Y-m-d H:i:s');
	    $tipoTransaccion = 'RA3';

        $tipoTransmision=$_REQUEST['tipoTransmision'];
        $centroDistribucion=$_REQUEST['centroDistribucion'];
        $vin1= $_REQUEST['vines'];


        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");
        $vin=str_ireplace($buscar,$reemplazar,$vin1);

        $cadena = chunk_split($vin, 17,"','");
        
        $vines=substr($cadena,0,-2);


	    $rsUnidadesCentros = getGroupUnidadesByCentros(getHistoricoUnidades341($startDate, $today, $vines, $centroDistribucion, $tipoTransmision));
	    //echo json_encode($rsUnidadesCentros);
	    generarReporte341($rsUnidadesCentros);
	    insertarTransacciones($rsUnidadesCentros, $tipoTransaccion, $todayDateTime);
	    generarLog341($rsUnidadesCentros, $todayDateTime);
	}
    function getHistoricoUnidades341($startDate, $endDate, $vines, $centroDistribucion, $tipoTransmision) {

        if ($tipoTransmision =='primer3R') {
            $transmision="SELECT MIN";
        }else{
            $transmision="SELECT MAX";
        }

  		$sqlHistoricoUnidades341 = "SELECT DISTINCT hu.centroDistribucion,hu.vin,hu.fechaEvento,hu.claveMovimiento,hu.distribuidor, ".
                                "(SELECT ra.descripcionRampa FROM caRampasTbl ra WHERE ra.centroDistribucion = hu.localizacionUnidad) as rampa, ".
                                "(SELECT ra.codigoRampa FROM caRampasTbl ra WHERE ra.centroDistribucion = hu.localizacionUnidad) as codigoRampa, ".
                                "(SELECT ra.codigoLocalizacion FROM caRampasTbl ra WHERE ra.centroDistribucion = hu.localizacionUnidad) as codigoLocalizacion ".
                                "FROM alhistoricounidadestbl hu, alunidadestbl au, casimbolosunidadestbl su ".
                                "WHERE hu.vin = au.vin ".
                                "AND au.simboloUnidad = su.simboloUnidad ".
                                "AND su.marca not in ('HY','KI') ".
                                "AND CAST(hu.fechaEvento as date) >= CAST('".$startDate."' AS DATE) ".
                                //"AND hu.vin NOT IN(SELECT tu.vin FROM altransaccionunidadtbl tu WHERE tu.vin = hu.vin AND tu.tipoTransaccion IN('H10')) ".
                                //"AND hu.vin NOT IN(SELECT tu.vin FROM altransaccionunidadtbl tu WHERE tu.vin = hu.vin AND tu.tipoTransaccion IN('RA3'))  ".
                                "AND hu.centroDistribucion  IN ('".$centroDistribucion."') ".
                                //"AND hu.centroDistribucion  IN ('CDSAL') ".
                                "AND substring(hu.distribuidor,1,1) !='H' ".                                
                                "AND hu.claveMovimiento IN ('AM','EP','AS','ER','SO','SI','SV','ST','SC','EF','SJ','EC','SH','SF','SN','SP','FE') ".
                                "and hu.vin in ('".$vines.") ".
                                "AND hu.fechaEvento =(".$transmision."(hu1.fechaEvento) from alHistoricoUnidadesTbl hu1 where hu1.vin=hu.vin AND hu1.claveMovimiento in ('AM','EP','AS','ER','SO','SI','SV','ST','SC','EF','SJ','EC','SH','SF','SN','SP','FE')) ".
                              //  "AND hu.idTarifa NOT IN (SELECT ta.idTarifa FROM caTarifasTbl ta WHERE hu.idTarifa = ta.idTarifa AND ta.tarifa = '13') ".
                                "GROUP BY hu.vin;";

  		$rsUnidadesCentros = fn_ejecuta_query($sqlHistoricoUnidades341);
  		return($rsUnidadesCentros);
    }


    function getGroupUnidadesByCentros($rsUnidades341) {

    	if(!isset($rsUnidades341['root']))
    		return;

    	$rsCentrosDistribucion = array();
    	foreach ($rsUnidades341['root'] as $unidad) {
	    	$unidadRecord = $unidad;

	    	if(preg_match('/^CD+/', $unidad['localizacionUnidad']))
	    		$patio = substr($unidad['localizacionUnidad'], 2);
	    	else
	    		$patio = $unidad['localizacionUnidad'];

	    	$unidadRecord = array('patio' => $patio, 'vin' => $unidad['vin'], 'centroDistribucion' => $unidad['centroDistribucion'],
			    				  'claveMovimiento' => $unidad['claveMovimiento'], 'codigoLocalizacion' => $unidad['codigoLocalizacion'],
			    				  'prodstatus' => $unidad['prodstatus'], 'vupdate' => $unidad['vupdate'],
			    				  'vuptime' => $unidad['vuptime'], 'fechaEvento' => $unidad['fechaEvento'],
			    				  'localizacionUnidad' => $unidad['localizacionUnidad']);

	    	if (!isset($rsCentrosDistribucion[$unidad['centroDistribucion']])) {
	    		$rsCentrosDistribucion[$unidad['centroDistribucion']] = array();
	    		$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']] = array();
	    		$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
	    	} else {
	    		if (!isset($rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']])) {
	    			$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']] = array();
	    			$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
	    		} else{
	    			$rsCentrosDistribucion[$unidad['centroDistribucion']][$unidadRecord['patio']][] = $unidadRecord;
	    		}
	    	}
    	}

    	return $rsCentrosDistribucion;
    }


    function generarReporte341($rsCentrosDistribucion) {

    	if (count($rsCentrosDistribucion) < 1)
    		return;

        $sqlGetFolio = "SELECT LPAD(FOLIO + 1,5,0) AS numFolio, folio + 1 as actualFolio ".
                      "FROM trfoliostbl ".
                      "WHERE centroDistribucion = 'CDTOL' ".
                      "AND tipoDocumento = '3R'";

      $rsGetFolio = fn_ejecuta_query($sqlGetFolio);

      $updFolio = "UPDATE trfoliostbl ".
                  "SET folio = '".$rsGetFolio['root'][0]['actualFolio']."' ".
                  "WHERE centroDistribucion = 'CDTOL' ".
                  "AND tipoDocumento = '3R';";

      $rsUpdFolio = fn_ejecuta_query($updFolio);

    	//$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA3R.txt";
    	$fileDir = "C:/carbook/i341_CB/RA3R".$rsGetFolio['root'][0]['numFolio'].".txt";
    	$flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
    	$recordsPositionValue = array();
    	foreach ($rsCentrosDistribucion as $nombreCentro => $centroDistribucion) {
    	    	if($nombreCentro == 'CDSAL'){
    				$rampaArr = 'ADIMSRAMPMX806';
    				$ptoUnd = 'X6';
    			}elseif($nombreCentro == 'CDTOL'){
    				$rampaArr = 'ADIMSRAMPMX807';
    				$ptoUnd = 'X7';
    			}elseif($nombreCentro =='CDANG'){
    				$rampaArr = 'ADIMSRAMPMX808';
    				$ptoUnd = 'X8';
    			}elseif($nombreCentro == 'CDAGS'){
    				$rampaArr = 'ADIMSRAMPMX011';
    				$ptoUnd = 'X4';
    			}elseif($nombreCentro == 'CDMAZ'){
    				$rampaArr = 'ADIMSRAMPMX809';
    				$ptoUnd = 'X9';
    			}elseif($nombreCentro == 'CDSFE'){
    				$rampaArr = 'ADIMSRAMPMX988';
    				$ptoUnd = 'XO';
    			}elseif($nombreCentro == 'CDCUA'){
    				$rampaArr = 'ADIMSRAMPMX888';
    				$ptoUnd = 'XU';
    			}elseif($nombreCentro == 'CDLZC'){
    				$rampaArr = 'ADIMSRAMPMX816';
    				$ptoUnd = 'PT';
    			}


    		foreach ($centroDistribucion as $nombrePatio => $patios) {
                foreach ($patios as $unidad) {

                   $sqlGetFecha = "SELECT h.vin, h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%m%d%y')AS fechaFormato,h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%H%i')AS horaFormato ".
                                            "FROM alhistoricounidadestbl h ".
                                            "WHERE h.fechaEvento =(SELECT MAX(hu1.fechaEvento) from alHistoricoUnidadesTbl hu1 where hu1.vin=h.vin AND hu1.claveMovimiento in ('AM','EP','AS','ER','SO','SI','SV','ST','SC','EF','SJ','EC','SH','SF','SN','SP','FE')) ".
                                            "AND h.vin = '".$unidad['vin']."'; ";

                    $rsGetFecha = fn_ejecuta_query($sqlGetFecha);

                    if ($_REQUEST['fechaTransmision']== '') {
                        $fecha= $rsGetFecha['root'][0]['fechaFormato'];
                        $Hora= $rsGetFecha['root'][0]['horaFormato'];
                    }else{

                        $fecha=substr($_REQUEST['fechaTransmision'],8,2).substr($_REQUEST['fechaTransmision'],5,2).substr($_REQUEST['fechaTransmision'],2,2);

                        $Hora= substr($_REQUEST['fechaTransmision'],11,2).substr($_REQUEST['fechaTransmision'],14,2);
                    }
                                    
                    if ($_REQUEST['tipoTransmision']=='primer3R') {
                        $transmision="SELECT MIN";
                    }else{
                        $transmision="SELECT MAX";
                    }

                }

    		//A) ENCABEZADO
    		//Posiciones - Valores
    		$record = array( 0 =>  'ISA*03*RA3R      *00*          *ZZ*',
						    35 => $rampaArr." ",
						    50 => "*ZZ*ADIMS          *",
						    //70 => date_format(date_create($today), 'y'),
						    //72 => date_format(date_create($today), 'm'),
						    74 => $fecha.$Hora,
						    76 => '*',
						    77 => date_format(date_create(date('H:i:s')), 'H'),
						    79 => date_format(date_create(date('H:i:s')), 'i'),
						    81 => '*U*00300*',
						    90 => sprintf('%09d', count($patios)),
						    99 => '*0*P*'
						   );
    		    			//echo $nombrePatio;


    		$recordsPositionValue[] = $record;
    		$headerTxt = getTxt($record, array_keys($record));
    		fwrite($flReporte660, $headerTxt.out('n', 1));
    			//B) DETALLE UNIDADES
    			foreach ($patios as $unidad) {

                   $sqlGetFecha = "SELECT h.vin, h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%m%d%y')AS fechaFormato,h.fechaEvento,DATE_FORMAT(h.fechaEvento,'%H%i')AS horaFormato ".
                                            "FROM alhistoricounidadestbl h ".
                                            "WHERE h.fechaEvento =(SELECT MAX(hu1.fechaEvento) from alHistoricoUnidadesTbl hu1 where hu1.vin=h.vin AND hu1.claveMovimiento in ('AM','EP','AS','ER','SO','SI','SV','ST','SC','EF','SJ','EC','SH','SF','SN','SP','FE')) ".
                                            "AND h.vin = '".$unidad['vin']."'; ";

                    $rsGetFecha = fn_ejecuta_query($sqlGetFecha);

                    if ($_REQUEST['fechaTransmision']== '') {
                        $fecha= $rsGetFecha['root'][0]['fechaFormato'];
                        $Hora= $rsGetFecha['root'][0]['horaFormato'];
                    }else{

                        $fecha=substr($_REQUEST['fechaTransmision'],8,2).substr($_REQUEST['fechaTransmision'],5,2).substr($_REQUEST['fechaTransmision'],2,2);

                        $Hora= substr($_REQUEST['fechaTransmision'],11,2).substr($_REQUEST['fechaTransmision'],14,2);
                    }
                                    
                    if ($_REQUEST['tipoTransmision']=='primer3R') {
                        $transmision="SELECT MIN";
                    }else{
                        $transmision="SELECT MAX";
                    }



					//Posiciones - Valores
					$record = array( 0 =>  '3R',
								     2 => $ptoUnd,
								     //4 => date('mdy'),
                                     //4 => $rsGetFecha['root'][0]['fechaFormato'],
                                     4 => $fecha,
								    10 => $unidad['vin'],
								    27 => 'AA111',
								    32 => out('s', 17),
								    //49 => date_format(date_create($todayDateTime), 'h'),
								    //51 => date_format(date_create($todayDateTime), 'i'),
                                    //49=> $rsGetFecha['root'][0]['horaFormato'],
                                    49=> $Hora,
								    //53 => substr(date_format(date_create($rsGetFecha['root'][0]['horaFormato']), 'A'), 0, 1),
								    54 => out('s', 25),
								    79 => lws_marca($unidad['vin'][0], $unidad['codigoLocalizacion']),
							   	   );

					$detalleTxt = getTxt($record, array_keys($record));
    				fwrite($flReporte660, $detalleTxt.out('n', 1));
    			}
    			//C) TRAILER
    			//Posiciones - Valores
    			$record = array(0 => 'IEA*01*', 7 => sprintf('%09d', count($patios)));
    			$recordsPositionValue[] = $record;
    			$trailerTxt = getTxt($record, array_keys($record));
    			fwrite($flReporte660, $trailerTxt.out('n', 1));
    		}
		}
		fclose($flReporte660);
    }
    function insertarTransacciones($rsCentrosDistribucion, $tipoTransaccion, $dateTime) {

    	if (!isset($rsCentrosDistribucion) || count($rsCentrosDistribucion) < 1)
    		return;

    	/*$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl".
    							"(tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, ".
								"claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) VALUES";*/

		foreach ($rsCentrosDistribucion as $nombreCentro => $rsCentroDistribucion) {
			foreach ($rsCentroDistribucion as $nombrePatio => $rsPatio) {
				$i = 0;
		
				foreach ($rsPatio as $unidad) {
					if($i++ > 0)
						// $sqlInsertTransaccion .= ', ';

					$vupdate = DateTime::createFromFormat('d/m/Y', $unidad['vupdate']);
					$vupdate = date_format($vupdate, 'Y-m-d');

					$vuptime = $unidad['vuptime'];
					if(strlen($unidad['vuptime']) > 5)
						$vuptime = substr($unidad['vuptime'], 0, 5);

                     $sqlGetFolio = "SELECT LPAD(FOLIO + 1,5,0) AS numFolio, folio  as actualFolio ".
                      "FROM trfoliostbl ".
                      "WHERE centroDistribucion = 'CDTOL' ".
                      "AND tipoDocumento = '3R'";

                    $rsGetFolio = fn_ejecuta_query($sqlGetFolio);


					$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl ".
    											"(tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, ".
												"claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) VALUES ".
											 	"('$tipoTransaccion', '$unidad[centroDistribucion]', '$unidad[vin]', '$dateTime', ".
											 	"'$unidad[claveMovimiento]', '$unidad[fechaEvento]', 'RA3R".$rsGetFolio['root'][0]['numFolio']."', ".
											 	"'$vupdate', '$vuptime');";

					fn_ejecuta_query($sqlInsertTransaccion);

				}
			}
		}


		//echo json_encode($sqlInsertTransaccion);

    }
    function generarLog341($rsCentrosDistribucion, $dateTime) {

    	$unidadesR3Suma = 0;
    	if (isset($rsCentrosDistribucion) && count($rsCentrosDistribucion) > 1) {
	    	foreach ($rsCentrosDistribucion as $nombreCentro => $rsCentroDistribucion) {
				foreach ($rsCentroDistribucion as $nombrePatio => $rsPatio) {
					$unidadesR3Suma += count($rsPatio);
				}
			}
		}

    	$record1 = array( 0 => 'TOTAL DE UNDS EN R3',
					     20 => date_format(date_create($dateTime), 'd/m/Y'),
					     30 => '-',
					     31 => date_format(date_create($dateTime), 'H:i:s'),
					     40 => ':',
						);

    	$record2 = array(0 =>  $unidadesR3Suma);

    	//$fileDir = $_SERVER['DOCUMENT_ROOT'].'/TOT3R.log';

    	/*$fileDir = "C:/carbook/i341_CB/TOT3R.log";
    	$log341 = fopen($fileDir, 'a+') or die("$dateTime .- No se pudo generar archivo de LOG");
    	fwrite($log341, getTxt($record1, array_keys($record1)).out('n', 1));
    	fwrite($log341, getTxt($record2, array_keys($record2)).out('n', 1));
    	fclose($log341);*/
    }

?>    