<?php
	require("../funciones/generales.php");
	require("../funciones/utilidades.php");

	date_default_timezone_set('America/Mexico_City');

    echo "Generacion de Archivo 510: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

    $sqlFechaConsulta = "SELECT date_add(CURDATE(), INTERVAL -20 DAY) as FechaConsulta";
    
    $rs = fn_ejecuta_query($sqlFechaConsulta);
    $fechaConsulta = $rs['root'][0]['FechaConsulta'];

    $sqlDistribuidorValido = "SELECT nombre FROM caGeneralesTbl WHERE tabla = 'funcion510' AND columna = 'DISTRIBUIDORES' ";

    $rs = fn_ejecuta_query($sqlDistribuidorValido);
    $distValidos = $rs['root'][0]['nombre'];

    $sqlEstatus01 = "SELECT nombre FROM caGeneralesTbl WHERE tabla = 'funcion510' AND columna = 'CDTOLCDANG' ";

    $rs = fn_ejecuta_query($sqlEstatus01);
    $estatus01 = $rs['root'][0]['nombre'];

    $sqlEstatus02 = "SELECT nombre FROM caGeneralesTbl WHERE tabla = 'funcion510' AND columna = 'CDSALCDAGS' ";

    $rs = fn_ejecuta_query($sqlEstatus02);
    $estatus02 = $rs['root'][0]['nombre'];

    $sqlEstatus03 = "SELECT nombre FROM caGeneralesTbl WHERE tabla = 'funcion510' AND columna = 'CDSFECDVER' ";

    $rs = fn_ejecuta_query($sqlEstatus03);
    $estatus03 = $rs['root'][0]['nombre'];

    $sqlTarifa = "SELECT idTarifa FROM catarifastbl WHERE TARIFA = '13' ";

    $rs = fn_ejecuta_query($sqlTarifa);
    $tEspacial = $rs['root'][0]['idTarifa'];


    $sqlUnidades =  "INSERT INTO alHistoricoUnidadesTmp (idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
    				"SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
                    " distribuidor, idTarifa, localizacionUnidad, claveChofer, 'PROCESO 510' as observaciones, usuario, ip ".
					"FROM alhistoricounidadestbl ".
					"WHERE centroDistribucion IN ('CDSAL','CDAGS','CDTOL','CDANG','CDSFE','CDVER')  ".
					"AND claveMovimiento = 'EP' ".
    				"AND distribuidor IN (".$distValidos.") ".
    				"AND (DATE(fechaEvento) BETWEEN '".$fechaConsulta."' AND curdate()) ".
    				"AND vin NOT IN (SELECT vin ".
					"				  FROM alTransaccionUnidadTbl ".
                    "				  WHERE tipoTransaccion = '510' ) ".
                    "AND vin NOT IN (SELECT vin FROM alHistoricoUnidadesTmp WHERE alhistoricounidadestmp.observaciones = 'PROCESO 510')".
                    "AND VIN NOT IN (SELECT vin FROM alinstruccionesmercedestbl)".
                    "AND idTarifa <> (".$tEspacial.") ";
    
    $rs = fn_ejecuta_query($sqlUnidades);
    //echo json_encode($rs);

    $sqlUnidades01 ="INSERT INTO alHistoricoUnidadesTmp (idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
    				" distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
    				"SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
                    " distribuidor, idTarifa, localizacionUnidad, claveChofer, 'PROCESO 510' as observaciones, usuario, ip ".
					"FROM alhistoricounidadestbl ".
					"WHERE centroDistribucion IN ('CDTOL','CDANG')  ".
					"AND claveMovimiento IN (".$estatus01.") ".    				 
    				"AND (DATE(fechaEvento) BETWEEN '".$fechaConsulta."' AND curdate()) ".
    				"AND vin NOT IN (SELECT vin ".
					"				  FROM alTransaccionUnidadTbl ".
                    "				  WHERE tipoTransaccion = '510' ) ".
                    "AND vin NOT IN (SELECT vin FROM alHistoricoUnidadesTmp WHERE observaciones = 'PROCESO 510') ".
                    "AND VIN NOT IN (SELECT vin FROM alinstruccionesmercedestbl)".
                    "AND idTarifa <> (".$tEspacial.") ";
    
    $rs = fn_ejecuta_query($sqlUnidades01);
    //echo json_encode($rs);

    $sqlUnidades02 = "INSERT INTO alHistoricoUnidadesTmp (idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
    				 " distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
    				 "SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
                     " distribuidor, idTarifa, localizacionUnidad, claveChofer,'PROCESO 510' as observaciones, usuario, ip ".
					 "FROM alhistoricounidadestbl ".
					 "WHERE centroDistribucion IN ('CDSAL','CDANG')  ".
					 "AND claveMovimiento IN (".$estatus02.") ".    				 
    				 "AND (DATE(fechaEvento) BETWEEN '".$fechaConsulta."' AND curdate()) ".
    				 "AND vin NOT IN (SELECT vin ".
					 "				  FROM alTransaccionUnidadTbl ".
                     "				  WHERE tipoTransaccion = '510' ) ".
                     "AND vin NOT IN (SELECT vin FROM alHistoricoUnidadesTmp WHERE alhistoricounidadestmp.observaciones = 'PROCESO 510') ".
                     "AND VIN NOT IN (SELECT vin FROM alinstruccionesmercedestbl)".
                     "AND idTarifa <> (".$tEspacial.") ";
    
    $rs = fn_ejecuta_query($sqlUnidades02);
    //echo json_encode($rs);

    $sqlUnidades03 ="INSERT INTO alHistoricoUnidadesTmp (idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
    				" distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ) ".
    				"SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, ".
                    " distribuidor, idTarifa, localizacionUnidad, claveChofer,'PROCESO 510' as observaciones, usuario, ip ".
					"FROM alhistoricounidadestbl ".
					"WHERE centroDistribucion IN ('CDSFE','CDVER')  ".
					"AND claveMovimiento IN (".$estatus03.") ".    				 
    				"AND (DATE(fechaEvento) BETWEEN '".$fechaConsulta."' AND curdate()) ".
    				"AND vin NOT IN (SELECT vin ".
					"				  FROM alTransaccionUnidadTbl ".
                    "				  WHERE tipoTransaccion = '510' ) ".
                    "AND vin NOT IN (SELECT vin FROM alHistoricoUnidadesTmp)".
                    "AND VIN NOT IN (SELECT vin FROM alinstruccionesmercedestbl)".
                    "AND idTarifa <> (".$tEspacial.") ";
    

    $rs = fn_ejecuta_query($sqlUnidades03);
    //echo json_encode($rs);    

    $sqlExistencia660 = "SELECT centroDistribucion, vin, distribuidor, 'Unidad no Existe en la 660' as observaciones ".
                        "FROM alhistoricounidadestmp ".
                        "WHERE vin NOT IN (SELECT vin FROM al660tbl WHERE scaccode = 'XTRA' ".
                        "  AND (dealerid <>'% %' OR dealerid IS NOT NULL) ".
                        "  AND (orisplc <>'% %' OR orisplc IS NOT NULL) ".
                        "  AND (dessplc <>'% %' OR dessplc IS NOT NULL) ".
                        "GROUP BY al660tbl.vin) ";

    $rs = fn_ejecuta_query($sqlExistencia660);
    
    if($rs['root']){
       
        $CentroDistribucion = $rs['root'][0]['centroDistribucion'];
        $UnidadVin = $rs['root'][0]['vin'];
        $distribuidor = $rs['root'][0]['distribuidor'];
        $observaciones = $rs['root'][0]['observaciones'];        

        //echo date("Y-m-d H:i:s");

        $dirPath = "C:\\carbook\\generacion_510\\logInexistentes\\";
        $fileName= "logInexistentes_".date("YmdHi").".txt";
        $filePath = $dirPath.$fileName;        

        $logInexistentes=fopen($filePath,"a") or die("Problemas Forest");
        //fputs($logInexistentes,"aqui inserte una linea poderosa");

        for($a=0; $a < count($rs[root]); $a++){
            $CentroDistribucion = $rs['root'][$a]['centroDistribucion'];
            $UnidadVin = $rs['root'][$a]['vin'];
            $distribuidor = $rs['root'][$a]['distribuidor'];
            $observaciones = $rs['root'][$a]['observaciones'];

            fputs($logInexistentes, $CentroDistribucion.",".$UnidadVin.",".$distribuidor.",".$observaciones."\n" );

            //$eliminarTemporal = "DELETE FROM alhistoricounidadestmp WHERE vin = '".$UnidadVin."'";                                    
        }
       
        fclose($logInexistentes);
        echo "Termino Log Unidades Inexistentes en 660: ".date("Y-m-d H:i", strtotime("now"))."<\br>";
    }
    else{

        echo"No Existen Unidades Inexistentes en la 660. " ."<\br>";
        $consultaImportacion = "SELECT A.simboloUnidad, C.tipoOrigenUnidad, B.distribuidor, B.vin ".
                           "  FROM alunidadestbl A , alhistoricounidadestmp B, casimbolosunidadestbl C ".
                           "  WHERE A.vin = B.vin ".
                           "    AND B.observaciones = 'PROCESO 510' ".
                           "    AND A.simboloUnidad = C.simboloUnidad "; 

        $rs = fn_ejecuta_query($consultaImportacion);
        //echo json_encode($rs);

        $simboloUnidad = $rs['root'][0]['simboloUnidad'];
        $tipoOrigen = $rs['root'][0]['tipoOrigenUnidad'];
        $distribuidor = $rs['root'][0]['distribuidor'];
        $extraeDistribuidor = substr($distribuidor,0,2);
        $unVin = $rs['root'][0]['vin'];  

        for($b=0; $b < count($rs['root']); $b++){
            $simboloUnidad = $rs['root'][$b]['simboloUnidad'];
            $tipoOrigen = $rs['root'][$b]['tipoOrigenUnidad'];
            $distribuidor = $rs['root'][$b]['distribuidor'];            
            if($extraeDistribuidor == 'M8'){
                echo($simboloUnidad.",");
                $sqlActualizaTipo = "UPDATE casimbolosunidadestbl SET tipoOrigenUnidad = 'I' WHERE simboloUnidad = '".$simboloUnidad."'" ;  
                $rs = fn_ejecuta_query($sqlActualizaTipo);
                //echo json_encode($rs);
            }
            elseif($tipoOrigen == 'E'){
                echo($tipoOrigen.",");
                $eliminarExportacion = "DELETE FROM alHistoricoUnidadesTmp WHERE vin '".$unVin."'";   
                $rs = fn_ejecuta_query($eliminarExportacion);
                //echo json_encode($rs);
            }
        } 
    }
    $sqlSA = "INSERT INTO al510tmp (vin,dealerId,oriSplc,routeDes) ".
             "select a6.vin,a6.dealerid, a6.orisplc,a6.routedes from (select a6.* from al660tbl a6, alhistoricounidadestmp h ".
             "where h.vin = a6.vin) as a6 ".
             "where (a6.vupdate = (select DISTINCT MAX(a6_1.vupdate) FROM (select a6_1.* from al660tbl a6_1, alhistoricounidadestmp h ".
             "Where h.vin = a6_1.vin) as a6_1 WHERE a6_1.vin = a6.vin) ".
             "and a6.vuptime = (select DISTINCT MAX(a6_1.vuptime) FROM (select a6_1.* from al660tbl a6_1, alhistoricounidadestmp h ".
             "where h.vin = a6_1.vin) as a6_1 WHERE a6_1.vin = a6.vin)) ".
             "and a6.scaccode = 'XTRA' ".
             "and a6.prodstatus <> 'SA' ".
             "and (a6.dealerid <> ' ' OR a6.dealerid IS NOT NULL) ".
             "and (a6.orisplc <>'% %' OR a6.orisplc IS NOT NULL) ".
             "and (a6.dessplc <>'% %' OR a6.dessplc IS NOT NULL) ".
             "group by a6.vin ";

            fn_ejecuta_query($sqlSA);

    $sqlNsa = "INSERT INTO al510tmp (vin,dealerId,oriSplc,routeDes) ".
              "select a6.vin,a6.dealerid, a6.orisplc,a6.routedes from (select a6.* from al660tbl a6, alhistoricounidadestmp h ".
              "where h.vin = a6.vin) as a6 ".
              "where (a6.vupdate = (select DISTINCT MAX(a6_1.vupdate) FROM (select a6_1.* from al660tbl a6_1, alhistoricounidadestmp h ".
              "Where h.vin = a6_1.vin) as a6_1 WHERE a6_1.vin = a6.vin) ".
              "and a6.vuptime = (select DISTINCT MAX(a6_1.vuptime) FROM (select a6_1.* from al660tbl a6_1, alhistoricounidadestmp h ".
              "where h.vin = a6_1.vin) as a6_1 WHERE a6_1.vin = a6.vin)) ".
              "and a6.scaccode = 'XTRA' ".
              "and a6.prodstatus = 'SA' ".
              "and (a6.dealerid <>'% %' OR a6.dealerid IS NOT NULL) ".
              "and (a6.orisplc <>'% %' OR a6.orisplc IS NOT NULL) ".
              "and (a6.dessplc <>'% %' OR a6.dessplc IS NOT NULL) ".
              "group by a6.vin ";

            fn_ejecuta_query($sqlNsa);             

            
            //echo json_encode($rs);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlSA;
                $a['successMessage'] = "Si Inserto WE";

                $sqlActualiza = "SELECT centroDistribucion,SUBSTRING(fechaEvento,1,10) as FechaMovimiento, SUBSTRING(fechaEvento,12,8) as horaMovimiento, ". 
                                "SUBSTRING((SELECT fechaEvento ".
                                "FROM alhistoricounidadestbl ".
                                "WHERE alhistoricounidadestbl.vin = alhistoricounidadestmp.vin ".
                                "AND alhistoricounidadestbl.claveMovimiento = 'EP'),1,10) as fechaEP, ".
                                "SUBSTRING((SELECT fechaEvento ".
                                "FROM alhistoricounidadestbl ".
                                "WHERE alhistoricounidadestbl.vin = alhistoricounidadestmp.vin ".
                                "AND alhistoricounidadestbl.claveMovimiento = 'EP'),12,8) as horaEP, claveMovimiento, vin  ".
                                "FROM alhistoricounidadestmp ";
                                
                                $rs = fn_ejecuta_query($sqlActualiza);

                //echo json_encode($rs);

                for($c=0; $c < count($rs['root']); $c++){
                    $cDistribucion = $rs['root'][$c]['centroDistribucion'];                    
                    $fechaEstatus = $rs['root'][$c]['FechaMovimiento'];
                    $horaEstatus = $rs['root'][$c]['horaMovimiento'];
                    $fechasEP = $rs['root'][$c]['fechaEP'];
                    $horasEP = $rs['root'][$c]['horaEP'];
                    $cMovimiento = $rs['root'][$c]['claveMovimiento'];
                    $nVin = $rs['root'][$c]['vin'];
                  
                    $sqlActualiza510 = " UPDATE al510tmp ".
                                       " SET fechaEvento = '".$fechaEstatus."', ".
                                       "     horaEvento = '".$horaEstatus."', ".
                                       "     fechaMovimientoEp = '".$fechasEP."', ".
                                       "     horaMovimientoEp = '".$horasEP."', ".
                                       "     claveMovimiento = '".$cMovimiento."', ".
                                       "     centroDistribucion = '".$cDistribucion."' ".
                                       " WHERE vin = '".$nVin."'";

                    fn_ejecuta_query($sqlActualiza510);                   
                }
            }
    $sqlExisteInterfaz = "SELECT * FROM alinterfasechryslertbl ".
                         " WHERE vin IN (SELECT vin FROM alhistoricounidadestmp WHERE observaciones = 'PROCESO 510') ";  

    $rs = fn_ejecuta_query($sqlExisteInterfaz);

    if($rs['root']){
        echo "Existen Unidades de Interfaz";
    }
    else{
        echo "no Existen Unidades de Interfaz";
    }      

?>
 