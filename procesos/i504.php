<?php
	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	include_once("../funciones/utilidades.php");	
	require_once("../funciones/utilidadesProcesos.php");
	require_once("../json/interfaz SICA.php");
	require_once("../funciones/generalesSICA.php");


	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['int504Hdn']){
        case 'valida':
            valida();
            break;
        case 'inserta':
            generaInterfase();
            break;
        default:
            echo '';
    }

	function valida(){
		$a = array('success' => true, 'msjResponse' => '', 'nombreArchivo'=>'', 'insertaInterface'=>0);
		
		$sql = "SELECT * FROM trInterfacesFechaTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					 " AND tipoInterface = 'A'".    				 # Anticipos de Gastos
					 " AND SUBSTRING(fechaInicio,1,10) = '".$_REQUEST['fechaInicio']."'".
					 " AND SUBSTRING(fechaFinal,1,10) = '".$_REQUEST['fechaInicio']."'";
		//echo "$sql<br>";
		$rsFecha = fn_ejecuta_query($sql);
		//var_dump($rsFecha);
		
		
		$sql = "SELECT COUNT(*) AS totalReg FROM trGastosViajeTractorTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					 " AND concepto in('7001','9032')".
					 " AND SUBSTRING(fechaEvento,1,10) = '".$_REQUEST['fechaInicio']."'".
					 " AND transmitido is null ".
					 " AND claveMovimiento NOT IN ('GX','GT')";    			# GX=Cancelado GT=Transmitido"
		//echo "$sql<br>";
		$rsGastos = fn_ejecuta_query($sql);
		//var_dump($rsGastos);
		
		$falta = (empty($rsGastos['root'][0]['totalReg']))?0:$rsGastos['root'][0]['totalReg'];
		//var_dump($falta);
		
		if(($rsFecha['root'][0]['estatus'] == 'G' || $rsFecha['root'][0]['estatus'] == 'T') && $falta == 0)		# Generado o Trasmitido a Contabilidad
		{
				$a['success'] = false;
				$a['msjResponse'] = 'Interfase ya generada para esa fecha.';
		}
		
		if($a['success'])
		{
				if(($rsFecha['root'][0]['estatus'] == 'G' || $rsFecha['root'][0]['estatus'] == 'T') && $falta > 0)		# Generado o Trasmitido a Contabilidad
				{
						//$a['success'] = false;
						$a['insertaInterface'] = 0;
						$a['msjResponse'] = 'Interfase para esa fecha ya generada, pero hay registros sin acumular.';
				}				
		}
		
		if($a['success'])
		{
				if($falta == 0)					# Generado
				{
						$a['success'] = false;
						//$a['msjResponse'] = 'No existen datos con ese criterio.';
						$a['msjResponse'] = 'No existe informaci&oacuten a transmitir para esa fecha.';
				}
				else
				{
						$a['insertaInterface'] = 1;
				}
		}

		if($a['success'])
		{
				setlocale(LC_TIME, 'spanish');
				$mes = (date("n", strtotime($_REQUEST['fechaInicio'])));
				$nombreMes = strftime("%B",mktime(0, 0, 0, $mes));
				
				$aux = explode('-', $_REQUEST['fechaInicio']);
				$anio = $aux[0];				
				$mes = $aux[1];
				$dia = $aux[2];
				
				$a['nombreArchivo'] = str_pad($_REQUEST['tipo'], 2, "0", STR_PAD_LEFT).str_pad($dia, 2, "0", STR_PAD_LEFT).strtoupper(substr($nombreMes,0,3)).str_pad($anio, 4, " ", STR_PAD_LEFT).'.ANT';
		}
		
		echo json_encode($a);
	}
		
	function generaInterfase(){
		$a = array('success' => true, 'msjResponse' => 'Interfase generada correctamente.', 'nombreArchivo'=>'', 'existeTmp'=>0);
		
		$dir 	= "E:/carbook/i504/respArchivo/";
		$arch = $_REQUEST['nombreArchivo'].'.nvo';
		
		$sql = "SELECT cuentaContable, tipoCuenta FROM caConceptosCentrosTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['i504RemesaCmb']."'".
					 " AND concepto in('7001','9032')";
		//echo "$sql<br>";
		$rsCtas = fn_ejecuta_query($sql);
		//var_dump($rsCtas);		

		for ($i=0; $i <sizeof($rsCtas['root']); $i++) { 
			$selCuentaSica="SELECT * FROM conCatCuentasTbl WHERE cuenta ='".$rsCtas['root'][$i]['cuentaContable']."';";
			$rsCuenta=fn_ejecuta_query_SICA($selCuentaSica);
			

			if (sizeof($rsCuenta['root'])==0) {
				//$arrCuenta[]=$rsCtas['root'][$i]['cuentaContable'];
				$selNombre="SELECT concat(apellidoMaterno,' ', apellidoPaterno,' ', nombre) as nombre FROM caChoferesTbl where claveChofer='".substr($rsCtas['root'][$i]['cuentaContable'],-5)."'";
				$rsNombre=fn_ejecuta_query($selNombre);

				$insertCuenta="INSERT INTO conCatCuentasTbl (claveCompania, cuenta, nombre, afectable, tipo, naturaleza, ubicacion, reportar, manejaSegmentos, numeroSegmentos, divisa, 	codigoAgrupador, RFC, estatus, idUsuarioAct, fechaAct, ipAct, macAddressAct) 
							VALUES ('4', '".$rsCtas['root'][$i]['cuentaContable']."', 
										'".$rsNombre['root'][0]['nombre']."', '1', '1', '1', ' ', '1', '0', '0', 'MXN', ' ', ' ', '1', '1', now(), '10.212.134.204', '10.212.134.204') ";
				$rsIns=fn_ejecuta_query_SICA($insertCuenta);

				$sqlCuenta= "SELECT * FROM  conCatCuentasTbl where cuenta='".$rsCtas['root'][$i]['cuentaContable']."'";
				$rsCuenta=fn_ejecuta_query_SICA($sqlCuenta);

				for ($j=1; $j <= 12; $j++) { 
					$rsConSaldos="INSERT INTO conSaldosTbl (claveCompania, tipo, calendario, periodo, idCuentaContable, idCentroCostos, saldoInicial, cargos, abonos, saldoFinal) ".
								"VALUES ('4', '1', year(now()), '".$j."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '0.00', '0.00', '0.00', '0.00')";
					$rsIns=fn_ejecuta_query_SICA($rsConSaldos);
				}

			}else{

			}
		}
		
		$ind = 0;
		$totabo = 0;
		
		$sql = "SELECT * FROM trGastosViajeTractorTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['i504RemesaCmb']."'".
					 " AND concepto in('7001','9032')".
					 " AND SUBSTRING(fechaEvento,1,10) = '".$_REQUEST['i504DiaDtm']."'".
					 " AND claveMovimiento NOT IN ('GX','GT')";    			# GX=Cancelado GT=Transmitido"
		//echo "$sql<br>";		
		$rsGastos = fn_ejecuta_query($sql);
		//var_dump($rsGastos);		
		
		$idx = 0;
		$a['nombreArchivo'] = $dir.$arch;
		$fp = fopen($a['nombreArchivo'], 'w');
		foreach($rsGastos['root'] AS $index => $row)
		{
				$aux = explode('-',substr($row['fechaEvento'],0,10));
				$anio = $aux[0];				
				$mes = $aux[1];
				$dia = $aux[2];
				$fecha = $dia.'/'.$mes.'/'.$anio;
				
				$ctacon = rtrim($rsCtas['root'][0]['cuentaContable']);
				$cvecia = 4;
				$anio 	= substr($row['fechaEvento'],0,4);
				$mes 		= substr($row['fechaEvento'],5,2);
				$fecpol	= $fecha;

				$sql = "SELECT ch.cuentaContable FROM trViajesTractoresTbl vt, caChoferesTbl ch ".
							" WHERE vt.idViajeTractor = ".$row['idViajeTractor'].
							//" AND vt.centroDistribucion = '".$_REQUEST['i504RemesaCmb']."'".
							" AND ch.claveChofer = vt.claveChofer";
				//echo "$sql<br>";
				$rsCtaChof = fn_ejecuta_query($sql);
				//var_dump($rsCtaChof);
				
				$ctacon = armaCuentaContable(rtrim($rsCtas['root'][0]['cuentaContable']), rtrim($rsCtaChof['root'][0]['cuentaContable']), '0');
				//var_dump($ctacon);
				$numpol = str_pad('1', 6, "0", STR_PAD_LEFT);
				$tippol = $_REQUEST['tipo'];
				$numche = 0;
				$cargo = '0.00';
				$abono = $row['importe'];
				if(rtrim($rsCtas['root'][0]['tipoCuenta']) == 'C')
				{
						$cargo = $row['importe'];
						$abono = '0.00';
				}
				$desmov = 'FOLIO DE RECIBO '.str_pad($row['folio'], 6, "0", STR_PAD_LEFT);
				$tippro = 0;
				$ind++;
				$orden = $ind;
				
				fputs($fp, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
				$idx++;
				/*
        if($idx != $rsGastos['records'])
        {
        		fputs($fp,"\n");
      	}
      	*/
      	fputs($fp,"\n");
				
				$totabo = $totabo + $row['importe'];				
		}
				
		#### Inserta cuenta de Abono (BANCOS) ####		
		$sql = "SELECT cuentaContable, tipoCuenta FROM caConceptosCentrosTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['i504RemesaCmb']."'".
					 " AND concepto = '".$_REQUEST['numero']."'";
		//echo "$sql<br>";
		$rsCtas = fn_ejecuta_query($sql);
		//var_dump($rsCtas);
		
		$ind++;
		$orden = $ind;
		$ctacon = rtrim($rsCtas['root'][0]['cuentaContable']);
		$cargo = '0.00';
		$abono = $totabo;
		if(rtrim($rsCtas['root'][0]['tipoCuenta']) == 'C')
		{
				$cargo = $totabo;
				$abono = '0.00';
		}
		$desmov = 'ANTICIPOS DE VIAJE';
		fputs($fp, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
		
		fclose($fp);
		
		//Verifica si existe el directorio tmp, sino lo crea
		if (!file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				mkdir('../../carbookv1.2/modules/interfacesContables/tmp/', 0777);
		}
		if (file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				copy($a['nombreArchivo'], '../../carbookv1.2/modules/interfacesContables/tmp/'.$arch);
				$a['existeTmp'] = 1;
		}
				
		//Se realiza la actualización de la interfase contable		
		foreach($rsGastos['root'] AS $index => $row)
		{
				$sql = "UPDATE trGastosViajeTractorTbl".
							 " SET transmitido = 'GT'".					# Transmitido
							 " WHERE centroDistribucion = '".$row['centroDistribucion']."'".
							 " AND idViajeTractor = ".$row['idViajeTractor'].
							 " AND SUBSTRING(fechaEvento,1,10) = '".substr($row['fechaEvento'],0,10)."'".
							 " AND concepto = '".$row['concepto']."'".
							 " AND folio = '".$row['folio']."'".
							 " AND claveMovimiento = '".$row['claveMovimiento']."'";
				//echo "$sql<br>";
				fn_ejecuta_query($sql);	 		//CHK
				
				if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
	      {
	         $a['success']		 = false;
	         $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten del Viaje.";
	         $a['sql'] = $sql;
	         break;
	      }
		}
		
		if($_REQUEST['insertaInterface'] == 1)
		{
				setlocale(LC_TIME, 'spanish');
				$horaActual = date('H:i:s');
				$sql = "INSERT INTO trInterfacesFechaTbl (centroDistribucion, tipoInterface, fechaInicio, fechaFinal, estatus) VALUES (".
								"'".$_REQUEST['i504RemesaCmb']."', ".
								"'A', ".
								//"NULL, ".
								"'".($_REQUEST['i504DiaDtm'].' '.$horaActual)."', ".
								"'".($_REQUEST['i504DiaDtm'].' '.$horaActual)."', ".
								"'G')";
				//echo "$sql<br>";
				fn_ejecuta_query($sql);	 		//CHK
				if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
	      {
	         $a['success']		 = false;
	         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
	         $a['sql'] = $sql;
	      }								
		}


		$patio=$_REQUEST['i504RemesaCmb'];
		$dia=$_REQUEST['i504DiaDtm'];
		$tipo=$_REQUEST['tipo'];
		$numero=$_REQUEST['numero'];
		$tipoPoliza='G';

		generaInterfaseSICA($patio,$dia,$tipo,$numero,$tipoPoliza);


		$selTipoPoliza="SELECT * FROM conTiposPolizasTbl WHERE claveCompania=4  AND tipoPoliza=".$tipo."";
		$rsTipoPoliza=fn_ejecuta_query_SICA($selTipoPoliza);

		// echo json_encode($selTipoPoliza);

		$sqlFolio="select * from conFoliosPolizasTbl ".
					"where anio='".$anio."' ".
					"AND claveCompania='4' ".
					"and mes='".$mes."' ".
					"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
		$rsFolio=fn_ejecuta_query_SICA($sqlFolio);


		$folio1=$folio=$rsFolio['root'][0]['folio'] -1;
		$a['msjResponse'] = "Interfase generada correctamente. Folio Poliza SICA  ".$folio1." .";

		// fn_ejecuta_query(false);	//CHK
		// fn_ejecuta_query_SICA(false);	//CHK


     echo json_encode($a);
	}
?>
