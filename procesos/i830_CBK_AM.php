<?php
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

   i830_AM($vin,$clave);

	function i830_AM($vin,$clave){
	    $startDate = date('Y-m-d', strtotime("-360 days")); //- 7 días
	    $today = date('Y-m-d');
	    $todayDateTime = date('Y/m/d H:i:s');
			//echo "inicio i830".$todayDateTime;

	    $p01Count = 0; //Contador de unidades con clave de movimiento 'AM'
	    $d04Count = 0; //Contador de unidades con clave de movimiento 'OM'

	    $tipoTransmision=$_REQUEST['tipoTransmision'];
        $centroDistribucion=$_REQUEST['centroDistribucion'];
        $vin1= $vin;


        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");
        $vin=str_ireplace($buscar,$reemplazar,$vin1);

        $cadena = chunk_split($vin, 17,"','");
        
        $vines=substr($cadena,0,-2);

        $tipoMvo= $clave;

	    //$unidadesSin660 = getUnidadesSin660($startDate, $today);
	    $unidades = busquedaUnidades1($startDate, $today,$vines,$tipoMvo);

	    //Conteo de unidades AM-P01, OM-D04
	    foreach ($unidades as $unidad) {
	    	if ($unidad['claveMovimiento'] == 'AM')
		    	$p01Count++;
		    else //OM
		    	$d04Count++;
	    }

	    //Se genera archivo de unidades que no están en la 660
	    //createLogSin660($unidadesSin660, "RAT660.FAL");

	    //Ordena las Unidades con 660 por Fecha y Tractor para creación de Logs *No sé si sea mejor ordenar en el query;
	    //usort($unidades, 'sortByFechaYTractor');

	    //Se genera archivo de unidades que están en la 660
	    createArchivoCon6601($unidades);

	    //Se genera Log de Avisos de Proceso y Cifras de Control, éste siempre se ejecuta...
	    //createLogProceso($p01Count, $d04Count, $todayDateTime);

	    //Se genera Log de Cifras de Control, sólo se ejecuta si hubo movimientos...
	    //createLogProceso2($p01Count, $d04Count, $todayDateTime);

	    //Se Afecta la BD insertando las unidades en alTransaccionUnidadTbl
	    insertTransacciones1($unidades);

	    if (isset($unidades) && count($unidades) > 0)
	    	updFolio1();
	}

//-----FIN TRAP830------------------------------------------------------------------------------------------------------
    /*
    * DECLARACIÓN Y DEFINICION DE FUNCIONES PARA LA INTERFAZ TRAP830
    *
    */
    //Obtiene un folio de parámetros de Sistema...
    function getFolio1() {
    	//Obtiene el Folio de parametros del sistem
    	$sqlGetFolio = "SELECT f.folio FROM trFoliosTbl f ".
    				   "WHERE f.centroDistribucion = 'TCO' ".
    				   "AND f.compania = '830' ".
    				   "AND f.tipoDocumento = 'T';";

    	$rsFolio = fn_ejecuta_query($sqlGetFolio);

    	$folio = $rsFolio['root'][0]['folio'];

    	if ($folio=='1') {
    		$sqlUpdFolio = "UPDATE trFoliosTbl f set folio='9999' ".
    				   "WHERE f.centroDistribucion = 'TCO' ".
    				   "AND f.compania = '830' ".
    				   "AND f.tipoDocumento = 'T';";

    		$rsFolio = fn_ejecuta_query($sqlUpdFolio);    		
    	}

    	return $folio;
    }
    //Actualiza(autoincrementa) el Folio de parámetros de Sistema...
    function updFolio1() {
    	//Si no existió ningún tipo de error actualiza el folio
    	if (!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {

    		$folio = getFolio1();
	    	$sqlUpdFolio = "UPDATE trFoliosTbl ".
	    				   "SET folio = '".(intval($folio) - 1)."' ".
	    				   "WHERE centroDistribucion = 'TCO' ".
	    				   "AND compania = '830' ".
	    				   "AND tipoDocumento = 'T';";

	    	fn_ejecuta_query($sqlUpdFolio);
    	}
    }

    function getUnidadesSin6601($startDate, $endDate) {

    	$sqlExcluir660 = "(SELECT a6.vin FROM al660Tbl a6)";

    	$sqlHistoricoSin660 = "SELECT hu2.* FROM alHistoricoUnidadesTbl hu2 ".
			    				"WHERE hu2.idTarifa <> (SELECT t.idTarifa FROM caTarifasTbl t WHERE t.tarifa = '13') ".
			    				"AND hu2.claveMovimiento IN ('AM', 'OM') ".
			    				"AND hu2.centroDistribucion IN ('CDTOL', 'CDSAL', 'CDAGS', 'CDSFE', 'CDANG') ".
			    				"AND DATE(hu2.fechaEvento) BETWEEN '$startDate' AND '$endDate' ".
			    				//"AND DATE(hu2.fechaEvento) BETWEEN '2017-10-01' AND '2017-10-31' ".
			                    "AND hu2.vin NOT IN $sqlExcluir660;";

	    $rsUnidadesSin660 = fn_ejecuta_query($sqlHistoricoSin660);

	    return $rsUnidadesSin660['root'];
    }

	function busquedaUnidades1($startDate, $endDate,$vines,$tipoMvo) {		

		if ($tipoMvo=='AM') {
			$sqlHistorico660 = "SELECT hu.vin,
									    adddate(hu.fechaevento,
									        interval 2 minute) as fechaEvento,
									    hu.claveMovimiento,
									    hu.distribuidor as disUnidad,
									    hu.centroDistribucion,    
									    e.folioTimbrado,
									    f.viaje,
									    g.tractor ".
							"FROM  alhistoricounidadestbl hu,
								    alunidadestbl tr,
								    casimbolosunidadestbl su,    
								    trunidadesdetallestalonestbl d,
								    trtalonesviajestbl e,
								    trviajestractorestbl f,
								    catractorestbl g ".
							"WHERE  hu.claveMovimiento in ('AM') ".
							//"AND hu.fechaEvento BETWEEN cast('2019-06-04' as date) AND cast('2020-06-16' as date) ".
							"AND hu.vin = tr.vin ".
					        "AND hu.vin = d.vin ".
					        "AND e.idtalon = d.idtalon ".
					        "AND f.idViajeTractor = e.idViajeTractor ".
					        "AND f.idtractor = g.idtractor ".
					        "AND hu.idtalon = e.idtalon ".
					        "AND d.estatus != 'C' ".
					        "AND e.claveMovimiento != 'TX' ".
							"AND hu.centroDistribucion IN ('CDSAL','CDTOL','CDLZC','CDAGS','CDVER','CDSFE','CDANG') ".
							"AND tr.vin = hu.vin ".
							"AND hu.vin in ('".$vines.") ".
							"AND tr.simboloUnidad = su.simboloUnidad ".
							"AND su.marca not in('KI','HY') ".
							"AND hu.idTarifa not in (SELECT tar.idTarifa  FROM catarifastbl tar WHERE tar.tarifa = '13') ".
							"AND hu.fechaEvento =(SELECT max(h2.fechaevento) from alhistoricounidadestbl h2 where hu.vin=h2.vin and hu.claveMovimiento=h2.claveMovimiento) ";
							//"AND tr.vin not in(SELECT rv.vin FROM altransaccionunidadtbl rv WHERE rv.tipoTransaccion = 'P01' AND cast(rv.fechaMovimiento as date)= cast(hu.fechaEvento as date)) ".
							/*"UNION ".
							"SELECT hu.vin, adddate(hu.fechaevento , interval 1 minute) as fechaEvento,hu.claveMovimiento, hu.distribuidor as disUnidad, hu.centroDistribucion ".
							"FROM alhistoricounidadestbl hu, alunidadestbl tr, casimbolosunidadestbl su ".
							"WHERE  hu.claveMovimiento in ('RP') ".
							//"AND hu.fechaEvento BETWEEN cast('2019-06-04' as date) AND cast('2020-06-16' as date) ".
							"AND hu.centroDistribucion IN ('CDSAL','CDTOL','CDLZC','CDAGS','CDVER','CDSFE','CDANG')  ".
							"AND tr.vin = hu.vin  ".
							"AND hu.vin in ('".$vines.") ".
							"AND tr.simboloUnidad = su.simboloUnidad  ".
							"AND su.marca not in('KI','HY')  ".
							///"AND hu.idTarifa not in (SELECT tar.idTarifa  FROM catarifastbl tar WHERE tar.tarifa = '13')  ".
							"AND hu.fechaEvento =(SELECT max(h2.fechaevento) from alhistoricounidadestbl h2 where hu.vin=h2.vin and hu.claveMovimiento=h2.claveMovimiento) ";*/
							//"AND tr.vin not in(SELECT rv.vin FROM altransaccionunidadtbl rv WHERE rv.tipoTransaccion = 'P01' AND cast(rv.fechaMovimiento as date)= cast(hu.fechaEvento as date))";

			$rsUnidades660 = fn_ejecuta_query($sqlHistorico660);
			//echo $sqlHistorico660."<br><br><br>";
			//echo json_encode($rsUnidades660);
		}else if ($tipoMvo=='RP') {
			$sqlHistorico660 = /*"SELECT hu.vin, adddate(hu.fechaevento , interval -1 minute) as fechaEvento,hu.claveMovimiento, hu.distribuidor as disUnidad, hu.centroDistribucion ".
							"FROM alhistoricounidadestbl hu, alunidadestbl tr, casimbolosunidadestbl su ".
							"WHERE  hu.claveMovimiento in ('AM') ".
							//"AND hu.fechaEvento BETWEEN cast('2019-06-04' as date) AND cast('2020-06-16' as date) ".
							"AND hu.centroDistribucion IN ('CDSAL','CDTOL','CDLZC','CDAGS','CDVER','CDSFE','CDANG') ".
							"AND tr.vin = hu.vin ".
							"AND hu.vin in ('".$vines.") ".
							"AND tr.simboloUnidad = su.simboloUnidad ".
							"AND su.marca not in('KI','HY') ".
							"AND hu.idTarifa not in (SELECT tar.idTarifa  FROM catarifastbl tar WHERE tar.tarifa = '13') ".
							"AND hu.fechaEvento =(SELECT max(h2.fechaevento) from alhistoricounidadestbl h2 where hu.vin=h2.vin and hu.claveMovimiento=h2.claveMovimiento) ";
							//"AND tr.vin not in(SELECT rv.vin FROM altransaccionunidadtbl rv WHERE rv.tipoTransaccion = 'P01' AND cast(rv.fechaMovimiento as date)= cast(hu.fechaEvento as date)) ".
							"UNION ".*/
							"SELECT hu.vin, adddate(hu.fechaevento , interval -2 minute) as fechaEvento,hu.claveMovimiento, hu.distribuidor as disUnidad, hu.centroDistribucion ".
							"FROM alhistoricounidadestbl hu, alunidadestbl tr, casimbolosunidadestbl su ".
							"WHERE  hu.claveMovimiento in ('RP') ".
							//"AND hu.fechaEvento BETWEEN cast('2019-06-04' as date) AND cast('2020-06-16' as date) ".
							"AND hu.centroDistribucion IN ('CDSAL','CDTOL','CDLZC','CDAGS','CDVER','CDSFE','CDANG')  ".
							"AND tr.vin = hu.vin  ".
							"AND hu.vin in ('".$vines.") ".
							"AND tr.simboloUnidad = su.simboloUnidad  ".
							"AND su.marca not in('KI','HY')  ".
							///"AND hu.idTarifa not in (SELECT tar.idTarifa  FROM catarifastbl tar WHERE tar.tarifa = '13')  ".
							"AND hu.fechaEvento =(SELECT max(h2.fechaevento) from alhistoricounidadestbl h2 where hu.vin=h2.vin and hu.claveMovimiento=h2.claveMovimiento) ";
							//"AND tr.vin not in(SELECT rv.vin FROM altransaccionunidadtbl rv WHERE rv.tipoTransaccion = 'P01' AND cast(rv.fechaMovimiento as date)= cast(hu.fechaEvento as date))";

			$rsUnidades660 = fn_ejecuta_query($sqlHistorico660);
			//echo $sqlHistorico660."<br><br><br>";
			//echo json_encode($rsUnidades660);
		}

		

		return $rsUnidades660['root'];

	}

	function createArchivoCon6601($unidades) {
		//echo json_encode($unidades);
		//echo "contadore unidades".sizeof($unidades);

		if (!isset($unidades) || count($unidades) < 1)
			return;
		/*El nombre del archivo es un texto de 12 caracteres
    	 *pos 0 valor fijo : 'IGSC'  												Longitud: 4 caracteres
    	 *pos 4 folio : USING(&&&&) *Valor autoincremental y     					Longitud: 4 caracteres
    	 *							es actualizado cada que sea utilizado
    	 *							siempre y cuando existan movimentos
    	 *							Viene de caGeneralesTbl
    	 *pos 8 valor fijo : '.R41'													Longitud: 4 caracteres
    	 */
		$folio = getFolio1();
    	$fileName = "IGSC".sprintf("%04d", $folio).".R41";
		//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir = "C:/carbook/i830_CB/".$fileName;
		$fileDir = fopen("C:/carbook/i830_CB//".$fileName, 'a') or die('No se pudo generar Reporte');
		$todayDateTime = date_create(date('Y/m/d H:i:s'));
		$contador=0;

		$encabezado =  array(
							 1 => 'IGR41',
							 6 => date_format($todayDateTime, 'm'),
							 8 => date_format($todayDateTime, 'd'),
							10 => date_format($todayDateTime, 'y'),
							12 => date_format($todayDateTime, 'H'),
							14 => date_format($todayDateTime, 'i'),
							16 => sprintf('%06d', (count($unidades) )),
							22 => 'IGSC',
							26 => sprintf('%04d', $folio),
							30 => '.R41'
							);

		fwrite($fileDir, getTxt21($encabezado).out('n', 1));


		//foreach ($unidades as $unidad=> $unidad) {	
		
		for ($i=0; $i <sizeof($unidades)  ; $i++) { 

			

			$fechaEvento = date_create($unidades[$i]['fechaEvento']);

			//Incrementa los contadores ya sea AM o OM
			if ($unidades[$i]['claveMovimiento'] == 'CO' || $unidades[$i]['claveMovimiento'] == 'EO' || $unidades[$i]['claveMovimiento'] == 'EH' || $unidades[$i]['claveMovimiento'] == 'FC'  || $unidades[$i]['claveMovimiento'] == 'PR' || $unidades[$i]['claveMovimiento'] == 'UE' || $unidades[$i]['claveMovimiento'] == 'SP' || $unidades[$i]['claveMovimiento'] == 'US' || $unidades[$i]['claveMovimiento'] == 'AM') {
				$p01Count++;
			} else /*if($unidades[$i]['claveMovimiento'] == 'OM')*/ {
				$d04Count++;
			}

			/*$sqlGetSplc = "SELECT splcCode ".
									"FROM casplctbl ".
									"WHERE patioCode = '".$unidades[$i]['disTalon']."';";

		 	$rsGetSplc = fn_ejecuta_query($sqlGetSplc);*/
		 	//echo json_encode($unidades[$i]['claveMovimiento']);

			 /*if($unidades[$i]['claveMovimiento'] == 'UE'){
				 $splcUnd = '922786720';
			 }else{
				 $splcUnd= '922786000';
			 }*/

			 if ($unidades[$i]['centroDistribucion'] == 'CDTOL') { 
			 	$splcUnd = '958770000';
			 }

			 if ($unidades[$i]['centroDistribucion'] == 'CDVER') {
			 	$splcUnd = '979792000';
			 }

			 if ($unidades[$i]['centroDistribucion'] == 'CDLZC') {
			 	$splcUnd = '948218000';
			 }

			 if ($unidades[$i]['centroDistribucion'] == 'CDSFE') {
			 	$splcUnd = '922000999';
			 }
			 if ($unidades[$i]['centroDistribucion'] == 'CDSAL') {
			 	$splcUnd = '922786000';
			 }
			 if ($unidades[$i]['centroDistribucion'] == 'CDAGS') {
			 	$splcUnd = '940720000';
			 }
			 if ($unidades[$i]['centroDistribucion'] == 'CDANG') {
			 	$splcUnd = '922000333';
			 }

			 	

			if ($unidades[$i]['claveMovimiento'] == 'RP'){
			 	$claveMovimiento='D04';
			 	$fechaEvento=date_create($unidades[$i]['fechaEvento']);
			 	$contador ++;

			 	$registro =  array(
							 1 => $unidades[$i]['talon'],
							 9 => out('s', 7),
							16 => $unidades[$i]['vin'],
							33 => date_format($fechaEvento, 'm'),
							35 => date_format($fechaEvento, 'd'),
							37 => date_format($fechaEvento, 'y'),
							39 => date_format($fechaEvento, 'H'),
							41 => date_format($fechaEvento, 'i'),
							//43 => getTransaccion($unidades[$i]['claveMovimiento']),
							43 => $claveMovimiento,
							46 => out('s', 19),
							65 => sprintf('%-9s',$splcUnd),
							70 => out('s', 23),
							93 => $unidades[$i]['tractor'],
						   101 => out('s', 12)
							);

				fwrite($fileDir, getTxt21($registro).out('n', 1));
				//echo "2  ";
				

			 }

			 if ($unidades[$i]['claveMovimiento'] == 'AM'){
			 	$claveMovimiento='P01';
			 	$fechaEvento=date_create($unidades[$i]['fechaEvento']);
			 	$contador ++;

			 		$registro =  array(
								 1 => $unidades[$i]['folioTimbrado'],
								 2 => '     ',
								 7 => $unidades[$i]['vin'],
								24 => date_format($fechaEvento, 'm'),
								26 => date_format($fechaEvento, 'd'),
								28 => date_format($fechaEvento, 'y'),
								30 => date_format($fechaEvento, 'H'),
								32 => date_format($fechaEvento, 'i'),
								//43 => getTransaccion($unidades[$i]['claveMovimiento']),
								34 => $claveMovimiento,
								37 => sprintf('%-9s',$splcUnd),
								38 => out('s', 11),
								48 => $unidades[$i]['disUnidad'],
								49 => out('s', 27),
								50 => $unidades[$i]['tractor'],
							    51 => out('s', 7),
							   52 => sprintf('%-3s',$unidades[$i]['viaje']),
							   53 => out('s', 7)
								);

				fwrite($fileDir, getTxt21($registro).out('n', 1));

				//echo "5  ";

			 }

		}
		$trailer = array(
						 1 => 'EOF',
						 2 => sprintf('%06d', (count($contador) ))
						);
		fwrite($fileDir, getTxt21($trailer).out('n', 1));

		fclose($fileDir);

		//subirFtp_Comodato($fileName);
	}
	function createLogSin6601($unidades, $fileName) {

		if (!isset($unidades) || count($unidades) < 1)
			return;

		//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir = "C:/carbook/i830_CB//".$fileName;
		$fileDir = fopen("C:/carbook/i830_CB//".$fileName, 'a') or die('No se pudo generar Reporte');
		foreach ($unidades as $unidad) {
			fwrite($fileDir, "NO EXISTE trat660 |".$unidad['vin']." |   |". substr($unidad['vin'], 9).out('n', 1));
		}
		fclose($fileFir);

	}
	//Log de Avisos de Proceso y Cifras de Control, éste siempre se ejecuta...
	function createLogProceso1($p01Count, $d04Count, $todayDateTime) {
		/* NOMBRE DE ARCHIVO DE 12 CARACTERES + año mes dia hora minuto segundo .log
		*  IGSCffff.R41aaaammddhhmmss.log
		*  Se crea haya o no movimientos
		*/
		$todayDateTime = date_create($todayDateTime);
		$fechaInicio = date_format($todayDateTime, 'd/m/Y');
		$tiempoInicio = date_format($todayDateTime, 'H:i:s');
		$fileName =  "IGSCffff.R41".date("Ymdhis");

		//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir = "C:/carbook/i830_CB//".$fileName;
		$fileDir = fopen("C:/carbook/i830_CB//".$fileName, 'a') or die('No se pudo generar Reporte');

		fwrite($fileDir, "=============================================================>".out('n',1));
		fwrite($fileDir, "Inicio...........|".$fechaInicio."|<a>|".$tiempoInicio."|".out('n',1));
		fwrite($fileDir, "Archivo generado:|IGSCffff.R41 |||".out('n',1));
		fwrite($fileDir, "Movtos P01:  |".$p01Count."   |||".out('n',1));
		fwrite($fileDir, "Movtos D04:  |".$d04Count."   |||".out('n',1));
		fwrite($fileDir, "Movtos generados:|".($p01Count + $d04Count)."   |||".out('n',1));
		$tiempoFin = date('H:i:s');
		fwrite($fileDir, "Termino..........|".date('d/m/Y')."|<a>|".$tiempoFin."|".out('n',1));
		fwrite($fileDir, "=============================================================>".out('n',1));

		fclose($fileDir);

	}
	//Log de Cifras de Control, sólo se ejecuta si hubo movimientos...
	function createLogProceso21($p01Count, $d04Count, $todayDateTime) {
		/* NOMBRE DE ARCHIVO DE 12 CARACTERES
		*  IGSCffff.R41.log
		*  Solo se ejecuta si hay movimientos
		*/

		if ($p01Count + $d04Count == 0)
			return;

		$todayDateTime = date_create($todayDateTime);
		$fechaInicio = date_format($todayDateTime, 'd/m/Y');
		$tiempoInicio = date_format($todayDateTime, 'H:i:s');
		$fileName =  "IGSCffff.R41.log";

		//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
		//$fileDir = "C:/carbook/i830_CB//".$fileName;
		$fileDir = fopen("C:/carbook/i830_CB//".$fileName, 'a') or die('No se pudo generar Reporte');

		fwrite($fileDir, "=============================================================>".out('n',1));
		fwrite($fileDir, "Inicio...........|".$fechaInicio."|<a>|".$tiempoInicio."|".out('n',1));
		fwrite($fileDir, "Movtos generados:|".($p01Count + $d04Count)."   |||".out('n',1));
		$tiempoFin = date('H:i:s');
		fwrite($fileDir, "Termino..........|".date('d/m/Y')."|<a>|".$tiempoFin."|".out('n',1));
		fwrite($fileDir, "=============================================================>".out('n',1));

		fclose($fileDir);

	}
	function insertTransacciones1($unidades) {
		//echo json_encode($unidades);
		//echo "contador".count($unidades);

		if (!isset($unidades) || count($unidades) == 0)
			return;

		$folio = getFolio1();

		$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl ".
								"(tipoTransaccion, centroDistribucion, folio, vin, ".
								"fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, ".
								"prodStatus, fecha, hora) ".
								"VALUES";

		for ($i = 0; $i < count($unidades); $i++) {
			if ($i > 0)
				$sqlInsertTransaccion .= ", ";

			$unidad = $unidades[$i];
			//echo count($unidad);
			//echo $i;
			//Fomatea la fecha y hora proveniente de la 660
			$vupdateParse = date_format(DateTime::createFromFormat('d/m/Y', $unidad['vupdate']), 'Y/m/d');
			$vuptimeParse = substr($unidad['vuptime'], 0, 5);
			//echo $vupdateParse;
			//echo $vuptimeParse;

			if($unidad['claveMovimiento']=='AM'){
				//echo "entra 1 p01  ";
				$sqlInsertTransaccion .= "('P01',".
					 "'".$unidad['centroDistribucion']."', ".
					 "'".$folio."', ".
					 "'".$unidad['vin']."', ".
					 "'".date('Y-m-d H:i:s')."', ".
					 "'".$unidad['claveMovimiento']."', ".
					 "'".$unidades[$i]['fechaEvento']."', ".
					 "'".$unidad['prodStatus']."', ".
					 "'".$vupdateParse."', ".
					 "'".$vuptimeParse."')";

				fn_ejecuta_query($sqlInsertTransaccion);
			}

			if($unidad['claveMovimiento']=='RP'){
								//echo "entra 2 D04";

				$sqlInsertTransaccion2 = "INSERT INTO alTransaccionUnidadTbl ".
				"(tipoTransaccion, centroDistribucion, folio, vin, ".
				"fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, ".
				"prodStatus, fecha, hora) ".
				"VALUES";

				$sqlInsertTransaccion2 .= "('D04',".
					 "'".$unidad['centroDistribucion']."', ".
					 "'".$folio."', ".
					 "'".$unidad['vin']."', ".
					 "'".date('Y-m-d H:i:s')."', ".
					 "'".$unidad['claveMovimiento']."', ".
					 "'".$unidades[$i]['fechaEvento']."', ".
					 "'".$unidad['prodStatus']."', ".
					 "'".$vupdateParse."', ".
					 "'".$vuptimeParse."')";

				fn_ejecuta_query($sqlInsertTransaccion2);
			}


		}
		
	}

	function sortByFechaYTractor1($a, $b) {
	    if (($a['fechaEvento'] == $b['fechaEvento'])) {

	    	return ($a['tractor'] - $b['tractor']);
	    }
	    return (date_create($a['fechaEvento']) < date_create($b['fechaEvento'])) ? -1 : 1;
	}

	function getTransaccion11($claveMovimiento) {

		/*if($claveMovimiento == 'CO' || $claveMovimiento == 'EO' || $claveMovimiento == 'EH' || $claveMovimiento == 'FC' || $claveMovimiento == 'PR' || $claveMovimiento == 'PR'){
			$claveMovimiento='P01';
		} else if($claveMovimiento == 'CO' || $claveMovimiento == 'EO' || $claveMovimiento == 'EH' || $claveMovimiento == 'FC' || $claveMovimiento == 'PR' || $claveMovimiento == 'PR'){
			$claveMovimiento='D04';
		} */
		
		$claveMovimiento='P01';

		return $claveMovimiento;
	}

	function getTransaccion21($claveMovimiento) {

		/*if($claveMovimiento == 'CO' || $claveMovimiento == 'EO' || $claveMovimiento == 'EH' || $claveMovimiento == 'FC' || $claveMovimiento == 'PR' || $claveMovimiento == 'PR'){
			$claveMovimiento='P01';
		} else if($claveMovimiento == 'CO' || $claveMovimiento == 'EO' || $claveMovimiento == 'EH' || $claveMovimiento == 'FC' || $claveMovimiento == 'PR' || $claveMovimiento == 'PR'){
			$claveMovimiento='D04';
		} */
		
		$claveMovimiento='D04';

		return $claveMovimiento;
	}


	function getTxt21($texts){
        $positions = array_keys($texts);
        $text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = $positions[$i - 1] - 1;
                $antLength = strlen($texts[$positions[$i - 1]]);
            }
            $text .= out('s', ($positions[$i] - 1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }

     /*function subirFtp_Comodato($nombreArchivo){
        $fileDir = "C:/carbook/i830_CB/".$nombreArchivo;
        if(file_exists($fileDir)){
            # Definimos las variables
            $host = "ftp.iclfca.com";
            //$port = 21;
            $user = "IG";
            $password = "dBJY76ig";
            $ruta = "/ig/SC/";
            $file = $fileDir;//tobe uploaded
            $remote_file = $nombreArchivo;
            $nuevo_fichero = "C:/carbook/i830_CB/respaldo/".$nombreArchivo;

            # Realizamos la conexion con el servidor
            $conn_id = @ftp_connect($host);//,$port);
            if($conn_id){
                    # Realizamos el login con nuestro usuario y contraseña
                if(@ftp_login($conn_id,$user,$password)){
                    # Canviamos al directorio especificado
                    if(@ftp_chdir($conn_id,$ruta)){
                        # Subimos el fichero
                        if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
                        //echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
                        }else{
                            //echo "No ha sido posible subir el fichero";
                        }
                    }else
                        echo "No existe el directorio especificado";
                }else
                    echo "El usuario o la contraseña son incorrectos";
                    # Cerramos la conexion ftp
                    ftp_close($conn_id);
            }else
                echo "No ha sido posible conectar con el servidor";
        }else{
            echo "no existe el archivo";
        }if(!copy($file, $nuevo_fichero)){
            echo "Error al copiar $fichero...\n";
        }else{
            unlink($file);
            echo "si se copio el archivo";
        }
    }*/

?>
