<?php
	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");



	global $dirPath;
	global $inicioFile;
	global $fileName;

	date_default_timezone_set('America/Mexico_City');

	//$dirPath = "C:carbook/i816/";
	$dirPath = "E:\\carbook\\i816EXT\\";
	$filePath = $dirPath.$inicioFile;
	$ejecutaProceso = true;

	ejecutaEXT();

	function ejecutaEXT(){





		$fecha = date('Y-m-j');
		$nuevafecha = strtotime ( '-15 day' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m-j' , $nuevafecha );

		$sqlEXT = "SELECT * ".
					"FROM alunidadestbl al, alhistoricounidadestbl ud, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = ud.vin ".
					"AND ud.vin = dy.vin ".
					"AND ud.claveMovimiento ='EP' ".
					"AND ud.fechaEvento BETWEEN '".$fechaAnt."' AND '".$nuevafecha."' ".
					//"AND ud.fechaEvento BETWEEN '2017-02-22' AND '2017-03-02' ".
					//"AND dy.cveStatus in('PK') ".
					"AND dy.cveStatus in('3K') ".
					"AND dy.cveDisFac = ud.distribuidor ".
					"AND ud.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='ERC') ".
					"AND ud.vin  in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='ALR') ".
					"AND ud.vin  in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='TLR') ";
		$rsEXT =fn_ejecuta_query($sqlEXT);
		//echo json_encode($rsEXT);

		for ($i=0; $i <sizeof($rsEXT['root']) ; $i++) {
			//echo json_encode($rsEXT['root'][$i]['cveStatus']);
			if ($rsEXT['root'][$i]['cveStatus'] == '3K' /*|| $rsEXT['root'][$i]['cveStatus'] == 'PK'*/) {
				$arrKia[] = $rsEXT['root'][$i];
			}
		/////////////////////////////////////////////////////////
			else{
				//echo "string";
			}
		}

		if (count($arrKia) != 0) {
			generaEXTKIA1($arrKia);
			//generaALRKIA($arrKia);
		}
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function generaEXTKIA1($arrKia){
		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');

		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816EXT\\";

		$inicioFile = "KMM_EXT_".$fecha.$hora.".txt";
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"EXTH"." "."TRA"."  "."GMX"."  "."EXT".$fecha.$hora.PHP_EOL);

		//detalle



		for ($i=0; $i <sizeof($arrKia) ; $i++) {

			if ($arrKia[$i]['centroDistribucion'] == 'CDTOL') {
				$origin = 'FT15';
			}
			if ($arrKia[$i]['centroDistribucion'] == 'CDSFE') {
				$origin = 'FT18';
			}



			fwrite($archivo,"EXT  RC".$arrKia[$i]['vin'].substr($arrKia[$i]['fechaEvento'],0,4).substr($arrKia[$i]['fechaEvento'],5,2).
				substr($arrKia[$i]['fechaEvento'],8,2).substr($arrKia[$i]['fechaEvento'],11,2).substr($arrKia[$i]['fechaEvento'],14,2).
				substr($arrKia[$i]['fechaEvento'],17,2).$origin."                "."TRA  T".PHP_EOL);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('ERC', '".
										$arrKia[$i]['centroDistribucion']."', '".
										$arrKia[$i]['vin']."', '".
										$arrKia[$i]['fechaEvento']."', '".
										$arrKia[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
			echo json_encode($insTransaccion);

		}
		//fin de archivo
		$long=(sizeof($arrKia)+2);
		fwrite($archivo,"EXTT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);
	}
?>
