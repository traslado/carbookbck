<?php
   
   /*
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    date_default_timezone_set('America/Mexico_City');

		ejecutaGTE(); */
		
	function ejecutaGTE(){	
		echo "Inicio iGTE: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
        $sqlGTE = 	"SELECT dy.cveStatus ". 
					"FROM alhistoricoUnidadesTbl al, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = dy.vin ".
					"AND al.centroDistribucion='LZC02' ".
					"AND al.claveMovimiento='L1' ".
					"AND SUBSTR(dy.nomFac,-1) ='M' ".
					"AND dy.cveStatus in ('TK','TY') ".
					"AND dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
					"AND dy.fechaMovimiento is not null ".
					"AND dy.vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='RLS') ".
					"AND dy.vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='REP') ".
					"AND dy.vin not in (SELECT vin FROM altransaccionunidadtbl  WHERE tipoTransaccion ='GTE') ";


		$rsGTE= fn_ejecuta_query($sqlGTE);


		for ($i=0; $i <sizeof($rsGTE['root']) ; $i++) { 

			if (substr($rsGTE['root'][$i]['cveStatus'], -2) == 'TK') {				
				$arrK[] = $rsGTE['root'][$i];				
			}
		/////////////////////////////////////////////////////////
			else if (substr($rsGTE['root'][$i]['cveStatus'], -2) == 'TY') {
				$arrH[] = $rsGTE['root'][$i];
			}
			else{
				//echo "string";
			}
		}
		if (count($arrK) != 0) {
			$varEstatus = 'TK';
			$nomArchivo = 'K';
			$portCode = 'FT16';
			 ejecutaGTE1($varEstatus,$nomArchivo,$portCode);
			
		}	

		if (count($arrH) != 0) {
			$varEstatus = 'TY';
			$nomArchivo = 'H';
			$portCode = '1145';
			 ejecutaGTE1($varEstatus,$nomArchivo,$portCode);
		}
		echo "FIN iGTE: ".date("Y-m-d H:i", strtotime("now"))."\r\n";			
	} 



	function ejecutaGTE1($varEstatus,$nomArchivo,$portCode){	     

		$sqlGenerarDatos= "SELECT dy.vin,dy.cveDisFac,dy.cveStatus,dy.nomFac,dy.dirEnt,dy.fechaMovimiento ".
						  "FROM alinstruccionesMercedestbl dy,alhistoricoUnidadesTbl hu ".
						  "WHERE SUBSTR(dy.nomFac,-1) ='M' ".
						  "AND dy.vin=hu.vin ".
						  "AND hu.claveMovimiento='L1' ".
						  "AND dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
						  "AND dy.fechaMovimiento is not null ".
						  "AND dy.vin not in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='GTE') ".
						  "AND dy.vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='RLS') ".
						  "AND dy.vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='REP') ".
						  "AND dy.cveLoc='LZC02' ".
						  "AND dy.cveStatus='".$varEstatus."' ";

       	$rssqlGenerarDatos= fn_ejecuta_query($sqlGenerarDatos);
		
       	for ($i=0; $i < sizeof($rssqlGenerarDatos['root']) ; $i++) { 

			if (substr($rssqlGenerarDatos['root'][$i]['nomFac'], -1) == 'M') {	
				$arrGTE[] = $rssqlGenerarDatos['root'][$i];	
				}
			else {

			}
		}

		if (count($arrGTE) != 0) {
		generaGTE($arrGTE,$nomArchivo,$portCode);

		}
	}

	function generaGTE($arrGTE,$nomArchivo,$portCode){

		$fecha = date("YmdHis");
	    $fileDir = "E:/carbook/archivosInterfacesGLOVIS/respGTE/".$nomArchivo."MM_GTE_".$fecha.".txt";
	    $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
	    $recordsPositionValue = array();
	    $nombreBusqueda= $nomArchivo."MM_GTE_".$fecha.".txt";

	    //A) ENCABEZADO
	    fwrite($fileRec, 'GTEH APS  GMX  GTE'."$fecha".PHP_EOL);
	        

	    $contador=count($arrGTE);

	        //B) DETALLE UNIDADES

	        for ($i=0; $i<$contador ; $i++) 
	        { 

	            fwrite($fileRec,'GTE  '.$arrGTE[$i]['vin'].sprintf('%-4s',$arrGTE[$i]['dirEnt']).$portCode.' '.sprintf('%-5s',$arrGTE[$i]['cveDisFac']).substr($arrGTE[$i]['fechaMovimiento'],0,4).substr($arrGTE[$i]['fechaMovimiento'],5,2).substr($arrGTE[$i]['fechaMovimiento'],8,2).substr($arrGTE[$i]['fechaMovimiento'],11,2).substr($arrGTE[$i]['fechaMovimiento'],14,2).substr($arrGTE[$i]['fechaMovimiento'],17,2).'0000000000                                                   '.PHP_EOL);


				$today = date("Y-m-d H:i:s");
				$fecha1 = substr($today,0,10);
				$hora=substr($today,11,8);

			$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin, ".
			 				"fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".
							"VALUES ('GTE','LZC02',NULL,'".$arrGTE[$i]['vin']."','".$arrGTE[$i]['fechaMovimiento']."','L3','".$today."','SO','".$fecha1."','".$hora."') ";    

			fn_ejecuta_query($sqlAddTransaccion);


		$UpdStatus= "UPDATE alinstruccionesmercedestbl set cveStatus='SO' WHERE vin='".$arrGTE[$i]['vin']."' and cveLoc='LZC02'";

		fn_ejecuta_query($UpdStatus);
		
	        }
			//C) TRAILER
	        $long=(sizeof($arrGTE)+2);
			fwrite($fileRec,"GTET ".sprintf('%06d',($long)));
	       	fclose($fileRec);
	        ftpArchivo_04($nombreBusqueda);
		
	}

function ftpArchivo_04($nombreBusqueda){
			if(file_exists("E:/carbook/archivosInterfacesGLOVIS/respGTE/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/archivosInterfacesGLOVIS/respGTE/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/archivosInterfacesGLOVIS/respGTE/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							//echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			//echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	} 
?>