<?php
	require_once("../funciones/generales.php");
	//require_once("../funciones/utilidades.php");
    //require_once("../funciones/utilidadesProcesos.php");

	$files = glob('C:/carbook/815/hyu/*'); // obtiene todos los archivos
	foreach($files as $file){
	  if(is_file($file)) // si se trata de un archivo
	    unlink($file); // lo elimina
	}

	$fecha = strtotime ( '-40 day' , strtotime (date("Y-m-d")) ) ;
	$fechaDesde = date ( 'Y-m-d' , $fecha );
	$fechaHasta = date('Y-m-d');




	$selHYU="SELECT   dt.centroDistribucion,dt.vin,dt.fechaEvento, dt.claveMovimiento,dt.distribuidor,dt.idTarifa, tl.idViajeTractor,"
			 ." dy.vin, dy.cveLoc, dy.fEvento,dy.hEvento,dy.cveDisEnt, tr.compania,tr.tractor"
				." FROM alhistoricounidadestbl dt , trunidadesdetallestalonestbl ts , trtalonesviajestbl tl, trviajestractorestbl tv, "
				." catractorestbl tr, alunidadestbl al , casimbolosunidadestbl su  , alinstruccionesmercedestbl dy"
				." WHERE dt.centroDistribucion in ('CDVER','CDLZC','CDTOL','CDSFE','CDAGS')"
				." AND dt.claveMovimiento='AM'"
				." AND dt.fechaEvento BETWEEN '".$fechaDesde."' AND '".$fechaHasta."'"
				." AND dt.idTarifa in ('01','00')"+
				." AND dt.vin = ts.vin"
				." AND ts.idtalon = tl.idtalon"
				." AND tl.tipoDocumento ='R'"
				." AND tl.idViajeTractor = tv.idViajeTractor"
				." AND tv.idTractor = tr.idTractor"
				." AND tr.tractor !='12'"
				." AND tr.compania in ('CR','TR','ST','TC')"
				." AND dt.vin = al.vin"
				." AND al.simboloUnidad = su.simboloUnidad"
				." AND su.simboloUnidad in ('GI10NO','GI10SE','ELANT','IX35TU','SPORTA','FORTEK','SONATA','SORENT','KIARIO','IX25SU','OPTIMA','HYUSFE','IX25SU')"
				." AND dt.vin = dy.vin"
				." AND dy.cveStatus ='PH'"
				." AND NOT EXISTS (SELECT * FROM altransaccionunidadtbl un WHERE dt.vin = un.vin AND un.tipoTransaccion='TLP' )"
				." AND NOT EXISTS (SELECT * FROM altransaccionunidadtbl un WHERE dt.vin = un.vin AND un.tipoTransaccion='TLR' )"
				." AND NOT EXISTS (SELECT * FROM alinstruccionesmercedestbl y WHERE dt.vin = y.vin AND y.cveStatus in ('1H','2H'))"
				." ORDER BY tr.compania,tl.idViajeTractor,dt.fechaEvento,dt.vin";
	$rstSelHYU=fn_ejecuta_query($selHYU);
	//echo json_encode($rstSelHYU);

	if ($rstSelHYU['records']!=0) {
		generaArchivoHYU($rstSelHYU);
	}
	else{

		$fechaArchivo=date("YmdHms");

		$fileDir="C:/carbook/815/hyu/HMM_TLP".$fechaArchivo.".txt";
		$fileLog = fopen($fileDir, 'a+');	

		$fileDir="C:/carbook/815/hyu/HMM_EXT".$fechaArchivo.".txt";
		$fileLog = fopen($fileDir, 'a+');	

	}


	function generaArchivoHYU($rstSelHYU){
		$sqlFolioStr = "SELECT estatus from cageneralestbl where tabla='815' AND columna='TRA' AND valor='MX' AND nombre='H'";
		$FolioRst = fn_ejecuta_query($sqlFolioStr);
		//echo json_encode($FolioRst);

		$folio = $FolioRst['root'][0]['estatus'] +1;

		$sqlActualizaFolioStr="UPDATE cageneralestbl SET estatus='".$folio."' WHERE tabla='815' and columna='TRA' and valor='MX' and idioma='ESP' and estatus='".$FolioRst['root'][0]['estatus']."'";
		$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

		$fechaArchivo=date("YmdHms");


	    //GENERACION ARCHIVO TLP
		$fileDir="C:/carbook/815/hyu/HMM_TLP".$fechaArchivo.".txt";
		$fileLog = fopen($fileDir, 'a+');	


		$encabezado1 = array(1  => "TLPH ",
			    			7   => "TRA  ",
			    			11  => "GMX  ",
			    			16  => "TLP",
			    			29  => $fechaArchivo);
			    			
		$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

			if (strlen($folio)==1) {
	   			$ceros1='00000';
	   		}
	   		if (strlen($folio)==2) {
	   			$ceros1='0000';
	   		}
	   		if (strlen($folio)==3) {
	   			$ceros1='000';
	   		}
	   		if (strlen($folio)==4) {
	   			$ceros1='00';
	   		}
	   		if (strlen($folio)==5) {
	   			$ceros1='0';
	   		}

	   		$cons=0;
		for ($i=0;$i < sizeof($rstSelHYU['root']);$i++) {			

	    		$encabezado2 = array(1  => "TLP  ",
					    			7   => $rstSelHYU['root'][$i]['cveLoc']." ",
					    			11  => $rstSelHYU['root'][$i]['vin'],
					    			16  => $rstSelHYU['root'][$i]['cveDisEnt'],
					    			29  => date('Ymd',strtotime($rstSelHYU['root'][$i]['fechaEvento'])),
					    			32  => $ceros1.$folio,
					    			35  => "09N0000000000");
					    			
	    		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			$cons++;
		}

			if (strlen($cons)==1) {
	   			$ceros='00000';
	   		}
	   		if (strlen($cons)==2) {
	   			$ceros='0000';
	   		}
	   		if (strlen($cons)==3) {
	   			$ceros='000';
	   		}
	   		if (strlen($cons)==4) {
	   			$ceros='00';
	   		}
	   		if (strlen($cons)==5) {
	   			$ceros='0';
	   		}


		$encabezado3 = array(1  => "TLPT ",
			    			7   => $ceros,
			    			8   => $cons+2);
			    			
		$text = getTxt($encabezado3, array_keys($encabezado3)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);


	    //GENERACION ARCHIVO EXT
		$fileDir="C:/carbook/815/hyu/HMM_EXT".$fechaArchivo.".txt";
		$fileLog = fopen($fileDir, 'a+');	


		$encabezado1 = array(1  => "EXTH ",
			    			7   => "TRA  ",
			    			11  => "GMX  ",
			    			16  => "EXT",
			    			29  => $fechaArchivo);
			    			
		$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

			
	   		$cons=0;
		for ($i=0;$i < sizeof($rstSelHYU['root']);$i++) {			

	    		$encabezado2 = array(1  => "EXT  ",
	    							 5=> "YE",
					    			11  => $rstSelHYU['root'][$i]['vin'],
					    			19  => date('Ymd',strtotime($rstSelHYU['root'][$i]['fechaEvento'])),
					    			25  => $rstSelHYU['root'][$i]['cveLoc'],
					    			30  => $rstSelHYU['root'][$i]['cveDisEnt']." ",
					    			32  => $rstSelHYU['root'][$i]['distribuidor'],
					    			35  => '          ',
					    			40  => "TRA  ",
					    			45  => "T                            ");
					    			
	    		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
			$cons++;
		}

			if (strlen($cons)==1) {
	   			$ceros='00000';
	   		}
	   		if (strlen($cons)==2) {
	   			$ceros='0000';
	   		}
	   		if (strlen($cons)==3) {
	   			$ceros='000';
	   		}
	   		if (strlen($cons)==4) {
	   			$ceros='00';
	   		}
	   		if (strlen($cons)==5) {
	   			$ceros='0';
	   		}


		$encabezado3 = array(1  => "EXTT ",
			    			7   => $ceros,
			    			8   => $cons+2);
			    			
		$text = getTxt($encabezado3, array_keys($encabezado3)).out('n', 1); //out es un salto de linea
	    fwrite($fileLog, $text);

	    $today=date('Y-m-d H:m:s');

  		for ($i=0;$i < sizeof($rstSelHYU['root']);$i++) {			

		    $insTransaccion="INSERT INTO altransaccionunidadtbl (tipoTransaccion,centroDistribucion,folio, vin, fechaGeneracionUnidad,"
		    			." claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) "
						." VALUES ('TLP','"
							.$rstSelHYU['root'][$i]['centroDistribucion']
							."',null,'"
							.$rstSelHYU['root'][$i]['vin']
							."','".$rstSelHYU['root'][$i]['fechaEvento']
							."','".$rstSelHYU['root'][$i]['claveMovimiento']
							."','".$today
							."', null, null, null)";
			$rstInsTransaccion=fn_ejecuta_query($insTransaccion);

			$updTransaccion="UPDATE alinstruccionesmercedestbl SET cveStatus='AH' WHERE vin='".$rstSelHYU['root'][$i]['vin']."' AND cveStatus='PH'";			
			$rstUpdTransaccion=fn_ejecuta_query($updTransaccion);

		}
	}

	function getTxt($texts, $positions){
    	$text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = ($positions[$i]);
                $antLength = strlen($texts[$positions[$i]]);
            }
            $text .= out('s', ($positions[$i]-1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }
    function out($char, $times){
        $validChar = array(
                    "n" => PHP_EOL,
                    "t" => "\t",
                    "s" => " ");
        $chars = '';
        for ($i=0; $i < $times ; $i++) { 
            $chars .= $validChar[$char];
        }
        fseek($myfile, -20);   
        return $chars;
    }

    for ($i=0; $i <si ; $i++) { 
    	# code...
    }

?>