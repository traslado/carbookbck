<?php
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    i830_OM_manual();

	function i830_OM_manual(){
	      $startDate = date('Y-m-d', strtotime("-360 days")); //- 7 días
	    $today = date('Y-m-d');
	    $todayDateTime = date('Y/m/d H:i:s');
			echo "inicio i830".$todayDateTime;

	    $p01Count = 0; //Contador de unidades con clave de movimiento 'AM'
	    $d04Count = 0; //Contador de unidades con clave de movimiento 'OM'

	    $tipoTransmision=$_REQUEST['tipoTransmision'];
        $centroDistribucion=$_REQUEST['centroDistribucion'];
        $vin1= $_REQUEST['vines'];


        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");
        $vin=str_ireplace($buscar,$reemplazar,$vin1);

        $cadena = chunk_split($vin, 17,"','");
        
        $vines=substr($cadena,0,-2);

        $tipoMvo= $_REQUEST['tipoMvo'];


	    //Conteo de unidades AM-P01, OM-D04
	    foreach ($unidades as $unidad) {
	    	if ($unidad['claveMovimiento'] == 'AM')
		    	$p01Count++;
		    else //OM
		    	$d04Count++;
	    }

	    //Se genera archivo de unidades que no están en la 660
	    //createLogSin660($unidadesSin660, "RAT660.FAL");

	    //Ordena las Unidades con 660 por Fecha y Tractor para creación de Logs *No sé si sea mejor ordenar en el query;
	    //usort($unidades, 'sortByFechaYTractor');
	     //$unidadesSin660 = getUnidadesSin660($startDate, $today);
	    $unidades = busquedaUnidades1($startDate, $today,$vines,$tipoMvo);


	    //Se genera archivo de unidades que están en la 660
	    createArchivoCon6601($unidades);

	    //Se genera Log de Avisos de Proceso y Cifras de Control, éste siempre se ejecuta...
	    //createLogProceso($p01Count, $d04Count, $todayDateTime);

	    //Se genera Log de Cifras de Control, sólo se ejecuta si hubo movimientos...
	    //createLogProceso2($p01Count, $d04Count, $todayDateTime);

	    //Se Afecta la BD insertando las unidades en alTransaccionUnidadTbl
	    //insertTransacciones1($unidades);

	    if (isset($unidades) && count($unidades) > 0)
	    	updFolio1();
	}

//-----FIN TRAP830------------------------------------------------------------------------------------------------------
    /*
    * DECLARACIÓN Y DEFINICION DE FUNCIONES PARA LA INTERFAZ TRAP830
    *
    */
    //Obtiene un folio de parámetros de Sistema...
    function getFolio1() {
    	//Obtiene el Folio de parametros del sistem
    	$sqlGetFolio = "SELECT f.folio FROM trFoliosTbl f ".
    				   "WHERE f.centroDistribucion = 'TCO' ".
    				   "AND f.compania = '830' ".
    				   "AND f.tipoDocumento = 'R';";

    	$rsFolio = fn_ejecuta_query($sqlGetFolio);

    	$folio = $rsFolio['root'][0]['folio'];

    	if ($folio=='1') {
    		$sqlUpdFolio = "UPDATE trFoliosTbl f set folio='9999' ".
    				   "WHERE f.centroDistribucion = 'TCO' ".
    				   "AND f.compania = '830' ".
    				   "AND f.tipoDocumento = 'R';";

    		$rsFolio = fn_ejecuta_query($sqlUpdFolio);    		
    	}

    	return $folio;
    }
    //Actualiza(autoincrementa) el Folio de parámetros de Sistema...
    function updFolio1() {
    	//Si no existió ningún tipo de error actualiza el folio
    	if (!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {

    		$folio = getFolio1();
	    	$sqlUpdFolio = "UPDATE trFoliosTbl ".
	    				   "SET folio = '".(intval($folio) - 1)."' ".
	    				   "WHERE centroDistribucion = 'TCO' ".
	    				   "AND compania = '830' ".
	    				   "AND tipoDocumento = 'R';";

	    	fn_ejecuta_query($sqlUpdFolio);
    	}
    }

    function getUnidadesSin6601($startDate, $endDate) {

    	$sqlExcluir660 = "(SELECT a6.vin FROM al660Tbl a6)";

    	$sqlHistoricoSin660 = "SELECT hu2.* FROM alHistoricoUnidadesTbl hu2 ".
			    				"WHERE hu2.idTarifa <> (SELECT t.idTarifa FROM caTarifasTbl t WHERE t.tarifa = '13') ".
			    				"AND hu2.claveMovimiento IN ('AM', 'OM') ".
			    				"AND hu2.centroDistribucion IN ('CDTOL', 'CDSAL', 'CDAGS', 'CDSFE', 'CDANG') ".
			    				"AND DATE(hu2.fechaEvento) BETWEEN '$startDate' AND '$endDate' ".
			    				//"AND DATE(hu2.fechaEvento) BETWEEN '2017-10-01' AND '2017-10-31' ".
			                    "AND hu2.vin NOT IN $sqlExcluir660;";

	    $rsUnidadesSin660 = fn_ejecuta_query($sqlHistoricoSin660);

	    return $rsUnidadesSin660['root'];
    }

	function busquedaUnidades1($startDate, $endDate,$vines,$tipoMvo) {

		
			$sqlHistorico660 = "SELECT h1.vin, adddate(h1.fechaevento , interval 2 minute) as fechaEvento,h1.claveMovimiento, h1.distribuidor as disUnidad, h1.centroDistribucion ,h1.idTarifa, e.folioTimbrado, f.viaje,g.tractor ".
							"FROM alHistoricoUnidadesTbl h1, alUnidadesTbl al, caSimbolosUnidadesTbl ca, caTarifasTbl tb, caGeneralesTbl c,trunidadesdetallestalonestbl d, trtalonesviajestbl e, trviajestractorestbl f, catractorestbl g ".
							//WHERE CAST(h1.fechaEvento AS DATE) >= CAST('2024-06-01' as DATE)  
							"WHERE CAST(h1.fechaEvento AS DATE) >= CAST('$startDate' as DATE) ".
							"AND h1.centroDistribucion IN ('CDTOL','CDSAL','CDVER','CDSFE','CDAGS','CDANG','CDLZC','CMDAT')  ".
							"AND h1.claveMovimiento IN ('ED','OM','RO','PT','OK','CS','TYTT','TCT') ".
							"AND h1.vin = al.vin ".
					        "AND h1.vin = d.vin ".
					        "AND e.idtalon = d.idtalon ".
					        "AND f.idViajeTractor = e.idViajeTractor ".
					        "AND f.idtractor = g.idtractor ".
					        "AND h1.idtalon= e.idtalon ".
					        "AND h1.vin in ('".$vines.") ".
					        "AND d.estatus !='C' ".
					        "AND e.claveMovimiento !='TX' ".
							// "AND h1.distribuidor not IN ('M8220', 'M8221', 'M8270', 'M8271','M8010','M8271','M8221','M8190','M8131') ".
							"AND al.distribuidor NOT IN(SELECT di.distribuidorCentro FROM cadistribuidorescentrostbl di WHERE al.distribuidor = di.distribuidorCentro AND di.tipoDistribuidor='DX') ".
							"AND substring(al.distribuidor,1,1) !='H' ".
							"AND al.simboloUnidad = ca.simboloUnidad    ".                     
							"AND ca.marca not in ('HY','KI','RN','SE','MI') ".
							"AND h1.idTarifa = tb.idTarifa ".
							//"AND tb.tarifa !='13' ".
							"AND c.tabla = 'interfaces' ".
							"AND c.columna = 'splc' ".
							"AND c.valor = h1.centroDistribucion ";
							//"AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='H10') ".
							//"AND h1.vin NOT IN (SELECT tr.vin FROM altransaccionunidadtbl tr WHERE h1.vin = tr.vin AND tr.tipoTransaccion='D09') ";

			$rsUnidades660 = fn_ejecuta_query($sqlHistorico660);
			//echo $sqlHistorico660."<br><br><br>";
			//echo json_encode($rsUnidades660);
			

		

		return $rsUnidades660['root'];

	}

	function createArchivoCon6601($unidades) {
		//echo json_encode($unidades);
		//echo "contadore unidades".sizeof($unidades);

		if (!isset($unidades) || count($unidades) < 1)
			return;
		/*El nombre del archivo es un texto de 12 caracteres
    	 *pos 0 valor fijo : 'IGSC'  												Longitud: 4 caracteres
    	 *pos 4 folio : USING(&&&&) *Valor autoincremental y     					Longitud: 4 caracteres
    	 *							es actualizado cada que sea utilizado
    	 *							siempre y cuando existan movimentos
    	 *							Viene de caGeneralesTbl
    	 *pos 8 valor fijo : '.R41'													Longitud: 4 caracteres
    	 */
		for ($i=0; $i <sizeof($unidades)  ; $i++) { 
			
			$folio = getFolio1();
	    	$fileName = "IGSC".sprintf("%04d", $folio).".R41";
			//$fileDir = fopen($_SERVER['DOCUMENT_ROOT']."/$fileName", 'a') or die('No se pudo generar Reporte');
			//$fileDir = "C:/carbook/i830_CB/".$fileName;
			$fileDir = fopen("C:/carbook/i830_CB//".$fileName, 'a') or die('No se pudo generar Reporte');
			$todayDateTime = date_create(date('Y/m/d H:i:s'));
			$contador=sizeof($unidades) + 2;

			$encabezado =  array(
								 1 => 'IGR41',
								 6 => date_format($todayDateTime, 'm'),
								 8 => date_format($todayDateTime, 'd'),
								10 => date_format($todayDateTime, 'y'),
								12 => date_format($todayDateTime, 'H'),
								14 => date_format($todayDateTime, 'i'),
								16 => sprintf('%06d', $contador ),
								22 => 'IGSC',
								26 => sprintf('%04d', $folio),
								30 => '.R41'
								);

			fwrite($fileDir, getTxt21($encabezado).out('n', 1));


			//foreach ($unidades as $unidad=> $unidad) {	
			
			for ($i=0; $i <sizeof($unidades)  ; $i++) { 

				

				$fechaEvento = date_create($unidades[$i]['fechaEvento']);

				 if ($unidades[$i]['centroDistribucion'] == 'CDTOL') { 
				 	$splcUnd = '958770000';
				 }

				 if ($unidades[$i]['centroDistribucion'] == 'CDVER') {
				 	$splcUnd = '979792000';
				 }

				 if ($unidades[$i]['centroDistribucion'] == 'CDLZC') {
				 	$splcUnd = '948218000';
				 }

				 if ($unidades[$i]['centroDistribucion'] == 'CDSFE') {
				 	$splcUnd = '922000999';
				 }
				 if ($unidades[$i]['centroDistribucion'] == 'CDSAL') {
				 	$splcUnd = '922786000';
				 }
				 if ($unidades[$i]['centroDistribucion'] == 'CDAGS') {
				 	$splcUnd = '940720000';
				 }
				 if ($unidades[$i]['centroDistribucion'] == 'CDANG') {
				 	$splcUnd = '922000333';
				 }				 	

				 if ($unidades[$i]['idTarifa'] == '6') {
				 	$disCambio="select  * from aldestinosespecialestbl
								where vin in ('".$unidades[$i]['vin']."')
								AND idDestinoEspecial=(SELECT max(idDestinoEspecial) from aldestinosespecialestbl where vin='".$unidades[$i]['vin']."')";
					$rsDistCambio=fn_ejecuta_query($disCambio);
				 	$distribuidor=$rsDistCambio['root'][0]['distribuidorDestino'];
				 	// code...
				 }else{
				 	$distribuidor=$unidades[$i]['disUnidad'];
				 }

				

				 if ($unidades[$i]['claveMovimiento'] == 'OM'){
				 	$claveMovimiento='D09';
				 	$fechaEvento=date_create($unidades[$i]['fechaEvento']);
				 	$contador ++;

				 	$registro =  array(
								 1 => $unidades[$i]['folioTimbrado'],
								 2 => '     ',
								 7 => $unidades[$i]['vin'],
								24 => date_format($fechaEvento, 'm'),
								26 => date_format($fechaEvento, 'd'),
								28 => date_format($fechaEvento, 'y'),
								30 => date_format($fechaEvento, 'H'),
								32 => date_format($fechaEvento, 'i'),
								//43 => getTransaccion($unidades[$i]['claveMovimiento']),
								34 => $claveMovimiento,
								37 => sprintf('%-9s',$splcUnd),
								38 => out('s', 11),
								48 => $distribuidor,
								49 => out('s', 27),
								50 => $unidades[$i]['tractor'],
							    51 => out('s', 7),
							   52 => sprintf('%-3s',$unidades[$i]['viaje']),
							   53 => out('s', 7)
								);
				 	

					fwrite($fileDir, getTxt21($registro).out('n', 1));

					//echo "5  ";
					$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl ".
								"(tipoTransaccion, centroDistribucion, folio, vin, ".
								"fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, ".
								"prodStatus, fecha, hora) ".
								"VALUES('D09',".
					 "'".$unidades[$i]['centroDistribucion']."', ".
					 "'".$folio."', ".
					 "'".$unidades[$i]['vin']."', ".
					 "'".date('Y-m-d H:i:s')."', ".
					 "'".$unidades[$i]['claveMovimiento']."', ".
					 "'".$unidades[$i]['fechaEvento']."', ".
					 "'".$unidades[$i]['prodStatus']."', ".
					 "'".$vupdateParse."', ".
					 "'".$vuptimeParse."')";
					 $rsGuarda=fn_ejecuta_query($sqlInsertTransaccion);	

				 }

				 $contador=sizeof($unidades) + 2;

			}
			$trailer = array(
							 1 => 'EOF',
							 2 => sprintf('%06d', $contador)
							);
			fwrite($fileDir, getTxt21($trailer).out('n', 1));

			fclose($fileDir);
							 
		}

		//subirFtp_Comodato($fileName);
	}
	

	function insertTransacciones1($unidades) {
		//echo json_encode($unidades);
		//echo "contador".count($unidades);

		if (!isset($unidades) || count($unidades) == 0)
			return;

		$folio = getFolio1();

		$sqlInsertTransaccion = "INSERT INTO alTransaccionUnidadTbl ".
								"(tipoTransaccion, centroDistribucion, folio, vin, ".
								"fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, ".
								"prodStatus, fecha, hora) ".
								"VALUES";

		for ($i = 0; $i < count($unidades); $i++) {
			if ($i > 0)
				$sqlInsertTransaccion .= ", ";

			$unidad = $unidades[$i];
			//echo count($unidad);
			//echo $i;
			//Fomatea la fecha y hora proveniente de la 660
			$vupdateParse = date_format(DateTime::createFromFormat('d/m/Y', $unidad['vupdate']), 'Y/m/d');
			$vuptimeParse = substr($unidad['vuptime'], 0, 5);
			//echo $vupdateParse;
			//echo $vuptimeParse;

			if($unidad['claveMovimiento']=='OM'){
				//echo "entra 1 p01  ";
				$sqlInsertTransaccion .= "('D09',".
					 "'".$unidad['centroDistribucion']."', ".
					 "'".$folio."', ".
					 "'".$unidad['vin']."', ".
					 "'".date('Y-m-d H:i:s')."', ".
					 "'".$unidad['claveMovimiento']."', ".
					 "'".$unidades[$i]['fechaEvento']."', ".
					 "'".$unidad['prodStatus']."', ".
					 "'".$vupdateParse."', ".
					 "'".$vuptimeParse."')";

				fn_ejecuta_query($sqlInsertTransaccion);
			}

			

		}
		
	}

	


	function getTxt21($texts){
        $positions = array_keys($texts);
        $text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = $positions[$i - 1] - 1;
                $antLength = strlen($texts[$positions[$i - 1]]);
            }
            $text .= out('s', ($positions[$i] - 1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }

    

?>
