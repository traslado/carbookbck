<?php
/*
   session_start();
	
	//include 'iVerificacionRepuve.php';
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");

	ejecutaPDI(); */

/*    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["interfacesLzcPdiArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["interfacesLzcPdiArchivoFld"]["error"];

		} else {
			$temp_file_name = $_FILES['interfacesLzcPdiArchivoFld']['tmp_name']; 
			$original_file_name = $_FILES['interfacesLzcPdiArchivoFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/genArchPdi/'. $file_name . $ext;
		  
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					//$a['root'] = genArchivoTxt($new_name);
					//echo("aqui imprimio".$inputFileName);
					$a['root'] = leerXLS($new_name);
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

    function leerXLS($inputFileName) {
    	
    	/** Error reporting 
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try{
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		}catch(Exception $e){
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) <= 67 && $highestRow <= 3 ){
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}


		if (ord(strtoupper($highestColumn)) < 68){
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);			
			return $root;
		}

		if (strlen($rowData[0][0]) < 17){
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN-Simbolo-Distribuidor-Color]', 'fail'=>'Y');
			return $root;
		}
		//------------------------------------------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true));
		
		for($row = 0; $row<sizeof($rowData); $row++){
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;


			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {
				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						if(!$isValidArr[$key]['exist']){
							$siNo = ($key == 'VIN') ? ' ya ' : ' no ';
							$errorMsg .= 'El '.$key.$siNo.'Existe!';
							$isTrue = false; 
						}
					}
				}
			}

			if ($isTrue){ //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa					

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
				//echo $rowData[$row][1];

				$addPio = "INSERT INTO alPioTbl (vin, portCode, workCode,orderType,estatus,fechaEvento)".
				"VALUES( '".$rowData[$row][0]."' ,".
					"'FT16', ".
					"'".$rowData[$row][3]."', ". //$rowData[$row][3]
					"'A', ".
					"'PD', ".						
					" NOW() ) ";

				fn_ejecuta_query($addPio);

				/*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCompania'],'PR',
						  $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCompania'], $repuve, $chofer, $observaciones,$rowData[$row][4]);

				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
					
					$errorMsg = 'Agregado Correctamente';					
				}else{
					$errorMsg = 'Registro No Agregado';
				}								
			}
			$root[]= array('VIN'=>$rowData[$row][1],'nose'=>$errorMsg);	

		}
		
		return $root;	
	}*/

	
	
  		function ejecutaPDI(){
  		echo "Inicio PDI: ".date("Y-m-d H:i", strtotime("now"))."\r\n";	

        $sqlRec = 	"SELECT dy.vin, dy.cveStatus ".
					"FROM alultimodetalletbl al, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = dy.vin ".
					"AND dy.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
					"AND al.centroDistribucion='LZC02' ".
					"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='ACN') ";

		$rsRec= fn_ejecuta_query($sqlRec);


		for ($i=0; $i <sizeof($rsRec['root']) ; $i++) { 

			if (substr($rsRec['root'][$i]['cveStatus'], -2) == 'DK' ||  substr($rsRec['root'][$i]['cveStatus'], -2) == 'TK') {				
				$arrK[] = $rsRec['root'][$i];				
			}
		/////////////////////////////////////////////////////////
			else if (substr($rsRec['root'][$i]['cveStatus'], -2) == 'DH') {
				$arrH[] = $rsRec['root'][$i];
			}
			else{
				//echo "string";
			}
		}
		if (count($arrK) != 0) {
			$varEstatus = 'DK';
			$nomArchivo = 'K';
			genInterface($varEstatus,$nomArchivo);
		}	
		if (count($arrH) != 0) {
           
		}
		echo "Fin PDI: ".date("Y-m-d H:i", strtotime("now"))."\r\n";			
	} 



	/*function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
		$tabla = 'tabla';
		$columna = 'columna';
		switch(strtoupper($field)) {
			case 'VIN':
				$tabla = 'alPioTbl'; $columna = 'vin'; break;

		}
		$sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
		$rs = fn_ejecuta_query($sqlExist);
		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
	}*/

	function genInterface($varEstatus,$nomArchivo){
		$errorMsg = '';
		//echo "Inicio RA2V Comodato: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
		$startDate = date('Y-m-d', strtotime("-5 days")); //- 5 días
	    $yesterday = date('Y-m-d', strtotime("-1 days"));
	    $today = date('Y-m-d');
	    $todayDateTime = date('Y-m-d H:i:s');

		$sqlGeneraDatos = "SELECT ap.vin, ap.workCode, hu.fechaEvento ".
							"from alpiotbl ap, alinstruccionesmercedestbl m,alHistoricoUnidadesTbl hu ".
							"where ap.vin not in (select vin from altransaccionunidadtbl where tipoTransaccion='ACN') ".
							"and ap.vin in (select vin from altransaccionunidadtbl where tipoTransaccion='REC') ".
							"and ap.estatus='PD' ".
							"and ap.vin=m.vin ".
							"and m.modelDesc in (select simboloUnidad from casimbolosunidadestbl where marca in ('KI','HY')) ".
							"and ap.vin=hu.vin ".
							"and hu.centroDistribucion='LZC02' ".
							"and hu.claveMovimiento='PI' ".
							"and m.cveStatus in ('".$varEstatus."','TK') ";

	    $rsGeneraDatos = fn_ejecuta_query($sqlGeneraDatos);	    

    	if(sizeof($rsGeneraDatos['root']) != null){
			//$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
			$fecha = date("YmdHis");
			$genArchivo = "KMM_ACN_".$fecha.".txt";
			$nombreBusqueda = "KMM_ACN_".$fecha.".txt";
	        $fileDir = "E:/carbook/archivosInterfacesGLOVIS/respACN/".$genArchivo;
	        $rampaFija = '807';
	        $lw_ramp = 'ADIMSRAMPMX';
	        $flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
	        $recordsPositionValue = array();
	        //for ($i = 0; $i <= $patioGroup; i++) {       
	            //foreach ($patioGroup as $conteo => $patioGroup) {
	            //A) ENCABEZADO
	            fwrite($flReporte660,'ACNH APS  GMX  ACN'."$fecha".PHP_EOL);
	                

	                //B) DETALLE UNIDADES
                
                for ($i=0; $i < sizeof($rsGeneraDatos['root']); $i++) { 
                    fwrite($flReporte660,'ACN  FT16 '.$rsGeneraDatos['root'][$i]['vin'].sprintf('%-6s',$rsGeneraDatos['root'][$i]['workCode']).substr($rsGeneraDatos['root'][$i]['fechaEvento'],0,4).substr($rsGeneraDatos['root'][$i]['fechaEvento'],5,2).substr($rsGeneraDatos['root'][$i]['fechaEvento'],8,2).substr($rsGeneraDatos['root'][$i]['fechaEvento'],11,2).substr($rsGeneraDatos['root'][$i]['fechaEvento'],14,2).substr($rsGeneraDatos['root'][$i]['fechaEvento'],17,2).'                         '.PHP_EOL);


						$today = date("Y-m-d H:i:s");
						$fecha = substr($today,0,10);
						$hora=substr($today,11,8);

							$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin, ".
							 				"fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".
											"VALUES ('ACN','LZC02',NULL,'".$rsGeneraDatos['root'][$i]['vin']."',"."now()".",'PI','".$rsGeneraDatos['root'][$i]['fechaEvento']."','".$varEstatus."','".$fecha."','".$hora."') ";    

							fn_ejecuta_query($sqlAddTransaccion);


							$UpdStatus= "UPDATE alPioTbl set estatus='PG' WHERE vin='".$rsGeneraDatos['root'][$i]['vin']."' ";

							fn_ejecuta_query($UpdStatus);


                }
                fwrite($flReporte660, 'ACNT '.sprintf('%06d', (sizeof($rsGeneraDatos['root'])+2)));
		        fclose($flReporte660);
	    		ftpArchivo_02($nombreBusqueda);
			}else{

    	}

		//echo "termino PIO LC-Logistic: ".date("Y-m-d H:i", strtotime("now"))."\r\n";  		
	}

		function ftpArchivo_02($nombreBusqueda){
			if(file_exists("E:/carbook/archivosInterfacesGLOVIS/respACN/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/archivosInterfacesGLOVIS/respACN/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/archivosInterfacesGLOVIS/respACN/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			//echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}
?>