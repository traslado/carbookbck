<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");


    $vin1=$_REQUEST['unidades'];


	$buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
	$reemplazar=array("", "", "", "");
	$vin=str_ireplace($buscar,$reemplazar,$vin1);

	$cadena = chunk_split($vin, 17,"','");

	
	$unidades=substr($cadena,0,-2);

	//echo $unidades;
	$_SESSION['idUsuario'] = 1;
	if($_REQUEST['trap728ModoRdo'] == "D"){
			fn_detener_unidades($unidades,
								fn_hold_liberar($_REQUEST['trap728EstatusCmb'],
												//$_REQUEST['trap728CDistCmb'],
												$_REQUEST['trap728ModoRdo']
								)
		);
	}
	else{
		fn_liberar_unidades($unidades,
								fn_hold_liberar($_REQUEST['trap728EstatusCmb'],
												//$_REQUEST['trap728CDistCmb'],
												$_REQUEST['trap728ModoRdo']
								)
		);

	}

	function fn_detener_unidades($unidades,$holdLibera,$centroDistribucion){
		echo $centroDistribucion;
		$arrayUnidTxt = array($unidades);
		$datelog = date("YmdHm");
		$filelog = fn_genera_encabezado_log($datelog,$_REQUEST['trap728EstatusCmb']);
		foreach ($arrayUnidTxt as $idx => $row) {
			#############################################
			##  no existen en el cabecero de  unidades ##
			#############################################
			if($row['existe'] == 1){
				$sqlHoldStr =	"SELECT * FROM alHoldsUnidadesTbl ".
								"WHERE distribuidorCentro ='". $_REQUEST['trap728CDistCmb'] . "' ".
								"AND claveHold = '". $_REQUEST['trap728EstatusCmb'] . "' ".
								"AND vin = '". $row . "'";
				$holdsRst = fn_ejecuta_query($sqlHoldStr);
				if ($holdsRst['records'] == 0) {
					$insHoldStr = 	"INSERT INTO alHoldsUnidadesTbl VALUES(".
									"'". $row ."',".
									"'". $_REQUEST['trap728EstatusCmb'] . "',".
									"'". $_REQUEST['trap728CDistCmb'] . "',".
									"'". date("Y-m-d H:m:s") . "')";
					fn_ejecuta_query($insHoldStr);
				}
				$text = $row ."|Estatus sin correspondecia:estatus no valido 	". $_REQUEST['trap728EstatusCmb']. "|\n";
				fn_genera_log($filelog,$text);
			}
			##########################################
			##  existen en el cabecero de  unidades ##
			##########################################
			else{

				$sqlDetUndStr =	"SELECT * FROM alHistoricoUnidadesTbl  ".
								"WHERE vin = '". $row . "'".
								"AND claveMovimiento  = '". $_REQUEST['trap728EstatusCmb'] . "' ".
								"AND fechaEvento = (SELECT MAX(fechaEvento) FROM alHistoricoUnidadesTbl  ".
								"WHERE vin = '". $row . "'".
								"AND claveMovimiento  = '". $_REQUEST['trap728EstatusCmb'] . "')";
				$detUndRst = fn_ejecuta_query($sqlDetUndStr);
				//echo json_encode($detUndRst);
				if($detUndRst['records'] > 0){
					$sqlDetUndLibStr =	"SELECT * FROM alHistoricoUnidadesTbl  ".
								"WHERE vin = '". $row . "'".
								"AND claveMovimiento  = '". $holdLibera . "' ".
								"AND fechaEvento >= '". $detUndRst['root'][0]['fechaEvento'] ."'";
					$detUndLibRst = fn_ejecuta_query($sqlDetUndLibStr);
					//echo json_encode($sqlDetUndLibStr);
					if($detUndLibRst['records'] >0){
						$sqlUtDetStr =	"SELECT *,'". $_REQUEST['trap728EstatusCmb'] ."' as claveMovimientoNuevo,now() as fechaEventoNuevo ".
										"FROM alHistoricoUnidadesTbl  ".
										"WHERE centroDistribucion = '". $detUndLibRst['root'][0]['centroDistribucion'] . "' ".
										"AND VIN = '". $row . "'".
										"AND claveMovimiento NOT IN (SELECT valor FROM cageneralestbl ".
																		"WHERE (tabla = 'alHoldsUnidadesTbl' and columna = 'claveHold') ".
																		"OR (tabla = 'alHistoricoUnidadesTbl ' and columna = 'nvalidos') ".
										                            ") ".
										"AND fechaEvento = (SELECT MAX(fechaEvento) ".
															"FROM alHistoricoUnidadesTbl  ".
										                    "WHERE centroDistribucion = '". $detUndLibRst['root'][0]['centroDistribucion'] . "' ".
										                    "AND VIN = '". $row . "'".
										                    "AND claveMovimiento NOT IN (SELECT valor FROM cageneralestbl ".
																		"WHERE (tabla = 'alHoldsUnidadesTbl' and columna = 'claveHold') ".
																		"OR (tabla = 'alHistoricoUnidadesTbl ' and columna = 'nvalidos') ".
										                            ")".
															")";
						$utDetallRst = fn_ejecuta_query($sqlUtDetStr);
						$rec = $utDetallRst['root'][0];
						$arrayUnidTxt = 	$rec;
						if($rec['claveChofer'] == ""){
							$rec['claveChofer'] = "null";
						}
						else{
							$rec['claveChofer'] = "'".$rec['claveChofer']."'";
						}
						$inHisUnd =	"INSERT INTO alHistoricoUnidadesTbl(centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer, observaciones, usuario, ip) VALUES(".
									"'". $rec['centroDistribucion'] ."',".
									"'". $rec['vin'] ."',".
									"'". $rec['fechaEventoNuevo'] ."',".
									"'". $rec['claveMovimientoNuevo'] ."',".
									"'". $rec['distribuidor'] ."',".
									"". $rec['idTarifa'] .",".
									"'". $rec['localizacionUnidad'] ."',".
									"". $rec['claveChofer'] .",".
									"'". $rec['observaciones'] ."',".
									"'". $_SESSION['idUsuario'] ."',".
									"'127.0.0.1')";
						//fn_ejecuta_query($inHisUnd);
						//echo json_encode($inHisUnd);

					}
					else{
						$text = $row['vin'] ."|DET. NO LOBERADA;Ya tiene estatus detenido|\n";
						fn_genera_log($filelog,$text);
					}
				}
				else{
					$text = $row['vin'] ."|Estatus sin correspondecia:estatu no valido 	". $_REQUEST['trap728EstatusCmb']. "|\n";
					fn_genera_log($filelog,$text);

				}
				
			}
		}
		if($arrayUnidTxt){
			fn_prepara_info($arrayUnidTxt,$filelog);
		}
		else{
			echo json_encode(array('succes'=>false,'msjResponse'=>"No se encontraron unidades para detener"));
		}

	}

	function fn_liberar_unidades($unidades,$holdLibera){
		$arrayUnidTxt = array($unidades);
		$datelog = date("YmdHm");
		$filelog = fn_genera_encabezado_log($datelog,$_REQUEST['trap728EstatusCmb']);

		foreach ($arrayUnidTxt as $idx => $row) {
			if($row == 1){				
				$delHoldStr = 	"DELETE FROM alHoldsUnidadesTbl ".
								"WHERE vin = '". $row ."' ".
								"distribuidorCentro = '". $_REQUEST['trap728CDistCmb'] . "' ";
				fn_ejecuta_query($delHoldStr);
				$text = $row ."|Und no registrada: No ha sido Recibida,no existe 	". $_REQUEST['trap728EstatusCmb']. "|\n";
				fn_genera_log($filelog,$text);	
				$text = $row ."|No detenida POR	". $_REQUEST['trap728EstatusCmb']. "|\n";
				fn_genera_log($filelog,$text);	
				
			}
			else{
				$sqlUndHoldStr =	"SELECT * FROM  alHistoricoUnidadesTbl ".
									"WHERE vin = '". $row . "'".
									"AND claveMovimiento ='". $_REQUEST['trap728EstatusCmb'] . "' ".
									"AND fechaevento = (SELECT MAX(fechaEvento) ".
														"FROM alHistoricoUnidadesTbl WHERE vin = '". $row. "'".
														"AND claveMovimiento ='". $_REQUEST['trap728EstatusCmb'] . "')".
									"AND fechaEvento >= (SELECT MAX(fechaEvento) ".
														"FROM alHistoricoUnidadesTbl ". 
														"WHERE vin = '". $row. "'".
														"AND claveMovimiento = '". $holdLibera . "') ".
									"UNION ".
									"SELECT * FROM  alHistoricoUnidadesTbl ".
									"WHERE vin = '". $row. "'".
									"AND claveMovimiento = '". $holdLibera . "' ".
									"AND fechaevento = (SELECT MAX(fechaEvento) FROM alHistoricoUnidadesTbl ".
														"WHERE vin = '". $row. "'".
														"AND claveMovimiento ='". $holdLibera ."') ".
									"ORDER BY fechaEvento";
				$UndHodlRst = fn_ejecuta_query($sqlUndHoldStr);
				//echo json_encode($UndHodlRst);
				if($UndHodlRst['records']>=1){		
					$sqlUtDetStr =	"SELECT *,'". $_REQUEST['trap728EstatusCmb'] ."' as claveMovimientoNuevo,now() as fechaEventoNuevo ".
									"FROM alHistoricoUnidadesTbl  ".
									"WHERE centroDistribucion = '". $_REQUEST['trap728CDistCmb'] . "' ".
									"AND VIN = '". $row . "'".
									" AND claveMovimiento  IN (SELECT valor FROM cageneralestbl ".
																	"WHERE (tabla = 'alHoldsUnidadesTbl' and columna = 'claveHold') ".
																	"OR (tabla = 'alHistoricoUnidadesTbl ' and columna = 'nvalidos') ".
									                            ") ".
									" AND fechaEvento = (SELECT MAX(fechaEvento) ".
														"FROM alHistoricoUnidadesTbl  ".
									                    "WHERE centroDistribucion = '". $_REQUEST['trap728CDistCmb'] . "' ".
									                    "AND VIN = '". $row . "'".
									                    " AND claveMovimiento  IN (SELECT valor FROM cageneralestbl ".
																	"WHERE (tabla = 'alHoldsUnidadesTbl' and columna = 'claveHold') ".
																	"OR (tabla = 'alHistoricoUnidadesTbl ' and columna = 'nvalidos') ".
									                            ")".
														")";
					$utDetallRst = fn_ejecuta_query($sqlUtDetStr);
					//echo json_encode($utDetallRst);
					$rec = $utDetallRst['root'][0];
					$arrayUnidTxt = 	$rec;
					if($rec['claveChofer'] == ""){
						$rec['claveChofer'] = "null";
					}
					else{
						$rec['claveChofer'] = "'".$rec['claveChofer']."'";
					}
					$inHisUnd =	"INSERT INTO alHistoricoUnidadesTbl(centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer, observaciones, usuario, ip) VALUES(".
								"'". $rec['centroDistribucion'] ."',".
								"'". $rec['vin'] ."',".
								"'". $rec['fechaEventoNuevo'] ."',".
								"'". $rec['claveMovimientoNuevo'] ."',".
								"'". $rec['distribuidor'] ."',".
								"". $rec['idTarifa'] .",".
								"'". $rec['localizacionUnidad'] ."',".
								"". $rec['claveChofer'] .",".
								"'". $rec['observaciones'] ."',".
								"'". $_SESSION['idUsuario'] ."',".
								"'127.0.0.1')";
					fn_ejecuta_query($inHisUnd);

				}
				else{
					if($UndHodlRst['records']==0){

						$text = $row['vin'] ."|CON 	". $_REQUEST['trap728EstatusCmb']. "PREVIO  no tiene estatus de detenida|\n";
						fn_genera_log($filelog,$text);

					}
					else if($UndHodlRst['records']==2){

						$text = $row['vin'] ."|Unidad ya ha sido liberada: 	". $_REQUEST['trap728EstatusCmb']. "|\n";
						fn_genera_log($filelog,$text);
					}
				}

			}
		}
		if($arrayUnidTxt){
			fn_prepara_info($arrayUnidTxt,$filelog);

		}
		else{
			echo json_encode(array('succes'=>false,'msjResponse'=>"No se encontraron unidades para liberar"));
		}
	}
	function fn_genera_encabezado_log($datelog,$estatus){
		$estatus = ($estatus == "D")?"Detener":"Liberar";
		$fileDir="C:/carbook/i728/log_". $estatus ."_". $datelog .".log";
		//$fileDir = '../generacion_550/log_'. $estatus .'_'. $datelog .'.log';
		$fileLog = fopen($fileDir, 'a+');
		$text = "PROCESO|".$estatus ." Unidades|\n";
	    fwrite($fileLog, $text);
		//return $fileDir;
		//fclose($fileLog);

	}
	function fn_genera_log($fileDir,$text){
		$fileLog = fopen($fileDir, 'a+');
	    fwrite($fileLog, $text);
		fclose($fileLog);

	}
	function fn_prepara_info($arrayUnidTxt,$filelog){
				//echo json_encode($arrayUnidTxt);
		$nuevoArray = array($arrayUnidTxt);
		foreach ($nuevoArray as $key => $rec) {
			$sqlTraStr =	"SELECT * FROM alTransaccionUnidadTbl ".
							"WHERE tipoTransaccion = 'H10' ".
							"AND vin = '". $rec['vin'] ."' ".
							"AND claveMovimiento = '". $rec['claveMovimientoNuevo'] ."' ".
							"AND fechaMovimiento ='". $rec['fechaEventoNuevo'] ."'";
			$traRst = fn_ejecuta_query($sqlTraStr);
			if($traRst['records'] == 0){

				$sql660Str	= 	"SELECT  *  from al660tbl a6 ".
								"WHERE vin = '". $rec['vin'] ."' ".
								"AND scaccode in ('DCC','XTRA','MITS','XKSM') ".
								"AND oriSplc is not null ".
								"AND scaccode is not null ".
								"AND vuptime = (SELECT max(vuptime) from al660tbl WHERE vin = '". $rec['vin'] ."' ".
									"AND scaccode in ('DCC','XTRA','MITS','XKSM') ".
									"AND vupdate = (SELECT max(vupdate) from al660tbl WHERE vin = '". $rec['vin'] ."' ".
									"AND scaccode in ('DCC','XTRA','MITS','XKSM')))";
				$a66Rst = fn_ejecuta_query($sql660Str);
				//echo json_encode($a66Rst);
				if($a66Rst["records"] ==0){
					$sqlCabUndStr =	"SELECT * from al660tbl a6, alunidadestbl al ".
									"WHERE a6.vin = al.vin ".
									"AND al.vin = '". $rec['vin'] ."' ".
									"AND a6.scaccode in ('DCC','XTRA','MITS','XKSM') ".
									"AND al.distribuidor = 'USA' ".
									"ORDER BY vupdate desc,vuptime desc";
					$cabUndRst = fn_ejecuta_query($sqlCabUndStr);
					//echo json_encode($sqlCabUndStr);
					if($cabUndRst['records'] >0){
						$cabUndRst['root'][0]['fechaEvento'] = $rec['fechaEventoNuevo'];
						$cabUndRst['root'][0]['claveMovimientoNuevo'] = $rec['claveMovimientoNuevo'];
						$nuevoArray[] = $cabUndRst['root'][0];
					}
					else{
						$text = "Unidad sin 660".",".$rec['fechaEventoNuevo'].",H10,".$rec['claveMovimientoNuevo']."\n";
						fn_genera_log($filelog,$text);
					}

				}
				else{
					//echo json_encode($a66Rst);
					$traRst['root'][0]['fechaEvento'] = $rec['fechaEventoNuevo'];
					$traRst['root'][0]['claveMovimientoNuevo'] = $rec['claveMovimientoNuevo'];
					$nuevoArray[] = $traRst['root'][0];
				}
			}
		}

		foreach ($nuevoArray as $key => $row) {
			$nuevoArray[$key]['orisplc'] = fn_ori_splc($nuevoArray[$key]['centroDistribucion']);
			//echo json_encode($nuevoArray[$key]['orisplc']);
			if($row['scaccode'] == 'XTRA' || $row['scaccode'] == 'XKSM'){
				$nuevoArray[$key]['scaccode'] = 'DCC';
			}
			//echo json_encode($nuevoArray[$key]['scaccode']);
		}
		$numReg = count($nuevoArray);
		if($numReg<0){
			echo json_encode(array('succes'=>false,'msjResponse'=>"No se encontraron unidades"));
		}
		else{
			$nuevoArray = getGroupedArray($nuevoArray, array('scaccode','orisplc','claveMovimientoNuevo','fechaEvento'));
			fn_genera_archivo($nuevoArray,$numReg);
		}
	}
	function fn_genera_archivo($array,$totalRegistros){
		$sqlDelSegStr = "select valor as segtype, nombre as delay ".
						"from cageneralestbl where tabla = 'i728' ".
						" and columna = '". $_REQUEST['trap728EstatusCmb'] ."'";
		$delSegRst = fn_ejecuta_query ($sqlDelSegStr);
		//echo json_encode($delSegRst);
		$segtype = $delSegRst['root'][0]['segtype'];
		$delay = $delSegRst['root'][0]['delay'];
		//se Crea Archivo 
		$keys = array_keys($array);
		//echo json_encode($keys);

	    for ($i=0;$i < sizeof($keys);$i++) {
   			$fileDir="C:/carbook/i728/". $keys[$i] .".txt";
   			//echo json_encode($keys[$i]);
	    	//$fileDir = '../generacion_550/'. $keys[$i] .'.txt';
		    $fileLog = fopen($fileDir, 'a+');
			
			$segmento = 1;
			$countUnds =1;
			$countSegSc= 1; 
			$encabezado1 = array(1 => 'ISA*03*RA550     *00*          *ZZ*XTRA           *ZZ*ADMIS',
			 				   60 => $keys[$i],
			 				   64 => '      ',
			 				   70 => '*',
			 				   71 => date("ymd*Hi"),
			 				   82 => '*U*00200*',
			 				   91 => '00000000'.$i,
			 				   100 => '*0*P>');

		    $encabezado2 = array(1 => 'GS*VI*XTRA*VISTA*', 
			 				   18 => date("ymd*Hi"),
			 				   29 => '*00001*T*1');

		    
		    //Por cada unidad...
		    	
		    	$text = getTxt($encabezado1, array_keys($encabezado1)); //out es un salto de linea
			    fwrite($fileLog, $text);
			    $text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
		    	$keysSplc = array_keys($array[$keys[$i]]);
			    for ($j=0;$j < sizeof($keysSplc);$j++) {
			    	$keysStatus = array_keys($array[$keys[$i]][$keysSplc[$j]]);
			    	for ($k=0;$k < sizeof($keysStatus);$k++) {
			    		$keysFecha = array_keys($array[$keys[$i]][$keysSplc[$j]][$keysStatus[$k]]);
			    		for ($l=0;$l < sizeof($keysFecha);$l++) {
			    			$rows = $array[$keys[$i]][$keysSplc[$j]][$keysStatus[$k]][$keysFecha[$l]];

			    			
			    			$encabezado3 = array(1 => 'ST*550*00001', 
				 				   13 => str_pad($segmento,4,"0",STR_PAD_LEFT));
			    			$text = getTxt($encabezado3, array_keys($encabezado3)).out('n', 1); //out es un salto de linea
			    			fwrite($fileLog, $text);

			    			$encabezado4 = array(1 => 'BV5*', 
								 				   5 => $segtype,
								 				   6 => '*XTRA*',
								 				   12 => $keysSplc[$j],
								 				   21 => '*',
								 				   22 => str_pad($totalRegistros,3,"0",STR_PAD_LEFT),
								 				   25 => '*',
								 				   26 => $delay,
								 				   28 => '*',
								 				   29=> date("ymd***Hi",strtotime($keysFecha[$l])));

			    			$text = getTxt($encabezado4, array_keys($encabezado4)).out('n', 1); //out es un salto de linea
			    			
			    			fwrite($fileLog, $text);
			    			foreach ($rows as $index => $row) {
				    			$detalle1 = array(1 => 'VI*', 
								 				   4 => $row['vin'],
								 				   21 => '*',
								 				   22 => substr($row['vin'], 10,11),
								 				   23 => '*',
								 				   24 => substr($row['routedes'], 0,1),
								 				   26 => '*****');
				    			$text = getTxt($detalle1, array_keys($detalle1)).out('n', 1); //out es un salto de linea
				    			fwrite($fileLog, $text);
    			    			//fn_afectacion_db($row);
			    			}

			    			$detalle2 = array(1 => 'SE*', 
							 				   4 => str_pad(count($rows),6,"0",STR_PAD_LEFT),
							 				   10 => '*00001',
							 				   16 => str_pad($segmento,4,"0",STR_PAD_LEFT));
			    			$text = getTxt($detalle2, array_keys($detalle2)).out('n', 1); //out es un salto de linea
			    			fwrite($fileLog, $text);
			    			$segmento ++;
			    			
			    		}
			    		
			    	}

			    }
		 	
 			fclose($fileLog);
		}
		echo json_encode(array('succes'=>true,'msjResponse'=>"Operacion completada correctamente"));
		
	}

	function fn_hold_liberar($esatusDetener_pa,/*,$distribuidorCentro_pa,*/$detn_pa){
		//echo $detn_pa;
		$whereStr = "WHERE tabla = 'alHoldsUnidadesTbl'"; /*AND columna = '". $distribuidorCentro_pa . "'";*/
		if ($detn_pa == "D") {
			$lsCondicionStr = fn_construct($esatusDetener_pa, "valor", 2);            
		}
		else{
			$lsCondicionStr = fn_construct($esatusDetener_pa, "estatus", 2);

		}

		$whereStr = fn_concatena_condicion($whereStr, $lsCondicionStr);
		
		$sqlHoldLiberaStr =	"SELECT * FROM caGeneralesTbl ".$whereStr;
		$holdLiberaRst = fn_ejecuta_query($sqlHoldLiberaStr);
		//echo json_encode($holdLiberaRst);
		
		return ($detn_pa == "D")?$holdLiberaRst['root'][0]['estatus']:$holdLiberaRst['root'][0]['valor'],$holdLiberaRst['root'][0]['columna'];
	}
?>