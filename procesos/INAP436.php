<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");

    $fechaAnt = strtotime ( '-20 day' , strtotime (date("Y-m-d")) ) ;
    $fechaAnt = date ( 'Y-m-d' , $fechaAnt );
    //valida($fechaAnt);
    
    $getConsulta = 	" SELECT hu.centroDistribucion, au.avanzada, hu.fechaEvento, ct.tarifa, hu.distribuidor, hu.claveMovimiento,au.simboloUnidad, hu.vin,a6.orisplc,a6.dealerid, a6.dealerid2, a6.routeori ,a6.routedes".
                   	" FROM  alhistoricounidadestbl hu,catarifastbl ct,alunidadestbl au,casimbolosunidadestbl su,al660tbl a6,altransaccionunidadtbl tu".
                   	" WHERE hu.vin = au.vin ".
                   	" AND   hu.idtarifa = ct.idtarifa ".
                   	" AND   au.simboloUnidad = su.simboloUnidad ".
                    " AND   su.tipoOrigenUnidad <> 'E' ".
                   	" AND   hu.vin = a6.vin".
                   	" AND   (au.vin = a6.vin and au.avanzada = a6.numavanzada)".
                    " AND   hu.fechaEvento BETWEEN '2015-01-01' AND '2015-01-02 ' ".
					//" AND   $fecha = date('Y-m-d','H,m,s' $hoy[0])".
					" AND   hu.centroDistribucion IN ('CDSAL','CDAGS','CDTOL','CDANG','CDSFE','CDVER') ".
 					" AND   hu.vin NOT IN ( SELECT dy.vin  
											FROM alinstruccionesmercedestbl dy 
					                        WHERE dy.vin = hu.vin) ".
                    " AND  (hu.claveMovimiento = 'EP' AND hu.distribuidor IN  ('M8220','M8221','M8270','M8271'))".
                    " AND   hu.vin  NOT IN (SELECT tu.vin 
                                            FROM altransaccionunidadtbl tu 
                    						WHERE tu.tipoTransaccion = ('510'))".
                   " AND   hu.vin  NOT IN ( 
						                    SELECT a6.vin 
						                    FROM   al660tbl a6 
											WHERE  hu.vin = a6.vin 
											AND a6.scaccode = 'XTRA' 
											AND a6.orisplc  IS NOT NULL 
											AND a6.dessplc  IS NOT NULL 
											AND a6.dealerid IS NOT NULL 
				                            AND a6.vuptime = (SELECT max(a6.vuptime) from al660tbl a6 WHERE hu.vin = a6.vin 
											AND a6.scaccode = 'XTRA') 
											AND a6.vupdate = (SELECT max(a6.vupdate) from al660tbl a6 WHERE hu.vin = a6.vin 
											AND a6.scaccode  = 'XTRA')".
					" AND hu.vin = (SELECT max(hu.vin)
							                FROM al660tbl a6   
										    ORDER BY (a6.vupdate,a6.vuptime))".
					" AND  hu.fechaEvento = (SELECT max(hu.fechaEvento) from alhistoricounidadestbl hu 
					                           WHERE  hu.claveMovimiento in ('DT','ER','EP')) ".
					" AND  hu.fechaEvento = (SELECT max(hu.fechaEvento) from alhistoricounidadestbl hu 
					                           WHERE hu.claveMovimiento = ('AM'))".
				 	" AND a6.prodstatus = (SELECT al.prodstatus FROM al660tbl al 
										  WHERE al.prodstatus = 'SA'
											 AND al.orisplc  = al.orisplc
											 AND al.dealerid = al.dealerid
											 AND al.routedes = al.routedes
											 AND al.vuptime = (SELECT max(al.vuptime) from al660tbl al WHERE al.scaccode = 'XTRA'
											 AND al.prodstatus = 'SA')
											 AND al.vupdate = (SELECT max(al.vupdate) from al660tbl al WHERE al.scaccode = 'XTRA'AND al.prodstatus = 'SA'))".
					" AND a6.prodstatus = ( SELECT a6.prodstatus FROM al660tbl a6 
										   WHERE a6.prodstatus <> 'SA'
											 AND a6.scaccode = 'XTRA'
											 AND a6.orisplc  IS NOT NULL
											 AND a6.dessplc  IS NOT NULL
											 AND a6.dealerid IS NOT NULL
			                                 AND a6.vuptime = (SELECT max(a6.vuptime) from al660tbl a6 WHERE a6.prodstatus <> 'SA'
											 AND a6.scaccode = 'XTRA')
											 AND a6.vupdate = (SELECT max(a6.vupdate) from al660tbl a6 WHERE a6.prodstatus <> 'SA'
											 AND a6.scaccode  = 'XTRA')".
					" AND a6.prodstatus = (SELECT a6.prodstatus  FROM al660tbl a6 
										 WHERE a6.prodstatus = 'SA'
										 AND a6.scaccode  = 'XTRA'
										 AND a6.dealerid  = a6.dealerid 
										 AND a6.dealerid2 = a6.dealerid2 
										 AND a6.routeori = a6.dealerid2
										 AND a6.routedes = a6.dealerid 
										 AND a6.vuptime  = (SELECT max(a6.vuptime) from al660tbl a6 WHERE a6.prodstatus <> 'SA'
										 AND a6.scaccode = 'XTRA')
										 AND a6.vupdate  = (SELECT max(a6.vupdate) from al660tbl a6 WHERE a6.prodstatus <> 'SA'
										 AND a6.scaccode = 'XTRA')))";

		      				$rs = fn_ejecuta_query($getConsulta);
		  					fn_genera_archivo($rs['root'],$rs['records']);
		  			 
		  			 //$sqlAddtransaccionUnidades = "INSERT INTO altransaccionUnidadestmp (centroDistribucion,avanzada,fechaEvento,tarifa,distribuidor,claveMovimiento,prodstatus 
										 //scaccode,dealerid,dealerid2,routeori,routedes,llave )". 
                                         
                    //fn_ejecuta_query($sqlAddtransaccionUnidades));		
	
		  		$sqlUpd1 = "UPDATE casimbolosunidadestbl su,alunidadestbl au SET su.tipoOrigenUnidad = 'E'".
						    "WHERE   substr(au.distribuidor,1,1) = 'M' ".
						    "AND     SUBSTR(au.distribuidor,1,2) <>'M8' ".
						    "AND     su.tipoOrigenUnidad = 'I'";
				  fn_ejecuta_query($sqlUpd1);
				if(isset($_SESSION['error_sql'])&& $_SESSION['error_sql'] != ''){
					$rs['succes'] = false;
					$rs['error'] = $_SESSION['error_sql'];

				} 
				$sqlUpd2 = "UPDATE cadistribuidorescentrostbl dc,alhistoricounidadestbl hu,casimbolosunidadestbl su 
				            SET  su.tipoOrigenUnidad = 'E'".
						   "WHERE   dc.distribuidorcentro = hu.distribuidor ".
						   "AND     su.tipoOrigenUnidad   = 'E'";


				 fn_ejecuta_query($sqlUpd2);
				 if(isset($_SESSION['error_sql'])&& $_SESSION['error_sql'] != ''){
					$rs['succes'] = false;
					$rs['error'] = $_SESSION['error_sql'];

				} 
                echo json_encode($rs);            
 
    
    function fn_genera_archivo($array,$totalRegistros){
		$sqlDelSegStr = "select valor as segtype, nombre as delay ".
						"from cageneralestbl where tabla TRAP436 = '' ".
						" and columna = 'SA'";
		$delSegRst = fn_ejecuta_query ($sqlDelSegStr);
		$segtype = $delSegRst['root'][0]['segtype'];
		$delay = $delSegRst['root'][0]['delay'];
		//se Crea Archivo 
		$segmento = 1;
		$countUnds =1;  
		$countSegSc= 1;
	    $fileDir = '../generacion_550/chr510v.txt';
		$fileLog = fopen($fileDir, 'a+');	
	    $encabezado1 = array(1 => '510');
		$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
		fwrite($fileLog, $text);	 				
	    for ($i=0;$i < sizeof($array);$i++) {
	    	 $row = $array [$i];
	    	if ($countUnds ==1 ) {
	    		$encabezado2 = array(1  => 'HDR',
	    			4  => 'VISTA',
	    			19  => 'XTRA',
	    			23  => substr($row['orisplc'], 0,9),
	    			32  => substr($countSegSc, 0,3));
	    		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
		   		$countSegSc++;
		
			}
			$encabezado3 = array(  1  => 'DTL', 
				4  => $row['vin'],
			   21 => substr($row['routeri'], 0,5),
			   26 => substr($row['routedes'], 0,5),
			   31 => '           ',
			   39 => substr($row['dealerid'],0,5),
			   44 => '           ',
			   56 => substr($row['dealerid2'],0,5),
			   61 => '           ',
			   71 => date('ymd',strtotime($row['vupdate'])),
			   77 => date('His',strtotime($row['vuptime'])),
			   81 =>  '                    ',
			  101 => date('ymd',strtotime($row['fechaEvento'])),
			  107 => date('His',strtotime($row['fechaEvento'])),
			  111 => substr($row['factura'],0,10),
			  121 => substr($row['consecutivo'],0,3),
			  124 =>  '                    ');
		$text = getTxt($encabezado3, array_keys($encabezado3)).out('n', 1); //out es un salto de linea
		fwrite($fileLog, $text);	    			
		$countUnds=($countUnds==100)?1:	$countUnds+1;    			
		
		
	}

	echo json_encode(array('succes'=>true,'msjResponse'=>"Operacion completada correctamente"));           
 

}


	
?> 

  

 






  
								 