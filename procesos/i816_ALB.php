<?php
	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");

	

	global $dirPath;
	global $inicioFile;
	global $fileName;

	date_default_timezone_set('America/Mexico_City');

	//$dirPath = "C:carbook/i816/";
	$dirPath = "E:\\carbook\\i816ALB\\";
	$filePath = $dirPath.$inicioFile;
	$ejecutaProceso = true;

	//ejecutaALB();
	//ejecutaEXT();

	/*while(true){
		if(date("i")%60 == 0){
			if($ejecutaProceso == "S"){
					echo "Inicio: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
				ejecutaALB();
					if(file_exists($filePath)){											
						$fichero = "C:carbook/i816/".$inicioFile.".txt";
						$nuevo_fichero = "C:carbook/i816/".str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).".txt";
						if (!copy($fichero, $nuevo_fichero)) {
							echo "Error al copiar $fichero...\n";
						}   						   						   					
					} else {
					echo "El archivo no existe\r\n";
				}		
				echo "Termino: ".date("Y-m-d H:i", strtotime("now"))."\r\n";					
				$ejecutaProceso = "N";
			}
		}
		else{
			$ejecutaProceso = "S";
		}				
	}	*/	


	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function ejecutaALB(){

		date_default_timezone_set('America/Mexico_City');
		$hoy = getdate();
		$fecha = date('Y-m-d', $hoy[0]);

		$fechaAnt = strtotime ( '-15 day' , strtotime (date("Y-m-d")) ) ;
	    $fechaAnt = date ( 'Y-m-d' , $fechaAnt );

	    $sqlALB = "SELECT * FROM alunidadestbl al, alhistoricounidadestbl ud, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = ud.vin ".
					"AND ud.vin = dy.vin ".
					"AND ud.claveMovimiento ='OM' ".
					"AND ud.fechaEvento BETWEEN '".$fechaAnt."' AND '".$fecha."' ".
					"AND dy.cveStatus in('AK','AH') ".
					"AND dy.cveDisFac = ud.distribuidor ".
					"AND ud.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='ALB')" ;
		$rsALB = fn_ejecuta_query($sqlALB);
		//echo json_encode($sqlALB);


		for ($i=0; $i <sizeof($rsALB['root']) ; $i++) { 
			//echo json_encode($rsALB['root'][$i]['cveStatus']);
			if ($rsALB['root'][$i]['cveStatus'] == 'AK') {				
				$arrKia[] = $rsALB['root'][$i];				
			}
		/////////////////////////////////////////////////////////
			else if ($rsALB['root'][$i]['cveStatus'] == 'AH') {
				$arrHyu[] = $rsALB['root'][$i];
			}
			else{
				echo "string";
			}
		}
		
		if (count($arrKia) != 0) {
			generaALBKIA($arrKia);
			//generaALRKIA($arrKia);
		}	
		if (count($arrHyu) != 0) {
			generaALBHYU($arrHyu);		
			//generaALRHYU($arrHyu);
		}		
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function generaALBKIA($arrKia){
		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');				
			
		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816ALB\\";

		$inicioFile = "KMM_ALB_".$fecha.$hora.".txt";
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"ALBH"." "."TRA"."  "."GMX"."  "."ALB".$fecha.$hora.PHP_EOL);

		//detalle

		$sqlFolioStr = "SELECT * FROM trfoliostbl WHERE tipoDocumento='TH' AND compania='"./*$KIA[$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrKia[0]['centroDistribucion']."'";
		$FolioRst = fn_ejecuta_query($sqlFolioStr);

		$folio = $FolioRst['root'][0]['folio'] + 1;

		$sqlActualizaFolioStr="UPDATE trfoliostbl SET folio =". $folio." where tipoDocumento='TH' and compania='"./*$rsTLR['root'][$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrKia[0]['centroDistribucion']."'";
		$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

		//echo json_encode($FolioRst['root'][0]['folio']);

		for ($i=0; $i <sizeof($arrKia) ; $i++) { 

			$sqlCveDisFat ="SELECT * FROM capuertoshktbl ".
							"WHERE origen ='".$arrKia[$i]['cveDisFac']."'";
			$rsCve = fn_ejecuta_query($sqlCveDisFat);
			//echo json_encode($rsCve);

			if ($rsCve['records'] != 0) {
				$cveDis ='C';
			}
			else{
				$cveDis ='D';
			}

			$sqlTractor ="SELECT  ca.tractor ".
							"FROM trunidadesdetallestalonestbl ud , trtalonesviajestbl tl, trviajestractorestbl vi, catractorestbl ca ".
							"WHERE ud.vin ='".$arrKia[$i]['vin']."' ".
							"AND ud.estatus =1 ".
							"AND ud.idTalon = tl.idTalon ".
							"AND tl.idViajeTractor = vi.idViajeTractor ".
							"AND vi.idTractor = ca.idTractor ";
			$rsTractor =fn_ejecuta_query($sqlTractor);

				

			fwrite($archivo,"ALB"."  ".substr($arrKia[$i]['fechaEvento'],0,4).substr($arrKia[$i]['fechaEvento'],5,2).
				substr($arrKia[$i]['fechaEvento'],8,2).substr($arrKia[$i]['fechaEvento'],11,2).substr($arrKia[$i]['fechaEvento'],14,2).substr($arrKia[$i]['fechaEvento'],17,2));
			fwrite($archivo,$arrKia[$i]['vin']."P".sprintf("%-5s",$arrKia[$i]['cveLoc']).$cveDis.sprintf("%-5s",$arrKia[$i]['cveDisFac'])."D".sprintf("%-12s",$arrKia[$i]['chipNum'])."000000000000000");
			fwrite($archivo,substr($arrKia[$i]['fechaEvento'],0,4).substr($arrKia[$i]['fechaEvento'],5,2).substr($arrKia[$i]['fechaEvento'],8,2));
			fwrite($archivo,/*$arrKia[$i]['folio']*/sprintf('%06d',$FolioRst['root'][0]['folio']).sprintf('%06d',$rsTractor['root'][0]['tractor'])."09".sprintF('%02d',sizeof($arrKia)).$arrKia[$i]['distribuidor'].PHP_EOL);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('ALB', '".
										$arrKia[$i]['centroDistribucion']."', '".
										$arrKia[$i]['vin']."', '".
										$arrKia[$i]['fechaEvento']."', '".
										$arrKia[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
	

			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='EK' WHERE vin='".$arrKia[$i]['vin']."'"." AND cveStatus ='AK'" ;
			$rstUpt=fn_ejecuta_query($uptdy);
			
			
		}


		//fin de archivo
		$long=(sizeof($arrKia)+2);
		fwrite($archivo,"ALBT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);	
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function generaALBHYU($arrHyu){
		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');				
			
		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816ALB\\";

		$inicioFile = "HMM_ALB_".$fecha.$hora.".txt";
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"ALBH"." "."TRA"."  "."GMX"."  "."ALB".$fecha.$hora.PHP_EOL);

		//detalle

		$sqlFolioStr = "SELECT * FROM trfoliostbl WHERE tipoDocumento='TH' AND compania='"./*$KIA[$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrHyu[0]['centroDistribucion']."'";
		$FolioRst = fn_ejecuta_query($sqlFolioStr);

		$folio = $FolioRst['root'][0]['folio'] + 1;

		$sqlActualizaFolioStr="UPDATE trfoliostbl SET folio =". $folio." where tipoDocumento='TH' and compania='"./*$rsTLR['root'][$i]['compania']*/"TR" ."'"." ". "and centroDistribucion ='".$arrHyu[0]['centroDistribucion']."'";
		$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

		//echo json_encode($FolioRst['root'][0]['folio']);






		for ($i=0; $i <sizeof($arrHyu) ; $i++) { 

			$sqlCveDisFat ="SELECT * FROM capuertoshktbl ".
							"WHERE origen ='".$arrHyu[$i]['cveDisFac']."'";
			$rsCve = fn_ejecuta_query($sqlCveDisFat);
			//echo json_encode($rsCve);

			if ($rsCve['records'] != 0) {
				$cveDis ='C';
			}
			else{
				$cveDis ='D';
			}

			$sqlTractor ="SELECT  ca.tractor ".
							"FROM trunidadesdetallestalonestbl ud , trtalonesviajestbl tl, trviajestractorestbl vi, catractorestbl ca ".
							"WHERE ud.vin ='".$arrHyu[$i]['vin']."' ".
							"AND ud.estatus =1 ".
							"AND ud.idTalon = tl.idTalon ".
							"AND tl.idViajeTractor = vi.idViajeTractor ".
							"AND vi.idTractor = ca.idTractor ";
			$rsTractor =fn_ejecuta_query($sqlTractor);

				

			fwrite($archivo,"ALB"."  ".substr($arrHyu[$i]['fechaEvento'],0,4).substr($arrHyu[$i]['fechaEvento'],5,2).
				substr($arrHyu[$i]['fechaEvento'],8,2).substr($arrHyu[$i]['fechaEvento'],11,2).substr($arrHyu[$i]['fechaEvento'],14,2).substr($arrHyu[$i]['fechaEvento'],17,2));
			fwrite($archivo,$arrHyu[$i]['vin']."P".sprintf("%-5s",$arrHyu[$i]['cveLoc']).$cveDis.sprintf("%-5s",$arrHyu[$i]['cveDisFac'])."D".sprintf("%-12s",$arrHyu[$i]['chipNum'])."000000000000000");
			fwrite($archivo,substr($arrHyu[$i]['fechaEvento'],0,4).substr($arrHyu[$i]['fechaEvento'],5,2).substr($arrHyu[$i]['fechaEvento'],8,2));
			fwrite($archivo,/*$arrHyu[$i]['folio']*/sprintf('%06d',$FolioRst['root'][0]['folio']).sprintf('%06d',$rsTractor['root'][0]['tractor'])."09".sprintF('%02d',sizeof($arrHyu)).$arrHyu[$i]['distribuidor'].PHP_EOL);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('ALB', '".
										$arrHyu[$i]['centroDistribucion']."', '".
										$arrHyu[$i]['vin']."', '".
										$arrHyu[$i]['fechaEvento']."', '".
										$arrHyu[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
	

			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='EH' WHERE vin='".$arrHyu[$i]['vin']."'"." AND cveStatus ='AH'" ;
			$rstUpt=fn_ejecuta_query($uptdy);
			
			
		}
		//fin de archivo
		$long=(sizeof($arrHyu)+2);
		fwrite($archivo,"ALBT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);	
	}
?>