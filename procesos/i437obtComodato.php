	<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");	
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");
   	require_once("../funciones/phpexcel/PHPExcel.php");


    genera_437CMDAT();

    function genera_437CMDAT(){

    	echo "Inicio 437 Comodato: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

    	$startDate = date('Y-m-d', strtotime("-15 days"));
        $today = date('Y-m-d');
        $todayDateTime = date('Y-m-d H:i:s');
    	
    	$sqlConsulta="SELECT  a6.scacCode, a6.oriSplc, hu.vin, (SELECT date_format(hu2.fechaEvento, '%Y/%m/%d %H:%i:%s') FROM alhistoricounidadestbl hu2 WHERE hu.vin=hu2.vin AND hu2.claveMovimiento='AM') as fechaAM, ".
    					"a6.routeOri, a6.routeDes, a6.desSplc, date_format(hu.fechaEvento, '%Y/%m/%d %H:%i:%s') as fechaEvento,a6.prodStatus ".
						"FROM alhistoricounidadestbl hu, alunidadestbl al, casimbolosunidadestbl ca, cadistribuidorescentrostbl di, al660tbl a6 ".
						"WHERE hu.centroDistribucion IN ('CMDAT') ".
						"AND hu.vin = al.vin ".
						"AND ca.simboloUnidad=al.simboloUnidad ".
						"AND di.distribuidorCentro = al.distribuidor ".
						"AND di.tipoDistribuidor ='DX' ".
						"AND ca.marca not in('KI','HY') ".
						"AND hu.claveMovimiento IN ('OM','OK') ".
						"AND CAST(hu.fechaEvento AS DATE) BETWEEN CAST('".$startDate."' AS DATE) AND CAST('".$today."' AS DATE) ".
						"AND hu.idTarifa != '13' ".
						"AND hu.vin=a6.vin ".						
						"AND a6.ScacCode='XTRA' ".
						"AND a6.id660= (SELECT max(al660.id660) from al660tbl al660 where al660.vin=a6.vin AND al660.ScacCode='XTRA') ".
						"AND hu.vin not in (SELECT tr.vin FROM altransaccionunidadtbl tr where tr.vin=hu.vin AND tipoTransaccion='540')";
		$rs=fn_ejecuta_query($sqlConsulta);


		if ($rs['records'] != null) {
			//fn_genera_archivo($rs['root'],$rs['records'],$countSegSc);
			generaEXCEL($rs);
			$countSeg[]=sizeof($rs['root']);

			$countSegSc=array_sum($countSeg);

			//echo $countSegSc;
		}
		else{
			echo "no existen unidades por transmitir";
		}	
	}			

	function generaEXCEL($rs){

		$updFolio="UPDATE trfoliostbl SET folio='1' WHERE centroDistribucion='CMDAT' and compania='I' and tipoDocumento='540'";
		fn_ejecuta_upd($updFolio);

		$Nombre = "540obt.xlsx";
		$formato = 'Excel2007';
		$Excel = new PHPExcel();

		date_default_timezone_set('America/Mexico_City');


		$fileDir="C:/carbook/i437obtCMDAT/";

		$Excel->getProperties()->setCreator("CARBOOK")
		->setTitle("540obt");

		
		$Excel->setActiveSheetIndex(0);
		$Excel->getActiveSheet()->setTitle('Hoja1')
			->setCellValue('A1', "SCAC *")
			->setCellValue('B1', "SPLC ORIGIN *")
			->setCellValue('C1', "VIN *")
			->setCellValue('D1', "PICKUP DATE *")
			->setCellValue('E1', "ROUTE ORIGIN *")
			->setCellValue('F1', "ROUTE DESTINATION *")
			->setCellValue('G1', "DESTINATION SPLC *")
			->setCellValue('H1', "DELIVERY DATE *")
			->setCellValue('I1', "INVOICE NUMBER *");
			

		for ($i=0;  $i < count($rs['root']) ; $i++) {

		
			$sqlFolio="SELECT * FROM trfoliostbl ".
						"WHERE centroDistribucion='CMDAT' ".
						"AND tipoDocumento='540' ";
			$rsFolio=fn_ejecuta_query($sqlFolio);

			$fechaFolio = $fecha = date("Ymd");

			$folio =substr($fechaFolio,2,6).sprintf("%'.04d\n",$rsFolio['root'][0]['folio']);


			$Excel->setActiveSheetIndex(0);
			$Excel->getActiveSheet()->setTitle('Hoja1')



			->setCellValue('A'. ($i+2), $rs['root'][$i]['scacCode'])

			->setCellValue('B'. ($i+2), $rs['root'][$i]['oriSplc'])

			->setCellValue('C'. ($i+2), $rs['root'][$i]['vin'])

			->setCellValue('D'. ($i+2), substr($rs['root'][$i]['fechaAM'],0,16))
  
			->setCellValue('E'. ($i+2), $rs['root'][$i]['routeOri'])

			->setCellValue('F'. ($i+2), $rs['root'][$i]['routeDes'])

			->setCellValue('G'. ($i+2), $rs['root'][$i]['desSplc'])
			
			->setCellValue('H'. ($i+2), substr($rs['root'][$i]['fechaEvento'],0,16))
			
			->setCellValue('I'. ($i+2), $folio);

			$folionuevo=$rsFolio['root'][0]['folio'] +1;

			$updFolio="UPDATE trfoliostbl SET folio='".$folionuevo."' WHERE centroDistribucion='CMDAT' and compania='I' and tipoDocumento='540'";
			fn_ejecuta_upd($updFolio);

			$today = date("Y-m-d H:i:s");
			$fecha = substr($today,0,10);
			$hora=substr($today,11,8);
			$fecha1 = $rs['root'][$i]['fechaEvento'];

			$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".					 					
								"VALUES ('540','CMDAT','".$folio."','".
								$rs['root'][$i]['vin'].
								"','".$today.
								"','OM','".
								$fecha1."','".
								$rs['root'][$i]['prodStatus']."','".
								$fecha."','".
								$hora."') ";    

			fn_ejecuta_query($sqlAddTransaccion);

		}

		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename="540obt.xlsx"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($Excel, 'Excel2007');
		$objWriter->save($fileDir.$Nombre);	

	}

	echo json_encode(array('succes'=>true,'msjResponse'=>"Operacion completada correctamente"));           

?>  