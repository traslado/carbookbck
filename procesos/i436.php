<?php
		$_REQUEST['zonaHoraria'] = date_default_timezone_get();
		session_start();
		require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/utilidadesProcesos.php");        
    
    switch($_REQUEST['actionHdn']){
        case 'getCargaVIN':
            getCargaVIN();
            break;
        case 'transmite':
            genera436();
            break;
        default:
            echo 'no esta entrando a ningun proceso.'; 
    }

    function getCargaVIN(){
        $a = array();
        $a['successTitle'] = "Manejador de Archivos";
        $a['msjResponse'] = "Carga efectuada.";

        if(isset($_FILES)) {
            if($_FILES["archivo"]["error"] > 0){
                $a['success'] = false;
                $a['msjResponse'] = $_FILES["archivo"]["error"];

            } else {
                $temp_file_name = $_FILES['archivo']['tmp_name'];
                $original_file_name = $_FILES['archivo']['name'];

                // Find file extention
                $ext = explode ('.', $original_file_name);
                $ext = $ext [count ($ext) - 1];

                // Remove the extention from the original file name
                $file_name = str_replace ($ext, '', $original_file_name);

                $new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
                
                //$temp_file_name = '../../tmp/demo.xlsx';											//S�lo para probar por fuera
                //$new_name = '../../respaldos/demo.xlsx';											//S�lo para probar por fuera                
                //$a['root'] = leerXLS($new_name);															//S�lo para probar por fuera
                                    					  
               	//comentar para pruebas
                if (move_uploaded_file($temp_file_name, $new_name)){
                    if (!file_exists($new_name)){
                        $a['success'] = false;
                        $a['msjResponse'] = "Error al procesar el archivo " . $new_name;
                    } else {
                        $a['archivo'] = $file_name . $ext;
                        $x = leerXLS($new_name);
                        $a['success'] = $x['success'];
												$a['msjResponse'] = $x['msjResponse'];
								        $a['nombreArchivo'] = $x['nombreArchivo'];
								        $a['rutasDiferentes'] = $x['rutasDiferentes'];
                    }
                } else {
                    $a['success'] = false;
                    $a['msjResponse'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
                }
            }
        } else {
            $a['success'] = false;
            $a['msjResponse'] = "Error en archivo."; 
        }
         
        echo json_encode($a);
    }

		function leerXLS($inputFileName) {
        /** Error reporting */
        error_reporting(E_ERROR);		//CHK

        //Establece la zona horaria
				date_default_timezone_set($_REQUEST['zonaHoraria']);
				
				$a 					 = array('success' =>true, 'sql' =>'', 'msjResponse' => "Carga efectuada.", 'root' => '', 'records' => 0);
				$unidOK  		 = array();
				$unidMalRuta = array('root' => '', 'records' => 0);
				
				$fechaActual = date('Y/m/d');
				$horaActual  = date('H:i:s');

        //  Include PHPExcel_IOFactory
        include '../funciones/Classes/PHPExcel/IOFactory.php';

        //$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
        //echo $inputFileName;
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader =
             PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error cargando el archivo "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //Se obtiene el valor de cada registro de la columna "M" sin tomar en cuenta el cabecero
        $rowData 	 = $sheet->rangeToArray('M2:' . $highestColumn . $highestRow);
        $rowSerie  = $sheet->rangeToArray('L2:' . 'L' . $highestRow);        
        $rowOrigen = $sheet->rangeToArray('B2:' . 'B' . $highestRow);
        $rowVIN    = $sheet->rangeToArray('A2:' . 'A' . $highestRow);
        //var_dump('SerOri',$rowSerie);
        
        $i = 0;
        $j = 0;
        $m = 0;
        foreach($rowOrigen as $idx)
        {
        		$aux = explode(" ",$idx[0]);
        		$origenOBT = $aux[0];
        		$vin = '';
        		if(!empty($origenOBT))
        		{
        				if($origenOBT == 'TRATLYD' || $origenOBT == 'TRASLYD')
        				{
        					
		        				//var_dump($origenOBT);
		        				$k = 0;
						        foreach($rowVIN as $row2)
						        {
						        		if($j == $k)
						        		{
						        				$aux = explode(" ",$row2[0]);
								        		$vin = $aux[0];
						        				break;
						        		}				        		
						        		$k++;
						        }
				        		if(!empty($vin))
				        		{				        
				        				//var_dump($vin);
				        				$sql = "SELECT DISTINCT origendetramo AS origenRubicon FROM alCargaBlueReportTbl".
				        				       " WHERE vin = '".$vin."'";
				        				//echo "$sql<br>";
				        				$rstOri = fn_ejecuta_query($sql);		
				        				//var_dump($rstOri);

				        				$rstOri['records']='1';

				        				//echo $rstOri['records'];
				        				if($rstOri['records'] > 0)
				        				{
				        					$rstOri['root'][0]['origenRubicon']=$origenOBT;
				        						if($rstOri['root'][0]['origenRubicon'] == $origenOBT)
				        						{
								        				$k = 0;
												        foreach($rowData as $row3)
												        {
												        		if($j == $k)
												        		{
								        								$unidOK[$i] = $row3;
												        				break;
												        		}				        		
												        		$k++;
												        }
								        				$k = 0;
												        foreach($rowSerie as $row3)
												        {
												        		if($j == $k)
												        		{
								        								$rowAuxSerie[$i] = $row3;
								        								$i++;									        								
												        				break;
												        		}				        		
												        		$k++;
												        }												        
				        						}
				        						else
				        						{
																$unidMalRuta['root'][$m]['vin'] 				= $vin;
																$unidMalRuta['root'][$m]['oriOBT'] 			= $origenOBT;
																$unidMalRuta['root'][$m]['oriRubicon'] 	= $rstOri['root'][0]['origenRubicon'];
																$m++;				        								
				        						}
				        				}
				        				else
				        				{
														/*$unidMalRuta['root'][$m]['vin'] 				= $vin;
														$unidMalRuta['root'][$m]['oriOBT'] 			= 'NO EXISTE REGISTRO EN BLUE REPORT';
														$unidMalRuta['root'][$m]['oriRubicon'] 	= 'NO EXISTE REGISTRO EN BLUE REPORT';
														$m++;	*/
														$unidMalRuta['root'][$m]['vin'] 				= $vin;
																$unidMalRuta['root'][$m]['oriOBT'] 			= $origenOBT;
																$unidMalRuta['root'][$m]['oriRubicon'] 	= $rstOri['root'][0]['origenRubicon'];
																$m++;	

														
				        				}
		        				}   						
        				}
        				else
        				{
        					
		        				$k = 0;
						        foreach($rowData as $row3)
						        {
						        		if($j == $k)
						        		{
		        								$unidOK[$i] = $row3;
						        				break;
						        		}				        		
						        		$k++;
						        }	
		        				$k = 0;
						        foreach($rowSerie as $row3)
						        {
						        		if($j == $k)
						        		{
		        								$rowAuxSerie[$i] = $row3;
								        		$i++;			        								
						        				break;
						        		}				        		
						        		$k++;
						        }							        
        				}
        		}
        		$j++;
        }
        $rowSerie = $rowAuxSerie;
				$unidMalRuta['records'] = $m;        
        
        //var_dump('DATA',$rowData);
        //var_dump('OK',$unidOK);
        //var_dump('Serie',$rowSerie);
        //var_dump('Mala Ruta',$unidMalRuta);

        //-------------Hacemos algunas validaci�nes propias de un archivo v�lido de Excel----------------

        if (ord(strtoupper($highestColumn)) != 13 && $highestRow <= 1) {            
            $a['success'] = false;
            $a['msjResponse'] = '<b>Archivo incorrecto.</br> Ingrese un archivo con datos completos.</b>';
        }
        
        /*
        if (ord(strtoupper($highestColumn)) < 2) {
            $root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
            return $root;
        }
        if (strlen($rowData[0][0]) < 1) {
            $root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [No.Comprobante-fechaEvento-litros-importe,placas]', 'fail'=>'Y');
            return $root;
        }
        */
        //------------------------------------------------------------------------------------------------

        if($a['success'])
        {
		        $totalIns = 0;
		        for($row = 0; $row<sizeof($unidOK); $row++){
		        	
		        
				        $rowArr 	 = explode("|", $unidOK[$row][0]);
				        $rowSerArr = explode("|", $rowSerie[$row][0]);
				        //var_dump($rowArr);
				        
				        $numavanzada = (empty($rowSerArr[0]))?$rowArr[0]:$rowSerArr[0];	
				        $scaccode 	 = $rowArr[1];
				        $prodstatus  = $rowArr[2];
				        $orisplc 		 = $rowArr[3];
				        $dessplc 		 = $rowArr[4];
				        $vin 				 = $rowArr[5];
				        $routeori 	 = $rowArr[6];
				        $routedes 	 = $rowArr[7];
				        $von 				 = $rowArr[8];
				        $dealerid 	 = $rowArr[9];
				        $model 			 = $rowArr[18];
				        $colorcode 	 = $rowArr[21];		        
				        $startdate   = '';
				        $enddate		 = '';
				        $dealerid2   = '';		    
				        $vupdate   	 = $fechaActual;
				        $vuptime   	 = $horaActual;
				        $estimdate   = '';
				        $expedite    = '';
				        $merlcode    = '';
				        $city   		 = '';
				        $state			 = '';		        
				        $ladingdes	 = '';
				        $authoriza	 = '';
				        $weight			 = '';
				        $height			 = '';
				        $lenght			 = '';
				        $width			 = '';
				        $volume			 = '';
				        $tipoOrigen	 = '';
				        $estatus		 = '';
				        		        
				        /*
				        var_dump('Serie',$serie);
				        var_dump('scaccode',$scaccode);
				        var_dump('prodstatus',$prodstatus);
				        var_dump('orisplc',$orisplc);
				        var_dump('dessplc',$dessplc);
				        var_dump('vin',$vin);
				        var_dump('routeori',$routeori);
				        var_dump('routedes',$routedes);
				        var_dump('von',$von);
				        var_dump('dealerid',$dealerid);
				        var_dump('model',$model);
				        var_dump('colorcode',$colorcode);
				        echo "<br><br><br>";
				        */
				        
				        if(!empty($numavanzada))
				        {
				        		//var_dump($numavanzada);		                
		                $sql = "INSERT INTO al660Tbl (vin, numavanzada, scaccode, prodstatus, orisplc, dessplc, routeori, routedes, von, ".
			      							 "dealerid, startdate, enddate, dealerid2, vupdate, vuptime, estimdate, expedite, merlcode, model, city, ".
			      							 "state, colorcode, ladingdes, authoriza, weight, height, lenght, width, volume, tipoOrigen, estatus) VALUES('".	                      				
										        $vin."', '".
										        $numavanzada."', '".
										        $scaccode."', '".
										        $prodstatus."', '".
										        $orisplc."', '".
										        $dessplc."', '".										        
										        $routeori."', '".
										        $routedes."', '".
										        $von."', '".
										        $dealerid."', '".
										        $startdate."', '".
										        $enddate."', '".
										        $dealerid2."', '".
										        $vupdate."', '".
										        $vuptime."', '".
										        $estimdate."', '".
										        $expedite."', '".
										        $merlcode."', '".
										        $model."', '".
										        $city."', '".
			    									$state."', '".
										        $colorcode."', '".
										        $ladingdes."', '".
										        $authoriza."', '".
										        $weight."', '".
										        $height."', '".
										        $lenght."', '".
										        $width."', '".
										        $volume."', '".
										        $tipoOrigen."', '".
										        $estatus."');";
		                //echo "$sql<br>";
		                fn_ejecuta_query($sql);		
		                
										if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
												$a['success'] = false;
												$a['sql'] = $sql;
												$a['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del registro 660.";
												break;
										}
										$a['root'][$totalIns]['numAvanzada'] = $numavanzada;
										$totalIns++;								
										$a['records'] = $totalIns;
				        }
				        
				        if($a['success'])
				        {
				        		//$a['msjResponse'] = 'Agregado correctamente';
				      	}
		        }        		
        }
        
        if($a['success'])
        {
        		$x = genera436($a['root'], $a['records']);
        		
        		$a['success'] = $x['success'];
						$a['msjResponse'] = $x['msjResponse'];
						$a['sql'] = $x['sql'];
        }
        
        if($unidMalRuta['records'] > 0)
        {
        		$a['nombreArchivo'] = generaExcel($unidMalRuta);
        }
        $a['rutasDiferentes'] = $unidMalRuta['records'];
        
        return $a;
    }

    function genera436($root, $records){
    	//echo "inicio 436";
    	
    	$x = array('success' =>false, 'sql' =>'', 'msjResponse' => 'Carga Efectuada.', 'transmisiones' => '', 'root' => $root, 'records' => $records);

	    $fechaAnt = strtotime ( '-20 day' , strtotime (date("Y-m-d H:i:s")) ) ;
	    $fechaAnt = date ( 'Y-m-d', $fechaAnt );
	    $fechaAnt = '2017-05-01';

	    $today=date("Y-m-d");
	    $records = 0;
	    $t = 0;
	    $arrVIN = array();

			//var_dump($x['root']);

	    #>>>> AGUASCALIENTES; SALTILLO; <<<<<
	    foreach($x['root'] as $row => $idx)
	    {
	    		//var_dump($idx['numAvanzada']);
			    $sql = "SELECT DISTINCT su.clasificacion, a6.prodstatus, hu.centroDistribucion, au.avanzada, hu.fechaEvento, ct.tarifa, hu.distribuidor, hu.claveMovimiento,au.simboloUnidad, hu.vin,a6.orisplc,a6.dealerid, a6.dealerid2, a6.routeori ,a6.routedes,a6.vupdate,a6.vuptime,tl.folioTimbrado, 0 AS totalReg ".
			    //$sql = "SELECT  distinct a6.oriSplc, COUNT(*) AS totalReg ".
									"FROM  alHistoricoUnidadesTbl hu ,alUnidadesTbl au ,caTarifasTbl ct  ,caSimbolosUnidadesTbl su  ,al660Tbl a6, caDistribuidoresCentrosTbl cd, trunidadesdetallestalonestbl tr,trtalonesviajestbl tl ".
									"WHERE hu.vin = au.vin  ".
									"AND   hu.claveMovimiento IN ('ED','OM','ER','RO','PT','EP')  ".
									//"AND   hu.distribuidor IN  ('M8220','M8221','M8270','M8271') ".
									"AND   hu.centroDistribucion IN ('CDSAL','CDAGS') ".
									"AND   hu.fechaEvento >= '".$fechaAnt."'".									
									"AND   hu.idTarifa = ct.idtarifa  ".
									//"AND   ct.tipoTarifa != 'E' ".
									"AND   hu.idTarifa != 5  ".									
									"AND   au.simboloUnidad = su.simboloUnidad ".
									"AND   (au.tipoCambioDestino != 'FLOTILLA' OR au.tipoCambioDestino IS NULL) ".
									"AND   su.marca not in('KI','HY') ".							
									"AND   cd.distribuidorCentro = hu.distribuidor ".
									"AND   cd.tipoDistribuidor = 'DI' ".
									"AND   hu.vin  NOT IN (SELECT tu.vin  FROM alTransaccionUnidadTbl tu  WHERE tu.tipoTransaccion = ('510')) ".
									"AND   hu.vin= a6.vin ".
									 "and tr.vin=hu.vin ".
       								 "and tr.idtalon=tl.idtalon ".
       								 "AND tl.claveMovimiento='TE' ".
									"AND   a6.numavanzada = '".$idx['numAvanzada']."' ".
									"AND   au.avanzada = a6.numavanzada  ".
									"AND   a6.vupdate  = (SELECT MAX(a60.vupdate) FROM al660tbl a60 WHERE a6.vin = a60.vin ) ".
									"AND   a6.orisplc !=''  ".
									"AND   a6.dessplc  !=''   ".
									"AND   a6.dealerid !=''   ".
									"AND   a6.prodStatus !='SA' ".
									"AND   a6.scacCode='XTRA' ".
									//"and a6.oriSplc = '922786000' ".			//CHK del
									"GROUP BY a6.oriSplc ".
									"ORDER BY tl.folioTimbrado";
					//echo "$sql<br>";	 				
					$rs1 = fn_ejecuta_query($sql);   					
					if($rs1['records'] > 0)
					{
							//var_dump($rs1);
							for($z=0;$z<$rs1['records'];$z++)
							{
									if(!in_array($rs1['root'][$z]['vin'], $arrVIN)){
											$arrVIN[] = $rs1['root'][$z]['vin'];
											
											$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
														"WHERE hu.vin = '".$rs1['root'][$z]['vin']."' ".
														//"AND hu.centroDistribucion = '".$rs1['root'][$z]['centroDistribucion']."' ".
														"AND hu.claveMovimiento IN ('AM') ".
														"AND hu.idTarifa != 5";
											//echo "$sql<br>";	 				
											$rsAM = fn_ejecuta_query($sql);   																					
											
											if($rs1['root'][$z]['vin'] == '9BD265552L9150781')
											{
													//echo "$sql<br>";
											}
											
											$rs1['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs1['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
											$rs1['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs1['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);
											$fechaAM = date("Y-m-d",strtotime($rs1['root'][$z]['vupdate']));											
											
											$sql = "SELECT MAX(hu.fechaEvento) AS fechaOM FROM alHistoricoUnidadesTbl hu ".
														"WHERE hu.vin = '".$rs1['root'][$z]['vin']."' ".
														//"AND hu.centroDistribucion = '".$rs1['root'][$z]['centroDistribucion']."' ".
														"AND hu.claveMovimiento IN ('OM') ".
														"AND hu.idTarifa != 5";
											//echo "$sql<br>";	 				
											$rsOM = fn_ejecuta_query($sql);  											
											
											$rs1['root'][$z]['fechaEvento'] = ($rsOM['records'] == 0 || empty($rsOM['root'][0]['fechaOM']))?$rs1['root'][$z]['fechaEvento']:$rsOM['root'][0]['fechaOM'];
											$fechaOM = date("Y-m-d",strtotime(substr($rs1['root'][$z]['fechaEvento'],0,10)));											

											if($fechaOM < $fechaAM)
											{
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs1['root'][$z]['vin']."' ".
																//"AND hu.centroDistribucion = '".$rs1['root'][$z]['centroDistribucion']."' ".
																"AND hu.claveMovimiento IN ('AM') ".
																"AND SUBSTRING(hu.fechaEvento,1,10) <= '".$fechaOM."'".
																"AND hu.idTarifa != 5";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);
													
													if($rsAM['records'] > 0)
													{ 													
															$rs1['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs1['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
															$rs1['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs1['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);															
													}
											}	
											
											//Verifica el tarctor 12
											$sql = "SELECT DISTINCT ct.* FROM trUnidadesDetallesTalonesTbl dt, trTalonesViajesTbl tv, trViajesTractoresTbl vt, caTractoresTbl ct".
											       " WHERE dt.vin = '".$rs1['root'][$z]['vin']."'".
											       " AND tv.idTalon = (SELECT MAX(dt2.idTalon) FROM trUnidadesDetallesTalonesTbl dt2 WHERE dt2.vin = '".
											       										$rs1['root'][$z]['vin']."' AND dt2.estatus = 'S')".
											       " AND vt.idViajeTractor = tv.idViajeTractor".
											       " AND ct.idTractor = vt.idTractor".
											       " AND ct.tractor = 12";
											$rsTr12 = fn_ejecuta_query($sql);
//echo "$sql<br>";											
//var_dump($rsTr12['records']);
											if($rsTr12['records'] > 0)
											{
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs1['root'][$z]['vin']."' ".
																"AND hu.idTarifa != 5 ".
																"AND hu.claveMovimiento IN ('AM')";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);  
													
													$rs1['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs1['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
													$rs1['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs1['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);
													
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaOM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs1['root'][$z]['vin']."' ".
																"AND hu.idTarifa != 5 ".
																"AND hu.claveMovimiento IN ('OM')";
													//echo "$sql<br>";	 				
													$rsOM = fn_ejecuta_query($sql);  											
													
													$rs1['root'][$z]['fechaEvento'] = ($rsOM['records'] == 0 || empty($rsOM['root'][0]['fechaOM']))?$rs1['root'][$z]['fechaEvento']:$rsOM['root'][0]['fechaOM'];													
											}
											
											if($rs1['root'][$z]['tarifa'] == '00'){
													$sql = "SELECT distribuidorDestino FROM alDestinosEspecialesTbl WHERE vin = '".$rs1['root'][$z]['vin']."' ORDER BY idDestinoEspecial DESC";
													//echo "$sql<br>";	 				
													$rsDS = fn_ejecuta_query($sql);
													if($rsDS['records'] > 0){
															//$rs1['root'][$z]['dealerid'] = $rsDS['root'][0]['distribuidorDestino'];		//Se deshabilita por el momento por instruccion de OB
													}
											}											

											$find = strpos($rs1['root'][$z]['distribuidor'], 'M8');
											if($find !== false && $rs1['root'][$z]['claveMovimiento'] == 'EP')
											{
													//var_dump($rs1['root'][$z]['distribuidor']);
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs1['root'][$z]['vin']."' ".
																"AND hu.claveMovimiento IN ('EP')";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);
													$rs1['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs1['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
													$rs1['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs1['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8); 													
													
													$rs1['root'][$z]['fechaEvento'] = ($rsAM['records'] == 0 || empty($rsAM['root'][0]['fechaAM']))?$rs1['root'][$z]['fechaEvento']:$rsAM['root'][0]['fechaAM'];
											}													
											
											$x['transmisiones']['root'][$t]['clasificacion']			= $rs1['root'][$z]['clasificacion'];
											$x['transmisiones']['root'][$t]['prodstatus'] 				= $rs1['root'][$z]['prodstatus'];
											$x['transmisiones']['root'][$t]['centroDistribucion'] = $rs1['root'][$z]['centroDistribucion'];
											$x['transmisiones']['root'][$t]['avanzada'] 					= $rs1['root'][$z]['avanzada'];
											$x['transmisiones']['root'][$t]['fechaEvento'] 				= $rs1['root'][$z]['fechaEvento'];
											$x['transmisiones']['root'][$t]['tarifa'] 						= $rs1['root'][$z]['tarifa'];
											$x['transmisiones']['root'][$t]['distribuidor'] 			= $rs1['root'][$z]['distribuidor'];
											$x['transmisiones']['root'][$t]['claveMovimiento'] 		= $rs1['root'][$z]['claveMovimiento'];
											$x['transmisiones']['root'][$t]['simboloUnidad'] 			= $rs1['root'][$z]['simboloUnidad'];
											$x['transmisiones']['root'][$t]['vin'] 								= $rs1['root'][$z]['vin'];
											$x['transmisiones']['root'][$t]['orisplc'] 						= $rs1['root'][$z]['folioTimbrado'];
											$x['transmisiones']['root'][$t]['dealerid'] 					= $rs1['root'][$z]['dealerid'];
											$x['transmisiones']['root'][$t]['dealerid2'] 					= $rs1['root'][$z]['dealerid2'];
											$x['transmisiones']['root'][$t]['routeori'] 					= $rs1['root'][$z]['routeori'];
											$x['transmisiones']['root'][$t]['routedes'] 					= $rs1['root'][$z]['routedes'];
											$x['transmisiones']['root'][$t]['vupdate'] 						= $rs1['root'][$z]['vupdate'];
											$x['transmisiones']['root'][$t]['vuptime'] 						= $rs1['root'][$z]['vuptime'];
											$x['transmisiones']['root'][$t]['totalReg'] 					= $rs1['root'][$z]['totalReg'];
											$t++;											
									}									
							}
							$records = $records + $rs1['records'];
							$x['transmisiones']['records'] = $records;
					}
	    }
	    //var_dump($arrVIN);
	    //var_dump(count($x['transmisiones']));
	    
	    #>>>> TOLUCA y LOS ANGELES<<<<<
	    foreach($x['root'] as $row => $idx)
	    {
	    		//var_dump($idx['numAvanzada']);
			    $sql = "SELECT DISTINCT su.clasificacion, a6.prodstatus, hu.centroDistribucion, au.avanzada, hu.fechaEvento, ct.tarifa, hu.distribuidor, hu.claveMovimiento,au.simboloUnidad, hu.vin,a6.orisplc,a6.dealerid, a6.dealerid2, a6.routeori ,a6.routedes,a6.vupdate,a6.vuptime,a6.vuptime,tl.folioTimbrado, 0 AS totalReg ".
			    //$sql = "SELECT  distinct a6.oriSplc, COUNT(*) AS totalReg ".
									"FROM  alHistoricoUnidadesTbl hu ,alUnidadesTbl au ,caTarifasTbl ct  ,caSimbolosUnidadesTbl su  ,al660Tbl a6, caDistribuidoresCentrosTbl cd, trunidadesdetallestalonestbl tr,trtalonesviajestbl tl ".
									"WHERE hu.vin = au.vin  ".
									"AND   hu.claveMovimiento IN ('ED','OM','ER','OK','RO','PT','EP')  ".
									//"AND   hu.distribuidor IN  ('M8220','M8221','M8270','M8271') ".
									"AND   hu.centroDistribucion IN ('CDTOL', 'CDANG') ".
									"AND   hu.fechaEvento >= '".$fechaAnt."'".									
									"AND   hu.idTarifa = ct.idtarifa  ".
									//"AND   ct.tipoTarifa != 'E' ".
									"AND   hu.idTarifa != 5  ".									
									"AND   au.simboloUnidad = su.simboloUnidad ".
									"AND   (au.tipoCambioDestino != 'FLOTILLA' OR au.tipoCambioDestino IS NULL) ".									
									"AND   su.marca not in('KI','HY') ".							
									"AND   cd.distribuidorCentro = hu.distribuidor ".
									"AND   cd.tipoDistribuidor = 'DI' ".
									"AND   hu.vin  NOT IN (SELECT tu.vin  FROM alTransaccionUnidadTbl tu  WHERE tu.tipoTransaccion = ('510')) ".
									"AND   hu.vin= a6.vin ".
									 "and tr.vin=hu.vin ".
       								 "and tr.idtalon=tl.idtalon ".
       								 "AND tl.claveMovimiento='TE' ".
									"AND   a6.numavanzada = '".$idx['numAvanzada']."' ".
									"AND   au.avanzada = a6.numavanzada  ".
									"AND   a6.vupdate  = (SELECT MAX(a60.vupdate) FROM al660tbl a60 WHERE a6.vin = a60.vin ) ".
									"AND   a6.orisplc !=''  ".
									"AND   a6.dessplc  !=''   ".
									"AND   a6.dealerid !=''   ".
									"AND   a6.prodStatus !='SA' ".
									"AND   a6.scacCode='XTRA' ".
									//"and a6.oriSplc = '922786000' ".			//CHK del
									"GROUP BY a6.oriSplc ".
									"ORDER BY tl.folioTimbrado";
					//echo "$sql<br>";	 				
					$rs2 = fn_ejecuta_query($sql);   						
					if($rs2['records'] > 0)
					{
							//var_dump($rs2);			
							for($z=0;$z<$rs2['records'];$z++)
							{
									$add = 1;
									$encontro = 0;
									if(in_array($rs2['root'][$z]['vin'], $arrVIN)){
											for($w=0;$w<count($arrVIN);$w++)
											{
													//var_dump($x['transmisiones']['root'][$w]);
													if(isset($x['transmisiones']['root'][$w]['vin']) && $x['transmisiones']['root'][$w]['vin'] == $rs2['root'][$z]['vin'] && isset($x['transmisiones']['root'][$w]['distribuidor']) && substr($x['transmisiones']['root'][$w]['distribuidor'],0,2) == 'M8' &&
														isset($x['transmisiones']['root'][$w]['claveMovimiento']) && $x['transmisiones']['root'][$w]['claveMovimiento'] != 'EP')
													{
															$encontro = 1;
															//var_dump($x['transmisiones']['root'][$w]['claveMovimiento']);
															break;
													}
											}
											if(!$encontro)
											{
													$add = 0;
											}
									}
									if($add){
											if(!$encontro)
											{
													$arrVIN[] = $rs2['root'][$z]['vin'];
											}
											
											$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
														"WHERE hu.vin = '".$rs2['root'][$z]['vin']."' ".
														//"AND hu.centroDistribucion = '".$rs1['root'][$z]['centroDistribucion']."' ".
														"AND hu.claveMovimiento IN ('AM') ".
														"AND hu.idTarifa != 5";
											//echo "$sql<br>";	 				
											$rsAM = fn_ejecuta_query($sql);   
											
											$rs2['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs2['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
											$rs2['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs2['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);
											$fechaAM = date("Y-m-d",strtotime($rs2['root'][$z]['vupdate']));
											
											$sql = "SELECT MAX(hu.fechaEvento) AS fechaOM FROM alHistoricoUnidadesTbl hu ".
														"WHERE hu.vin = '".$rs2['root'][$z]['vin']."' ".
														//"AND hu.centroDistribucion = '".$rs1['root'][$z]['centroDistribucion']."' ".
														"AND hu.claveMovimiento IN ('OM') ".
														"AND hu.idTarifa != 5";
											//echo "$sql<br>";	 				
											$rsOM = fn_ejecuta_query($sql);  											
											
											$rs2['root'][$z]['fechaEvento'] = ($rsOM['records'] == 0 || empty($rsOM['root'][0]['fechaOM']))?$rs2['root'][$z]['fechaEvento']:$rsOM['root'][0]['fechaOM'];
											$fechaOM = date("Y-m-d",strtotime(substr($rs2['root'][$z]['fechaEvento'],0,10)));																						

											if($fechaOM < $fechaAM)
											{
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs2['root'][$z]['vin']."' ".
																//"AND hu.centroDistribucion = '".$rs1['root'][$z]['centroDistribucion']."' ".
																"AND hu.claveMovimiento IN ('AM') ".
																"AND SUBSTRING(hu.fechaEvento,1,10) <= '".$fechaOM."'".
																"AND hu.idTarifa != 5";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);
													
													if($rsAM['records'] > 0)
													{ 													
															$rs2['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs2['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
															$rs2['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs2['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);															
													}
											}
											
											//Verifica el tarctor 12
											$sql = "SELECT DISTINCT ct.* FROM trUnidadesDetallesTalonesTbl dt, trTalonesViajesTbl tv, trViajesTractoresTbl vt, caTractoresTbl ct".
											       " WHERE dt.vin = '".$rs2['root'][$z]['vin']."'".
											       " AND tv.idTalon = (SELECT MAX(dt2.idTalon) FROM trUnidadesDetallesTalonesTbl dt2 WHERE dt2.vin = '".
											       										$rs2['root'][$z]['vin']."' AND dt2.estatus = 'S')".
											       " AND vt.idViajeTractor = tv.idViajeTractor".
											       " AND ct.idTractor = vt.idTractor".
											       " AND ct.tractor = 12";
											$rsTr12 = fn_ejecuta_query($sql);

											if($rsTr12['records'] > 0)
											{
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs2['root'][$z]['vin']."' ".
																"AND hu.idTarifa != 5 ".
																"AND hu.claveMovimiento IN ('AM')";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);  
													
													$rs2['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs2['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
													$rs2['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs2['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);
													
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaOM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs2['root'][$z]['vin']."' ".
																"AND hu.idTarifa != 5 ".
																"AND hu.claveMovimiento IN ('OM')";
													//echo "$sql<br>";	 				
													$rsOM = fn_ejecuta_query($sql);  											
													
													$rs2['root'][$z]['fechaEvento'] = ($rsOM['records'] == 0 || empty($rsOM['root'][0]['fechaOM']))?$rs2['root'][$z]['fechaEvento']:$rsOM['root'][0]['fechaOM'];													
											}											
											
											if($rs2['root'][$z]['tarifa'] == '00'){
													$sql = "SELECT distribuidorDestino FROM alDestinosEspecialesTbl WHERE vin = '".$rs2['root'][$z]['vin']."' ORDER BY idDestinoEspecial DESC";
													//echo "$sql<br>";	 				
													$rsDS = fn_ejecuta_query($sql);
													if($rsDS['records'] > 0){
															//$rs2['root'][$z]['dealerid'] = $rsDS['root'][0]['distribuidorDestino'];		//Se deshabilita por el momento por instruccion de OB
													}
											}											
											
											$find = strpos($rs2['root'][$z]['distribuidor'], 'M8');
											//var_dump($rs2['root'][$z]['vin'], $rs2['root'][$z]['distribuidor'], $rs2['root'][$z]['claveMovimiento']);
											if($find !== false && $rs2['root'][$z]['claveMovimiento'] == 'EP')
											{
													//var_dump($rs1['root'][$z]['distribuidor']);
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs2['root'][$z]['vin']."' ".
																"AND hu.claveMovimiento IN ('EP')";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);
													$rs2['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs2['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
													$rs2['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs2['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8); 													
													
													$rs2['root'][$z]['fechaEvento'] = ($rsAM['records'] == 0 || empty($rsAM['root'][0]['fechaAM']))?$rs2['root'][$z]['fechaEvento']:$rsAM['root'][0]['fechaAM'];													
													if($encontro)
													{
															$x['transmisiones']['root'][$w]['clasificacion'] 			= $rs2['root'][$z]['clasificacion'];
															$x['transmisiones']['root'][$w]['prodstatus'] 				= $rs2['root'][$z]['prodstatus'];
															$x['transmisiones']['root'][$w]['centroDistribucion'] = $rs2['root'][$z]['centroDistribucion'];
															$x['transmisiones']['root'][$w]['avanzada'] 					= $rs2['root'][$z]['avanzada'];
															$x['transmisiones']['root'][$w]['fechaEvento'] 				= $rs2['root'][$z]['fechaEvento'];
															$x['transmisiones']['root'][$w]['tarifa'] 						= $rs2['root'][$z]['tarifa'];
															$x['transmisiones']['root'][$w]['distribuidor'] 			= $rs2['root'][$z]['distribuidor'];
															$x['transmisiones']['root'][$w]['claveMovimiento'] 		= 'EP';
															$x['transmisiones']['root'][$w]['simboloUnidad'] 			= $rs2['root'][$z]['simboloUnidad'];
															$x['transmisiones']['root'][$w]['vin'] 								= $rs2['root'][$z]['vin'];
															$x['transmisiones']['root'][$w]['orisplc'] 						= $rs2['root'][$z]['folioTimbrado'];
															$x['transmisiones']['root'][$w]['dealerid'] 					= $rs2['root'][$z]['dealerid'];
															$x['transmisiones']['root'][$w]['dealerid2'] 					= $rs2['root'][$z]['dealerid2'];
															$x['transmisiones']['root'][$w]['routeori'] 					= $rs2['root'][$z]['routeori'];
															$x['transmisiones']['root'][$w]['routedes'] 					= $rs2['root'][$z]['routedes'];
															$x['transmisiones']['root'][$w]['vupdate'] 						= $rs2['root'][$z]['vupdate'];
															$x['transmisiones']['root'][$w]['vuptime'] 						= $rs2['root'][$z]['vuptime'];
															$x['transmisiones']['root'][$w]['totalReg'] 					= $rs2['root'][$z]['totalReg'];
															$add = 0;
													}
											}
											
											if($add){
													$x['transmisiones']['root'][$t]['clasificacion']			= $rs2['root'][$z]['clasificacion'];
													$x['transmisiones']['root'][$t]['prodstatus'] 				= $rs2['root'][$z]['prodstatus'];
													$x['transmisiones']['root'][$t]['centroDistribucion'] = $rs2['root'][$z]['centroDistribucion'];
													$x['transmisiones']['root'][$t]['avanzada'] 					= $rs2['root'][$z]['avanzada'];
													$x['transmisiones']['root'][$t]['fechaEvento'] 				= $rs2['root'][$z]['fechaEvento'];
													$x['transmisiones']['root'][$t]['tarifa'] 						= $rs2['root'][$z]['tarifa'];
													$x['transmisiones']['root'][$t]['distribuidor'] 			= $rs2['root'][$z]['distribuidor'];
													$x['transmisiones']['root'][$t]['claveMovimiento'] 		= $rs2['root'][$z]['claveMovimiento'];
													$x['transmisiones']['root'][$t]['simboloUnidad'] 			= $rs2['root'][$z]['simboloUnidad'];
													$x['transmisiones']['root'][$t]['vin'] 								= $rs2['root'][$z]['vin'];
													$x['transmisiones']['root'][$t]['orisplc'] 						= $rs2['root'][$z]['folioTimbrado'];
													$x['transmisiones']['root'][$t]['dealerid'] 					= $rs2['root'][$z]['dealerid'];
													$x['transmisiones']['root'][$t]['dealerid2'] 					= $rs2['root'][$z]['dealerid2'];
													$x['transmisiones']['root'][$t]['routeori'] 					= $rs2['root'][$z]['routeori'];
													$x['transmisiones']['root'][$t]['routedes'] 					= $rs2['root'][$z]['routedes'];
													$x['transmisiones']['root'][$t]['vupdate'] 						= $rs2['root'][$z]['vupdate'];
													$x['transmisiones']['root'][$t]['vuptime'] 						= $rs2['root'][$z]['vuptime'];
													$x['transmisiones']['root'][$t]['totalReg'] 					= $rs2['root'][$z]['totalReg'];
													$t++;													
											}
									}									
							}
							$records = $records + $rs2['records'];
							$x['transmisiones']['records'] = $records;
					}
	    }
	    //var_dump($arrVIN);
	    //var_dump($x['transmisiones']);
	    
	    
	    #>>>> VERACRUZ; SANTA FE; <<<<<
	    foreach($x['root'] as $row => $idx)
	    {
	    		//var_dump($idx['numAvanzada']);
			    $sql = "SELECT DISTINCT su.clasificacion, a6.prodstatus, hu.centroDistribucion, au.avanzada, hu.fechaEvento, ct.tarifa, hu.distribuidor, hu.claveMovimiento,au.simboloUnidad, hu.vin,a6.orisplc,a6.dealerid, a6.dealerid2, a6.routeori ,a6.routedes,a6.vupdate,a6.vuptime,tl.folioTimbrado, 0 AS totalReg ".
			    //$sql = "SELECT  distinct a6.oriSplc, COUNT(*) AS totalReg ".
									"FROM  alHistoricoUnidadesTbl hu ,alUnidadesTbl au ,caTarifasTbl ct  ,caSimbolosUnidadesTbl su  ,al660Tbl a6, caDistribuidoresCentrosTbl cd, trunidadesdetallestalonestbl tr,trtalonesviajestbl tl ".
									"WHERE hu.vin = au.vin  ".
									"AND   hu.claveMovimiento IN ('ED','OM','RO','PT','EP')  ".
									//"AND   hu.distribuidor IN  ('M8220','M8221','M8270','M8271') ".
									"AND   hu.centroDistribucion IN ('CDSFE','CDVER','CDLZC','CMDAT') ".
									"AND   hu.fechaEvento >= '".$fechaAnt."'".									
									"AND   hu.idTarifa = ct.idtarifa  ".
									//"AND   ct.tipoTarifa != 'E' ".
									"AND   hu.idTarifa != 5  ".									
									"AND   au.simboloUnidad = su.simboloUnidad ".
									"AND   (au.tipoCambioDestino != 'FLOTILLA' OR au.tipoCambioDestino IS NULL) ".									
									"AND   su.marca not in('KI','HY') ".							
									"AND   cd.distribuidorCentro = hu.distribuidor ".
									"AND   cd.tipoDistribuidor = 'DI' ".
									"AND   hu.vin  NOT IN (SELECT tu.vin  FROM alTransaccionUnidadTbl tu  WHERE tu.tipoTransaccion = ('510')) ".
									"AND   hu.vin= a6.vin ".
									 "and tr.vin=hu.vin ".
       								 "and tr.idtalon=tl.idtalon ".
       								 "AND tl.claveMovimiento='TE' ".
									"AND   a6.numavanzada = '".$idx['numAvanzada']."' ".
									"AND   au.avanzada = a6.numavanzada  ".
									"AND   a6.vupdate  = (SELECT MAX(a60.vupdate) FROM al660tbl a60 WHERE a6.vin = a60.vin ) ".
									"AND   a6.orisplc !=''  ".
									"AND   a6.dessplc  !=''   ".
									"AND   a6.dealerid !=''   ".
									"AND   a6.prodStatus !='SA' ".
									"AND   a6.scacCode='XTRA' ".
									//"and a6.oriSplc = '922786000' ".			//CHK del
									"GROUP BY a6.oriSplc ".
									"ORDER BY tl.folioTimbrado";
					//echo "$sql<br>";	 				
					$rs3 = fn_ejecuta_query($sql);   		
					if($rs3['records'] > 0)
					{
							//var_dump($rs3);			
							for($z=0;$z<$rs3['records'];$z++)
							{
									//var_dump($rs3['root'][$z]['vin'], $rs3['root'][$z]['distribuidor'], $rs3['root'][$z]['claveMovimiento']);									
									$add = 1;
									$encontro = 0;
									if(in_array($rs3['root'][$z]['vin'], $arrVIN)){
											//var_dump(count($arrVIN));
											for($w=0;$w<count($arrVIN);$w++)
											{
													//var_dump($x['transmisiones']['root'][$w]);
													if(isset($x['transmisiones']['root'][$w]['vin']) && $x['transmisiones']['root'][$w]['vin'] == $rs3['root'][$z]['vin'] && isset($x['transmisiones']['root'][$w]['distribuidor']) && substr($x['transmisiones']['root'][$w]['distribuidor'],0,2) == 'M8' &&
														isset($x['transmisiones']['root'][$w]['claveMovimiento']) && $x['transmisiones']['root'][$w]['claveMovimiento'] != 'EP')
													{
															$encontro = 1;
															//var_dump($x['transmisiones']['root'][$w]['vin']);
															break;
													}
											}
											if(!$encontro)
											{
													$add = 0;
											}
									}
									if($add){																
											if(!$encontro)
											{
													$arrVIN[] = $rs3['root'][$z]['vin'];
											}											

											$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
														"WHERE hu.vin = '".$rs3['root'][$z]['vin']."' ".
														//"AND hu.centroDistribucion = '".$rs1['root'][$z]['centroDistribucion']."' ".
														"AND hu.claveMovimiento IN ('AM') ".
														"AND hu.idTarifa != 5";
											//echo "$sql<br>";	 				
											$rsAM = fn_ejecuta_query($sql);   
											
											$rs3['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs3['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
											$rs3['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs3['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);
											
											$fechaAM = date("Y-m-d",strtotime($rs3['root'][$z]['vupdate']));											
											
											$sql = "SELECT MAX(hu.fechaEvento) AS fechaOM FROM alHistoricoUnidadesTbl hu ".
														"WHERE hu.vin = '".$rs3['root'][$z]['vin']."' ".
														//"AND hu.centroDistribucion = '".$rs3['root'][$z]['centroDistribucion']."' ".
														"AND hu.claveMovimiento IN ('OM') ".
														"AND hu.idTarifa != 5";
											//echo "$sql<br>";	 				
											$rsOM = fn_ejecuta_query($sql);  											
											
											$rs3['root'][$z]['fechaEvento'] = ($rsOM['records'] == 0 || empty($rsOM['root'][0]['fechaOM']))?$rs3['root'][$z]['fechaEvento']:$rsOM['root'][0]['fechaOM'];											
											$fechaOM = date("Y-m-d",strtotime(substr($rs3['root'][$z]['fechaEvento'],0,10)));											

											if($fechaOM < $fechaAM)
											{
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs3['root'][$z]['vin']."' ".
																//"AND hu.centroDistribucion = '".$rs1['root'][$z]['centroDistribucion']."' ".
																"AND hu.claveMovimiento IN ('AM') ".
																"AND SUBSTRING(hu.fechaEvento,1,10) <= '".$fechaOM."'".
																"AND hu.idTarifa != 5";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);
													
													if($rsAM['records'] > 0)
													{ 													
															$rs3['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs3['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
															$rs3['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs3['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);															
													}
											}		

											//Verifica el tarctor 12
											$sql = "SELECT DISTINCT ct.* FROM trUnidadesDetallesTalonesTbl dt, trTalonesViajesTbl tv, trViajesTractoresTbl vt, caTractoresTbl ct".
											       " WHERE dt.vin = '".$rs3['root'][$z]['vin']."'".
											       " AND tv.idTalon = (SELECT MAX(dt2.idTalon) FROM trUnidadesDetallesTalonesTbl dt2 WHERE dt2.vin = '".
											       										$rs3['root'][$z]['vin']."' AND dt2.estatus = 'S')".
											       " AND vt.idViajeTractor = tv.idViajeTractor".
											       " AND ct.idTractor = vt.idTractor".
											       " AND ct.tractor = 12";
											$rsTr12 = fn_ejecuta_query($sql);

											if($rsTr12['records'] > 0)
											{
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs3['root'][$z]['vin']."' ".
																"AND hu.idTarifa != 5 ".
																"AND hu.claveMovimiento IN ('AM')";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);  
													
													$rs3['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs3['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
													$rs3['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs3['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8);
													
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaOM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs3['root'][$z]['vin']."' ".
																"AND hu.idTarifa != 5 ".
																"AND hu.claveMovimiento IN ('OM')";
													//echo "$sql<br>";	 				
													$rsOM = fn_ejecuta_query($sql);  											
													
													$rs3['root'][$z]['fechaEvento'] = ($rsOM['records'] == 0 || empty($rsOM['root'][0]['fechaOM']))?$rs3['root'][$z]['fechaEvento']:$rsOM['root'][0]['fechaOM'];													
											}
											
											if($rs3['root'][$z]['tarifa'] == '00'){
													$sql = "SELECT distribuidorDestino FROM alDestinosEspecialesTbl WHERE vin = '".$rs3['root'][$z]['vin']."' ORDER BY idDestinoEspecial DESC";
													//echo "$sql<br>";	 				
													$rsDS = fn_ejecuta_query($sql);
													if($rsDS['records'] > 0){
															//$rs3['root'][$z]['dealerid'] = $rsDS['root'][0]['distribuidorDestino'];		//Se deshabilita por el momento por instruccion de OB
													}
											}

											$find = strpos($rs3['root'][$z]['distribuidor'], 'M8');
											if($find !== false && $rs3['root'][$z]['claveMovimiento'] == 'EP')
											{
													//var_dump($rs1['root'][$z]['distribuidor']);
													$sql = "SELECT MAX(hu.fechaEvento) AS fechaAM FROM alHistoricoUnidadesTbl hu ".
																"WHERE hu.vin = '".$rs3['root'][$z]['vin']."' ".
																"AND hu.claveMovimiento IN ('EP')";
													//echo "$sql<br>";	 				
													$rsAM = fn_ejecuta_query($sql);
													$rs3['root'][$z]['vupdate'] = ($rsAM['records'] == 0)?$rs3['root'][$z]['vupdate']:substr($rsAM['root'][0]['fechaAM'],0,10);
													$rs3['root'][$z]['vuptime'] = ($rsAM['records'] == 0)?$rs3['root'][$z]['vuptime']:substr($rsAM['root'][0]['fechaAM'],11,8); 													
													
													$rs3['root'][$z]['fechaEvento'] = ($rsAM['records'] == 0 || empty($rsAM['root'][0]['fechaAM']))?$rs3['root'][$z]['fechaEvento']:$rsAM['root'][0]['fechaAM'];
													if($encontro)
													{
															$x['transmisiones']['root'][$w]['clasificacion'] 			= $rs3['root'][$z]['clasificacion'];
															$x['transmisiones']['root'][$w]['prodstatus'] 				= $rs3['root'][$z]['prodstatus'];
															$x['transmisiones']['root'][$w]['centroDistribucion'] = $rs3['root'][$z]['centroDistribucion'];
															$x['transmisiones']['root'][$w]['avanzada'] 					= $rs3['root'][$z]['avanzada'];
															$x['transmisiones']['root'][$w]['fechaEvento'] 				= $rs3['root'][$z]['fechaEvento'];
															$x['transmisiones']['root'][$w]['tarifa'] 						= $rs3['root'][$z]['tarifa'];
															$x['transmisiones']['root'][$w]['distribuidor'] 			= $rs3['root'][$z]['distribuidor'];
															$x['transmisiones']['root'][$w]['claveMovimiento'] 		= 'EP';
															$x['transmisiones']['root'][$w]['simboloUnidad'] 			= $rs3['root'][$z]['simboloUnidad'];
															$x['transmisiones']['root'][$w]['vin'] 								= $rs3['root'][$z]['vin'];
															$x['transmisiones']['root'][$w]['orisplc'] 						= $rs3['root'][$z]['folioTimbrado'];	
															$x['transmisiones']['root'][$w]['dealerid'] 					= $rs3['root'][$z]['dealerid'];
															$x['transmisiones']['root'][$w]['dealerid2'] 					= $rs3['root'][$z]['dealerid2'];
															$x['transmisiones']['root'][$w]['routeori'] 					= $rs3['root'][$z]['routeori'];
															$x['transmisiones']['root'][$w]['routedes'] 					= $rs3['root'][$z]['routedes'];
															$x['transmisiones']['root'][$w]['vupdate'] 						= $rs3['root'][$z]['vupdate'];
															$x['transmisiones']['root'][$w]['vuptime'] 						= $rs3['root'][$z]['vuptime'];
															$x['transmisiones']['root'][$w]['totalReg'] 					= $rs3['root'][$z]['totalReg'];
															$add = 0;
													}
											}
											if($add){											
													$x['transmisiones']['root'][$t]['clasificacion']			= $rs3['root'][$z]['clasificacion'];
													$x['transmisiones']['root'][$t]['prodstatus'] 				= $rs3['root'][$z]['prodstatus'];
													$x['transmisiones']['root'][$t]['centroDistribucion'] = $rs3['root'][$z]['centroDistribucion'];
													$x['transmisiones']['root'][$t]['avanzada'] 					= $rs3['root'][$z]['avanzada'];
													$x['transmisiones']['root'][$t]['fechaEvento'] 				= $rs3['root'][$z]['fechaEvento'];
													$x['transmisiones']['root'][$t]['tarifa'] 						= $rs3['root'][$z]['tarifa'];
													$x['transmisiones']['root'][$t]['distribuidor'] 			= $rs3['root'][$z]['distribuidor'];
													$x['transmisiones']['root'][$t]['claveMovimiento'] 		= $rs3['root'][$z]['claveMovimiento'];
													$x['transmisiones']['root'][$t]['simboloUnidad'] 			= $rs3['root'][$z]['simboloUnidad'];
													$x['transmisiones']['root'][$t]['vin'] 								= $rs3['root'][$z]['vin'];
													$x['transmisiones']['root'][$t]['orisplc'] 						= $rs3['root'][$z]['folioTimbrado'];
													$x['transmisiones']['root'][$t]['dealerid'] 					= $rs3['root'][$z]['dealerid'];
													$x['transmisiones']['root'][$t]['dealerid2'] 					= $rs3['root'][$z]['dealerid2'];
													$x['transmisiones']['root'][$t]['routeori'] 					= $rs3['root'][$z]['routeori'];
													$x['transmisiones']['root'][$t]['routedes'] 					= $rs3['root'][$z]['routedes'];
													$x['transmisiones']['root'][$t]['vupdate'] 						= $rs3['root'][$z]['vupdate'];
													$x['transmisiones']['root'][$t]['vuptime'] 						= $rs3['root'][$z]['vuptime'];
													$x['transmisiones']['root'][$t]['totalReg'] 					= $rs3['root'][$z]['totalReg'];
													$t++;
											}
									}
							}
							$records = $records + $rs3['records'];
							$x['transmisiones']['records'] = $records;
					}
	    }
	    //var_dump($arrVIN);
	    //var_dump($x['transmisiones']);
	    
			if ($x['transmisiones']['records'] != 0) {
									
					foreach ($x['transmisiones']['root'] as $clave => $fila) {
						$oriSplcArr[$clave] = $fila['orisplc'];
					}
					array_multisort($oriSplcArr, SORT_ASC, $x['transmisiones']['root']);
					
					$z = -1;
					$oriSplc = '';
					$oriSplcArr = '';
					foreach ($x['transmisiones']['root'] as $clave => $fila) {
							//var_dump('FILA',$fila);
							if($fila['orisplc'] != $oriSplc)
							{
									$count = 1;
									$z++;
									$oriSplcArr[$z]['orisplc'] 	= $fila['orisplc'];
									$oriSplcArr[$z]['totalReg'] = $count;									
							}
							else
							{
									$count++;
									$oriSplcArr[$z]['totalReg'] = $count;									
							}							
							$oriSplc = $fila['orisplc'];
					}
										
					foreach ($oriSplcArr as $idx => $row) {
							$z = 0;
							foreach ($x['transmisiones']['root'] as $clave => $fila) {
									if($fila['orisplc'] == $row['orisplc'])
									{
											$x['transmisiones']['root'][$z]['totalReg'] = $row['totalReg'];
									}
									$z++;
							}							
					}
				
					//var_dump($oriSplcArr);
					//var_dump('TRA',$x['transmisiones']);
					$z = fn_genera_archivo($x['transmisiones']['root'], $x['transmisiones']['records'], $oriSplcArr);					
					
					$x['success'] = $z['success'];
					$x['msjResponse'] = $z['msjResponse'];
					$x['sql'] = $z['sql'];
			}	    
			else{
					$x['success'] = false;
					$x['msjResponse'] = "<b>No existen unidades por transmitir.</b>";
			}	     
			return $x; 
	}					  		
    
  function fn_genera_archivo($array, $totalRegistros, $oriSplcArr){
		
		$x = array('success' =>true, 'msjResponse' => 'Carga Efectuada.');
		//se Crea Archivo 
		$segmento = 1;
		$countUnds =1;  
		$fileDir="E:/carbook/i436/chr510v.txt";
		$nombreArch = "chr510v.txt";
		$file2Dir="E:/carbook/i436/510v1";
		
		$fileLog = fopen($fileDir, 'a+');		  
		
		//$fp = fopen($file2Dir, 'w');
		
		setlocale(LC_TIME, 'spanish');
		$today = date("Y-m-d");
		$anio = substr($today,0,4);
		$tipoDocto = substr($today,3,1);
		$fechaFull = date("ymdHis");
		$nombreNewArch = '510_'.$fechaFull.'.txt';
		$newArch = "E:/carbook/i436/respaldos/chr510_".$fechaFull.'.txt';
		
	  $imprime = 1;
	  $reg     = 0;
	  $totrec  = 0;
	  $totreg  = 0;
	  $cien    = 1;
	  $cien1   = 1;
	  $oriSplc = '';
	  for ($i=0;$i < sizeof($array);$i++) {
    	$row = $array [$i];	    	 
    	$totrec  = $row['totalReg'];
    	$totdec  = $row['totalReg'] / 100;
    	$segchar = number_format($totdec,2, '.', '');    	    	    	
    	$aux1 = explode('.', $segchar);    	
			$int = str_pad($aux1[0], 4, "0", STR_PAD_LEFT);
			$dec = str_pad($aux1[1], 2, "0", STR_PAD_LEFT);			
			$segmentos = (int) ($int);
			//var_dump($cien1,$segmentos,$row['orisplc']);
			$reg++;
    	if($cien1 > $segmentos)
    	{
    			$totreg = $totrec;
    	}
    	else
    	{
    			$totreg = 100;
    			if($reg == 101)
    			{
    					$reg    	= 1;
    					$imprime 	= 1;
    					$cien++;
    					$totrec = $totrec - 100;
    			}
    			if($cien > $segmentos)
    			{
    					$totreg = $totrec;
    			}
    	}
    	if($oriSplc != $row['orisplc'])
    	{
    			$imprime = 1;
    	}
    	$oriSplc = $row['orisplc'];

			$sqlFactura="SELECT folio AS factura FROM trFoliosTbl WHERE centroDistribucion = 'TCO' AND compania = 'VI' AND tipoDocumento = '".$tipoDocto."'";
			//echo "<br>$sqlFactura<br>";
			$rs=fn_ejecuta_query($sqlFactura);
			if($rs['records'] == 0)
			{
					$sql="INSERT INTO trFoliosTbl (`centroDistribucion`, `compania`, `tipoDocumento`, `folio`) VALUES ('TCO','VI','".$tipoDocto."','0')";
					//echo "<br>$sql<br>";
					fn_ejecuta_query($sql);
					
					if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
						$x['success'] = false;
						$x['sql'] = $sql;
						$x['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del Folio.";
					}
					
					$rs['root'][0]['factura'] = 1;
			}
			else
			{
					$rs['root'][0]['factura'] = $rs['root'][0]['factura'] + 1;
			}
    	
    	//var_dump('REG',$reg);
    	if ($imprime == 1) {
	    		$encabezado1 = array(1 => ':510:');
					$text = getTxt($encabezado1, array_keys($encabezado1)).out('n', 1); //out es un salto de linea
					fwrite($fileLog, $text);
	    		
	    		$encabezado2 = array(	1  	=> 'HDR',
											    			4  	=> 'VISTA',
											    			19  =>'          ',
											    			20	=>'XTRA'  ,
											    			29  => substr($row['orisplc'], 0,9),
											    			38	=>' ',
											    			50  => str_pad($row['totalReg'], 3, " ", STR_PAD_RIGHT));
	    		$text = getTxt($encabezado2, array_keys($encabezado2)).out('n', 1); //out es un salto de linea
			    fwrite($fileLog, $text);
		   		$countSegSc++;

			}

			//echo json_encode($rs2['root'][$i]['row']);

			$sqlFolioTimb="SELECT  * FROM trunidadesdetallestalonestbl a, trtalonesviajestbl b ".
							"where a.idtalon=b.idtalon ".
							"and b.claveMovimiento='TE' ".
							"and a.vin='".$row['vin']."'";
			$rsFolio=fn_ejecuta_query($sqlFolioTimb);
			

			$encabezado3 = array( 1 => 'DTL', 
														4 => str_pad($row['vin'], 17, " ", STR_PAD_RIGHT),														
												   21 => str_pad(substr($row['routeori'], 0,5), 5, " ", STR_PAD_RIGHT),
												   26 => str_pad(substr($row['routedes'], 0,5), 5, " ", STR_PAD_RIGHT),
												   31 => '        ',
												   39 => str_pad(substr($row['dealerid'], 0,5), 5, " ", STR_PAD_RIGHT),
												   44 => '            ',
												   56 => str_pad(substr($row['dealerid'], 0,5), 5, " ", STR_PAD_RIGHT),
												   61 => '          ',
												   71 => date('ymd',strtotime($row['vupdate'])),
												   77 => date('Hi',strtotime($row['vuptime'])),
												   81 => '                    ',
												  101 => date('ymd',strtotime($row['fechaEvento'])),
												  107 => date('Hi',strtotime($row['fechaEvento'])),
												  //111 => str_pad($rsFolio['root'][0]['folioTimbrado'], 13, " ", STR_PAD_LEFT),
												  111 => sprintf('%-13s', $rsFolio['root'][0]['folioTimbrado']),					
												  //111 => str_pad($rs['root'][0]['factura'], 10, "0", STR_PAD_LEFT),
												  //121 => str_pad($countSegSc++, 3, "0", STR_PAD_LEFT),
												  124 => '                     ');
				$text = getTxt($encabezado3, array_keys($encabezado3)).out('n', 1); //out es un salto de linea
				fwrite($fileLog, $text);	    			
				$countUnds=($countUnds==100)?1:$countUnds++;
		
				$vin = $arrS3[$i]['vin'];
				$today = date("Y-m-d H:i:s");
				$fecha = substr($today,0,10);
				$hora=substr($today,11,8);
				$fecha1 = $row['fechaEvento'];    	
		
				$sqlAddTransaccion= "INSERT INTO alTransaccionUnidadTbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".					 					
														"VALUES ('510','".$row['centroDistribucion'].
														"','".$rsFolio['root'][0]['folioTimbrado']."','".
														$row['vin'].
														"','".$fecha1.
														"','".$row['claveMovimiento']."','".
														$today."','".
														$nombreNewArch."','".
														$fecha."','".
														$hora."') ";    
					//echo "$sqlAddTransaccion<br>";
					fn_ejecuta_query($sqlAddTransaccion);
					
					if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
						$x['success'] = false;
						$x['sql'] = $sqlAddTransaccion;
						$x['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten de la Transacci&oacuten.";
					}					
					
					$imprime = 0;	
					
					$cvecia = substr($row['centroDistribucion'],2,3);
					$avanzada = $row['avanzada'];
					$claveMovimiento = $row['claveMovimiento'];
					
					$fechaEvento = $row['claveMovimiento'];
					
					$aux = explode('-',substr($row['fechaEvento'],0,10));
					$anio = $aux[0];				
					$mes = $aux[1];
					$dia = $aux[2];
					$fechaEvento = $dia.'/'.$mes.'/'.$anio;
					
					$aux = explode('/',substr($row['vupdate'],0,10));
					$anio = $aux[0];				
					$mes = $aux[1];
					$dia = $aux[2];
					$vupdate = $dia.'/'.$mes.'/'.$anio;
					
					//fputs($fp, $cvecia. '|'. $avanzada. '|'. $claveMovimiento. '|'. $fechaEvento. '|'. $vupdate. '|'. $row['vuptime']. '|'. $row['prodstatus']. '|');
					//fputs($fp,"\n");
					
					$updFac= "UPDATE trFoliosTbl SET folio = ".$rs['root'][0]['factura']." WHERE centroDistribucion = 'TCO' AND compania = 'VI' AND tipoDocumento = '".$tipoDocto."'";
					//echo "<br>$updFac<br>";
					fn_ejecuta_query($updFac);
					
					if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
						$x['success'] = false;
						$x['sql'] = $updFac;
						$x['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten del Folio.";
					}									
			}
			//fclose($fp);

			//Hace un respaldo del archivo transmitido				
			copy($fileDir, $newArch);
					
			return $x;
		}

    function generaExcel($unidMalRuta){
				require_once('../funciones/Classes/PHPExcel.php');
				require_once('../funciones/Classes/PHPExcel/IOFactory.php');					
				
				$objXLS = new PHPExcel();
				$objSheet = $objXLS->setActiveSheetIndex(0);
				$objSheet->setCellValue('A1', 'VIN');
				$objSheet->setCellValue('B1', 'ORIGEN OBT');
				$objSheet->setCellValue('C1', 'ORIGEN RUBICON');    		
				
				$j = 2;
		    foreach ($unidMalRuta['root'] as $row) {		
		        $objSheet->setCellValue('A'.$j, $row['vin']);
		        $objSheet->setCellValue('B'.$j, $row['oriOBT']);
		        $objSheet->setCellValue('C'.$j, $row['oriRubicon']);
		        $j++;
		    }		
				$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
				$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
				$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);		    		
				
				$objXLS->getActiveSheet()->setTitle('rutasDiferentes');
				$objXLS->setActiveSheetIndex(0);
		    $fecha = date("YmdHis");

				//Verifica si existe el directorio tmp, sino lo crea
				if (!file_exists("../../respaldos/rutasDifOBT-Rubicon/")) {	
						mkdir("../../respaldos/rutasDifOBT-Rubicon/", 0777);
				}
				
				if (file_exists("../../respaldos/rutasDifOBT-Rubicon/")) {	
						$nombreArchivoFull = "../../respaldos/rutasDifOBT-Rubicon/rutasDiferentes_".$fecha.".xls";
						$nombreArchivo = "rutasDiferentes_".$fecha.".xls";
						$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
						$objWriter->save($nombreArchivoFull);				
				}
				return $nombreArchivo;
    }
?> 

  

 






  
								 