<?php
	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	include_once("../funciones/utilidades.php");	
	require_once("../funciones/utilidadesProcesos.php");
	require_once("../json/interfaz SICAgastos.php");
	require_once("../funciones/generalesSICA.php");

	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['int477Hdn']){
        case 'valida':
            valida();
            break;
        case 'inserta':
            generaInterfase();
            break;
        default:
            echo '';
    }

	function valida(){
		$a = array('success' => true, 'msjResponse' => '', 'nombreArchivo'=>'', 'insertaInterface'=>0);
		
		$sql = "SELECT * FROM trInterfacesFechaTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					 " AND tipoInterface = 'C'".    				 # Anticipos de Gastos
					 " AND SUBSTRING(fechaInicio,1,10) = '".$_REQUEST['fechaInicio']."'".
					 " AND SUBSTRING(fechaFinal,1,10) = '".$_REQUEST['fechaInicio']."'";
		//echo "$sql<br>";
		$rsFecha = fn_ejecuta_query($sql);
		//var_dump($rsFecha);
		
		
		$sql = "SELECT COUNT(*) AS totalReg FROM trPolizaInterfaceTbl".
					 " WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					 " AND SUBSTRING(fechaMovimiento,1,10) = '".$_REQUEST['fechaInicio']."'".
					 " AND tipoDocumento IN ('G', 'S')".
					 " AND claveMovimiento = 'C'";
		//echo "$sql<br>";
		$rsGastos = fn_ejecuta_query($sql);
		//var_dump($rsGastos);
		
		$falta = (empty($rsGastos['root'][0]['totalReg']))?0:$rsGastos['root'][0]['totalReg'];
		//var_dump($falta);

		if($falta == 0)					# Generado
		{
				$a['success'] = false;
				$a['msjResponse'] = 'No existen datos con ese criterio.';
				//$a['msjResponse'] = 'No existe informaci&oacuten a transmitir para esa fecha.';
		}
		else
		{
				$a['insertaInterface'] = 1;
		}

		if($a['success'])
		{
				if(($rsFecha['root'][0]['estatus'] == 'G' || $rsFecha['root'][0]['estatus'] == 'T') && $falta == 0)		# Generado o Trasmitido a Contabilidad
				{
						//$a['success'] = false;
						$a['msjResponse'] = 'Interfase ya generada para esa fecha.';
				}
		}
		
		if($a['success'])
		{
				if(($rsFecha['root'][0]['estatus'] == 'G' || $rsFecha['root'][0]['estatus'] == 'T') && $falta > 0)		# Generado o Trasmitido a Contabilidad
				{
						//$a['success'] = false;
						$a['insertaInterface'] = 0;
						$a['msjResponse'] = 'Interfase para esa fecha ya generada, pero hay registros sin acumular.';
				}				
		}
		
		if($a['success'])
		{
				setlocale(LC_TIME, 'spanish');
				$mes = (date("n", strtotime($_REQUEST['fechaInicio'])));
				$nombreMes = strftime("%B",mktime(0, 0, 0, $mes));
				
				$aux = explode('-', $_REQUEST['fechaInicio']);
				$anio = $aux[0];				
				$mes = $aux[1];
				$dia = $aux[2];
				
				$a['nombreArchivo'] = str_pad($_REQUEST['tipo'], 2, "0", STR_PAD_LEFT).str_pad($dia, 2, "0", STR_PAD_LEFT).strtoupper(substr($nombreMes,0,3)).str_pad($anio, 4, " ", STR_PAD_LEFT).'.CON';
		}
		
		echo json_encode($a);
	}
		
	function generaInterfase(){

		##### CUENTAS GTOS #####

		$sql = "SELECT * FROM trPolizaInterfaceTbl".
				" WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
				" AND SUBSTRING(fechaMovimiento,1,10) = '".$_REQUEST['fechaInicio']."'".
				" AND tipoDocumento IN ('G')".
				" AND claveMovimiento = 'C'";
			//echo "$sql<br>";
		$rsGastos = fn_ejecuta_query($sql);

		$sql = "SELECT a.*, b.tipoCuenta".
			 " FROM trGastosViajeTractorTbl a, caConceptosCentrosTbl b".
			 " WHERE a.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
			 " AND SUBSTRING(a.fechaEvento,1,10) = '".substr($rsGastos['root'][0]['fechaPoliza'], 0, 10)."'".
			 //" AND a.folio = '".$row['folio']."'".
			 " AND a.concepto NOT IN (select valor from cageneralestbl where tabla='conceptos' and columna='9000')".
			 " AND a.claveMovimiento != 'GX'".							#>>DIF. DE CANCELADO <<
			 " AND (a.claveMovimiento = 'GC' OR ".						#>>COMPLEMENTO P/POLIZA CONTABLE <<
			 " a.claveMovimiento = 'GP' )   ".							#>>EXCEDENTE ALIMENTOS/DIESEL   <<
			 " AND b.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
			 " AND b.concepto = a.concepto".
			 " ORDER BY a.folio, b.tipoCuenta, a.concepto";
		//echo "$sql<br>";
		$rsCtas = fn_ejecuta_query($sql);


		for ($i=0; $i < sizeof($rsCtas['root']); $i++) { 
			$selCuentaSica="SELECT * FROM conCatCuentasTbl WHERE cuenta ='".$rsCtas['root'][$i]['cuentaContable']."';";
			$rsCuenta=fn_ejecuta_query_SICA($selCuentaSica);
			

			if (sizeof($rsCuenta['root']) == 0) {
				//$arrCuenta[]=$rsCtas['root'][$i]['cuentaContable'];
				$selNombre="SELECT concat(apellidoMaterno,' ', apellidoPaterno,' ', nombre) as nombre FROM caChoferesTbl where claveChofer='".substr($rsCtas['root'][$i]['cuentaContable'],-5)."'";
				$rsNombre=fn_ejecuta_query($selNombre);

				$insertCuenta="INSERT INTO conCatCuentasTbl (claveCompania, cuenta, nombre, afectable, tipo, naturaleza, ubicacion, reportar, manejaSegmentos, numeroSegmentos, divisa, 	codigoAgrupador, RFC, estatus, idUsuarioAct, fechaAct, ipAct, macAddressAct) 
							VALUES ('4', '".$rsCtas['root'][$i]['cuentaContable']."', 
							'".$rsNombre['root'][0]['nombre']."', '1', '1', '1', ' ', '1', '0', '0', 'MXN', ' ', ' ', '1', '1', now(), '10.212.134.204', '10.212.134.204') ";
				$rsIns=fn_ejecuta_query_SICA($insertCuenta);

				$sqlCuenta= "SELECT * FROM  conCatCuentasTbl where cuenta='".$rsCtas['root'][$i]['cuentaContable']."'";
				$rsCuenta=fn_ejecuta_query_SICA($sqlCuenta);

				for ($j=1; $j <= 12; $j++) { 
					$rsConSaldos="INSERT INTO conSaldosTbl (claveCompania, tipo, calendario, periodo, idCuentaContable, idCentroCostos, saldoInicial, cargos, abonos, saldoFinal) ".
								"VALUES ('4', '1', year(now()), '".$j."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '0.00', '0.00', '0.00', '0.00')";
					$rsIns=fn_ejecuta_query_SICA($rsConSaldos);
				}
			}else{

			}
		}

		$a = array('success' => true, 'msjResponse' => 'Interfase generada correctamente.', 'nombreArchivo'=>'', 'existeTmp'=>0);
	
		setlocale(LC_TIME, 'spanish');
		$dir 		= "E:/carbook/i477/respArchivo/";
		
		
		##### GASTOS #####
				
		$arch 	= $_REQUEST['nombreArchivo'];
		$arch1 	= $_REQUEST['nombreArchivo'].'.nvo';

		$sql = "SELECT * FROM trPolizaInterfaceTbl".
				" WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
				" AND SUBSTRING(fechaMovimiento,1,10) = '".$_REQUEST['fechaInicio']."'".
				" AND tipoDocumento IN ('G')".
				" AND claveMovimiento = 'C'";
		//echo "$sql<br>";
		$rsGastos = fn_ejecuta_query($sql);
		$rsGastos1 = $rsGastos;
		//var_dump($rsGastos);
		
		$idx = 0;
		$a['nombreArchivo'] = $dir.$arch;
		$a['nombreArchivo1'] = $dir.$arch1;
		$fp = fopen($a['nombreArchivo'], 'w');
		$fp1 = fopen($a['nombreArchivo1'], 'w');
		foreach($rsGastos['root'] AS $index => $row)
		{
			$sql = "SELECT a.*, b.tipoCuenta".
					" FROM trGastosViajeTractorTbl a, caConceptosCentrosTbl b".
					" WHERE a.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					" AND SUBSTRING(a.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
					" AND a.folio = '".$row['folio']."'".
					" AND a.concepto NOT IN (select valor from cageneralestbl where tabla='conceptos' and columna='9000')".
					" AND a.claveMovimiento != 'GX'".							#>>DIF. DE CANCELADO <<
					" AND (a.claveMovimiento = 'GC' OR ".						#>>COMPLEMENTO P/POLIZA CONTABLE <<
					" a.claveMovimiento = 'GP')".								#>>EXCEDENTE ALIMENTOS/DIESEL   <<
					" AND b.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					" AND b.concepto = a.concepto".
					" ORDER BY a.folio, b.tipoCuenta, a.concepto";
			//echo "$sql<br>";
			$rsCtas = fn_ejecuta_query($sql);
			//var_dump($rsCtas);
			
			$ind = 0;
			foreach($rsCtas['root'] AS $index2 => $row2)
			{
				$cvecia = 4;
				$aux    = explode('-',substr($row2['fechaEvento'],0,10));
				$anio   = $aux[0];
				$mes    = $aux[1];
				$mes1   = $aux[1];
				$dia    = $aux[2];
				$fecha  = $dia.'/'.$mes.'/'.$anio;
										
				if((int) $mes == (int) $row2['mesAfectacion'])
				{
					$anio 	= substr($row2['fechaEvento'],0,4);
					$mes 	= substr($row2['fechaEvento'],5,2);
					$fecpol	= $fecha;
				}
				else
				{
					$mes = $row2['mesAfectacion'];
					if((int) $mes1 < (int) $row2['mesAfectacion'])
					{
						$anio 	= substr($row2['fechaEvento'],0,4) - 1;
						$dia 	= ultimoDiaMes($anio, $mes);
						$fecpol = $dia.'/'.str_pad($mes, 2, "0", STR_PAD_LEFT).'/'.$anio;
					}
					else
					{
						$anio 	= substr($row2['fechaEvento'],0,4);
						$dia 	= ultimoDiaMes($anio, $mes);
						$fecpol = $dia.'/'.str_pad($mes, 2, "0", STR_PAD_LEFT).'/'.$anio;
					}
				}
				$numpol = str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
				$tippol = $_REQUEST['tipo'];
				$ctacon = rtrim($row2['cuentaContable']);
				//var_dump($ctacon);
				$numche = 0;
				$cargo = 0;
				$abono = str_pad($row2['subTotal'], 17, " ", STR_PAD_LEFT);
				if(rtrim($row2['tipoCuenta']) == 'C')
				{
					$cargo = $row2['subTotal'];
					$abono = str_pad('0.00', 17, " ", STR_PAD_LEFT);
				}
				$desmov = 'LIQUIDACION DE GTOS(REM '.str_pad($_REQUEST['i477Remesa'], 2, " ", STR_PAD_LEFT).') '.str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
				if($row2['claveMovimiento'] == 'GC')			# Complemento de gastos
				{
					$desmov = 'COMP LIQUID DE GTOS(REM '.str_pad($_REQUEST['i477Remesa'], 2, " ", STR_PAD_LEFT).') '.str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
				}
				$tippro = 0;
				$ind++;
				$orden = $ind;
				
				fputs($fp, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
				$idx++;
				fputs($fp,"\n");
      	
				# Genera interfase para nueva contabilidad armando cta con Numero de operador y/o num de tractor

				/* $sql = "SELECT cuentaContable FROM caConceptosCentrosTbl ".
						 " WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
						 " AND concepto = '".$row2['concepto']."'";
				//echo "$sql<br>";
				$rsCtaConc = fn_ejecuta_query($sql);
				//var_dump($rsCtaConc);

				$ctacon = rtrim($rsCtaConc['root'][0]['cuentaContable']);						

				$sql = "SELECT ch.cuentaContable FROM trViajesTractoresTbl vt, caChoferesTbl ch ".
						" WHERE vt.idViajeTractor = ".$row2['idViajeTractor'].
						" AND vt.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
						" AND ch.claveChofer = vt.claveChofer";
				//echo "$sql<br>";
				$rsCtaChof = fn_ejecuta_query($sql);
				//var_dump($rsCtaChof);
															
				if(substr($ctacon, 0, 3) == '113' || substr($ctacon, 0, 3) == '116')
				{
					if(substr($row2['centroDistribucion'], 2, 3) == 'SAL' && $row2['concepto'] == '7013' &&
						 (int) rtrim($rsCtaChof['root'][0]['cuentaContable']) >= 8000 && (int) rtrim($rsCtaChof['root'][0]['cuentaContable']) <= 8999)
					{
						$rsCtaChof['root'][0]['cuentaContable'] = 16;										
					}
					$ctacon = armaCuentaContable($ctacon, rtrim($rsCtaChof['root'][0]['cuentaContable']), '0');
				}
				else
				{
					$sql = "SELECT DISTINCT ca.tractor FROM trViajesTractoresTbl tr, caTractoresTbl ca".
							 " WHERE tr.idViajeTractor = ".$row2['idViajeTractor'].
							 " AND ca.idTractor = tr.idTractor";
					//echo "$sql<br>";		
					$rsTractor = fn_ejecuta_query($sql);
					$ctacon = armaCuentaContable($ctacon, rtrim($rsTractor['root'][0]['tractor']), rtrim($rsCtaChof['root'][0]['cuentaContable']));
				}*/
				
				fputs($fp1, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
      			fputs($fp1,"\n");
			}
		}
		fclose($fp);
		fclose($fp1);

		//Verifica si existe el directorio tmp, sino lo crea
		if (!file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
			mkdir('../../carbookv1.2/modules/interfacesContables/tmp/', 0777);
		}
		if (file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
			copy($a['nombreArchivo1'], '../../carbookv1.2/modules/interfacesContables/tmp/'.$arch1);
			$a['existeTmp'] = 1;
		}	
		
		
		##### CUENTAS SUELDOS #####
				
		$arch2 	= $_REQUEST['nombreArchivo'].'.sdo';
		$arch22 = $arch2.'.nvo';

		$sql = "SELECT * FROM trPolizaInterfaceTbl".
				" WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
				" AND SUBSTRING(fechaMovimiento,1,10) = '".$_REQUEST['fechaInicio']."'".
				" AND tipoDocumento IN ('S')".
				" AND claveMovimiento = 'C'";
		//echo "$sql<br>";
		$rsGastos = fn_ejecuta_query($sql);
		$rsSueldos = $rsGastos;
		//var_dump($rsGastos);
				
		if($rsGastos['records'] > 0)
		{
			$idx = 0;
			$a['nombreArchivo2'] = $dir.$arch2;
			$a['nombreArchivo22'] = $dir.$arch22;
			$fp2  = fopen($a['nombreArchivo2'], 'w');
			$fp22 = fopen($a['nombreArchivo22'], 'w');
			$folant = 0;
			foreach($rsGastos['root'] AS $index => $row)
			{						
				if($_REQUEST['tipo'] == '6')
				{
					$sql = "SELECT a.*, b.tipoCuenta".
							" FROM trGastosViajeTractorTbl a, caConceptosCentrosTbl b".
							" WHERE a.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
							" AND SUBSTRING(a.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
							" AND a.folio = '".$row['folio']."'".
							" AND a.claveMovimiento = 'GS'".	# >>> SUELDO PAGADO <<<
							" AND a.concepto = '141'".
							" AND SUBSTRING(a.cuentaContable, 4, 1) = '.'".
							" AND b.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
							" AND b.concepto = a.concepto".
							" ORDER BY 4, 17, 2";			//ORDER BY folio, tipoCuenta, concepto
				}
				else
				{
					$sql = "SELECT a.*, b.tipoCuenta".
							" FROM trGastosViajeTractorTbl a, caConceptosCentrosTbl b".
							" WHERE a.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
							" AND SUBSTRING(a.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
							" AND a.folio = '".$row['folio']."'".
							" AND a.claveMovimiento = 'GS'".	# >>> SUELDO PAGADO <<<
							" AND SUBSTRING(a.cuentaContable, 4, 1) = '.'".
							" AND b.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
							" AND b.concepto = a.concepto".
							" UNION ".
							" SELECT c.*, d.tipoCuenta".
							" FROM trGastosViajeTractorTbl c, caConceptosCentrosTbl d".
							" WHERE c.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
							" AND SUBSTRING(c.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
							" AND c.folio = '".$row['folio']."'".
							" AND c.claveMovimiento = 'GS'".	# >>> SUELDO PAGADO <<<
							" AND c.concepto IN ('138', '137')".
							" AND SUBSTRING(c.cuentaContable, 4, 1) = '.'".
							" AND d.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
							" AND d.concepto = c.concepto".
							" ORDER BY 4, 17, 2";			//ORDER BY folio, tipoCuenta, concepto
				}
				//echo "$sql<br>";
				$rsCtas = fn_ejecuta_query($sql);

				$sqlConcepto="SELECT a.*, b.tipoCuenta ".
							"FROM trGastosViajeTractorTbl a, caConceptosCentrosTbl b ".
							"WHERE a.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
							"AND SUBSTRING(a.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
							"AND a.folio = '".$row['folio']."'".
							"AND a.claveMovimiento = 'GS' ".
							"AND SUBSTRING(a.cuentaContable, 4, 1) = '.' ".
							"AND b.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
							"AND b.concepto = a.concepto ".
							"AND a.concepto='141';";
				$rsConcepto=fn_ejecuta_query($sqlConcepto);

				if ($rsConcepto['records']==0) {
					$cuenta="SELECT * FROM caconceptoscentrostbl WHERE concepto=141 AND centroDistribucion='".$_REQUEST['centroDistribucion']."';";
					$rscuenta=fn_ejecuta_query($cuenta);

					$insConcepto="INSERT INTO trgastosviajetractortbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip, subTotal) ".
								"VALUES ('".$rsCtas['root'][0]['idViajeTractor']."', '141', '".$rsCtas['root'][0]['centroDistribucion']."', '".$rsCtas['root'][0]['folio']."', '".$rsCtas['root'][0]['fechaEvento']."', '".$rscuenta['root'][0]['cuentaContable']."', '".$rsCtas['root'][0]['mes']."', '0.00', 'PAGO NETO', 'GS', '".$rsCtas['root'][0]['usuario']."', '".$rsCtas['root'][0]['ip']."', '0.00')";
					$rsInsConcepto=fn_ejecuta_query($insConcepto);
				}

				for ($i=0; $i < sizeof($rsCtas['root']); $i++) { 
					$selCuentaSica="SELECT * FROM conCatCuentasTbl WHERE cuenta ='".$rsCtas['root'][$i]['cuentaContable']."';";
					$rsCuenta=fn_ejecuta_query_SICA($selCuentaSica);
					

					if (sizeof($rsCuenta['root']) == 0) {
						//$arrCuenta[]=$rsCtas['root'][$i]['cuentaContable'];
						$selNombre="SELECT concat(apellidoMaterno,' ', apellidoPaterno,' ', nombre) as nombre FROM caChoferesTbl where claveChofer='".substr($rsCtas['root'][$i]['cuentaContable'],-5)."'";
						$rsNombre=fn_ejecuta_query($selNombre);

						$insertCuenta="INSERT INTO conCatCuentasTbl (claveCompania, cuenta, nombre, afectable, tipo, naturaleza, ubicacion, reportar, manejaSegmentos, numeroSegmentos, divisa, 	codigoAgrupador, RFC, estatus, idUsuarioAct, fechaAct, ipAct, macAddressAct) 
									VALUES ('4', '".$rsCtas['root'][$i]['cuentaContable']."', 
												'".$rsNombre['root'][0]['nombre']."', '1', '1', '1', ' ', '1', '0', '0', 'MXN', ' ', ' ', '1', '1', now(), '10.212.134.204', '10.212.134.204') ";
						$rsIns=fn_ejecuta_query_SICA($insertCuenta);

						$sqlCuenta= "SELECT * FROM  conCatCuentasTbl where cuenta='".$rsCtas['root'][$i]['cuentaContable']."'";
						$rsCuenta=fn_ejecuta_query_SICA($sqlCuenta);

						for ($j=1; $j <= 12; $j++) { 
							$rsConSaldos="INSERT INTO conSaldosTbl (claveCompania, tipo, calendario, periodo, idCuentaContable, idCentroCostos, saldoInicial, cargos, abonos, saldoFinal) ".
										"VALUES ('4', '1', year(now()), '".$j."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '0.00', '0.00', '0.00', '0.00')";
							$rsIns=fn_ejecuta_query_SICA($rsConSaldos);
						}
					}else{

					}
				}
				//var_dump($rsCtas);
				
				$ind = 0;

				$conceptosDeducciones = array('8000','8001','8002','8003','8004','2331','2227','2228','2332','2233','2230','2231');

				foreach($rsCtas['root'] AS $index2 => $row2)
				{
					if(in_array($row2['concepto'], $conceptosDeducciones))
					{
						$row2['subTotal']= $row2['subTotal'] * -1;
					}

					$cvecia = 4;												
					$aux = explode('-',substr($row['fechaMovimiento'],0,10));
					$anio = $aux[0];				
					$mes = $aux[1];
					$dia = $aux[2];
					$fecha = $dia.'/'.$mes.'/'.$anio;

					$anio 	= (int) substr($row['fechaMovimiento'],0,4);
					$mes 		= (int) substr($row['fechaMovimiento'],5,2);
					$fecpol	= $fecha;
					$numpol = str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
					$tippol = $_REQUEST['tipo'];
					$ctacon = rtrim($row2['cuentaContable']);
					//var_dump($ctacon);
					$numche = 0;
					$cargo = '0.00';
					$abono = $row2['subTotal'];
					if(rtrim($row2['tipoCuenta']) == 'C')
					{
						$cargo = $row2['subTotal'];
						$abono = '0.00';
					}
					$desmov = 'LIQUIDACION DE SDOS(REM '.str_pad($_REQUEST['i477Remesa'], 2, " ", STR_PAD_LEFT).') '.str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
					$tippro = 0;
					$ind++;
					$orden = $ind;
					
					fputs($fp2, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
					$idx++;
					fputs($fp2,"\n");

					# Genera interface para nueva contabilidad armando cta con Numero de operador y/o num de tractor

					/*  	$sql = "SELECT cuentaContable FROM caConceptosCentrosTbl ".
					" WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					" AND concepto = '".$row2['concepto']."'";
					//echo "$sql<br>";
					$rsCtaConc = fn_ejecuta_query($sql);
					//var_dump($rsCtaConc);

					$ctacon = rtrim($rsCtaConc['root'][0]['cuentaContable']);

					$sql = "SELECT ch.cuentaContable FROM trViajesTractoresTbl vt, caChoferesTbl ch ".
					" WHERE vt.idViajeTractor = ".$row2['idViajeTractor'].
					" AND vt.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					" AND ch.claveChofer = vt.claveChofer";
					//echo "$sql<br>";
					$rsCtaChof = fn_ejecuta_query($sql);
					//var_dump($rsCtaChof);

					if(substr($ctacon, 0, 3) == '113' || substr($ctacon, 0, 3) == '116')
					{
					$ctacon = armaCuentaContable($ctacon, rtrim($rsCtaChof['root'][0]['cuentaContable']), '0');
					}
					else
					{
					$sql = "SELECT DISTINCT ca.tractor FROM trViajesTractoresTbl tr, caTractoresTbl ca".
					" WHERE tr.idViajeTractor = ".$row2['idViajeTractor'].
					" AND ca.idTractor = tr.idTractor";
					//echo "$sql<br>";		
					$rsTractor = fn_ejecuta_query($sql);
					//var_dump($rsObs);
					$ctacon = armaCuentaContable($ctacon, rtrim($rsTractor['root'][0]['tractor']), rtrim($rsCtaChof['root'][0]['cuentaContable']));
					}*/

					fputs($fp22, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
					fputs($fp22,"\n");				      	
				}
			}
			fclose($fp2);
			fclose($fp22);
			
			//Verifica si existe el directorio tmp, sino lo crea
			if (!file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				mkdir('../../carbookv1.2/modules/interfacesContables/tmp/', 0777);
			}
			if (file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				copy($a['nombreArchivo22'], '../../carbookv1.2/modules/interfacesContables/tmp/'.$arch22);
				$a['existeTmp'] = 1;
			}				
		}
		else
		{
			$a['msjResponse'] = 'No existe informaci&oacuten a transmitir para sueldos entre las fechas.';
		}
		
		//Se realiza la actualización de la interfase contable		
		foreach($rsGastos1['root'] AS $index => $row)
		{
			$sql = "UPDATE trPolizaInterfaceTbl".
					" SET claveMovimiento = 'T'".					# Transmitido
					" WHERE centroDistribucion = '".$row['centroDistribucion']."'".
					" AND folio = '".$row['folio']."'".
					" AND SUBSTRING(fechaMovimiento,1,10) = '".substr($row['fechaMovimiento'],0,10)."'".
					" AND SUBSTRING(fechaPoliza,1,10) = '".substr($row['fechaPoliza'],0,10)."'".
					" AND concepto = '".$row['concepto']."'".
					" AND tipoDocumento = '".$row['tipoDocumento']."'".
					" AND importe = ".$row['importe'];
			//echo "$sql<br>";
			fn_ejecuta_query($sql);	//CHK
				
			if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
	      	{
				$a['success']		= false;
				$a['msjResponse']	= $_SESSION['error_sql']. " En la actualizaci&oacuten de la Interfase.";
				$a['sql'] = $sql;
				break;
	      	}
		}
		
		foreach($rsSueldos['root'] AS $index => $row)
		{
			$sql = "UPDATE trPolizaInterfaceTbl".
					 " SET claveMovimiento = 'T'".					# Transmitido
					 " WHERE centroDistribucion = '".$row['centroDistribucion']."'".
					 " AND folio = '".$row['folio']."'".
					 " AND SUBSTRING(fechaMovimiento,1,10) = '".substr($row['fechaMovimiento'],0,10)."'".
					 " AND SUBSTRING(fechaPoliza,1,10) = '".substr($row['fechaPoliza'],0,10)."'".
					 " AND concepto = '".$row['concepto']."'".
					 " AND tipoDocumento = '".$row['tipoDocumento']."'".							 
					 " AND importe = ".$row['importe'];
			//echo "$sql<br>";
			fn_ejecuta_query($sql);	 		//CHK
			
			if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
	      	{
				$a['success']		= false;
				$a['msjResponse']	= $_SESSION['error_sql']. " En la actualizaci&oacuten de la Interfase.";
				$a['sql'] = $sql;
				break;
	      	}
		}		

		if($_REQUEST['insertaInterface'] == 1)
		{
			setlocale(LC_TIME, 'spanish');
			$horaActual = date('H:i:s');
			$sql = "INSERT INTO trInterfacesFechaTbl (centroDistribucion, tipoInterface, fechaInicio, fechaFinal, estatus) VALUES (".
					"'".$_REQUEST['centroDistribucion']."', ".
					"'C', ".
					//"NULL, ".
					"'".($_REQUEST['fechaInicio'].' '.$horaActual)."', ".
					"'".($_REQUEST['fechaInicio'].' '.$horaActual)."', ".
					"'G')";
			//echo "$sql<br>";
			fn_ejecuta_query($sql);	 		//CHK
			if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
	      	{
				$a['success']		= false;
				$a['msjResponse']	= $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
				$a['sql'] = $sql;
	      	}
		}

		$patio=$_REQUEST['i477RemesaCmb'];
		$dia=$_REQUEST['i477DiaDtm'];
		$tipo=$_REQUEST['tipo'];
		$numero=$_REQUEST['numero'];
		$tipoPoliza='G';

		$selTipoPoliza="SELECT * FROM conTiposPolizasTbl WHERE claveCompania=4  AND tipoPoliza=".$tipo."";
		$rsTipoPoliza=fn_ejecuta_query_SICA($selTipoPoliza);

		$sqlFolio="SELECT max(numero) as folioMenor from  conPolizasTbl ".
					"where anio='".$anio."' ".
					"AND claveCompania='4' ".
					"and mes='".$mes."' ".
					"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
		$rsFolio=fn_ejecuta_query_SICA($sqlFolio);
		$folio1=floatval($rsFolio['root'][0]['folioMenor']);

		generaInterfaseSICA($patio,$dia,$tipo,$numero,$tipoPoliza);

		// echo json_encode($selTipoPoliza);

		$sqlFolio="SELECT max(numero) as folioMayor from  conPolizasTbl ".
					"where anio='".$anio."' ".
					"AND claveCompania='4' ".
					"and mes='".$mes."' ".
					"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
		$rsFolio=fn_ejecuta_query_SICA($sqlFolio);
		$folio2=$rsFolio['root'][0]['folioMayor'];
		if ($folio2 > $folio1) {
			$folio1++;
		}

		$a['msjResponse'] = "Interfase generada correctamente. Folios SICA del ".$folio1."  al ".$folio2.". Tipo de Poliza ".$rsTipoPoliza['root'][0]['idTipoPoliza']." .";

		// fn_ejecuta_query(false);	//CHK
		// fn_ejecuta_query_SICA(false);	//CHK

		echo json_encode($a);

	}
	
	function ultimoDiaMes($anio, $mes) {
	  return str_pad(date("d",(mktime(0,0,0,$mes+1,1,$anio)-1)), 2, "0", STR_PAD_LEFT);
	}
	
?>
