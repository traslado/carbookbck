<?php
	 // INTERFAZ 815
	 // Archivos a generar
	 // este proceso genera movimientos de los eventos de asignacion (AM) para unidades marca Hyundai y Kia.


	require_once("../funciones/generales.php");
	require_once("../funciones/utilidades.php");
	require_once("../funciones/funcionesGlobales.php");

	date_default_timezone_set('America/Mexico_City');
	$hoy = getdate();
	$fecha = date('Y-m-d', $hoy[0]);

    $fechaAnt = strtotime ( '-15 day' , strtotime (date("Y-m-d")) ) ;
	$fechaAnt = date ( 'Y-m-d' , $fechaAnt );


  	global $dirPath;
	global $inicioFile;
	global $fileName;

	date_default_timezone_set('America/Mexico_City');

	//$dirPath = "C:carbook/i815/";
	$$dirPath = "E:\\carbook\\i816TLP\\";
	$filePath = $dirPath.$inicioFile;
	$ejecutaProceso = true;

	ejecutaTLP();

	function ejecutaTLP(){

		$fechaAnt = strtotime ( '-300 day' , strtotime (date("Y-m-d")) ) ;
	    $fechaAnt = date ( 'Y-m-d' , $fechaAnt );

		$fecha = date('Y-m-j');
		$nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
		$nuevafecha = date ( 'Y-m-j' , $nuevafecha );

		$sqlTLP = "SELECT * FROM alunidadestbl al, alhistoricounidadestbl ud, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = ud.vin ".
					"AND ud.vin = dy.vin ".
					"AND ud.claveMovimiento ='AM' ".
					"AND ud.fechaEvento BETWEEN '".$fechaAnt."' AND '".$nuevafecha."' ".
					//"AND dy.cveStatus in('PK','PH','QK') ".
					"AND dy.cveStatus in('PK') ".
					"AND dy.cveDisFac = ud.distribuidor ".
					"AND ud.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='TLP') ";
		$rsTLP = fn_ejecuta_query($sqlTLP);

		//echo json_encode($rsTLP);

		for ($i=0; $i <sizeof($rsTLP['root']) ; $i++) {
			//echo json_encode($rsTLP['root'][$i]['cveStatus']);
			if ($rsTLP['root'][$i]['cveStatus'] == 'PK' /*|| $rsTLP['root'][$i]['cveStatus'] == 'QK'*/) {
				$arrKIATLP[] = $rsTLP['root'][$i];
			}
		/////////////////////////////////////////////////////////
			else if ($rsTLP['root'][$i]['cveStatus'] == '3H') {
				$arrHYUTLP[] = $rsTLP['root'][$i];
			}
			else{
				$arrTLP[] = $rsTLP['root'][$i];
			}
		}
		//""echo json_encode($arrHYUTLP);
		if (count($arrKIATLP) != 0) {
			//generaTLPKIA($arrKIATLP);
			generaEXTKIA2($arrKIATLP);
		}
		if (count($arrHYUTLP) != 0) {
			generaTLPHYU($arrHYUTLP);
		}
		/*if (count($arrHYUTLP) != 0) {
			generaTLPKIA($arrTLP);
		}*/
	}

	function generaEXTKIA2($arrKIATLP){
		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');

		//$directorio ="C:carbook/i816/";
		$directorio = "E:\\carbook\\i816TLP\\";

		$inicioFile = "KMM_EXT_".$fecha.$hora.".txt";
		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"EXTH"." "."TRA"."  "."GMX"."  "."EXT".$fecha.$hora.PHP_EOL);

		//detalle



		for ($i=0; $i <sizeof($arrKIATLP) ; $i++) {

			if ($arrKIATLP[$i]['centroDistribucion'] == 'CDTOL') {
				$origin = 'FT15';
			}
			if ($arrKIATLP[$i]['centroDistribucion'] == 'CDSFE') {
				$origin = 'FT18';
			}



			fwrite($archivo,"EXT  YE".$arrKIATLP[$i]['vin'].substr($arrKIATLP[$i]['fechaEvento'],0,4).substr($arrKIATLP[$i]['fechaEvento'],5,2).
				substr($arrKIATLP[$i]['fechaEvento'],8,2).substr($arrKIATLP[$i]['fechaEvento'],11,2).substr($arrKIATLP[$i]['fechaEvento'],14,2).
				substr($arrKIATLP[$i]['fechaEvento'],17,2).sprintf('%-5s',$origin).sprintf('%-5s',$arrKIATLP[$i]['distribuidor'])."          "."TRA  T".PHP_EOL);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('TLP', '".
										$arrKIATLP[$i]['centroDistribucion']."', '".
										$arrKIATLP[$i]['vin']."', '".
										$arrKIATLP[$i]['fechaEvento']."', '".
										$arrKIATLP[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
			//echo json_encode($insTransaccion);

			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='AK' WHERE vin='".$arrKIATLP[$i]['vin']."'"." AND cveStatus in('PK')" ;
			$rstUpt=fn_ejecuta_query($uptdy);

		}
		//fin de archivo
		$long=(sizeof($arrKIATLP)+2);
		fwrite($archivo,"EXTT ".sprintf('%06d',($long)).PHP_EOL);
		fclose($archivo);
	}

	/*function generaTLPKIA($arrKIATLP){

		$fecha = date('Ymd');
		$hora = date("His");
		$today =  date('Y-m-d H:i:s');


		$dirPath = "E:\\carbook\\i816TLP\\";
		$inicioFile = "KMM"."_TLP_".$fecha.$hora.".txt";

		$archivo = fopen($dirPath.$inicioFile,"w");


		//ARCHIVO TLP ENCABEZADO HYUNDAI
		fwrite($archivo,"TLPH"." "."TRA"."  "."GMX"."  "."TLP".$fecha.$hora.PHP_EOL);

		$sqlFolioStr = "SELECT * FROM trfoliostbl WHERE tipoDocumento='TH' AND compania='"./*$KIA[$i]['compania']"TR" ."'"." ". "and centroDistribucion ='".$arrKIATLP[$i]['centroDistribucion']."'";
		$FolioRst = fn_ejecuta_query($sqlFolioStr);

		$folio = $FolioRst['root'][0]['folio'] + 1;

		$sqlActualizaFolioStr="UPDATE trfoliostbl SET folio =". $folio." where tipoDocumento='TH' and compania='"./*$rsTLR['root'][$i]['compania']"TR" ."'"." ". "and centroDistribucion ='".$arrKIATLP[$i]['centroDistribucion']."'";
		$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

		//UNIDADES
		for ($i=0; $i <sizeof($arrKIATLP) ; $i++) {

			fwrite($archivo,"TLP"."  ".sprintf("%-5s",$arrKIATLP[$i]['cveDisEnt']).
			$arrKIATLP[$i]['vin'].sprintf("%-5s",$arrKIATLP[$i]['cveLoc']).substr($arrKIATLP[$i]['fechaEvento'],0,4).
			substr($arrKIATLP[$i]['fechaEvento'],5,2).substr($arrKIATLP[$i]['fechaEvento'],8,2).sprintf('%06d',($FolioRst['root'][0]['folio'])));
			fwrite($archivo,"09"."N"."0000000000".PHP_EOL);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('TLP', '".
										$arrKIATLP[$i]['centroDistribucion']."', '".
										$arrKIATLP[$i]['vin']."', '".
										$arrKIATLP[$i]['fechaEvento']."', '".
										$arrKIATLP[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);

			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='AK' WHERE vin='".$arrKIATLP[$i]['vin']."'"." AND cveStatus in('PK','QK')" ;
			$rstUpt=fn_ejecuta_query($uptdy);

		}

		$lineas = count($arrKIATLP)+2;
		//TRAILER
		fwrite($archivo,"TLPT"." ".sprintf('%06d',($lineas)).PHP_EOL);

		fclose($archivo);

	}

	function generaTLPHYU($arrHYUTLP){

		$fecha = date('Ymd');
		$hora = date("His");
		$today =  date('Y-m-d H:i:s');


		$dirPath = "E:\\carbook\\i816TLP\\";
		$inicioFile = "HMM"."_TLP_".$fecha.$hora.".txt";

		$archivo = fopen($dirPath.$inicioFile,"w");


		//ARCHIVO TLP ENCABEZADO HYUNDAI
		fwrite($archivo,"TLPH"." "."TRA"."  "."GMX"."  "."TLP".$fecha.$hora.PHP_EOL);

		$sqlFolioStr = "SELECT * FROM trfoliostbl WHERE tipoDocumento='TH' AND compania='"./*$KIA[$i]['compania']"TR" ."'"." ". "and centroDistribucion ='".$arrHYUTLP[$i]['centroDistribucion']."'";
		$FolioRst = fn_ejecuta_query($sqlFolioStr);

		$folio = $FolioRst['root'][0]['folio'] + 1;

		$sqlActualizaFolioStr="UPDATE trfoliostbl SET folio =". $folio." where tipoDocumento='TH' and compania='"./*$rsTLR['root'][$i]['compania']"TR" ."'"." ". "and centroDistribucion ='".$arrHYUTLP[$i]['centroDistribucion']."'";
		$ActualizaFolioRst= fn_ejecuta_query($sqlActualizaFolioStr);

		//UNIDADES
		for ($i=0; $i <sizeof($arrHYUTLP) ; $i++) {

			fwrite($archivo,"TLP"."  ".sprintf("%-5s",$arrHYUTLP[$i]['cveDisEnt']).
			$arrHYUTLP[$i]['vin'].sprintf("%-5s",$arrHYUTLP[$i]['cveLoc']).substr($arrHYUTLP[$i]['fechaEvento'],0,4).
			substr($arrHYUTLP[$i]['fechaEvento'],5,2).substr($arrHYUTLP[$i]['fechaEvento'],8,2).sprintf('%06d',($FolioRst['root'][0]['folio'])));
			fwrite($archivo,"09"."N"."0000000000".PHP_EOL);

			$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('TLP', '".
										$arrHYUTLP[$i]['centroDistribucion']."', '".
										$arrHYUTLP[$i]['vin']."', '".
										$arrHYUTLP[$i]['fechaEvento']."', '".
										$arrHYUTLP[$i]['claveMovimiento']."', '".
										$today."', 'I', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')";
			fn_ejecuta_query($insTransaccion);
			$uptdy="UPDATE alinstruccionesmercedestbl SET cveStatus='AH' WHERE vin='".$arrHYUTLP[$i]['vin']."'"." AND cveStatus ='PH'" ;
			$rstUpt=fn_ejecuta_query($uptdy);

		}

		$lineas = count($arrHYUTLP)+2;
		//TRAILER
		fwrite($archivo,"TLPT"." ".sprintf('%06d',($lineas)).PHP_EOL);

		fclose($archivo);
	}*/
?>
