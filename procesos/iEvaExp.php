<?php
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    date_default_timezone_set('America/Mexico_City');


	$sqlGenerarDatos= "SELECT dy.vin, dy.fEvento,dy.hEvento,dy.fPed,dy.cveDisFac, al.fechaEvento ". 
						"FROM alhistoricounidadesTbl al, alinstruccionesmercedestbl dy ".
						"WHERE al.vin = dy.vin ".
						"AND al.centroDistribucion='LZC02' ".
						"AND al.claveMovimiento = 'D5' ".
						"AND dy.vin NOT IN (SELECT tu.VIN FROM altransaccionunidadtbl tu WHERE tu.vin = dy.vin AND tu.tipoTransaccion = 'EVA') ".
						"AND dy.vin IN (SELECT tu.VIN FROM altransaccionunidadtbl tu WHERE tu.vin = dy.vin AND tu.tipoTransaccion = 'EVO')";

    $rsSqlGenerarDatos= fn_ejecuta_query($sqlGenerarDatos);

	echo json_encode($rsSqlGenerarDatos['root']);
	
	if(sizeof($rsSqlGenerarDatos['root']) >= 1){

		
	    $sqlGenFolio = "SELECT SUM(FOLIO + 1) as folio FROM trfoliostbl ".
						"WHERE centroDistribucion = 'LZC02' ".
						"AND compania = 'TR' ".
						"AND tipoDocumento = 'EO'; ";

		$rsGenFolio = fn_ejecuta_query($sqlGenFolio);

		$sqlUpdFolio = "UPDATE  trfoliostbl ".
						"SET folio = '".$rsGenFolio['root'][0]['folio']."' ".
						"WHERE centroDistribucion = 'LZC02' ".
						"AND compania = 'TR' ".
						"AND tipoDocumento = 'EO';";

		fn_ejecuta_query($sqlUpdFolio);	

			$fecha = date("YmdHis");
		    $fileDir = "E:/carbook/interfacesExportacion/KMM_EVE_".$fecha;
		    $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
		    $recordsPositionValue = array();

		    $nombreBusqueda = "KMM_EVE_".$fecha;

		            
		    //A) ENCABEZADO
		    fwrite($fileRec, 'EVEH APS  GMX  EVE'."$fecha".PHP_EOL);
		        
		    //B) DETALLE UNIDADES
		    for ($i=0; $i < sizeof($rsSqlGenerarDatos['root']); $i++) { 
		        fwrite($fileRec,"EVE  FT16 VA".
		        	$rsSqlGenerarDatos['root'][$i]['vin']."GLV ".
		        	sprintf('%-5s',$rsSqlGenerarDatos['root'][$i]['cveDisFac']).substr($_REQUEST['fInterfaz'],0,4).substr($_REQUEST['fInterfaz'],5,2).substr($_REQUEST['fInterfaz'],8,2).$_REQUEST['numHora'].$_REQUEST['numMinuto'].$_REQUEST['numSegundo'].sprintf('%012d',$rsGenFolio['r.oot'][0]['folio']+0).sprintf('%-20s',$rsSqlGenerarDatos['root'][$i]['fPed']).
		        	'          '.'KIA Mexico'.'                    '.PHP_EOL);
		    }
			//C) TRAILER
		    fwrite($fileRec, 'EVET '.sprintf('%06d', (sizeof($rsSqlGenerarDatos['root'])+2)));
		   	fclose($fileRec);
		   	ftpArchivo ($nombreBusqueda);

			$contador= count($rsSqlGenerarDatos['root']);

			for ($i=0; $i<($contador); $i++){ 
				$vin = $rsSqlGenerarDatos['root'][$i]['vin'];
				$fecha1 = $rsSqlGenerarDatos['root'][$i]['fEvento']." ".$rsSqlGenerarDatos['root'][$i]['hEvento'];
				$today = date("Y-m-d H:i:s");
				$fecha = substr($today,0,10);
				$hora=substr($today,11,8);

				$sqlAddTransaccion = "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".					 					
									"VALUES ('EVA','LZC02','".$rsGenFolio['root'][0]['folio']."','".
									$rsSqlGenerarDatos['root'][$i]['vin'].
									"','".$fecha1.
									"','D5','".
									$today."','".
									$varEstatus."','".
									$fecha."','".
									$hora."') ";    

				fn_ejecuta_query($sqlAddTransaccion);

				$sqlUpd ="UPDATE alinstruccionesmercedestbl set cveStatus='WK' WHERE vin ='".$rsSqlGenerarDatos['root'][$i]['vin']."' ";
				fn_ejecuta_query($sqlUpd);
			}


				if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
	                $a['sql'] = $rsSqlGenerarDatos;
	                $a['successMessage'] = "Generado ";
	            } else {
	                $a['success'] = false;
	                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddBancos;

	                $errorNoArr = explode(":", $_SESSION['error_sql']);
	            	if($errorNoArr[0] == '1062'){
	            		$e[] = array('id'=>'duplicate','msg'=>getBancosDuplicateMsg());	
	            	}
	            } 

		}else{
			echo json_encode($rsSqlGenerarDatos['root']);
			echo "No hay unidades pendientes por procesar";
		}
	

		

	function ftpArchivo($nombreBusqueda){
			if(file_exists("E:/carbook/interfacesExportacion/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/interfacesExportacion/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/interfacesExportacion/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}
?>  
