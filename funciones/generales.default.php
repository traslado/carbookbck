<?php
    require("sqlErrorMessages.php");

    function fn_ejecuta_query($ps_sql, $log = true){
		set_time_limit(0);
        $_SESSION['error_sql'] = "";
        $_SESSION['sql_debug'] = $ps_sql;

        date_default_timezone_set('America/Mexico_City');
		
		$usuario = $_SESSION['usuario'];
        $ip = $_SERVER['REMOTE_ADDR'];

        $modulo = $_SESSION['modulo'];
        $link = mysql_connect ("127.0.0.1", "root", "toor") or die (json_encode(array('succcess'=>'false','error_sql'=>mysql_error(),'errorMessage'=>'Error de conexi&oacute;n')));
        mysql_select_db ("tracomexdbp");
        $result = mysql_query($ps_sql, $link);
        $rsArr = array('success'=>true,'records'=>mysql_num_rows($result));
        $iInt = 0;
        //To array
        while($row = mysql_fetch_assoc($result)){
            $rsArr['root'][$iInt] = $row;
            $iInt++;
        };

        if (!isset($rsArr['root'])) {
            $rsArr['root'] = null;
        }

        //Log
        if($log){
            $logFile = fopen(str_replace('funciones', 'log', str_replace('generales.php', "log_".date("Y-m-d", strtotime("now")).".log", __FILE__)), "a");
            fwrite($logFile, date("H:i:s - ", strtotime("now")).$ps_sql."\r\n");
            fclose($logFile);
        }

        if (mysql_errno($link) == 0)
            $error_msg = "";
        else
            $error_msg = mysqlErrorMessages(mysql_errno($link));
		 
        $_SESSION['error_sql'] = $error_msg;
        $_SESSION['error_sql_debug'] = $error_msg;
		      
        return $rsArr;
    }
?>