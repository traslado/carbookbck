<?php
	function lws_ramp($key) {
    	$lw_ramp = 'ADIMSRAMPMX';
    	$lws_ramp = array('SAL'   => '806', 'TOL'   => '807',   'TRAC1' => '807',
    					  'TRAC2' => '890', 'TRAC3' => '891',   'TRAC4' => '892',
    					  'TRAC5' => '893', 'TRAC6' => '894',   'TRAC7' => '895',
    					  'ANG'   => '808', 'MAZ'   => '809',   'AGS'   => '011',
    					  'VER'   => '805', 'SFE'   => '988',   'CUA'   => '888', 'LZC' => '816');

    	if (isset($lws_ramp[$key]))
    		$lw_ramp .= $lws_ramp[$key];

    	return $lw_ramp;
    }
    function lws_cod($key) {
    	$lw_cod = 'SINPA';
    	$lws_cod = array( 'SAL'   => 'X6', 'TOL'   => 'X7',   'TRAC1' => 'X7',
    					  'TRAC2' => 'YB', 'TRAC3' => 'YC',   'TRAC4' => 'YD',
    					  'TRAC5' => 'YE', 'TRAC6' => 'YF',   'TRAC7' => 'YG',
    					  'ANG'   => 'X8', 'MAZ'   => 'X9',   'AGS'   => 'X4',
    					  'VER'   => 'MX', 'SFE'   => 'XO',   'CUA'   => 'XU', 'LZC' => 'PT');

    	if (isset($lws_cod[$key])) 
    		$lw_cod = $lws_cod[$key];

    	return $lw_cod;
    }
    function lws_marca($vinFirstChar, $scaccode = null) {
    	
    	$lw_marca = 'C';

    	if (ord(strtolower($unidad[0][0])) >= 97 && ord(strtolower($unidad[0][0])) <= 122)
    		$lw_marca = 'M';

    	if ($scaccode != null) {
    		if ($scaccode == 'CHMO')
    			$lw_marca = '3';
    	}

    	return $lw_marca;
    }
    function lws_scac_code($vinFirstChar) {
    	$lw_scac_code = 'XTRA';

    	$lws_scac_code = array('#' => 'MBEN', '&' => 'MBEN', '%' => 'MBEN',
    						   '(' => 'MBEN', 'D' => 'MITS', 'E' => 'MITS',
    						   'F' => 'MITS', 'G' => 'MITS', 'H' => 'MITS');

    	if (isset($lws_scac_code[$unidadFirstChar]))
    		$lw_scac_code = $lws_scac_code[$unidadFirstChar];

    	return $lw_scac_code;
    }
    function lws_anio($vinFirstChar) {
    	$lw_anio =  $vinFirstChar;

    	$lws_anio = array('#' => '5', '&' => '6', '%' => '7', 
    					  '(' => '8', 'D' => '4', 'E' => '5', 
    					  'F' => '6', 'G' => '7', 'H' => '8');

    	if (isset($lws_anio[$vinFirstChar]))
    		$lw_anio = $lws_anio[$vinFirstChar];

    	return $lw_anio;

    }
   	    	
    function getTxt($texts, $positions){
    	$text = '';
        for ($i=0; $i < count($positions); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = ($positions[$i]);
                $antLength = strlen($texts[$positions[$i]]);
            }
            $text .= out('s', ($positions[$i]-1) - ($antPos + $antLength)).$texts[$positions[$i]];
        }
        return $text;
    }

    function getCiaLocFromLocalidad($localizacionUnidad){
    	//DEFINE variable ciaLoc
	    //Generalmente es las 3 primeras letras de localidad de ubicación
	    $ciaLoc = substr($localizacionUnidad, 0, 3);

	    //Cuando localidad de ubicación = TRAC# .·. $ciaLoc = TR#
	    if (preg_match('/^(TRAC)[0-9]+/', $localizacionUnidad))
	    	$ciaLoc = 'TRAC' . substr($localizacionUnidad, 4);

	    if ($unidad['claveMovimientoTalon'] == 'TC')//Talón Cancelado
	    	$ciaLoc = $localizacionUnidad;

	    if ($unidad['claveMovimiento'] == 'CO' && $localizacionUnidad == 'UPFIT')
	    	$ciaLoc = $localizacionUnidad;

	    return $ciaLoc;
    }

    function isDuplicatedTransaccionByCiaLoc($unidad, $ciaLoc, $today) {
    	//Verifica no exista en alTransaccionUnidadTbl un registro "R2V Y Patio = ciaLoc Y fechaMovimiento 
    	//O
    	//Verifica no exista en alTransaccionUnidadTbl un registro "R2V Y Patio = ciaLoc Y fechaMovimiento 
    	$fechaEventoUnidad = date('Y-m-d', strtotime($unidad['fechaEvento']));

    	$isDuplicated = false;
	    if($unidad['tipoTransaccion'] == 'RA3' && $unidad['patioTransaccion'] == $unidad['centroDistribucion'] && 
	    	$fechaEventoUnidad == $today)
    		$isDuplicated = true;

    	if($unidad['tipoTransaccion'] == 'R2V' && $unidad['patioTransaccion'] == $ciaLoc && 
    		$fechaEventoUnidad == $today)
    		$isDuplicated = true;

    	if ($unidad['transaccionDuplicada'] == '1')
    		$isDuplicated = true;

    	return $isDuplicated;

    }
    
?>