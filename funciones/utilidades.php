<?php
	define("latin1UcChars", "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ");
	define("latin1LcChars", "àáâãäåæçèéêëìíîïðñòóôõöøùúûüý");

    class programaexterno
    {
        private $rutaPHP = "C:\Servidores\php\php.exe";
        public function runprogram($ls_program) {
            $comando = $this->rutaPHP." ${ls_program}";
            exec($comando);
        }
    }
	
	if(isset($_REQUEST['actionHdn']) && $_REQUEST['actionHdn'] == 'eliminaArchivo' && isset($_REQUEST['archivo']) && !empty($_REQUEST['archivo']))
	{
			//El archivo debe venir con todo y la ruta donde se encuentra
			eliminaArchivo($_REQUEST['archivo']);
	}

	//Remplazar variables decimales vacías con un cero
	function replaceEmptyDec($dataStr){
		if($dataStr == ''){
			return 0;
		} else {
			return $dataStr;
		}
	}

	//Remplazar variables vacias con NULL
	function replaceEmptyNull($dataStr){
		if($dataStr == "" || $dataStr == "''"){
			return 'NULL';
		} else {
			return $dataStr;
		}
	}

    //Remplazar variables vacias con 0.00
    function replaceEmptyFloat($dataStr){
        if($dataStr == ""){
            return '0.00';
        } else {
            return $dataStr;
        }
    }

	function trasformUppercase($array){
		$upArray = array();
		foreach ($array as $key => $value) {
			if (strpos($key, "ActionHdn")) {
				$upArray[$key] = $value;
			} else {
				$upArray[$key] = strtoupper(strtr($value, latin1LcChars, latin1UcChars));
			}
		}
		return $upArray;
	}

    function toUTF8($text){
        return iconv('UTF-8', 'windows-1252', $text);
    }

    function numToWord($number){
        $dictionary  = array(
            0                   => 'cero',
            1                   => 'un',
            2                   => 'dos',
            3                   => 'tres',
            4                   => 'cuatro',
            5                   => 'cinco',
            6                   => 'seis',
            7                   => 'siete',
            8                   => 'ocho',
            9                   => 'nueve',
            10                  => 'diez',
            11                  => 'once',
            12                  => 'doce',
            13                  => 'trece',
            14                  => 'catorce',
            15                  => 'quince',
            16                  => 'dieciseis',
            17                  => 'diecisiete',
            18                  => 'dieciocho',
            19                  => 'diecinueve',
            20                  => 'veinte',
            21                  => 'veintiun',
            22                  => 'veintidos',
            23                  => 'veintitres',
            24                  => 'veinticuatro',
            25                  => 'veinticinco',
            26                  => 'veintiseis',
            27                  => 'veintisiete',
            28                  => 'veintiocho',
            29                  => 'veintinueve',
            30                  => 'treinta',
            40                  => 'cuarenta',
            50                  => 'cincuenta',
            60                  => 'sesenta',
            70                  => 'setenta',
            80                  => 'ochenta',
            90                  => 'noventa',
            100                 => 'cien',
            101                 => 'cientos',
            200                 => 'doscientos',
            300                 => 'trescientos',
            400                 => 'cuatrocientos',
            500                 => 'quinientos',
            600                 => 'seiscientos',
            700                 => 'setecientos',
            800                 => 'ochocientos',
            900                 => 'novecientos',
            1000                => 'mil',
            1000000             => 'millon',
            'millones'          => 'millones',
        );
        $string = '';
        switch($number){
            case $number < 30:
                $string  = $dictionary[$number];
                break;
            case $number < 100:
                $decenas = ((int)($number/10)) * 10;
                $unidades = $number % 10;
                $string = $dictionary[$decenas]; 
                if($unidades){
                    $string .= ' y '.numToWord($unidades);
                }
                break;
            case $number < 1000:
                $string = '';
                $centenas = ((int)($number/100))*100;
                //echo 'centenas '.$centenas.'</br>';
                $restante = $number % 100;
                //echo 'restante '.$restante.'</br>';
                $unidades = ((int)($number/100))*100;
                //echo 'unidades '.$unidades.'</br>';
                if($centenas == 100 && $unidades > 0){
                    $string = 'ciento ';
                } else{
                    $string = $dictionary[$centenas].' ';    
                }
                $string .= numToWord($restante);
                break;
            case $number < 2000:
                $string = 'mil ';
                if($number != 1000){
                    $restante = $number % 1000;
                    $string .= numToWord($restante);
                }
                break; 
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int)($number / $baseUnit);
                $remainder = $number % $baseUnit;
                if($baseUnit == 1000000 && $numBaseUnits > 1 ){ //Si es baseunit millon  y es mas de 1 millon
                    $baseUnit = 'millones'; // millones
                }
                $string = numToWord($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= ' '.numToWord($remainder);
                }
                break;

        }
        return $string; //Convierte numeros en letras

    }
    //Formatea cantidad tipo $##,###,###.00
    function toMoney($val,$symbol='$',$r=2){
        $n = $val; 
        $c = is_float($n) ? 1 : number_format($n,$r);
        $d = '.';
        $t = ',';
        $sign = ($n < 0) ? '-' : '';
        $i = $n=number_format(abs($n),$r); 
        $j = (($j = $i.length) > 3) ? $j % 3 : 0; 

       return  $symbol.$sign .($j ? substr($i,0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/',"$1" + $t,substr($i,$j)) ;
    }

    //Funcion que regresa nvas lineas o tabs para archivos de txt planos
    function out($char, $times){
        $validChar = array(
                    "n" => PHP_EOL,
                    "t" => "\t",
                    "s" => " "
            );
        $chars = '';
        for ($i=0; $i < $times ; $i++) { 
            $chars .= $validChar[$char];
        }
        fseek($myfile, -20);   
        return $chars;
    }

    function pluck_array_reduce($key, $data) {
      return array_reduce($data, function($result, $array) use($key){
        isset($array[$key]) && $result[] = $array[$key];

        return $result;
      }, array());
    }
    function getGroupedArray($array, $keyFieldsToGroup) { 
        $newArray = array();
        foreach ($array as $record) {
            $newArray = getRecursiveArray($record, $keyFieldsToGroup, $newArray);
        }
        return $newArray;
    }

    function getRecursiveArray($itemArray, $keys, $newArray){
        if (count($keys) > 1) 
            $newArray[$itemArray[$keys[0]]] = getRecursiveArray($itemArray, array_splice($keys, 1), $newArray[$itemArray[$keys[0]]]);
        else
            $newArray[$itemArray[$keys[0]]][] = $itemArray;
        return $newArray;
     }

   /* function fn_ori_splc($centroDistribucion){
        $ori_splc = array('CDSAL' => '922786801',
                          'CDTOL' => '958770807',
                          'CDANG' => '958770808',
                          'CDAGS' => '940606000',
                          'CDMAZ' => '958770809',
                          'CDSFE' => '978442999',
                          'CDMAT' => '978442999',
                          'CDCUA' => '957692999',
                          'CDLZC' => '999990907');
        return  $ori_splc[$centroDistribucion];
    }*/

    function validarFechaHora($fecha = "", $hora = "", $offset = 1){
        $response = array();

        //VALIDACION FECHA
        if($fecha != ''){
            $anio = substr($fecha, 0, 4);
            $mes = substr($fecha, 6, 2);
            $dia = substr($fecha, 4, 2);

            $response['fechaValida'] = checkdate($dia, $mes, $anio);

            if($response['fechaValida']){
                if($anio == date("Y")){
                    $response['anioActual'] = true;
                } else if (date('m') == 1){
                    if($anio == date("Y", strtotime("-".$offset." year"))){
                        $response['anioAnterior'] = true;
                    }
                } else {
                    $response['fechaVieja'] = 1;
                }
            }
        }

        if($hora != ''){
            $response['horaValida'] = preg_match("/(2[0-4]|[01][1-9]|10)([0-5][0-9])([0-5][0-9])/", $hora);
        }

        return $response;
    }

    function getClientIP(){
        $ipaddress = '';
        
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    #Acepta de parms: $cuentaContable, $subCta1, $subCta2. Equivalente a tras471n de informix
    function armaCuentaContable($cuentaContable, $subCta1, $subCta2){
    		$_REQUEST['cuentaContable'] = $cuentaContable;
    		$_REQUEST['subCta1'] = str_pad($subCta1, 4, "0", STR_PAD_LEFT);
    		$_REQUEST['subCta2'] = str_pad($subCta2, 5, "0", STR_PAD_LEFT);
    		    		
    		if(!isset($_REQUEST['cuentaContable']) || empty($_REQUEST['cuentaContable']))
    		{
    				$ctaCont = 'SIN CUENTA';
    		}
    		else
    		{
    				$cambia = 0;
    				$nivel = 0;
    				for($x=0;$x<strlen($cuentaContable);$x++)
    				{
    						if(substr($cuentaContable,$x,1) != '1' && substr($cuentaContable,$x,1) != '2' && substr($cuentaContable,$x,1) != '3' &&
    							 substr($cuentaContable,$x,1) != '4' && substr($cuentaContable,$x,1) != '5' && substr($cuentaContable,$x,1) != '6' &&
    							 substr($cuentaContable,$x,1) != '7' && substr($cuentaContable,$x,1) != '8' && substr($cuentaContable,$x,1) != '9' &&
    							 substr($cuentaContable,$x,1) != '0' && substr($cuentaContable,$x,1) != '.')
    						{
    								$cambia++;
    								$nivel = ($nivel == 0)?1:$nivel;
    						}
    						else
    						{
    								if(substr($cuentaContable,$x,1) == '.' && $cambia > 0)
    								{
    										$nivel++;
    								}
    						}
    				}    				
    				if($nivel > 0)
    				{
    						$cta_tractor = $_REQUEST['subCta1'];
    						$ctaCont = '';
    						$ind2 = -1;
    						$cambia = 1;    						
    						for($x=0;$x<strlen($cuentaContable);$x++)
    						{
		    						if(substr($cuentaContable,$x,1) == '1' || substr($cuentaContable,$x,1) == '2' || substr($cuentaContable,$x,1) == '3' ||
		    							 substr($cuentaContable,$x,1) == '4' || substr($cuentaContable,$x,1) == '5' || substr($cuentaContable,$x,1) == '6' ||
		    							 substr($cuentaContable,$x,1) == '7' || substr($cuentaContable,$x,1) == '8' || substr($cuentaContable,$x,1) == '9' ||
		    							 substr($cuentaContable,$x,1) == '0' || substr($cuentaContable,$x,1) == '.')
		    						{
		    								$ctaCont .= substr($cuentaContable,$x,1);
		    								if(substr($cuentaContable,$x,1) == '.' && $cambia > 1)
		    								{
		    										$cta_tractor = $_REQUEST['subCta2'];
		    										$cambia++;
		    										$ind2 = -1;
		    								}
		    						}
		    						else
		    						{
		    								$cambia++;
		    								$ind2++;
		    								if(substr($cta_tractor,$ind2,1) == ' ')
		    								{
		    										$ctaCont .= '0';
		    								}
		    								else
		    								{
		    										$ctaCont .= substr($cta_tractor,$ind2,1);
		    								}
		    						}		    						
    						}
    				}
    				else
    				{
    						$ctaCont = $_REQUEST['cuentaContable'];
    				}
    				$cuentaContable = $ctaCont;
    				
    		}
		    //var_dump($cuentaContable);    		
    		return $cuentaContable;
    }
    
		//Elimina archivo
		function eliminaArchivo($archivo){
				unlink($archivo);
		}    
?>