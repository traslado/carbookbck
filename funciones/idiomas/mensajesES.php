<?php
	//ERROR PHP ACTION
	function getErrorAction(){
		echo json_encode(array('success'=>false, 'errorMessage'=>'PHP sin Action a realizar'));
	}
	//Usuarios
	function getUsuariosSuccessMsg(){
		return 'Usuario Creado Correctamente';
	}
	function getUsuariosUpdMsg(){
		return 'Usuario Actualizado Correctamente';
	}
	function getCambiarContraseniaSuccessMsg(){
		return 'Contrase&ntilde;a Actualizada Correctamente';
	}
	function getCambiarContraseniaErrorMsg(){
		return 'Contrase&ntilde;a Actual Incorrecta';
	}	
	function getCambiarContraseniaRepeatMsg(){
		return 'Contrase&ntilde;a Nueva Usada Anteriormente.<br>Elige Una Nueva';
	}
	function getUsuariosModulosSuccessMsg(){
		return 'Permisos Asignados Correctamente';
	}
	function getUsuariosModulosDltMsg(){
		return 'Permisos Eliminados Correctamente';
	}

	//Panel de Control
	function getSisWallpaperSuccessMsg(){
		return 'Fondo de Pantalla Actualizado';
	}
	function getSisThemeSuccessMsg(){
		return 'Tema Actualizado';
	}
	function getSisDesktopSuccessMsg(){
		return 'Iconos Actualizados';
	}

	// Generales
	function getRequerido(){
		return 'Este campo es requerido...';
	}
	function getErrorRequeridos(){
		return 'Falto llenar campos requeridos';
	}
	function getMsgTitulo(){
		return 'Resultado de la Solicitud';
	}

	//Bancos
	function getBancosUpdateMsg(){
		return 'Banco Actualizado Correctamente';
	}
	function getBancosDuplicateMsg(){
		return 'El Banco Ingresado ya Existe';
	}
	function getBancosSuccessMsg(){
		return 'Banco Guardado Correctamente.';
	}

	//Rutas
	function getRutasSuccessMsg(){
		return 'Ruta Creada Correctamente';
	}

	function getRutasUpdateMsg(){
		return 'Ruta Actualizada Correctamente';
	}
	
	//Compa�ias
	function getCiaSuccesMsg()
	{
		return 'Compa&ntilde;&iacute;a Guardada Correctamente.';
	}
	function getCiaUpdtMsg()
	{
		return 'Compa&ntilde;&iacute;a Actualizada Correctamente.';
	}
	function getCiaDelMsg()
	{
		return 'Compa&ntilde;&iacute;a Eliminada Correctamente.';
	}
	function getCiaDelErrorMsg()
	{
		return 'Es necesario seleccionar una Compa&ntilde;&iacute;a ...';
	}
	function getMsgCiaAsig()
	{
		return 'Compa&ntilde;&iacute;a(s) Asignada(s).';
	}

	//Plazas
	function getPlazaSuccessMsg() {
		return 'Plaza Creada Correctamente.';
	}
	function getPlazaUpdMsg() {
		return 'Plaza Actualizada Correctamente.';
	}
	function getKmPlazaSuccessMsg(){
		return 'Kilometros Asignados Correctamente';
	}
	function getKmPlazaUpdMsg(){
		return 'Kilometros Actualizados Correctamente';
	}
	function getNoKmPlazaRegistradoMsg($origen, $destino){
		return 'No Existen Kilometros Registrados de '.$origen.' a '.$destino;
	}

	//Paises
	function getPaisSuccessMsg(){
		return 'Pais Creado Correctamente';
	}
	function getPaisUpdateMsg(){
		return 'Pais Actualizado Correctamente';
	}
	function getPaisDeleteMsg(){
		return 'Pais Borrado Correctamente';
	}
	function getPaisDuplicateMsg(){
		return 'Pais Ya Existente';
	}

	//Estados
	function getEstadoSuccessMsg(){
		return 'Estado Creado Correctamente';
	}
	function getEstadoUpdateMsg(){
		return 'Estado Actualizado Correctamente';
	}
	function getEstadoDeleteMsg(){
		return 'Estado Borrado Correctamente';
	}
	function getEstadoDuplicateMsg(){
		return 'Estado Ya Existente';
	}

	//Municipios
	function getMunicipioSuccessMsg(){
		return 'Municipio Creado Correctamente';
	}
	function getMunicipioUpdateMsg(){
		return 'Municipio Actualizado Correctamente';
	}
	function getMunicipioDeleteMsg(){
		return 'Municipio Borrado Correctamente';
	}
	function getMunicipioDuplicateMsg(){
		return 'Municipio Ya Existente';
	}

	//Colonia
	function getColoniaSuccessMsg(){
		return 'Colonia Creada Correctamente';
	}
	function getColoniaUpdateMsg(){
		return 'Colonia Actualizada Correctamente';
	}
	function getColoniaDeleteMsg(){
		return 'Colonia Borrada Correctamente';
	}
	function getColoniaDuplicateMsg(){
		return 'Colonia Ya Existente';
	}

	//Regiones
	function getRegionSuccessMsg(){
		return 'Region Creada Correctamente';
	}
	function getRegionUpdateMsg(){
		return 'Region Actualizada Correctamente';
	}
	function getRegionDeleteMsg(){
		return 'Region Borrada Correctamente';
	}

	//Generales
	function getGeneralesSuccessMsg(){
		return 'Registro Creado Correctamente';
	}
	function getGeneralesUpdateMsg(){
		return 'Registro Actualizado Correctamente';
	}
	function getGeneralesDeleteMsg(){
		return 'Registro Borrado Correctamente';
	}

	//Tractores
	function getTractoresSuccessMsg(){
		return 'Tractor Creado Correctamente';
	}
	function getTractoresUpdateMsg(){
		return 'Tractor Actualizado Correctamente';
	}
	function getTractoresDeleteMsg(){
		return 'Tractor Borrado Correctamente';
	}
	//--Errores Tractores
	//Error No 1062
	function getTractoresDuplicateMsg(){
		return 'Existe otro tractor con el mismo No. serie o placas';
	}
	function getBloquearTractorMsg(){
		return 'Tractor Bloqueado Correctamente';
	}
	function getTractoresLiberarMsg(){
		return 'Tractor Liberado Correctamente';
	}
	function getTractoresLiberarMasivoMsg(){
		return 'Tractores Liberados Correctamente';
	}
	//Mantenimiento Tractores
	function getMantenimientoNecesario($patio){
		return 'AVISO: Se debe enviar tractor a mantenimiento al <br> '.$patio;
	}
	function getMantenimientoSuccessMsg(){
		return 'Mantenimiento Generado Correctamente';
	}

	//Choferes
	function getChoferesSuccessMsg(){
		return 'Chofer Creado Correctamente';
	}
	function getChoferesUpdateMsg(){
		return 'Chofer Actualizado Correctamente';
	}
	function getChoferesDeleteMsg(){
		return 'Chofer Borrado Correctamente';
	}

	function getReacomodoChofer(){
		return 'El reacomodo se realiz&oacute; satisfactoriamente';
	}
	function getChoferesDescuentosSuccessMsg(){
		return 'Descuentos Personales Generados Correctamente';
	}
	//--Errores Choferes
	//Error No 1062
	function getChoferesDuplicateMsg(){
		return 'Existe otro chofer con la misma clave o licencia';
	}
	function getEsperaChoferesAddMsg(){
		return 'Se Agregaron los Datos Correctamente';
	}
	function getEsperaChoferesUpdateMsg(){
		return 'Asignaci&oacute;n Actualizada Correctamente';
	}

	//Tarifas
	function getTarifasSuccessMsg(){
		return 'Tarifa Creada Correctamente';
	}
	function getTarifasUpdateMsg(){
		return 'Tarifa Actualizada Correctamente';
	}
	function getTarifasDeleteMsg(){
		return 'Tarifa Borrada Correctamente';
	}
	function getNoTarifaUnidadMsg($vin, $tipoTarifa){
		return $vin." No Tiene tarifa de ".$tipoTarifa;
	}

	//Distribuidores
	function getDistribuidoresSuccessMsg(){
		return 'Distribuidor Creado Correctamente';
	}
	function getDistribuidoresUpdateMsg(){
		return 'Distribuidor Actualizado Correctamente';
	}
	function getDistribuidoresDeleteMsg(){
		return 'Distribuidor Borrado Correctamente';
	}
	function getDistribuidoresUpdateNoDirectionUpdMsg(){
		return 'Distribuidor Actualizado Correctamente.<br>No hubo direcciones a actualizar.';	
	}

	//Color Sistema
	function getSistemaColorSuccessMsg(){
		return 'Se ha agregado Correctamente el Color';
	}
	function getSistemaColorUpdateMsg(){
		return 'Se ha actualizado Correctamente el Color';
	}

	//Distribuidores Especiales
	function getDistEspecialSuccessMsg($tipo){
		switch ($tipo) {
			case 'DE':
				return 'Destino Especial Creado Correctamente';
				break;
			case 'CD':
				return 'Centro de Distribucion Creado Correctamente';
				break;
			case 'PA':
				return 'Patio Creado Correctamente';
				break;
			default:
				return '';
				break;
		}
	}
	function getDistEspecialUpdMsg($tipo){
		switch ($tipo) {
			case 'DE':
				return 'Destino Especial Actualizado Correctamente';
				break;
			case 'CD':
				return 'Centro de Distribucion Actualizado Correctamente';
				break;
			case 'PA':
				return 'Patio Actualizado Correctamente';
				break;
			default:
				return '';
				break;
		}
	}

	//Marcas por Centro de Distribucion
	function getAsignarMarcaSuccess(){
		return "Marca Asignada Correctamente";
	}

	//Direcciones
	function getDireccionesSuccessMsg($cuant){
		if($cuant == 1)
			return 'Direcciones Creadas Correctamente';
		else
			return 'Direccion Creada Correctamente';
	}
	function getDireccionesUpdateMsg($cuant){
		if ($cuant == 1)
			return 'Direcciones Actualizadas Correctamente';
		else
			return 'Direccion Actualizada Correctamente';
	}
	function getDireccionesDeleteMsg($cuant){
		if ($cuant == 1)
			return 'Direcciones Borradas Correctamente';
		else
			return 'Direccion Borrada Correctamente';
	}
	function getDireccionesDltMult(){
		return 'Direcciones Eliminadas Correctamente: ';
	}
	//--Errores Distribuidores
	//Error No 1062
	function getDistribuidoresDuplicateMsg(){
		return 'Centro Distribucion Duplicado';
	}

	//Colores
	function getColoresSuccessMsg(){
		return 'Color Creado Correctamente';
	}
	function getColoresUpdateMsg(){
		return 'Color Actualizado Correctamente';
	}
	function getColoresDeleteMsg(){
		return 'Color Borrado Correctamente';
	}
	//--Errores Colores
	//Error No 1062
	function getColoresDuplicateMsg(){
		return 'Esta Marca ya esta asignada a este color';
	}

	//Proveedores
	function getProveedoresSuccessMsg(){
		return 'Proveedor Creado Correctamente';
	}
	function getProveedoresUpdateMsg(){
		return 'Proveedor Actualizado Correctamente';
	}
	function getProveedoresDeleteMsg(){
		return 'Proveedor Borrado Correctamente';
	}

	//Gastos Tractor
	function getGastosTractorSuccessMsg(){
		return 'Gasto de Tractor Creado Correctamente';
	}
	function getGastosTractorUpdateMsg(){
		return 'Gasto de Tractor Actualizado Correctamente';
	}
	function getGastosTractorDeleteMsg(){
		return 'Gasto de Tractor Borrado Correctamente';
	}
	function getGastosViajeAcompananteSuccessMsg(){
		return 'Gasto de Acompa&ntilde;ante Creado Correctamente';
	}

	//Marcas Unidades
	function getMarcasUnidadesSuccessMsg(){
		return 'Marca Creada Correctamente';
	}
	function getMarcasUnidadesUpdateMsg(){
		return 'Marca Actualizada Correctamente';
	}
	function getMarcasUnidadesDeleteMsg(){
		return 'Marca Borrada Correctamente';
	}

	//Clasificacion Marca
	function getClasificacionMarcaSuccessMsg(){
		return 'Clasificacion de la Marca Creada Correctamente';
	}
	function getClasificacionMarcaUpdateMsg(){
		return 'Clasificacion de la Marca Actualizada Correctamente';
	}
	function getClasificacionMarcaDeleteMsg(){
		return 'Clasificacion de la Marca Borrada Correctamente';
	}

	//Conceptos Centros
	function getConceptosCentrosSuccessMsg(){
		return 'Concepto de Distribuidor Creado Correctamente';
	}
	function getConceptosCentrosUpdateMsg(){
		return 'Concepto de Distribuidor Actualizado Correctamente';
	}
	function getConceptosCentrosDeleteMsg(){
		return 'Concepto de Distribuidor Borrado Correctamente';
	}
	//Error No 1062
	function getConceptosCentrosDuplicateMsg(){
		return 'Este Distribuidor ya tiene un concepto igual';
	}

	//Simbolos Unidades
	function getSimbolosUnidadesSuccessMsg(){
		return 'Simbolo de Unidad Creado Correctamente';
	}
	function getSimbolosUnidadesUpdateMsg(){
		return 'Simbolo de Unidad Actualizado Correctamente';
	}
	function getSimbolosUnidadesDeleteMsg(){
		return 'Simbolo de Unidad Borrado Correctamente';
	}

	//Conceptos
	function getConceptosSuccessMsg(){
		return 'Concepto Creado Correctamente';
	}
	function getConceptosUpdateMsg(){
		return 'Concepto Actualizado Correctamente';
	}
	function getConceptosDeleteMsg(){
		return 'Concepto Borrado Correctamente';
	}

	//Unidades
	function getUnidadesSuccessMsg(){
		return 'Unidad Creada Correctamente';
	}
	function getUnidadesMasivoSuccessMsg(){
		return 'Unidades Creadas Correctamente';
	}
	function getUnidadesUpdMsg(){
		return 'Unidad Actualizada Correctamente';
	}
	function getUnidadesNotUpdMsg(){
		return 'No Hubo Nada Que Actualizar a la Unidad';
	}
	function getHistoricoUnidadMsg(){
		return 'Hist&oacute;rico de la Unidad Agregado';
	}
	function getHistoricoUnidadMasivoMsg(){
		return 'Hist&oacute;rico de las Unidades Agregados Correctamente';
	}
	function getHistoricoUpdMsg(){
		return 'Hist&oacute;rico Actualizado Correctamente';
	}
	function getHistClaveMovUpdMsg(){
		return "Clave de Movimiento Actualizada Correctamente";
	}
	function getHistDistribuidorUpdMsg(){
		return 'Distribuidor Actualizado Correctamente';
	}
	function getUltimoDetalleMsg(){
		return 'Ultimo Detalle Actualizado Correctamente';
	}
	function getModificacionUnidadSuccessMsg(){
		return 'Unidad Actualizada Correctamente';
	}
	function getUnidadesLiberarMsg(){
		return 'Unidad Liberada Correctamente';
	}
	function getUnidadesBloquearMsg(){
		return 'Unidad Bloqueada Correctamente';
	}
	function getUnidadesTrap003Msg(){
		return 'Cambio de Estatus De La Unidad Correcto';
	}
	function getUnidadDetenidaSuccessMsg(){
		return 'Unidad Detenida Correctamente';
	}
	function getUnidadesDetenidasSuccessMsg(){
		return 'Unidades Detenidas Correctamente';
	}
	function getUnidadesDetencionDistribuidor($distribuidor){
		return 'Distribuidor '.$distribuidor.' Detenido Correctamente';
	}
	function getUnidadesDetencionSimbolo($simbolo){
		return 'S&iacute;mbolo '.$simbolo.' Detenido Correctamente';
	}
	function getUnidadesDetencionDistSimbolo($distribuidor, $simbolo){
		return 'S&iacute;mbolo '.$simbolo.' del Distribuidor '.$distribuidor.' Detenido Correctamente';
	}
	function getUnidadesDetencionVin($vin){
		return 'Unidad '.$vin.' Detenida Correctamente';
	}
	function getUnidadesLiberarDetencionDistribuidor($distribuidor){
		return 'Distribuidor '.$distribuidor.' Liberado Correctamente';
	}
	function getUnidadesLiberarDetencionSimbolo($simbolo){
		return 'S&iacute;mbolo '.$simbolo.' Liberado Correctamente';
	}
	function getUnidadesLiberarDetencionDistSimbolo($distribuidor, $simbolo){
		return 'S&iacute;mbolo '.$simbolo.' del Distribuidor '.$distribuidor.' Liberado Correctamente';
	}
	function getHoldUnidadesMessage($ok, $fail){
		return 'Correctos: '.$ok.'<br>'.'Fallos: '.$fail;
	}
	function getUnidadesQuitarHold($cantVin){
		if ($cantVin > 1) {
			return 'Unidades Liberadas Correctamente';
		} else {
			return 'Unidad Liberada Correctamente';
		}
	}
	function getInsertarEntradaPatio(){
		return 'Entrada a Patio Generada Correctamente';
	}
	function getUnidadesUpdObservaciones(){
		return 'Observaciones Actualizadas Correctamente';
	}
	function getErrorBloquearUnidad(){
		return 'No se pudo bloquear la Unidad';
	}
	function getInsertarUnidadMasivoMSg(){
		return 'Unidades Insertadas Correctamente. <br>Error al insertar los siguientes VIN: ';
	}
	function getErrorInsertarUnidadMasivo(){
		return "Error al insertar los siguientes VIN: ";
	}
	//Grupos
	function getGrupoSuccessMsg(){
		return 'Grupo Creado Correctamente';
	}
	function getGrupoUpdateMsg(){
		return 'Grupo Actualizado Correctamente';
	}
	function getGrupoMissingDataMsg(){
		return 'Faltan datos necesarios para la creacion del grupo';
	}
	function getGrupoFailedClasifMsg(){
		return 'Error al insertar con las siguientes Clasificaciones: ';
	}
	function getGrupoFailedDistMsg(){
		return 'Error al insertar con los siguientes Distribuidores: ';	
	}
	function getGrupoLugaresSuccessMsg(){
		return 'Lugares para el Grupo Creados Correctamente';
	}
	function getGrupoLugaresFailedMsg(){
		return 'Error al insertar los siguientes lugares: ';
	}

	//Localizacion Patios
	function getLocalizacionSuccessMsg(){
		return 'Unidades Colocadas Correctamente.';
	}
	function getLocalizacionFailedMsg(){
		return 'No se dieron localizacion a las siguientes unidades: ';
	}
	function getLocalizacionPatiosSuccessMsg($fila, $lugar){
		return 'La unidad ha sido colocada en <br>Fila: '.$fila."<br>Lugar: ".$lugar;
	}
	function getLocalizacionPatiosDeleteMsg(){
		return 'La unidad ha sido retirada del patio.';
	}
	function getLocalizacionPatiosUpdMsg(){
		return 'Localizaciones Actualizadas Correctamente. ';
	}
	function getLocalizacionPatiosUpdFailedMsg(){
		return 'Error al Actualizar: ';
	}
	function getNoPatioMsg($vin){
		return 'A la unidad '.$vin.' no se le ha podido asignar un patio';
	}
	function getLocalizacionBloqueadaStr(){
		return 'Lugares Bloqueados Correctamente';
	}
	function getLocalizacionDesbloqueadaStr(){
		return 'Lugares Desbloqueados Correctamente';
	}

	//Clasificaci�n - Tarifa
	function getClasificacionTarifaSuccessMsg(){
		return 'Relacion entre Clasificacion y Tarifa Guardada Correctamente';
	}

	//Cambio de Destino
	function getCambioDestinoSuccessMsg(){
		return 'Cambio de Destino Creado Correctamente';
	}
	function getCambioDestinoDltMsg(){
		return 'Cambio de Destino Eliminado Correctamente';
	}
	function getCambioDestinoEntradaPatioDltMsg(){
		return 'No se puede borrar el Cambio de Destino, la unidad ya cuenta con entrada a patio ';
	}

	//Destinos Especiales
	function getDestinosEspecialesSuccessMsg(){
		return 'Servicio Especial Creado Correctamente. ';
	}
	function getDestinosEspecialesFailedMsg(){
		return 'Error al insertar los siguientes VIN: ';
	}
	function getDestinosEspecialesUpdMsg(){
		return 'Destino Especial Actualizado Correctamente.';
	}
	function getDestinosEspecialesDltMsg(){
		return 'Destinos Especial Eliminado Correctamente.';
	}
	function getDestinoEspecialCancelarMsg(){
		return 'Servicio Especial Cancelado Correctamente';
	}
	function getGastosEspecialesSuccessMsg(){
		return 'Gastos Especiales Creados Correctamente';
	}
	function getDestinosEspecialesSinTarifaEspecialMsg(){
		return 'Unidades cuya clasificacion no tiene asignada tarifa especial: ';
	}

	//Impuestos
	function getImpuestosSuccessMsg(){
		return 'Impuesto Creado Correctamente';
	}
	function getImpuestosUpdMsg(){
		return 'Impuesto Actualizado Correctamente';
	}

	//Series
	function getSeriesSuccessMsg(){
		return 'Serie Creada Correctamente';
	}
	function getSeriesUpdateMsg(){
		return 'Serie Actualizada Correctamente';
	}

	//Cuentas Contables
	function getCuentasSuccessMsg(){
		return 'Cuenta Creada Correctamente';
	}
	function getCuentasUpdMsg(){
		return 'Cuenta Modificada Correctamente';
	}

	//Cuentas Bancarias
	function getCuentasBancariasSuccessMsg(){
		return 'Cuenta Bancaria Creada Correctamente';
	}

	function getCuentasBancariasUpdMsg(){
		return 'Cuenta Bancaria Actualizada Correctamente';
	}

	//Previajes
	function getPreViajeSuccessMsg(){
		return 'PreViaje Creado Correctamente';
	}
	//Viajes Vac�os
	function getGastosViajeVacioSuccessMsg(){
		return 'Viaje Vacio Creado Correctamente';
	}
	//Gastos Viaje
	function getGastosViajeTractorSuccessMsg(){
		return 'Gastos del Viaje Agregados Correctamente';
	}
	function getConceptosNoExist(){
		return 'Concepto de Gasto No Existe';
	}
	function getGastosCanceladosSuccessMsg(){
		return 'Gastos Cancelados Correctamente';
	}
	///Viajes
	function getViajeSuccessMsg() {
		return 'Viaje Creado Correctamente';
	}
	function getViajeEstatusUpdMsg(){
		return 'Estatus del Viaje Actualizado Correctamente';
	}
	function getNoViajeErrorMsg(){
		return 'No existe un Viaje con los datos ingresados.';
	}
	// Viajes Temporales
	function getViajeTempDltMsg(){
		return 'Viaje Temporal Eliminado Correctamente';
	}
	//Talones
	function getUnidadTalonSuccessMsg(){
		return 'Unidades Agregadas al Talon Correctamente';
	}
	function getUnidadTalonUpdateMsg(){
		return 'Unidades del Talon Actualizadas Correctamente';
	}
	function getUnidadTalonDltMsg(){
		return 'Unidades Borarradas del Talon Correctamente';
	}
	function getEntregaTalonSuccessMsg(){
		return 'Talon Entregado Correctamente';
	}
	function getTalonesViajeAddMsg($numTalones){
		if($numTalones == 1){
			return 'Talon Agregado Correctamente';
		} else {
			return 'Talones Agregados Correctamente';
		}
	}
	function getTalonesViajeUpdMsg(){
		return 'Talones Actualizados Correctamente';
	}
	function getTalonesViajeDltMsg($count){
		if($count > 1){
			return 'Talones Eliminados Correctamente';
		} else {
			return 'Talon Eliminado Correctamente';
		}
	}
	//Folios
	function getNoFolioMsg(){
		return 'No Existe Folio Relacionado';
	}
	//Cancelacion de Viajes
	function getViajeCanceladoSuccessMsg($tipoCancelacion){
		switch ($tipoCancelacion) {
			case 'VV':
				return 'Previaje Cancelado Correctamente';
				break;
			case 'VG':
				return 'Viaje con Gastos Cancelado Correctamente';
				break;
			case 'VA':
				return 'Viaje Asignado Cancelado Correctamente';
				break;
			case 'FO':
				return 'Folio de Gasto Cancelado Correctamente';
				break;
		}
	}
	//Cancelacion de unidades
	function getTalonCanceladoSuccessMsg(){
		return 'Talon Cancelado Correctamente';
	}
	function getCancelarUnidadSuccessMsg(){
		return 'Unidad Cancelada Correctamente';
	}
	//Comprobacion Viaje
	function getComprobacionViajeSuccessMsg(){
		return 'Recepci�n Exitosa';
	}

	//Da�os
	function getDanosSuccessMsg(){
		return 'Da&ntilde;o Creado Correctamente';
	}
	function getDanosUpdMsg(){
		return 'Da&ntilde;o Actualizado Correctamente';
	}

	//Da�os Vics
	function getDanosVicsSuccessMsg(){
		return 'Da&ntilde;os Creados Correctamente.';
	}
	function getDanosVicsFailedMsg(){
		return 'Error al crear los siguientes da&ntilde;os: ';
	}

	//Retrabajos
	function getRetrabajosSuccessMsg(){
		return 'Retrabajo Creado Correctamente';
	}
	function getRetrabajosUpdMsg(){
		return 'Retrabajo Actualizado Correctamente';
	}

	//cambios de destino programados
	function getHoldsSuccessMsg(){
		return 'Cambio de Destino Creado Correctamente';
	}
	function getHoldsDltMsg(){
		return 'Cambio de Destino Borrado Correctamente';
	}

	//Polizas
	function getFacturaGastosSuccessMsg(){
		return 'Factura Creada Correctamente';
	}
	function getFacturaGastosDltMsg(){
		return 'Factura Eliminada Correctamente';
	}
	function getPolizaSuccessMsg(){
		return 'Poliza Generada Correctamente';
	}
	function getPolizaParcialSuccessMsg(){
		return 'Poliza Parcial Generada Correctamente';
	}

	//IVA
	function  getIVAErrorMsg(){
		return 'IVA No Encontrado';
	}

	//FacturacionTCO
	function getFacturacionTCOSuccessMsg(){
		return 'Carga Generada Correctamente';
	}
	function getFacturacionTCOUpdMsg(){
		return 'Unidad Modificada Correctamente';
	}
	function getFacturacionTCOCancelMsg(){
		return 'Unidad Cancelada Correctamente';
	}

	//Facturas
	function getFacturaSuccessMsg(){
		return 'Factura Generada Correctamente';
	}
	function getFacturaMasivaSuccessMsg(){
		return 'Facturas Generadas Correctamente';
	}
	function getFacturaCanceladaSuccessMsg(){
		return 'Factura Cancelada Correctamente';
	}

	//Notas de Credito
	function getNotaCreditoSuccessMsg(){
		return 'Nota de Credito Generada Correctamente';
	}
	function getNotaCreditoMasivaSuccessMsg(){
		return 'Notas de Credito Generadas Correctamente';
	}
	function getNotaCreditoCanceladaSuccessMsg(){
		return 'Nota de Credito Cancelada Correctamente';
	}

	//Facturacion Unidades
	function getFacturacionUnidadesSuccessMsg(){
		return 'Unidades Facturadas Correctamente';
	}

	function getFacturacionUnidadesCobroMsg(){
		return 'Unidades Cobradas Correctamente';
	}


?>
