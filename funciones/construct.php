<?php
	function fn_construct($texto, $columna, $tipoDato, $banderaLike = 0){
		// Tipo de dato 0 - numerico, 1 - String, 2 - Fecha
		global $errorFiltro;
		global $globalSimbolo;
		global $globalOperando;
		global $condicion;

		global $gi_ini1;
		global $gi_long1;
		global $gi_ini2;
		global $gi_long2;

		$errorFiltro = 0;
		$globalSimbolo = 0;
		$globalOperando = "";
		$condicion = "";
		
		$gi_ini1  = 0;
		$gi_long1 = 0;
		$gi_ini2  = 0;
		$gi_long2 = 0;		

		$construct = "";
		$li_posini = 0;
		$li_posfin = 0;
		$li_long = 0;
		$continua = 1;
	
		if ($errorFiltro == 0){
			$texto = trim($texto);
			//SI ESTA VACIO NO SE CONTINUA
			if ($continua && strlen($texto) == 0){
			   $construct = "";
			   $continua = 0;
			}
			//SI CONTIENE NULL SE REVISA SI TAMBIEN CONTIENE NOT Y NO SE CONTINUA
			if ($continua && inStr(1, strtoupper($texto), "NULL") > 0){
				if (inStr(1, strtoupper($texto), "NOT") > 0)
					$construct = $columna . " IS NOT NULL";
				else
					$construct = $columna . " IS NULL";
				$continua = 0;
			}
			if ($continua){
				$condicion = "";
				$errorFiltro = 0;

				if ($tipoDato == 1 || $tipoDato == 2)
					$delimitador = "'";
				else
					$delimitador = "";

				checarSimboloInicio("<>", $texto, 1);
				checarSimboloInicio("!=", $texto, 2);
				checarSimboloInicio(">=", $texto, 3);
				checarSimboloInicio("<=", $texto, 4);

				if ($globalSimbolo == 0) {
					checarSimboloInicio("=", $texto, 5);
					checarSimboloInicio(">", $texto, 6);
					checarSimboloInicio("<", $texto, 7);
				}
				// ?????
				checarSimboloMedio("..", $texto, 9);
				//OR
				checarSimboloVarios("|", $texto, 10);
				//LIKE
				if($globalSimbolo == 0 && $tipoDato == 1){
					$globalSimbolo = 11;
					if($banderaLike == 0){
						$globalOperando = 'LIKE';
					} else {
						$globalOperando = '=';
					}
				}

				if ($errorFiltro != 1){
					//CERO ES CUANDO NO LLEVA UN OPERADOR
					if ($globalSimbolo == 0){
						//Y SI ES FECHA
						if ($tipoDato == 2){
							//AQUI DEBERIA IR EL FORMATO DE LA FECHA SI SE NECESITARA
						}
						$condicion = $columna . " = " . $delimitador . $texto . $delimitador;
					}       
					if ($globalSimbolo >= 1 && $globalSimbolo <= 4){
						if ($tipoDato == 2){
							$texto1 = substr($texto, 2);
							//AQUI DEBERIA IR EL FORMATO DE LA FECHA SI SE NECESITARA
							$texto = substr(texto, 0, 2) . $texto1;
						}
					  $condicion = $columna . " " . $globalOperando . " " . $delimitador . substr($texto, 2) . $delimitador;
					}                 
					if ($globalSimbolo >= 5 && $globalSimbolo <= 7){
					  if ($tipoDato == 2){
					     $texto1 = substr($texto, 1);
					     //$texto1 = fn_formatea_fecha($texto1, 1, "/");
					     //$texto1 = fn_format_Dis_Nat($texto1);
					     $texto = substr($texto, 0, 1) . $texto1;
					  }
					  $condicion = $columna . " " . $globalOperando . " " . $delimitador . substr($texto, 1) . $delimitador;
					}               
					if ($globalSimbolo >= 8 && $globalSimbolo <= 9){
					    if  ($tipoDato == 2){
					         $texto1 = substr($texto, $gi_ini1, $gi_long1);
					         //$texto1 = fn_formatea_fecha($texto1, 1, "/");
					         //$texto1 = fn_format_Dis_Nat($texto1);
					           
					         $texto2 = substr($texto, $gi_ini2, $gi_long2);
					         $texto2 = fn_formatea_fecha($texto2, 1, "/");
					         $texto2 = fn_format_Dis_Nat($texto2);
					           
					         $condicion = $columna . " BETWEEN " . $delimitador . $texto1 . $delimitador . " AND " . $delimitador . $texto2 . $delimitador;
					    }
					    else
					        $condicion = $columna . " BETWEEN " . $delimitador . substr($texto, $gi_ini1, $gi_long1) . $delimitador . " AND " . $delimitador . substr($texto, $gi_ini2, $gi_long2) . $delimitador;
					}                      
					if ($globalSimbolo == 10){
					      $li_posini = 1;
					      $li_posfin = 1;
					      $li_long = 0;
					      
					      while (inStr($li_posini, $texto, "|") > 0){
					            $li_posfin = inStr($li_posini, $texto, "|");
					            $li_long = $li_posfin - $li_posini;
					            
					            if ($li_posini > 1)
					               $condicion = $condicion . " OR ";
					            
					            if ($tipoDato == 2)
					            {
					               $texto1 = substr($texto, $li_posini - 1, $li_long);
					               //$texto1 = fn_formatea_fecha($texto1, 1, "/");
					               //$texto1 = fn_format_Dis_Nat($texto1);
					               $condicion = $condicion . " " . $columna + " = " . $delimitador . $texto1 . $delimitador;
					            }
					            else
					               $condicion = $condicion . " " . $columna . " = " . $delimitador . substr($texto, $li_posini - 1, $li_long) . $delimitador;
					            
					            $li_posini = $li_posfin + 1;
					      }               
					      if ($li_posini > 1)
					         $condicion = $condicion . " OR ";
					        
					      if ($tipoDato == 2)
					      {
					         $texto1 = substr($texto, $li_posini);
					         //$texto1 = fn_formatea_fecha($texto1, 1, "/");
					         //$texto1 = fn_format_Dis_Nat($texto1);
					         $texto = $texto1;
					         $condicion = " (" . $condicion . " " . $columna . " = " . $delimitador . $texto . $delimitador . ") ";
					      }
					      else
					         $condicion = " (" . $condicion . " " . $columna . " = " . $delimitador . substr($texto, $li_posini - 1) . $delimitador . ") ";
					}				                        
					if ($globalSimbolo >= 11 && $globalSimbolo <= 12){
						if($banderaLike == 0){
							$condicion = $columna . " " . $globalOperando . " " . $delimitador . "%" . $texto . "%" . $delimitador;
						} else {
							$condicion = $columna . " " . $globalOperando . " " . $delimitador . $texto . $delimitador;
						}
					}
					$construct = $condicion;
				}
				else
					$construct = "";
			}
		}
		return $construct;
	}
	
	function checarSimboloInicio($operando, $cadena, $simbolo){
		global $errorFiltro;
		global $globalSimbolo;
		global $globalOperando;
		global $condicion;

		$pos = 0;
		
		$pos = inStr(1, $cadena, $operando);

		if ($pos != 0) {
			if ((inStr($pos + 1, $cadena, $operando) == 0) && $pos == 1){
				if ($globalSimbolo == 0){
				   $globalSimbolo = $simbolo;
				   $globalOperando = $operando;
				}
				else
				  $errorFiltro = 1;
			}
			else
				$errorFiltro = 1;
		}
	}

	//NO TENGO IDEA PARA QUE SEA ESTA FUNCION
	function checarSimboloMedio($operando, $cadena, $simbolo){
		global $errorFiltro;
		global $globalSimbolo;
		global $globalOperando;
		global $condicion;

		global $gi_ini1;
		global $gi_long1;
		global $gi_ini2;
		global $gi_long2;

		$pos = 0;
		
		$pos = inStr(1, $cadena, $operando);

		if ($pos != 0){ 
			if ((inStr($pos + 1, $cadena, $operando) == 0) && ($pos > 1 && $pos < strlen($cadena))){
				if ($globalSimbolo == 0){
					$globalSimbolo = $simbolo;
					$globalOperando = $operando;
					$gi_ini1 = 0;
					$gi_long1 = $pos - 1;
					$gi_ini2 = $pos + strlen($operando) - 1;
					$gi_long2 = strlen($cadena) - $pos + strlen($operando);
				}
				else
					$errorFiltro = 1;
			}
			else
				$errorFiltro = 1;
		}
	}

	function checarSimboloVarios($operando, $cadena, $simbolo){
		global $errorFiltro;
		global $globalSimbolo;
		global $globalOperando;
		global $condicion;

		global $gi_ini1;
		global $gi_long1;
		global $gi_ini2;
		global $gi_long2;

		$pos = 0;
		
		$pos = InStr(1, $cadena, $operando);
		if ($pos > 1 && $pos < strlen($cadena)){
			if (inStr(strlen($cadena) - strlen($operando) + 1, $cadena, $operando) == 0){
				if ($globalSimbolo == 0){
					$globalSimbolo = $simbolo;
					$globalOperando = $operando;
				}
				else				
					$errorFiltro = 1;
			}
			else
				$errorFiltro = 1;
		}
		else
			if ($pos <> 0)
			   $errorFiltro = 1;
	}	

	function existeOperando($operando, $cadena, $simbolo, $tipoDato){
		global $errorFiltro;
		global $globalSimbolo;
		global $globalOperando;
		global $condicion;

		$pos = 0;

		$pos = inStr(1, $cadena, $operando);
		if ($pos != 0){
			if ($globalSimbolo == 0 && $tipoDato != 0){
			   $globalSimbolo = $simbolo;
			   $globalOperando = "LIKE";
		   }
		   else
			   $errorFiltro = 1;
		}
	}
	
	function inStr($pi_posini, $texto, $cadena){
		$long = strlen($cadena);
		for ($i = $pi_posini - 1; $i < strlen($texto); $i++)
		{
			if (trim(substr($texto, $i, $long)) == trim($cadena))
			{
		  		return $i + 1;
			}
		}
		return 0; 
	}  
	
	function fn_concatena_condicion($where, $condicion){
    	if ($condicion != ""){
       		if ($where == "") 
          		$where = "WHERE ";
       		else
          		$where = $where . " AND ";
       		$where = $where . $condicion;
		}    
		return $where;
	}
	
?>