<?php
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");


        echo "Inicio Custodias: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

        //INGRESO CUSTODIA - IU
        // UNIDADES DE EXPOTACION

        $sqlGetUnds = "SELECT h1.vin, '2018-04-01' as fechaInicial,h1.centroDistribucion, su.marca,'0093' as claveConcepto,1 as claveGrupo,now() as fechaEvento, null as fechaCobro, 0 as cantidad, 1 as importe, 'UI' as claveMovimiento, null as numeroFactura,   
DATEDIFF('2018-05-01','2018-04-01') as numerInserciones,
DATEDIFF(now(), h1.fechaEvento) as diasTotal,
DATEDIFF('2018-04-01', h1.fechaEvento) as comparacion 
FROM alhistoricounidadestbl h1,alunidadestbl au, casimbolosunidadestbl su
WHERE h1.claveMovimiento = 'L2'
AND h1.vin = au.vin
AND au.simboloUnidad = su.simboloUnidad
AND su.marca = 'GM'
AND h1.centroDistribucion = 'LZC02'
AND cast(h1.fechaEvento as date) < '2018-04-01' 
AND h1.vin NOT IN (SELECT h2.VIN FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L3');";

        $rsSqlGetUnds = fn_ejecuta_query($sqlGetUnds);


        $contador = sizeof($rsSqlGetUnds['root']);

        echo(" este es el numero total de Unidades:".$contador."\r\n");

            for ($i=0; $i <$contador; $i++) {

                $numComparacion = $rsSqlGetUnds['root'][$i]['comparacion'];
                $incDias = $rsSqlGetUnds['root'][$i]['diasTotal'];
                $numdiasTranscurridos = $rsSqlGetUnds['root'][$i]['numerInserciones'];
                $cDistribucion = $rsSqlGetUnds['root'][$i]['centroDistribucion'];
                $marcas = $rsSqlGetUnds['root'][$i]['marca'];
                $uVin = $rsSqlGetUnds['root'][$i]['vin'];
                $concepto = $rsSqlGetUnds['root'][$i]['claveConcepto'];
                $numGrupo = $rsSqlGetUnds['root'][$i]['claveGrupo'];                
                $numCantidad = $rsSqlGetUnds['root'][$i]['cantidad'];
                $monto = $rsSqlGetUnds['root'][$i]['importe'];
                $cMovimiento = $rsSqlGetUnds['root'][$i]['claveMovimiento'];
                $fechaInicial = $rsSqlGetUnds['root'][$i]['fechaInicial'];
                
                echo "antes de entrar a la validacion".$incDias."\r\n"; 

                echo "NumInserciones:".$numdiasTranscurridos."<br/>";
                echo "DiasTotal:".$incDias."<br/>";
                echo "comparacion:".$numComparacion."<br/>";

                if($incDias >= 5){
                    // unicamnete caso 2 , 3 , 4
                    //$numdiasTranscurridos= $numdiasTranscurridos -4;                     
                    //---------------------------------------------
                    // para caso 1 y 5
                    if ($numComparacion <= 4) {                        

                        $diasMes = 4 - $numComparacion;
                        
                        $numdiasTranscurridos= $numdiasTranscurridos - $diasMes ; 
                    }

                    echo "transcurridos2:".$numdiasTranscurridos."<br/>";                  
                    echo "diasMes".$diasMes;
                    //--------------------------------------------                
                    for ($e=0; $e < $numdiasTranscurridos; $e++) {
                        //$numDia = 1;
                                           
                        echo "numero de dias en total A INSERTAR:".$numdiasTranscurridos."<br/>";
                        echo("VIN: ".$uVin."  FECHAEVENTO".$fechaInicial." DIAS:".$incDias."<br/>");

                        // unicamente caso 2 , 3 , 4
                        //$incremento = $e + 4;
                        //--------------------------------------------------------------
                        // unicamnete caso 1 y 5
                        $incremento = $e + $diasMes;
                        //-----------------------------------------------------------------
                        // CASO 1 y5 UNICAMENTE
                        //$fechaInicial = '2018-02-01 11:59:25';
                        //--------------------------------------------------------------
                        $fRecortada = substr($fechaInicial, 0,10);
                        $fechaAnt = strtotime ($incremento.' day' , strtotime ($fRecortada) );
                        // PARA EL CASO 1 Y 5
                        $fechaAnt = strtotime ($e.' day' , strtotime ($fRecortada) ); 
                        // -------------------------------------------------------------
                        $fechaAnt = date ( 'Y-m-d' , $fechaAnt );
                         
                        $addUndImp = "INSERT INTO cocustodiastbl(centroDistribucion, marcaUnidad, vin, claveConcepto, claveGrupo, fechaEvento, fechaCobro, cantidad, importe, claveMovimiento, numeroFactura) ".
                        "VALUES (".
                        "'".$cDistribucion."', ".
                        "'".$marcas."', ".
                        "'".$uVin."', ".
                        "'".$concepto."', ".
                        "'".$numGrupo."', ".
                        "'".$fechaAnt."', ".
                        "null, ".
                        "'".$numCantidad."', ".
                        "'".$monto."', ".
                        "'".$cMovimiento."', ".
                        "null )";

                        fn_ejecuta_query($addUndImp);

                        //$numDia = $numDia +1;                       
                    }
                }else{
                    echo "Unidad con menos de 5 dias";
                }                  
            }
                    
        //echo "Final Custodias: ".date("Y-m-d H:i", strtotime("now"))."\r\n";

        /*
            -- primer query implementado    
                SELECT h1.vin, '2017-12-01' as fechaInicial,h1.centroDistribucion, su.marca,'0093' as claveConcepto,1 as claveGrupo,(SELECT max(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L2' AND h2.vin = h1.vin) as fechaEvento, null as fechaCobro, 0 as cantidad, 1 as importe, 'UI' as claveMovimiento, null as numeroFactura,
                DATEDIFF(h1.fechaEvento, '2017-12-01') as numerInserciones,
                DATEDIFF(h1.fechaEvento,(SELECT max(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L2' AND h2.vin = h1.vin)) as  diasTotal,
                DATEDIFF('2017-12-01',(SELECT max(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L2' AND h2.vin = h1.vin)) AS comparacion
                FROM alhistoricounidadestbl h1, alunidadestbl au, casimbolosunidadestbl su
                WHERE h1.claveMovimiento = 'L3'
                AND h1.vin = au.vin
                AND au.simboloUnidad = su.simboloUnidad
                AND su.marca = 'GM'
                AND h1.centroDistribucion = 'LZC02'
                AND h1.fechaEvento BETWEEN '2017-12-01' AND '2017-12-31'
                AND h1.vin IN (SELECT h2.VIN FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L2' AND h2.fechaEvento <= '2017-12-01')

            -- segundo Query Implementado

                SELECT  h1.vin, (SELECT h3.fechaEvento FROM alhistoricounidadestbl h3 WHERE h3.vin = au.vin AND h3.claveMovimiento = 'L2') as fechaInicial,h1.centroDistribucion, su.marca,'0093' as claveConcepto,1 as claveGrupo,now() as fechaEvento, null as fechaCobro, 0 as cantidad, 1 as importe, 'UI' as claveMovimiento, null as numeroFactura, 
                DATEDIFF(h1.fechaEvento, (SELECT h3.fechaEvento FROM alhistoricounidadestbl h3 WHERE h3.claveMovimiento = 'L2' AND h3.fechaEvento >= '2017-12-01' AND h3.vin = h1.vin)) as numerInserciones,
                DATEDIFF(h1.fechaEvento, (SELECT h3.fechaEvento FROM alhistoricounidadestbl h3 WHERE h3.claveMovimiento = 'L2' AND h3.fechaEvento >= '2017-12-01' AND h3.vin = h1.vin)) as diasTotal,
                DATEDIFF(h1.fechaEvento, (SELECT h3.fechaEvento FROM alhistoricounidadestbl h3 WHERE h3.claveMovimiento = 'L2' AND h3.fechaEvento >= '2017-12-01' AND h3.vin = h1.vin)) as comparacion
                FROM alhistoricounidadestbl h1,alunidadestbl au, casimbolosunidadestbl su
                WHERE h1.claveMovimiento = 'L3'
                AND h1.vin = au.vin
                AND au.simboloUnidad = su.simboloUnidad
                AND su.marca = 'GM'
                AND h1.centroDistribucion = 'LZC02'
                AND h1.fechaEvento BETWEEN '2017-12-02' AND '2017-12-31'
                AND h1.vin IN (SELECT h2.VIN FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L2' AND h2.fechaEvento >= '2017-12-01')

            -- tercer Query Implementado

                SELECT h1.vin, (SELECT h3.fechaEvento FROM alhistoricounidadestbl h3 WHERE h3.vin = au.vin AND h3.claveMovimiento = 'L2') as fechaInicial,h1.centroDistribucion, su.marca,'0093' as claveConcepto,1 as claveGrupo,now() as fechaEvento, null as fechaCobro, 0 as cantidad, 1 as importe, 'UI' as claveMovimiento, null as numeroFactura, 
                DATEDIFF('2018-01-01',h1.fechaEvento) as numerInserciones,
                DATEDIFF((SELECT H2.fechaEvento FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L3' AND h2.fechaEvento >= '2017-12-01' AND h2.vin = h1.vin),h1.fechaEvento) as diasTotal,
                DATEDIFF((SELECT H2.fechaEvento FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L3' AND h2.fechaEvento >= '2017-12-01' AND h2.vin = h1.vin),h1.fechaEvento) as comparacion
                FROM alhistoricounidadestbl h1,alunidadestbl au, casimbolosunidadestbl su
                WHERE h1.claveMovimiento = 'L2'
                AND h1.vin = au.vin
                AND au.simboloUnidad = su.simboloUnidad
                AND su.marca = 'GM'
                AND h1.centroDistribucion = 'LZC02'
                AND h1.fechaEvento BETWEEN '2017-12-01' AND '2017-12-31'
                AND h1.vin IN (SELECT h2.VIN FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L3' AND h2.fechaEvento >= '2018-01-01');

            -- cuarto Query Implementado
                SELECT  h1.vin, h1.fechaEvento as fechaInicial,h1.centroDistribucion, su.marca,'0093' as claveConcepto,1 as claveGrupo,now() as fechaEvento, null as fechaCobro, 0 as cantidad, 1 as importe, 'UI' as claveMovimiento, null as numeroFactura,  
                DATEDIFF('2018-01-01',h1.fechaEvento) as numerInserciones,
                DATEDIFF(now(), h1.fechaEvento) as diasTotal,
                DATEDIFF(now(), h1.fechaEvento) as comparaciones  
                FROM alhistoricounidadestbl h1,alunidadestbl au, casimbolosunidadestbl su
                WHERE h1.claveMovimiento = 'L2'
                AND h1.vin = au.vin
                AND au.simboloUnidad = su.simboloUnidad
                AND h1.centroDistribucion = 'LZC02'
                AND h1.fechaEvento BETWEEN '2017-12-01' AND '2017-12-31'
                AND h1.vin NOT IN (SELECT h2.VIN FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L3')

            -- quinto query implementado

                    SELECT h1.vin, '2017-12-01' as fechaInicial,h1.centroDistribucion, su.marca,'0093' as claveConcepto,1 as claveGrupo,now() as fechaEvento, null as fechaCobro, 0 as cantidad, 1 as importe, 'UI' as claveMovimiento, null as numeroFactura,   
                        DATEDIFF('2018-01-01','2017-12-01') as numerInserciones,
                        DATEDIFF(now(), h1.fechaEvento) as diasTotal,
                        DATEDIFF('2017-12-01', h1.fechaEvento) as comparacion 
                        FROM alhistoricounidadestbl h1,alunidadestbl au, casimbolosunidadestbl su
                        WHERE h1.claveMovimiento = 'L2'
                        AND h1.vin = au.vin
                        AND au.simboloUnidad = su.simboloUnidad
                        AND su.marca = 'GM'
                        AND h1.centroDistribucion = 'LZC02'
                        AND cast(h1.fechaEvento as date) < '2017-12-01' 
                        AND h1.vin NOT IN (SELECT h2.VIN FROM alhistoricounidadestbl h2 WHERE h2.claveMovimiento = 'L3');
        */
?>    


