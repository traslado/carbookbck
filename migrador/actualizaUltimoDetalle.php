<?php

// PARA EJECUTAR ESTA INTERFACE PRIMERO SE DEBE DE MODIFICAR LA FECHA DE LOS 2 QUERYS EN DONDE PIDE LA FECHA DESDE DONDE SE QUIERE CORRER LA INTERFACE HASTA EL DIA DE HOY LOS DOS QUERYS SON LOS SIGUIENTES ($sqlGetUnidades,$sqlAddUnd).

	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
	
    $sqlGetUnidades = " SELECT * 
						FROM alhistoricounidadestbl hu 
						WHERE CAST(hu.fechaEvento AS DATE) BETWEEN CAST('2020-01-01' AS DATE) AND CAST(now() AS DATE) 
						AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) 
                      							FROM alhistoricounidadestbl hu2 
                      							WHERE hu2.vin = hu.vin 
                      							AND hu2.claveMovimiento not in (SELECT ge.valor 
                                                  FROM cageneralestbl ge 
                                                  WHERE ge.tabla = 'alHistoricoUnidadesTbl' 
                                                  AND ge.columna = 'nvalidos'))";

    $rsUnidades = fn_ejecuta_query($sqlGetUnidades);

    //echo json_encode($rs);

    $numUnidades = sizeof($rsUnidades['root']); 


    echo "numeroTotal Unidades".$numUnidades;   

    for ($i=0; $i < $numUnidades; $i++) { 
    	
    	$undValor = $rsUnidades['root'][$i]['vin'];


		$alCompara = "SELECT  vin,centroDistribucion ".
					"FROM alultimodetalletbl ".
					"WHERE vin = '".$undValor."' ";

		$rsVin = fn_ejecuta_query($alCompara);

	if (sizeof($rsVin['root']) == 1 ) {

		// if ($rsVin['root'][0]['centroDistribucion'] != 'CMDAT' && $rsVin['root'][0]['centroDistribucion'] != 'LZC02' ) {

		if ($rsUnidades['root'][$i]['claveChofer'] == null)
		{

			$claveChofer= "null";
		}
		else 
		{
			$claveChofer=$rsUnidades['root'][$i]['claveChofer'];

		}

			$sqlUpdUnd = "UPDATE alultimodetalletbl ".
						"SET centroDistribucion= '".$rsUnidades['root'][$i]['centroDistribucion']."', ".
						"fechaEvento = '".$rsUnidades['root'][$i]['fechaEvento']."', ".
						"claveMovimiento = '".$rsUnidades['root'][$i]['claveMovimiento']."', ".
						"distribuidor = '".$rsUnidades['root'][$i]['distribuidor']."', ".
						"idTarifa = '".$rsUnidades['root'][$i]['idTarifa']."', ".
						"localizacionUnidad = '".$rsUnidades['root'][$i]['localizacionUnidad']."', ".
						"claveChofer =".$claveChofer.",".
						"usuario = '".$rsUnidades['root'][$i]['usuario']."', ".
						"observaciones = '".$rsUnidades['root'][$i]['observaciones']."', ".
						"ip = '".$rsUnidades['root'][$i]['ip']."' ".
						"WHERE vin = '".$rsUnidades['root'][$i]['vin']."'";


					fn_ejecuta_query($sqlUpdUnd);

		         echo "se actualizo la unidad".$rsUnidades['root'][$i]['vin'];

	/*	}
		else
		{
 			
 			echo "Unidad de COMODATO  O LZC02 (No actualizada UD)".$rsUnidades['root'][$i]['vin'];

		}*/
	}

		else {
		
			$sqlAddUnd = "INSERT INTO alultimodetalletbl (centroDistribucion,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) 
						SELECT centroDistribucion,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip 
						FROM alhistoricounidadestbl hu 
						WHERE CAST(hu.fechaEvento AS DATE) BETWEEN CAST('2020-01-01' AS DATE) AND CAST(now() AS DATE) 
						AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) 
                      							FROM alhistoricounidadestbl hu2 
                      							WHERE hu2.vin = hu.vin 
                      							AND hu2.claveMovimiento not in (SELECT ge.valor 
                                                  FROM cageneralestbl ge 
                                                  WHERE ge.tabla = 'alHistoricoUnidadesTbl' 
                                                  AND ge.columna = 'nvalidos')) 
												AND  hu.vin = '".$rsUnidades['root'][$i]['vin']."'  ";

			fn_ejecuta_query($sqlAddUnd);

			echo "se inserto la unidad".$rsUnidades['root'][$i]['vin'];									
		}

    }

  
?>