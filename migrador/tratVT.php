<?php
	session_start();
	
    require_once("../funciones/generalesProduccion.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

	$dirFile = "C:\\servidores\\apache\\htdocs\\tratdt\\vtFebrero.csv";
	$productos = fopen ($dirFile , "r" );//leo el archivo que contiene los datos del producto
	$tractorID = '';
	$origenPlaza = '';
	$destinoPlaza = '';
	
	while (($datos =fgetcsv($productos,1000,",")) !== FALSE ){//Leo linea por linea del archivo hasta un maximo de 1000 caracteres por linea leida usando coma(,) como delimitador
 		
		$sqlGetViaje = "SELECT idTractor FROM caTractoresTbl ".
						"WHERE compania = '".$datos[1]."' ".
						"AND tractor = '".$datos[2]."';";

		$rsIdTractor = fn_ejecuta_query($sqlGetViaje);

		$tractorID = $rsIdTractor['root'][0]['idTractor'];

		$sqlGetPlazaOri = "SELECT idPlaza as idOrigen FROM caPlazasTbl ".
						  "WHERE plaza = '".$datos[12]."' ";

		$rsIdPlazaOri = fn_ejecuta_query($sqlGetPlazaOri);

		$sqlGetPlazaDes = "SELECT idPlaza as idDestino FROM caPlazasTbl ".
						  "WHERE plaza = '".$datos[14]."' ";

		$rsIdPlazaDes = fn_ejecuta_query($sqlGetPlazaDes);

		$sqlGetKmPlaza = "SELECT 1 as kmsPlaza  ".
							"FROM cakilometrosplazatbl ".
							"WHERE idPlazaOrigen = ".$datos[12]." ".
							"AND idPlazaDestino = ".$datos[14]." ";
	
		$rsKmPlaza = fn_ejecuta_query($sqlGetKmPlaza);


		if($rsIdTractor['root'][0]['idTractor'] == ''){
			echo "El tractor: ".$datos[2]." de la compania: ".$datos[1]. ", se va a insertar el tractor en el catalogo.    ";

			$sqlAddTractor = "INSERT INTO caTractoresTbl (compania, tractor, marca, modelo, serie, placas, rendimiento, ejes, observaciones, estatus, tarjetaIave, kilometrosservicio, tipoTractor) ".
								"VALUES ('".$datos[1]."', ".
								"'".$datos[2]."', 'FL','...','.','.','2.10','0','Nvo Tractor con migracion','1','0','10000','FL'); ";

			fn_ejecuta_query($sqlAddTractor);


			$sqlGetViaje = "SELECT idTractor FROM caTractoresTbl ".
							"WHERE compania = '".$datos[1]."' ".
							"AND tractor = '".$datos[2]."';";

			$rsIdTractor = fn_ejecuta_query($sqlGetViaje);

			$tractorID = $rsIdTractor['root'][0]['idTractor'];
			
		}elseif ($rsIdPlazaOri['root'][0]['idOrigen'] == '') {
			echo("La Plaza Origen u oriRut: ".$datos[12]." No existe, ccrealo en la tabla caPlazasTbl    ");			
		}elseif ($rsIdPlazaDes['root'][0]['idDestino'] == '') {
			echo("La Plaza Destino o desRut: ".$datos[14]." No existe, ccrealo en la tabla caPlazasTbl    ");
		}elseif ($rsKmPlaza['root'][0]['kmsPlaza']) {
			echo("No existe Kilometros para la relacion de : ".$datos[12]." y ".$datos[14]." , necesitas darlos de alta EN cakilometrosplazatbl     ");
		}

		$sqlGetViajeTractor = "SELECT 1 as vTractor FROM trviajestractorestbl ".
								"WHERE centroDistribucion = '".$datos[0]."' ".
								"AND idTractor = ".$tractorID." ".
								"AND viajeifx = '".$datos[4]."' ".
								"AND idPlazaOrigen = '".$origenPlaza."' ".
								"AND idPlazaDestino = '".$destinoPlaza."'; ";

		$rsSqlGetViajeTractor = fn_ejecuta_query($sqlGetViajeTractor);

		if($rsSqlGetViajeTractor['root'][0]['vTractor'] == '1'){

			echo nl2br("Viaje: ".$datos[4].", ya existe \r\n", false);

			$updviaje = "UPDATE trviajestractorestbl ".
					    "SET numeroUnidades = '".$datos[25]."', ".
					    "    numeroRepartos = '".$datos[10]."', ".
					    "    claveMovimiento = '".$datos[26]."' ".
						"WHERE centroDistribucion = '".$datos[0]."' ".
						"AND idTractor = ".$tractorID." ".
						"AND numViaje = '".$datos[4]."' ".
						"AND idPlazaOrigen = ".$origenPlaza." ".
						"AND idPlazaDestino = ".$destinoPlaza;

			//fn_ejecuta_query($updviaje);					
		}						

		$tractorID = $rsIdTractor['root'][0]['idTractor'];
		$origenPlaza = $rsIdPlazaOri['root'][0]['idOrigen'];
		$destinoPlaza = $rsIdPlazaDes['root'][0]['idDestino'];	


		$sqlAddTratVt = "INSERT INTO tratVt (centroDistribucion, companiaTractor, tractor, idtractor, numviaje, fechaEvento, numTalones, aux01, aux02, aux03, numDistribuidor, numChofer, oriRut, idPlazaOrigen, desRut, idPlazaDestino, cveRuta, oriReg, desReg, cveReg, iva, kmsRec, impxunidad, retencion, diasVia, numUnidades, cveProc) ".
		"VALUES ('".$datos[0]."', ".
		"'".$datos[1]."', ".
		"'".$datos[2]."', ".
		"'".$tractorID."', ".
		"'".$datos[4]."', ".
		"'".$datos[5]."', ".
		"'".$datos[6]."', ".
		"'".$datos[7]."', ".
		"'".$datos[8]."', ".
		"'".$datos[9]."', ".
		"'".$datos[10]."', ".
		"'".$datos[11]."', ".
		"'".$datos[12]."', ".
		"'".$origenPlaza."', ".
		"'".$datos[14]."', ".
		"'".$destinoPlaza."', ".
		"'".$datos[16]."', ".
		"'".$datos[17]."', ".
		"'".$datos[18]."', ".
		"'".$datos[19]."', ".
		"'".$datos[20]."', ".
		"'".$datos[21]."', ".
		"'".$datos[22]."', ".
		"'".$datos[23]."', ".
		"'".$datos[24]."', ".
		"'".$datos[25]."', ".
		"'".$datos[26]."') ";

		$rsSqlAddVt = fn_ejecuta_query($sqlAddTratVt);				
	}


	fclose ($productos);//Cierra el archivo

?>	