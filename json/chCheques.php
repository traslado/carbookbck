<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/idiomas/mensajesES.php");
    require_once("../funciones/fpdf/fpdf.php");

    $_REQUEST = trasformUppercase($_REQUEST);
    $_SESSION['usuario'] = 'antoniox';

    switch($_REQUEST['chChequesActionHdn']){
    	case 'getSigCheque':
    		getSigCheque();
    		break;
    	case 'getCheques':
    		echo json_encode(getCheques());
    		break;
    	case 'getFacturasTCO':
    		echo json_encode(getFacturasTCO());
    		break;
    	case 'getFacturasInfo':
    		getFacturasInfo();
    		break;
    	case 'addCheque':
    		addCheque();
    		break;
    	case 'printCheque':
    		printCheque();
    		break;
        case 'cancelarCheque':
            cancelarCheque();
            break;
        case 'addChequeEspecial':
            echo json_encode(addChequeEspecial());
            break;
        case 'generaPDF':
            generaPDF();
            break;
        case 'printPDF':
            printPDF();
            break;
    	default:
            echo '';
    		
    }
    function getSigCheque(){

        $sqlSigCheque = "SELECT *, IFNULL(MAX(folioCheque) + 1, '1') as sigCheque ".
    					"FROM chChequesTbl WHERE idCuentaBancaria = ".$_REQUEST['facturacionIdCtaBancaria'].";";

    	$rs = fn_ejecuta_query($sqlSigCheque);

    	echo json_encode($rs);
    }
    function getCheques(){
    	$lsWhereStr = "WHERE ch.compania = ca.compania ".
    				  "AND ch.distribuidorCentro = di.distribuidorCentro ".
    				  "AND ch.idCuentaBancaria = cu.idCuentaBancaria ".
    				  "AND cu.idBanco = ba.idBanco ";

    	if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionIdCheque'], "ch.idCheque", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionFolioCheque'], "ch.folioCheque", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
    	if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionDistribuidor'], "di.distribuidorCentro", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionIdCtaBancaria'], "cu.idCuentaBancaria", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionCtaBancaria'], "cu.cuenta", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionCompania'], "ca.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionIdBanco'], "ba.idBanco", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionBanco'], "ba.banco", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionFechaInicial'], "ch.fechaInicial", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionFechaFinal'], "ch.fechaFinal", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

    	$sqlGetCheques = "SELECT ch.idCheque, ch.folioCheque, ch.compania, ca.descripcion, ch.distribuidorCentro, di.descripcionCentro, ".
    					 "ch.idCuentaBancaria, cu.cuenta, cu.idBanco, ba.banco, cu.estatus, ch.numeroUnidades, ch.importe, ".
    					 "ch.fechaInicial, ch.fechaFinal, ch.fechaElaboracion, ch.fechaEntrega, ch.fechaCobro ".
    					 "FROM chChequesTbl ch, caCompaniasTbl ca, caDistribuidoresCentrosTbl di, caCuentasBancariasTbl cu, caBancosTbl ba ".
    					 $lsWhereStr;

    	$rs = fn_ejecuta_query($sqlGetCheques);

        for ($i=0; $i < count($rs['root']) ; $i++) { 
            $rs['root'][$i]['descCentro'] = $rs['root'][$i]['distribuidorCentro'].' - '.$rs['root'][$i]['descripcionCentro']; 
        }
    	return $rs;
    }
    function getFacturasTCO(){
    	$lsWhereStr = "WHERE ca.estatusChequeBonificacion = 'D' ";
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionCompania'], "ca.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionDistribuidor'], "ca.distribuidor", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionFechaInicio'], "ca.fechaFacturacion", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionFechaFin'], "ca.fechaFacturacion", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

    	$sqlgetFacturas = "SELECT ca.*, (SELECT u.simboloUnidad FROM alUnidadesTbl u WHERE u.vin = ca.vin) AS simbolo, ".
    					  "(SELECT su.importeBonificacion FROM caSimbolosUnidadesTbl su, alUnidadesTbl u ".
    					  					"WHERE u.vin = ca.vin AND u.simboloUnidad = su.simboloUnidad)  AS importeBonificacion, ".
						  "(SELECT 1 FROM faCargaTCOCanceladasTbl can WHERE can.vin = ca.vin ) AS cancelada ".
    					  "FROM faCargaTcoTbl ca ".$lsWhereStr;

    	$rs = fn_ejecuta_query($sqlgetFacturas);

    	return $rs;
    }
    function getFacturasInfo(){
    	$lsWhereStr = "";
    	if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionFechaInicio'], "fechaFacturacion", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionFechaFin'], "fechaFacturacion", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['facturacionDistribuidor'], "distribuidor", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

    	$sqlFacturas = "SELECT (SELECT COUNT(*) FROM faCargaTcoTbl ".$lsWhereStr.") AS delPeriodo, ".
							  "(SELECT COUNT(*) FROM facargatcocanceladastbl ".$lsWhereStr.") AS canceladas, ".
							  "(SELECT COUNT(*) FROM faCargaTcoTbl ".$lsWhereStr.") - ".
							  "(SELECT COUNT(*) FROM facargatcocanceladastbl ".$lsWhereStr.") AS porPagar;";

    	$rs = fn_ejecuta_query($sqlFacturas);

    	echo json_encode($rs);
    }

    function addCheque(){
    	$a['success'] = true;

    	if($_REQUEST['facturacionFolioCheque'] == ""){
            $e[] = array('id'=>'facturacionFolioCheque','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionDistribuidor'] == ""){
            $e[] = array('id'=>'facturacionDistribuidor','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionIdBanco'] == ""){
            $e[] = array('id'=>'facturacionIdBanco','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionCtaBancaria'] == ""){
            $e[] = array('id'=>'facturacionCtaBancaria','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionNumUnidades'] == ""){// Numero unidades por pagar---
            $e[] = array('id'=>'facturacionNumUnidades','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionFechaInicio'] == ""){
            $e[] = array('id'=>'facturacionFechaInicio','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionFechaFin'] == ""){  	
            $e[] = array('id'=>'facturacionFechaFin','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if($_REQUEST['facturacionTipoCheque'] == 'E' && $_REQUEST['facturacionImporte'] == ""){
            $e[] = array('id'=>'facturacionImporte','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $fechaFin = $_REQUEST['facturacionFechaFin'];
        $fechaInicio = $_REQUEST['facturacionFechaInicio'];
        $tipoCheque = $_REQUEST['facturacionTipoCheque'] || 'B'; 
    	if($a['success'] == true){

	    	$sqlInsertCheque = "INSERT INTO chChequesTbl(folioCheque, compania, distribuidorCentro, idCuentaBancaria, ".
	    					   "estatusCheque, numeroUnidades, importe, fechaInicial, fechaFinal, fechaElaboracion, ".
	    					   "fechaEntrega, fechaCobro) ";
            //348.00 FIJO
            
            $importe = (floatval($_REQUEST['facturacionNumUnidades']) * 348.00); 
			$sqlInsertCheque .= "VALUES(".$_REQUEST['facturacionFolioCheque'].", "."'TCO', '".$_REQUEST['facturacionDistribuidor']."', ".
								$_REQUEST['facturacionCtaBancaria'].", '".$tipoCheque."', ".$_REQUEST['facturacionNumUnidades'].", ".
								$importe.", '".$fechaInicio."', '".$fechaFin."', '".
								date('Y-m-d')."', NULL, NULL)";

			//fn_ejecuta_query($sqlInsertCheque);
			//$folioCheque = mysql_insert_id();
			
			$_REQUEST['facturacionFechaInicio'] = '>='.$fechaInicio;
			$_REQUEST['facturacionFechaFin'] = '<='.$fechaFin;
			$rsFacturas = getFacturasTCO();
			$rsFacturas = $rsFacturas['root'];

			$errors = array();

			$sqlDetalleCheque = "INSERT INTO chChequesDetalleTbl(idCheque, vin, ".
								"importe, estatusCheque, observaciones) VALUES";

			for ($i=0; $i < count($rsFacturas); $i++) { 
				$importe = floatval($rsFacturas[$i]['importeBonificacion']);
				if($rsFacturas[$i]['cancelada'] == "" && ($importe < 348.00 || $importe == null)){
					$errors[] = array('index' => $i, 'simbolo' => $rsFacturas[$i]['simbolo'], 'vin' => $rsFacturas[$i]['vin'], 'importe' => $rsFacturas[$i]['importeBonificacion']);
					continue;
				}
				if($i > 0){
					$sqlDetalleCheque .= ", ";
				}
				$tipo = $rsFacturas[$i]['cancelada'] == 1 ? 'C': 'B';
				//después hace un replace de ####
				$sqlDetalleCheque .= "(#### , '".$rsFacturas[$i]['vin']."', 348.00, '".$tipo."', '')";
									
			}
			if(count($errors) > 0){
				$a['success'] = false;
				$a['errorMessage'] = 'S&iacute;mbolos con distinto Importe.';
				$a['errors'] = $errors; 
			} else{

				fn_ejecuta_Add($sqlInsertCheque); //INSERTA EL CHEQUE
				$idCheque = mysql_insert_id();
				$sqlDetalleCheque = str_replace("####", $idCheque, $sqlDetalleCheque);
				fn_ejecuta_Add($sqlDetalleCheque); // INSERTA EL DETALLE

                // ACTUALIZA EN LAS BLANQUITAS/FACTURAS
                $sqlUpdFacturas = "UPDATE faCargaTCOTbl SET chequeBonificacion = ".$idCheque.
                                  " WHERE VIN IN (SELECT VIN FROM chChequesDetalleTbl WHERE idCheque = ".$idCheque.
                                  " AND estatusChequeBonificacion = 'D')";

                fn_ejecuta_Upd($sqlUpdFacturas);

				if(!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
					$a['sql'] = $sqlUpdFacturas;
	            	$a['successMessage'] = 'Cheque Creado Correctamente';
                    $a['idCheque'] = $idCheque;
                     
				} else {
	            	$a['success'] = false;
	            	$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDetalleCheque;
	        	}
			}
		}
		echo json_encode($a);
    }
    function addChequeEspecial(){
        $a['success'] = true;

        if($_REQUEST['facturacionFolioCheque'] == ""){
            $e[] = array('id'=>'facturacionFolioCheque','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionDistribuidor'] == ""){
            $e[] = array('id'=>'facturacionDistribuidor','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionIdBanco'] == ""){
            $e[] = array('id'=>'facturacionIdBanco','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionCtaBancaria'] == ""){
            $e[] = array('id'=>'facturacionCtaBancaria','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionTipoCheque'] == ""){
            $e[] = array('id'=>'facturacionTipoCheque','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionNumUnidades'] == ""){// Numero unidades por pagar---
            $e[] = array('id'=>'facturacionNumUnidades','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionFechaInicio'] == ""){
            $e[] = array('id'=>'facturacionFechaInicio','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionFechaFin'] == ""){     
            $e[] = array('id'=>'facturacionFechaFin','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['facturacionImporte'] == ""){     
            $e[] = array('id'=>'facturacionImporte','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if($_REQUEST['facturacionImporte'] == ""){
            $e[] = array('id'=>'facturacionImporte','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $fechaFin = $_REQUEST['facturacionFechaFin'];
        $fechaInicio = $_REQUEST['facturacionFechaInicio'];
        if($a['success'] == true){

            $sqlInsertCheque = "INSERT INTO chChequesTbl(folioCheque, compania, distribuidorCentro, idCuentaBancaria, ".
                               "estatusCheque, numeroUnidades, importe, fechaInicial, fechaFinal, fechaElaboracion, ".
                               "fechaEntrega, fechaCobro, observaciones) ";
            //348.00 FIJO
             
            $sqlInsertCheque .= "VALUES(".$_REQUEST['facturacionFolioCheque'].", "."'TCO', '".$_REQUEST['facturacionDistribuidor']."', ".
                                $_REQUEST['facturacionCtaBancaria'].", '".$_REQUEST['facturacionTipoCheque']."', ".$_REQUEST['facturacionNumUnidades'].", ".
                                $_REQUEST['facturacionImporte'].", '".$fechaInicio."', '".$fechaFin."', '".
                                date('Y-m-d')."', NULL, NULL, '".$_REQUEST['facturacionObservaciones']."');";

            //fn_ejecuta_query($sqlInsertCheque);
            //$folioCheque = mysql_insert_id();

            //--------------- SI ES UN CHEQUE ESPECIAL INSERTA Y TERMINA EL PROCESO ---------------------
            if($_REQUEST['facturacionTipoCheque'] == 'E'){
                fn_ejecuta_Add($sqlInsertCheque); //INSERTA EL CHEQUE
                if(!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
                    $idCheque = mysql_insert_id();
                    $a['sql'] = $sqlUpdFacturas;
                    $a['successMessage'] = 'Cheque Creado Correctamente';
                    $a['idCheque'] = $idCheque;
                     
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDetalleCheque;
                }
                return $a;
            }
            //--------------------------------------------------------------------------------------------
            
            $_REQUEST['facturacionFechaInicio'] = '>='.$fechaInicio;
            $_REQUEST['facturacionFechaFin'] = '<='.$fechaFin;
            $rsFacturas = getFacturasTCO();
            $rsFacturas = $rsFacturas['root'];

            $errors = array();

            $sqlDetalleCheque = "INSERT INTO chChequesDetalleTbl(idCheque, vin, ".
                                "importe, estatusCheque, observaciones) VALUES";

            for ($i=0; $i < count($rsFacturas); $i++) { 
                $importe = floatval($rsFacturas[$i]['importeBonificacion']);
                if($rsFacturas[$i]['cancelada'] == "" && ($importe < 348.00 || $importe == null)){
                    $errors[] = array('index' => $i, 'simbolo' => $rsFacturas[$i]['simbolo'], 'vin' => $rsFacturas[$i]['vin'], 'importe' => $rsFacturas[$i]['importeBonificacion']);
                    continue;
                }
                if($i > 0){
                    $sqlDetalleCheque .= ", ";
                }
                $tipo = $rsFacturas[$i]['cancelada'] == 1 ? 'C': 'B';
                //después hace un replace de ####
                $sqlDetalleCheque .= "(#### , '".$rsFacturas[$i]['vin']."', 348.00, '".$tipo."', '".$_REQUEST['facturacionObservaciones']."')";
                                    
            }
            if(count($errors) > 0){
                $a['success'] = false;
                $a['errorMessage'] = 'S&iacute;mbolos con distinto Importe.';
                $a['errors'] = $errors; 
            } else{

                fn_ejecuta_query($sqlInsertCheque); //INSERTA EL CHEQUE
                $idCheque = mysql_insert_id();
                $sqlDetalleCheque = str_replace("####", $idCheque, $sqlDetalleCheque);
                fn_ejecuta_query($sqlDetalleCheque); // INSERTA EL DETALLE

                // ACTUALIZA EN LAS BLANQUITAS/FACTURAS
                $sqlUpdFacturas = "UPDATE faCargaTCOTbl SET chequeBonificacion = ".$idCheque.
                                  " WHERE VIN IN (SELECT VIN FROM chChequesDetalleTbl WHERE idCheque = ".$idCheque.
                                  " AND estatusChequeBonificacion = 'D')";

                fn_ejecuta_query($sqlUpdFacturas);

                if(!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
                    $a['sql'] = $sqlUpdFacturas;
                    $a['successMessage'] = 'Cheque Creado Correctamente';
                    $a['idCheque'] = $idCheque;
                     
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDetalleCheque;
                }
            }
        }
        $a['emptyFields'] = $e;
        return $a;

    }
    function printCheque(){
        
        if($_REQUEST['facturacionIdCheque'] == ""){
            echo 'Error al Generar el Cheque';
            return;
        }
        //Obtiene los datos del Cheque
        $rsCheque = getCheques();
        $rsCheque = $rsCheque['root'];
        //Solo se necesita un solo registro
        if(count($rsCheque) != 1){
            echo 'Error al Imprimir el Cheque';
            return;
        }
        $rsCheque = $rsCheque[0];

        $fechaHoy = date_create();
        $localidad = 'MEXICO D.F.';

        $LINEA_MAX_LENGTH = 80;

        $myfile = fopen($_SERVER['DOCUMENT_ROOT']."chequeBonificacion.txt", "w") or die("Unable to open file!");

        //Fecha
        $text = out('n', 2).out('t', 10).$localidad.' a '.date_format($fechaHoy, 'd/m/Y');
        fwrite($myfile, $text);
        //Distribuidor
        //-------------------------------------------------------------------
        $left_ast = ($LINEA_MAX_LENGTH - strlen($rsCheque['descripcionCentro']))/2;
        for ($i = 0; $i <$left_ast - 2; $i++) 
            $asteriscos .= '*';
        fwrite($myfile, out('n', 2).$asteriscos);
        //------------------------------------------------------------------
        $text = ' '.$rsCheque['descripcionCentro'];
        fwrite($myfile, $text);
        fwrite($myfile, ' '.$asteriscos);
        //Cantidad
        $text = out('t',3).toMoney($rsCheque['importe'], '');
        fwrite($myfile, $text);
        //Cantidad Texto
        $text = out('n', 2).strtoupper(numToWord($rsCheque['importe'])).' PESOS 00/100 M.N.';
        fwrite($myfile, $text);

        //Concepto del Pago
            //Distribudor
        $text = out('n', 10).$rsCheque['distribuidorCentro'].' '.$rsCheque['descripcionCentro'];
        fwrite($myfile, $text);
            //Compañia
        $text = out('n', 1).$rsCheque['descripcion'];
            //No. Cheque, Banco y Cuenta
        $text .= ' CHEQ. NO. '. $rsCheque['folioCheque'].' '.$rsCheque['banco'].' CTA. '.$rsCheque['cuenta'];
        fwrite($myfile, $text);
            //Nombre Concepto
        $fechaInicial = date_create($rsCheque['fechaInicial']);
        $fechaFinal = date_create($rsCheque['fechaFinal']);
        $concepto = $rsCheque['observaciones'];
        $text = out('n', 1).( $concepto != "" ? $concepto : ('DEVOLUCION POR BONIFICACION DE '.$rsCheque['numeroUnidades'].' UNIDADES'));
        $text .= ' DEL '.date_format($fechaInicial, 'd/m/Y').' AL '.date_format($fechaFinal, 'd/m/Y');
        fwrite($myfile, $text);

        //Debe y Haber
        $text = out('n', 3).out('t', 9).toMoney($rsCheque['importe']).out('t',2).toMoney($rsCheque['importe']);
        fwrite($myfile, $text);
        $text = out('n', 6).out('t', 9).toMoney($rsCheque['importe']).out('t',2).toMoney($rsCheque['importe']);
        fwrite($myfile, $text);

        //No. Cheque
        $text = out('n', 1).'No. cheque: '.$rsCheque['folioCheque'];
        fwrite($myfile, $text);

        //Hecha Por
        $text = out('n', 2).date_format($fechaHoy, 'd/m/Y').' '.$_SESSION['usuario'];
        fwrite($myfile, $text);


        fclose($myfile);

        header('Location: /chequeBonificacion.txt');
        //echo file_get_contents("c://www/chequeBonificacion.txt");

    }
    function cancelarCheque(){
        $a['success'] = true;
        $e = array();
        if($_REQUEST['facturacionIdChequeHdn'] == ""){
            $e[] = array('id'=> 'facturacionIdChequeHdn', 'msg'=> getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['facturacionTipoCancelacion'] == ""){
            $e[] = array('id'=> 'facturacionTipoCancelacion', 'msg'=> getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($a['success'] == true){

            $sqlCancelacionDetalle = "DELETE FROM chChequesDetalleTbl WHERE idCheque = ".$_REQUEST['facturacionIdChequeHdn'].";";
            $sqlUpdFacturas = "UPDATE faCargaTCOTbl SET chequeBonificacion = NULL ".
                              "WHERE (SELECT vin FROM chChequesDetalleTbl WHERE idCheque = ".$_REQUEST['facturacionIdChequeHdn'].");";

            if($_REQUEST['facturacionTipoCancelacion'] == 'F'){ // ELIMINA FISICO
                $sqlCancelacionCheque =  "DELETE FROM chChequesTbl WHERE idCheque = ".$_REQUEST['facturacionIdChequeHdn'].";";

            } else if($_REQUEST['facturacionTipoCancelacion'] == 'N'){
                $sqlCancelacionCheque =  "UPDATE chChequesTbl SET estatusCheque = 'C' WHERE idCheque = ".$_REQUEST['facturacionIdChequeHdn'].";";
            }

            fn_ejecuta_query($sqlUpdFacturas);
            fn_ejecuta_query($sqlCancelacionDetalle);
            fn_ejecuta_query($sqlCancelacionCheque);

            if(!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
                $a['sql'] = $sqlCancelacionCheque;
                $a['successMessage'] = 'Cheque Cancelado Correctamente';                     
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelacionCheque;
            }
            echo json_encode($a);
        }
    }
    
    function generaPDF(){

        $rsCheques = file_get_contents('php://input');
        $rsCheques = json_decode($rsCheques, true);
        $columnKeys =  array_keys($rsCheques[0]);

        $abc = range("A", "Z");

        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial','',7);

        $pos = array(0, 20, 30, 50, 120, 130, 140, 160, 180);
        $head = array('Fecha', 'Cuenta', 'distribuidorCentro', 'Distribuidor', 'Folio' , '# U.', 'Importe', 'Banco', 'Estatus');

        for ($i=0; $i < count($columnKeys) -2  ; $i++) {
            if($columnKeys[$i] == 'distribuidorCentro')
                continue;
            $pdf->SetXY($pos[$i], 0);
            $pdf->Cell(2, 2, $head[$i]);
        }
        
        for ($j=0; $j < count($rsCheques) ; $j++) { 
           $cont = 0;
           for ($i=0; $i < count($columnKeys) - 2; $i++) { 
                if($columnKeys[$i] == 'distribuidorCentro')
                    continue;
                $pdf->SetXY($pos[$i], ($j + 1)*5); 
                $data = $rsCheques[$j][$columnKeys[$i]];
                $pdf->Cell(2, 2, $data);
                $cont++;
                
           }
        }

        $filename= $_SERVER['DOCUMENT_ROOT']."reporteCheques " . date('Y-m-d') . ".pdf";
        $pdf->Output($filename,'F');
    } 
    function printPDF(){

        $file = $_SERVER['DOCUMENT_ROOT'].'testpdf.pdf';
        $filename =  'Reporte de Cheques '.date('Y-m-d').'.pdf'; 

        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Accept-Ranges: bytes');

        readfile($file);

    }
?>
