<?php
  session_start();
	
	include_once("../funciones/generales.php");
	include_once("../funciones/construct.php");
	require_once('../funciones/Classes/PHPExcel/IOFactory.php');

  switch($_REQUEST['actionHdn']){
      case 'leeArchivo':
          leeArchivo();
          break;
      case 'addUnidadesMasivo':
          addUnidadesMasivo();
          break;          
      default:
          echo '';
  }

	function leeArchivo(){
		  $a = array();
			$a['success'] 		= true;
			$a['msjResponse'] = "";
			$a['unidades'] 		= '';
			
			if(isset($_FILES)) {
				if($_FILES["trapCambioEstatusMasivoArchivoFld"]["error"] > 0){
					$a['success'] = false;
					$a['message'] = $_FILES["trapCambioEstatusMasivoArchivoFld"]["error"];
					$a['msjResponse'] = "Falta seleccionar el archivo." ;
		
				} else {
					$temp_file_name = $_FILES['trapCambioEstatusMasivoArchivoFld']['tmp_name']; 
					$original_file_name = $_FILES['trapCambioEstatusMasivoArchivoFld']['name'];
				  
					// Find file extention 
					$ext = explode ('.', $original_file_name); 
					$ext = $ext [count ($ext) - 1]; 
				 	
					// Remove the extention from the original file name 
					$file_name = str_replace ($ext, '', $original_file_name); 
					$nombre = $file_name . $ext;
				 	
				  $new_name = $_SERVER['DOCUMENT_ROOT'].'/archCambioEstatus/'. $file_name . $ext;
				  //$new_name = $_SERVER['DOCUMENT_ROOT'].'archCambioEstatus/cambioEstatus.xlsx';				//CHK solo para pruebas local
					///*
					if (move_uploaded_file($temp_file_name, $new_name)){		
						if (!file_exists($new_name)){
							$a['success'] = false;
							$a['msjResponse'] = "Error al procesar el archivo " . $new_name;
						} else {
							$a['success'] = true;
							$a['msjResponse'] = "Archivo Cargado";
							$a['archivo'] = $file_name . $ext;
							$a = leerXLS($new_name);
						}
				 	} else { 
						$a['success'] = false;
						$a['msjResponse'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name.'. Favor de avisar a Sistemas.';
					}
					//*/
					//$a = leerXLS($new_name);																														//CHK solo para pruebas local
				}
			} else {
				$a['success'] = false;
				$a['msjResponse'] = "Error al leer el archivo.";
			}
			
			echo json_encode($a);			
	}
	
	function leerXLS($nombreArch){
		  $a = array();
		  $a['success'] 		= true;
			$a['msjResponse'] = "Archivo cargado.<br>";
			$a['unidades'] 		= '';
			$error = 0;
			
		  date_default_timezone_set('America/Mexico_City');
		
			$ruta = "../../archCambioEstatus/";
			$rutaRespal = "../../respaldoArchCambioEstatus/";
			//var_dump($nombreArch);

		  try {
		      $inputFileType 	= PHPExcel_IOFactory::identify($nombreArch);
		      $objReader 			= PHPExcel_IOFactory::createReader($inputFileType);
		      $objPHPExcel = $objReader->load($nombreArch);
		  } catch(Exception $e) {
		      $a['success'] = false;
		      $a['msjResponse'] = "Error al leer el archivo ".pathinfo($inputFileName,PATHINFO_BASENAME);
		  }
		  
		  if($a['success'])
		  {
				  $sheet = $objPHPExcel->getSheet(0);
				  $highestRow = $sheet->getHighestRow();
				  $highestColumn = $sheet->getHighestColumn();
				
				  //Se obtiene el valor de cada registro de la columna "A" sin tomar en cuenta el cabecero
				  $rowData 	= $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);
	        $j = 0;
	        for($i = 0; $i<sizeof($rowData); $i++){
	        		$vin = $rowData[$i][0];
	        		if(strlen($vin) == 17)
	        		{
									$encontro = false;
									foreach($a['unidades']['root'] as $item)
									{
											if(isset($item['vin']) && $item['vin'] == $vin)
											{
													$encontro = true;
													break;
											}
									}
									if(!$encontro)
									{
			        				$a['unidades']['root'][$j]['vin'] = $vin;
			        				$j++;											
									}
	        		}
	        }				   		  		
		  }
		  if($j > 0)
		  {
        	$k = 0;
        	foreach($a['unidades']['root'] as $item)
					{
							$vin = $item['vin'];

			        //getNoBloqueadas			        
			        $sql = "SELECT u.vin, u.avanzada, u.distribuidor AS distribuidorInicial, h.claveMovimiento, 1 AS valido, '' AS error, ".
                     "u.simboloUnidad, u.color, u.folioRepuve, h.*, t.descripcion AS descTarifa, d2.idPlaza, ".
                     "d2.estatus AS estatusDistribuidor, d2.direccionEntrega, sm.marca, sm.descripcion AS nombreSimbolo, ".
                     "(SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin ".
                     "AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 ".
                     "WHERE ud2.vin=ud.vin)) AS claveMovDetenidas, ".
                     "(SELECT g2.nombre FROM caGeneralesTbl g2 WHERE g2.tabla = 'alUnidadesDetenidasTbl' ".
                     "AND g2.columna = 'claveMovimiento' AND g2.valor = ".
                     "(SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin ".
                     "AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 ".
                     "WHERE ud2.vin=ud.vin))) AS nombreClaveMovDetenidas, ".
                     "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h.claveMovimiento ".
                     "AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreClaveMov, ".
                     "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d ".
                     "WHERE d.distribuidorCentro=h.distribuidor) AS nombreDistribuidor, ".
                     "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE ".
                     "d.distribuidorCentro=h.centroDistribucion) AS nombreCentroDist, ".
                     "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE ".
                     "d.distribuidorCentro=h.localizacionUnidad) AS descLocalizacion,".
                     "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = sm.marca) as descMarca, ".
                     "(SELECT co.descripcion FROM caColorUnidadesTbl co WHERE co.marca = sm.marca ".
                        "AND co.color = u.color) AS nombreColor, ".
                     "(SELECT 1 FROM alUnidadesDetenidasTbl de WHERE de.vin = u.vin AND de.claveMovimiento = 'UD') AS detenida ".                             
                     "FROM alUnidadesTbl u, alUltimoDetalleTbl h, caTarifasTbl t, caSimbolosUnidadesTbl sm, caDistribuidoresCentrosTbl d2 ".
		        				 "WHERE u.vin = h.vin ".
                      "AND h.idTarifa = t.idTarifa ".
                      "AND sm.simboloUnidad = u.simboloUnidad ".
                      "AND u.distribuidor = d2.distribuidorCentro ".
                      "AND h.claveMovimiento NOT IN ('CA') ".
                      "AND u.vin NOT IN  (SELECT tmp.vin from alUnidadesTmp tmp) ".
                      "AND u.vin = '".$vin."'";	                                           
			        //echo "$sql<br>";
			        $rs = fn_ejecuta_query($sql);
			        if($rs['records'] == 0)
			        {
			        		$sql = "SELECT u.avanzada, u.distribuidor AS distribuidorInicial, u.simboloUnidad,  ".
			        		       "(SELECT h.claveMovimiento FROM alUltimoDetalleTbl h WHERE h.vin = u.vin) AS claveMovimiento, ".
			        		       "(SELECT t.descripcion FROM caTarifasTbl t WHERE h.vin = u.vin) AS descTarifa, ".
			        		       "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d ".
                     		 " WHERE d.distribuidorCentro = u.distribuidor) AS nombreDistribuidor, ".
                     		 "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor = h.claveMovimiento ".
                     		 "AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreClaveMov, sm.descripcion AS nombreSimbolo ".                     		                      		 
			        		       " FROM alUnidadesTbl u, caSimbolosUnidadesTbl sm WHERE u.vin = '".$vin."'";
					        //echo $sql;
					        $rs = fn_ejecuta_query($sql);

			        		$a['unidades']['root'][$k]['avanzada'] 						= $rs['root'][0]['avanzada'];
			        		$a['unidades']['root'][$k]['distribuidorInicial'] = $rs['root'][0]['distribuidorInicial'];
			        		$a['unidades']['root'][$k]['claveMovimiento'] 		= $rs['root'][0]['claveMovimiento'];
			        		$a['unidades']['root'][$k]['simboloUnidad'] 			= $rs['root'][0]['simboloUnidad'];		        		
			        		$a['unidades']['root'][$k]['descTarifa'] 					= $rs['root'][0]['descTarifa'];
			        		$a['unidades']['root'][$k]['nombreSimbolo'] 			= $rs['root'][0]['nombreSimbolo'];			        		
			        		$a['unidades']['root'][$k]['valido'] 							= 0;
			        		$a['unidades']['root'][$k]['error'] 							= 'Unidad no disponible.';				        		
			        		$a['unidades']['root'][$k]['descTarifa'] 					= $rs['root'][0]['idTarifa']." - ".$rs['root'][0]['descTarifa'];
			        		$a['unidades']['root'][$k]['descDist'] 						= $rs['root'][0]['distribuidor']." - ".$rs['root'][0]['nombreDistribuidor'];
			        		$a['unidades']['root'][$k]['descSimbolo'] 				= $rs['root'][0]['simboloUnidad']." - ".$rs['root'][0]['nombreSimbolo'];
			        		$a['unidades']['root'][$k]['descClaveMov'] 				= $rs['root'][0]['claveMovimiento']." - ".$rs['root'][0]['nombreClaveMov'];		
			        		$a['unidades']['root'][$k]['distribuidor'] 				= $rs['root'][0]['distribuidor'];
					        if($rs['records'] == 0)
					        {
			        				$a['unidades']['root'][$k]['descSimbolo'] 		= 'NO EXISTE LA UNIDAD EN EL CATALOGO.';		
			        				$a['unidades']['root'][$k]['descTarifa'] 			= 'NO EXISTE.';
			        				$a['unidades']['root'][$k]['descDist'] 				= 'NO EXISTE.';
			        				$a['unidades']['root'][$k]['descClaveMov'] 		= 'NO EXISTE.';
					        }	
					        $error++;		        			        		
			        }
			        else
			        {			        		
			        		$a['unidades']['root'][$k]['avanzada'] 						= $rs['root'][0]['avanzada'];
			        		$a['unidades']['root'][$k]['distribuidorInicial'] = $rs['root'][0]['distribuidorInicial'];
			        		$a['unidades']['root'][$k]['claveMovimiento'] 		= $rs['root'][0]['claveMovimiento'];
			        		$a['unidades']['root'][$k]['simboloUnidad'] 			= $rs['root'][0]['simboloUnidad'];
			        		$a['unidades']['root'][$k]['idTarifa'] 						= $rs['root'][0]['idTarifa'];
			        		$a['unidades']['root'][$k]['descTarifa'] 					= $rs['root'][0]['descTarifa'];
			        		$a['unidades']['root'][$k]['nombreSimbolo'] 			= $rs['root'][0]['nombreSimbolo'];
			        		$a['unidades']['root'][$k]['valido'] 							= (int)$rs['root'][0]['valido'];
			        		$a['unidades']['root'][$k]['error'] 							= $rs['root'][0]['error'];			        				        		
			            $a['unidades']['root'][$k]['descDist'] 						= $rs['root'][0]['distribuidor']." - ".$rs['root'][0]['nombreDistribuidor'];			            
			            $a['unidades']['root'][$k]['descTarifa'] 					= $rs['root'][0]['idTarifa']." - ".$rs['root'][0]['descTarifa'];
			            $a['unidades']['root'][$k]['descSimbolo'] 				= $rs['root'][0]['simboloUnidad']." - ".$rs['root'][0]['nombreSimbolo'];			            
			            $a['unidades']['root'][$k]['descClaveMov'] 				= $rs['root'][0]['claveMovimiento']." - ".$rs['root'][0]['nombreClaveMov'];
			            //$a['unidades']['root'][$k]['descCentroDist'] 			= $rs['root'][0]['centroDistribucion']." - ".$rs['root'][0]['nombreCentroDist'];			        		
			            //$a['unidades']['root'][$k]['descColor'] 					= $rs['root'][0]['color']." - ".$rs['root'][0]['nombreColor'];
			            //$a['unidades']['root'][$k]['marcaDesc'] 					= $rs['root'][0]['marca']." - ".$rs['root'][0]['descMarca'];
			            $a['unidades']['root'][$k]['idPlaza'] 						= $rs['root'][0]['idPlaza'];
			            $a['unidades']['root'][$k]['centroDistribucion'] 	= $rs['root'][0]['centroDistribucion'];
			          	$a['unidades']['root'][$k]['localizacionUnidad'] 	= $rs['root'][0]['localizacionUnidad'];
			            $a['unidades']['root'][$k]['claveChofer'] 				= $rs['root'][0]['claveChofer'];
			            $a['unidades']['root'][$k]['observaciones'] 			= $rs['root'][0]['observaciones'];
			            $a['unidades']['root'][$k]['fechaEvento'] 				= $rs['root'][0]['fechaEvento'];
			            $a['unidades']['root'][$k]['distribuidor'] 				= $rs['root'][0]['distribuidor'];
			        		if($a['unidades']['root'][$k]['claveMovimiento'] != 'PR')		//UNIDAD RECIBIDA
			        		{
			        				$a['unidades']['root'][$k]['valido'] = 0;
			        				$a['unidades']['root'][$k]['error']  = 'Estatus actual inv&aacutelido.';
											$error++;		        			        					        				
			        		}
			        }
			        $k++;
					}	
					for($i=0; $i<$j; $i++)
					{
							$arrValidos[$i]['valido'] = $a['unidades']['root'][$i]['valido'];
							$arrMovs[$i]['claveMovimiento'] = $a['unidades']['root'][$i]['claveMovimiento'];
					}
					//var_dump($arrValidos);
					array_multisort($arrValidos,SORT_DESC,$arrMovs,SORT_ASC,$a['unidades']['root']);
					if($error > 0)
					{
							$a['msjResponse'] .= "Existen unidades inv&aacutelidas.";
					}
		  }
		  else
		  {
		      $a['success'] = false;
		      $a['msjResponse'] = "<b>No existen unidades. Favor de revisar el archivo de excel.</b>";		  		
		  }
		  $a['unidades']['records'] = $j;
		  //var_dump($a['unidades']['root']);	
		  
			copy($nombreArch, $rutaRespal.'verificacion'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
			unlink($nombreArch);
		
			return $a;		
}

function addUnidadesMasivo()
{
    $a = array();
    $a['success'] = true;
    $a['successMessage'] = '';

    include_once "alUnidades.php";
  
    $arrUnidades = json_decode($_POST['arrUnidades'],true);						//CHK
    //$arrUnidades = json_decode($_GET['arrUnidades'],true);						//CHK
    //var_dump($arrUnidades);
    foreach($arrUnidades as $index => $row)
		{
        //var_dump($row);
        $row['observaciones'] = ' ';
        $data = addHistoricoUnidad($row['centroDistribucion'],$row['vin'],'SL', $row['distribuidor'],$row['idTarifa'],$row['localizacionUnidad'],$row['claveChofer'],$row['observaciones'],$_SESSION['idUsuario'], $nInt*2);

        if ($data['success'] == false) {
            $a['success'] = false;
            $a['errorMessage'] = $data['errorMessage'];
        } else {
            $a['successMessage'] = getHistoricoUnidadMasivoMsg();
        }
    } 

    $a['errors'] = $e;
    $a['successTitle'] = getMsgTitulo();

	  echo json_encode($a);
}
?>