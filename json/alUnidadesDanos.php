	<?php

		session_start();
		require_once("../funciones/generales.php");
		require_once("../funciones/construct.php");
		require_once("../funciones/utilidades.php");

		switch($_REQUEST['alUnidadesDanosActionHdn']){

		    case 'getUnidadesAvanzada':
		    	getUnidadesAvanzada();		
		    break;
		    case 'delTmpDan':
		    	delTmpDan();		
		    break;
		    case 'getDanosUnidad':
		    	getDanosUnidad();		
		    break;
		    case 'getDatosUnidad':
		    	getDanosUnidad();		
		    break;
     	    case 'addDanosTmp':
		    	addDanosTmp();		
		    break;
		    case 'tipoDeslindeCmb':
		    	tipoDeslindeCmb();		
		    break;
		    case 'areaDanoCmb':
		    	areaDanoCmb();		
		    break;
		    case 'tipoDanoCmb':
		    	tipoDanoCmb();		
		    break;
		    case 'severidadDanoCmb':
		    	severidadDanoCmb();		
		    break;
     	    case 'validaDeslindeHistorico':
		    	validaDeslindeHistorico();		
		    break;
		    case 'addDanos':
		    	addDanos();		
		    break;
		    case 'delDanos':
		    	delDanos();		
		    break;
		    case 'validaUD':
		    	validaUD();		
		    break;
		    default:
		        echo '';
		}

		function getUnidadesAvanzada(){
        $sqlDatosUnidad = " SELECT au.vin,
							CONCAT(cs.simboloUnidad,'-',cs.descripcion) as descSimbolo,
							CONCAT(cs.clasificacion,'-',cl.descripcion) as descFamilia,
							CONCAT(cs.marca,'-',cm.descripcion) as descMarca,
							CONCAT(dc.distribuidorCentro,'-',dc.descripcionCentro) as descDistribuidor,
							CONCAT(au.color,'-',cu.descripcion) as descColor
							FROM alunidadestbl au, casimbolosunidadestbl cs, caclasificacionmarcatbl cl, cadistribuidorescentrostbl dc, camarcasunidadestbl cm,cacolorunidadestbl cu
							WHERE au.avanzada ='".$_REQUEST['alUnidadesAvanzadaHdn']."' 
							and au.simboloUnidad=cs.simboloUnidad
							and cs.marca=cm.marca
							and cs.clasificacion=cl.clasificacion
							and cm.marca=cl.marca
							and au.color=cu.color
							and cs.marca=cu.marca
							and au.distribuidor=dc.distribuidorCentro ";

       $rssqlDatosUnidad= fn_ejecuta_query($sqlDatosUnidad);

		echo json_encode($rssqlDatosUnidad);  				
		}

		  function delTmpDan(){

        $deleteDanosTmp = "DELETE FROM aldanosUnidadesTmp WHERE vin = '".$_REQUEST['vin']."' ".
        			  	  "AND idDanoUnidad IS NOT NULL  ";

        fn_ejecuta_query($deleteDanosTmp);
  		}	

  		  function getDanosUnidad(){

        $sqlDanosUnidad = " SELECT al.centroDistribucion,al.vin,cd.areaDano, ".
						"(SELECT CONCAT(cd.areaDano,'-',ge.nombre) as a FROM cageneralestbl ge WHERE ge.tabla='caDanosTbl' AND ge.columna='areaDano' AND ".
						"ge.valor=cd.areaDano) as descAreaDano, ".
						"cd.tipoDano, ".
						"(SELECT CONCAT(cd.tipoDano,'-',ge.nombre) as t FROM cageneralestbl ge WHERE ge.tabla='caDanosTbl' AND ge.columna='tipoDano' AND ".
						"ge.valor=cd.tipoDano) as descTipoDano, ".
						"cd.severidadDano, ".
						"(SELECT CONCAT(cd.severidadDano,'-',ge.nombre) as s FROM cageneralestbl ge WHERE ge.tabla='caDanosTbl' AND ".
						"ge.columna='severidadDano' AND ge.valor=cd.severidadDano) as descSeveridadDano, ".
						"al.observacion, ".
						"(SELECT CONCAT (al.tipoDano,'-',ge.nombre) FROM cageneralestbl ge where tabla='alhistoricoUnidadesTbl' and columna='claveMovimiento' and al.tipoDano=ge.valor)as tipoDeslinde, ".
						"al.idDanoUnidad,al.idDano ".
						"FROM aldanosunidadestbl al,cadanostbl cd ".
						"WHERE al.vin ='".$_REQUEST['modUnidadVin']."' ".
						"AND al.idDano=cd.idDano ";

       $rssqlDanosUnidad= fn_ejecuta_query($sqlDanosUnidad);

       echo json_encode($rssqlDanosUnidad);  
 	 }	
 	 function addDanosTmp(){

        $addHistTmp = "INSERT INTO  aldanosUnidadesTmp (idDanoUnidad, idDano, centroDistribucion,vin,observaciones) ".
                        "SELECT idDanoUnidad, idDano, centroDistribucion,vin,observacion ".
                        "FROM aldanosUnidadesTbl ".
                        "WHERE idDanoUnidad = '".$_REQUEST['idDanoUnidad']."';";

        $rs = fn_ejecuta_query($addHistTmp);

      echo json_encode($rs);
    }

     	 function tipoDeslindeCmb(){

        $tipoDeslinde = "SELECT DISTINCT   CONCAT(valor,'-',nombre) as descDeslinde,valor ".
        			  "from cageneralestbl where nombre like '%DESLINDE%' ORDER BY valor ";

        $rs = fn_ejecuta_query($tipoDeslinde);

        echo json_encode($rs);
    }

     	 function areaDanoCmb(){

        $areaDano = "SELECT CONCAT(valor,'-',nombre) as descAreaDano, valor ".
        			 "from cageneralestbl where tabla='caDanosTbl' and columna='areaDano' ";

        $rs = fn_ejecuta_query($areaDano);

        echo json_encode($rs);
    }
         	 function tipoDanoCmb(){

        $tipoDano = "SELECT CONCAT(valor,'-',nombre) as descTipoDano, valor ".
        			"from cageneralestbl where tabla='caDanosTbl' and columna='tipoDano' ";

        $rs = fn_ejecuta_query($tipoDano);

        echo json_encode($rs);
    }
         	 function severidadDanoCmb(){

        $severidadDano = "SELECT CONCAT(valor,'-',nombre) as descSeveridadDano, valor ".
        			  "from cageneralestbl where tabla='caDanosTbl' and columna='severidadDano' ";

        $rs = fn_ejecuta_query($severidadDano);

        echo json_encode($rs);
    }


      function validaDeslindeHistorico(){

        $severidadDano = "SELECT * from alhistoricoUnidadesTbl ".
        			     "where vin='".$_REQUEST['vinHdn']."' and claveMovimiento='".$_REQUEST['claveMovimientoHdn']."' ";

        $rs = fn_ejecuta_query($severidadDano);

        echo json_encode($rs);
    }



      function addDanos(){

        $json = json_decode($_REQUEST["changes"],true);
        $ciasesval= $_REQUEST["ciasesval"] ;
        $vin =$_REQUEST["vin"];


        for ($i=0; $i < sizeof($json); $i++) { 


        $danoCaDanosTbl = "SELECT * FROM cadanostbl ".
		     "WHERE areaDano='".$json[$i]["descAreaDano"]."'  ".
		     "AND tipoDano='".$json[$i]["descTipoDano"]."' ".
		     "AND severidadDano='".$json[$i]["descSeveridadDano"]."' ";

         $rsdanoCaDanosTbl = fn_ejecuta_query($danoCaDanosTbl);

         if (sizeof($rsdanoCaDanosTbl['root']) == 0) {

         $insertCadanos= "INSERT INTO cadanostbl (areaDano,tipoDano,severidadDano) ".
         				"VALUES ('".$json[$i]["descAreaDano"]."', '".$json[$i]["descTipoDano"]."','".$json[$i]["descSeveridadDano"]."') ";

        fn_ejecuta_query($insertCadanos);

          $danoInsertado = "SELECT * FROM cadanostbl ".
		     "WHERE areaDano='".$json[$i]["descAreaDano"]."'  ".
		     "AND tipoDano='".$json[$i]["descTipoDano"]."' ".
		     "AND severidadDano='".$json[$i]["descSeveridadDano"]."' ";

         $rsdanoInsertado = fn_ejecuta_query($danoInsertado);


         $descDanoNuevo= "SELECT CONCAT((SELECT CONCAT(ge.valor,'-',ge.nombre) as a ".
    	 							   "FROM cageneralestbl ge ".
    	 							   "WHERE ge.tabla='caDanosTbl' ". 
    	 							   "AND ge.columna='areaDano' ".
    	 							   "AND ge.valor=".$json[$i]["descAreaDano"]."),'-', ".
							           "(SELECT CONCAT(ge.valor,'-',ge.nombre) as t ".
							           "FROM cageneralestbl ge ".
							           "WHERE ge.tabla='caDanosTbl' ".
							           "AND ge.columna='tipoDano' ".
							           "AND ge.valor=".$json[$i]["descTipoDano"]."),'-', ".
									   "(SELECT CONCAT(ge.valor,'-',ge.nombre) as s ".
									   "FROM cageneralestbl ge ".
									   "WHERE ge.tabla='caDanosTbl' ".
									   "AND ge.columna='severidadDano' ". 
									   "AND ge.valor=".$json[$i]["descSeveridadDano"].") ) as descripcionDano ";

			$rsdescDanoNuevo = fn_ejecuta_query($descDanoNuevo);


			$insertalDanosNvo= "INSERT INTO aldanosunidadestbl (idDano,centroDistribucion,vin,observacion,tipoDano) ".
						"VALUES ('".$rsdanoInsertado['root'][0]["idDano"]."','".$ciasesval."','".$vin."','".$rsdescDanoNuevo['root'][0]["descripcionDano"]."','".$json[$i]["tipoDeslinde"]."') ";

			fn_ejecuta_query($insertalDanosNvo);

    	 }

    	 else
    	 {

    	 	$descDano= "SELECT CONCAT((SELECT CONCAT(ge.valor,'-',ge.nombre) as a ".
    	 							   "FROM cageneralestbl ge ".
    	 							   "WHERE ge.tabla='caDanosTbl' ". 
    	 							   "AND ge.columna='areaDano' ".
    	 							   "AND ge.valor=".$json[$i]["descAreaDano"]."),'-', ".
							           "(SELECT CONCAT(ge.valor,'-',ge.nombre) as t ".
							           "FROM cageneralestbl ge ".
							           "WHERE ge.tabla='caDanosTbl' ".
							           "AND ge.columna='tipoDano' ".
							           "AND ge.valor=".$json[$i]["descTipoDano"]."),'-', ".
									   "(SELECT CONCAT(ge.valor,'-',ge.nombre) as s ".
									   "FROM cageneralestbl ge ".
									   "WHERE ge.tabla='caDanosTbl' ".
									   "AND ge.columna='severidadDano' ". 
									   "AND ge.valor=".$json[$i]["descSeveridadDano"].") ) as descripcionDano ";

			$rsdescDano = fn_ejecuta_query($descDano);


		$insertalDanos= "INSERT INTO aldanosunidadestbl (idDano,centroDistribucion,vin,observacion,tipoDano) ".
						"VALUES ('".$rsdanoCaDanosTbl['root'][0]["idDano"]."','".$ciasesval."','".$vin."','".$rsdescDano['root'][0]["descripcionDano"]."','".$json[$i]["tipoDeslinde"]."') ";

		fn_ejecuta_query($insertalDanos);

    	 }

        }
    }

  function delDanos(){

        $deleteAldanosUnidadesTbl= "DELETE FROM aldanosunidadestbl ".
                    "WHERE vin = '".$_REQUEST["vin"]."' ".
                    "AND idDanoUnidad IN (SELECT tmp.idDanoUnidad FROM aldanosUnidadesTmp tmp ".
                                        "WHERE tmp.vin = '".$_REQUEST["vin"]."' ".
                                        "AND idDanoUnidad IS NOT NULL );";

        fn_ejecuta_query($deleteAldanosUnidadesTbl);


        $deleteAldanosUnidadesTmp = "DELETE FROM aldanosUnidadesTmp ".
                    "WHERE vin = '".$_REQUEST["vin"]."' ".
                    "AND idDanoUnidad IS NOT NULL";

        fn_ejecuta_query($deleteAldanosUnidadesTmp);
}

function validaUD(){

        $sqlUD = "SELECT * FROM alultimoDetalleTbl ".
            "WHERE vin = '".$_REQUEST["vin"]."' ";

		 $rssqlUD = fn_ejecuta_query($sqlUD);


        $sqlDanos = "SELECT * FROM aldanosunidadestbl ".
            "WHERE vin = '".$_REQUEST["vin"]."' ";

		 $rssqlDanos = fn_ejecuta_query($sqlDanos);

			if ($rssqlUD['root'][0]['centroDistribucion'] == 'QRO02') {

					if($rssqlUD['root'][0]['claveMovimiento'] == 'L3' && sizeof($rssqlDanos['root']) == 0){


			        $delSalida = "DELETE  FROM alhistoricoUnidadesTbl ".
			            "WHERE vin = '".$_REQUEST["vin"]."' ". 
			            "AND claveMovimiento in ('L3','D6') ";

					 $rsdelSalida = fn_ejecuta_query($delSalida);



           $sqlSelUd ="SELECT vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
              "claveChofer, observaciones, usuario, ip ".
              "FROM alhistoricounidadestbl h1 ".
              "WHERE h1.vin = '".$_REQUEST["vin"]."' ". 
              "AND h1.fechaEvento = (SELECT MAX(hu2.fechaEvento) ".
                                    "FROM alhistoricounidadestbl hu2 ".
                                    "WHERE hu2.vin = h1.vin ".
                                    "AND hu2.claveMovimiento not in (SELECT ge.valor ". 
                                                                    "FROM cageneralestbl ge ". 
                                                                    "WHERE ge.tabla = 'alHistoricoUnidadesTbl' ".
                                                                    "AND ge.columna = 'nvalidos')) ";

                  $rssqlSelUd= fn_ejecuta_query($sqlSelUd);                                                            


                  $dltUdUnd = "SELECT *  FROM alultimodetalletbl ".
                              "WHERE vin = '".$_REQUEST["vin"]."' ";

                  $rsVin = fn_ejecuta_query($dltUdUnd);


                  if (sizeof($rsVin['root']) == 1 ) {

                            if ($rssqlSelUd['root'][0]['claveChofer'] == null)
                             {

                                 $claveChofer= "null";
                              }
                            else 
                              {
                                 $claveChofer=$rssqlSelUd['root'][0]['claveChofer'];

                              }


                  $sqlUpdUnd = "UPDATE alultimodetalletbl ".
                        "SET centroDistribucion= '".$rssqlSelUd['root'][0]['centroDistribucion']."', ".
                        "fechaEvento = '".$rssqlSelUd['root'][0]['fechaEvento']."', ".
                        "claveMovimiento = '".$rssqlSelUd['root'][0]['claveMovimiento']."', ".
                        "distribuidor = '".$rssqlSelUd['root'][0]['distribuidor']."', ".
                        "idTarifa = '".$rssqlSelUd['root'][0]['idTarifa']."', ".
                        "localizacionUnidad = '".$rssqlSelUd['root'][0]['localizacionUnidad']."', ".
                        "claveChofer =".$claveChofer.",".
                        "usuario = '".$rssqlSelUd['root'][0]['usuario']."', ".
                        "observaciones = '".$rssqlSelUd['root'][0]['observaciones']."', ".
                        "ip = '".$rssqlSelUd['root'][0]['ip']."' ".
                        "WHERE vin = '".$rssqlSelUd['root'][0]['vin']."'";

                    fn_ejecuta_query($sqlUpdUnd);

                  }

                  else

                  {

                  $addUdUnd = "INSERT INTO alultimodetalletbl(vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, ".
                              "localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                              "SELECT vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                              "claveChofer, observaciones, usuario, ip ".
                              "FROM alhistoricounidadestbl h1 ".
                              "WHERE h1.vin = '".$_REQUEST["vin"]."' ". 
                              "AND h1.fechaEvento = (SELECT MAX(hu2.fechaEvento) ".
                                                    "FROM alhistoricounidadestbl hu2 ".
                                                    "WHERE hu2.vin = h1.vin ".
                                                    "AND hu2.claveMovimiento not in (SELECT ge.valor ". 
                                                                                    "FROM cageneralestbl ge ". 
                                                                                    "WHERE ge.tabla = 'alHistoricoUnidadesTbl' ".
                                                                                    "AND ge.columna = 'nvalidos')) ";

                    fn_ejecuta_query($addUdUnd);

                  }

			    }
			}
    }

?>
