<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("catIVA.php");

    $simbolosFaltantes = array();
    $distFaltantes = array();

    $a = array("success"=>true);
    $errorUnidades = array();

	$tempFileName = $_FILES['faCargaTCOArchivoFl']['tmp_name']; 
	$originalFileName = $_FILES['faCargaTCOArchivoFl']['name'];

	if($tempFileName != ''){
		$ext = explode ('.', $originalFileName); 
		$ext = $ext [count ($ext) - 1]; 

		$fileName = str_replace ($ext, '', $originalFileName); 
	 	
	  	$newName = $_SERVER['DOCUMENT_ROOT'].'/Temp/'. $fileName . $ext;

	  	if (move_uploaded_file($tempFileName, $newName)){
				
			if (file_exists($newName)){

				$tcoFile = fopen($newName, 'r');
				if($tcoFile){
					$linea = 1;
					$vin = "";
					$dataArr = array();
					$valueSep = "";
					$numCol = 0;
					$mensaje = "";
					$extension = "";

					if($_REQUEST['faCargaTCOTipoRdo'] == '1'){
						$extension = "txt";
						$valueSep = "|";
						$numCol = 10;
					}
					else{
						$extension = "csv";
						$valueSep = ",";
						$numCol = 12;
					}
					
					if($extension == $ext){
						while (($registroConcat = fgets($tcoFile)) !== false) {
							if(trim($registroConcat) != ""){
								$rowArr = explode($valueSep, substr(rtrim($registroConcat), 0, -1));
								array_push($dataArr, $rowArr);

								if(sizeof($rowArr) != $numCol){
									$a['success'] = false;
									$a['errorMessage'] = "Error al leer linea ".strval($linea)." del archivo";
									break;
								}

								$linea += 1;
							}
						}
					} else {
						$a['success'] = false;
						$a['errorMessage'] = "Tipo de archivo incorrecto";
					}

					if($a['success']){
						foreach ($dataArr as $rowArr) {
							if($_REQUEST['faCargaTCOTipoRdo'] == '1')
								$vin = cargaFinancieraTCO($rowArr);
							else
								$vin = cargaPlantaTCO($rowArr);

							if($vin != null){
								array_push($errorUnidades, $vin);
								$a['success'] = false;
							}
						}
					}

				} else {
					$a['success'] = false;
					$a['errorMessage'] = "Error al abrir el archivo " . $newName;
				}
			} else {
				$a['success'] = false;
				$a['errorMessage'] = "Error al procesar el archivo " . $newName;
			}
	 	} else { 
			$a['success'] = false;
			$a['errorMessage'] = "No se pudo mover el archivo " . $tempFileName. '</br> a '.$newName;
		}
	}

	//Distribuidores Faltantes
	if(sizeof($distFaltantes) > 0){
		$mensaje .= "</br></br>Distribuidores No Existentes:</br>";

		foreach ($distFaltantes as $distribuidor) {
			$mensaje .= $distribuidor."</br>";
		}
	}

	//Simbolos Faltantes
	if(sizeof($simbolosFaltantes) > 0){
		$mensaje .= "</br></br>Simbolos No Existentes:</br>";

		foreach ($simbolosFaltantes as $simbolo) {
			$mensaje .= $simbolo."</br>";
		}
	}

	if($a['success'])
		$a['successMessage'] = "Carga de Facturacion TCO Realizada Correctamente".$mensaje;
	else {
		if(sizeof($errorUnidades) > 0){
			$a['errorMessage'] = "Unidades con error:".$mensaje;
		}
	}

	echo json_encode($a);

	function cargaFinancieraTCO($rowArr){
		if(checkDistSimbolo($rowArr[0], $rowArr[2])){
			$compania = "TCO";
			$flotilla = "N";
			$importeSeguro = "";
			$costoUnidad = "";
			$cuotaAsociacion = "";
			$cuotaTraslado = "";

			//COMPANIA
			switch (substr($rowArr[0], 0, 1)) {
				case 'J':
					$compania = 'MCO';
					break;
				case 'Q':
					$compania = 'QCO';
					break;
				default:
					break;
			}

			//FECHA
			$fecha = DateTime::createFromFormat('ymd',$rowArr[4]);

			//FLOTILLA
			if(trim($rowArr[8]) != ""){
				$flotilla = $rowArr[8];
			}


			//Revisar si existe
			$insertarNuevoRegistro = checkUnidad($compania, $rowArr[0], $rowArr[1], $fecha);

			if($insertarNuevoRegistro){
				$importeSeguro = strval(intval(substr($rowArr[5], 0, -2))).".".substr($rowArr[5], -2);
				$costoUnidad = strval(intval(substr($rowArr[3], 0, -2))).".".substr($rowArr[3], -2);
				$cuotaAsociacion = strval(intval(substr($rowArr[6], 0, -2))).".".substr($rowArr[6], -2);
				$cuotaTraslado = strval(intval(substr($rowArr[7], 0, -2))).".".substr($rowArr[7], -2);

				$sqlAddCargaTCOStr = "INSERT INTO faCargaTCOTbl (compania, distribuidor, simboloUnidad, vin, fechaFacturacion, importeSeguro,".
										"estatusChequeBonificacion, estatusNotaBonificacion, tarifaBonificacion, costoUnidad, cuotaAsociacion, ".
										"cuotaTraslado, chequeBonificacion, notaBonificacion, flotilla) VALUES (".
										"'".$compania."',".
										"'".$rowArr[0]."',".
										"'".$rowArr[2]."',".
										"'".$rowArr[1]."',".
										"'".$fecha->format('Y-m-d')."',".
										$importeSeguro.",".
										"'D',".
										"'D',".
										"(SELECT importeBonificacion FROM caSimbolosUnidadesTbl WHERE simboloUnidad = '".$rowArr[2]."'),".
										$costoUnidad.",".
										$cuotaAsociacion.",".
										$cuotaTraslado.",".
										"NULL,".
										"NULL,".
										"'".$flotilla."')";

				fn_ejecuta_query($sqlAddCargaTCOStr);

				if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					return null;
				} else {
					echo "ERROR: ".$_SESSION['error_sql'];
					return $rowArr[1];
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	function cargaPlantaTCO($rowArr){
		$compania = 'TST';
		$distribuidor = substr($rowArr[5], 2);
		$constante = 0.0062603;

		if(checkDistSimbolo($distribuidor, $rowArr[1])){
			if(!in_array(array('01M8','01M6'), substr($rowArr[5], 0, 3))){
				$flotilla = "";

				if((intval($rowArr[3]) >= 300 && intval($rowArr[3]) < 400) || (intval($rowArr[3]) >= 500 && intval($rowArr[3]) < 600))
					$flotilla = 'N';
				else
					$flotilla = 'V';

				//FECHA
				$fecha = DateTime::createFromFormat('d/m/Y',$rowArr[2]);

				//Revisar si existe
				$insertarNuevoRegistro = checkUnidad($compania, $distribuidor, $rowArr[0], $fecha);

				if($insertarNuevoRegistro){
					$rsIVA = getIVA();
					if(isset($rsIVA['root'][0]['importe']))
						$iva = 1 + (floatval($rsIVA['root'][0]['importe']) / 100);
					else
						$iva = 1.16;

					$costoUnidad = $rowArr[11];
					$importeSeguro = floatval($costoUnidad) * $constante * $iva;
					if($rowArr[4] == 'C'){
						$importeSeguro *= -1;
					}

					$cuotaAsociacion = $rowArr[8];
					$cuotaTraslado = $rowArr[9];

					$sqlAddCargaTCOStr = "INSERT INTO faCargaTCOTbl (compania, distribuidor, vin, fechaFacturacion, importeSeguro,".
											"estatusChequeBonificacion, estatusNotaBonificacion, tarifaBonificacion, costoUnidad, cuotaAsociacion, ".
											"cuotaTraslado, chequeBonificacion, notaBonificacion, flotilla) VALUES (".
											"'".$compania."',".
											"'".$distribuidor."',".
											"'".$rowArr[0]."',".
											"'".$fecha->format('Y-m-d')."',".
											$importeSeguro.",".
											"'D',".
											"'D',".
											"(SELECT importeBonificacion FROM caSimbolosUnidadesTbl WHERE simboloUnidad = '".$rowArr[1]."'),".
											$costoUnidad.",".
											$cuotaAsociacion.",".
											$cuotaTraslado.",".
											"NULL,".
											"NULL,".
											"'".$flotilla."')";

					fn_ejecuta_query($sqlAddCargaTCOStr);

					if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
						return null;
					} else {
						return $rowArr[1];
					}
				} else {
					return null;
				}
			}
		}
	}

	function checkUnidad($compania, $distribuidor, $vin, $nuevaFecha){
		$sqlCheckUnidad = "SELECT * FROM faCargaTCOTbl ".
							"WHERE compania = '".$compania."' ".
							"AND vin = '".$vin."'";

		$rs = fn_ejecuta_query($sqlCheckUnidad);
		
		if(intval($rs['records']) > 0){
			$datos = $rs['root'][0];
			$fecha = DateTime::createFromFormat('Y-m-d', $datos['fechaFacturacion']);

			//Mismo dist y mismo mes, la unidad se da de baja
			//Mismo dist y diferente mes, la unidad se cancela

			//Diferente dist y mismo mes, se da de baja y se da de alta con el nvo dist
			//Diferente dist y diferente mes, se cancela y se da de alta con el nvo dist
			if($fecha->format('Y-m') == $nuevaFecha->format('Y-m')){

				if($fecha->format('Y-m-d') == $nuevaFecha->format('Y-m-d'))
					return false;

				//Mismo mes = baja
				$sqlBaja = "UPDATE faCargaTCOTbl SET ".
							"estatusChequeBonificacion = 'B',".
							"estatusNotaBonificacion = 'B' ".
							"WHERE compania = '".$compania."' ".
							"AND distribuidor = '".$datos['distribuidor']."' ".
							"AND vin = '".$vin."'";

				fn_ejecuta_query($sqlBaja);

			} else {
				//Diferente mes = cancelacion
				$sqlAddCargaTCOStr = "INSERT INTO faCargaTCOCanceladasTbl (compania, distribuidor, vin, fechaFacturacion, fechaCancelacion, ".
										"importeSeguro, estatusChequeBonificacion, estatusNotaBonificacion, tarifaBonificacion, costoUnidad, ".
										"cuotaAsociacion, cuotaTraslado, chequeBonificacion, notaBonificacion, flotilla) VALUES (".
										"'".$compania."',".
										"'".$datos['distribuidor']."',".
										"'".$vin."',".
										"'".$fecha->format('Y-m-d')."',".
										"'".$nuevaFecha->format('Y-m-d')."',".
										$datos['importeSeguro'].",".
										"'D',".
										"'D',".
										$datos['tarifaBonificacion'].",".
										$datos['costoUnidad'].",".
										$datos['cuotaAsociacion'].",".
										$datos['cuotaTraslado'].",".
										replaceEmptyNull($datos['chequeBonificacion']).",".
										replaceEmptyNull($datos['notaBonificacion']).",".
										"'".$datos['flotilla']."')";

				fn_ejecuta_query($sqlAddCargaTCOStr);
			}

			if($datos['distribuidor'] == $distribuidor)
				return false;
			else 
				return true;
		} else {
			return true;
		}
	}

	function checkDistSimbolo($distribuidor, $simbolo){
		global $distFaltantes;
		global $simbolosFaltantes;
		$valid = true;

		if(!in_array($distribuidor, $distFaltantes)){
			$sqlCheckDist = "SELECT 1 FROM caDistribuidoresCentrosTbl ".
							"WHERE distribuidorCentro = '".$distribuidor."'";

			$rs = fn_ejecuta_query($sqlCheckDist);

			if(sizeof($rs['root']) == 0){
				array_push($distFaltantes, $distribuidor);
				$valid = false;
			}
		} else {
			$valid = false;
		}

		if(!in_array($simbolo, $simbolosFaltantes)){
			$sqlCheckSimbolo = "SELECT 1 FROM caSimbolosUnidadesTbl ".
								"WHERE simboloUnidad = '".$simbolo."'";

			$rs = fn_ejecuta_query($sqlCheckSimbolo);

			if(sizeof($rs['root']) == 0){
				array_push($simbolosFaltantes, $simbolo);
				$valid = false;
			}
		} else {
			$valid = false;
		}

		return $valid;
	}

?>