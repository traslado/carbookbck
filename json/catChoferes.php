<?php
    $_REQUEST['zonaHoraria'] = date_default_timezone_get();
    session_start();
    $_SESSION['modulo'] = "catChoferes";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
    
    switch($_REQUEST['catChoferesActionHdn']){
        case 'getChoferes':
            getChofer();
            break;
        case 'addChofer':
            addChofer();
            break;
        case 'updChofer':
            updChofer();
            break;
        case 'dltChofer':
            dltChofer();
            break;
        case 'addDescuentosChofer':
            addDescuentosChofer();
            break;
        case 'getCatChoferes':
            getCatChoferes();
            break;
        default:
            echo '';
    }
            
    function getChofer() {
        $lsWhereStr = "";
        
        //Establece la zona horaria
				date_default_timezone_set($_REQUEST['zonaHoraria']);
		   	$fechaActual = date('Y-m-d');

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesClaveChoferTxt'], "claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesApellidoPaternoTxt'], "apellidoPaterno", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesApellidoMaternoTxt'], "apellidoMaterno", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesNombreTxt'], "nombre", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesLicenciaTxt'], "licencia", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesVigenciaLicenciaTxt'], "vigenciaLicencia", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesCuentaContableTxt'], "cuentaContable", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesTipoChoferHdn'], "tipoChofer", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }       
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesEstatusHdn'], "estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }    
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesCentroDistribucionHdn'], "centroDistribucionOrigen", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        $lsWhereStr .= (empty($lsWhereStr))?" WHERE ":" AND ";
        $lsWhereStr .= "estatus =1 ORDER BY ch.claveChofer ";

        $sqlGetChoferesStr = "SELECT ch.*, ".
                             "(SELECT dc.descripcionCentro FROM cadistribuidorescentrostbl dc ".
                                "WHERE ch.centroDistribucionOrigen = dc.distribuidorCentro) AS nombreDistOrigen, " .
                             "(SELECT g.nombre FROM cageneralestbl g WHERE g.valor=ch.estatus ".
                                "AND g.tabla='caChoferesTbl' AND g.columna='estatus') AS nombreEstatus, ".
                             "(SELECT g.nombre FROM cageneralestbl g WHERE g.valor=ch.tipoChofer ".
                                "AND g.tabla='caChoferesTbl' AND g.columna='tipoChofer') AS nombreTipo, ".
                             " '' AS msgVigLic, '' AS msgVigCer, '' AS msgVigCon ".   
                             "FROM caChoferesTbl ch " . $lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetChoferesStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $vigenciaLicencia = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaLicencia']));
            $vigenciaCertificado = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaCertificado']));
            $vigenciaContrato = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaContrato']));
            
            if($fechaActual > $vigenciaLicencia && !empty($rs['root'][$iInt]['vigenciaLicencia']) && $rs['root'][$iInt]['vigenciaLicencia'] != '0000-00-00')
            {
            		$rs['root'][$iInt]['msgVigLic'] = 'La vigencia de la licencia ya est&aacute vencida.';
            }
            if($fechaActual > $vigenciaCertificado && !empty($rs['root'][$iInt]['vigenciaCertificado']) && $rs['root'][$iInt]['vigenciaCertificado'] != '0000-00-00')
            {
            		$rs['root'][$iInt]['msgVigCer'] = 'La vigencia del certificado ya est&aacute vencida.';
            }            
            if($fechaActual > $vigenciaContrato && !empty($rs['root'][$iInt]['vigenciaContrato']) && $rs['root'][$iInt]['vigenciaContrato'] != '0000-00-00')
            {
            		$rs['root'][$iInt]['msgVigCon'] = 'La vigencia del contrato ya est&aacute vencida.';
            }            
            
            $rs['root'][$iInt]['nombreChofer'] = $rs['root'][$iInt]['claveChofer']." - ".
                                                 $rs['root'][$iInt]['nombre']." ".
                                                 $rs['root'][$iInt]['apellidoPaterno']." ".
                                                 $rs['root'][$iInt]['apellidoMaterno'];
        }
            
        echo json_encode($rs);
    }

        function getCatChoferes() {
        $lsWhereStr = "";
        
        //Establece la zona horaria
                date_default_timezone_set($_REQUEST['zonaHoraria']);
            $fechaActual = date('Y-m-d');

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesClaveChoferTxt'], "claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesApellidoPaternoTxt'], "apellidoPaterno", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesApellidoMaternoTxt'], "apellidoMaterno", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesNombreTxt'], "nombre", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesLicenciaTxt'], "licencia", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesVigenciaLicenciaTxt'], "vigenciaLicencia", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesCuentaContableTxt'], "cuentaContable", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesTipoChoferHdn'], "tipoChofer", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }          
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catChoferesCentroDistribucionHdn'], "centroDistribucionOrigen", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        
        $lsWhereStr .= " ORDER BY ch.claveChofer ";

        $sqlGetChoferesStr = "SELECT ch.*, ".
                             "(SELECT dc.descripcionCentro FROM cadistribuidorescentrostbl dc ".
                                "WHERE ch.centroDistribucionOrigen = dc.distribuidorCentro) AS nombreDistOrigen, " .
                             "(SELECT g.nombre FROM cageneralestbl g WHERE g.valor=ch.estatus ".
                                "AND g.tabla='caChoferesTbl' AND g.columna='estatus') AS nombreEstatus, ".
                             "(SELECT g.nombre FROM cageneralestbl g WHERE g.valor=ch.tipoChofer ".
                                "AND g.tabla='caChoferesTbl' AND g.columna='tipoChofer') AS nombreTipo, ".
                             " '' AS msgVigLic, '' AS msgVigCer, '' AS msgVigCon ".   
                             "FROM caChoferesTbl ch " . $lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetChoferesStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $vigenciaLicencia = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaLicencia']));
            $vigenciaCertificado = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaCertificado']));
            $vigenciaContrato = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaContrato']));
            
            if($fechaActual > $vigenciaLicencia && !empty($rs['root'][$iInt]['vigenciaLicencia']) && $rs['root'][$iInt]['vigenciaLicencia'] != '0000-00-00')
            {
                    $rs['root'][$iInt]['msgVigLic'] = 'La vigencia de la licencia ya est&aacute vencida.';
            }
            if($fechaActual > $vigenciaCertificado && !empty($rs['root'][$iInt]['vigenciaCertificado']) && $rs['root'][$iInt]['vigenciaCertificado'] != '0000-00-00')
            {
                    $rs['root'][$iInt]['msgVigCer'] = 'La vigencia del certificado ya est&aacute vencida.';
            }            
            if($fechaActual > $vigenciaContrato && !empty($rs['root'][$iInt]['vigenciaContrato']) && $rs['root'][$iInt]['vigenciaContrato'] != '0000-00-00')
            {
                    $rs['root'][$iInt]['msgVigCon'] = 'La vigencia del contrato ya est&aacute vencida.';
            }            
            
            $rs['root'][$iInt]['nombreChofer'] = $rs['root'][$iInt]['claveChofer']." - ".
                                                 $rs['root'][$iInt]['nombre']." ".
                                                 $rs['root'][$iInt]['apellidoPaterno']." ".
                                                 $rs['root'][$iInt]['apellidoMaterno'];
        }
            
        echo json_encode($rs);
    }

    function addChofer() {
        $a = array();
        $e = array();
        $a['success'] = true;


        if($_REQUEST['catChoferesClaveChoferTxt'] == ""){
            $e[] = array('id'=>'catChoferesClaveChoferTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesApellidoPaternoTxt'] == ""){
            $e[] = array('id'=>'catChoferesApellidoPaternoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesNombreTxt'] == ""){
            $e[] = array('id'=>'catChoferesNombreTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesLicenciaTxt'] == ""){
            $e[] = array('id'=>'catChoferesLicenciaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesCuentaContableTxt'] == ""){
            $e[] = array('id'=>'catChoferesCuentaContableTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesEstatusHdn'] == ""){
            $e[] = array('id'=>'catChoferesEstatusHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesTipoChoferHdn'] == ""){
            $e[] = array('id'=>'catChoferesTipoChoferHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesVigenciaLicenciaTxt'] == ""){
            $e[] = array('id'=>'catChoferesVigenciaLicenciaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesCentroDistribucionHdn'] == ""){
            $e[] = array('id'=>'catChoferesCentroDistribucionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

         if($_REQUEST['catChoferesFechaingresoTxt'] == ""){
            $e[] = array('id'=>'catChoferesFechaingresoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesCertificadoTxt'] == ""){
            $e[] = array('id'=>'catChoferesCertificadoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesVigenciaCertificadoTxt'] == ""){
            $e[] = array('id'=>'catChoferesVigenciaCertificadoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesnumContratoTxt'] == ""){
            $e[] = array('id'=>'catChoferesnumContratoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesVigenciaContratoTxt'] == ""){
            $e[] = array('id'=>'catChoferesVigenciaContratoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesCalculoistpTxt'] == ""){
            $e[] = array('id'=>'catChoferesCalculoistpTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if($a['success'] == true){
            $sqlAddChoferStr = "INSERT INTO caChoferesTbl (claveChofer,apellidoPaterno,apellidoMaterno,nombre,fechaIngreso,licencia,certificado,vigenciaCertificado,numeroContrato,vigenciaContrato,calculoistp,cuentaContable,estatus,tipoChofer,vigenciaLicencia,centroDistribucionOrigen,ultimaUnidad,correoElectronico) ".
                               "VALUES(".$_REQUEST['catChoferesClaveChoferTxt'].", ".
                               "'".$_REQUEST['catChoferesApellidoPaternoTxt']."', ".
                               "'".$_REQUEST['catChoferesApellidoMaternoTxt']."', ".
                               "'".$_REQUEST['catChoferesNombreTxt']."', ".
                               "'".$_REQUEST['catChoferesFechaingresoTxt']."', ".
                               "'".$_REQUEST['catChoferesLicenciaTxt']."',".
                               "'".$_REQUEST['catChoferesCertificadoTxt']."',".
                               "'".$_REQUEST['catChoferesVigenciaCertificadoTxt']."',".
                               "'".$_REQUEST['catChoferesnumContratoTxt']."',".
                               "'".$_REQUEST['catChoferesVigenciaContratoTxt']."',".
                               "'".$_REQUEST['catChoferesCalculoistpTxt']."',".
                               "'".$_REQUEST['catChoferesCuentaContableTxt']."',".
                               "'".$_REQUEST['catChoferesEstatusHdn']."',".
                               "'".$_REQUEST['catChoferesTipoChoferHdn']."', ".
                               "'".$_REQUEST['catChoferesVigenciaLicenciaTxt']."',".
                               "'".$_REQUEST['catChoferesCentroDistribucionHdn']."',".
                               "null, ".
                               "'correo@tracomex.com.mx')";

            $rs = fn_ejecuta_Add($sqlAddChoferStr);
            
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlAddChoferStr;
                $a['successMessage'] = getChoferesSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddChoferStr;

                $errorNoArr = explode(":", $_SESSION['error_sql']);
                if($errorNoArr[0] == '1062'){
                    $e[] = array('id'=>'duplicate','msg'=>getChoferesDuplicateMsg());   
                }
            }   
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function updChofer() {
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catChoferesClaveChoferTxt'] == ""){
            $e[] = array('id'=>'catChoferesClaveChoferTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesApellidoPaternoTxt'] == ""){
            $e[] = array('id'=>'catChoferesApellidoPaternoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesNombreTxt'] == ""){
            $e[] = array('id'=>'catChoferesNombreTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesLicenciaTxt'] == ""){
            $e[] = array('id'=>'catChoferesLicenciaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesCuentaContableTxt'] == ""){
            $e[] = array('id'=>'catChoferesCuentaContableTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesEstatusHdn'] == ""){
            $e[] = array('id'=>'catChoferesEstatusHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesTipoChoferHdn'] == ""){
            $e[] = array('id'=>'catChoferesTipoChoferHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesVigenciaLicenciaTxt'] == ""){
            $e[] = array('id'=>'catChoferesVigenciaLicenciaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catChoferesCentroDistribucionHdn'] == ""){
            $e[] = array('id'=>'catChoferesCentroDistribucionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success'] == true) {
            
            $sqlUpdChoferStr = "UPDATE caChoferesTbl ".
                                "SET apellidoPaterno= '".$_REQUEST['catChoferesApellidoPaternoTxt']."', ".
                                "apellidoMaterno= '".$_REQUEST['catChoferesApellidoMaternoTxt']."', ".
                                "nombre= '".$_REQUEST['catChoferesNombreTxt']."', ".
                                "curp= '".$_REQUEST['catChoferesCURPTxt']."', ".
                                "telEmpresa= '".$_REQUEST['catChoferesTelEmpresaTxt']."', ".
                                "licencia= '".$_REQUEST['catChoferesLicenciaTxt']."', ".
                                "cuentaContable= '".$_REQUEST['catChoferesCuentaContableTxt']."', ".
                                "estatus= '".$_REQUEST['catChoferesEstatusHdn']."', ".
                                "fechaIngreso= '".$_REQUEST['catChoferesFechaingresoTxt']."', ".
                                "certificado= '".$_REQUEST['catChoferesCertificadoTxt']."', ".
                                "vigenciaCertificado='".$_REQUEST['catChoferesVigenciaCertificadoTxt']."', ".
                                "numeroContrato= '".$_REQUEST['catChoferesnumContratoTxt']."', ".
                                "vigenciaContrato= '".$_REQUEST['catChoferesVigenciaContratoTxt']."', ".
                                "calculoistp= '".$_REQUEST['catChoferesCalculoistpTxt']."', ".
                                "vigenciaLicencia= '".$_REQUEST['catChoferesVigenciaLicenciaTxt']."', ".
                                "centroDistribucionOrigen= '".$_REQUEST['catChoferesCentroDistribucionHdn']."', ".
                                "cuentaContable= '".$_REQUEST['catChoferesCuentaContableTxt']."', ".
                                "clabeInterbancaria= '".$_REQUEST['catChoferesClabeTxt']."', ".
                                "cuentaBancaria= '".$_REQUEST['catChoferesCuentaBancariaTxt']."', ".
                                "rfc= '".$_REQUEST['catChoferesRFCTxt']."' ".
                                "WHERE claveChofer=".$_REQUEST['catChoferesClaveChoferTxt'];
        
            $rs = fn_ejecuta_Upd($sqlUpdChoferStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlUpdChoferStr;
                    $a['successMessage'] = getChoferesUpdateMsg();
                    $a['id'] = $_REQUEST['catChoferesClaveChoferTxt'];
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdChoferStr;

                $errorNoArr = explode(":", $_SESSION['error_sql']);
                if($errorNoArr[0] == '1062'){
                    $e[] = array('id'=>'catChoferesClaveChoferTxt','msg'=>getChoferesDuplicateMsg());  
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function dltChofer(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $sqlDeleteChoferStr = "DELETE FROM caChoferesTbl WHERE claveChofer=".$_REQUEST['catChoferesClaveChoferTxt'];
        
        $rs = fn_ejecuta_query($sqlDeleteChoferStr);

        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
            $a['sql'] = $sqlDeleteChoferStr;
            $a['successMessage'] = getChoferesDeleteMsg();
            $a['id'] = $_REQUEST['catChoferesClaveChoferTxt'];
        } else {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDeleteChoferStr;
        }
        
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function addDescuentosChofer(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['descuentosChoferClaveChoferHdn'] == ""){
            $e[] = array('id'=>'descuentosChoferClaveChoferHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['descuentosChoferCentroOrigenHdn'] == ""){
            $e[] = array('id'=>'descuentosChoferCentroOrigenHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $conceptoArr = explode('|', substr($_REQUEST['descuentosChoferConceptoHdn'], 0, -1));
        $pagoCobroArr = explode('|', substr($_REQUEST['descuentosChoferPagoCobroHdn'], 0, -1));
        $tipoImporteArr = explode('|', substr($_REQUEST['descuentosChoferTipoImporteHdn'], 0, -1));
        $importeArr = explode('|', substr($_REQUEST['descuentosChoferImporteHdn'], 0, -1));
        $saldoArr = explode('|', substr($_REQUEST['descuentosChoferSaldoHdn'], 0, -1));

        if($a['success']){
            $sqlCleanDescuentosChofer = "DELETE FROM roConceptosDescuentosChoferTbl ".
                                        "WHERE claveChofer = '".$_REQUEST['descuentosChoferClaveChoferHdn']."'";

            fn_ejecuta_query($sqlCleanDescuentosChofer);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $fechaEvento = date("Y-m-d");

                $sqlInsertDescuentos = "INSERT INTO roConceptosDescuentosChoferTbl (claveChofer, concepto, centroDistribucion,".
                                        "secuencia, fechaEvento, pagadoCobro, tipoImporte, importe, saldo, estatus) VALUES ";
                $inserts = "";

                for ($i=0; $i < sizeof($conceptoArr); $i++) {
                    if($conceptoArr[$i] != ''){
                        if($i > 0){
                            $inserts .= ",";
                        }

                        $inserts .= "(".$_REQUEST['descuentosChoferClaveChoferHdn'].",".
                                                $conceptoArr[$i].",".
                                                "'".$_REQUEST['descuentosChoferCentroOrigenHdn']."',".
                                                strval($i+1).",".
                                                "'".$fechaEvento."',".
                                                "'".$pagoCobroArr[$i]."',".
                                                "'".$tipoImporteArr[$i]."',".
                                                $importeArr[$i].",".
                                                $saldoArr[$i].",".
                                                "'P')";
                    }
                }

                if($inserts != "")
                    fn_ejecuta_Add($sqlInsertDescuentos.$inserts);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['successMessage'] = getChoferesDescuentosSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlInsertDescuentos;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCleanDescuentosChofer;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }
?>