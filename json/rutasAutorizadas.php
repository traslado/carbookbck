<?php
    session_start();

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");   

    //$_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['rutasAutorizadasHdn']){
        case 'comboDestino':
            comboDestino();
            break;
        case 'obtenerRutas':
            obtenerRutas();
            break;
        case 'guardaRuta':
            guardaRuta();
            break;
        default:
            echo '';
    }

    function comboDestino(){

        $sqlDestino =   "SELECT distinct destino , destino as destinoCmb
                                FROM trrutasautorizadastbl order by destino asc";

        $rs = fn_ejecuta_query($sqlDestino);

        echo json_encode($rs);
    }

    function obtenerRutas(){

        switch($_REQUEST['origen']){
            case 'CDTOL':
                $origen='CD TOLUCA';
                break;
            case 'CDLZC':
                $origen='CD LAZARO';
                break;            
            case 'CDSAL':
                $origen='CD SALTILLO';
                break;
            case 'CDSFE':
                $origen='CD SANTA FE';
                break;
            case 'CDVER':
                $origen='CD VERACRUZ';
                break;
            case 'CDSLP':
                $origen='CD SAN LUIS POTOSI';
                break;
            case 'CDMTY':
                $origen='CD MONTERREY';
                break;
            case 'CDAGS':
                $origen='CD AGUASCALIENTES';
                break;
            case 'CDTUX':
                $origen='TUXPAN';
                break;    
            default:
                echo '';
        }    

        $sqlRutasDestino =   "SELECT * FROM trrutasautorizadastbl
                            WHERE origen='".$origen."'
                            AND destino='".$_REQUEST['destino']."'";

        $rs = fn_ejecuta_query($sqlRutasDestino);

        echo json_encode($rs);
    }

    function guardaRuta(){

        switch($_REQUEST['origen']){
            case 'CDTOL':
                $origen='CD TOLUCA';
                break;
            case 'CDLZC':
                $origen='CD LAZARO';
                break;            
            case 'CDSAL':
                $origen='CD SALTILLO';
                break;
            case 'CDSFE':
                $origen='CD SANTA FE';
                break;
            case 'CDVER':
                $origen='CD VERACRUZ';
                break;
            case 'CDSLP':
                $origen='CD SAN LUIS POTOSI';
                break;
            case 'CDMTY':
                $origen='CD MONTERREY';
                break;
            case 'CDAGS':
                $origen='CD AGUASCALIENTES';
                break;
            case 'CDTUX':
                $origen='TUXPAN';
                break;  
            default:
                echo '';
        }    


        $sqlRutasDestino =   "SELECT * FROM trrutasautorizadastbl
                            WHERE origen='".$origen."'
                            AND destino='".$_REQUEST['destino']."'";

        $rs = fn_ejecuta_query($sqlRutasDestino);
        echo sizeof($rs['root']);

        if (sizeof($rs['root'])>=0) {
            $insRuta="INSERT INTO trrutasautorizadastbl (origen, ruta1, ruta2, ruta3, ruta4, ruta5, ruta6, ruta7, ruta8, ruta9, ruta10, ruta11, ruta12, ruta13, ruta14, ruta15, ruta16, ruta17, ruta18, ruta19, ruta20, ruta21, ruta22, ruta23, ruta24, ruta25, destino, kms, dieselOrigen, dieselDestino) VALUES ('".$origen."', '".$_REQUEST['ruta1'].
                "', '".$_REQUEST['ruta2'].
                "', '".$_REQUEST['ruta3'].
                "', '".$_REQUEST['ruta4'].
                "', '".$_REQUEST['ruta5'].
                "', '".$_REQUEST['ruta6'].
                "', '".$_REQUEST['ruta7'].
                "', '".$_REQUEST['ruta8'].
                "', '".$_REQUEST['ruta9'].
                "', '".$_REQUEST['ruta10'].
                "', '".$_REQUEST['ruta11'].
                "', '".$_REQUEST['ruta12'].
                "', '".$_REQUEST['ruta13'].
                "', '".$_REQUEST['ruta14'].
                "', '".$_REQUEST['ruta15'].
                "', '".$_REQUEST['ruta16'].
                "', '".$_REQUEST['ruta17'].
                "', '".$_REQUEST['ruta18'].
                "', '".$_REQUEST['ruta19'].
                "', '".$_REQUEST['ruta20'].
                "', '".$_REQUEST['ruta21'].
                "', '".$_REQUEST['ruta22'].
                "', '".$_REQUEST['ruta23'].
                "', '".$_REQUEST['ruta24'].
                "', '".$_REQUEST['ruta25'].
                "', '".$_REQUEST['destino'].
                "', '".$_REQUEST['kilometros'].
                "', '".$_REQUEST['dieselOrigen'].
                "', '".$_REQUEST['dieselDestino']."');";
                fn_ejecuta_query($insRuta);

        }else{
            $updateRuta ="UPDATE trrutasautorizadastbl 
                SET 
                    ruta1 = '".$_REQUEST['ruta1']."',
                    ruta2 = '".$_REQUEST['ruta2']."',
                    ruta3 = '".$_REQUEST['ruta3']."',
                    ruta4 = '".$_REQUEST['ruta4']."',
                    ruta5 = '".$_REQUEST['ruta5']."',
                    ruta6 = '".$_REQUEST['ruta6']."',
                    ruta7 = '".$_REQUEST['ruta7']."',
                    ruta8 = '".$_REQUEST['ruta8']."',
                    ruta9 = '".$_REQUEST['ruta9']."',
                    ruta10 = '".$_REQUEST['ruta10']."',
                    ruta11 = '".$_REQUEST['ruta11']."',
                    ruta12 = '".$_REQUEST['ruta12']."',
                    ruta13 = '".$_REQUEST['ruta13']."',
                    ruta14 = '".$_REQUEST['ruta14']."',
                    ruta15 = '".$_REQUEST['ruta15']."',
                    ruta16 = '".$_REQUEST['ruta16']."',
                    ruta17 = '".$_REQUEST['ruta17']."',
                    ruta18 = '".$_REQUEST['ruta18']."',
                    ruta19 = '".$_REQUEST['ruta19']."',
                    ruta20 = '".$_REQUEST['ruta20']."',
                    ruta21 = '".$_REQUEST['ruta21']."',
                    ruta22 = '".$_REQUEST['ruta22']."',
                    ruta23 = '".$_REQUEST['ruta23']."',
                    ruta24 = '".$_REQUEST['ruta24']."',
                    kms = '".$_REQUEST['kilometros']."',
                    dieselOrigen = '".$_REQUEST['dieselOrigen']."',
                    dieselDestino = '".$_REQUEST['dieselDestino']."'
                    WHERE origen = '".$origen."' AND destino='".$_REQUEST['destino']."'";

                fn_ejecuta_query($updateRuta);

        }        

    }

?>