
<?php
	session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");


    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['trReImpresionPoliza']) {
        case 'getDatosPoliza':
            getDatosPoliza();
           break;
        case 'reImpresioPoliza':
        	reImpresioPoliza();
        break;
        default:
            echo 'NO HACE NADA';
    }

function getDatosPoliza(){
    $selStr = "SELECT IFNULL(SUM(subtotal),0) AS subtotal, gvt.claveMovimiento ".
              "FROM trgastosviajetractortbl gvt, caGeneralesTbl gen ".
              "WHERE gvt.concepto = gen.valor ".
              "AND gen.tabla = 'comprobacionPoliza' AND gen.columna = '".$_SESSION['usuCompania']."' ".
              "AND gvt.folio = ".$_REQUEST['reImpresionPolizaFolio']." ".
              "AND SUBSTRING(gvt.fechaEvento,1,10) = '".$_REQUEST['reImpresionPolizaFecha']."' ".
              "AND gvt.centroDistribucion = '".$_SESSION['usuCompania']."'";
    $selRst = fn_ejecuta_query($selStr);
    $claveMovimiento = $selRst['root'][0]['claveMovimiento'];
    if ($claveMovimiento == 'XC') {
        $claveMovimiento = 'GC';
        $rsSqlViajePoliza['records'] = 0;
        $rsSqlViajePoliza['msjResponse'] = 'Complemento de poliza cancelada.';
        echo json_encode($rsSqlViajePoliza);
        die();
    } elseif ($claveMovimiento == 'XP') {
        $claveMovimiento = 'GP';
        $rsSqlViajePoliza['records'] = 0;
        $rsSqlViajePoliza['msjResponse'] = 'Poliza de gastos cancelada.';
        echo json_encode($rsSqlViajePoliza);
        die();
    }

	$sqlViajePoliza = "SELECT DISTINCT concat(co.compania,' - ', co.descripcion) as compania,gt.mesAfectacion,gt.folio, tr.tractor, vt.viaje, concat(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as operador, tr.rendimiento, ".
					"(SELECT pl.Plaza FROM caplazastbl pl WHERE pl.idPlaza = vt.idPlazaOrigen) as origen, ".
                    "(SELECT pl1.Plaza FROM caplazastbl pl1 WHERE pl1.idPlaza = vt.idPlazaDestino) as destino, ".
                    "vt.kilometrosTabulados, vt.numeroRepartos as distribuidores, ".
					"(SELECT count(tl1.distribuidor) FROM trtalonesviajestbl tl1 WHERE tl1.idViajeTractor = vt.idViajeTractor) as talones, ".
					"vt.numeroUnidades, cast(vt.fechaEvento as date) as fechaEvento, vt.idViajeTractor, ".
					"ifNull((SELECT sum(gt2.importe) FROM trgastosviajetractortbl gt2 WHERE gt2.idViajeTractor = vt.idViajeTractor AND gt2.folio = gt.folio AND concepto = '7012'),00.00) as ivaAcreditable, ".
					"ifNull((SELECT sum(gt2.importe) FROM trgastosviajetractortbl gt2 WHERE gt2.idViajeTractor = vt.idViajeTractor AND concepto = '7001'),00.00) as anticipos, ".
					"ifNull((SELECT sum(gt2.importe) FROM trgastosviajetractortbl gt2 WHERE gt2.idViajeTractor = vt.idViajeTractor AND gt2.folio = gt.folio AND claveMovimiento = 'GP' AND concepto NOT IN(9000, 9001,9002,9003,9004,9005,9006 ,9007,9008,9009,9010,9011,9012)),00.00) as comprobado, ".
					"ifNull((SELECT sum(gt2.importe) FROM trgastosviajetractortbl gt2 WHERE gt2.idViajeTractor = vt.idViajeTractor AND gt2.folio = gt.folio AND concepto = '7025'),00.00) as cargaOperador, ".
					"ifNull((SELECT sum(gt2.importe) FROM trgastosviajetractortbl gt2 WHERE gt2.idViajeTractor = vt.idViajeTractor AND gt2.folio = gt.folio AND concepto = '7026'),00.00) as pagoEfectivo, ".
                    "IFNULL((SELECT gen.valor FROM caGeneralesTbl gen WHERE gen.tabla = 'catChoferestbl' AND gen.columna = 'sinPolizaSueldos' AND gen.valor = vt.claveChofer AND estatus = 1),0) AS imprimeSueldos ".
					"FROM trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trgastosviajetractortbl gt, cacompaniastbl co ".
					"WHERE vt.idViajeTractor = gt.idViajeTractor ".
					"AND vt.idTractor = tr.idTractor ".
					"AND vt.claveChofer = ch.claveChofer ".
					"AND vt.claveMovimiento = 'VP' ".
					"AND tr.compania = co.compania ".
                    "AND gt.claveMovimiento = 'GP' ".
					"AND gt.folio = '".$_REQUEST['reImpresionPolizaFolio']."' ".
                    "AND gt.centroDistribucion='".$_SESSION['usuCompania']."' ".
					"AND cast(gt.fechaEvento as date) = '".$_REQUEST['reImpresionPolizaFecha']."' ".
                    "AND SUBSTRING(gt.fechaEvento,6,2) = gt.mesAfectacion";//revisar insert trgastosviajetractortbl campo mesAfectacion y quitar esta linea

	$rsSqlViajePoliza = fn_ejecuta_query($sqlViajePoliza);
    if ($rsSqlViajePoliza['records'] > 0) {
        $rsSqlViajePoliza['root'][0]['claveMovimiento'] = $claveMovimiento;
    }
    echo json_encode($rsSqlViajePoliza);
}

?>