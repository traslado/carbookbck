<?php
    if (!isset($_SESSION)) {
        session_start();
    }
    /**
    * </>: ~/06/2020 ::1:@aha
    * @category Polizas
    * @package Descuentos Personales <concepto 2228>
    * @see Prestamos Personales 'prestamosPersonalesWin'
    */
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    error_reporting(E_ALL ^ E_DEPRECATED);
    date_default_timezone_set('America/Mexico_City');
    setlocale(LC_TIME , 'es_MX.UTF-8');
    setlocale(LC_MONETARY, 'en_MX');
    $so = strtoupper(substr(PHP_OS, 0, 3));

    // while (true)
    // {
        $lb_log = false;
        /**
        ******************************************************************************
        * 2228  2228    2228    2228    2228    2228    2228    2228    2228    2228
        ******************************************************************************
        */
        $fechaActual = date('Y-m-d');
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . '    <2228> ACTUALIZANDO PRESTAMOS PERSONALES' . PHP_EOL;

        // CREA/ACTUALIZA TABULADOR DE DESCUENTO POR PERIODO
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' >>1. TABULADO DE DESCUENTO POR PERIODO' . PHP_EOL;

        $selStr = 'SELECT pr.* ' .
                  'FROM roPrestamosPersonalesTbl pr ' .
                  'WHERE pr.idEstatus = \'0\' ' .
                  "AND pr.fechaInicio BETWEEN '${fechaActual} 00:00:00' AND '${fechaActual} 23:59:59' " .
                  'ORDER BY pr.idDescuento, pr.secuencia';
        $procPrestamosRst = fn_ejecuta_sql($selStr, $lb_log);

        if ($procPrestamosRst['records'] > 0)
        {
            $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
            echo $fechaHora . '  Generando tabulador de prestamos activos ... ' . sizeof($procPrestamosRst['root']) . '' . PHP_EOL;

            foreach ((array) $procPrestamosRst['root'] as $index => $row)
            {
                $a = array('success' => true);
                $fechaInicioAux = $row['fechaInicio'];
                $mesesPago = intval($row['mesesPago']);
                $periodo = intval($row['periodo']);

                $selStr = 'SELECT * FROM roControlDescuentoPeriodoTbl ' .
                          'WHERE idDescuento = '.$row['idDescuento'].' '.
                          'ORDER BY secuencia DESC LIMIT 1';
                $tabRst = fn_ejecuta_sql($selStr, $lb_log);
                if ($tabRst['records'] == 0) {
                    // insert
                    $length = $mesesPago / $periodo;
                    $sec = 0;
                    for ($i=0; $i < $length; $i++) {
                        $fechaInicioArr = explode('-', $fechaInicioAux);
                        $fechaInicio = $fechaInicioAux.' 00:00:00';
                        if ($periodo > 1) {
                            $mes = intval($fechaInicioArr[1]);
                            $periodoAux = $periodo -1;
                            if ($mes == 12) {
                                $mes = $periodoAux;
                                $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                            } else {
                                $mesAux = $mes + $periodoAux;
                                if ($mesAux > 12) {
                                    $mes = $mesAux - 12;
                                    $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                                } else {
                                    $mes = $mesAux;
                                }
                            }
                            $fechaInicioArr[1] = $mes;
                        }
                        $diaFinal = calculaDiasMes(intval($fechaInicioArr[0]), $fechaInicioArr[1]);
                        $fechaFinal = $fechaInicioArr[0].'-'.$fechaInicioArr[1].'-'.$diaFinal.' 23:59:59';
                        $sec++;
                        $insStr = "INSERT INTO roControlDescuentoPeriodoTbl (idDescuento, secuencia, pagoPeriodoTotal, ctrlDescPeriodo, fechaInicio, fechaFinal) VALUES (".
                                  "".$row['idDescuento'].", " .
                                  "".$sec.", " .
                                  "".$row['pagoPeriodo'].", " .
                                  "".$row['pagoPeriodo'].", " .
                                  "'".$fechaInicio."', " .
                                  "'".$fechaFinal."')";
                        fn_ejecuta_sql($insStr, $lb_log);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql']!="") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql'];
                            $a['sql_error']   = $insStr;
                            break;
                        }
                        $fechaInicioStr = substr($fechaFinal, 0,10);
                        $fechaInicioAux = date("Y-m-d",strtotime($fechaInicioStr."+ 1 days"));
                    }
                } else {// update
                    $length = $mesesPago / $periodo;
                    $secAux = intval($tabRst['root'][0]['secuencia']);
                    for ($i=0; $i < $length; $i++) {
                        $fechaInicioArr = explode('-', $fechaInicioAux);
                        $fechaInicio = $fechaInicioAux.' 00:00:00';
                        if ($periodo > 1) {
                            $mes = intval($fechaInicioArr[1]);
                            $periodoAux = $periodo -1;
                            if ($mes == 12) {
                                $mes = $periodoAux;
                                $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                            } else {
                                $mesAux = $mes + $periodoAux;
                                if ($mesAux > 12) {
                                    $mes = $mesAux - 12;
                                    $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                                } else {
                                    $mes = $mesAux;
                                }
                            }
                            $fechaInicioArr[1] = $mes;
                        }
                        $diaFinal = calculaDiasMes(intval($fechaInicioArr[0]), $fechaInicioArr[1]);
                        $fechaFinal = $fechaInicioArr[0].'-'.$fechaInicioArr[1].'-'.$diaFinal.' 23:59:59';

                        $selStr = 'SELECT * FROM roControlDescuentoPeriodoTbl ' .
                                  'WHERE idDescuento = '.$row['idDescuento'].' '.
                                  "AND '${fechaInicioAux}' BETWEEN fechaInicio AND fechaFinal ".
                                  'ORDER BY secuencia DESC LIMIT 1';
                        $tabRst = fn_ejecuta_sql($selStr, $lb_log);
                        if ($tabRst['records'] > 0) {
                            $updStr = "UPDATE roControlDescuentoPeriodoTbl ".
                                      "SET pagoPeriodoTotal = pagoPeriodoTotal + ".$row['pagoPeriodo'].", ".
                                      "ctrlDescPeriodo = ctrlDescPeriodo + ".$row['pagoPeriodo']." ".
                                      "WHERE idTabulador = ".$tabRst['root'][0]['idTabulador'];
                            fn_ejecuta_sql($updStr, $lb_log);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['msjResponse'] = $_SESSION['error_sql'];
                                $a['sql_error']   = $updStr;
                                break;
                            }
                        } else {
                            $sec = ($i == 0)?$secAux:$sec;
                            $sec++;
                            $insStr = "INSERT INTO roControlDescuentoPeriodoTbl (idDescuento, secuencia, pagoPeriodoTotal, ctrlDescPeriodo, fechaInicio, fechaFinal) VALUES (".
                                      "".$row['idDescuento'].", " .
                                      "".$sec.", " .
                                      "".$row['pagoPeriodo'].", " .
                                      "".$row['pagoPeriodo'].", " .
                                      "'".$fechaInicio."', " .
                                      "'".$fechaFinal."')";
                            fn_ejecuta_sql($insStr, $lb_log);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql']!="") {
                                $a['success']     = false;
                                $a['msjResponse'] = $_SESSION['error_sql'];
                                $a['sql_error']   = $insStr;
                                break;
                            }
                        }
                        $fechaInicioStr = substr($fechaFinal, 0,10);
                        $fechaInicioAux = date("Y-m-d",strtotime($fechaInicioStr."+ 1 days"));
                    }
                }

                if ($a['success']) {
                    $updEstStr = 'UPDATE roPrestamosPersonalesTbl '.
                                 'SET idEstatus = 1 '.
                                 'WHERE idPrestamo = '.$row['idPrestamo'];
                    fn_ejecuta_sql($updEstStr, $lb_log);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql_error']   = $updEstStr;
                    } else {
                        // Actualiza porcentaje cuando inicia un nuevo prestamo
                        $selEstStr = 'SELECT * FROM roPrestamosPersonalesTbl '.
                                     'WHERE idDescuento = '.$row['idDescuento'].' '.
                                     'AND idEstatus = 1 '.
                                     'ORDER BY idPrestamo ASC';
                        $prestRst = fn_ejecuta_sql($selEstStr, $lb_log);
                        $porcentaje = 15;
                        if ($prestRst['records'] > 1) {
                            $porcentaje = 20;
                        }

                        $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                                  "SET tipoImporte = ".$porcentaje." ".
                                  "WHERE idDescuento = ".$row['idDescuento'];
                        fn_ejecuta_sql($updStr, $lb_log);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql'];
                            $a['sql_error']   = $updStr;
                        }
                    }
                }
                if ($a['success'] == false) {
                    $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
                    $fehalog = strftime("%Y-%m-%d");
                    $logFile = fopen("../log/logDescPersonales_err${fehalog}.log", "a");
                    fwrite($logFile, "/*${fechaHora}*/ ".$a['sql_error'].";\r\n");
                    fclose($logFile);
                }
                fn_ejecuta_sql($a['success'], $lb_log);
            }
        } else {
            echo $fechaHora . '  No encontre nuevos prestamos <tabular>.' . PHP_EOL;
        }
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' <<1::' . PHP_EOL;

        // Si cancelan polizas del periodo anterior o el ultimo periodo no alcanzo a cubrir el prestamo (Entonces se crea un nuevo tabulado para pagar diferencia) Solo cuando ya no tiene control periodo
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' >>2. VALIDANDO ULTIMO TABULADO' . PHP_EOL;
        $anioActual = date('Y');
        $mesActual = date('m');
        $anioProceso = intval($anioActual);
        $mesProceso = intval($mesActual) -1;
        if ($mesProceso == 0) {
            $anioProceso--;
            $mesProceso = 12;
        }
        $mesProceso = str_pad($mesProceso,2,"0",STR_PAD_LEFT);
        $diaFinalMesAnt = calculaDiasMes($anioProceso, $mesProceso);
        $fechaFinalProc = $anioProceso.'-'.$mesProceso.'-'.$diaFinalMesAnt.' 23:59:59';

        $selStr = 'SELECT cp.* ' .
                  'FROM roControlDescuentoPeriodoTbl cp, roConceptosDescuentosChoferTbl cd ' .
                  'WHERE cp.idDescuento = cd.idDescuento '.
                  'AND cd.concepto = \'2228\' '.
                  "AND cp.fechaFinal = '${fechaFinalProc}' " .
                  'ORDER BY cp.idDescuento';
        $periodoAntRst = fn_ejecuta_sql($selStr, $lb_log);
        for ($i=0; $i < $periodoAntRst['records']; $i++) {
            $a = array('success' => true);

            $sec = intval($periodoAntRst['root'][$i]['secuencia']);
            $idDescuento = $periodoAntRst['root'][$i]['idDescuento'];
            $fechaInicio = $periodoAntRst['root'][$i]['fechaInicio'];
            $fechaFinal = $periodoAntRst['root'][$i]['fechaFinal'];

            $secActual = $sec+1;
            $selStr = 'SELECT * ' .
                      'FROM roControlDescuentoPeriodoTbl ' .
                      'WHERE idDescuento = '.$idDescuento.' '.
                      'AND secuencia = '.$secActual;
            $periodosActRst = fn_ejecuta_sql($selStr, $lb_log);
            if ($periodosActRst['records'] == 0) {// Valida que no tenga mas periodos definidos
                $selStr = 'SELECT pp.idPrestamo, pp.idEstatus, pp.periodo, SUM(ph.adeudo) AS adeudoPeriodo ' .
                          'FROM roPrestamosPersonalesTbl pp, roPagosOperadorHistoricoTbl ph ' .
                          'WHERE pp.idPrestamo = ph.idPrestamo ' .
                          'AND ph.concepto = \'2228\' '.
                          'AND pp.idEstatus IN (1,2) '.
                          'AND ph.claveMovimiento = \'PA\' '.
                          "AND ph.fechaEvento BETWEEN '${fechaInicio}' AND '${fechaFinal}' " .
                          "AND pp.idDescuento = ${idDescuento}";
                $controlDescPeriodoRst = fn_ejecuta_sql($selStr, $lb_log);
                $pagoPeriodoTotal = floatval($periodoAntRst['root'][$i]['pagoPeriodoTotal']);
                $adeudoPeriodo = floatval($controlDescPeriodoRst['root'][0]['adeudoPeriodo']);
                $periodo = floatval($controlDescPeriodoRst['root'][0]['periodo']);
                $difAdeudo = $pagoPeriodoTotal - $adeudoPeriodo;

                $updStr = "UPDATE roControlDescuentoPeriodoTbl ".
                          "SET ctrlDescPeriodo = ".$difAdeudo." ".
                          "WHERE idTabulador = ".$periodoAntRst['root'][$i]['idTabulador'];
                fn_ejecuta_sql($updStr, $lb_log);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql_error']   = $updStr;
                } else {
                    $selStr = 'SELECT * FROM roControlDescuentoPeriodoTbl ' .
                              'WHERE idDescuento = '.$idDescuento.' '.
                              'ORDER BY secuencia';
                    $difAdeudosRst = fn_ejecuta_sql($selStr, $lb_log);
                    $adeuAcumulado = 0;
                    for ($idif=0; $idif < $difAdeudosRst['records']; $idif++) { 
                        $adeuAcumulado += floatval($difAdeudosRst['root'][$idif]['ctrlDescPeriodo']);
                    }
                    if ($adeuAcumulado > 0) {// Tabulado de la deuda restante
                        $fechaInicioStr = substr($fechaFinal, 0,10);
                        $fechaInicioAux = date("Y-m-d",strtotime($fechaInicioStr."+ 1 days"));
                        $fechaInicioArr = explode('-', $fechaInicioAux);
                        $fechaInicio = $fechaInicioAux.' 00:00:00';
                        if ($periodo > 1) {
                            $mes = intval($fechaInicioArr[1]);
                            $periodoAux = $periodo -1;
                            if ($mes == 12) {
                                $mes = $periodoAux;
                                $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                            } else {
                                $mesAux = $mes + $periodoAux;
                                if ($mesAux > 12) {
                                    $mes = $mesAux - 12;
                                    $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                                } else {
                                    $mes = $mesAux;
                                }
                            }
                            $fechaInicioArr[1] = $mes;
                        }
                        $diaFinal = calculaDiasMes(intval($fechaInicioArr[0]), $fechaInicioArr[1]);
                        $fechaFinal = $fechaInicioArr[0].'-'.$fechaInicioArr[1].'-'.$diaFinal.' 23:59:59';

                        $insStr = "INSERT INTO roControlDescuentoPeriodoTbl (idDescuento, secuencia, pagoPeriodoTotal, ctrlDescPeriodo, fechaInicio, fechaFinal) VALUES (".
                                  "".$idDescuento.", " .
                                  "".$secActual.", " .
                                  "".$adeuAcumulado.", " .
                                  "".$adeuAcumulado.", " .
                                  "'".$fechaInicio."', " .
                                  "'".$fechaFinal."')";
                        fn_ejecuta_sql($insStr, $lb_log);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql']!="") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql'];
                            $a['sql_error']   = $insStr;
                        }
                    }
                }
            }

            if ($a['success'] == false) {
                $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
                $fehalog = strftime("%Y-%m-%d");
                $logFile = fopen("../log/logDescPersonales_err${fehalog}.log", "a");
                fwrite($logFile, "/*${fechaHora}*/ ".$a['sql_error'].";\r\n");
                fclose($logFile);
            }

            fn_ejecuta_sql($a['success'], $lb_log);
        }
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' <<2::' . PHP_EOL;

        // ACTUALIZA TOTAL DE PAGOS Y ESTATUS EN PRESTAMOS PERSONALES, DESCUENTOS POR PERIODO Y CONCEPTOS DESCUENTOS CHOFER (PERIODO ACTUAL)
        // roconceptosdescuentoschofertbl   saldo,estatus
        // roPrestamosPersonalesTbl idEstatus
        // roControlDescuentoPeriodoTbl ctrlDescPeriodo
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' >>3. CALCULO DE TOTALES PRESTAMOS' . PHP_EOL;
        $selStr = 'SELECT cp.* ' .
                  'FROM roControlDescuentoPeriodoTbl cp, roConceptosDescuentosChoferTbl cd ' .
                  'WHERE cp.idDescuento = cd.idDescuento '.
                  'AND cd.concepto = \'2228\' '.
                  "AND '${fechaActual}' BETWEEN cp.fechaInicio AND cp.fechaFinal " .
                  'ORDER BY cp.idDescuento';
        $descuentosRst = fn_ejecuta_sql($selStr, $lb_log);

        for ($i=0; $i < $descuentosRst['records']; $i++) {
            $a = array('success' => true);

            $sec = intval($descuentosRst['root'][$i]['secuencia']);
            for ($j=0; $j < 2; $j++) { //Upd periodo actual y anterior
                $idDescuento = $descuentosRst['root'][$i]['idDescuento'];
                $selStr = 'SELECT * ' .
                          'FROM roControlDescuentoPeriodoTbl ' .
                          'WHERE idDescuento = '.$idDescuento.' '.
                          'AND secuencia = '.$sec;
                $periodosRst = fn_ejecuta_sql($selStr, $lb_log);
                $fechaInicio = $periodosRst['root'][0]['fechaInicio'];
                $fechaFinal = $periodosRst['root'][0]['fechaFinal'];

                $selStr = 'SELECT pp.idPrestamo, pp.idEstatus, SUM(ph.adeudo) AS adeudoPeriodo ' .
                          'FROM roPrestamosPersonalesTbl pp, roPagosOperadorHistoricoTbl ph ' .
                          'WHERE pp.idPrestamo = ph.idPrestamo ' .
                          'AND ph.concepto = \'2228\' '.
                          'AND pp.idEstatus IN (1,2) '.
                          'AND ph.claveMovimiento = \'PA\' '.
                          "AND ph.fechaEvento BETWEEN '${fechaInicio}' AND '${fechaFinal}' " .
                          "AND pp.idDescuento = ${idDescuento}";
                $controlDescPeriodoRst = fn_ejecuta_sql($selStr, $lb_log);
                $pagoPeriodoTotal = floatval($periodosRst['root'][0]['pagoPeriodoTotal']);
                $adeudoPeriodo = floatval($controlDescPeriodoRst['root'][0]['adeudoPeriodo']);
                $difAdeudo = $pagoPeriodoTotal - $adeudoPeriodo;

                $updStr = "UPDATE roControlDescuentoPeriodoTbl ".
                          "SET ctrlDescPeriodo = ".$difAdeudo." ".
                          "WHERE idTabulador = ".$periodosRst['root'][0]['idTabulador'];
                fn_ejecuta_sql($updStr, $lb_log);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql_error']   = $updStr;
                }

                $selStr = 'SELECT pp.idPrestamo, pp.idEstatus, pp.deudaTotal ' .
                          'FROM roPrestamosPersonalesTbl pp ' .
                          'WHERE pp.idEstatus IN (1,2) '.
                          "AND pp.idDescuento = ${idDescuento} ".
                          'ORDER BY pp.idPrestamo, pp.secuencia';
                $prestamosRst = fn_ejecuta_sql($selStr, $lb_log);
                for ($k=0; $k < $prestamosRst['records']; $k++) { 
                    $selStr = 'SELECT SUM(ph.adeudo) AS adeudoPrestamo ' .
                              'FROM roPagosOperadorHistoricoTbl ph ' .
                              'WHERE ph.idPrestamo = '.$prestamosRst['root'][$k]['idPrestamo'].' '.
                              'AND ph.concepto = \'2228\' '.
                              'AND ph.claveMovimiento = \'PA\' '.
                              'ORDER BY ph.fechaEvento';
                    $pagosRst = fn_ejecuta_sql($selStr, $lb_log);
                    $deudaTotal = floatval($prestamosRst['root'][$k]['deudaTotal']);
                    $adeudoPrestamo = floatval($pagosRst['root'][0]['adeudoPrestamo']);

                    $idEstatus = 1;
                    if (round($deudaTotal,2) == round($adeudoPrestamo,2)) {
                        $idEstatus = 2;
                    }

                    $updStr = "UPDATE roPrestamosPersonalesTbl ".
                              "SET idEstatus = ".$idEstatus." ".
                              "WHERE idPrestamo = ".$prestamosRst['root'][$k]['idPrestamo'];
                    fn_ejecuta_sql($updStr, $lb_log);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql_error']   = $updStr;
                        break;
                    }
                }

                $sec--;
                if ($sec == 0) {
                    break;
                }
                if ($a['success'] == false) {
                    break;
                }
            }
            $selStr = "SELECT pp.idDescuento, IFNULL((SELECT SUM(deudaTotal) FROM roPrestamosPersonalesTbl WHERE idEstatus IN (1,2) AND idDescuento = ${idDescuento}),0) AS deudaTotal, IFNULL(SUM(ph.adeudo),0) AS adeudoGeneral " .
                      'FROM roPrestamosPersonalesTbl pp, roPagosOperadorHistoricoTbl ph ' .
                      'WHERE pp.idPrestamo = ph.idPrestamo ' .
                      'AND ph.concepto = \'2228\' '.
                      'AND pp.idEstatus IN (1,2) '.
                      'AND ph.claveMovimiento = \'PA\' '.
                      "AND pp.idDescuento = ${idDescuento} ".
                      'ORDER BY pp.idDescuento, pp.secuencia';
            $pagosRst = fn_ejecuta_sql($selStr, $lb_log);
            $deudaTotal = floatval($pagosRst['root'][0]['deudaTotal']);
            $adeudoGeneral = floatval($pagosRst['root'][0]['adeudoGeneral']);
            if (round($deudaTotal,2) != 0.00) {
                $estatus = 'P';
                if (round($deudaTotal,2) == round($adeudoGeneral,2)) {
                    $estatus = 'L';
                }
                $saldo = $deudaTotal - $adeudoGeneral;

                $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                          "SET importe = ".$deudaTotal.", ".
                          "saldo = ".$saldo.", ".
                          "estatus = '".$estatus."' ".
                          "WHERE idDescuento = ".$idDescuento;
                fn_ejecuta_sql($updStr, $lb_log);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql_error']   = $updStr;
                }
            }

            if ($a['success']) {
                // actualiza porcentaje del prestamo
                $selStr = "SELECT a.* ".
                          "FROM roPrestamosPersonalesTbl a ".
                          "WHERE a.idDescuento = ${idDescuento} ".
                          "AND a.idEstatus = '1' ".
                          "ORDER BY a.secuencia ASC";
                $prestamoRst = fn_ejecuta_sql($selStr, $lb_log);

                $arrPagoPeriodo = array_map(function ($rowPeriodo){return floatval($rowPeriodo['pagoPeriodo']);}, $prestamoRst['root']);
                $sumapagomensual = array_sum($arrPagoPeriodo);
                $sumaPorcentaje = 0;
                $idxRec = 0;
                foreach ((array) $prestamoRst['root'] as $idxp => $prestamorow) {
                    $idxRec++;
                    $porcentaje = round(floatval($prestamorow['pagoPeriodo']) / $sumapagomensual,2);
                    if ($idxRec == $prestamoRst['records']) {
                        $porcentaje = 1 - $sumaPorcentaje;
                    }
                    $porcentajeAux = $porcentaje * 100;
                    $updPorcStr = 'UPDATE roPrestamosPersonalesTbl '.
                                  'SET porcentaje = '.$porcentajeAux.' '.
                                  'WHERE idPrestamo = '.$prestamorow['idPrestamo'];
                    fn_ejecuta_sql($updPorcStr, $lb_log);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql_error']   = $updPorcStr;
                        break;
                    }
                    $sumaPorcentaje += $porcentaje;
                }
                if ($a['success']) {
                    $updPorcStr = 'UPDATE roPrestamosPersonalesTbl '.
                                  'SET porcentaje = 0 '.
                                  "WHERE idDescuento = ${idDescuento} ".
                                  'AND idEstatus IN (0,2)';
                    fn_ejecuta_sql($updPorcStr, $lb_log);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql_error']   = $updPorcStr;
                    }
                }
            }

            if ($a['success'] == false) {
                $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
                $fehalog = strftime("%Y-%m-%d");
                $logFile = fopen("../log/logDescPersonales_err${fehalog}.log", "a");
                fwrite($logFile, "/*${fechaHora}*/ ".$a['sql_error'].";\r\n");
                fclose($logFile);
            }

            fn_ejecuta_sql($a['success'], $lb_log);
        }
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' <<3::' . PHP_EOL;

        // ACTUALIZA PORCENTAJE CUANDO NO CUBRE EL PAGO MANDAR ALERT (DIA dd AL INICIO DE CADA PERIODO)
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' >>4. ACTUALIZANDO PORCENTAJE DE DESCUENTO' . PHP_EOL;
        $diaActual = date('d');
        if (intval($diaActual) == 1) {
            $folioconc = date('Ymd');
            $selStr = 'SELECT * FROM roObservacionesDescuentosTbl ' .
                      'WHERE idDescuento = \'0\' '.
                      "AND observaciones = '2228${folioconc}' ".
                      "AND SUBSTRING(fechaEvento,1,10) = '$fechaActual'";
            $bandRst = fn_ejecuta_sql($selStr, $lb_log);
            if ($bandRst['records'] == 0) {
                $a = array('success' => true);
                $param15Str = '';
                $param20Str = '';
                $selStr = 'SELECT * FROM roconceptosdescuentoschofertbl ' .
                          'WHERE estatus = \'P\' '.
                          'AND concepto = \'2228\' '.
                          'AND tipoConcepto = \'A\' '.
                          'ORDER BY claveChofer';
                $descuentosPendRst = fn_ejecuta_sql($selStr, $lb_log);
                for ($i=0; $i < $descuentosPendRst['records']; $i++) {
                    $row = $descuentosPendRst['root'][$i];
                    $idDescuento = $row['idDescuento'];
                    $msjPorcentaje = 'NORMAL';

                    $selStr = 'SELECT * FROM roPrestamosPersonalesTbl ' .
                              'WHERE idEstatus = 1 '.
                              "AND idDescuento = ${idDescuento} ".
                              'ORDER BY idPrestamo, secuencia';
                    $prestamosRst = fn_ejecuta_sql($selStr, $lb_log);
                    $porcentaje = 15;
                    if ($prestamosRst['records'] > 1) {
                        $porcentaje = 20;
                        $msjPorcentaje = 'MAS DE UN PRESTAMO EN PROCESO';
                    }
                    $adeuAcumulado = 0;
                    $selStr = 'SELECT * FROM roControlDescuentoPeriodoTbl ' .
                              'WHERE idDescuento = '.$idDescuento.' '.
                              "AND fechaFinal <= '${fechaFinalProc}' ".
                              'ORDER BY secuencia';
                    $difAdeudosRst = fn_ejecuta_sql($selStr, $lb_log);
                    for ($idif=0; $idif < $difAdeudosRst['records']; $idif++) { 
                        $adeuAcumulado += floatval($difAdeudosRst['root'][$idif]['ctrlDescPeriodo']);
                    }
                    if ($adeuAcumulado > 0) {
                        $porcentaje = 20;
                        $msjPorcentaje = 'NO SE CUBRIO EL ADEUDO DE MESES ANTERIORES. DIFERENCIA $ '.round($adeuAcumulado,2);
                    }
                    if ($i > 0) {
                        $param15Str .= ($porcentaje == 15)?':':'';
                        $param20Str .= ($porcentaje == 20)?':':'';
                    }
                    $paramAuxStr = $row['claveChofer'].'|'.$row['centroDistribucion'].'|'.round($adeuAcumulado,2);
                    if ($porcentaje == 15) {
                        $param15Str .= $paramAuxStr;
                    } else {
                        $param20Str .= $paramAuxStr;
                    }

                    $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                              "SET tipoImporte = ".$porcentaje." ".
                              "WHERE idDescuento = ".$idDescuento;
                    fn_ejecuta_sql($updStr, $lb_log);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql_error']   = $updStr;
                    }
                }
                $paramStr = $param15Str.'#'.$param20Str;
                if ($a['success']) {
                    $insStr = "INSERT INTO roObservacionesDescuentosTbl (idDescuento, observaciones, parametros, fechaEvento, idUsuario) VALUES ".
                              "(0, '2228${folioconc}', ".
                              "'${paramStr}', ".
                              "NOW(), 0)";
                    $insRst = fn_ejecuta_sql($insStr, $lb_log);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql_error']   = $insStr;
                    }
                }

                if ($a['success'] == false) {
                    $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
                    $fehalog = strftime("%Y-%m-%d");
                    $logFile = fopen("../log/logDescPersonales_err${fehalog}.log", "a");
                    fwrite($logFile, "/*${fechaHora}*/ ".$a['sql_error'].";\r\n");
                    fclose($logFile);
                }

                fn_ejecuta_sql($a['success'], $lb_log);
            }
        }
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' <<4::' . PHP_EOL;

        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . '    </2228> ... ' . sizeof($procPrestamosRst['root']) . ' ' . PHP_EOL;
        echo "\n";
        fn_ejecuta_sql($procPrestamosRst['success'], $lb_log);

        /**
        ******************************************************************************
        * 2227  2227    2227    2227    2227    2227    2227    2227    2227    2227
        ******************************************************************************
        */
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . '    <2227> ACTUALIZANDO CONTROL' . PHP_EOL;
        // CREA CONTROL
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' >>1. TABULADO' . PHP_EOL;
        $selStr = 'SELECT cd.* ' .
                  'FROM roConceptosDescuentosChoferTbl cd ' .
                  'WHERE cd.concepto = \'2227\' '.
                  'AND cd.tipoConcepto = \'A\' '.
                  'AND cd.estatus = \'P\' '.
                  "AND SUBSTRING(cd.fechaEvento,1,10) = '${fechaActual}' " .
                  'ORDER BY cd.idDescuento';
        $genPeriodoRst = fn_ejecuta_sql($selStr, $lb_log);
        for ($i=0; $i < $genPeriodoRst['records']; $i++) { 
            $a = array('success' => true);
            $row = $genPeriodoRst['root'][$i];

            $selStr = 'SELECT cp.* ' .
                      'FROM roControlDescuentoPeriodoTbl cp ' .
                      'WHERE cp.idDescuento = '.$row['idDescuento'];
            $ctrlRst = fn_ejecuta_sql($selStr, $lb_log);
            if ($ctrlRst['records'] == 0) {
                $fechaInicioAux = $fechaActual;
                $periodo = intval($row['periodo']);
                $fechaInicioArr = explode('-', $fechaInicioAux);
                $fechaInicio = $fechaInicioAux.' 00:00:00';
                $sec = 0;
                if ($periodo > 1) {
                    $mes = intval($fechaInicioArr[1]);
                    $periodoAux = $periodo -1;
                    if ($mes == 12) {
                        $mes = $periodoAux;
                        $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                    } else {
                        $mesAux = $mes + $periodoAux;
                        if ($mesAux > 12) {
                            $mes = $mesAux - 12;
                            $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                        } else {
                            $mes = $mesAux;
                        }
                    }
                    $fechaInicioArr[1] = $mes;
                }
                $diaFinal = calculaDiasMes(intval($fechaInicioArr[0]), $fechaInicioArr[1]);
                $fechaFinal = $fechaInicioArr[0].'-'.$fechaInicioArr[1].'-'.$diaFinal.' 23:59:59';
                $sec++;
                $insStr = "INSERT INTO roControlDescuentoPeriodoTbl (idDescuento, secuencia, pagoPeriodoTotal, ctrlDescPeriodo, fechaInicio, fechaFinal) VALUES (".
                          "".$row['idDescuento'].", " .
                          "".$sec.", " .
                          "".$row['importe'].", " .
                          "".$row['importe'].", " .
                          "'".$fechaInicio."', " .
                          "'".$fechaFinal."')";
                fn_ejecuta_sql($insStr, $lb_log);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql']!="") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql_error']   = $insStr;
                }
            }

            if ($a['success'] == false) {
                $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
                $fehalog = strftime("%Y-%m-%d");
                $logFile = fopen("../log/logDescPersonales_err${fehalog}.log", "a");
                fwrite($logFile, "/*${fechaHora}*/ ".$a['sql_error'].";\r\n");
                fclose($logFile);
                break;
            }

            fn_ejecuta_sql($a['success'], $lb_log);
        }
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' <<1::' . PHP_EOL;

        // ACTUALIZA CONTROL
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' >>2. CONTROL 2227' . PHP_EOL;
        $diaActual = date('d');
        if (intval($diaActual) == 1) {
            $anioActual = date('Y');
            $mesActual = date('m');
            $anioProceso = intval($anioActual);
            $mesProceso = intval($mesActual) -1;
            if ($mesProceso == 0) {
                $anioProceso--;
                $mesProceso = 12;
            }
            $mesProceso = str_pad($mesProceso,2,"0",STR_PAD_LEFT);
            $diaFinalMesAnt = calculaDiasMes($anioProceso, $mesProceso);
            $fechaFinalProc = $anioProceso.'-'.$mesProceso.'-'.$diaFinalMesAnt.' 23:59:59';

            $selStr = 'SELECT cp.*, cd.importe, cd.periodo ' .
                      'FROM roControlDescuentoPeriodoTbl cp, roConceptosDescuentosChoferTbl cd ' .
                      'WHERE cp.idDescuento = cd.idDescuento '.
                      'AND cd.concepto = \'2227\' '.
                      'AND cd.estatus = \'P\' '.
                      "AND cp.fechaFinal = '${fechaFinalProc}' " .
                      'ORDER BY cp.idDescuento';
            $periodoAntRst = fn_ejecuta_sql($selStr, $lb_log);
            for ($i=0; $i < $periodoAntRst['records']; $i++) {
                $a = array('success' => true);

                $fechaInicioAux = $fechaActual;
                $sec = intval($periodoAntRst['root'][$i]['secuencia']);
                $idDescuento = $periodoAntRst['root'][$i]['idDescuento'];
                $importe = floatval($periodoAntRst['root'][$i]['importe']);
                $periodo = intval($periodoAntRst['root'][$i]['periodo']);
                // $fechaInicio = $periodoAntRst['root'][$i]['fechaInicio'];
                // $fechaFinal = $periodoAntRst['root'][$i]['fechaFinal'];
                $secActual = $sec+1;

                $fechaInicioArr = explode('-', $fechaInicioAux);
                $fechaInicio = $fechaInicioAux.' 00:00:00';

                if ($periodo > 1) {
                    $mes = intval($fechaInicioArr[1]);
                    $periodoAux = $periodo -1;
                    if ($mes == 12) {
                        $mes = $periodoAux;
                        $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                    } else {
                        $mesAux = $mes + $periodoAux;
                        if ($mesAux > 12) {
                            $mes = $mesAux - 12;
                            $fechaInicioArr[0] = intval($fechaInicioArr[0]) +1;
                        } else {
                            $mes = $mesAux;
                        }
                    }
                    $fechaInicioArr[1] = $mes;
                }
                $diaFinal = calculaDiasMes(intval($fechaInicioArr[0]), $fechaInicioArr[1]);
                $fechaFinal = $fechaInicioArr[0].'-'.$fechaInicioArr[1].'-'.$diaFinal.' 23:59:59';

                $insStr = "INSERT INTO roControlDescuentoPeriodoTbl (idDescuento, secuencia, pagoPeriodoTotal, ctrlDescPeriodo, fechaInicio, fechaFinal) VALUES (".
                          "".$idDescuento.", " .
                          "".$secActual.", " .
                          "".$importe.", " .
                          "".$importe.", " .
                          "'".$fechaInicio."', " .
                          "'".$fechaFinal."')";
                fn_ejecuta_sql($insStr, $lb_log);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql']!="") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql_error']   = $insStr;
                }

                if ($a['success'] == false) {
                    $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
                    $fehalog = strftime("%Y-%m-%d");
                    $logFile = fopen("../log/logDescPersonales_err${fehalog}.log", "a");
                    fwrite($logFile, "/*${fechaHora}*/ ".$a['sql_error'].";\r\n");
                    fclose($logFile);
                    break;
                }
                fn_ejecuta_sql($a['success'], $lb_log);
            }
        }
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' <<2::' . PHP_EOL;

        // ACTUALIZA SALDO Y ESTATUS
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' >>3. ACTUALIZA SALDO' . PHP_EOL;
        $selStr = 'SELECT cp.* ' .
                  'FROM roControlDescuentoPeriodoTbl cp, roConceptosDescuentosChoferTbl cd ' .
                  'WHERE cp.idDescuento = cd.idDescuento '.
                  'AND cd.concepto = \'2227\' '.
                  "AND '${fechaActual}' BETWEEN cp.fechaInicio AND cp.fechaFinal " .
                  'ORDER BY cp.idDescuento';
        $descuentosRst = fn_ejecuta_sql($selStr, $lb_log);

        for ($i=0; $i < $descuentosRst['records']; $i++) {
            $a = array('success' => true);

            $idDescuento = $descuentosRst['root'][$i]['idDescuento'];
            $sec = intval($descuentosRst['root'][$i]['secuencia']);
            for ($j=0; $j < 2; $j++) { //Upd periodo actual y anterior
                $selStr = 'SELECT * ' .
                          'FROM roControlDescuentoPeriodoTbl ' .
                          'WHERE idDescuento = '.$idDescuento.' '.
                          'AND secuencia = '.$sec;
                $periodosRst = fn_ejecuta_sql($selStr, $lb_log);
                $fechaInicio = $periodosRst['root'][0]['fechaInicio'];
                $fechaFinal = $periodosRst['root'][0]['fechaFinal'];

                $selStr = 'SELECT SUM(ph.adeudo) AS adeudoPeriodo ' .
                          'FROM roPagosOperadorHistoricoTbl ph ' .
                          'WHERE ph.idPrestamo = '.$idDescuento.' '.
                          'AND ph.concepto = \'2227\' '.
                          'AND ph.claveMovimiento = \'PA\' '.
                          "AND ph.fechaEvento BETWEEN '${fechaInicio}' AND '${fechaFinal}'";
                $controlDescPeriodoRst = fn_ejecuta_sql($selStr, $lb_log);
                $pagoPeriodoTotal = floatval($periodosRst['root'][0]['pagoPeriodoTotal']);
                $adeudoPeriodo = floatval($controlDescPeriodoRst['root'][0]['adeudoPeriodo']);
                $difAdeudo = $pagoPeriodoTotal - $adeudoPeriodo;

                $updStr = "UPDATE roControlDescuentoPeriodoTbl ".
                          "SET ctrlDescPeriodo = ".$difAdeudo." ".
                          "WHERE idTabulador = ".$periodosRst['root'][0]['idTabulador'];
                fn_ejecuta_sql($updStr, $lb_log);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql_error']   = $updStr;
                }

                $sec--;
                if ($sec == 0) {
                    break;
                }
                if ($a['success'] == false) {
                    break;
                }
            }
            $selStr = 'SELECT * FROM roControlDescuentoPeriodoTbl ' .
                      'WHERE idDescuento = '.$idDescuento.' '.
                      'ORDER BY secuencia ASC';
            $difAdeudosRst = fn_ejecuta_sql($selStr, $lb_log);
            $adeuAcumulado = 0;
            for ($idif=0; $idif < $difAdeudosRst['records']; $idif++) { 
                $adeuAcumulado += floatval($difAdeudosRst['root'][$idif]['ctrlDescPeriodo']);
            }

            $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                      "SET saldo = ".$adeuAcumulado." ".
                      "WHERE idDescuento = ".$idDescuento;
            fn_ejecuta_sql($updStr, $lb_log);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success']     = false;
                $a['msjResponse'] = $_SESSION['error_sql'];
                $a['sql_error']   = $updStr;
            }

            if ($a['success'] == false) {
                $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
                $fehalog = strftime("%Y-%m-%d");
                $logFile = fopen("../log/logDescPersonales_err${fehalog}.log", "a");
                fwrite($logFile, "/*${fechaHora}*/ ".$a['sql_error'].";\r\n");
                fclose($logFile);
            }

            fn_ejecuta_sql($a['success'], $lb_log);
        }
        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . ' <<3::' . PHP_EOL;


        $fechaHora = strftime("%Y-%m-%d %H:%M:%S");
        echo $fechaHora . '    </2227> ... ' . sizeof($procPrestamosRst['root']) . ' ' . PHP_EOL;
        echo "\n";
        fn_ejecuta_sql($procPrestamosRst['success'], $lb_log);

    //     sleep(10);
    // }

    function calculaDiasMes($anio, $mes){
        $dia = 31;
        switch($mes){
            case '02':
                $dia = ((($anio % 4)==0 && ($anio % 100)!=0) || ($anio % 400)==0)?29:28; break;
            case '04': case '06': case '09': case '11':
                $dia = 30; break;
        }
        return $dia;
    }
?>