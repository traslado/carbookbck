<?php

    $_REQUEST['zonaHoraria'] = date_default_timezone_get();
    
    session_start();

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("trGastosViajeTractor.php");



    


    switch($_REQUEST['autorizacionesHdn']){
        case 'GUARDARAUTORIZACION':
            GUARDARAUTORIZACION();
            break; 
        case 'SUMESTANCIAS':
            SUMESTANCIAS();
            break; 
        case 'SUMREPARACIONES':
            SUMREPARACIONES();
            break;
        case 'SUMGRATIFICACIONES':
            SUMGRATIFICACIONES();
            break;
        case 'COMBOREPARACIONES':
            COMBOREPARACIONES();
            break;  
        case 'CONSULTATAXIS':
            CONSULTATAXIS();
            break;

        case 'CONSULTAMACHETEROS':
            CONSULTAMACHETEROS();
            break;       
        case 'MACHETEROSENTREGA':
            MACHETEROSENTREGA();
            break;    
        case 'CONSULTATAXISEXTRA':
            CONSULTATAXISEXTRA();
            break;      
        default:
            echo '';

    }

    $_REQUEST = trasformUppercase($_REQUEST);
    

    function GUARDARAUTORIZACION(){ 

        $selIdViaje="SELECT * from trviajestractorestbl
                        WHERE claveChofer=".$_REQUEST['claveChofer']
                        ." and claveMovimiento in('VE','VF','VC','VU')
                        and viaje =(SELECT max(viaje) FROM trviajestractorestbl where claveChofer=".$_REQUEST['claveChofer'].");";
        $rsViaje=fn_ejecuta_query($selIdViaje);

        if ($rsViaje['root'] !=0) {
            $idViajeTractor="'".$rsViaje['root'][0]['idViajeTractor']."'";
        }else{
            $idViajeTractor='null';
        }

        $insAutorizacion = "INSERT INTO autorizacionesespecialestbl (idViajeTractor,centroDistribucion, claveChofer, tipoAutorizacion, estatus, fechaEvento, cantidad, observaciones,descripcion,origen,idUsuario) ".
                        "VALUES (".$idViajeTractor.",'".$_REQUEST['ciaSesVal']."', '".$_REQUEST['claveChofer']."', '".$_REQUEST['tipo']."', 'autorizado', now(), '".$_REQUEST['importe']."', '".$_REQUEST['observaciones']."', '".$_REQUEST['tipoRep']."'".",'CB','".$_SESSION['idUsuario']."');";

        $rs = fn_ejecuta_query($insAutorizacion);
        
    }

    function SUMESTANCIAS(){
        $sqlSumEst="SELECT sum(cantidad) as numDias
                    from autorizacionesespecialestbl
                    where claveChofer=".$_REQUEST['claveChofer']. "
                    and tipoAutorizacion='ESTANCIAS'
                    and estatus='autorizado';";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }

    function SUMREPARACIONES(){
        $sqlSumEst="SELECT sum(cantidad) as totRep
                    from autorizacionesespecialestbl
                    where claveChofer=".$_REQUEST['claveChofer']. "
                    and tipoAutorizacion='REPARACIONES'
                    and estatus='autorizado';";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }

    function SUMGRATIFICACIONES(){
        $sqlSumEst="SELECT sum(cantidad) as totGrat
                    from autorizacionesespecialestbl
                    where claveChofer=".$_REQUEST['claveChofer']. "
                    and tipoAutorizacion='GRATIFICACIONES'
                    AND cargoOperador is null 
                    and estatus='autorizado';";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }

    function CONSULTATAXIS(){
        $sqlSumEst="SELECT sum(cantidad) as totGrat
                    from autorizacionesespecialestbl
                    where claveChofer=".$_REQUEST['claveChofer']. "
                    and tipoAutorizacion='TAXIS'
                    and estatus='autorizado';";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }

    function CONSULTAMACHETEROS(){
        $sqlSumEst="SELECT sum(cantidad) as totGrat
                    from autorizacionesespecialestbl
                    where claveChofer=".$_REQUEST['claveChofer']. "
                    and tipoAutorizacion='MANIOBRAS'
                    and estatus='autorizado';";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }
    function MACHETEROSENTREGA(){
        $sqlSumEst="SELECT sum(numeroUnidades) as totGrat from trtalonesviajestbl
                    where idViajeTractor=".$_REQUEST['idViajeTractorHdn']. "
                    and claveMovimiento='TE'";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }
    

    function COMBOREPARACIONES(){
        $sqlTipo="SELECT valor as tipoReparacion, nombre as descripcion
                    from cageneralestbl
                    where tabla='autorizacionesespecialestbl'";
        $rsTipo=fn_ejecuta_query($sqlTipo);

        echo json_encode($rsTipo);
    }

       function CONSULTATAXISEXTRA(){
        $sqlSumEst="SELECT sum(cantidad) as totGrat
                    from autorizacionesespecialestbl
                    where claveChofer=".$_REQUEST['claveChofer']. "
                    and tipoAutorizacion='TAXIS EXTRAORDINARIOS'
                    and estatus='autorizado';";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }

    
?>
