<?php

    $savePath = "./../";

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/barcode.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $pdf = new FPDF('L', 'mm', array(216, 140));

    $success = true;

    if($_REQUEST['trViajesTractoresIdViajeHdn'] == ""){
        $success = false;
    }

    if ($success) {
        $sqlGetDataStr = "SELECT vt.*, tr.tractor, tr.marca, tr.modelo, tr.placas, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno,".
                            "ch.licencia, ch.vigenciaLicencia, co.descripcion AS nombreCompania, co.compania, ".
                            "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = tr.marca) AS descMarcaTractor, ".
                            "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                    "WHERE pl.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, " .
                            "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                    "WHERE pl2.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                            "pa.pais, es.estado, mu.municipio, col.colonia, col.cp, dr.calleNumero, dr.direccion ".
                            "FROM trViajesTractoresTbl vt, caTractoresTbl tr, caChoferesTbl ch, caCompaniasTbl co, ".
                                "caDistribuidoresCentrosTbl dc, caDireccionesTbl dr, caPaisesTbl pa, caEstadosTbl es,  ".
                                "caMunicipiosTbl mu, caColoniasTbl col ".
                            "WHERE tr.idTractor = vt.idTractor ".
                            "AND ch.claveChofer = vt.claveChofer ".
                            "AND co.compania = tr.compania ".
                            "AND dc.distribuidorCentro = vt.centroDistribucion ".
                            "AND dc.direccionFiscal = dr.direccion ".
                            "AND dr.idColonia = col.idColonia ".
                            "AND col.idMunicipio = mu.idMunicipio ".
                            "AND mu.idEstado = es.idEstado ".
                            "AND es.idPais = pa.idPais ".
                            "AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'];

        $rs = fn_ejecuta_query($sqlGetDataStr);

        if (sizeof($rs) > 0) {
            $pdf = generarPdf($rs['root'][0], $pdf, $nInt);

        }
        //print_r($rs['root'][0]);
        $pdf->Output('bitactoraViaje.pdf', I);
    } else {
        echo "ERROR";
    }

    function generarPdf($data, $pdf, $n){
        $border = 0;

        $pdf->AddPage();
        $pdf->SetAutoPageBreak(false);
        $pdf->AddFont('skyline', '', 'skyline-reg.php');
        $pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->SetFont('Times','',10);
        //Titulo
        ///Compañia
        $pdf->SetY(5);
        $pdf->SetX(108-50);
        $pdf->Cell(100,4,$data['nombreCompania'], $border,0, 'C');
        ///Bitacora
        $pdf->SetY(10);
        $pdf->SetX(108-50);        
        $pdf->Cell(100,4,"BITACORA DE HORAS DE SERVICIO DEL OPERADOR", $border,1, 'C');

        //Permisionario
        $pdf->SetY(20);
        $pdf->SetX(10);
        $pdf->Cell(150,4,"Nombre del Permisionario:  "."(".$data['compania'].")  ".$data['nombreCompania'], $border,1, 'L');
        //Domicilio Permisionario
        $pdf->SetX(10);
        $pdf->Cell(150,4,"Domicilio:  ".toUTF8($data['calleNumero'].", ".$data['colonia'].", ".$data['municipio'].", ".
                                        $data['estado'].", ".$data['pais'].", CP. ".$data['cp']), $border,1, 'L');
        //Num Tractor
        $pdf->SetX(10);
        $pdf->Cell(150,4,"Eco:  ".$data['tractor'], $border,1, 'L');
        //Marca Tractor, Modelo, Placas
        $pdf->SetX(10);
        $pdf->Cell(80,4,"Tracto Camion Marca:  ".$data['descMarcaTractor'], $border,0, 'L');
        $pdf->Cell(30,4,"Modelo:  ".$data['modelo'], $border,0, 'L');
        $pdf->Cell(40,4,"Placas vehiculo:  ".$data['placas'], $border,1, 'L');
        //Nombre chofer
        $pdf->SetX(10);
        $pdf->Cell(150,4,"Nombre del operador:  ".
            toUTF8($data['nombre']." ".$data['apellidoPaterno']." ".$data['apellidoMaterno']), $border,1, 'L');
        //Licencia y Vigencia
        $pdf->SetX(10);
        $pdf->Cell(60,4,"No de Licencia:  ".$data['licencia'], $border,0, 'L');
        $pdf->Cell(40,4,"Vigencia:  ".$data['vigenciaLicencia'], $border,1, 'L');
        //Viaje, Origem y Destino
        $pdf->SetX(10);
        $pdf->Cell(30,4,"VIAJE:  ".$data['viaje'], $border,0, 'L');
        $pdf->Cell(60,4,"Origen:  ".toUTF8($data['descPlazaOrigen']), $border,0, 'L');
        $pdf->Cell(60,4,"Destino:  ".toUTF8($data['descPlazaDestino']), $border,1, 'L');
        //Ruta
        $sqlGetTalonesStr = "SELECT tv.idPlazaOrigen, tv.idPlazaDestino, kp.kilometros, ".
                            "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, " .
                            "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino ".
                            "FROM trTalonesViajesTbl tv, caKilometrosPlazaTbl kp ".
                            "WHERE kp.idPlazaOrigen = tv.idPlazaOrigen ".
                            "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                            "AND tv.idViajeTractor = ".$_REQUEST['trViajestTractoresIdViajeHdn']." ".
                            "ORDER BY kilometros";

        $rsRuta = fn_ejecuta_query($sqlGetTalonesStr);
        if(sizeof($rsRuta['root']) > 0){
            $rutas = array();
            $rutaTxt = "";
            foreach ($rsRuta['root'] as $destino) {
                if(!in_array($destino['idPlazaDestino'], $rutas)){
                    array_push($rutas, $destino['idPlazaDestino']);
                    if($rutaTxt != ""){
                        $rutaTxt .= " - ";
                    }
                    $rutaTxt .= $destino['descPlazaDestino'];
                }
            }
        }
        $pdf->SetX(10);
        $pdf->Cell(100,4,"Ruta a seguir:  ".$rutaTxt, $border,1, 'L');

        //Fecha Inicio, Fecha de Terminacion
        $t = strtotime($data['fechaEvento']);
        $pdf->SetY(55);
        $pdf->SetX(10);
        $pdf->Cell(80,5,"Fecha de inicio:  ".date('d/m/Y',$t), $border,0, 'L');
        $pdf->Cell(70,5,"Fecha de terminacion: _____________________", $border,1, 'L');

        //Grid
        ///Horizontal
        $long = str_repeat("-", 166);
        $pdf->SetX(10);
        $pdf->Cell(195,3,$long, $border,0, 'L');
        $pdf->SetY(71);
        $pdf->SetX(10);
        $pdf->Cell(195,3,$long, $border,0, 'L');
        $pdf->SetY(111);
        $pdf->SetX(10);
        $pdf->Cell(195,3,$long, $border,0, 'L');
        ///Vertical
        $long = str_repeat("|", 25);

        //Columns
        $mod = 1;
        $xPos = 10+$mod;
        $pdf->SetY(62);
        $pdf->SetX($xPos-$mod);
        $pdf->MultiCell(2,2,$long, $border, 'C');
        $pdf->SetY(63);
        $pdf->SetX($xPos);
        $pdf->MultiCell(22,4,"Hora de Salida", $border, 'C');
        $xPos += 22;
        $pdf->SetY(62);
        $pdf->SetX($xPos-$mod);
        $pdf->MultiCell(2,2,$long, $border, 'C');
        $pdf->SetY(63);
        $pdf->SetX($xPos);
        $pdf->MultiCell(22,4,"Hora de Llegada", $border, 'C');
        $xPos += 22;
        $pdf->SetY(62);
        $pdf->SetX($xPos-$mod);
        $pdf->MultiCell(2,2,$long, $border, 'C');
        $pdf->SetY(63);
        $pdf->SetX($xPos);
        $pdf->MultiCell(35,4,"Horas de servicio conduciendo", $border, 'C');
        $xPos += 35;
        $pdf->SetY(62);
        $pdf->SetX($xPos-$mod);
        $pdf->MultiCell(2,2,$long, $border, 'C');
        $pdf->SetY(63);
        $pdf->SetX($xPos);
        $pdf->MultiCell(35,4,"Horas de servicio sin conducir", $border, 'C');
        $xPos += 35;
        $pdf->SetY(62);
        $pdf->SetX($xPos-$mod);
        $pdf->MultiCell(2,2,$long, $border, 'C');
        $pdf->SetY(63);
        $pdf->SetX($xPos);
        $pdf->MultiCell(28,4,"Paradas no programadas", $border, 'C');
        $xPos += 28;
        $pdf->SetY(62);
        $pdf->SetX($xPos-$mod);
        $pdf->MultiCell(2,2,$long, $border, 'C');
        $pdf->SetY(63);
        $pdf->SetX($xPos);
        $pdf->MultiCell(28,4,"Horas Fuera de servicio", $border, 'C');
        $xPos += 28;
        $pdf->SetY(62);
        $pdf->SetX($xPos-$mod);
        $pdf->MultiCell(2,2,$long, $border, 'C');
        $pdf->SetY(63);
        $pdf->SetX($xPos);
        $pdf->MultiCell(25,4,"Hora de Descanso", $border, 'C');
        $xPos += 25;
        $pdf->SetY(62);
        $pdf->SetX($xPos-$mod);
        $pdf->MultiCell(2,2,$long, $border, 'C');

        //FIRMAS
        $pdf->SetY(130);
        $pdf->SetX(42);
        $pdf->MultiCell(50,4,str_repeat("_", 25)."  OPERADOR", $border, 'C');
        $pdf->SetY(130);
        $pdf->SetX(117);
        $pdf->MultiCell(50,4,str_repeat("_", 25)."  TRANSDRIZA S.A. DE C.V.", $border, 'C');

        return $pdf;
    }

?> 