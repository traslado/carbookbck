<?php

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    //require_once("alUnidades.php");
	ini_set('memory_limit', '1024M'); // or you could use 1G
        //$_REQUEST = trasformUppercase($_REQUEST);
    
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    //$_REQUEST = trasformUppercase($_REQUEST);
	                  
    switch($_REQUEST['movimientoUnidadesActionHdn']){
        case 'copiarArchivo':
            copiarArchivo();
            break;
        case 'leerXLS':
            leerXLS();                                                 
            break;
        case 'getConsultaDetalle':
            getConsultaDetalle();                                               
            break;
        case 'getConsultaCabecero':
            getConsultaCabecero();                                               
            break;
        case 'copiarArchivoVinVision':
            copiarArchivoVinVision();                                               
            break; 
        case 'cargaVinVision':
            cargaVinVision();                                               
            break;
        case 'ConsultaUnidadesVinVision':
        	ConsultaUnidadesVinVision();
        	break;
        case 'facturarUnidadesPeriodo':
        	facturarUnidadesPeriodo();	         
        	break;
        case 'creacionReporteExcel':
        	creacionReporteExcel();	
        	break;
        case 'consultaReportes':
        	consultaReportes();	
        	break;
        case 'SimbolosPendientes':
        	SimbolosPendientes();	
        	break;
        case 'numeroSimbolo':
        	numeroSimbolo();	
        	break;
        default:
            echo '';
    }

	function copiarArchivo(){
	session_start();
	//include 'leerXLS.php';

    $a = array();
	$a['successTitle'] = "Manejador de Archivos";

	if(isset($_FILES)) {
		if($_FILES["rhEmpleadosPathPhotoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["rhEmpleadosPathPhotoFld"]["error"];
		} else {
			$temp_file_name = $_FILES['rhEmpleadosPathPhotoFld']['tmp_name']; 
			$original_file_name = $_FILES['rhEmpleadosPathPhotoFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/Temp/'. $file_name . $ext;
		  
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;					
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;					
					$a['root'] = leerXLS($new_name);
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
			$a['successMessage'] = "Archivo Cargado";
			
			//}else {
			//	$a['success'] = false;
			//	$a['errorMessage'] = "Error FILES NOT SET";
			//}
		}
	}
	echo json_encode($a);
}
    

function leerXLS($inputFileName){
    	/** Error reporting */    
	error_reporting(E_ALL);
	require_once("../funciones/Classes/PHPExcel.php");
	
	date_default_timezone_set ("America/Mexico_City");
	
		//  Include PHPExcel_IOFactory
	include '../funciones/Classes/PHPExcel/IOFactory.php';	
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
	try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
	$sheet = $objPHPExcel->getSheet(0); 
	$highestRow = $sheet->getHighestRow(); 
	$highestColumn = $sheet->getHighestColumn();

	//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
	$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow);
		
		//------------------------------------------------------------------------------------------------
		
		$sqlInsertaCabecero = "INSERT INTO alMovimientosUnidadesTbl (nombrePeriodo, estatusPeriodo, fechaInicial, fechaFinal, totalUnidades, unidadesFacturadas, unidadesPendientes, numeroFactura) ".
            				  "VALUES(".
        					  "'".$_REQUEST['movimientoUnidadesNomPeriodoTxt']."', ".
        					  "'A', ".
        					  "'".$_REQUEST['movimientoUnidadesFinicioCmb']."', ".
        					  "'".$_REQUEST['movimientoUnidadesFfinalCmb']."', ".
        					  "'000', ".
        					  "'000', ".
        					  "'000', ".
        					  "'000') ";


		$rs = fn_ejecuta_Add($sqlInsertaCabecero);

		$sqlConsultaCabecero = "SELECT idPeriodo FROM almovimientosunidadestbl ".
								" WHERE nombrePeriodo = '".$_REQUEST['movimientoUnidadesNomPeriodoTxt']."' ";

        $rs = fn_ejecuta_query($sqlConsultaCabecero);

        $idPeriodo = $rs['root'][0]['idPeriodo'];

		$isValidArr = array('tipoFactura' =>array('val'=>'','size'=>true,'exist'=>true),
								'busUnit'=>array('val'=>'','size'=>true,'exist'=>true),
								'mercado' =>array('val'=>'','size'=>true,'exist'=>true),
								'simboloUnidad' =>array('val'=>'','size'=>true,'exist'=>true),
								'serie' =>array('val'=>'','size'=>true,'exist'=>true),
								'vin' =>array('val'=>'','size'=>true,'exist'=>true),
								'distribuidor' =>array('val'=>'','size'=>true,'exist'=>true),
								'fechaDocumento' =>array('val'=>'','size'=>true,'exist'=>true),
								'estatus' =>array('val'=>'','size'=>true,'exist'=>true)
							);
		
		for($row = 0; $row<sizeof($rowData); $row++){
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...			
			$simUnidad = substr($rowData[$row][3],0,3);

			if($rowData[$row][2] == 'M')
			{

				if($rowData[$row][1] == 'TO'){
					$sqlInsertaDetalle = "INSERT INTO alMovimientoUnidadesDetalleTbl ".
            							"VALUES(".
            				  			"'".$idPeriodo."', ".
        					  			"'".$rowData[$row][0]."', ".
        					  			"'".$rowData[$row][1]."', ".
        					  			"'".$rowData[$row][2]."', ".
        					  			"'".$rowData[$row][3]."', ".
        					  			"'".$rowData[$row][4]."', ".
        					  			"'".$rowData[$row][5]."', ".
        					  			"'".$rowData[$row][6]."', ".
        					  			"'".$rowData[$row][7]."', ".
        					  			"'P')";

					$rs = fn_ejecuta_Add($sqlInsertaDetalle);
				}
				if($rowData[$row][1] == 'SA'){
					$sqlInsertaDetalle = "INSERT INTO alMovimientoUnidadesDetalleTbl ".
            							"VALUES(".
            				  			"'".$idPeriodo."', ".
        					  			"'".$rowData[$row][0]."', ".
        					  			"'".$rowData[$row][1]."', ".
        					  			"'".$rowData[$row][2]."', ".
        					  			"'".$rowData[$row][3]."', ".
        					  			"'".$rowData[$row][4]."', ".
        					  			"'".$rowData[$row][5]."', ".
        					  			"'".$rowData[$row][6]."', ".
        					  			"'".$rowData[$row][7]."', ".
        					  			"'P')";

					$rs = fn_ejecuta_Add($sqlInsertaDetalle);
				}
				if($rowData[$row][1] == 'CH'){

					$sqlInsertaDetalle = "INSERT INTO alMovimientoUnidadesDetalleTbl ".
            							"VALUES(".
            				  			"'".$idPeriodo."', ".
        					  			"'".$rowData[$row][0]."', ".
        					  			"'".$rowData[$row][1]."', ".
        					  			"'".$rowData[$row][2]."', ".
        					  			"'".$rowData[$row][3]."', ".
        					  			"'".$rowData[$row][4]."', ".
        					  			"'".$rowData[$row][5]."', ".
        					  			"'".$rowData[$row][6]."', ".
        					  			"'".$rowData[$row][7]."', ".
        					  			"'P')";

					$rs = fn_ejecuta_Add($sqlInsertaDetalle);
				}
				if($rowData[$row][1] == 'SV'){

					$sqlInsertaDetalle = "INSERT INTO alMovimientoUnidadesDetalleTbl ".
            							"VALUES(".
            				  			"'".$idPeriodo."', ".
        					  			"'".$rowData[$row][0]."', ".
        					  			"'".$rowData[$row][1]."', ".
        					  			"'".$rowData[$row][2]."', ".
        					  			"'".$rowData[$row][3]."', ".
        					  			"'".$rowData[$row][4]."', ".
        					  			"'".$rowData[$row][5]."', ".
        					  			"'".$rowData[$row][6]."', ".
        					  			"'".$rowData[$row][7]."', ".
        					  			"'P')";

					$rs = fn_ejecuta_Add($sqlInsertaDetalle);
				}
				if($rowData[$row][1] == 'MI'){

					$sqlInsertaDetalle = "INSERT INTO alMovimientoUnidadesDetalleTbl ".
            							"VALUES(".
            				  			"'".$idPeriodo."', ".
        					  			"'".$rowData[$row][0]."', ".
        					  			"'".$rowData[$row][1]."', ".
        					  			"'".$rowData[$row][2]."', ".
        					  			"'".$rowData[$row][3]."', ".
        					  			"'".$rowData[$row][4]."', ".
        					  			"'".$rowData[$row][5]."', ".
        					  			"'".$rowData[$row][6]."', ".
        					  			"'".$rowData[$row][7]."', ".
        					  			"'P')";

					$rs = fn_ejecuta_Add($sqlInsertaDetalle);
				}
				if($simUnidad =='FFF'){

					$sqlInsertaDetalle = "INSERT INTO alMovimientoUnidadesDetalleTbl ".
            							"VALUES(".
            				  			"'".$idPeriodo."', ".
        					  			"'".$rowData[$row][0]."', ".
        					  			"'".$rowData[$row][1]."', ".
        					  			"'".$rowData[$row][2]."', ".
        					  			"'".$rowData[$row][3]."', ".
        					  			"'".$rowData[$row][4]."', ".
        					  			"'".$rowData[$row][5]."', ".
        					  			"'".$rowData[$row][6]."', ".
        					  			"'".$rowData[$row][7]."', ".
        					  			"'P')";

					$rs = fn_ejecuta_Add($sqlInsertaDetalle);
				}
			}
		}

		$sqlConsultaTotales = 	"SELECT COUNT(A.VIN) AS totalUnidades, ".
      							"(SELECT COUNT(B.VIN) FROM alMovimientoUnidadesDetalleTbl B ".
       							"	WHERE B.estatus = 'P' ".
       							"     AND B.idPeriodo = '".$idPeriodo."') AS unidadesPendientes, ".
      							"(SELECT COUNT(C.VIN) FROM alMovimientoUnidadesDetalleTbl C ".
       							"   WHERE C.estatus = 'F' ".
       							"     AND C.idPeriodo = '".$idPeriodo."') AS unidadesFacturadas ".
								"FROM alMovimientoUnidadesDetalleTbl A ".
								" WHERE A.idPeriodo = '".$idPeriodo."' ";

		$rs = fn_ejecuta_query($sqlConsultaTotales);

		$tUnidades = $rs['root'][0]['totalUnidades'];
		$pendientes = $rs['root'][0]['unidadesPendientes'];
		$facturadas = $rs['root'][0]['unidadesFacturadas'];		

		$sqlActualizaTotales = "UPDATE alMovimientosUnidadesTbl ".
								" SET totalUnidades = '".$tUnidades."', ".
								" unidadesFacturadas = '".$facturadas."', ".
								" unidadesPendientes = '".$pendientes."' ".
								" WHERE idPeriodo = '".$idPeriodo."' ";

		$rs = fn_ejecuta_Upd($sqlActualizaTotales);								

		return $rs['root'];
	}

	function getConsultaDetalle(){

		 if ($gb_error_filtro == 0){
	        $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesNomPeriodoTxt'], "MO.nombrePeriodo", 0);
	        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
			$lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesFinicioCmb'], "MO.fechaInicial", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
	        $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesFfinalCmb'], "MO.fechaFinal", 0);
	        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }

		$sqlConsultaGrid = " SELECT MD.idPeriodo, MD.tipoFactura, MD.busUnit, MD.mercado, MD.simboloUnidad, MD.serie, MD.vin, MD.distribuidor, MD.fechaDocumento, MD.estatus ".
							"FROM almovimientosunidadestbl MO, almovimientounidadesdetalletbl MD ".
							"WHERE MO.idPeriodo = MD.idPeriodo ".
	  						"AND MO.nombrePeriodo = '".$_REQUEST['movimientoUnidadesNomPeriodoTxt']."'  ".
	  						"AND MO.fechaInicial = '".$_REQUEST['movimientoUnidadesFinicioCmb']."'  ".
	  						"AND MO.fechaFinal = '".$_REQUEST['movimientoUnidadesFfinalCmb']."' ";

		$rs = fn_ejecuta_query($sqlConsultaGrid);  				

		echo json_encode($rs);
	}

	function getConsultaCabecero(){
		
		$sqlConsultaCabecero = "SELECT idPeriodo, nombrePeriodo, unidadesFacturadas, unidadesPendientes, ".
			    				"totalunidades, estatusPeriodo, numeroFactura , (SELECT GE.nombre ".
															"FROM cageneralestbl GE ".
															"WHERE GE.tabla = 'alMovimientosUnidadesTbl' ".
                                                            "AND  GE.columna = 'estatusPeriodo' ".
															"AND GE.valor =  alMovimientosUnidadesTbl.estatusPeriodo)  AS detalleEstatus ".
		"FROM almovimientosunidadestbl  ".
		" WHERE fechaInicial >= '".$_REQUEST['movimientoUnidadesPerInicioCmb']."'  ".
	  	"  AND fechaFinal <= '".$_REQUEST['movimientoUnidadesPerFinalCmb']."' ";
		

	  	$rs = fn_ejecuta_query($sqlConsultaCabecero);  				

		echo json_encode($rs);
	}
	
	function copiarArchivoVinVision(){
	session_start();
	//include 'leerXLS.php';
		
    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	if(isset($_FILES)) {
		if($_FILES["movimientoUnidadesVinFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["movimientoUnidadesVinFld"]["error"];
		} else {
			$temp_file_name = $_FILES['movimientoUnidadesVinFld']['tmp_name']; 
			$original_file_name = $_FILES['movimientoUnidadesVinFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/Temp/'. $file_name . $ext;
		  
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;					
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";

					$a['archivo'] = $file_name . $ext;
					
					$tcoFile = fopen($new_name, 'r');
					$linea = 1;
					$dataArr = array();
					$valueSep = "";
					$numCol = 23;
					$mensaje = "";

					while (($rowArr = fgetcsv($tcoFile, 1000, ",")) !== false) {
							if($linea == 1){
								$linea += 1;
								continue;
							}
						if(sizeof($rowArr) != $numCol){
							$a['success'] = false;							
							$a['errorMessage'] = "Error al leer linea ".strval($linea)." del archivo";
							break;
						}
						else{							

	  						$sqlInsertVinVision = "INSERT INTO alMovimientosVinVisionTbl".
  							"(vin, modelo, rampaOrigen, codigoEnsamble, buildCodeDate, fechaLiberado, diasEstancia, embarque, codigoHoldOrigen, holdTransportista, ".
  							" estatusActual, fechaHoraEstatusActual, estatusEspecial, ubicacionActual, informacionUbicacion, centroDistribucion, rampaDestino, ".
  							" codigoDistribuidor, nombreDistribuidor, ciudadDistribuidor, vendido, plataformaImportacion, estimadoArribo )".
  							" VALUES ( ".
  							"'".$rowArr[0]."', ".
  							"'".$rowArr[1]."', ".
  							"'".$rowArr[2]."', ".
  							"'".$rowArr[3]."', ".
  							"'".$rowArr[4]."', ".
  							"'".$rowArr[5]."', ".
  							"'".$rowArr[6]."', ".
  							"'".$rowArr[7]."', ".
  							"'".$rowArr[8]."', ".
  							"'".$rowArr[9]."', ".
  							"'".$rowArr[10]."', ".
  							"'".$rowArr[11]."', ".
  							"'".$rowArr[12]."', ".
  							"'".$rowArr[13]."', ".
  							"'".$rowArr[14]."', ".
  							"'".$rowArr[15]."', ".
  							"'".$rowArr[16]."', ".
  							"'".$rowArr[17]."', ".
  							"'".$rowArr[18]."', ".
  							"'".$rowArr[19]."', ".
  							"'".$rowArr[20]."', ".
  							"'".$rowArr[21]."', ".
  							"'".$rowArr[22]."' ) ";			
					
							$rs = fn_ejecuta_Add($sqlInsertVinVision);

						echo json_encode($rs);

						}

						$linea += 1;
					}												
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
			$a['successMessage'] = "Archivo Cargado";			
		}
	}
	echo json_encode($a);
}

function ConsultaUnidadesVinVision(){
	$sqlConsultaUnidadesVinVision = "SELECT  vin, modelo, estatusActual,fechaHoraEstatusActual ".
									"FROM alMovimientosvinvisiontbl ";

	$rs = fn_ejecuta_query($sqlConsultaUnidadesVinVision);

	echo json_encode($rs);	
}

function facturarUnidadesPeriodo(){
	// actualizas en el detalle los estatus de las unidades con base al estatus de VinVision
	$sqlActualizaUnidades = "UPDATE almovimientounidadesdetalletbl ". 
							"SET estatus = 'F' ".
							"WHERE almovimientounidadesdetalletbl.vin = (SELECT almovimientounidadesdetalletbl.vin ".
							"FROM  almovimientosvinvisiontbl VV, alMovimientosUnidadesTbl MU ". 
							"WHERE almovimientounidadesdetalletbl.vin = VV.vin ".
                            "AND MU.idPeriodo = almovimientounidadesdetalletbl.idPeriodo ".
                            "AND VV.estatusActual = 'Delivered - (D)' ".
                            "AND mu.idPeriodo = '".$_REQUEST['periodo']."') ";

	$rs = fn_ejecuta_Upd($sqlActualizaUnidades);


	$sqlActualizaCabecero = "UPDATE alMovimientosUnidadesTbl ".
							 "SET alMovimientosUnidadesTbl.unidadesFacturadas = (SELECT COUNT(md.vin) as unidadesFacturadas ".
																			  "FROM alMovimientoUnidadesDetalleTbl md ".
                                                                              "WHERE alMovimientosUnidadesTbl.idPeriodo = md.idPeriodo ".
																				"AND md.estatus = 'F' ".
																				"AND md.idPeriodo = '".$_REQUEST['periodo']."' ), ".
							    " alMovimientosUnidadesTbl.unidadesPendientes = (SELECT COUNT(md.vin) as unidadesPendientes ".
																			  	"FROM alMovimientoUnidadesDetalleTbl md ".
                                                                              	"WHERE alMovimientosUnidadesTbl.idPeriodo =  md.idPeriodo ".
																				  "AND md.estatus = 'P' ".
																				  "AND md.idPeriodo = '".$_REQUEST['periodo']."')
							WHERE alMovimientosUnidadesTbl.idPeriodo = '".$_REQUEST['periodo']."' ";
	$rs = fn_ejecuta_Upd($sqlActualizaCabecero);

	

	$sqlConsultaCierre = "SELECT unidadesPendientes  FROM alMovimientosUnidadesTbl ".
						 "WHERE idPeriodo = '".$_REQUEST['periodo']."' ";						 

	$rs = fn_ejecuta_query($sqlConsultaCierre);		

	$numeroPendientes = $rs['root'][0]['unidadesPendientes'];

	if($numeroPendientes == '0'){

	 	$sqlActualizaEstatus =  "UPDATE alMovimientosUnidadesTbl".
	 							" SET estatusPeriodo = 'C'".
	 							"WHERE idPeriodo = '".$_REQUEST['periodo']."' ";

	 	$rs = fn_ejecuta_Upd($sqlActualizaEstatus);

	 	
	}
	else{
			 	$sqlActualizaEstatus =  "UPDATE alMovimientosUnidadesTbl".
	 							" SET estatusPeriodo = 'A'".
	 							"WHERE idPeriodo = '".$_REQUEST['periodo']."' ";

	 			$rs = fn_ejecuta_Upd($sqlActualizaEstatus);
	 			
	}				 

}

function actualizaFactura(){	

	$sqlActualizaFactura = "UPDATE alMovimientosUnidadesTbl".
							"SET numeroFactura = '".$_REQUEST['numeroFactura']."' ".
							"WHERE idPeriodo = '".$_REQUEST['periodo']."' ";

	$rs = fn_ejecuta_Upd($sqlActualizaFactura);

	 			echo json_encode($rs);
}

function creacionReporteExcel($idPeriodo){	
	
	facturarUnidadesPeriodo();		

    $sqlConsultaReporte =   "SELECT 'KZX' AS tipoDocumento,  md.busUnit, md.mercado, md.simboloUnidad, md.serie, md.vin, md.distribuidor, ".
                            "md.fechaDocumento, su.descripcion, vv.rampaDestino, su.descTarifaFactura ".
                            " FROM alMovimientoUnidadesDetalleTbl md, alMovimientosVinVisionTbl vv, casimbolosunidadestbl su ".
                            " WHERE md.vin = vv.vin ".
                            " AND md.simboloUnidad = su.simboloUnidad ".
                            " AND md.estatus = 'F' ".
                            " AND md.idPeriodo = '".$_REQUEST['periodo']."'";
                            
    
    $rs = fn_ejecuta_query($sqlConsultaReporte);

    $sqlConsultaAgrupaciones =  "SELECT su.descTarifaFactura, count(descTarifaFactura)  as conteo ".                   
								" FROM almovimientounidadesdetalletbl md, casimbolosunidadestbl su ".
								" WHERE md.simboloUnidad = su.simboloUnidad ".								
								" AND md.estatus = 'F'".
								" AND md.idPeriodo = '".$_REQUEST['periodo']."'".
								" GROUP BY su.descTarifaFactura";

	$rj = fn_ejecuta_query($sqlConsultaAgrupaciones);		

    $sqlConsultaTotal = 	"SELECT count(descTarifaFactura)  as sumaUnidades ".                   
							" FROM almovimientounidadesdetalletbl md, casimbolosunidadestbl su ".
							" WHERE md.simboloUnidad = su.simboloUnidad ".								
							" AND md.estatus = 'F'".
							" AND md.idPeriodo = '".$_REQUEST['periodo']."'";							

	$rc = fn_ejecuta_query($sqlConsultaTotal);							

    //echo json_encode($rs); 

	error_reporting(E_ALL);

	include_once '../funciones/Classes/PHPExcel.php';
	include '../funciones/Classes/PHPExcel/IOFactory.php';	

	$objXLS = new PHPExcel();
	$objSheet = $objXLS->setActiveSheetIndex(0);
	$objSheet->setCellValue('A1', 'TIPO DOCUMENTO');
	$objSheet->setCellValue('B1', 'BUS UNIT');
	$objSheet->setCellValue('C1', 'MERCADO');
	$objSheet->setCellValue('D1', 'MODELO');
	$objSheet->setCellValue('E1', 'SERIE');
	$objSheet->setCellValue('F1', 'VIN');
	$objSheet->setCellValue('G1', 'DISTRIBUIDOR');
	$objSheet->setCellValue('H1', 'FECHA DOCUMENTO');
	$objSheet->setCellValue('I1', 'MODELO');
	$objSheet->setCellValue('J1', 'RAMPA DESTINO');
	$objSheet->setCellValue('K1', 'TIPO UNIDAD');

	//$i = 3; //Numero de fila donde se va a comenzar a rellenar
	$fila = $rs['root'];
	//print_r($fila);
	
	$objSheet = $objXLS->setActiveSheetIndex(0);
    for ($i=0, $j=2;  $i < count($fila); $i++, $j++) {

        $objSheet->setCellValue('A'.$j, $fila[$i]['tipoDocumento']);
        $objSheet->setCellValue('B'.$j, $fila[$i]['busUnit']);
        $objSheet->setCellValue('C'.$j, $fila[$i]['mercado']);
        $objSheet->setCellValue('D'.$j, $fila[$i]['simboloUnidad']);
        $objSheet->setCellValue('E'.$j, $fila[$i]['serie']);
        $objSheet->setCellValue('F'.$j, $fila[$i]['vin']);
        $objSheet->setCellValue('G'.$j, $fila[$i]['distribuidor']);
        $objSheet->setCellValue('H'.$j, $fila[$i]['fechaDocumento']);
        $objSheet->setCellValue('I'.$j, $fila[$i]['descripcion']);
        $objSheet->setCellValue('J'.$j, $fila[$i]['rampaDestino']);
        $objSheet->setCellValue('K'.$j, $fila[$i]['descTarifaFactura']);
    }
    $j++;
    $objSheet->setCellValue('A'.$j, 'MODELO');
    $objSheet->setCellValue('B'.$j, 'KZX 1ra Facturacion');    	
    $j++;
	
	//print_r($rj['root']);
	//$objSheet = $objXLS->setActiveSheetIndex(0);
    for ($i2 = 0, $j2 = $j ;$i2 < count($rj['root']); $i2++, $j2++) {

    	
        $objSheet->setCellValue('A'.$j2, $rj['root'][$i2]['descTarifaFactura']);        
        $objSheet->setCellValue('B'.$j2, $rj['root'][$i2]['conteo']);
    }

    $j2++;
    $i2 = 0;
    $objSheet->setCellValue('A'.$j2, 'Suma de Unidades:');    
    $objSheet->setCellValue('B'.$j2, $rc['root'][$i2]['sumaUnidades']);
    echo $rc['root'][$i2]['sumaUnidades'];

	$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("J")->setAutoSize(true);
	$objXLS->getActiveSheet()->getColumnDimension("K")->setAutoSize(true);
	
	$objXLS->getActiveSheet()->setTitle('movUnidades');
	$objXLS->setActiveSheetIndex(0);
    $fecha = date("Ymd");;
    echo $fecha;

	$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
	$objWriter->save("c:/archImportado/UnidadesFacturadas-".$fecha.".xls");
	exit;
	
}

function consultaReportes(){

    //print_r($_REQUEST);
    //return;

    switch($_REQUEST['movimientoUnidadesTipoReporteHdn']){
        case 'consultaClasificacion':

			$sqlDescTarifa = " SELECT DISTINCT descTarifaFactura ".
							 " FROM casimbolosunidadestbl ".
							 " WHERE descTarifaFactura <> '' ";

			$rs = fn_ejecuta_query($sqlDescTarifa);							
		break;
		case 'consultaDistribuidor':
			
			$sqlDistribuidor = "SELECT DISTINCT distribuidor ". 
							   "FROM almovimientounidadesdetalletbl";

			$rs = fn_ejecuta_query($sqlDistribuidor);				
    	break;
    	case 'consultaSimboloUnidad':

    	    $sqlSimboloUnidad = "SELECT simboloUnidad,concat(simboloUnidad,' - ',descTarifaFactura) as clasificacionTarifa ".
								" FROM casimbolosunidadestbl ".
								" where descTarifaFactura <> ''";

							$rs = fn_ejecuta_query($sqlSimboloUnidad);						
		break;
		case 'ConsultaPeriodos':

			$sqlPeriodos = "SELECT idPeriodo, nombrePeriodo FROM almovimientosunidadestbl";

			$rs = fn_ejecuta_query($sqlPeriodos);
		break;
		case 'ejecutaQuery02':
		    	$lsWhereStr =   " WHERE md.vin = vv.vin ".
		    					" AND md.mercado = 'M' ".
		    					" AND ge.valor = md.estatus ".
		    					" AND ge.tabla = 'alMovimientoUnidadesDetalleTbl' ".
                            	" AND md.simboloUnidad = su.simboloUnidad ".                            	
                            	" AND md.idPeriodo = '".$_REQUEST['periodo']."'";

    		if ($gb_error_filtro == 0){
	    		$lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesClasCmb'], "su.descTarifaFactura ", 1);
		    	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    	}
	    	if ($gb_error_filtro == 0){
	    		$lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesSimbCmb'], "md.simboloUnidad", 1);
		    	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    	}
        	if ($gb_error_filtro == 0){
	            $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesDistCmb'], "md.distribuidor", 1);
            	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        	}
        	if ($gb_error_filtro == 0){
	            $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesUfaltantesHdn'], "md.estatus", 1);
            	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        	}

	    	$sqlConsultaRangoStr = "SELECT 'KZX' AS tipoDocumento,  md.busUnit, md.mercado, md.simboloUnidad, md.serie, md.vin, md.distribuidor, ".
            	                   "md.fechaDocumento, su.descripcion, vv.rampaDestino, su.descTarifaFactura, ge.nombre ".
                               	   " FROM alMovimientoUnidadesDetalleTbl md, alMovimientosVinVisionTbl vv, casimbolosunidadestbl su, caGeneralesTbl ge ".$lsWhereStr;

        	$rs = fn_ejecuta_query($sqlConsultaRangoStr);        

			echo json_encode($rs);


			error_reporting(E_ALL);

			include_once '../funciones/Classes/PHPExcel.php';
			include '../funciones/Classes/PHPExcel/IOFactory.php';	

			$objXLS = new PHPExcel();
			$objSheet = $objXLS->setActiveSheetIndex(0);
			$objSheet->setCellValue('A1', 'TIPO DOCUMENTO');
			$objSheet->setCellValue('B1', 'BUS UNIT');
			$objSheet->setCellValue('C1', 'MERCADO');
			$objSheet->setCellValue('D1', 'MODELO');
			$objSheet->setCellValue('E1', 'SERIE');
			$objSheet->setCellValue('F1', 'VIN');
			$objSheet->setCellValue('G1', 'DISTRIBUIDOR');
			$objSheet->setCellValue('H1', 'FECHA DOCUMENTO');
			$objSheet->setCellValue('I1', 'MODELO');
			$objSheet->setCellValue('J1', 'RAMPA DESTINO');
			$objSheet->setCellValue('K1', 'TIPO UNIDAD');
			$objSheet->setCellValue('L1', 'ESTATUS UNIDAD');

			//$i = 3; //Numero de fila donde se va a comenzar a rellenar
			$fila = $rs['root'];
			//print_r($fila);
		
			$objSheet = $objXLS->setActiveSheetIndex(0);
    		for ($i=0, $j=2;  $i < count($fila); $i++, $j++) {

        		$objSheet->setCellValue('A'.$j, $fila[$i]['tipoDocumento']);
        		$objSheet->setCellValue('B'.$j, $fila[$i]['busUnit']);
        		$objSheet->setCellValue('C'.$j, $fila[$i]['mercado']);
        		$objSheet->setCellValue('D'.$j, $fila[$i]['simboloUnidad']);
        		$objSheet->setCellValue('E'.$j, $fila[$i]['serie']);
        		$objSheet->setCellValue('F'.$j, $fila[$i]['vin']);
        		$objSheet->setCellValue('G'.$j, $fila[$i]['distribuidor']);
        		$objSheet->setCellValue('H'.$j, $fila[$i]['fechaDocumento']);
        		$objSheet->setCellValue('I'.$j, $fila[$i]['descripcion']);
        		$objSheet->setCellValue('J'.$j, $fila[$i]['rampaDestino']);
        		$objSheet->setCellValue('K'.$j, $fila[$i]['descTarifaFactura']);
        		$objSheet->setCellValue('L'.$j, $fila[$i]['nombre']);
    		}

			$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("J")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("K")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("L")->setAutoSize(true);
			
			$objXLS->getActiveSheet()->setTitle('movUnidades');
			$objXLS->setActiveSheetIndex(0);
    		$fecha = date("Ymd");;
    		echo $fecha;

			$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
			$objWriter->save("c:/archImportado/ReporteUnidadesPeriodo-".$fecha.".xls");
			exit;
		break;
		case 'ejecutaQuery01': 
			$lsWhereStr =   " WHERE md.vin = vv.vin ".
		    					" AND md.mercado = 'M' ".
		    					" AND ge.valor = md.estatus ".
		    					" AND ge.tabla = 'alMovimientoUnidadesDetalleTbl' ".
                            	" AND md.simboloUnidad = su.simboloUnidad ".                            	
                            	" AND md.fechaDocumento BETWEEN '".$_REQUEST['fInicial']."' AND '".$_REQUEST['fFinal']."' ";

    		if ($gb_error_filtro == 0){
	    		$lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesClasCmb'], "su.descTarifaFactura ", 1);
		    	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    	}
	    	if ($gb_error_filtro == 0){
	    		$lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesSimbCmb'], "md.simboloUnidad", 1);
		    	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    	}
        	if ($gb_error_filtro == 0){
	            $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesDistCmb'], "md.distribuidor", 1);
            	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        	}
        	if ($gb_error_filtro == 0){
	            $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesUfaltantesHdn'], "md.estatus", 1);
            	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        	}


	    	$sqlConsultaRangoStr = "SELECT 'KZX' AS tipoDocumento,  md.busUnit, md.mercado, md.simboloUnidad, md.serie, md.vin, md.distribuidor, ".
            	                   "md.fechaDocumento, su.descripcion, vv.rampaDestino, su.descTarifaFactura, ge.nombre ".
                               	   " FROM alMovimientoUnidadesDetalleTbl md, alMovimientosVinVisionTbl vv, casimbolosunidadestbl su, caGeneralesTbl ge ".$lsWhereStr;

        	$rs = fn_ejecuta_query($sqlConsultaRangoStr);        

			echo json_encode($rs);

			error_reporting(E_ALL);

			include_once '../funciones/Classes/PHPExcel.php';
			include '../funciones/Classes/PHPExcel/IOFactory.php';	

			$objXLS = new PHPExcel();
			$objSheet = $objXLS->setActiveSheetIndex(0);
			$objSheet->setCellValue('A1', 'TIPO DOCUMENTO');
			$objSheet->setCellValue('B1', 'BUS UNIT');
			$objSheet->setCellValue('C1', 'MERCADO');
			$objSheet->setCellValue('D1', 'MODELO');
			$objSheet->setCellValue('E1', 'SERIE');
			$objSheet->setCellValue('F1', 'VIN');
			$objSheet->setCellValue('G1', 'DISTRIBUIDOR');
			$objSheet->setCellValue('H1', 'FECHA DOCUMENTO');
			$objSheet->setCellValue('I1', 'MODELO');
			$objSheet->setCellValue('J1', 'RAMPA DESTINO');
			$objSheet->setCellValue('K1', 'TIPO UNIDAD');
			$objSheet->setCellValue('L1', 'ESTATUS UNIDAD');

			//$i = 3; //Numero de fila donde se va a comenzar a rellenar
			$fila = $rs['root'];
			//print_r($fila);
		
			$objSheet = $objXLS->setActiveSheetIndex(0);
    		for ($i=0, $j=2;  $i < count($fila); $i++, $j++) {

        		$objSheet->setCellValue('A'.$j, $fila[$i]['tipoDocumento']);
        		$objSheet->setCellValue('B'.$j, $fila[$i]['busUnit']);
        		$objSheet->setCellValue('C'.$j, $fila[$i]['mercado']);
        		$objSheet->setCellValue('D'.$j, $fila[$i]['simboloUnidad']);
        		$objSheet->setCellValue('E'.$j, $fila[$i]['serie']);
        		$objSheet->setCellValue('F'.$j, $fila[$i]['vin']);
        		$objSheet->setCellValue('G'.$j, $fila[$i]['distribuidor']);
        		$objSheet->setCellValue('H'.$j, $fila[$i]['fechaDocumento']);
        		$objSheet->setCellValue('I'.$j, $fila[$i]['descripcion']);
        		$objSheet->setCellValue('J'.$j, $fila[$i]['rampaDestino']);
        		$objSheet->setCellValue('K'.$j, $fila[$i]['descTarifaFactura']);
        		$objSheet->setCellValue('L'.$j, $fila[$i]['nombre']);
    		}

			$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("F")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("G")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("H")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("I")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("J")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("K")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("L")->setAutoSize(true);
			
			$objXLS->getActiveSheet()->setTitle('movUnidades');
			$objXLS->setActiveSheetIndex(0);
    		$fecha = date("Ymd");;
    		echo $fecha;

			$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
			$objWriter->save("c:/archImportado/ReporteUnidadesFecha-".$fecha.".xls");
			exit;
		break;
       	default:
           $rs = 'fghjgfyjy';            
    }
    echo json_encode($rs);
}

function SimbolosPendientes(){

	$sqlSimbolosNuevos = "select distinct md.simboloUnidad ".
						 "from almovimientounidadesdetalletbl md ".
						 "where md.simboloUnidad not in(select su.simboloUnidad ".
						 " 								from casimbolosunidadestbl su) ";

	$rs = fn_ejecuta_query($sqlSimbolosNuevos);        
	echo json_encode($rs);
}

function numeroSimbolo(){

	$sqlnumeroSimbolos = "select count(distinct md.simboloUnidad) as numeroPendiente ".
						 "from almovimientounidadesdetalletbl md ".
						 "where md.simboloUnidad not in(select su.simboloUnidad ".
						 "								from casimbolosunidadestbl su) ";

	$rs = fn_ejecuta_query($sqlnumeroSimbolos);        
	echo json_encode($rs);
}

?>