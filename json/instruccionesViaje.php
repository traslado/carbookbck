<?php

    $_REQUEST['zonaHoraria'] = date_default_timezone_get();
    
    session_start();

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("trGastosViajeTractor.php");



    


    switch($_REQUEST['trConsultaHdn']){
        case 'OBTENEROPERADOR':
            OBTENEROPERADOR();
            break; 
        case 'GUARDAHOJA':
            GUARDAHOJA();
            break; 
        case 'DESTINOVIAJE':
            DESTINOVIAJE();
            break;
        case 'SUMGRATIFICACIONES':
            SUMGRATIFICACIONES();
            break;
        case 'COMBOREPARACIONES':
            COMBOREPARACIONES();
            break;        
        default:
            echo '';

    }

    $_REQUEST = trasformUppercase($_REQUEST);
    

    function OBTENEROPERADOR(){ 

        $sqlOperador="select b.numerounidades,b.viaje,a.tractor, concat(c.nombre,' ',c.apellidoPaterno,' ', c.apellidoMaterno) as operador, (SELECT f.plaza FROM caplazastbl f where f.idPlaza=b.idPlazaOrigen) as origen,(SELECT f.plaza FROM caplazastbl f where f.idPlaza=b.idPlazaDestino) as destino
        ,(SELECT concat(r.origen,'**',r.ruta1,'--',r.ruta2,'--',r.ruta3,'--',r.ruta4,'--',r.ruta5,'--',r.ruta6,'--',r.ruta7,'--',r.ruta8,'--',r.ruta9,'--',r.ruta10,'**',r.destino) 
 FROM trrutasautorizadastbl r, caplazastbl h where r.origen=h.plaza and h.idPlaza=b.idPlazaOrigen and r.destino=(select g.plaza from caplazastbl g where g.idPlaza=b.idPlazaDestino)) as ruta, b.idviajetractor,(select nombre from cageneralestbl g where g.tabla='pendientesPoliza' and g.columna='validos' and g.estatus=b.claveMovimiento) as estatus
            from catractorestbl a, trviajestractorestbl b, cachoferestbl c
            where a.idTractor=".$_REQUEST['tractor']."
            and a.idtractor=b.idtractor
            and c.clavechofer=b.clavechofer
            and b.fechaevento=(select max(a.fechaevento) from trviajestractorestbl a, catractorestbl b where  b.idTractor=".$_REQUEST['tractor']." and a.idtractor=b.idtractor AND a.claveMovimiento!='VA');";
        $rsOperador=fn_ejecuta_query($sqlOperador);

        echo json_encode($rsOperador);
        
    }



    function GUARDAHOJA(){
            $sqlSumEst="INSERT INTO trhojainstruccionestbl (`centroDistribucion`, `idTractor`, `nombreOperador`, `Origen`, `Destino`, `rutaAutorizada`, `cambioRuta`, `fechaSalida`, `horaSalida`, `observaciones`, `fechaHoja`, `autoriza`,`claveMovimiento`, `idUsuario`, `preRetorno`, `idviajetractor`) 
VALUES ('".$_REQUEST['ciaSesVal']."', '".$_REQUEST['tractor']."', '".$_REQUEST['operador']."', '".$_REQUEST['origen']."', '".$_REQUEST['destino']."', '".$_REQUEST['rutaAutorizada']."', '".$_REQUEST['cambioRuta']."', '".substr($_REQUEST['fecha'], 0,10)."', '".$_REQUEST['hora']."', '".$_REQUEST['instrucciones']."', NOW(), '".$_REQUEST['autoriza']."','HG' ,'".$_SESSION['idUsuario']."','".$_REQUEST['retorno']."','".$_REQUEST['idviajetractor']."');";
        $rsSum=fn_ejecuta_query($sqlSumEst);

    }

    function DESTINOVIAJE(){ 

        $sqlOperador="SELECT concat(r.origen,'**',r.ruta1,'--',r.ruta2,'--',r.ruta3,'--',r.ruta4,'--',r.ruta5,'--',r.ruta6,'--',r.ruta7,'--',r.ruta8,'--',r.ruta9,'--',r.ruta10,'**',r.destino) as rutaAutorizada
                from trrutasautorizadastbl r
                where r.origen='".substr($_REQUEST['origen'],8)."'
                AND r.destino='".$_REQUEST['destino']."'";
        $rsOperador=fn_ejecuta_query($sqlOperador);

        echo json_encode($rsOperador);
        
    }



    
?>
