<?php
    session_start();
	
	//include 'alCargaArchivoLzc.php';
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");
	
    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["estatusTenderCargaArchFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["estatusTenderCargaArchFld"]["error"];

		} else {
			$temp_file_name = $_FILES['estatusTenderCargaArchFld']['tmp_name']; 
			$original_file_name = $_FILES['estatusTenderCargaArchFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
		  
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLS($new_name);
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

    function leerXLS($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) < 4 && $highestRow > 2 ) {
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) <= 3) {
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17) {
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN-COMPANIA-distribuidor]', 'fail'=>'Y');
			return $root;
		}
		//-------------------------------a-----------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
							'Transportista'=>array('val'=>'','size'=>true,'exist'=>true),
								'Distribuidor' =>array('val'=>'','size'=>true,'exist'=>true)
							);
		
		for($row = 0; $row<sizeof($rowData); $row++){
			
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['VINN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['VINN']['val'] = $rowData[$row][0];
			$isValidArr['Transportista']['val'] = $rowData[$row][1];
			//$isValidArr['Simbolo']['size'] = strlen($rowData[$row][1]) > 0;
			$isValidArr['Distribuidor']['val'] = $rowData[$row][2];
			//$isValidArr['distribuidor']['size'] = strlen($rowData[$row][2]) == 5;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {


				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}
				if($rowData[$row][1] == null) {
					$errorMsg .='EL '.$key.' Transportista no existe';
					echo json_encode("Transportista".$rowData[$row][1]);
					$isTrue = false;
				}if($rowData[$row][2] == null) {
					$errorMsg .='El '.$key.' distribuidor no existe';
					echo json_encode("Distribuidor ".$rowData[$row][2]);
					$isTrue = false;					
				
				}
				else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						if (!$isValidArr[$key]['exist']) {

							 if (!$isValidArr[$key]['exist']) {

							 	if ($key == 'VIN') {
							 		$siNo = ' ya cuenta con tender';
							 	}
							 	else
							 	{
							 		$siNo = ' no Existe';

							 	}
						
							$errorMsg .= 'El '.$key.$siNo;
							$isTrue = false; 
						}
						}
					}
				
				}	

			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
				$_SESSION['usuCompania'] = 'LZC02';
				
			
				$today = date("Y-m-d H:i:s");
				$fecha = substr($today,0,10);
				$hora=substr($today,11,8);

				$sqlStatus = "SELECT cveStatus,nomfac ".
							 "FROM alInstruccionesMercedesTbl ".
							 "WHERE VIN = '".$rowData[$row][0]."' and cveLoc='LZC02' limit 1";

				$rssqlStatus = fn_ejecuta_query($sqlStatus);

				$numVIN = $rowData[$row][0];
				$dirEnt = $rowData[$row][1];
				$CveDisFac = $rowData[$row][2];


			if($rssqlStatus['root'][0]['cveStatus'] == 'DK' && $dirEnt =='KCSM'){


					if(substr($rssqlStatus['root'][0]['nomfac'], -1) == 'M' )
				{
					
					$nomfac=rtrim($rssqlStatus['root'][0]['nomfac'],'M');
					$tUnidad = 'TK';
				}

				else
				{
					$nomfac=$rssqlStatus['root'][0]['nomfac'];
					$tUnidad = 'TK';

				}

			}
			elseif($rssqlStatus['root'][0]['cveStatus'] == 'DK' && $dirEnt != 'KCSM' ) {

					if(substr($rssqlStatus['root'][0]['nomfac'], -1) == 'M')
				{
					
					$nomfac=$rssqlStatus['root'][0]['nomfac'];
					$tUnidad = 'TK';
				}

				else
				{

					$nomfac=$rssqlStatus['root'][0]['nomfac'].'M';
					$tUnidad = 'TK';


				}
			}
			elseif($rssqlStatus['root'][0]['cveStatus'] == 'DH' && $dirEnt =='KCSM' ){

					if(substr($rssqlStatus['root'][0]['nomfac'], -1) == 'M')
				{
					
					$nomfac=rtrim($rssqlStatus['root'][0]['nomfac'],'M');
					$tUnidad = 'TY';
				}

				else
				{
					$nomfac=$rssqlStatus['root'][0]['nomfac'];
					$tUnidad = 'TY';

				}

			}
			elseif($rssqlStatus['root'][0]['cveStatus'] == 'DH' && $dirEnt !='KCSM' ) {

					if(substr($rssqlStatus['root'][0]['nomfac'], -1) == 'M')
				{
					
					$nomfac=$rssqlStatus['root'][0]['nomfac'];
					$tUnidad = 'TY';
				}

				else
				{

					$nomfac=$rssqlStatus['root'][0]['nomfac']."M";
					$tUnidad = 'TY';

				}
			}
				$today =  date('Y-m-d H:i:s');

				$addHist = "UPDATE alInstruccionesMercedesTbl set cveStatus='".$tUnidad."' , ".
							"cveDisFac='".$CveDisFac."' , ".
							"cveColor='".$today."' , ".
							"cveDisEnt='".$CveDisFac."' , ".
							"dirEnt='".$dirEnt."', ".
							"nomfac='".$nomfac."'  ".
							"WHERE VIN='".$numVIN."' and cveLoc='LZC02' ";

				fn_ejecuta_query($addHist);	

				$updalUnid = "UPDATE alUnidadesTbl set distribuidor='".$CveDisFac."' ".
							"WHERE VIN='".$numVIN."'  ";

				fn_ejecuta_query($updalUnid);	

				$updUd = "UPDATE alultimodetalleTbl set distribuidor='".$CveDisFac."' ".
							"WHERE VIN='".$numVIN."'  ";

				fn_ejecuta_query($updUd);	

				/*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCOMPANIA'],'PR',
						  $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCOMPANIA'], $repuve, $chofer, $observaciones,$rowData[$row][4]);*/


				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					$errorMsg = 'Agregado Correctamente';
				} else {
					$errorMsg = 'Registro No Agregado';
				}		
			}
			$root[]= array('vin'=>$rowData[$row][0],'compania'=>$rowData[$row][1], 'distribuidor'=>$rowData[$row][2],'nose'=>$errorMsg);			
		}

		return $root;	


	}


	function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base


		switch(strtoupper($field)) {
			case 'VIN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus in ('TK','TY','SO')";
				$rs = fn_ejecuta_query($sqlExist);
			break;

			case 'VINN':

				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' ";

				$rs = fn_ejecuta_query($sqlExist);
				
			break;
			case 'TRANSPORTISTA':

				$sqlExist = "SELECT compania FROM caCompaniasTbl WHERE compania = '".$value."' and  tipoCompania='G'";

				$rs = fn_ejecuta_query($sqlExist);
				
			break;
			case 'DISTRIBUIDOR':

				$sqlExist = "SELECT distribuidorCentro FROM cadistribuidoresCentrosTbl WHERE distribuidorCentro = '".$value."'";

				$rs = fn_ejecuta_query($sqlExist);
			break;
		}


		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
}
		
?>