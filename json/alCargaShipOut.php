<?php
    session_start();
	
	//include 'iVerificacionRepuve.php';
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");
	
     $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["interfacesLzcShipOutArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["interfacesLzcShipOutArchivoFld"]["error"];

		} else {
			$temp_file_name = $_FILES['interfacesLzcShipOutArchivoFld']['tmp_name']; 
			$original_file_name = $_FILES['interfacesLzcShipOutArchivoFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
		  
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLS($new_name);
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

	ejecutaRRL();


    function leerXLS($inputFileName) {
    	
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try{
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		}catch(Exception $e){
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);
		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) <= 67 && $highestRow <= 3 ){
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) == 3){
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17){
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN-Forgon-Tarja]', 'fail'=>'Y');
			return $root;
		}
		//------------------------------------------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true));

			//echo($isValidArr);
		
		for($row = 0; $row<sizeof($rowData); $row++){
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {
				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						if(!$isValidArr[$key]['exist']){
							$siNo = ($key == 'VIN') ? ' ya ' : ' no ';
							$errorMsg .= 'El '.$key.$siNo.'Existe!';
							$isTrue = true; 


						}
					}
				}
			}



			if ($isTrue){ 

				$_SESSION['usuCompania'] = 'LZC02';

				$sqlUpdIm = "UPDATE alinstruccionesmercedestbl ".
							"SET trimCode='".$rowData[$row][1]."', ".
							"trimDesc='".$rowData[$row][2]."', ".
							"invPrice='P' ,".
							"fechaMovimiento='".$rowData[$row][3]."' ".
							"WHERE vin='".$rowData[$row][0]."' ";

				fn_ejecuta_query($sqlUpdIm);

				//Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa					

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl

				/*$addPio = "INSERT INTO alPioTbl (vin, portCode, workCode,orderType,estatus,fechaEvento)".
				"VALUES( '".$rowData[$row][0]."' ,".
					"'FT16', ".
					"'".$rowData[$row][3]."', ".
					"'A', ".
					"'PD', ".						
					" NOW() ) ";

				fn_ejecuta_query($addPio);*/
	

				/*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCompania'],'PR',
						  $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCompania'], $repuve, $chofer, $observaciones,$rowData[$row][4]);*/

				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){

			
					
					$errorMsg = 'Agregado Correctamente';					
				}else{
					$errorMsg = 'Registro No Agregado';
				}								
			}

		$root[]= array('VIN'=>$rowData[$row][1],'nose'=>$errorMsg);	

		}		
		return $root;	
	}	


	function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
		$tabla = 'tabla';
		$columna = 'columna';
		switch(strtoupper($field)) {
			case 'VIN':
				$tabla = 'altransaccionunidadtbl'; $columna = 'vin'; break;

		}
		$sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
		$rs = fn_ejecuta_query($sqlExist);
		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
	}


		function ejecutaRRL(){	

        $sqlGTE = 	"SELECT dy.cveStatus ".
					"FROM alultimodetalletbl al, alinstruccionesmercedestbl dy ".
					"WHERE al.vin = dy.vin ".
					"AND al.centroDistribucion='LZC02' ".
					"AND dy.vin not in (SELECT ht.vin FROM altransaccionunidadtbl ht WHERE ht.tipoTransaccion ='RRL') ".
					"AND dy.vin in (select vin from alHistoricoUnidadesTbl where claveMovimiento='L3') ";

		$rsGTE= fn_ejecuta_query($sqlGTE);


		for ($i=0; $i <sizeof($rsGTE['root']) ; $i++) { 

			if (substr($rsGTE['root'][$i]['cveStatus'], -1) == 'K') {				
				$arrK[] = $rsGTE['root'][$i];				
			}
		/////////////////////////////////////////////////////////
			else if (substr($rsGTE['root'][$i]['cveStatus'], -1) == 'H') {
				$arrH[] = $rsGTE['root'][$i];
			}
			else{
				//echo "string";
			}
		}
		if (count($arrK) != 0) {
			$varEstatus = 'DK';
			$nomArchivo = 'K';
			 ejecutaRRL1($varEstatus,$nomArchivo);
			
		}	
		if (count($arrH) != 0) {
			$varEstatus = 'DH';
			$nomArchivo = 'H';
			 ejecutaRRL1($varEstatus,$nomArchivo);
		}			
	} 



	function ejecutaRRL1($varEstatus,$nomArchivo){

			$sqlGenerarDatos= "SELECT vin,nomFac,trimCode,trimDesc,fEvento,hEvento,fechaMovimiento FROM alinstruccionesMercedestbl".
						  " WHERE vin not in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='RRL')  ".
						  " AND vin in (SELECT vin from altransaccionUnidadTbl where tipoTransaccion='RLS') ".
						  "AND vin in (SELECT vin from alultimodetalletbl where claveMovimiento='L3') ".
						  "AND cveStatus='".$varEstatus."' ".
						  "AND invPrice='P'";

       	$rssqlGenerarDatos= fn_ejecuta_query($sqlGenerarDatos);
		
       	for ($i=0; $i <sizeof($rssqlGenerarDatos['root']) ; $i++) { 


			if (substr($rssqlGenerarDatos['root'][$i]['nomFac'], -1) !='M') {	
				$arrUnidades[] = $rssqlGenerarDatos['root'][$i];	

				}
			else {

			}
		}

		if (count($arrUnidades) != 0) {
		generaRRL1($arrUnidades,$varEstatus,$nomArchivo);
		
		}


	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function generaRRL1($arrUnidades,$varEstatus,$nomArchivo){

		$fecha = date('Ymd');
	   	$hora = date("His");
	   	$today =  date('Y-m-d H:i:s');				
			
		//$directorio ="C:carbook/i816/";

		$directorio = "E:\\carbook\\archivosInterfaces\\";

		$inicioFile = "KMM_RRL_".$fecha.$hora.".txt";

		$nombreBusqueda = "KMM_RRL_".$fecha.$hora.".txt";

		$archivo = fopen($directorio.$inicioFile,"w");

		//encabezado
		fwrite($archivo,"RRLH"." "."APS"."  "."GMX"."  "."RRL".$fecha.$hora.PHP_EOL);

		//detalle

		for ($i=0; $i <sizeof($arrUnidades) ; $i++) {

			fwrite($archivo,"RRL  FT16 ".$arrUnidades[$i]['vin']."KCSM    FT14 ".substr($arrUnidades[$i]['fechaMovimiento'],0,4).substr($arrUnidades[$i]['fechaMovimiento'],5,2).substr($arrUnidades[$i]['fechaMovimiento'],8,2).substr($arrUnidades[$i]['fechaMovimiento'],11,2).substr($arrUnidades[$i]['fechaMovimiento'],14,2).substr($arrUnidades[$i]['fechaMovimiento'],17,2)."0000000000".sprintf('%-10s',$arrUnidades[$i]['trimCode'])."  A".sprintf('%-12s',$arrUnidades[$i]['trimDesc']).substr($arrUnidades[$i]['fechaMovimiento'],0,4).substr($arrUnidades[$i]['fechaMovimiento'],5,2).substr($arrUnidades[$i]['fechaMovimiento'],8,2).PHP_EOL);
			
		$insTransaccion = "INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus, fecha, hora) ".
								"VALUES ('RRL','LZC02','".$arrUnidades[$i]['vin']."', '".
										$arrUnidades[$i]['fechaMovimiento']."','ST', '".
										$today."', '".$varEstatus."', '".
										substr($today,0,10)."', '".
										substr($today,11,8)."')" ;
			fn_ejecuta_query($insTransaccion);

		$UpdStatus= "UPDATE alinstruccionesmercedestbl set invPrice='G' WHERE invPrice='P' and vin='".$arrUnidades[$i]['vin']."' ";

		fn_ejecuta_query($UpdStatus);
			//echo json_encode($insTransaccion);
		}
		//fin de archivo
		$long=(sizeof($arrUnidades)+2);
		fwrite($archivo,"RRLT ".sprintf('%06d',($long)));
		fclose($archivo);

		ftpArchivo($nombreBusqueda);
	}

		function ftpArchivo($nombreBusqueda){
			if(file_exists("E:/carbook/archivosInterfaces/".$nombreBusqueda)){
			# Definimos las variables
			$host="ftp.glovismx.com";
			$port=21;
			$user="GMX_TRA";
			$password="art180Xmg!";
			$ruta="/IN";
			$file = "E:/carbook/archivosInterfaces/".$nombreBusqueda;//tobe uploaded 
			$remote_file = "".$nombreBusqueda;						
			$nuevo_fichero = "E:/carbook/archivosInterfaces/".$nombreBusqueda;;
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}	
	}

?>