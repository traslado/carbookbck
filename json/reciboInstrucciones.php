<?php

    /*********************************************
    Sólo necesita reciboGastosIdViajeTractor para funcionar
    *********************************************/
    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/funcionesGlobales.php");
    require_once("trGastosViajeTractor.php");

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
    $a = array();
    $e = array();
    

    generarPdf(); 

    function generarPdf(){

        $SQL="SELECT a.*,b.tractor from trhojainstruccionestbl a, catractorestbl b
                WHERE a.idTractor=".$_REQUEST['tractor']."
                AND a.fechaHoja =(SELECT MAX(d.fechaHoja) FROM trhojainstruccionestbl d WHERE d.idTractor='".$_REQUEST['tractor']."')
                AND a.claveMovimiento='HG'
                AND a.idTractor=b.idTractor";
        $rsSql=fn_ejecuta_query($SQL);

        //echo json_encode($rsSql);

                if(sizeof($rsSql['root']) > 0){


        $pdf = new FPDF();
        $pdf->AddPage();

        $pdf->setY(15);
        $pdf->SetX(18);
        $pdf->SetFont('Arial', '', 18);
        $pdf->Cell(0,10,utf8_decode('RECIBO DE INSTRUCCIONES DE VIAJE'), $border,0, 'C');

        $pdf->setY(2);
        $pdf->SetX(18);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode($rsSql['root'][0]['fechaHoja']), $border,0, 'L');

        $pdf->setY(35);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode('TRACTOR:   '.$rsSql['root'][0]['tractor']), $border,0, 'L');

        $pdf->setY(45);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode('OPERADOR:  '.$rsSql['root'][0]['nombreOperador']), $border,0, 'L');

        $pdf->setY(55);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode('ORIGEN:   '.$rsSql['root'][0]['Origen']), $border,0, 'L');

        $pdf->setY(55);
        $pdf->SetX(100);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode('DESTINO:   '.$rsSql['root'][0]['Destino']), $border,0, 'C');

        if ($rsSql['root'][0]['cambioRuta']!=null) {
            $ruta=$rsSql['root'][0]['cambioRuta'];
        }else{
            $ruta=$rsSql['root'][0]['rutaAutorizada'];
        }

        $pdf->setY(75);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode('RUTA AUTORIZADA: '.substr($ruta,0,30)), $border,0, 'L');
        $pdf->setY(82);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,30,50)), $border,0, 'L');
        $pdf->setY(89);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,80,50)), $border,0, 'L');
        $pdf->setY(96);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,130,50)), $border,0, 'L');
        $pdf->setY(103);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,180,50)), $border,0, 'L');
        $pdf->setY(110);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,230,50)), $border,0, 'L');
        $pdf->setY(117);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,280,50)), $border,0, 'L');
        $pdf->setY(124);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,330,50)), $border,0, 'L');
        $pdf->setY(131);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,380,50)), $border,0, 'L');
        $pdf->setY(138);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,430,50)), $border,0, 'L');
        $pdf->setY(145);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 15);
        $pdf->Cell(0,10,utf8_decode(substr($ruta,480,50)), $border,0, 'L');

        $pdf->setY(150);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode('FECHA SALIDA:   '.$rsSql['root'][0]['fechaSalida']), $border,0, 'L');

        $pdf->setY(150);
        $pdf->SetX(100);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode('HORA SALIDA:   '.substr($rsSql['root'][0]['horaSalida'],11,10)), $border,0, 'C');

        $pdf->setY(160);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode('INSTRUCCIONES ESPECIALES: '.substr($rsSql['root'][0]['observaciones'],0,30)), $border,0, 'L');

        $pdf->setY(170);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode(substr($rsSql['root'][0]['observaciones'],30,70)), $border,0, 'L');

        $pdf->setY(180);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode(substr($rsSql['root'][0]['observaciones'],100,70)), $border,0, 'L');

        $pdf->setY(190);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0,10,utf8_decode(substr($rsSql['root'][0]['observaciones'],170,70)), $border,0, 'L');



        $pdf->setY(220);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0,10,utf8_decode('AUTORIZA: '.$rsSql['root'][0]['autoriza']), $border,0, 'L');

        $pdf->setY(220);
        $pdf->SetX(90);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0,10,utf8_decode('ACEPTA: '.$rsSql['root'][0]['nombreOperador']), $border,0, 'C');

        $pdf->setY(235);
        $pdf->SetX(20);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0,10,utf8_decode('PRE-RETORNO (este dato puede cambiar durante el viaje): '.$rsSql['root'][0]['preRetorno']), $border,0, 'L');



        // output file
        $pdf->Output('HOJA DE INSTRUCCIONES '.$rsSql['root'][0]['nombreOperador'].'.pdf', 'I');



        
        } else {
          echo json_encode(array('success'=>false, 'errorMessage'=>$_SESSION['error_sql']." <br> ".$sqlGetDataStr));
        }
    }
?>