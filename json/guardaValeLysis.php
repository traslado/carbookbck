<?php

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("trGastosViajeTractor.php");


    switch($_REQUEST['trConsultaHdn']){
        case 'GUARDAVALE':
            GUARDAVALE();
            break; 
        case 'CALCULOMES':
            CALCULOMES();
            break; 
        case 'GUARDACALCULO':
            GUARDACALCULO();
            break;
        case 'SUMGRATIFICACIONES':
            SUMGRATIFICACIONES();
            break;
        case 'COMBOREPARACIONES':
            COMBOREPARACIONES();
            break;        
        default:
            echo '';

    }
    

    function GUARDAVALE(){ 
        
        $sqlTractor="SELECT  * from trviajestractorestbl ".
                    "where idTractor='".$_REQUEST['tractor']."'".
                    "and fechaevento=(select max(fechaEvento) from trviajestractorestbl where idTractor='".$_REQUEST['tractor']."' and claveMovimiento!='VA');";
        $rsTractor=fn_ejecuta_query($sqlTractor);

        $sqlFolio="SELECT folio+1 as folio from trfoliostbl
                    WHERE tipoDocumento='VD'
                    AND centroDistribucion='".$_REQUEST['ciaSesVal']."'";
        $rsFolio=fn_ejecuta_query($sqlFolio);

        $sqlGuardaLts="INSERT INTO `trconceptosdieselbombastbl` (`idtractor`, `idViajeTractor`, `centroDistribucion`,`folio`, `litrosViaje`, `polizaPendiente`, `autTaller`, `acapulcoOax`, `regresoCargados`, `resguardos`, `postura`, `1.5Mes`, `libMexiquense`, `litrosTotal`, `fechaRegistro`) 
        VALUES ('".replaceEmptyNull($_REQUEST['tractor'])."', '".$rsTractor['root'][0]['idViajeTractor']."','".$_REQUEST['ciaSesVal']."','".$rsFolio['root'][0]['folio']."', ".replaceEmptyNull("'".$_REQUEST['viaje']."'").", ".replaceEmptyNull("'".$_REQUEST['poliza']."'").", ".replaceEmptyNull("'".$_REQUEST['taller']."'").", ".replaceEmptyNull("'".$_REQUEST['acapulco']."'").", ".replaceEmptyNull("'".$_REQUEST['regreso']."'").", ".replaceEmptyNull("'".$_REQUEST['resguardo']."'").", ".replaceEmptyNull("'".$_REQUEST['postura']."'").", ".replaceEmptyNull("'".$_REQUEST['mes']."'").", ".replaceEmptyNull("'".$_REQUEST['mex']."'").", ".replaceEmptyNull("'".$_REQUEST['totales']."'").",NOW())";
        $rsOperador=fn_ejecuta_query($sqlGuardaLts);

        $updFolio="UPDATE trfoliostbl set folio=".$rsFolio['root'][0]['folio']." WHERE tipoDocumento='VD' ".
                    "AND centroDistribucion='".$_REQUEST['ciaSesVal']."'";
        fn_ejecuta_query($updFolio);

        $sqlDatosVale="SELECT  * from trconceptosdieselbombastbl a, catractorestbl b ".
                        " where a.idTractor='".$_REQUEST['tractor']."'".
                        " and folio='".$rsFolio['root'][0]['folio']."'".
                        " and a.idtractor=b.idtractor";
        $rsVale=fn_ejecuta_query($sqlDatosVale);
        echo json_encode($rsVale);

        
    }

    function CALCULOMES(){        
        $sqlSumEst="SELECT a.rendimiento, c.mesAfectacion,c.litros, (SELECT sum(b.importe) 
                                            from trviajestractorestbl a, trgastosviajetractortbl b
                                            where a.idTractor='".$_REQUEST['tractor']."'
                                            and month(a.fechaevento)='".$_REQUEST['mes']."'
                                            and year(a.fechaEvento)='2021'
                                            and a.idviajeTractor=b.idviajetractor
                                            and b.concepto=2312 )as ticketCar,
                                        (SELECT sum(b.litros) 
                                            from trviajestractorestbl a, trgastosviajetractortbl b
                                            where a.idTractor='".$_REQUEST['tractor']."'
                                            and month(a.fechaevento)='".$_REQUEST['mes']."'
                                            and year(a.fechaEvento)='2021'
                                            and a.idviajeTractor=b.idviajetractor
                                            and b.concepto in (9000) )as litrosEfectivo,
                                        (SELECT  sum(a.kilometrosComprobados) 
                                            from trviajestractorestbl a
                                            where a.idTractor='".$_REQUEST['tractor']."'
                                            and month(a.fechaevento)='".$_REQUEST['mes']."'
                                            and year(a.fechaEvento)='2021') as KmsPagados           
                    from  catractorestbl a, trviajestractorestbl b, trgastosviajetractortbl c
                    where a.idtractor='".$_REQUEST['tractor']."'
                    and a.idtractor=b.idtractor
                    and b.idviajeTractor=c.idviajeTractor
                    and c.clavemovimiento='VG'
                    and c.mesAfectacion='".$_REQUEST['mes']."'";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }

    function GUARDACALCULO(){ 
        
        $sqlTractor="SELECT  * from trviajestractorestbl ".
                    "where idTractor='".$_REQUEST['tractor']."'".
                    "and fechaevento=(select max(fechaEvento) from trviajestractorestbl where idTractor='".$_REQUEST['tractor']."' and claveMovimiento!='VA');";
        $rsTractor=fn_ejecuta_query($sqlTractor);

        $sqlGuardaLts="INSERT INTO trgastosviajetractortbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, claveMovimiento, litros, usuario, ip, subTotal) ".
        "VALUES ('".$rsTractor['root'][0]['idViajeTractor']."', '1000', '".$_REQUEST['ciaSesVal']."', '1', NOW(), '0', '".$_REQUEST['mes']."', '0', 'RD', '".$_REQUEST['litros']."', '132', '10.0.0.1', NULL)";
        $rsOperador=fn_ejecuta_query($sqlGuardaLts);

        echo json_encode($rsOperador);
        
    }
    
?>
