<?php
	session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("catIVA.php");

    setlocale(LC_TIME, 'es_MX.utf8');

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
	
    switch($_REQUEST['faFacturasActionHdn']) {
        case 'getFacturas':
            echo json_encode(getFacturas());
            break;
        case 'getDetalleFactura':
            echo json_encode(getDetalleFactura());
            break;
        case 'getNotasCredito':
            getNotasCredito();
            break;
        case 'getDetalleNotaCredito':
            getDetalleNotaCredito();
            break;
        case 'getNextFolio':
            echo json_encode(getNextFolio($_REQUEST['faFacturasCompaniaHdn']));
            break;
        case 'getNextNumeroNota':
            echo json_encode(getNextNumeroNota($_REQUEST['faFacturasCompaniaHdn']));
            break;
        case 'addFactura':
            echo json_encode(addFactura($_REQUEST['faFacturasCompaniaHdn'],$_REQUEST['faFacturasDistribuidorHdn'],
                                        $_REQUEST['faFacturasFechaInicialTxt'], $_REQUEST['faFacturasFechaFinalTxt'],
                                        $_REQUEST['faFacturasFolioTxt']));
            break;
        case 'addFacturaMasiva':
            addFacturaMasiva();
            break;
        case 'cancelarFactura':
            cancelarFactura();
            break;
        case 'generarArchivoCOFIDI':
            echo json_encode(generarArchivoCOFIDI($_REQUEST['faFacturasCompaniaHdn'],$_REQUEST['faFacturasDistribuidorHdn'],
                                        $_REQUEST['faFacturasFolioTxt']));
            break;
        case 'addNotaCredito':
            echo json_encode(addNotaCredito($_REQUEST['faFacturasCompaniaHdn'],$_REQUEST['faFacturasNumeroNotaTxt'],$_REQUEST['faFacturasDistribuidorHdn'],
                                        $_REQUEST['faFacturasFechaInicialTxt'], $_REQUEST['faFacturasFechaFinalTxt'],$_REQUEST['faFacturasConceptoHdn']));
            break;
        case 'addNotaCreditoMasiva':
            addNotaCreditoMasiva();
            break;
        case 'cancelarNotaCredito':
            cancelarNotaCredito();
            break;
        case 'printLayout':
            printLayout();
            break;
        default:
            echo '';
    }

    function getFacturas(){
        $lsWhereStr = "WHERE co.compania = fa.compania ".
                      "AND dc.distribuidorCentro = fa.distribuidor ".
                      "AND dc.direccionFiscal = di.direccion ".
                      "AND di.idColonia = col.idColonia ".
                      "AND col.idMunicipio = mun.idMunicipio ".
                      "AND mun.idEstado =  edo.idEstado";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasCompaniaHdn'], "fa.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasDistribuidorHdn'], "fa.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFolioTxt'], "fa.folio", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFechaTxt'], "fa.fechaFacturacion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetFacturas = "SELECT fa.*, co.descripcion AS descripcionCompania, dc.descripcionCentro, dc.rfc, di.calleNumero, col.colonia, mun.municipio, edo.estado, ".
                          "(SELECT SUM(fd.importe) FROM faFacturasDetalleTbl fd WHERE fd.folio = fa.folio and fd.compania = fa.compania) as subTotal, ".
                          "(FORMAT((importe/1.16) * 0.16, 2)) AS importeIVA ".
                          "FROM faFacturasTbl fa, caCompaniasTbl co, caDistribuidoresCentrosTbl dc, ".
                          "caDireccionesTbl di, caColoniasTbl col, caMunicipiosTbl mun, caEstadosTbl edo ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetFacturas);

        for ($i=0; $i < sizeof($rs['root']); $i++) {
            $rs['root'][$i]['descCompania'] = $rs['root'][$i]['compania']." - ".$rs['root'][$i]['descripcionCompania'];
            $rs['root'][$i]['descDist'] = $rs['root'][$i]['distribuidor']." - ".$rs['root'][$i]['descripcionCentro'];
        }

        return $rs;
    }

    function getDetalleFactura(){
        $lsWhereStr = "WHERE fd.compania = fa.compania ".
                        "AND fd.folio = fa.folio";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasCompaniaHdn'], "fd.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasVinTxt'], "fd.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFolioTxt'], "fd.folio", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFechaTxt'], "fd.fechaFacturacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFechaInicioTxt'], "fd.fechaFacturacion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFechaFinTxt'], "fd.fechaFacturacion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetFacturas = "SELECT fd.vin, fd.fechaFacturacion, fd.importe, ".
                            "(SELECT 1 FROM faCargaTCOCanceladasTbl cc WHERE cc.vin = fd.vin AND cc.compania = fd.compania ".
                                "AND cc.distribuidor = fa.distribuidor) AS estatus ".
                          "FROM faFacturasDetalleTbl fd, faFacturasTbl fa ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetFacturas);

        for ($i=0; $i < sizeof($rs['root']); $i++) {
            if($rs['root'][$i]['estatus'] == '1') {
                $rs['root'][$i]['estatus'] = 'CANCELADA';
            }
        }

        return $rs;
    }

    function getNotasCredito(){
        $lsWhereStr = "WHERE co.compania = nc.compania ".
                      "AND dc.distribuidorCentro = nc.distribuidor ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasCompaniaHdn'], "nc.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasDistribuidorHdn'], "nc.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFolioTxt'], "nc.folio", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFechaTxt'], "nc.fechaFacturacion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetFacturas = "SELECT nc.*, co.descripcion AS descripcionCompania, dc.descripcionCentro ".
                          "FROM faNotasCreditoTbl nc, caCompaniasTbl co, caDistribuidoresCentrosTbl dc ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetFacturas);

        for ($i=0; $i < sizeof($rs['root']); $i++) {
            $rs['root'][$i]['descCompania'] = $rs['root'][$i]['compania']." - ".$rs['root'][$i]['descripcionCompania'];
            $rs['root'][$i]['descDist'] = $rs['root'][$i]['distribuidor']." - ".$rs['root'][$i]['descripcionCentro'];
        }

        echo json_encode($rs);
    }

    function getDetalleNotaCredito(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasCompaniaHdn'], "nd.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasVinTxt'], "nd.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasNumeroNotaTxt'], "nd.numeroNota", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturasFechaTxt'], "nd.fechaFacturacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetNotasCreditoDetalle = "SELECT nd.vin, nd.fechaFacturacion ".
                                        "FROM faNotasCreditoDetalleTbl nd ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetNotasCreditoDetalle);

        echo json_encode($rs);
    }

    function getNextFolio($compania){

        $sqlGetNextFolio = "SELECT COALESCE((SELECT MAX(folio)+1 FROM faFacturasTbl ".
                            "WHERE compania = '".$compania."'), 1) AS folio ";

        $rs = fn_ejecuta_query($sqlGetNextFolio);
            
        return $rs;
    }

    function getNextNumeroNota($compania){

        $sqlGetNextFolio = "SELECT COALESCE((SELECT MAX(numeroNota)+1 FROM faNotasCreditoTbl ".
                            "WHERE compania = '".$compania."'), 1) AS numeroNota ";

        $rs = fn_ejecuta_query($sqlGetNextFolio);
            
        return $rs;
    }

    function addFactura($compania, $distribuidor, $fechaInicial, $fechaFinal, $folio){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($compania == ""){
            $e[] = array('id'=>'faFacturasCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($distribuidor == ""){
            $e[] = array('id'=>'faFacturasDistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($fechaInicial == ""){
            $e[] = array('id'=>'faFacturasFechaInicialTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($fechaFinal == ""){
            $e[] = array('id'=>'faFacturasFechaFinalTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($folio == ""){
            $e[] = array('id'=>'faFacturasFolioTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            //REVISA QUE NO SE HAYA FACTURADO YA ESTE PERIODO
            $sqlCheckFacturacion = "SELECT 1 FROM faFacturasTbl WHERE compania = '".$compania."' ".
                                    "AND distribuidor = '".$distribuidor."' ".
                                    "AND fechaFacturacion = '".$fechaFinal."' ".
                                    "AND estatus = 'D'";

            $rs = fn_ejecuta_query($sqlCheckFacturacion);

            if(sizeof($rs['root']) > 0){
                $a['success'] = false;
                $a['errorMessage'] = "Periodo ya facturado para el distribuidor ".$distribuidor;
            }
        }

        if($a['success']){
            $data = getIVA();
            if(sizeof($data['root']) > 0){
                $iva = 1 + (floatval($data['root'][0]['importe']) / 100);
            } else {
                $a['success'] = false;
                $a['errorMessage'] = getIVAErrorMsg();
            }
        }
        
        if($a['success']){
            $sqlGetFacturacionTCO = "SELECT cd.vin, cd.fechaFacturacion, ".
                                    "(SELECT cc.fechaFacturacion FROM faCargaTCOCanceladasTbl cc WHERE cc.compania = cd.compania ".
                                        "AND cc.distribuidor = cd.distribuidor AND cc.vin = cd.vin) AS cancelada ".
                                    "FROM faCargaTCOTbl cd ".
                                    "WHERE cd.compania = '".$compania."' ".
                                    "AND cd.distribuidor = '".$distribuidor."' ".
                                    "AND cd.fechaFacturacion >= '".$fechaInicial."' ".
                                    "AND cd.fechaFacturacion <= '".$fechaFinal."' ".
                                    "AND cd.estatusChequeBonificacion = 'D'";

            $rsTCO = fn_ejecuta_query($sqlGetFacturacionTCO);

            if(sizeof($rsTCO['root']) <= 0){
                $a['success'] = false;
                $a['errorMessage'] = "No existen unidades para facturar en el periodo seleccionado";
            } else {
                $seriesArr = array();
                $seriesStr = "";
                $importe = 0.00;
                $unidadesError = array();
                $serieNoExiste = array();
                $numeroUnidades = sizeof($rsTCO['root']);

                //CALCULO DEL IMPORTE POR LA SERIE DE LA UNIDAD
                foreach ($rsTCO['root'] as $unidad) {
                    $serie = substr($unidad['vin'], -8, -6);
                    if(!in_array($serie, $seriesArr)){
                        array_push($seriesArr, $serie);
                    }

                }

                for ($i=0; $i < sizeof($seriesArr); $i++) { 
                    if($i != 0){
                        $seriesStr .= ",";
                    }

                    $seriesStr .= "'".$seriesArr[$i]."'";
                }

                $sqlGetSeries = "SELECT serie, importe FROM caSeriesTbl WHERE serie IN (".$seriesStr.")";
                
                $rsSeries = fn_ejecuta_query($sqlGetSeries);

                foreach ($rsTCO['root'] as &$unidad) {
                    $conSerie = false;
                    foreach ($rsSeries['root'] as $serie) {
                        if(substr($unidad['vin'], -8, -6) == $serie['serie']){
                            $importe += floatval($serie['importe']);
                            $unidad['importe'] = $serie['importe'];
                            $conSerie = true;
                        }
                    }

                    if(!$conSerie){
                        $a['success'] = false;
                        array_push($serieNoExiste, substr($unidad['vin'], -8, -6));
                    }
                }

                $importe *= $iva;
               
                if($a['success']){
                     //////////////////////////////////////////////////////////
                    //CREACION DE LA FACTURA

                    $sqlAddFactura = "INSERT INTO faFacturasTbl (compania, folio, tipo, distribuidor, fechaFacturacion, ".
                                        "estatus, numeroUnidades, importe) VALUE (".
                                        "'".$compania."',".
                                        "'".$folio."',".
                                        "'F',".
                                        "'".$distribuidor."',".
                                        "'".$fechaFinal."',".
                                        "'D',".
                                        $numeroUnidades.",".
                                        sprintf ("%.2f", $importe).")";

                     fn_ejecuta_query($sqlAddFactura);

                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        //GENERACION DEL DETALLE DE LA FACTURA
                        foreach ($rsTCO['root'] as &$unidad) {
                            $sqlAddDetalleFactura = "INSERT INTO faFacturasDetalleTbl (compania, folio, vin, importe, fechaFacturacion) VALUES (".
                                                        "'".$compania."',".
                                                        "'".$folio."',".
                                                        "'".$unidad['vin']."',".
                                                        $unidad['importe'].",".
                                                        "'".$unidad['fechaFacturacion']."')";

                            fn_ejecuta_query($sqlAddDetalleFactura);
                            
                            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){

                                //SI ESTA CANCELADA SE DEBE GENERAR NOTA DE CREDITO AUTOMATICAMENTE
                                if($unidad['cancelada'] != ''){
                                    $rsNota = getNextNumeroNota($compania);
                                    $data = addNotaCredito($compania, $rsNota['root'][0]['numeroNota'], $distribuidor, $fechaInicial, $fechaFinal, 'F');   

                                    if(!$data['success']){
                                        $a['success'] = false;
                                        $a['errorMessage'] = $data['errorMessage'];
                                        $e = $data['errors'];
                                    }
                                }
                            } else {
                                $a['success'] = false;
                                array_push($unidadesError, $unidad['vin']);   
                            }
                        }

                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddFactura;
                    }
                } else {
                    $a['errorMessage'] = "Error al obtener el importe de las series: ";

                    foreach ($serieNoExiste as $serie) {
                        $a['errorMessage'] .= "<br>".$serie;
                    }
                }
            }
        }

        if(!$a['success'] && sizeof($unidadesError) > 0){
            $a['errorMessage'] .= "<br><br>Unidades no insertadas: <br>";
            foreach ($unidadesError as $unidad) {
                $a['errorMessage'] .= $unidad."<br>";
            }
        }

        if($a['success']){
            $a['successMessage'] = getFacturaSuccessMsg();
        }

        $a['errors'] = $e;
        return $a;
    }

    function addFacturaMasiva(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturasCompaniaHdn'] == ""){
            $e[] = array('id'=>'faFacturasCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasFechaInicialTxt'] == ""){
            $e[] = array('id'=>'faFacturasFechaInicialTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasFechaFinalTxt'] == ""){
            $e[] = array('id'=>'faFacturasFechaFinalTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasFolioTxt'] == ""){
            $e[] = array('id'=>'faFacturasFolioTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            //Obtiene los distribuidores
            $sqlGetDistribuidores = "SELECT cd.distribuidor ".
                                    "FROM faCargaTCOTbl cd ".
                                    "WHERE cd.compania = '".$_REQUEST['faFacturasCompaniaHdn']."' ".
                                    "AND cd.fechaFacturacion >= '".$_REQUEST['faFacturasFechaInicialTxt']."' ".
                                    "AND cd.fechaFacturacion <= '".$_REQUEST['faFacturasFechaFinalTxt']."' ".
                                    "AND cd.estatusChequeBonificacion = 'D' ".
                                    "GROUP BY distribuidor ";

            $rsDist = fn_ejecuta_query($sqlGetDistribuidores);

            if(sizeof($rsDist['root']) > 0){
                $folio = $_REQUEST['faFacturasFolioTxt'];

                foreach ($rsDist['root'] as $distribuidor) {
                    $data = addFactura($_REQUEST['faFacturasCompaniaHdn'], $distribuidor['distribuidor'], $_REQUEST['faFacturasFechaInicialTxt'], 
                                        $_REQUEST['faFacturasFechaFinalTxt'], $folio);

                    if($data['success']){
                        $nextFolio = getNextFolio($_REQUEST['faFacturasCompaniaHdn']);

                        if(sizeof($nextFolio['root']) > 0){
                            $folio = $nextFolio['root'][0]['folio'];
                        } else {
                            $a['success'] = false;
                            $a['errorMessage'] = 'Error al obtener el folio consecutivo';
                            break;
                        }
                    } else {
                        $a['success'] = false;
                        if($a['errorMessage'] != "")
                            $a['errorMessage'] .= "<br>";

                        $a['errorMessage'] .= $data['errorMessage'];
                    }
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = "No existen unidades para facturar en el periodo seleccionado";
            }
        }

        if($a['success']){
            $a['successMessage'] = getFacturaMasivaSuccessMsg();
        }
        
        $a['errors'] = $e;
        echo json_encode($a);
    }

    function cancelarFactura(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturasCompaniaHdn'] == ""){
            $e[] = array('id'=>'faFacturasCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasFolioTxt'] == ""){
            $e[] = array('id'=>'faFacturasFolioTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            //SE ELIMINA EL DETALLE (LAS UNIDADES) DE LA FACTURA
            $sqlDelDetalleFactura = "DELETE FROM faFacturasDetalleTbl ".
                                    "WHERE compania = '".$_REQUEST['faFacturasCompaniaHdn']."' ".
                                    "AND folio = '".$_REQUEST['faFacturasFolioTxt']."'";

            fn_ejecuta_query($sqlDelDetalleFactura);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                //SE ACTUALIZA EL ESTATUS A "C"
                $sqlCancelarFactura = "UPDATE faFacturasTbl SET estatus = 'C', numeroUnidades = 0, importe = 0.00 ".
                                      "WHERE compania = '".$_REQUEST['faFacturasCompaniaHdn']."' ".
                                        "AND folio = '".$_REQUEST['faFacturasFolioTxt']."'";

                fn_ejecuta_query($sqlCancelarFactura);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['successMessage'] = getFacturaCanceladaSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlCancelarFactura;
                }

            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDelDetalleFactura;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }

    function generarArchivoCOFIDI($compania, $distribuidor, $folio){
        $a = array();

        $dirArchivo = "COFIDI/".strftime("%Y_%B_").$distribuidor;

        $archivo = fopen($_SERVER['DOCUMENT_ROOT'].$dirArchivo, "w") or die("Unable to open file!");

        //LINEA 1
        $linea1 = 'E01' . $folio . str_repeat(' ', 15) . date("Y-m-d H:i:s")."PAGO EN UNA SOLA EXHIBICION".str_repeat(' ', 123)."40500.00".
                    str_repeat(' ', 6)."0.00".str_repeat(' ', 160)."46980.00".str_repeat(' ', 6)."NO IDENTIFICADO".str_repeat(' ', 15)."24";

        fwrite($archivo, $linea1);


        //CIERRA Y MANDA A LA DIRECCION DEL ARCHIVO
        fclose($archivo);
        header('Location: /'.$dirArchivo);
        return $a;
    }

    function addNotaCredito($compania, $numeroNota, $distribuidor, $fechaInicial, $fechaFinal, $concepto){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($compania == ""){
            $e[] = array('id'=>'faFacturasCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($numeroNota == ""){
            $e[] = array('id'=>'faFacturasNumeroNotaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($distribuidor == ""){
            $e[] = array('id'=>'faFacturasDistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($fechaInicial == ""){
            $e[] = array('id'=>'faFacturasFechaInicialTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($fechaFinal == ""){
            $e[] = array('id'=>'faFacturasFechaFinalTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($concepto == ""){
            $e[] = array('id'=>'faFacturasConceptoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            //REVISA QUE NO SE HAYA GENERADO NOTA DE CREDITO YA ESTE PERIODO
            $sqlCheckNotaCredito = "SELECT 1 FROM faNotasCreditoTbl WHERE compania = '".$compania."' ".
                                    "AND distribuidor = '".$distribuidor."' ".
                                    "AND fechaElaboracion = '".$fechaFinal."' ".
                                    "AND estatus = 'D'";

            $rs = fn_ejecuta_query($sqlCheckNotaCredito);

            if(sizeof($rs['root']) > 0){
                $a['success'] = false;
                $a['errorMessage'] = "Ya se genero una nota de crédito en este periodo para el distribuidor ".$distribuidor;
            }
        }

        if($a['success']){
            $data = getIVA();
            if(sizeof($data['root']) > 0){
                $iva = 1 + (floatval($data['root'][0]['importe']) / 100);
            } else {
                $a['success'] = false;
                $a['errorMessage'] = getIVAErrorMsg();
            }
        }
        
        if($a['success']){
            $sqlGetFacturacionTCO = "SELECT cd.vin, cd.fechaFacturacion ".
                                    "FROM faCargaTCOCanceladasTbl cd ".
                                    "WHERE cd.compania = '".$compania."' ".
                                    "AND cd.distribuidor = '".$distribuidor."' ".
                                    "AND cd.fechaFacturacion >= '".$fechaInicial."' ".
                                    "AND cd.fechaFacturacion <= '".$fechaFinal."' ".
                                    "AND cd.estatusChequeBonificacion = 'D'";

            $rsTCO = fn_ejecuta_query($sqlGetFacturacionTCO);

            
            if(sizeof($rsTCO['root']) <= 0){
                $a['success'] = false;
                $a['errorMessage'] = "No existen unidades para generar una nota de credito en el periodo seleccionado";
            } else {
                $seriesArr = array();
                $seriesStr = "";
                $importe = 0.00;
                $unidadesError = array();
                $serieNoExiste = array();
                $numeroUnidades = sizeof($rsTCO['root']);

                //CALCULO DEL IMPORTE POR LA SERIE DE LA UNIDAD
                foreach ($rsTCO['root'] as $unidad) {
                    $serie = substr($unidad['vin'], -8, -6);
                    if(!in_array($serie, $seriesArr)){
                        array_push($seriesArr, $serie);
                    }

                }

                for ($i=0; $i < sizeof($seriesArr); $i++) { 
                    if($i != 0){
                        $seriesStr .= ",";
                    }

                    $seriesStr .= "'".$seriesArr[$i]."'";
                }

                $sqlGetSeries = "SELECT serie, importe FROM caSeriesTbl WHERE serie IN (".$seriesStr.")";
                
                $rsSeries = fn_ejecuta_query($sqlGetSeries);

                foreach ($rsTCO['root'] as &$unidad) {
                    $conSerie = false;
                    foreach ($rsSeries['root'] as $serie) {
                        if(substr($unidad['vin'], -8, -6) == $serie['serie']){
                            $importe += floatval($serie['importe']);
                            $unidad['importe'] = $serie['importe'];
                            $conSerie = true;
                        }
                    }

                    if(!$conSerie){
                        $a['success'] = false;
                        array_push($serieNoExiste, substr($unidad['vin'], -8, -6));
                    }
                }

                $importe *= $iva;
               
                if($a['success']){
                     //////////////////////////////////////////////////////////
                    //CREACION DE LA FACTURA

                    $sqlAddNotaCredito = "INSERT INTO faNotasCreditoTbl (compania, numeroNota, distribuidor, concepto, estatus, numeroUnidades, ".
                                            "importe, inicioPeriodo, finPeriodo, fechaElaboracion) VALUE (".
                                            "'".$compania."',".
                                            "'".$numeroNota."',".
                                            "'".$distribuidor."',".
                                            "'".$concepto."',".
                                            "'D',".
                                            $numeroUnidades.",".
                                            sprintf ("%.2f", $importe).",".
                                            "'".$fechaInicial."',".
                                            "'".$fechaFinal."',".
                                            "'".$fechaFinal."')";

                    fn_ejecuta_query($sqlAddNotaCredito);

                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        //GENERACION DEL DETALLE DE LA FACTURA
                        foreach ($rsTCO['root'] as &$unidad) {

                            $sqlAddDetalleNotaCredito = "INSERT INTO faNotasCreditoDetalleTbl (compania,numeroNota,vin,importe,fechaFacturacion) VALUES (".
                                                        "'".$compania."',".
                                                        "'".$numeroNota."',".
                                                        "'".$unidad['vin']."',".
                                                        $unidad['importe'].",".
                                                        "'".$unidad['fechaFacturacion']."')";

                            fn_ejecuta_query($sqlAddDetalleNotaCredito);
                            
                            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){

                            } else {
                                $a['success'] = false;
                                array_push($unidadesError, $unidad['vin']);   
                            }
                        }

                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddFactura;
                    }
                } else {
                    $a['errorMessage'] = "Error al obtener el importe de las series: ";

                    foreach ($serieNoExiste as $serie) {
                        $a['errorMessage'] .= "<br>".$serie;
                    }
                }
            }
        }

        if(!$a['success'] && sizeof($unidadesError) > 0){
            $a['errorMessage'] .= "<br><br>Unidades no insertadas: <br>";
            foreach ($unidadesError as $unidad) {
                $a['errorMessage'] .= $unidad."<br>";
            }
        }

        if($a['success']){
            $a['successMessage'] = getNotaCreditoSuccessMsg();
        }

        $a['errors'] = $e;
        return $a;
    }

    function addNotaCreditoMasiva(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturasCompaniaHdn'] == ""){
            $e[] = array('id'=>'faFacturasCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasFechaInicialTxt'] == ""){
            $e[] = array('id'=>'faFacturasFechaInicialTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasFechaFinalTxt'] == ""){
            $e[] = array('id'=>'faFacturasFechaFinalTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasNumeroNotaTxt'] == ""){
            $e[] = array('id'=>'faFacturasNumeroNotaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasConceptoHdn'] == ""){
            $e[] = array('id'=>'faFacturasConceptoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            //Obtiene los distribuidores
            $sqlGetDistribuidores = "SELECT cc.distribuidor ".
                                    "FROM faCargaTCOCanceladasTbl cc ".
                                    "WHERE cc.compania = '".$_REQUEST['faFacturasCompaniaHdn']."' ".
                                    "AND cc.fechaFacturacion >= '".$_REQUEST['faFacturasFechaInicialTxt']."' ".
                                    "AND cc.fechaFacturacion <= '".$_REQUEST['faFacturasFechaFinalTxt']."' ".
                                    "AND cc.estatusChequeBonificacion = 'D' ".
                                    "GROUP BY distribuidor ";

            $rsDist = fn_ejecuta_query($sqlGetDistribuidores);

            if(sizeof($rsDist['root']) > 0){
                $numeroNota = $_REQUEST['faFacturasNumeroNotaTxt'];

                foreach ($rsDist['root'] as $distribuidor) {
                    $data = addNotaCredito($_REQUEST['faFacturasCompaniaHdn'], $numeroNota, $distribuidor['distribuidor'], 
                                        $_REQUEST['faFacturasFechaInicialTxt'], $_REQUEST['faFacturasFechaFinalTxt'], $_REQUEST['faFacturasConceptoHdn']);
                    if($data['success']){
                        $nextFolio = getNextFolio($_REQUEST['faFacturasCompaniaHdn']);

                        if(sizeof($nextFolio['root']) > 0){
                            $numeroNota = $nextFolio['root'][0]['folio'];
                        } else {
                            $a['success'] = false;
                            $a['errorMessage'] = 'Error al obtener el folio consecutivo';
                            break;
                        }
                    } else {
                        $a['success'] = false;
                        if($a['errorMessage'] != "")
                            $a['errorMessage'] .= "<br>";

                        $a['errorMessage'] .= $data['errorMessage'];
                    }
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = "No existen unidades para generarles una nota de credito en el periodo seleccionado";
            }
        }

        if($a['success']){
            $a['successMessage'] = getNotaCreditoMasivaSuccessMsg();
        }
        
        $a['errors'] = $e;
        echo json_encode($a);
    }

    function cancelarNotaCredito(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturasCompaniaHdn'] == ""){
            $e[] = array('id'=>'faFacturasCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturasNumeroNotaTxt'] == ""){
            $e[] = array('id'=>'faFacturasNumeroNotaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            //SE ELIMINA EL DETALLE (LAS UNIDADES) DE LA FACTURA
            $sqlDelDetalleFactura = "DELETE FROM faNotasCreditoDetalleTbl ".
                                    "WHERE compania = '".$_REQUEST['faFacturasCompaniaHdn']."' ".
                                    "AND numeroNota = '".$_REQUEST['faFacturasNumeroNotaTxt']."'";

            fn_ejecuta_query($sqlDelDetalleFactura);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                //SE ACTUALIZA EL ESTATUS A "C"
                $sqlCancelarFactura = "UPDATE faNotasCreditoTbl SET estatus = 'C', numeroUnidades = 0, importe = 0.00 ".
                                      "WHERE compania = '".$_REQUEST['faFacturasCompaniaHdn']."' ".
                                        "AND numeroNota = '".$_REQUEST['faFacturasNumeroNotaTxt']."'";

                fn_ejecuta_query($sqlCancelarFactura);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['successMessage'] = getNotaCreditoCanceladaSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlCancelarFactura;
                }

            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDelDetalleFactura;
            }
        }
        
        $a['errors'] = $e;
        echo json_encode($a);
    }

    function printLayout(){
        $rsFacturas =  getFacturas();
        $rsDetalle = getDetalleFactura();

        $rs = $rsFacturas['root'][0];
        $rsDetalle = $rsDetalle['root'];

        //CÓDIGO DURO -------------------------------------------------
        $pais = 'MEXICO';
        $retencion = '0.00';
        $impuesto = 'IVA';
        $porcentaje = '16.00';
        $unidad = 'UNID';
        $condicionesPago = 'PAGO EN UNA SOLA EXHIBICION';
        $metodoPago = 'NO IDENTIFICADO';
        $descuento = '';
        $diasVencimiento = '30';
        $moneda = 'MXP';
        $denominacion = 'PESOS';
        $tipoDeCambio = '1.0000';
        $chtipoDocumento = 'TES'; //Chrysler Tipo Documento TES - Carrier with special movements
        $cantidadUnidades = '1'; //Creo que siempre es 1
        $ea5Concepto = 'CUOTA POR RECEPCION, ALMACENAMIENTO, CUSTODIA Y ADMINISTRACION DE SUS UNIDADES Con los siguientes numeros de serie:';
        $date1 = '01/05/2015';
        $date2 = '31/05/2015';
        $ea5Concepto2 = 'PERIODO DEL '.$date1.' AL '.$date2;
        //-------------------------------------------------------------
        $centavos =  floatval($rs['importe']) - floor(floatval($rs['importe']));

        if($centavos < 10){
            $centavos = '0'.$centavos;
        }
        $centavos = $centavos.'/100';

        //Total numero en palabra EJM (un mil doscientos treinta y tres PESOS 24/100 M.N.)
        $totalLetra = '('.numToWord($rs['importe']).' '.$denominacion.' '.$centavos.' M.N.)';

        $E01 = array('E01', $rs['folio'], date('Y-m-d h:m:s'), 
               $condicionesPago, $rs['subTotal'], $descuento, 
               $rs['motivoDescuento'], $rs['importe'], $metodoPago);

        $E01_pos = array(0, 3, 23, 42, 192, 206, 220, 370, 384);

        $E02 = array('E02', $rs['distribuidor'], $rs['rfc'], $rs['descripcionCentro']);
        $E02_pos = array(0, 3, 23, 36);

        
        $E03 = array('E03', $rs['calleNumero'], $rs['colonia'], $rs['municipio'], $rs['municipio'], $rs['estado'], $pais, $rs['cp']);
        $E03_pos = array(0, 3, 123, 173, 273, 323, 373, 423);

        $E04 = array('E04', $retencion, $rs['importeIVA']);
        $E04_pos = array(0, 3, 17);

        $E05 = array('E05', $impuesto, $rs['importeIVA'], $porcentaje);
        $E05_pos = array(0, 3, 8, 22);

        $E06 = array('E06', $impuesto, $porcentaje, $rs['importeIVA']);
        $E06_pos = array(0, 3, 8, 22);

        $E07 = array('E07', 'NO IDENTIFICADO', '', '', '', '', '');
        $E07_pos = array(0, 3, 153, 203, 253, 272, 286);

        $EA1 = array('EA1', $diasVencimiento, $moneda, $tipoDeCambio, strtoupper($totalLetra));
        $EA1_pos = array(0, 3, 6, 9, 23);

        $EX1 = array('EX1', $chtipoDocumento);
        $EX1_pos = array(0, 3);

        $EA5 = array('EA5', $ea5Concepto);
        $EA5_1 = array('EA5', $ea5Concepto2);
        $EA5_pos = array(0, 3);

        $C04 = array('C04', 'CLAVE DISTRIBUIDOR:'.$rs['distribuidor']);
        $C05 = array('C05', $date1);
        $C06 = array('C06', $date2);
        $C_pos = array(0, 3);


        $D01 = array();
        for ($i=0; $i < count($rsDetalle); $i++) { 
            $D01[] = array('D01', $cantidadUnidades, $unidad, substr($rsDetalle[$i]['vin'], 9), $rsDetalle[$i]['importe'], 
                number_format((float)($rsDetalle[$i]['importe'] * $cantidadUnidades), 2, '.', ''));
        }
        $D01_pos = array(0, 3, 17, 62, 212, 226);

        $fileDir = "facturacionElectronica/".$_REQUEST['faFacturasCompaniaHdn']."-".date("Y-M").".txt";

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'].$fileDir, "w") or die("Unable to open file!");

        //IMPRIME E01----------------------------
        $text = getTxt($E01, $E01_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME E02----------------------------
        $text = out('n',1).getTxt($E02, $E02_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME E03----------------------------
        $text = out('n',1).getTxt($E03, $E03_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME E04----------------------------
        $text = out('n',1).getTxt($E04, $E04_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME E05----------------------------
        $text = out('n',1).getTxt($E05, $E05_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME E06----------------------------
        $text = out('n',1).getTxt($E06, $E06_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME E07----------------------------
        $text = out('n',1).getTxt($E07, $E07_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME EA1----------------------------
        $text = out('n',1).getTxt($EA1, $EA1_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME EX1----------------------------
        $text = out('n',1).getTxt($EX1, $EX1_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME EA5----------------------------
        $text = out('n',1).getTxt($EA5, $EA5_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME EA5_1----------------------------
        $text = out('n',1).getTxt($EA5_1, $EA5_pos);
        fwrite($myfile, $text);
        //---------------------------------------
        //IMPRIME D01----------------------------
        for ($i=0; $i < count($rsDetalle) ; $i++) { 
            $text = out('n',1).getTxt($D01[$i], $D01_pos);
            fwrite($myfile, $text);
        }
        //---------------------------------------
        //IMPRIME C04------------------------------
        $text = out('n',1).getTxt($C04, $C_pos);
        fwrite($myfile, $text);
        //-----------------------------------------
        //IMPRIME C05------------------------------
        $text = out('n',1).getTxt($C05, $C_pos);
        fwrite($myfile, $text);
        //-----------------------------------------
        //IMPRIME C06------------------------------
        $text = out('n',1).getTxt($C06, $C_pos);
        fwrite($myfile, $text);
        //-----------------------------------------


        fclose($myfile);

        header('Location:'."../../".$fileDir);

    }

    //FUNCION PARA TXT *PARA EL LAYOUT
    //FUNCIÓN QUE LE LOS ARREGLOS DE LOS TEXTOS Y POSICIONES Y LOS CONCATENA...
    function getTxt($texts, $positions){
        $text = '';
        for ($i=0; $i < count($texts); $i++) {
            if($i == 0) {
                $antPos = 0;
                $antLength = 0;
            } else {
                $antPos = $positions[$i - 1];
                $antLength = strlen($texts[$i - 1]);
            }
            $text .= out('s', $positions[$i] - ($antPos + $antLength)).$texts[$i];
        }
        return $text;
    }

   
?>