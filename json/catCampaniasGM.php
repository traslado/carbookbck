<?php

	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");	
	
	switch($_REQUEST['catCampaniaHdn'])
	{
		case 'getCampanias':
			getCampanias();
			break;
		case 'getCamDesactivadas':
			getCamDesactivadas();
			break;
		case 'getCamActivadas':
			getCamActivadas();	
			break;
		case 'updDesCampania':
			updDesCampania();	
			break;
		case 'updActCampania':
			updActCampania();	
			break;
		case 'addCampania':
			echo json_encode(addCampania($_REQUEST['servAdicionalesCentroCmb'],$_REQUEST['servAdicionalesCampaniaTxt'],
										 $_REQUEST['servAdicionalesCostoTxt'],$_REQUEST['servAdicionalesMarcaCmb'],
										 $_REQUEST['servAdicionalesClasificacionCmb'],$_REQUEST['servAdicionalesVinicioDte'],
										 $_REQUEST['servAdicionalesVfinalDte'],$_REQUEST['servAdicionalesStatusCmb'],
										 $_REQUEST['servAdicionalesDescripcionTxt']));	
			break;
		case 'creaCampania':
			creaCampania();
			break;
		case 'addSalCampania':
			addSalCampania();
			break;
		case 'addEstatus':
			addEstatus();
			break;
		case 'sqlGetUltimoNumero':
			sqlGetUltimoNumero();
			break;
		case 'sqlGetUltimoNumero':
			sqlGetUltimoNumero();
			break;
		case 'addTmp':
			addTmp();
			break;
		case 'updServicioAdicionales':
			updServicioAdicionales();	
			break;
		case 'getExistente':
			getExistente();
		break;
		case 'updServAdicionales':
			updServAdicionales();	
			break;	
		case 'consultaClaveMovimiento':
			consultaClaveMovimiento();	
			break;	
		default:		
			echo '';			
		break;
	}

	function getCampanias(){

		$lsWhereStr = "WHERE sa.claveMovimiento = ge.valor AND ge.columna='claveMovimiento' ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['servAdicionalesCentroCmb'], "centroDistribucion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['servAdicionalesMarcaCmb'], "marca", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct(strtoupper($_REQUEST['servAdicionalesCampaniaTxt']), "claveMovimiento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['servAdicionalesClasificacionCmb'], "clasificacionMarca", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        		
		$sqlGetCampanias = "SELECT sa.centroDistribucion, CONCAT(sa.claveMovimiento,' - ' ,ge.nombre ) as servicio,
							(SELECT CONCAT(sa.clasificacionMarca,' - ' ,cm.descripcion ) FROM caclasificacionmarcatbl cm WHERE sa.marca = cm.marca AND sa.clasificacionMarca = cm.clasificacion) as clasificacion, sa.marca, sa.costo, sa.vigenciaInicio, sa.vigenciaFin, sa.estatus
							FROM caserviciosadicionalestbl sa, cageneralestbl ge ".$lsWhereStr;							

		$rsGetCampanias = fn_ejecuta_query($sqlGetCampanias);

		echo json_encode($rsGetCampanias);

	}

	function getMaxCampania(){
		$sqlMaxCampania = "SELECT MAX(valor + 1) as ultimaCampania ".
							"FROM cageneralestbl ".
							"WHERE tabla = 'catCampaniasTmp' ".
							"AND columna = 'numCampania';";

		$rsGetCampanias = fn_ejecuta_query($sqlGetCampanias);

		echo json_encode($rsGetCampanias);									
	}

	function getCamDesactivadas(){
		
		$sqlCampaniaDes =  "SELECT DISTINCT CONCAT(sa.claveMovimiento,' - ', ge.nombre) as nombre, sa.centroDistribucion, sa.clasificacionMarca,CONCAT(sa.clasificacionMarca,' - ',cm.descripcion) as descripcion
								FROM caserviciosadicionalestmp sa, cageneralestbl ge, caclasificacionmarcatbl cm
								WHERE sa.claveMovimiento = ge.valor
								AND cm.marca=sa.marca
								AND cm.clasificacion=sa.clasificacionMarca
								AND ge.columna='claveMovimiento'
								AND sa.estatus = '0'
							UNION
							SELECT DISTINCT CONCAT(sa.claveMovimiento,' - ', ge.nombre) as nombre, sa.centroDistribucion, sa.clasificacionMarca,''as descripcion
								FROM caserviciosadicionalestmp sa, cageneralestbl ge
								WHERE sa.claveMovimiento = ge.valor
								AND sa.clasificacionMarca IS NULL
								AND ge.columna='claveMovimiento'
								AND sa.estatus = '0' 
							ORDER BY 2;";

		$rsGetCamDes = fn_ejecuta_query($sqlCampaniaDes);

		echo json_encode($rsGetCamDes);							

	}

	function getCamActivadas(){
		$sqlCampaniaAct =   "SELECT DISTINCT CONCAT(sa.claveMovimiento,' - ', ge.nombre) as nombre, sa.centroDistribucion, sa.clasificacionMarca,CONCAT(sa.clasificacionMarca,' - ',cm.descripcion) as descripcion
								FROM caserviciosadicionalestmp sa, cageneralestbl ge, caclasificacionmarcatbl cm
								WHERE sa.claveMovimiento = ge.valor
								AND cm.marca=sa.marca
								AND cm.clasificacion=sa.clasificacionMarca
								AND ge.columna='claveMovimiento'
								AND sa.estatus = '1'
							UNION
							SELECT DISTINCT CONCAT(sa.claveMovimiento,' - ', ge.nombre) as nombre, sa.centroDistribucion, sa.clasificacionMarca,''as descripcion 
								FROM caserviciosadicionalestmp sa, cageneralestbl ge
								WHERE sa.claveMovimiento = ge.valor
								AND sa.clasificacionMarca IS NULL
								AND ge.columna='claveMovimiento'
								AND sa.estatus = '1' 
							ORDER BY 2; ";

		$rsGetCamAct = fn_ejecuta_query($sqlCampaniaAct);

		echo json_encode($rsGetCamAct);	
	}

	function updDesCampania(){

		if ($_REQUEST['clasificacionMarca'] == '') {

			$complementoQry= "AND clasificacionMarca IS NULL " ;
		}
		else
		{
			$complementoQry= "AND clasificacionMarca = '".$_REQUEST['clasificacionMarca']."' ";
		}

		$updDesCam = "UPDATE caserviciosadicionalestmp ".
						"SET estatus = '0' ".
						"wHERE claveMovimiento = '".strtoupper($_REQUEST['catCampaniaNum'])."' ".
						"AND centroDistribucion = '".$_REQUEST['centroDistribucion']."' ".
						$complementoQry;

		$rsUpdDesCam = fn_ejecuta_query($updDesCam);

		echo json_encode($rsUpdDesCam);						
	}
	function updActCampania(){

		if ($_REQUEST['clasificacionMarca'] == '') {

			$complementoQry= "AND clasificacionMarca IS NULL " ;
		}
		else
		{
			$complementoQry= "AND clasificacionMarca = '".$_REQUEST['clasificacionMarca']."' ";
		}
				
		$updDesCam = "UPDATE caserviciosadicionalestmp ".
						"SET estatus = '1' ".
						"wHERE claveMovimiento = '".strtoupper($_REQUEST['catCampaniaNum'])."' ".
						"AND centroDistribucion = '".$_REQUEST['centroDistribucion']."' ".
						$complementoQry;

		$rsUpdDesCam = fn_ejecuta_query($updDesCam);

		echo json_encode($rsUpdDesCam);						
	}

	function addCampania($cDistribucion,$servicio,$descServicio,$numcosto,$mUnidad,$numClasificacion,$fInicio,$fFinal,$numEstatus,$cveServicio){

		/*$dltCampanias =	"DELETE FROM cageneralestbl ".
						"WHERE tabla = 'catCampaniasTmp' ";
			
		$rsDltCampanias = fn_ejecuta_query($dltCampanias);

		echo json_encode($rsDltCampanias);	*/

		if($_REQUEST['servAdicionalesVinicioDte'] == ''){
			$_REQUEST['servAdicionalesVinicioDte'] = date('Y-m-d');
		}

		$insConceptos="INSERT INTO caconceptosfacturaciontbl (idConcepto, descripcionConcepto, tipoMercado, tarifa) ".
						"VALUES ('".$_REQUEST['servAdicionalesCampaniaTxt'].
						"', '".$_REQUEST['servAdicionalesDescripcionTxt'].
						"', 'Sin tipo ', '".
						$_REQUEST['servAdicionalesCostoTxt']."');";
		fn_ejecuta_Add($insConceptos);


		$addServAdicional = "INSERT INTO caserviciosadicionalestbl (centroDistribucion, clasificacionMarca, marca, claveMovimiento, costo, vigenciaInicio, vigenciaFin, estatus) ".
						"VALUES('".$_REQUEST['servAdicionalesCentroCmb']."',".
						replaceEmptyNull("'".$_REQUEST['servAdicionalesClasificacionCmb']."'").", ".
						"'".strtoupper($_REQUEST['servAdicionalesMarcaCmb'])."',".
						"'".strtoupper($_REQUEST['servAdicionalesCampaniaTxt'])."',".
						"'".strtoupper($_REQUEST['servAdicionalesCostoTxt'])."',".
						"'".$_REQUEST['servAdicionalesVinicioDte']."',".
						replaceEmptyNull("'".$servAdicionalesVfinalDte."'").", ".
						"'".$_REQUEST['servAdicionalesStatusCmb']."'); ";	

			
		fn_ejecuta_query($addServAdicional);


		$sqlconsultaCM = "SELECT * from caGeneralesTbl where valor='".$_REQUEST['servAdicionalesCampaniaTxt']."' ".
		    				 "AND tabla='alHistoricoUnidadesTbl' AND columna='claveMovimiento' ";              

        $rssqlconsultaCM = fn_ejecuta_query($sqlconsultaCM);

		if ($rssqlconsultaCM['records'] == 0) {


		$sqlCreaCampania = "INSERT INTO caGeneralesTbl (tabla, columna, valor, nombre, estatus, idioma) ".
					"VALUES('alHistoricoUnidadesTbl', ".
					"'claveMovimiento', ".
					"'".strtoupper($_REQUEST['servAdicionalesCampaniaTxt'])."', ".
					"'".strtoupper($_REQUEST['servAdicionalesDescripcionTxt'])."', ".
					"'0', ".
					"'ESP');";

		$rsSqlCreaCampania = fn_ejecuta_query($sqlCreaCampania);


       $sqlCreaCampaniaNUD = "INSERT INTO caGeneralesTbl (tabla, columna, valor, nombre, estatus, idioma) ".
					"VALUES('alHistoricoUnidadesTbl', ".
					"'nvalidos', ".
					"'".strtoupper($_REQUEST['servAdicionalesCampaniaTxt'])."', ".
					"'".strtoupper($_REQUEST['servAdicionalesDescripcionTxt'])."', ".
					"'0', ".
					"'ESP');";

		$rsSqlCreaCampaniaNUD = fn_ejecuta_query($sqlCreaCampaniaNUD);


		}
		

	}

	function updServicioAdicionales(){	

		$selTmp = "SELECT * FROM  caserviciosadicionalestmp ";

		$rsselTmp = fn_ejecuta_query($selTmp);	
		
	
        for ($i=0; $i < sizeof($rsselTmp['root']) ; $i++) { 


					$updateServiciosAdicionales = " UPDATE caserviciosadicionalestbl ".
												  "SET estatus='".$rsselTmp['root'][$i]['estatus']."' ".
										          "WHERE idServicioAdicional='".$rsselTmp['root'][$i]['idServicioAdicional']."' ";


				    $rsupdateServiciosAdicionales=fn_ejecuta_query($updateServiciosAdicionales);

		}

		addTmp();
	}

	function addSalCampania(){
		
		$vin1 = $_REQUEST['vines'];
		$vin2 = trim($vin1);
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin2);

		$cadena = chunk_split($vin, 17,"','");

		$vines = substr($cadena,0,-2);		
	    
		
		$sqlGetUnds = "SELECT vin FROM alultimodetalletbl ".
						"WHERE claveMovimiento NOT IN(SELECT VALOR ".
						"FROM cageneralestbl ".
						"WHERE tabla = 'catCampaniasTmp' ".
						"AND estatus  = 0) ".
						"AND centroDistribucion = 'LZC02' ".
						"AND vin IN('".$vines.") ";		

		$rsSqlGetUnds = fn_ejecuta_query($sqlGetUnds);

		echo json_encode($rsSqlGetUnds);
		

	    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
									"SET fechaEvento = NOW(), claveMovimiento = 'SC' ". 
									"WHERE claveMovimiento IN(SELECT valor FROM (SELECT ge.valor FROM cageneralestbl ge ".
                                    										    "WHERE ge.tabla = 'catCampaniasTmp' ".
                                        										"AND ge.estatus  = 0) AS valor) ".
									"AND vin IN (SELECT  vin FROM (SELECT ud.vin FROM alultimodetalletbl ud ".
									"WHERE ud.centroDistribucion = 'LZC02' ".
									"AND ud.claveMovimiento IN(SELECT VALOR FROM cageneralestbl ge ".
                                    						    "WHERE ge.tabla = 'catCampaniasTmp' ".
                                          						"AND ge.estatus  = 0) ".
                                          						"AND ud.vin IN('".$vines.")) as vin); ";

	    fn_ejecuta_query($sqlUpdUltimoDetalleStr);
	    
		
	    $sqlAddCambioHistoricoStr = "INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
									"SELECT DISTINCT centroDistribucion, vin, NOW() as fechaEvento,'SC' as claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer,'UNIDAD INGRESO ".$_REQUEST['cMovimiento']."' as observaciones, usuario, ip ".
									"FROM alhistoricounidadestbl ".
									"WHERE centroDistribucion = 'LZC02' ".
									"AND claveMovimiento IN (SELECT VALOR FROM cageneralestbl WHERE tabla = 'catCampaniasTmp' AND estatus  = 0) ".
									"AND vin IN('".$vines.") ";									
	    
	    $rs_01 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);
	    
		//echo json_encode($$rs_01);						
	}

	function addEstatus(){
		$vin1= $_REQUEST['vines'];
		$vin2=trim($vin1);
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin2);

		$cadena = chunk_split($vin, 17,"','");

		$vines = substr($cadena,0,-2);


	    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
	    							"SET fechaEvento = NOW(), ".
	    							"claveMovimiento = '".$_REQUEST['cMovimiento']."', ".
	    							"observaciones= 'UNIDAD INGRESO ".$_REQUEST['cMovimiento']."' ".
	    							"WHERE vin IN('".$vines.") ". 
	    							"AND claveMovimiento = '".$_REQUEST['cmAnterior']."' ".
	    							"AND vin not in (SELECT vin from alHistoricoUnidadesTbl where claveMovimiento='".$_REQUEST['cMovimiento']."') ";

	    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);
	    

	    $sqlAddCambioHistoricoStr = "INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
									"SELECT DISTINCT centroDistribucion, vin, NOW() as fechaEvento,'".$_REQUEST['cMovimiento']."' as claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer,'UNIDAD INGRESO ".$_REQUEST['cMovimiento']."' as observaciones, usuario, ip ".
									"FROM alhistoricounidadestbl ".
									"WHERE centroDistribucion = 'LZC02' ".
									"AND claveMovimiento = 'L2' OR claveMovimiento = 'SC' ".
									"AND vin IN('".$vines.")".
									"AND vin not in (SELECT vin from alHistoricoUnidadesTbl where claveMovimiento='".$_REQUEST['cMovimiento']."') ";
	    
	    $rs_01 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);
	    
		echo json_encode($sqlUpdUltimoDetalleStr);		
  
		//echo json_encode($vines);		
	}

	function sqlGetUltimoNumero(){

		$sqlGetMaximo = "SELECT MAX(valor) as incremental
							FROM cageneralestbl
							WHERE tabla = 'catCampaniasTmp' 
							AND columna = 'numCampania';";

		$rsGetMaximo = fn_ejecuta_Add($sqlGetMaximo);

		echo json_encode($rsGetMaximo);						
	}

	function addTmp(){
		
		$trcTmp = "DELETE FROM  caserviciosadicionalestmp; ";

		$rsTrcTmp = fn_ejecuta_query($trcTmp);

		echo json_encode($rsTrcTmp);	

		$sqlAddTmp ="INSERT INTO caserviciosadicionalestmp (idservicioAdicional,centroDistribucion, clasificacionMarca, marca, claveMovimiento, costo, vigenciaInicio, vigenciaFin, estatus)
						SELECT idservicioAdicional,centroDistribucion, clasificacionMarca, marca, claveMovimiento, costo, vigenciaInicio, vigenciaFin, estatus
						FROM caserviciosadicionalestbl;";

		fn_ejecuta_query($sqlAddTmp);
	}

	function getExistente(){

		    $sqlGetCampanias = "SELECT sa.costo, sa.vigenciaInicio, sa.vigenciaFin, ge.nombre, sa.estatus ".
              "FROM caserviciosadicionalestbl sa, cageneralestbl ge ".
              "WHERE sa.claveMovimiento = ge.valor ".
              "AND ge.columna = 'claveMovimiento' ".
              "AND sa.centroDistribucion = '".$_REQUEST['servAdicionalesCentroCmb']."' ".
              "AND sa.clasificacionMarca = '".$_REQUEST['servAdicionalesClasificacionCmb']."' ".
              "AND sa.marca = '".$_REQUEST['servAdicionalesMarcaCmb']."' ".
              "AND sa.claveMovimiento = '".strtoupper($_REQUEST['servAdicionalesCampaniaTxt'])."';";              

    			$rsGetCampanias = fn_ejecuta_query($sqlGetCampanias);

			    echo json_encode($rsGetCampanias);
	}

	function updServAdicionales(){	 

		$sqlUpdservicio = "UPDATE caserviciosadicionalestbl ".
							"SET costo = '".$_REQUEST['servAdicionalesCostoTxt']."', ".
								"vigenciaInicio = '".$_REQUEST['servAdicionalesVinicioDte']."', ".
    							"vigenciaFin = ".replaceEmptyNull("'".$_REQUEST['servAdicionalesVfinalDte']."'").", ".
    							"estatus = '".$_REQUEST['servAdicionalesStatusCmb']."' ".
							"WHERE centroDistribucion ='".$_REQUEST['servAdicionalesCentroCmb']."' ".
							"AND clasificacionMarca = '".$_REQUEST['servAdicionalesClasificacionCmb']."' ".
							"AND claveMovimiento = '".strtoupper($_REQUEST['servAdicionalesCampaniaTxt'])."' ".
							"AND MARCA = '".$_REQUEST['servAdicionalesMarcaCmb']."' ;";

		fn_ejecuta_query($sqlUpdservicio);


		$sqlUpdDescripcion = "UPDATE cageneralestbl ".
								"SET nombre = '".strtoupper($_REQUEST['servAdicionalesDescripcionTxt'])."' ".
								"WHERE TABLA = 'alHistoricoUnidadesTbl' ".
								"AND columna = 'claveMovimiento' ".
								"AND valor = '".strtoupper($_REQUEST['servAdicionalesCampaniaTxt'])."';";

		fn_ejecuta_query($sqlUpdDescripcion);

		$sqlUpdDescripcionNUD = "UPDATE cageneralestbl ".
								"SET nombre = '".strtoupper($_REQUEST['servAdicionalesDescripcionTxt'])."' ".
								"WHERE TABLA = 'alHistoricoUnidadesTbl' ".
								"AND columna = 'nvalidos' ".
								"AND valor = '".strtoupper($_REQUEST['servAdicionalesCampaniaTxt'])."';";

		fn_ejecuta_query($sqlUpdDescripcionNUD);

		$sqlUpdservicioTmp = "UPDATE caserviciosadicionalestmp ".
							"SET costo = '".$_REQUEST['servAdicionalesCostoTxt']."', ".
								"vigenciaInicio = '".$_REQUEST['servAdicionalesVinicioDte']."', ".
    							"vigenciaFin = ".replaceEmptyNull("'".$_REQUEST['servAdicionalesVfinalDte']."'").", ".
    							"estatus = '".$_REQUEST['servAdicionalesStatusCmb']."' ".
							"WHERE centroDistribucion ='".$_REQUEST['servAdicionalesCentroCmb']."' ".
							"AND clasificacionMarca = '".$_REQUEST['servAdicionalesClasificacionCmb']."' ".
							"AND claveMovimiento = '".strtoupper($_REQUEST['servAdicionalesCampaniaTxt'])."' ".
							"AND MARCA = '".$_REQUEST['servAdicionalesMarcaCmb']."' ;";

		fn_ejecuta_query($sqlUpdservicioTmp);
	}


		function consultaClaveMovimiento(){

		    $sqlconsultaCM = "SELECT * from caGeneralesTbl where valor='".$_REQUEST['claveMovimiento']."' ".
		    				 "AND tabla='alHistoricoUnidadesTbl' AND columna='claveMovimiento' ";              

    		$rssqlconsultaCM = fn_ejecuta_query($sqlconsultaCM);

			    echo json_encode($rssqlconsultaCM);
	}

?>	