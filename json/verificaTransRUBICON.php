<?php
  session_start();

  date_default_timezone_set('America/Mexico_City');	
  
  if(!isset($_SERVER['argv']))
  {
	    include_once("../funciones/generales.php");
	    include_once("../funciones/construct.php");	
  }
  else
  {
			include_once($_SERVER['argv'][1]."generales.php");
			include_once($_SERVER['argv'][1]."construct.php");  		
  }
			
	//var_dump($_SERVER['argv']);
	//Para correr desde consola: php "C:\wamp\www\carbookbck\json\verificaTransRUBICON.php" C:\Paso\log\ E:\carbook\i340_CB\respaldo\ C:\carbook\i340_CB\respaldo\ E:\carbook\i341_CB\respaldo\ C:\carbook\i341_CB\respaldo\ E:\carbook\i343a\respaldo\ C:\carbook\i343\respaldo\ E:\carbook\i830_CB\respaldo\ C:\carbook\i830_CB\respaldo\ busca 2020-06-24 vin E
	//$_SERVER['argv'][0]>Nombre del PHP
	//$_SERVER['argv'][1]>Directorio de funciones de PHP	
	//$_SERVER['argv'][2]>Directorio donde se encuentra el log de archivos con error de transmisi�n
	//$_SERVER['argv'][3]>Directorio donde se encuentra el log de los nombres de archivos con error de transmisi�n
	//$_SERVER['argv'][4]>Directorio donde se mover�n los archivos log que se retransmitieron con �xito
	//$_SERVER['argv'][5]>Directorio donde se encuentran los archivos fuente a transmitir
	//$_SERVER['argv'][6]>IP del server de FTP
	//$_SERVER['argv'][7]>Directorio remoto del server de FTP
	//$_SERVER['argv'][8]>Usuario del server de FTP
	//$_SERVER['argv'][9]>Password del server de FTP
	//$_SERVER['argv'][10]>Acci�n a realizar
	//$_SERVER['argv'][11]>Fecha de b�squeda
	//$_SERVER['argv'][12]>N�mero de VIN
	//$_SERVER['argv'][13]>Letra de la unidad donde se encuentran los archivos transmitidos de forma autom�tica
	
	if(!isset($_SERVER['argv']))
	{
			$_SERVER['argv'][0]  = 'C:\Paso\log\\';
			$_SERVER['argv'][1]  = $_REQUEST['dir'].':\carbook\i340_CB\respaldo\\';
			$_SERVER['argv'][2]  = 'C:\carbook\i340_CB\respaldo\\';
			$_SERVER['argv'][3]  = $_REQUEST['dir'].':\carbook\i341_CB\respaldo\\';
			$_SERVER['argv'][4]  = 'C:\carbook\i341_CB\respaldo\\';
			$_SERVER['argv'][5]  = $_REQUEST['dir'].':\carbook\i343a\respaldo\\';
			$_SERVER['argv'][6]  = 'C:\carbook\i343\respaldo\\';
			$_SERVER['argv'][7]  = $_REQUEST['dir'].':\carbook\i830_CB\respaldo\\';
			$_SERVER['argv'][8]  = 'C:\carbook\i830_CB\respaldo\\';			
			$_SERVER['argv'][9]  = $_REQUEST['actionHdn'];
			$_SERVER['argv'][10] = $_REQUEST['fecha'];
			$_SERVER['argv'][11] = $_REQUEST['vin'];
			$_SERVER['argv'][12] = $_REQUEST['dir'];
	}
	else
	{
			$arrTmp = $_SERVER['argv'];
			unset($_SERVER['argv']);
			$_SERVER['argv'][0]  = $arrTmp[1];
			$_SERVER['argv'][1]  = $arrTmp[2];
			$_SERVER['argv'][2]  = $arrTmp[3];
			$_SERVER['argv'][3]  = $arrTmp[4];
			$_SERVER['argv'][4]  = $arrTmp[5];
			$_SERVER['argv'][5]  = $arrTmp[6];
			$_SERVER['argv'][6]  = $arrTmp[7];
			$_SERVER['argv'][7]  = $arrTmp[8];
			$_SERVER['argv'][8]  = $arrTmp[9];
			$_SERVER['argv'][9]  = $arrTmp[10];
			$_SERVER['argv'][10]  = $arrTmp[11];			
			$_SERVER['argv'][11]  = $arrTmp[12];	
			$_SERVER['argv'][12]  = $arrTmp[13];	
			//var_dump($_SERVER['argv']);		
	}

  switch($_SERVER['argv'][9]){
  		case 'busca':
  				archTrans();
  				break;
  		case 'buscaVIN':
  				buscaVIN($_REQUEST['tipoTransaccion'], $_REQUEST['archivo']);
  				break;  
  		case 'generaExcel':
  				generaExcel();
  				break;  				
  		case 'generaUnidadesExcel':
  				generaUnidadesExcel();
  				break; 
  		case 'descargaLog':
  				descargaLog();
  				break;
  		case 'retransmitir':
  				retransmitir();
  				break;
  		case 'actualiza':
  				actualiza();
  				break;  
      default:
          echo '';
  }
  
  function archTrans(){
  		ini_set('max_execution_time', 300);
  		set_time_limit(300);
  		//$now = date("Y-m-d");
  		$now = date("Y-m-d",strtotime($_SERVER['argv'][10]));  
  		$_REQUEST['archivo'] = '';			
  		
  		//Cuando filtran por el VIN
  		if(isset($_SERVER['argv'][11]) && !empty($_SERVER['argv'][11]) && isset($_REQUEST['tipoTransmision']) && $_REQUEST['tipoTransmision'] != 5)
  		{
				  switch((int)$_REQUEST['tipoTransmision']){
				  		case 1:
				  				$tipo = 'R2V';
				  				$ext  = ".TXT";
				  				break;  
				  		case 2:
				  				$tipo = 'RA3';
				  				$ext  = ".TXT";
				  				break;  								  				
				  		case 3:
				  				$tipo = 'H10';
				  				$ext  = ".FAL";
				  				break;  	
				  		case 4:
				  				$tipo = 'R41';
				  				$ext  = ".TXT";
				  				break;  								  					  						
				      default:
				          echo '';
				  }
				  $and = "";
				  if(isset($_SERVER['argv'][10]) && !empty($_SERVER['argv'][10]))
				  {
				  		$and = " AND SUBSTRING(fechaMovimiento,1,10) = '".$_SERVER['argv'][10]."'";
				  }
  				$sql = "SELECT SUBSTRING(fechaGeneracionUnidad,1,10) AS fecha, prodStatus AS archivo FROM alTransaccionUnidadTbl ".
  				       " WHERE SUBSTRING(fechaGeneracionUnidad,1,10) >= '2020-06-24'".
  				       " AND vin IN ('".strtoupper($_SERVER['argv'][11])."')".
  				       " AND tipoTransaccion = '".$tipo."'".
  				       $and.
								 " ORDER BY fechaMovimiento DESC";
					//echo "$sql<br>";
					$rstFecha = fn_ejecuta_query($sql);
					if($rstFecha['records'] > 0)
					{
							$now = date("Y-m-d",strtotime($rstFecha['root'][0]['fecha']));
							$_REQUEST['archivo'] = $rstFecha['root'][0]['archivo'].$ext;
					}
					else
					{
							$now = date("Y-m-d");
							$intervalo = '+1 days';
							$now = date("Y-m-d",strtotime("$now ".$intervalo));
					}
  		}
			//var_dump($now);
  		$dir = $_SERVER['argv'][0];
			chdir($dir);
			array_multisort(array_map('filemtime', ($file = glob("*.*"))), SORT_DESC, $file);				
  		//var_dump($now);			
					
			//Obtiene los archivos log de lo que se transmiti� en la fecha deseada
			$i = 0;			
			foreach($file as $filename)
			{
					//var_dump($now,date("Y-m-d", filemtime($filename)));
					if($now == date("Y-m-d", filemtime($filename)))
					{
							if(filesize($filename) > 6000)
							{
									$name = str_replace("error", "files", $filename);
									$aux = explode("_",$filename);
									$aux2 = explode(".",$aux[1]);
									$tipoTrans = (count($aux) == 2)?$aux2[0]:$aux2[0].'_Manual';
									
								  switch($tipoTrans){
								  		case 'trans340':
								  				$dirFte = $_REQUEST['dir'].':/carbook/i340_CB/respaldo/';
								  				$tipo 	= 'R2V';
								  				$tipoT 	= 1;
								  				break;
								  		case 'trans340_Manual':
								  				$dirFte = 'C:/carbook/i340_CB/respaldo/';
								  				$tipo 	= 'R2V';
								  				$tipoT 	= 1;
								  				break;  
								  		case 'trans341':
								  				$dirFte = $_REQUEST['dir'].':/carbook/i341_CB/respaldo/';
								  				$tipo 	= 'RA3';
								  				$tipoT 	= 2;
								  				break;
								  		case 'trans341_Manual':
								  				$dirFte = 'C:/carbook/i341_CB/respaldo/';
								  				$tipo 	= 'RA3';
								  				$tipoT 	= 2;
								  				break;  								  				
								  		case 'trans343':
								  				$dirFte = $_REQUEST['dir'].':/carbook/i343a/respaldo/';
								  				$tipo 	= 'H10';
								  				$tipoT 	= 3;
								  				break;
								  		case 'trans343_Manual':
								  				$dirFte = 'C:/carbook/i343/respaldo/';
								  				$tipo 	= 'H10';
								  				$tipoT 	= 3;
								  				break;  	
								  		case 'trans830':
								  				$dirFte = $_REQUEST['dir'].':/carbook/i830_CB/respaldo/';
								  				$tipo 	= 'R41';
								  				$tipoT 	= 4;
								  				break;
								  		case 'trans830_Manual':
								  				$dirFte = 'C:/carbook/i830_CB/respaldo/';
								  				$tipo 	= 'R41';
								  				$tipoT 	= 4;
								  				break;  								  					  						
								      default:
								          echo '';
								  }
								  if((isset($_REQUEST['tipoTransmision']) && $_REQUEST['tipoTransmision'] != 5 && $_REQUEST['tipoTransmision'] == $tipoT) ||
								    (isset($_REQUEST['tipoTransmision']) && $_REQUEST['tipoTransmision'] == 5) || !isset($_REQUEST['tipoTransmision']))
								  {
										  //var_dump($filename,$tipo);																		
											$log[$i] = array('name'=>$filename, 'date'=>date("Y-m-d", filemtime($filename)), 'size'=>filesize($filename),
											                 'dirFte'=>$dirFte, 'tipoTransmision'=>$tipoTrans, 'tipoTransaccion'=>$tipo);	
											$i++; 								  		
								  }                        			 
							}
					}
					if($now > date("Y-m-d", filemtime($filename)))
					{
							break;
					}
			}
			//var_dump($log);	
						
			//Obtiene los archivos transmitidos
			$i = 0;			
			$k = 0;					
			foreach($log AS $row)
			{
					//var_dump($row['name']);				    
			    $file = fopen($row['name'],"rb");
			    $j = 0;
			    while(feof($file) === false)
			    {			       	
					    $lin = fgets($file);
					    $pos = strpos($lin, 'Transfer done:');
							if(!$pos === false)
							{
									//var_dump($lin);
									$aux = explode("'",$lin);								
									$log[$i]['files'][$j] = strtoupper($aux[1]);
									//var_dump(strtoupper($aux[1]));
									if((!isset($_REQUEST['archivo']) || empty($_REQUEST['archivo'])) || (isset($_REQUEST['archivo']) && !empty($_REQUEST['archivo']) && $_REQUEST['archivo'] == strtoupper($aux[1])))
									{
											$archTransmitidos['root'][$k] = array('logName'=>$row['name'], 'dirFte'=>$row['dirFte'], 'file'=>strtoupper($aux[1]),
											                              				'dateFTP'=>substr($lin,2,19),'tipoTransaccion'=>$row['tipoTransaccion']);
											$j++;	
											$k++;
									}
							}
							$log[$i]['records'] = $j;
			    }
			    fclose($file);
			    $i++;			   			
			}
			$archTransmitidos['records'] = $k;
  		if($archTransmitidos['records'] == 0)
  		{
  				$archTransmitidos['root'] = null;
  		}				
			//var_dump($archTransmitidos);	
			
			
			//Obtiene las unidades transmitidas
			$i = 0;
			$j = 0;
			foreach($archTransmitidos['root'] AS $row)
			{
					//var_dump('--',$row['file']);
			    $file = fopen($row['dirFte'].$row['file'],"rb");
					//var_dump($file);
			    if(isset($row['file']) && $file !== false)
			    {
			    		$aux = explode(".", $row['file']);					    
					    $rstVIN = buscaVIN($row['tipoTransaccion'], $aux[0]);
					    //var_dump($rstVIN);
					    if($rstVIN['records'] > 0)
					    {
									foreach($rstVIN['root'] AS $row2)
									{
							    		$arrUnidades['root'][$j] = array('dirFte'=>$row['dirFte'], 'file'=>strtoupper($row['file']),
									                              			 'tipoTransaccion'=>$row['tipoTransaccion'],'vin'=>$row2['vin']);
					    				$j++;
									}
					    		$i = $i + $rstVIN['records'];											
					    }
					    /*
					    while(feof($file) === false)
					    {
					    			$lin = fgets($file);
			    					if(substr($lin,0,3) == '2VX' || substr($lin,0,3) == '3RX' || (substr($lin,0,3) == 'VI*' && $row['tipoTransaccion'] == 'H10'))
			    					{
			    							$pos = ($row['tipoTransaccion'] != 'H10')?10:3;
			    							$arrUnidades['root'][$i] = array('dirFte'=>$row['dirFte'], 'file'=>strtoupper($row['file']),
									                              				 'tipoTransaccion'=>$row['tipoTransaccion'],'vin'=>substr($lin,$pos,17));
												$i++;
			    					}		    			
					    }*/
			  	}					
			}
			$arrUnidades['records'] = $i;
  		if($arrUnidades['records'] == 0)
  		{
  				$arrUnidades['root'] = null;
  		}			
			//var_dump($arrUnidades);


			//Verifica los archivos que no se hayan transmitido  		
  		if((!isset($_REQUEST['archivo']) || empty($_REQUEST['archivo'])) || (isset($_REQUEST['archivo']) && !empty($_REQUEST['archivo']) && $archTransmitidos['records'] == 0))
  		{
		  		$z	 = 1;
		  		$lim = 9;
		  		if(isset($_REQUEST['archivo']) && !empty($_REQUEST['archivo']) && $archTransmitidos['records'] == 0)
		  		{
				  		switch((int)$_REQUEST['tipoTransmision']){
						  		case 1:						# 'R2V'
											$z 	 = 1;
				  						$lim = 3;
						  				break;  
						  		case 2:						# 'RA3'
											$z 	 = 3;
				  						$lim = 5;
						  				break;  								  				
						  		case 3:						# 'H10'
											$z 	 = 5;
				  						$lim = 7;
						  				break;  	
						  		case 4:						# 'R41'
											$z 	 = 7;
				  						$lim = 9;
						  				break;  	
						      default:
						          echo '';
						  }			  				
		  		}
		  		
		  		for($x=$z; $x<$lim; $x++)
		  		{
							$dir = $_SERVER['argv'][$x];
							//var_dump($dir); 
				  		$archFaltantes = verificaTransmitidos($dir, $now, $archTransmitidos['root'], $archFaltantes);					
		  		}   				
  		}
 		
  		//var_dump($archFaltantes); 
  		$archFaltantes['records'] = count($archFaltantes['root']);
  		
  		if($archFaltantes['records'] == 0)
  		{
  				$archFaltantes['root'] = null;
					$arrUnidadesFaltantes['records'] = 0;
					$arrUnidadesFaltantes['root'] = null;
  		}
  		else
  		{
					$i = 0;
					foreach($archFaltantes['root'] AS $row)
					{
					    $file = fopen($row['fullFile'],"rb");
							//var_dump($row['file']);	
							if($file !== false)
							{
									//var_dump($file);								
							    while(feof($file) === false)
							    {
						    			$lin = fgets($file);
				    					if(substr($lin,0,3) == '2VX' || substr($lin,0,3) == '3RX' || (substr($lin,0,3) == 'VI*' && $row['tipoTransaccion'] == 'H10'))
				    					{
				    							$pos = ($row['tipoTransaccion'] != 'H10')?10:3;
				    							$arrUnidadesFaltantes['root'][$i] = array('dirFte'=>$row['dirFte'], 'file'=>strtoupper($row['file']),
										                              				 					'tipoTransaccion'=>$row['tipoTransaccion'],'vin'=>substr($lin,$pos,17));
													$i++;
				    					}		    			
							    }
					  	}
					}
					$arrUnidadesFaltantes['records'] = $i;
		  		if($arrUnidadesFaltantes['records'] == 0)
		  		{
		  				$arrUnidadesFaltantes['root'] = null;
		  		}						
  		}
  		//var_dump($arrUnidadesFaltantes);
  		$archTransmitidos['unidadesTransmitidas'] = $arrUnidades;
  		$archTransmitidos['archivosFaltantes'] 		= $archFaltantes;  		
  		$archTransmitidos['unidadesFaltantes'] 		= $arrUnidadesFaltantes;
  		
  		echo json_encode($archTransmitidos);
  }
  
  function verificaTransmitidos($dir, $now, $archTransmitidos, $archFaltantes){
			//var_dump($archTransmitidos);			
			chdir($dir);
			array_multisort(array_map('filemtime', ($file = glob("*.*"))), SORT_DESC, $file);		
			$i = count($archFaltantes['root']);						
			foreach($file as $filename)
			{
					if($now == date("Y-m-d", filemtime($filename)))
					{
							$encontro = false;
							foreach($archTransmitidos as $item)
							{																								
									if(isset($item['file']) && $item['file'] == $filename)
									{
											$encontro = true;
											break;
									}
							}
							if(!$encontro)
							{
								  switch($dir){
								  		case $_REQUEST['dir'].':\carbook\i340_CB\respaldo\\':
								  				$tipo = 'R2V';
								  				$tipoT 	= 1;
								  				break;
								  		case 'C:\carbook\i340_CB\respaldo\\':
								  				$tipo = 'R2V';
								  				$tipoT 	= 1;
								  				break;  
								  		case $_REQUEST['dir'].':\carbook\i341_CB\respaldo\\':
								  				$tipo = 'RA3';
								  				$tipoT 	= 2;
								  				break;
								  		case 'C:\carbook\i341_CB\respaldo\\':
								  				$tipo = 'RA3';
								  				$tipoT 	= 2;
								  				break;  								  				
								  		case $_REQUEST['dir'].':\carbook\i343a\respaldo\\':
								  				$tipo = 'H10';
								  				$tipoT 	= 3;
								  				break;
								  		case 'C:\carbook\i343\respaldo\\':
								  				$tipo = 'H10';
								  				$tipoT 	= 3;
								  				break;  	
								  		case $_REQUEST['dir'].':\carbook\i830_CB\respaldo\\':
								  				$tipo = 'R41';
								  				$tipoT 	= 4;
								  				break;
								  		case 'C:\carbook\i830_CB\respaldo\\':
								  				$tipo = 'R41';
								  				$tipoT 	= 4;
								  				break;  								  					  						
								      default:
								          echo '';
								  }
								  if(isset($_REQUEST['archivo']) && !empty($_REQUEST['archivo']) && $archTransmitidos['records'] == 0)
								  {
								  		if($_REQUEST['archivo'] == strtoupper($filename))
								  		{
													$archFaltantes['root'][$i]['files'] = $dir.$filename;											
													$archFaltantes['root'][$i] = array('fullFile'=>$dir.strtoupper($filename),
													                                   'dirFte'=>$dir, 
													                                   'file'=>strtoupper($filename),
													                              		 'dateFile'=>date("Y-m-d H:i:s", filemtime($filename)),
													                              		 'tipoTransaccion'=>$tipo,
													                              		 'dateFTP'=>date("Y-m-d H:i:s", filemtime($filename)),);
													$i++;		
													break;						  				
								  		}								  		
								  }
								  else
								  {	
										  if((isset($_REQUEST['tipoTransmision']) && $_REQUEST['tipoTransmision'] != 5 && $_REQUEST['tipoTransmision'] == $tipoT) ||
										    (isset($_REQUEST['tipoTransmision']) && $_REQUEST['tipoTransmision'] == 5) || !isset($_REQUEST['tipoTransmision']))
										  {
													$archFaltantes['root'][$i]['files'] = $dir.$filename;											
													$archFaltantes['root'][$i] = array('fullFile'=>$dir.strtoupper($filename),
													                                   'dirFte'=>$dir, 
													                                   'file'=>strtoupper($filename),
													                              		 'dateFile'=>date("Y-m-d H:i:s", filemtime($filename)),
													                              		 'tipoTransaccion'=>$tipo,
													                              		 'dateFTP'=>date("Y-m-d H:i:s", filemtime($filename)),);
													$i++;
										  }
								  }								  								  																		
							}
					}
					else
					{
							if($now > date("Y-m-d", filemtime($filename)))
							{
									break;
							}
					}	
			}
			return $archFaltantes; 		
  }
  
	#Busca las unidades del archivo de la transmisi�n
	function buscaVIN($tipoTransaccion, $archivo)
	{
			$sql = "SELECT DISTINCT vin FROM alTransaccionUnidadTbl".
      			 " WHERE tipoTransaccion = '".$tipoTransaccion."'".
      			 " AND prodStatus = '".$archivo."'";
			//echo "$sql<br>";
			$rstVIN = fn_ejecuta_query($sql);
			
			return $rstVIN;
	}  
	
  function generaExcel(){
			require_once('../funciones/Classes/PHPExcel.php');
			require_once('../funciones/Classes/PHPExcel/IOFactory.php');	
			
			$a = array('nombreArchivo' => '');
			
			$arrArchivos = json_decode($_POST['arrArchivos'],true);						//CHK
			//$arrArchivos = json_decode($_GET['arrArchivos'],true);					//CHK
			
			$idx 		 = ($_REQUEST['estatus'] > 1)?0:1;
			$estatus = ($_REQUEST['estatus'] == 1)?'EXITOSO':'FALLIDO';
			$fecha 	 = ($_REQUEST['estatus'] == 1)?'FECHA DE ENVIO POR FTP':'FECHA GENERACION ARCHIVO';
			
			$objXLS = new PHPExcel();
			$objSheet = $objXLS->setActiveSheetIndex(0);
			$objSheet->setCellValue('A1', 'ARCHIVO');
			$objSheet->setCellValue('B1', $fecha);
			$objSheet->setCellValue('C1', 'TIPO DE TRANSACCION');    		
			$objSheet->setCellValue('D1', 'ESTATUS DE ENVIO'); 
			
			$j = 2;
	    foreach ($arrArchivos as $row) {
	        $objSheet->setCellValue('A'.$j, $row['file']);
	        $objSheet->setCellValue('B'.$j, $row['dateFTP']);
	        $objSheet->setCellValue('C'.$j, $row['tipoTransaccion']);
	        $objSheet->setCellValue('D'.$j, $estatus);
	        $j++;
	    }		
	    
	    $objSheet->setCellValue('C'.$j, 'TOTAL DE ARCHIVOS:');    
    	$objSheet->setCellValue('D'.$j, $j-2);
    
			$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);		
			$objXLS->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);	    		
			
			$objXLS->getActiveSheet()->setTitle('monitorFTP');
			$objXLS->setActiveSheetIndex(0);
	    $fecha = date("ymdHi");

			//Verifica si existe el directorio tmp, sino lo crea
			if (!file_exists("../../respaldos/monitorFTP/")) {	
					mkdir("../../respaldos/monitorFTP/", 0777);
			}
			
			if (file_exists("../../respaldos/monitorFTP/")) {	
					$nombreArchivoFull = "../../respaldos/monitorFTP/archivosFTP".$idx."_".$fecha.".xls";
					$nombreArchivo = "archivosFTP".$idx."_".$fecha.".xls";
					$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
					$objWriter->save($nombreArchivoFull);				
			}
			$a['nombreArchivo'] = $nombreArchivo;
			
			echo json_encode($a);
  }

  function generaUnidadesExcel(){
			require_once('../funciones/Classes/PHPExcel.php');
			require_once('../funciones/Classes/PHPExcel/IOFactory.php');	
			
			$a = array('nombreArchivo' => '');
			
			$arrUnidades = json_decode($_POST['arrUnidades'],true);						//CHK
			//$arrUnidades = json_decode($_GET['arrUnidades'],true);					//CHK
			
			$idx 		 = ($_REQUEST['estatus'] > 1)?0:1;
			$estatus = ($_REQUEST['estatus'] == 1)?'EXITOSO':'FALLIDO';
			
			$objXLS = new PHPExcel();
			$objSheet = $objXLS->setActiveSheetIndex(0);
			$objSheet->setCellValue('A1', 'VIN');				
			$objSheet->setCellValue('B1', 'TIPO DE TRANSACCION');    				
			$objSheet->setCellValue('C1', 'ESTATUS DE ENVIO');
			
			$j = 2;
	    foreach ($arrUnidades as $row) {
	        $arch = $row['file'];
	        $objSheet->setCellValue('A'.$j, $row['vin']);	
	        $objSheet->setCellValue('B'.$j, $row['tipoTransaccion']);	                
	        $objSheet->setCellValue('C'.$j, $estatus);
	        $j++;
	    }	
	    $aux = explode(".", $arch);
	    $arch = $aux[0];
	    //var_dump($aux);		    
	    
	    $objSheet->setCellValue('B'.$j, 'TOTAL DE UNIDADES:');    
    	$objSheet->setCellValue('C'.$j, $j-2);
    
			$objXLS->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
			$objXLS->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);	    		
			
			$objXLS->getActiveSheet()->setTitle($aux[0].".".$aux[1]);
			$objXLS->setActiveSheetIndex(0);
	    $fecha = date("ymdHi");

			//Verifica si existe el directorio tmp, sino lo crea
			if (!file_exists("../../respaldos/monitorFTP/")) {	
					mkdir("../../respaldos/monitorFTP/", 0777);
			}
			
			if (file_exists("../../respaldos/monitorFTP/")) {	
					$nombreArchivoFull = "../../respaldos/monitorFTP/unidadesFTP".$idx."_".$arch."_".$fecha.".xls";
					$nombreArchivo = "unidadesFTP".$idx."_".$arch."_".$fecha.".xls";
					$objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5');
					$objWriter->save($nombreArchivoFull);				
			}
			$a['nombreArchivo'] = $nombreArchivo;
			
			echo json_encode($a);
  }

  function descargaLog(){
			$a = array('nombreArchivo' => '');
			
			//Verifica si existe el directorio tmp, sino lo crea
			if (!file_exists("../../tmp/")) {	
					mkdir("../../tmp/", 0777);
			}
			
			if (file_exists("../../tmp/")) {	
					 $aux = explode(".", $_REQUEST['archivo']);
					 $nombreArchivo = $aux[0].".log";
					 copy("C:/Paso/Log/".$_REQUEST['archivoLog'], "../../tmp/".$nombreArchivo);
			}
			$a['nombreArchivo'] = $nombreArchivo;
			
			echo json_encode($a);
  }
  
/*  
  function retransmitir(){						
			$files = array();
			$trans = array();
			$files = array_slice(scandir($_SERVER['argv'][2]), 2);
			$i = 0;
			
			foreach ($files as $key => $name)
			{
					$orig = $name;
					$name = str_replace("error", "files", $name);
					$archs = file($_SERVER['argv'][3].$name);
					$aux = explode("files",$name);
					$aux2 = explode(".",$aux[1]);
					$tipoTrans = $aux2[0];
					
				  switch($tipoTrans){
				  		case '340':
				  				$dirFte = $_SERVER['argv'][5].'i340_CB/respaldo/';
				  				break;
				  		case '341':
				  				$dirFte = $_SERVER['argv'][5].'i341_CB/respaldo/';
				  				break;  
				  		case '343':
				  				$dirFte = $_SERVER['argv'][5].'i343a/respaldo/';
				  				break; 				  						
				      default:
				          echo '';
				  }					
				  				
					foreach ($archs as $row)
					{
							$arch = str_replace("\r\n", "", $row);
							$arch = strtoupper($arch);
							$archTabla = str_replace(".TXT", "", $arch);
							$archTr = $dirFte.$arch;
							if (file_exists($archTr)) {
									$retrans = $_SERVER['argv'][4].str_replace("error", "retrans", $orig);
									$aux3 = explode(".",$retrans);
									$retrans = $aux3[0]."_".date("ymdHis").".".$aux3[1];						
									$trans[$i]['nombreArch'] = $archTabla;
									$trans[$i]['FTPArch'] = $archTr;
									$trans[$i]['errorArch'] = $_SERVER['argv'][2].$orig;
									$trans[$i]['retraArch'] = $retrans;
									$trans[$i]['ok'] = 0;
									$i++;
							}														
					}			
			}
			if($i > 0)
			{
					//var_dump($trans);
					$trans = enviaFTP($trans);
					foreach($trans as $row)
					{
							if($row['ok'] == 1)
							{
									actualiza($row);
							}
					}
			}
  }  

	function enviaFTP($trans){		
			$host			= $_SERVER['argv'][6];
			$user			= $_SERVER['argv'][8];
			$password	= $_SERVER['argv'][9];
			$ruta			= $_SERVER['argv'][7];

			# Realizamos la conexion con el servidor
			$conn_id = ftp_connect($host);

			// loggeo
			if($conn_id)
			{
					$login = ftp_login($conn_id, $user, $password); 
			}
			
			$error = 0;
			$i = 0;			
			// conexi�n
			if ((!$conn_id) || (!$login))
			{ 
					 $error++;
			}
			if($error == 0)
			{
					ftp_pasv($conn_id,true);					
					foreach($trans as $row)
					{
							$aux = explode("/",$row['FTPArch']);		
							$remoteFile = $ruta.$aux[4];
							var_dump($remoteFile);
							var_dump($row['FTPArch']);
							$descarga = ftp_put($conn_id, $remoteFile, $row['FTPArch'], FTP_BINARY);
							if($descarga)
							{ 
									$trans[$i]['ok'] = 1;
							}						
							$i++;					
					}								
			}
			ftp_close($conn_id);				

			return $trans;
	}
  
	#Actualiza a estatus 1 (Env�o OK) el archivo de la transmisi�n
	function actualiza($row)
	{
			$sql = "UPDATE alTransaccionUnidadTbl".
      			 " SET hora = RPAD(hora, 50, ' ')".
      			 " WHERE prodStatus = '".$row['nombreArch']."'";
			//echo "$sql<br>";
			fn_ejecuta_query($sql);		        
      
      $sql = "UPDATE alTransaccionUnidadTbl".
      			 " SET hora = INSERT(hora, 50, 1, '1')".
      			 " WHERE prodStatus = '".$row['nombreArch']."'";
			//echo "$sql<br>";
			fn_ejecuta_query($sql);	
			
			rename($row['errorArch'], $row['retraArch']);
	}
*/
	
?>