<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/idiomas/mensajesES.php");
    require_once("alUnidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch ($_REQUEST['roRodadosActionHdn']) {
    	case 'asignacionRodados':
    		asignacionRodados();
    		break;
    	case 'getChoferesDisponibles':
    		getChoferesDisponiblesRodados();
    		break;
        case 'getRodadasAsignadas':
            getRodadasAsignadas();
            break;
    	case 'getChoferUnidad':
    		getChoferUnidad();
    		break;
    	case 'getGastos':
    		getGastosRodadas();
    		break;
    	case 'addGasto':
    		addGastoRodada();
    		break;
        case 'recepcionRodada':
            recepcionRodada();
            break;
        case 'comprobarRodada':
            comprobarRodada();
            break;
        case 'getGastosRodada':
            getGastosRodada();
            break;
        case 'getChoferesGastos':
            echo json_encode(getChoferesGastos());
            break;
    	default:
            echo '';
    }
    function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    function getRodadasAsignadas(){
        $lsWhereStr = " ";
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasChoferHdn'], "ua.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasVinHdn'], "ua.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasAvanzadaHdn'], "ua.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasCentroDistHdn'], "ua.centroDistribucion", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasAreaHdn'], "ua.area", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasClaveMovimientoHdn'], "hu.claveMovimiento", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetRodadas =    "SELECT hu.vin, hu.localizacionUnidad, hu.fechaEvento, hu.distribuidor, dc.descripcionCentro, hu.idTarifa, ".
                            "su.simboloUnidad, su.descripcion as descSimbolo, u.color,  cu.descripcion as descColor, ua.consecutivo, ".
                            "hu.claveMovimiento as cveMovHistorico, ua.claveChofer ".
                            //"(SELECT COUNT(*) + 1 FROM alHistoricoUnidadesTbl hu2 WHERE hu2.vin < hu.vin"
                            "FROM alHistoricoUnidadesTbl hu, alUnidadesTbl u, roUnidadesAsignadasChoferTbl ua, ".
                                 "casimbolosUnidadesTbl su, caColorUnidadestbl cu, caDistribuidoresCentrosTbl dc, caTarifasTbl tt ".
                            "WHERE u.vin = hu.vin ".
                            "AND hu.fechaEvento = (select max(hu2.fechaEvento) FROM alHistoricoUnidadesTbl hu2 WHERE hu.vin = hu2.vin) ".
                            "AND ua.vin = hu.vin ".
                            "AND u.simboloUnidad = su.simboloUnidad ".
                            "AND u.color = cu.color ".
                            "AND su.marca = cu.marca ".
                            "AND hu.distribuidor = dc.distribuidorCentro ".
                            "AND hu.idTarifa = tt.idTarifa".$lsWhereStr." GROUP BY hu.vin ORDER BY ua.consecutivo";

        $rs = fn_ejecuta_query($sqlGetRodadas);

        for ($i=0; $i < count($rs['root']) ; $i++) { 
            $rs['root'][$i]['descSimbolo'] = $rs['root'][$i]['simboloUnidad']." - ".$rs['root'][$i]['descSimbolo'];
            $rs['root'][$i]['descColor'] = $rs['root'][$i]['color']." - ".$rs['root'][$i]['descColor'];
            $rs['root'][$i]['descDist'] = $rs['root'][$i]['distribuidor']." - ".$rs['root'][$i]['descripcionCentro'];
        }

        echo json_encode($rs);
    }

    function getGastosRodada(){
        $lsWhereStr = " ";
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasIdGastoHdn'], "gr.idGastoRodada", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasVINHdn'], "gr.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasClaveMovimientoHdn'], "hu.claveMovimiento", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasClaveChoferHdn'], "gr.claveChofer", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
       
        if($_REQUEST['roRodadasClaveChoferHdn'] == ""){
            $sqlGastoRodada =   "SELECT gr.idGastoRodada, gr.vin, gr.claveChofer, hu.claveMovimiento, ".
                                "CONCAT(ch.nombre,' ', ch.apellidoPaterno,' ', ch.apellidoMaterno) as nombreChofer, ".
                                "(SELECT uac.vin ".
                                "FROM roUnidadesAsignadasChoferTbl uac ".
                                "WHERE uac.consecutivo =  (SELECT min(consecutivo) FROM roUnidadesAsignadasChoferTbl uac2 ".
                                                "WHERE uac2.claveChofer = uac.claveChofer) ".
                                "AND uac.claveChofer = gr.claveChofer ".
                                "AND uac.vin = gr.vin) = gr.vin as esElSig ".
                                "FROM roGastosRodadasTbl gr, alHistoricoUnidadesTbl hu, caChoferesTbl ch ".
                                "WHERE gr.vin LIKE '%".$_REQUEST['roRodadasVINHdn']."' ".
                                "AND gr.vin = hu.vin ".
                                "AND gr.claveChofer = ch.claveChofer ".
                                "AND hu.fechaEvento =  (SELECT MAX(hu2.fechaEvento) ".
                                                "FROM alhistoricounidadestbl hu2 ".
                                                "WHERE hu2.vin = hu.vin) ".
                                "AND hu.claveMovimiento = 'RE' ".
                                "GROUP BY gr.idGastoRodada, gr.vin, gr.claveChofer";

            $rs = fn_ejecuta_query($sqlGastoRodada);
        }

        if (!isset($rs) || count($rs['root']) == 1 ){

           
            $sqlGetRodada = "SELECT gr.idGastoRodada, gr.centroDistribucion, gr.claveChofer, ".
                            "gr.vin, gr.concepto, gr.importe, gr.idTarifa, co.nombre AS descConcepto ".
                            "FROM caConceptosTbl co, roGastosRodadasTbl gr ".
                            "WHERE gr.concepto = co.concepto ".$lsWhereStr;

            $rs = fn_ejecuta_query($sqlGetRodada);
            for ($i=0; $i < count($rs['root']) ; $i++) { 
                $rs['root'][$i]['descConcepto'] = $rs['root'][$i]['concepto']." - ".$rs['root'][$i]['descConcepto'];
            }
        }
        echo json_encode($rs);
    }

    function getChoferesDisponiblesRodados(){

    	$lsWhereHistStr = "ON ch.claveChofer = hu.claveChofer";
    	$lsWhereChoferStr = "WHERE ch.estatus = 1 ".
    						"AND ch.tipoChofer = 'CR' ".// CHOFER RODADAS
    						"AND ch.claveChofer NOT IN ".
                            "(SELECT tmp.claveChofer FROM trViajesTractoresTmp tmp WHERE tmp.claveChofer IS NOT NULL) ";

        $tablaTmp = "(select hu3.* from alhistoricoUnidadesTbl hu3 ".
					"where fechaEvento = (select max(tmp.fechaEvento) from ".
					"(select hu.* from alhistoricoUnidadestbl hu ".
					"where hu.fechaEvento = (select max(hu2.fechaEvento) from alhistoricounidadestbl hu2 ".
					"where hu2.vin = hu.vin)) as tmp where tmp.claveChofer = hu3.claveChofer)) as hu ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasClaveChoferHdn'], "ch.claveChofer", 0);
            $lsWhereChoferStr = fn_concatena_condicion($lsWhereChoferStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasClaveMovimientoHdn'], "hu.claveMovimiento", 1, 1);
            $lsWhereHistStr = fn_concatena_condicion($lsWhereHistStr, $lsCondicionStr);
        }

        $sqlGetTractoresDisponibles =  "SELECT ch.*, ".
						        	   "(SELECT COUNT(hu2.vin) FROM alHistoricoUnidadesTbl hu2 ".
						                                        "WHERE ch.claveChofer = hu2.claveChofer ".
						                                        "AND hu2.claveMovimiento = 'RA') AS numRodadasAsignadas, ".
						        	   "hu.claveMovimiento, hu.vin FROM cachoferestbl ch ".
									   "LEFT JOIN ".$tablaTmp." ".
									   $lsWhereHistStr." ".
									   $lsWhereChoferStr.
									   "ORDER BY ch.claveChofer;";

        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);
        
        for ($i=0; $i < count($rs['root']); $i++) { 
            $rs['root'][$i]['nombreChofer'] = $rs['root'][$i]['claveChofer']." - ".
                                                 $rs['root'][$i]['nombre']." ".
                                                 $rs['root'][$i]['apellidoPaterno']." ".
                                                 $rs['root'][$i]['apellidoMaterno'];
        }
            
        echo json_encode($rs);
    }

    function getChoferesGastos(){
        
        $lsWhereStr = "";
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasClaveChoferHdn'], "rg.claveChofer", 0, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasFechaInicioHdn'], "rg.fechaEvento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasFechaFinHdn'], "rg.fechaEvento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetChoferGasto = "SELECT ch.*, rg.*, '2336 - Alimentos' as descConcepto, CONCAT(ch.claveChofer, ' - ', ch.nombre, ' ', ch.apellidoPaterno, ' ', ch.apellidoMaterno) AS nombreChofer ".
                             "FROM caChoferesTbl ch ".
                             "INNER JOIN roGastosRodadasTbl rg ".
                             "ON rg.claveChofer = ch.claveChofer ".
                             $lsWhereStr.
                             " GROUP BY ch.claveChofer;";

        $rsChofer = fn_ejecuta_query($sqlGetChoferGasto);

        return $rsChofer;
    }

    function getChoferUnidad(){

    	$lsWhereStr = "";
    	if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasClaveChoferHdn'], "ch.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['roRodadasClaveMovimientoHdn'], "hu.claveMovimiento", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetChoferUnidad = "SELECT claveChofer, CONCAT(claveChofer, ' - ', nombre, ' ', apellidoPaterno, ' ', apellidoMaterno) AS nombreCompleto ".
        					  "FROM caChoferesTbl WHERE claveChofer = ".$_REQUEST['roRodadasClaveChoferHdn'];

        $rs = fn_ejecuta_query($sqlGetChoferUnidad);

        echo json_encode($rs);
    }

    function asignacionRodados(){
    	
    	$a['success'] = true;
    	$e['error'] = array();

    	if($_REQUEST['roRodadosAsignacionGrdArr'] == ""){ //Store Proveniente del Front End
    		$a['success'] = false;
    		$e[] = array('id'=> 'roRodadosAsignacionGrdArr', 'msg'=> getRequerido());
    		$a['errorMessage'] = getErrorRequeridos();
    	}
    	
    	if ($a['success'] == true) {

    		$rodadasArr = json_decode($_REQUEST['roRodadosAsignacionGrdArr'], true);

    		//1.-OBTIENE LA PLAZA DEL LOG-IN Y LA COMPARA CON EL DIST FINAL DE LA UNIDAD SI ES IGUAL ES LOCAL SINO ES FORANEO
    		$sqlGetPlazaCtoDistribucion = "SELECT idPlaza FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$_SESSION['usuCompania']."';";
    		$rsPlaza = fn_ejecuta_query($sqlGetPlazaCtoDistribucion);

    		//L-LOCAL
    		//F-FORANEO
    		//2.- INSERTA enUnidadesAsignadasChofer el Consecutivo y si es Viaje Foraneo o Local con respecto a las Plazas
    		$sqlAddAsignacion = "INSERT INTO roUnidadesAsignadasChoferTbl VALUES ";
    		$sqlUpdChoferes = "UPDATE caChoferesTbl SET ultimaUnidad = ";
            //3.-INSERTA EN EL HISTORICO CON RA-UNIDAD ASIGNADA
            for($i = 0; $i < count($rodadasArr); $i++){
                if($i > 0){
                	$sqlAddAsignacion .= ', ';
                }
                $sqlMaxConsecutivo = "(SELECT IFNULL(MAX(consecutivo) + 1, '1') FROM (SELECT consecutivo FROM roUnidadesAsignadasChoferTbl ".
                					 "WHERE claveChofer = ".$rodadasArr[$i]['CHOFER'].") AS rodadasTmp)";

				//4.-ACTUALIZA LA ULTIMA UNIDAD QUE TRAE EL CHOFER
				$sqlUpdChoferes = "UPDATE caChoferesTbl SET ultimaUnidad = ".
									"'".$rodadasArr[$i]['VIN']."' WHERE claveChofer = ".$rodadasArr[$i]['CHOFER'].";";

                $sqlAddAsignacion .= "(".$rodadasArr[$i]['CHOFER'].", ".$sqlMaxConsecutivo.", '".$rodadasArr[$i]['VIN']."', '".
                					 $rodadasArr[$i]['CHOFERCENTROORIGEN']."', ".
                					 "(SELECT IF((SELECT idPlaza FROM caDistribuidoresCentrosTbl ".
                					 "WHERE distribuidorCentro = '".$rodadasArr[$i]['DISTRIBUIDOR']."') = ".$rsPlaza['root'][0]['idPlaza'].", 'L', 'F') ))";

                fn_ejecuta_query($sqlUpdChoferes);
                //Agrega en Historico
                $data = addHistoricoUnidad($_SESSION['usuCompania'],$rodadasArr[$i]['VIN'], 'RA', 
                							$rodadasArr[$i]['DISTRIBUIDOR'],$rodadasArr[$i]['IDTARIFA'] ,$rodadasArr[$i]['LOCALIZACION'],
                							$rodadasArr[$i]['CHOFER'],'',$_SESSION['idUsuario'], $i*2);

                if ($data['success'] == false) {
                    $a['success'] = false;
                    $a['errorMessage'] = $data['errorMessage'];
                } else {
                    $a['successMsg'] = getHistoricoUnidadMasivoMsg();
                }
            }

            fn_ejecuta_query($sqlAddAsignacion);
            if(!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
            	$a['sql'] = $sqlAddAsignacion;
            	$a['successMessage'] = "Unidades Asignadas Correctamente";
            } else{
            	$a['success'] = false;
            	$a['errorMessage'] = $_SESSION['error_sql']."</br>".$sqlAddAsignacion;
            	$a['sql'] = $sqlAddAsignacion;
            }

        }
    	echo json_encode($a);
    }

    function getGastosRodadas(){
    	//CDANG
    	//2315 - COMBUSTIBLE
    	//2333 - PEAJE

    	$sqlGetCalculoGasto =  "SELECT ge.columna, ge.valor AS concepto, cc.centroDistribucion, cc.calculo, cc.importe, ".
    						   "(SELECT co.nombre FROM caConceptosTbl co WHERE co.concepto = cc.concepto) AS descConcepto ".
    						   "FROM caGeneralesTbl ge ".
							   "INNER JOIN caConceptosCentrosTbl cc ".
							   "ON ge.valor =  cc.concepto ".
							   "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
							   "WHERE ge.tabla = 'roGastosRodadasTbl' ".
							   "AND ge.columna = 'Base".$_SESSION['usuCompania']."';" ;

    	$rsCalculo = fn_ejecuta_query($sqlGetCalculoGasto);
    	$idx = count($rsCalculo['root']);
    	$rsCalculo['root'][$idx] = array('concepto' => '7001', 'importe' =>'0.00', 'calculo' => '0.00', 'descConcepto' => 'TOTAL GASTOS');
    	echo json_encode($rsCalculo);
    }

    function addGastoRodada(){

    	$a['success'] = true;
    	$e = array();
    	if($_REQUEST['roRodadasVin'] == ""){
    		$a['success'] = false;
    		$a['errorMessage'] = getErrorRequeridos();
    		$e[] = array('id' => 'roRodadasVin', 'msg' => getRequerido());
    	}
    	if($_REQUEST['roRodadasSimbolo'] == ""){
    		$a['success'] = false;
    		$a['errorMessage'] = getErrorRequeridos();
    		$e[] = array('id' => 'roRodadasVin', 'msg' => getRequerido());
    	}
    	if($_REQUEST['roRodadosCveChofer'] == ""){
    		$a['success'] = false;
    		$a['errorMessage'] = getErrorRequeridos();
    		$e[] = array('id' => 'roRodadosCveChofer', 'msg' => getRequerido());
    	}
    	if($_REQUEST['roRodadasIdTarifa'] == ""){
    		$a['success'] = false;
    		$a['errorMessage'] = getErrorRequeridos();
    		$e[] = array('id' => 'roRodadasIdTarifa', 'msg' => getRequerido());
    	}
    	if($_REQUEST['roRodadasDistribuidor'] == ""){
    		$a['success'] = false;
    		$a['errorMessage'] = getErrorRequeridos();
    		$e[] = array('id' => 'roRodadasDistribuidor', 'msg' => getRequerido());
    	}
    	if($_REQUEST['roRodadasTotal'] == ""){
    		$a['success'] = false;
    		$a['errorMessage'] = getErrorRequeridos();
    		$e[] = array('id' => 'roRodadasTotal', 'msg' => getRequerido());
    	}
    	$conceptos = explode('|', $_REQUEST['roRodadasConceptos']);
    	if(in_array('', $conceptos)){
    		$a['success'] = false;
    		$a['errorMessage'] = getErrorRequeridos();
    		$e[] = array('id' => 'roRodadasIdPlaza', 'msg' => getRequerido());
    	}
    	$importes = explode('|', $_REQUEST['roRodadasImportes']);
    	if(in_array('', $importes)){
    		$a['success'] = false;
    		$a['errorMessage'] = getErrorRequeridos();
    		$e[] = array('id' => 'roRodadasIdPlaza', 'msg' => getRequerido());
    	}

    	if($a['success'] == true){

	    	$sqlAddGasto = "INSERT INTO roGastosRodadasTbl(idGastoRodada, centroDistribucion, claveChofer, vin, secuencial, concepto, ".
	    				   "importe, idTarifa, plaza, area, fechaEvento) VALUES";
			
			$sqlGastoInfo = "SELECT (SELECT IFNULL(MAX(secuencial) + 1, '1') FROM roGastosRodadasTbl WHERE vin = '".$_REQUEST['roRodadasVin']."') AS sig, ";
			$sqlGastoInfo .= "(SELECT idPlaza FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$_SESSION['usuCompania']."') AS idPlazaCentro, ";
			$sqlGastoInfo .= "(SELECT idPlaza FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$_REQUEST['roRodadasDistribuidor']."') AS idPlazaDist, ";
            $sqlGastoInfo .= "(SELECT IFNULL(MAX(idGastoRodada) + 1, '1') FROM roGastosRodadasTbl) AS idGasto;";

			$rsPlaza = fn_ejecuta_query($sqlGastoInfo);
			$rsPlaza = $rsPlaza['root'];

			$area = $rsPlaza[0]['idPlazaCentro'] == $rsPlaza[0]['idPlazaDist'] ? 'L' : 'F';
					  
	    	for ($i=0; $i < count($conceptos) ; $i++) { 
	    		if($i > 0)
	    			$sqlAddGasto .= ", ";

	    		$sqlAddGasto .= "(".$rsPlaza[0]['idGasto'].", '".$_SESSION['usuCompania']."', ".$_REQUEST['roRodadosCveChofer'].", '".$_REQUEST['roRodadasVin']."', ".
	    						(intval($rsPlaza[0]['sig']) + $i).", '".$conceptos[$i]."', ".$importes[$i].", ".
	    						$_REQUEST['roRodadasIdTarifa'].", (SELECT plaza FROM caPlazasTbl WHERE idPlaza =".$rsPlaza[0]['idPlazaDist']."), '".$area."', '".
	    						date('Y-m-d')."')";
	    	}

	    	fn_ejecuta_query($sqlAddGasto);

	    	if(!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == ""){
				$a['sql'] = $sqlAddGasto;
	    		$a['successMessage'] = "Gastos Agregados Correctamente";
                $a['idGasto'] = $rsPlaza[0]['idGasto'];
	    	} else{
	    		$a['success'] = false;
	    		$a['sql'] =  $sqlAddGasto;
	    		$a['errorMessage'] = $_SESSION['error_sql']."</br>".$sqlAddGasto;
	     	}
     	}
     	$a['errors'] = $e;
     	echo json_encode($a);
    }

    function printGastoRodada(){
        if($_REQUEST['gastosRodadasIdGasto'] ){
            
        }

    }

    function recepcionRodada(){
        $a['success'] = true;
        $e = array();
        if($_REQUEST['roRodadasRodadasVin'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasRodadasVin', 'msg' => getRequerido());
        }
        if($_REQUEST['roRodadasDistribuidor'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasDistribuidor', 'msg' => getRequerido());
        }
        if($_REQUEST['roRodadasIdTarifa'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasIdTarifa', 'msg' => getRequerido());
        }
        if($_REQUEST['roRodadasLocalizacion'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasLocalizacion', 'msg' => getRequerido());
        }
        if($_REQUEST['roRodadasChofer'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasChofer', 'msg' => getRequerido());
        }
        if($a['success'] == true){
        //Agrega en Historico
            $data = addHistoricoUnidad($_SESSION['usuCompania'],$_REQUEST['roRodadasRodadasVin'], 'RE', // RE- RODADA ENTREGADA
                                        $_REQUEST['roRodadasDistribuidor'],$_REQUEST['roRodadasIdTarifa'] ,$_REQUEST['roRodadasLocalizacion'],
                                        $_REQUEST['roRodadasChofer'],'',$_SESSION['idUsuario'], 2);

            if ($data['success'] == false) {
                $a['success'] = false;
                $a['errorMessage'] = $data['errorMessage'];
            } else {
                $a['successMessage'] = "Unidad Recibida Correctamente";
                $a['successMsg'] = getHistoricoUnidadMasivoMsg();
            }
        }
        echo json_encode($a);
    }
    function comprobarRodada(){
        $a['success'] = true;
        $e = array();
        if ($_REQUEST['roRodadasRodadasVin'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasRodadasVin', 'msg' => getRequerido());
        }
        if ($_REQUEST['roRodadasDistribuidor'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasDistribuidor', 'msg' => getRequerido());
        }
        if ($_REQUEST['roRodadasIdTarifa'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasIdTarifa', 'msg' => getRequerido());
        }
        if ($_REQUEST['roRodadasLocalizacion'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasLocalizacion', 'msg' => getRequerido());
        }
        if ($_REQUEST['roRodadasChofer'] == ""){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id' => 'roRodadasChofer', 'msg' => getRequerido());
        }

        $conceptos = explode('|', $_REQUEST['roRodadasConceptos']);
        $importes = explode('|', $_REQUEST['roRodadasImportes']);

        if ($a['success'] == true){
            //1.- Si hay nuevos conceptos ingresados, inserta los conceptos en roGastosRodadasTbl
            if ($_REQUEST['roRodadasConceptos'] != "" && $_REQUEST['roRodadasImportes'] != ""){
                
                $sqlConceptos = "INSERT INTO roGastosRodadasTbl(idGastoRodada, centroDistribucion, claveChofer, vin, ".
                                "secuencial, concepto, importe, idTarifa, plaza, area, fechaEvento, claveMovimiento) VALUES ";
                for ($i=0; $i < count($conceptos); $i++) { 
                    
                }
            }
        }

    }
?>