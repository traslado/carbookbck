<?php

	/*********************************************
	Sólo necesita alUnidadesVinHdn para funcionar
	*********************************************/

	$savePath = "./../";

	//require_once("../funciones/fpdf/fpdf.php");
	require_once("../funciones/Barcode39.php");
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");

	$success = true;

	$vinArr = explode('|', substr($_REQUEST['alUnidadesVinHdn'], 0, -1));

	for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) { 
		if($vinArr[$nInt] != ""){
			$sqlGetDataStr = "SELECT h.vin, h.distribuidor, lp.fila, lp.lugar, h.claveMovimiento, dc.tipoDistribuidor, ".
									 "dc.observaciones AS puerto, dc.descripcionCentro , p.pais, h.localizacionUnidad AS patio, h.fechaEvento, pl.plaza ".
									 "FROM caDistribuidoresCentrosTbl dc, caDireccionesTbl di, caColoniasTbl co, ".
									 "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p, caPlazasTbl pl, alHistoricoUnidadesTbl h ".
									 "LEFT JOIN allocalizacionpatiostbl lp ON lp.vin = h.vin ".
									 "WHERE dc.distribuidorCentro = h.distribuidor ".
									 "AND pl.idPlaza = dc.idPlaza ".
									 "AND di.direccion = dc.direccionEntrega ".
									 "AND co.idColonia = di.idColonia ".
									 "AND m.idMunicipio = co.idMunicipio ".
									 "AND e.idEstado = m.idEstado ".
									 "AND p.idPais = e.idPais ".
									 "AND h.vin = '".$vinArr[$nInt]."' ".
									 "AND fechaEvento = (SELECT MAX(h1.fechaEvento) ".
									 "FROM alHistoricoUnidadesTbl h1 ". 
									 "WHERE h1.vin = h.vin AND (h1.claveMovimiento = 'EP' OR h1.claveMovimiento = 'PR')) ";

			$rs = fn_ejecuta_query($sqlGetDataStr);
			$data = $rs['root'][0];
			if (sizeof($data) > 0) {
				
				$handle = printer_open("\\\\10.1.2.122\\B-EX4T1") or die('No se puede conectar!');
				printer_set_option($handle,PRINTER_PAPER_FORMAT,PRINTER_FORMAT_CUSTOM);
				printer_set_option($handle,PRINTER_PAPER_LENGTH,384);
				printer_set_option($handle,PRINTER_PAPER_WIDTH,102);
				printer_start_doc($handle, "C:\\servidores\\apache\\htdocs\\carbookBck\\json\\etiqueta_01.txt");
				printer_start_page($handle);

				//printer_set_option($handle,PRINTER_RESOLUTION_X,100);
				//printer_set_option($handle,PRINTER_RESOLUTION_y,100);
				//printer_set_option($handle,PRINTER_PAPER_FORMAT,PRINTER_SCALE,200);
				//printer_set_option($handle, PRINTER_TEXT_ALIGN, PRINTER_TA_LEFT);

				if($data['tipoDistribuidor'] == 'DX') {
					$paisPlaza = substr($data['pais'],0,6);
					$puertoNombre = substr($data['puerto'], 0,9);
				} else {
					$paisPlaza = substr($data['plaza'],0,6);
					$puertoNombre = substr($data['descripcionCentro'],15, 30);
				}

				$font = printer_create_font("Arial",30,30,100,false,false, false,0);
				$font_01 = printer_create_font("Arial",250,30,200,false,false, false,0);
				$font_02 = printer_create_font("Arial",420,50,300,false,false, false,0);
				$font_03 = printer_create_font("Arial",40,20,300,false,false, false,0);
				$font_04 = printer_create_font("C39HrP48DhTt",100,30,100,false,false, false,0);	
				$font_05 = printer_create_font("Arial",40,20,300,false,false, false,0);						
				
				printer_select_font($handle, $font);
				//Distribuidor
				printer_draw_text($handle,$data['distribuidor'],20,10);


				printer_select_font($handle, $font_03);
				printer_draw_text($handle,"Patio ".$data['patio'],450,25);
				printer_draw_text($handle,"Fila ".$data['fila'],450,70);
				printer_draw_text($handle,"Cajon ".$data['lugar'],450,150);		

				printer_select_font($handle, $font_01);
				//pais
				printer_draw_text($handle,$paisPlaza,2,130);

				printer_select_font($handle, $font_02);
				//nombre del puerto				
				printer_draw_text($handle,$puertoNombre,2,400);
				
				printer_select_font($handle, $font_05);
				//fechaEvento
				printer_draw_text($handle,$data['fechaEvento'],170,860);
					

				//****************
				//CODIGO DE BARRAS
				//****************
				printer_select_font($handle, $font_04);
				printer_draw_text($handle,'*'.$data['vin'].'*',30,550);
			
				
				printer_delete_font($font);
				printer_delete_font($font_01);
				printer_delete_font($font_02);
				printer_delete_font($font_03);
				printer_delete_font($font_04);			
				printer_end_page($handle);
				printer_end_doc($handle);
				printer_close($handle);			
			}
		}
	}
?>	