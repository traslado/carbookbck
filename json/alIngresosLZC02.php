<?php

	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");

	switch($_REQUEST['alIngresoUnidadesLzcHdn']){
		case 'addLazaroL1':
			addLazaroL1();
			break;        
	     case 'addLazaroL2':
	        addLazaroL2(); 
	        break;
	     case 'addLazaroL3':
	        addLazaroL3(); 
	        break;
	     case 'addPDI':
	        addPDI();
	        	break; 
	     case 'getLogin':
	        getLogin();
	        	break;   
	     case 'comboVines':
	        comboVines();
	        	break;  
	     case 'PDI':
	        PDI();
	        	break;   
	     case 'comboVinesL2':
	        comboVinesL2();
	        	break; 
	     case 'comboPDI':
	        comboPDI();
	        	break; 
	     case 'localizacionUnidadesLzc':
	        localizacionUnidadesLzc();
	        	break; 
	     case 'deleteTmp':
	        deleteTmp();
	        	break; 
	     case 'comboNumeroVines':
	        comboNumeroVines();
	        	break; 
	     case 'comboAreaDano':
	        comboAreaDano();
	        	break; 
	     case 'comboTipoDano':
	        comboTipoDano();
	        	break; 
	     case 'comboSeveridadDano':
	        comboSeveridadDano();
	        	break; 
	     case 'insertarDanosTmp':
	        insertarDanosTmp();
	        	break; 
	     case 'borrarTmp':
	        borrarTmp();
	        	break; 
	     case 'comboDanosCapturados':
	        comboDanosCapturados();
	        	break; 
	     case 'guardarTbl':
	        guardarTbl();
	        	break;
	     case 'consultaVinesDif':
	        consultaVinesDif();
	        	break;  
	    default:
	        echo '';
	}

	function addLazaroL1(){

		$sqlGetUnidad = "SELECT vin FROM alHistoricoUnidadesTbl dy ".
							"WHERE dy.claveMovimiento='L2' ".
							"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ".
							"AND dy.vin not in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='PI')";
		$rsSqlGetUnidad= fn_ejecuta_query($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] != '0'){
			
			echo '0|Unidad Con Salida|';

		}else{

		    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
		                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
		                                "claveChofer, observaciones, usuario, ip) ".
		                                "VALUES(".
		                                "'LZC02',".
		                                "'".$_REQUEST['alIngresoVinHdn']."',".
		                                "NOW(),".
		                                "'L3',".
		                                "'DIPRU',".
		                                "'1',".
		                                "'LZC02',".
		                                replaceEmptyNull($RQchofer).",".
		                                "'UNIDAD INGRESO L3',".
		                                "'98',".
		                                "'".getClientIP()."')";
		    
		    $rs_02 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);     

		   $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
		    							"SET fechaEvento = NOW(), ".
		    							"claveMovimiento = 'L3', ".
		    							"observaciones= 'UNIDAD INGRESO L3' ".
		    							"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
		    							"AND claveMovimiento = 'PI' ";

		    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);


		    echo "1||||";
		}	     	
	}

	function addLazaroL2(){
	    
		$sqlGetUnidad = "SELECT vin FROM alHistoricoUnidadesTbl dy ".
							"WHERE dy.claveMovimiento='L1' ".
							"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ".
							"AND dy.vin not in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='L2')";
		$rsSqlGetUnidad= fn_ejecuta_query($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] == '0'){
			
			echo '0|Unidad ya capturada|';

		}else{
		    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
		                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
		                                "claveChofer, observaciones, usuario, ip) ".
		                                "VALUES(".
		                                "'LZC02',".
		                                "'".$_REQUEST['alIngresoVinHdn']."',".
		                                "NOW(),".
		                                "'L2',".
		                                "'DIPRU',".
		                                "'1',".
		                                "'LZC02',".
		                                replaceEmptyNull($RQchofer).",".
		                                "'UNIDAD INGRESO L2',".
		                                "'98',".
		                                "'".getClientIP()."')";
		    
		    $rs_01 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);

		    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
		    							"SET fechaEvento = NOW(), ".
		    							"claveMovimiento = 'L2', ".
		    							"observaciones= 'UNIDAD INGRESO L2' ".
		    							"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
		    							"AND claveMovimiento = 'L1' ";

		    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);

		   	echo "1||||";


		//	echo json_encode($rsSqlGetUnidad);
		}    	
	}
	function addPDI(){

		$sqlGetUnidad = "SELECT vin FROM alHistoricoUnidadesTbl dy ".
							"WHERE dy.claveMovimiento='PI' ".
							"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ";
							//"AND dy.vin  in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND ud.claveMovimiento ='PI')";
		$rsSqlGetUnidad= fn_ejecuta_query($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] != '0'){
			
			echo '0|Unidad Con PI|';

		}else{

			/*$sql="SELECT * FROM alinstruccionesmercedestbl ".
				"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ";
			$rs=fn_ejecuta_query($sql);


		    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
		                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
		                                "claveChofer, observaciones, usuario, ip) ".
		                                "VALUES(".
		                                "'LZC02',".
		                                "'".$_REQUEST['alIngresoVinHdn']."',".
		                                "NOW(),".
		                                "'PI',".
		                                "'".$rs['root'][0]['cveDisFac']."',".
		                                "'1',".
		                                "'LZC02',".
		                                replaceEmptyNull($RQchofer).",".
		                                "'UNIDAD INGRESO PI',".
		                                "'98',".
		                                "'".getClientIP()."')";
		    
		    $rs_02 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);     

		   $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
		    							"SET fechaEvento = NOW(), ".
		    							"claveMovimiento = 'PI', ".
		    							"observaciones= 'UNIDAD INGRESO PI' ".
		    							"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
		    							"AND claveMovimiento = 'L2' ";

		    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);*/


		    echo "1||||";
		}	     	
	}

	function comboNumeroVines(){

		$sqlVines ="SELECT vin FROM alinstruccionesmercedestmp ".
					"WHERE vin LIKE '%".$_REQUEST['alIngresoVinHdn']."%' ".
					"AND cveStatus = 'DK'";
		$rsVines = fn_ejecuta_query($sqlVines);
		//echo json_encode($sqlVines);

		if($rsVines['records'] == '0'){			
			echo '0|Unidad Ya capturada |';
		}else{
          $response = createResponseCombos($rsVines['root']);        

        echo $response;
        //   echo "1||||";

		}	    	
	}

	function localizacionUnidadesLzc(){

		$sqlGetUnidad = "SELECT tm.nomFac, ca.descripcion FROM alinstruccionesmercedestmp tm, casimbolosunidadestbl ca ".
							"WHERE tm.vin LIKE '%".$_REQUEST['alIngresoVinHdn']."%'".
							" AND tm.modelDesc = ca.simboloUnidad";
		$rsSqlGetUnidad= fn_ejecuta_query($sqlGetUnidad);
		//echo json_encode($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] == '0'){
			
			echo '0|Unidad Ya capturada |';

		}else{

          $response = createResponseCombos($rsSqlGetUnidad['root']);        

        echo "1|".$response;
        //   echo "1||||";

		}	    	
	}

	function createResponseCombos($root){
        $response = "";
        global $spChar;
        $keys = array_keys($root[0]);

        foreach ($root as $row) {
            foreach ($keys as $key) {
                if($row[$key] == $spChar){
                    $response .= ";";
                } else {
                    $response .= $row[$key]."|";
                }
            }
        }

    
        return $response;
    }   

    function deleteTmp(){

    	$sqlCount ="SELECT count(vin) as conteo FROM alinstruccionesmercedestmp ".
					"WHERE cveStatus='DK'";
		$rsCount =fn_ejecuta_query($sqlCount);			
		//echo json_encode($rsCount['root'][0]['conteo']);

		if ($rsCount['root'][0]['conteo'] == '1') {
			$delAlTmp = "DELETE FROM alinstruccionesmercedestmp ";
    		fn_ejecuta_query($delAlTmp);
		}

    	$updTmp ="UPDATE alinstruccionesmercedestmp SET cveStatus='RK' WHERE vin='".$_REQUEST['alIngresoVinHdn']."'";
    	fn_ejecuta_query($updTmp);

		echo "0|Unidad Procesada Correctamente|";
    }

    function comboAreaDano(){

		$sqlArea ="SELECT concat(valor,'-',nombre) as area FROM cageneralestbl ".
					"WHERE tabla ='cadanostbl' ".
					"AND columna='areaDano' ";
		$rsArea = fn_ejecuta_query($sqlArea);
		//echo json_encode($sqlVines);

		if($rsArea['records'] == '0'){			
			echo '0|Unidad Ya capturada |';
		}else{
          $response = createResponseCombos($rsArea['root']);        

        echo $response;
        //   echo "1||||";

		}	    	
	}

	function comboTipoDano(){

		$sqlArea ="SELECT concat(valor,'-',nombre) as area FROM cageneralestbl ".
					"WHERE tabla ='cadanostbl' ".
					"AND columna='tipoDano' ";
		$rsArea = fn_ejecuta_query($sqlArea);
		//echo json_encode($sqlVines);

		if($rsArea['records'] == '0'){			
			echo '0|Unidad Ya capturada |';
		}else{
          $response = createResponseCombos($rsArea['root']);        

        echo $response;
        //   echo "1||||";

		}	    	
	}

	function comboDanosCapturados(){

		$sqlArea ="SELECT observaciones FROM aldanosunidadestmp
					WHERE vin ='".$_REQUEST['alIngresoVinHdn']."' ";
		$rsArea = fn_ejecuta_query($sqlArea);
		//echo json_encode($sqlVines);

		if($rsArea['records'] == '0'){			
			echo '0|Unidad Ya capturada |';
		}else{
          $response = createResponseCombos($rsArea['root']);        

        echo $response;
        //   echo "1||||";

		}	    	
	}

	function comboSeveridadDano(){

		$sqlArea ="SELECT concat(valor,'-',nombre) as area FROM cageneralestbl ".
					"WHERE tabla ='cadanostbl' ".
					"AND columna='severidadDano' ";
		$rsArea = fn_ejecuta_query($sqlArea);
		//echo json_encode($sqlVines);

		if($rsArea['records'] == '0'){			
			echo '0|Unidad Ya capturada |';
		}else{
          $response = createResponseCombos($rsArea['root']);        

        echo $response;
        //   echo "1||||";

		}	    	
	}

	function insertarDanosTmp(){
		$insDanosTmp ="INSERT INTO aldanosunidadestmp (idDano, centroDistribucion, vin, observaciones) ".
 						"VALUES ('".substr($_REQUEST['areaDanoCmb'],0,2).substr($_REQUEST['tipoDanoCmb'],0,2).substr($_REQUEST['severidadDanoCmb'],0,2).
 						"', 'LZC02', '".$_REQUEST['alIngresoVinHdn']."', '".$_REQUEST['areaDanoCmb']."-".$_REQUEST['tipoDanoCmb']."-".$_REQUEST['severidadDanoCmb']."')";
 		fn_ejecuta_query($insDanosTmp);

 		echo "0|Daño Agregado Correctamente|";

	}

	function borrarTmp(){
		$delTmpDanos ="DELETE FROM aldanosunidadestmp ".
						"WHERE vin ='".$_REQUEST['alIngresoVinHdn']."'";
		fn_ejecuta_query($delTmpDanos);

		echo "0|Proceso Cancelado|";
	}

	function guardarTbl(){	

		$sqlVIN ="SELECT * FROM alinstruccionesmercedestbl ".
				  " WHERE vin like '%".$_REQUEST['alIngresoVinHdn']."%'";
		$rsVIN =fn_ejecuta_query($sqlVIN);


		$sqlTmp ="SELECT * FROM aldanosunidadestmp ".
				  " WHERE vin ='".$_REQUEST['alIngresoVinHdn']."'";
		$rsTmp =fn_ejecuta_query($sqlTmp);
		//echo json_encode($rsTmp);

		for ($i=0; $i <sizeof($rsTmp['root']) ; $i++) { 
			$sqlExistCaDanos ="SELECT * FROM cadanostbl ".
							  "WHERE areaDano='".substr($rsTmp['root'][$i]['idDano'],0,2)."' ".
							  "AND tipoDano='".substr($rsTmp['root'][$i]['idDano'],2,2)."' ".
							  "AND severidadDano='".substr($rsTmp['root'][$i]['idDano'],4,2)."'";
			$rsExistCaDanos = fn_ejecuta_query($sqlExistCaDanos);
			//echo json_encode($rsExistCaDanos);

			if ($rsExistCaDanos['root'][$i]['idDano'] == null) {
				$sqlInsCaDanos ="INSERT INTO cadanostbl (areaDano, tipoDano, severidadDano) ".
								"VALUES ('".substr($rsTmp['root'][$i]['idDano'],0,2).
									 "', '".substr($rsTmp['root'][$i]['idDano'],2,2).
									 "', '".substr($rsTmp['root'][$i]['idDano'],4,2)."')";
				fn_ejecuta_query($sqlInsCaDanos);

				$sqlExistCaDanos ="SELECT * FROM cadanostbl ".
							  "WHERE areaDano='".substr($rsTmp['root'][$i]['idDano'],0,2)."' ".

							  
							  "AND severidadDano='".substr($rsTmp['root'][$i]['idDano'],4,2)."'";
				$rsExistCaDanos = fn_ejecuta_query($sqlExistCaDanos);
				echo json_encode($rsExistCaDanos);

				$insAlDanosTbl ="INSERT INTO aldanosunidadestbl(idDano, centroDistribucion, vin, observacion)".
									"SELECT ".$rsExistCaDanos['root'][0]['idDano'] ." as idDano, centroDistribucion, '". $rsVIN['root'][0]['vin'] ."' as vin, observaciones FROM aldanosunidadestmp ".
									"WHERE idDano ='".substr($rsTmp['root'][$i]['idDano'],0,2).substr($rsTmp['root'][$i]['idDano'],2,2).substr($rsTmp['root'][$i]['idDano'],4,2)."'";
				fn_ejecuta_query($insAlDanosTbl);
			}else{
				$insAlDanosTbl ="INSERT INTO aldanosunidadestbl(idDano, centroDistribucion, vin, observacion)".
									"SELECT ".$rsExistCaDanos['root'][0]['idDano'] ." as idDano, centroDistribucion, '". $rsVIN['root'][0]['vin'] ."' as vin, observaciones FROM aldanosunidadestmp ".
									"WHERE idDano ='".substr($rsTmp['root'][$i]['idDano'],0,2).substr($rsTmp['root'][$i]['idDano'],2,2).substr($rsTmp['root'][$i]['idDano'],4,2)."'";
				fn_ejecuta_query($insAlDanosTbl);

			}

		}

		
		$delTmpDanos ="DELETE FROM aldanosunidadestmp ".
						"WHERE vin ='".$_REQUEST['alIngresoVinHdn']."'";
		fn_ejecuta_query($delTmpDanos);

		

		$sqlGetUnidad = "SELECT vin FROM alHistoricoUnidadesTbl dy ".
							"WHERE dy.claveMovimiento='PI' ".
							//"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ";
							" AND vin like '%".$_REQUEST['alIngresoVinHdn']."%'";
							//"AND dy.vin  in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND ud.claveMovimiento ='PI')";
		$rsSqlGetUnidad= fn_ejecuta_query($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] != '0'){
			
			
		}else{

			$sql="SELECT * FROM alinstruccionesmercedestbl ".
				" WHERE vin like '%".$_REQUEST['alIngresoVinHdn']."%'";
			$rs=fn_ejecuta_query($sql);

			$sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
			                            "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
			                            "claveChofer, observaciones, usuario, ip) ".
			                            "VALUES(".
			                            "'LZC02',".
			                            "'".$rs['root'][0]['vin']."',".
			                            "NOW(),".
			                            "'PI',".
			                            "'".$rs['root'][0]['cveDisFac']."',".
			                            "'1',".
			                            "'LZC02',".
			                            replaceEmptyNull($RQchofer).",".
			                            "'UNIDAD INGRESO PI',".
			                            "'98',".
			                            "'".getClientIP()."')";

			$rs_02 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);     

			$sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
										"SET fechaEvento = NOW(), ".
										"claveMovimiento = 'PI', ".
										"observaciones= 'UNIDAD INGRESO PI' ".
										//"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
										" WHERE vin like '%".$_REQUEST['alIngresoVinHdn']."%'";
										"AND claveMovimiento = 'L2' ";
			$rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);
		}

		echo "0|Daños Guardados Correctamente|";

	}   

	function consultaVinesDif(){
		$sqlVinesDif ="SELECT vin FROM alinstruccionesmercedestmp ".
						"WHERE vin like '%".$_REQUEST['alIngresoVinHdn']."%'";
		$rsVinesDif = fn_ejecuta_query($sqlVinesDif);
		//echo json_encode($sqlVines);

		if($rsVinesDif['records'] == '0'){			
			echo '0|Unidad no Existente |';
		}else{
          $response = createResponseCombos($rsVinesDif['root']);        

        echo $response;
        //   echo "1||||";

		}	    	
	}
?>