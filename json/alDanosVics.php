<?php
	session_start();
	
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);
	
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    } 

	switch($_REQUEST['alDanosVicsActionHdn']){
		case 'addDanosVics':
			echo json_encode(addDanosVics($_REQUEST['alDanosTipoDanoCmb'],$_REQUEST['alDanosVicsVinTxt'],
								$_REQUEST['alDanosNumeroDanosTxt'], $_REQUEST['alDanosIdDanoTxt']));
			break;
		case 'validaDano':
			validaDano();
			break;	
		case 'getUltimoDanosVics':
			echo json_encode(getUltimoDanosVics());
			break;
		default:
            echo '';
	}

	function addDanosVics($origen, $vin, $numDanos, $idDano, $centroSesion = ""){
		$a = array();
        $e = array();
        $a['success'] = true;

		$vinArr = explode('|', $vin);
		$numDanosArr = explode('|', $numDanos);
		$idDanoArr = explode('|', $idDano);

		if($origen == ''){
			$a['success'] = false;
			$e[] = array('id'=>'alDanosTipoDanoCmb', 'msg'=>getRequerido());
			$a['errorMsg'] = getErrorRequeridos();
		}
		if(in_array('', $vinArr)){
			$a['success'] = false;
			$e[] = array('id'=> 'alDanosVicsVinTxt','msg' => getRequerido(), 'size'=>count($vinArr));
			$a['errorMsg'] = getErrorRequeridos();
		}
		if(in_array('', $numDanosArr)){
			$a['success'] = false;
			$e[] = array('id'=> 'alDanosNumeroDanosTxt','msg' => getRequerido(), 'size'=>count($numDanosArr));
			$a['errorMsg'] = getErrorRequeridos();
		}
		if(in_array('', $idDanoArr)){
			$a['success'] = false;
			$e[] = array('id'=> 'alDanosIdDanoTxt','msg' => getRequerido());
			$a['errorMsg'] = getErrorRequeridos();
		}

		if($centroSesion == ""){
			$centroSesion = $_SESSION['usuCompania'];
		}

		if($a['success'] == true){
			$sqlAddDanoStr = "";
			for($i = 0; $i< count($vinArr); $i++){
				if($i == 0){
					$sqlAddDanoStr = "INSERT INTO alDanosVicsTbl(centroDistribucion, vin, numeroDanos, ".
									 "tipoDano, idDano, fechaEvento) ".
									 "VALUES( '".$centroSesion."', ".
									 "'".$vinArr[$i]."', ".
									 "'".$numDanosArr[$i]."', ".
									 "'".$origen."', ".
									 $idDanoArr[$i].", ".
									 "'".date('Y/m/d H:i:s', time() + $i)."')";
				}else{
					$sqlAddDanoStr .= ", ('".$centroSesion."', '".$vinArr[$i]."', '".$numDanosArr[$i]."', '".
									  $origen."', ".$idDanoArr[$i].", '".date('Y/m/d H:i:s', time() + $i)."')";
				}
			}

			fn_ejecuta_Add($sqlAddDanoStr);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
				$a['successMessage'] = getDanosVicsSuccessMsg();
				$a['sql'] = $sqlAddDanoStr;
			}else{
				$a['success'] = false;
				$a['errorMessage'] = mysql_errno().' - '.$sqlAddDanoStr;
			}
		}			
		$a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();

		return $a;
	}

	function getUltimoDanosVics(){
		$lsWhereStr = "";
		$a['success'] = true;
		$e = array();

		if($_REQUEST['alDanosVicsVinTxt'] == ''){
			$e[] = array('id'=>'alDanosVicsVinTxt', 'msg'=>getRequerido());
			$a['success'] = false;
			$a['errorMessage'] = getErrorRequeridos();
			$a['root'] = array(); 
		}

		if($a['success'] == true){

			$sqlQuery = "SELECT MAX(numeroDanos) AS ultimoDano FROM alDanosVicsTbl ".
						"WHERE vin = '".$_REQUEST['alDanosVicsVinTxt']."'";

			$rs = fn_ejecuta_query($sqlQuery);
			$a = $rs;
		}
		return $a;
	}

	function validaDano(){
		$sqlDano="SELECT * FROM cadanostbl ".
				  "WHERE areaDano='".$_REQUEST['caDanosAreaDanoTxt']."' ".
				  "AND tipoDano='".$_REQUEST['caDanosTipoDanoTxt']."' ".
				  "AND severidadDano='".$_REQUEST['caDanosSeveridadDanoTxt']."' ";	

		$rsSqlDano=fn_ejecuta_query($sqlDano);

		echo json_encode($rsSqlDano);
	}
?>