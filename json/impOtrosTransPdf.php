<?php
    session_start();
    setlocale(LC_TIME, 'es_MX.utf8');
    $codigoTalonEspecial = 'TE';

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    
    /*if(isset($_REQUEST['actionHdn']) && $_REQUEST['actionHdn'] == "buscaFolio"){
        buscaFolio();
    }
    else
    {
        if($_REQUEST['esTractor12Hdn'] != ""){
            imprimirTalon12($_REQUEST['trViajesTractoresIdViajeHdn']);
        } else{
            imprimirTalon();
        }       
    }*/

     imprimirTalon();

    function buscaFolio(){
        $sql = "SELECT tv.*, tr.tractor, vt.viaje, tv.numeroUnidades, tr.rendimiento, vt.numeroRepartos AS distribuidores, vt.kilometrosTabulados, ".
               "concat(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as operador, cast(tv.fechaEvento as date) as fechaEvento,".
               "(SELECT count(tl1.distribuidor) FROM trtalonesviajestbl tl1 WHERE tl1.idViajeTractor = vt.idViajeTractor) as talones, ".
               "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS plazaOrigen, ".
               "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaDestino) AS plazaDestino ".
               "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch ".
               "WHERE tv.idViajeTractor = vt.idViajeTractor ".
               "AND vt.idTractor = tr.idTractor ".
               "AND vt.claveChofer = ch.claveChofer ".
               "AND tr.compania = '".$_REQUEST['compania']."' ".
               "AND tv.folio = ".$_REQUEST['folio']." ".
               "AND tv.centroDistribucion= '" . $_SESSION['usuCompania']."' ".
               "AND tv.tipotalon = '".$_REQUEST['tipoTalon']."'";
         //echo "$sql<br>";
         $rsTalon = fn_ejecuta_query($sql);
         
         echo json_encode($rsTalon);
    }


    function imprimirTalon(){
        $a = array();
        $e = array();
        $a['success'] = true;
        $a['msjResponse'] = 'Reimpresi&oacuten generada correctamente.';
        global $codigoTalonEspecial;

        if($_REQUEST['trViajesTractoresIdViajeHdn'] != ""){

            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                     "AND tv.claveMovimiento != 'TX' ".
                                     "AND tv.centroDistribucion= '" . $_SESSION['usuCompania'] . "' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].")";
        } else {
            /*if ($_REQUEST['trViajesTractoresFoliosHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresFoliosHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
           /* if ($_REQUEST['trViajesTractoresCompaniaHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresCompaniaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }*
            $tipoTalon = '';
            if(isset($_REQUEST['tipoTalon']))
            {
                $tipoTalon = " AND tv.tipotalon = '".$_REQUEST['tipoTalon']."'";
            }

            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].") ".
                                     "AND tv.centroDistribucion= '" . $_SESSION['usuCompania'] . "' ".
                                     $tipoTalon;*/

        }

        if ($a['success']) {

          $pdf = new FPDF('P', 'mm','A4');


          //echo "$sqlGetTalonesViajeStr<br>";
          $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);
          //echo json_encode($sqlGetTalonesViajeStr);
          $varInc = 0;

          //echo json_encode($rsTalon);

          for ($nInt=1 ; $nInt <= sizeof($rsTalon['root']) ; $nInt++) {


            $sqlGetTalonStr = "SELECT tv.*, dc.descripcionCentro, tr.tractor, ".//co.descripcion AS nombreCiaRemitente, co.rfc AS rfcRemitente, co.direccion AS dirCompania, 
                              "vt.claveChofer, ch.*, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist,".
                              "(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$varInc]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                              "(SELECT pl.plaza FROM caPlazasTbl pl ".
                              "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                              "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                              "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                              "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                              "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                              "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".
                              "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caChoferesTbl ch, ".
                              "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                              "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".// caCompaniasTbl co,
                              "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                              "AND dc.distribuidorCentro = tv.distribuidor ".
                              // "AND co.compania = tv.companiaRemitente ".
                              "AND tr.idTractor = vt.idTractor ".
                              "AND ch.claveChofer = vt.claveChofer ".
                              "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                              "WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                              "AND col.idColonia = di.idColonia ".
                              "AND mu.idMunicipio = col.idMunicipio ".
                              "AND es.idEstado = mu.idEstado ".
                              "AND pa.idPais = es.idPais ".
                              "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                              "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                               "AND tv.centroDistribucion= '" . $_SESSION['usuCompania'] . "' ".
                              "AND tv.idTalon = ".$rsTalon['root'][$varInc]['idTalon']." ".
                              "AND tv.claveMovimiento != 'TX' ";

            $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

            //echo json_encode($sqlGetTalonStr);

            if(sizeof($dataTalon['root']) > 0){
              $selStr = "SELECT * FROM caCompaniasTbl WHERE compania = '".$dataTalon['root'][0]['companiaRemitente']."'";
              $ciaRst = fn_ejecuta_query($selStr);
              if ($ciaRst['records'] > 0) {
                $dataTalon['root'][0]['nombreCiaRemitente'] = $ciaRst['root'][0]['descripcion'];
                $dataTalon['root'][0]['rfcRemitente'] = $ciaRst['root'][0]['rfc'];
                $dataTalon['root'][0]['dirCompania'] = $ciaRst['root'][0]['direccion'];
              } else {
                $selStr = "SELECT * FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$dataTalon['root'][0]['companiaRemitente']."'";
                $ciaRst = fn_ejecuta_query($selStr);
                $dataTalon['root'][0]['nombreCiaRemitente'] = $ciaRst['root'][0]['descripcionCentro'];
                $dataTalon['root'][0]['rfcRemitente'] = $ciaRst['root'][0]['rfc'];
                $dataTalon['root'][0]['dirCompania'] = $ciaRst['root'][0]['direccionEntrega'];
              }

              if($dataTalon['root'][0]['dirCompania'] != ''){

                $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                              "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                              "caEstadosTbl es, caPaisesTbl pa ".
                                              "WHERE pa.idPais = es.idPais ".
                                              "AND es.idEstado = mu.idEstado ".
                                              "AND mu.idMunicipio = co.idMunicipio ".
                                              "AND co.idColonia = dir.idColonia ".
                                              "AND dir.direccion = '".$dataTalon['root'][0]['dirCompania']."'";

                $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio']." ".$dirCompania['root'][0]['cp'];
                $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
              }

              $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ".
                                        "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                        "WHERE un.vin = ut.vin ".
                                        "AND su.simboloUnidad = un.simboloUnidad ".
                                        "AND ut.idTalon =".$rsTalon['root'][$varInc]['idTalon']." ".
                                        "AND ut.estatus != 'C' ";

              $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);


              if(sizeof($unidadesTalon['root']) > 0){

                $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                $dataTalon['root'][0]['apellidoPaterno']." ".
                $dataTalon['root'][0]['apellidoMaterno'];
                $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];

              if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                $sqlGetServEspStr = "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                    "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                    "WHERE dir.direccion = de.direccionDestino ".
                                    "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                    "AND dir.idColonia = co.idColonia ".
                                    "AND co.idMunicipio = mu.idMunicipio ".
                                    "AND mu.idEstado = es.idEstado ".
                                    "ANd es.idPais = pa.idPais ".
                                    "AND pl.idPlaza = dc.idPlaza ".
                                    "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                    "AND de.idDestinoEspecial =(SELECT MAX(idDestinoEspecial) FROM aldestinosespecialestbl where vin ='".$unidadesTalon['root'][0]['vin']."') ";

                $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);

                for ($nInt01=0; $nInt01 < sizeof($dataServEsp['root']); $nInt01++) {

                  $dataServEsp['root'][$nInt01]['descDist'] = $dataServEsp['root'][$nInt01]['distribuidor']." - ".
                  $dataServEsp['root'][$nInt01]['descripcionCentro'];

                  $dataServEsp['root'][$nInt01]['dirL1'] = $dataServEsp['root'][$nInt01]['calleNumero'];
                  $dataServEsp['root'][$nInt01]['dirL2'] = $dataServEsp['root'][$nInt01]['colonia'].", ".$dataServEsp['root'][$nInt01]['municipio']." ".$dataServEsp['root'][$nInt01]['cp'];
                  $dataServEsp['root'][$nInt01]['dirL3'] = $dataServEsp['root'][$nInt01]['estado'].", ".$dataServEsp['root'][$nInt01]['pais'];
                }
              }


            } else {
              $pdf->AddPage();
              $pdf->SetY(92+(4*$nInt)+0);
              $pdf->SetX(25+0);
              $pdf->Cell(17,3, 'TALON #'.$rsTalon['root'][$varInc]['idTalon']. ' SIN UNIDADES',0, 'C');
            }
          }
            $varInc ++ ;
        }
      }
                      generarPdf($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);

        //Output
        $pdf->Output('../../talon.pdf', I);
    }
    function imprimirTalon12($idViaje){
        $a = array();
        $e = array();
        $a['success'] = true;

        //SOLO PARA EL CASO DEL FONDO CON EL TALON ESCANEADO
        $fondoPath = "../img/impTalon.jpg";

        if ($idViaje == "") {
            $e[] = array('id'=>'idViaje','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success']) {
            $pdf = new FPDF('P', 'mm', 'A4');
            #Establecemos los márgenes izquierda, arriba y derecha:
            $pdf->SetMargins(215, 215 , 215);

            #Establecemos el margen inferior:
            $pdf->SetAutoPageBreak(true,215);

            $sqlGetTalonesViajeStr = "SELECT idTalon FROM trTalonesViajesTbl WHERE idViajeTractor = ".$idViaje ;

            $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);

            for ($nInt=0; $nInt < sizeof($rsTalon['root']); $nInt++) {


/*
$sqlGetTalonStr =  "SELECT   tv.*, dc.descripcionCentro AS nombreCiaRemitente, dc.rfc AS rfcRemitente, ".
"dc.direccionFiscal AS dirCompania, dc.descripcionCentro, tr.tractor, ".
"vt.claveChofer, ch.*, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, ".
"kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist,(SELECT sum(su.pesoAproximado) ".
"FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
"WHERE un.vin = ut.vin AND su.simboloUnidad = ".
"un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
"(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
"(SELECT pl2.plaza FROM ".
"caPlazasTbl pl2 WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
"(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
"(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".
"FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caChoferesTbl ch, caDistribuidoresCentrosTbl dc, ".
"caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, caMunicipiosTbl mu, ".
"caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
"WHERE tv.idViajeTractor = vt.idViajeTractor ".
"AND dc.distribuidorCentro = tv.distribuidor  ".
"AND dc.distribuidorCentro = tv.companiaRemitente ".
"AND tr.idTractor = vt.idTractor ".
"AND ch.claveChofer = vt.claveChofer ".
"AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 WHERE dc4.distribuidorCentro = tv.distribuidor) ".
"AND col.idColonia = di.idColonia ".
"AND mu.idMunicipio = col.idMunicipio ".
"AND es.idEstado = mu.idEstado ".
"AND pa.idPais = es.idPais ".
"AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
"AND kp.idPlazaDestino = tv.idPlazaDestino ".
"AND tv.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." ".
"AND tv.claveMovimiento != 'TX' limit 1 " ;



$dataTalon = fn_ejecuta_query($sqlGetTalonStr);
*/



               $sqlGetTalonStr = "SELECT tv.*, co.descripcion AS nombreCiaRemitente, co.rfc AS rfcRemitente, co.direccion AS dirCompania, dc.descripcionCentro, tr.tractor, ".
                                  "vt.claveChofer, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist, ".
                                  "(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                                  "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                        "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                                  "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                        "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                                  "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                                        "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                                  "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".
                                  "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caCompaniasTbl co, ".
                                    "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                                    "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
                                  "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                  "AND dc.distribuidorCentro = tv.distribuidor ".
                                  "AND co.compania = tv.companiaRemitente ".
                                  "AND tr.idTractor = vt.idTractor ".
                                  "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                                  "WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                                  "AND col.idColonia = di.idColonia ".
                                  "AND mu.idMunicipio = col.idMunicipio ".
                                  "AND es.idEstado = mu.idEstado ".
                                  "AND pa.idPais = es.idPais ".
                                  "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                                  "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                                  "AND idTalon = ".$rsTalon['root'][$nInt]['idTalon'];

                $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

                if(sizeof($dataTalon['root']) > 0){
                    if($dataTalon['root'][0]['dirCompania'] != ''){
                        $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                        "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                                        "caEstadosTbl es, caPaisesTbl pa ".
                                                        "WHERE pa.idPais = es.idPais ".
                                                        "AND es.idEstado = mu.idEstado ".
                                                        "AND mu.idMunicipio = co.idMunicipio ".
                                                        "AND co.idColonia = dir.idColonia ".
                                                        "AND dir.direccion = ".$dataTalon['root'][0]['dirCompania'];

                        $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                        $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                        $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio']." ".$dirCompania['root'][0]['cp'];
                        $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
                    }

                    $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ,(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal ".
                                                "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                                "WHERE un.vin = ut.vin ".
                                                "AND su.simboloUnidad = un.simboloUnidad ".
                                                "AND ut.idTalon =".$rsTalon['root'][$nInt]['idTalon']." ".
                                                "AND ut.estatus != 'C' ";

                    $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);

                    //echo $rSumaPeso[0]['totalPeso'];

                    if(sizeof($unidadesTalon['root']) > 0){
                        $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                        $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                        $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                                                                $dataTalon['root'][0]['apellidoPaterno']." ".
                                                                $dataTalon['root'][0]['apellidoMaterno'];
                        $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                        $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                        $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];

                        if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                            $sqlGetServEspStr = "SELECT de.distribuidorOrigen AS distribuidor, dc.descripcionCentro, de.direccionOrigen AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionOrigen ".
                                                "AND dc.distribuidorCentro = de.distribuidorOrigen ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                                "UNION ".
                                                "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionDestino ".
                                                "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."'";

                            $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);


                            for ($nInt=0; $nInt < sizeof($dataServEsp['root']); $nInt++) {
                                $dataServEsp['root'][$nInt]['descDist'] = $dataServEsp['root'][$nInt]['distribuidor']." - ".
                                                                            $dataServEsp['root'][$nInt]['descripcionCentro'];

                                $dataServEsp['root'][$nInt]['dirL1'] = $dataServEsp['root'][$nInt]['calleNumero'];
                                $dataServEsp['root'][$nInt]['dirL2'] = $dataServEsp['root'][$nInt]['colonia'].", ".$dataServEsp['root'][$nInt]['municipio']." ".$dataServEsp['root'][$nInt]['cp'];
                                $dataServEsp['root'][$nInt]['dirL3'] = $dataServEsp['root'][$nInt]['estado'].", ".$dataServEsp['root'][$nInt]['pais'];
                            }
                        }
                        //print_r($dirCompania['root'][0]);
                        $pdf = generarPdf($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);
                    } else {
                        $a['errorMessage'] = "ESTE TALON NO TIENE UNIDADES";
                        return $a;
                    }
                } else {
                    $a['errorMessage'] = "ERROR AL INTENTAR CARGAR EL TALON ".$rsTalon['root'][$nInt]['idTalon'];
                    return $a;
                }
            }

            echo json_encode($a);
        }
    }

    function generarPdf($pdf, $fondo, $data, $dirCompania, $unidades, $especiales){

        global $codigoTalonEspecial;
        $border = 0;
        $font = 'Arial';
        $offsetX = 0;
        $offsetY = 0;

        $pdf->AddPage();
        $pdf->SetFont($font,'U',25);
        $pdf->SetAutoPageBreak(false);

        if ($data['centroDistribucion']=='CDSLO') {
          $leyenda="  CARTA  PORTE ";
        }else{
          $leyenda="HOJA   DE   ENTREGA";
        }

        $pdf->SetY(8+$offsetY);
        $pdf->SetX(60+$offsetX);
        $pdf->Cell(130,3,$leyenda, $border, 0, 'L');

        $pdf->SetFont($font,'B',6);


        $pdf->Image('../json/images/logo.jpeg' , 25 ,15, 65 , 20,'JPG', 'http://www.desarrolloweb.com');

        $pdf->SetY(35+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(130,3," TRANSPORTE DE AUTOMOVILES SIN RODAR EN VEHICULOS TIPO GONDOLA", $border, 0, 'L');

        $pdf->SetY(37+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(130,3," RINES Y PARTES AUTOMOTRICES EN VEHICULOS TIPO CELDILLAS", $border, 0, 'L');

        $pdf->SetY(39+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(130,3," CAMINOS DE JURISDICCION FEDERAL", $border, 0, 'L');


        $pdf->SetY(41 +$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(130,3," TEL 722 2360490 AL 99", $border, 0, 'L');


         $pdf->SetFont($font,'B',9);

         //Compania


         if ($data['companiaTractor']=='TRANSDRIZA') {
            $pdf->SetY(19+$offsetY);
            $pdf->SetX(100+$offsetX);
            $pdf->Cell(130,3,"TRANSDRIZA S.A. DE C.V.", $border, 0, 'L'); 

            $pdf->SetY(22+$offsetY);
            $pdf->SetX(100+$offsetX);
            $pdf->Cell(130,3,"TRA891031SXA", $border, 0, 'L');

            $pdf->SetY(25+$offsetY);
            $pdf->SetX(100+$offsetX);
            $pdf->Cell(130,3,"Regimen: 624.Coordinados", $border, 0, 'L');      
         }else{
            $pdf->SetY(19+$offsetY);
            $pdf->SetX(100+$offsetX);
            $pdf->Cell(130,3,"AUTO TRANSPORTES CHRYMEX S.A. DE C.V.", $border, 0, 'L'); 

            $pdf->SetY(22+$offsetY);
            $pdf->SetX(100+$offsetX);
            $pdf->Cell(130,3,"ATC900122NU0", $border, 0, 'L');

            $pdf->SetY(25+$offsetY);
            $pdf->SetX(100+$offsetX);
            $pdf->Cell(130,3,"Régimen: 601.General de Ley Personas Morales", $border, 0, 'L');      
         }




        


        //FOLIO DEL TALON

        $pdf->SetY(20+$offsetY);
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(40,5,"FOLIO :", $border,0, 'L');

        $pdf->SetY(20+$offsetY);
        $pdf->SetX(170+$offsetX);
        $pdf->Cell(40,5, $data['centroDistribucion']." ".$data['folio'], $border,0, 'L');

        ///////Fecha
        date_default_timezone_set('America/Mexico_City');
        //$fecha1=substr($data['fechaEvento'],0,10);
        $fecha1 = date('Y-m-d  H:i:s');
        $fecha = explode('-',$fecha1);

        setlocale(LC_TIME, 'spanish');
        $nombre=strftime("%B",mktime(0, 0, 0, $fecha[1], 1, 2000));
        //echo $nombre;

        $pdf->SetY(8+$offsetY);        
        //Dia
        $pdf->SetX(180+$offsetX);
        $pdf->Cell(6,5, $fecha1, $border,0, 'C');
        //Mes
       /* $pdf->SetX(160+$offsetX);
        $pdf->Cell(20,5, strtoupper($nombre), $border,0, 'C');
        //Año
        $pdf->SetX(180+$offsetX);
        $pdf->Cell(10,5, $fecha[0], $border,0, 'C');
        ////////////
*/
        /////////Origen
        //Cod y Nombre Origen
        $pdf->SetY(58+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(87,3,"ORIGEN : ", $border,0, 'L');

        $pdf->SetY(58+$offsetY);
        $pdf->SetX(45+$offsetX);
        $pdf->Cell(87,3, $_SESSION['usuCompania'], $border,0, 'L');
        //Remitente
        $pdf->SetY(65+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(83,3, $data['nombreCiaRemitente'], $border,0, 'L');
        //RFC
        $pdf->SetY(68+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(30,3, $data['rfcRemitente'], $border,0, 'L');
        //DireccionL1
        $pdf->SetY(71+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL1Cia'], $border,0, 'L');
        //DireccionL2
        $pdf->SetY(74+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL2Cia'], $border,0, 'L');
        //DireccionL3
        $pdf->SetY(77+$offsetY);
        $pdf->SetX(25+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL3Cia'], $border,0, 'L');

        ////////Destino
        //Cod y Nombre Destino

          $detalle = json_decode($_REQUEST['vines'],true);          
        $destino = $detalle[0]['distribuidor'];


        $pdf->SetY(58+$offsetY);
        $pdf->SetX(100+$offsetX);
        $pdf->Cell(87,3,"DESTINO : ", $border,0, 'L');
       // echo json_encode($especiales);
        $pdf->SetY(58+$offsetY);
        $pdf->SetX(119+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $destino, $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $destino, $border,0, 'L');
        }

        //echo $especiales[0]['plaza'];
        //Destinatario
        $pdf->SetY(62+$offsetY);
        $pdf->SetX(119+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(79,3, substr($especiales[0]['descDist'],0,50), $border,0, 'L');
            //$pdf->Cell(79,3, substr($especiales[1]['descDist'],30,30), $border,0, 'L');
        } else {
            $pdf->Cell(79,3, substr($data['descDistribuidor'],0,5), $border,0, 'L');
            $pdf->SetY(65+$offsetY);
            $pdf->SetX(119+$offsetX);
            $pdf->Cell(79,3, $data['descripcionCentro'], $border,0, 'L');
        }

        //echo $especiales[0]['dirL3'];
        $pdf->SetY(68+$offsetY); 
        $pdf->SetX(131+$offsetX); 
        /*if($data['tipoTalon'] == $codigoTalonEspecial){
            //$pdf->Cell(79,3, substr($especiales[1]['descDist'],0,30), $border,0, 'L');
            $pdf->Cell(79,3, substr($especiales[1]['descDist'],30,30), $border,0, 'L');
        } else {
            $pdf->Cell(79,3, $data['descDistribuidor'], $border,0, 'L');
        }*/
        //RFC
        $pdf->SetY(65+$offsetY);
        $pdf->SetX(119+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            //$pdf->Cell(50,3, $especiales[0]['rfc'], $border,0, 'L');
        } else {
            //$pdf->Cell(50,3, $data['rfcDestinatario'], $border,0, 'L');
        }

        //echo $data['rfcDestinatario'];

        //DireccionL1
        $pdf->SetY(68+$offsetY);
        $pdf->SetX(119+$offsetX);

        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[0]['dirL1'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['dirL1Dest'], $border,0, 'L');
        }
        $pdf->SetY(71+$offsetY);
        $pdf->SetX(119+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[0]['dirL2'], $border,0, 'L');
        } else {
            //$pdf->Cell(82,3, $data['dirL2Dest'], $border,0, 'L');
        }
        //DireccionL3
        $pdf->SetY(74+$offsetY);
        $pdf->SetX(119+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[0]['dirL3'], $border,0, 'L');
        } else {
            //$pdf->Cell(82,3, $data['dirL3Dest'], $border,0, 'L');
        }

        //Se recogera    
        $pdf->SetFont($font,'U',9);  

         $pdf->SetY(70+(3*$nInt)+$offsetY);
          $pdf->SetX(93+$offsetX);
          $pdf->Cell(17,3, "DETALLE   DE   CARGA : ", $border,0, 'C');  

          $pdf->SetFont($font,'B',9);
          

        

          $pdf->SetY(80+(3*$nInt)+$offsetY);
          $pdf->SetX(25+$offsetX);
          $pdf->Cell(17,3, "SIMBOLO", $border,0, 'C'); 

          $pdf->SetY(80+(3*$nInt)+$offsetY);
          $pdf->SetX(55+$offsetX);
          $pdf->Cell(17,3, "VIN", $border,0, 'C');

           $pdf->SetY(80+(3*$nInt)+$offsetY);
          $pdf->SetX(95+$offsetX);
          $pdf->Cell(17,3, "DESCRIPCION", $border,0, 'C'); 

           $pdf->SetY(80+(3*$nInt)+$offsetY);
          $pdf->SetX(135+$offsetX);
          $pdf->Cell(17,3, "PESO", $border,0, 'C');  
      

        ///////Unidades
           $detalle = json_decode($_REQUEST['vines'],true);          
        $vines = $detalle[0]['vin'];

        for ($nInt=0; $nInt <sizeof($detalle);  $nInt++) { 
            $series= $detalle[$nInt]['vin'];
             ///echo $series;
        
        //for ($nInt=0; $nInt < sizeof($unidades); $nInt++) {

            $sqlUnidades="SELECT * FROM alUnidadesTbl a, caSimbolosUnidadesTbl c where a.simboloUnidad=c.simboloUnidad AND a.vin='".$detalle[$nInt]['vin']."'";
            $rsUnidad=fn_ejecuta_query($sqlUnidades);
            //Simbolo
            $pdf->SetY(90+(3*$nInt)+$offsetY);
            $pdf->SetX(29+$offsetX);
            $pdf->Cell(17,3, $rsUnidad['root'][0]['simboloUnidad'], $border,0, 'C');
            //vin
            $pdf->SetY(90+(3*$nInt)+$offsetY);
            $pdf->SetX(53+$offsetX);
            $pdf->Cell(22,3, $detalle[$nInt]['vin'], $border,0, 'C');
            //Descripcion Simbolo
            $pdf->SetY(90+(3*$nInt)+$offsetY);
            $pdf->SetX(86+$offsetX);
            $pdf->Cell(72,3, $rsUnidad['root'][0]['descripcionUnidad'], $border,0, 'L');
            //Peso Aproximado

            if ($unidades[$nInt]['pesoAprox']== '0') {
              $peso = '0999';
              $peso1[] = '0999';
            }else{
              $peso = $unidades[$nInt]['pesoAprox'];
              $peso1[] = $unidades[$nInt]['pesoAprox'];
            }

            $pesoTotal= array_sum($peso1);
            $peso= '0.00';


            $pdf->SetY(90+(3*$nInt)+$offsetY);
            $pdf->SetX(137+$offsetX);
            $pdf->Cell(72,3, $peso, $border,0, 'L');

;
        }

        //PESO APROXIMADO
        $pdf->SetY(200+$offsetY);
        $pdf->SetX(30+$offsetX);
        $pdf->Cell(72,3,"PESO APROXIMADO: ...  0.00 ".$pesoTotal,$border,0, 'L');

   

        /////////Chofer
        //Clave
        $pdf->SetY(220+$offsetY);
        $pdf->SetX(28+$offsetX);
        $pdf->Cell(10,3,"OPERADOR :--- SIN INFORMACION ----", $border, 0, 'L');
        //Nombre
        $pdf->SetY(220+$offsetY);
        $pdf->SetX(46+$offsetX);
        $pdf->Cell(95,3," - ".$data['nombreChofer'], $border, 0, 'C');
        //Tractor
        $pdf->SetY(220+$offsetY);
        $pdf->SetX(135+$offsetX);
        $pdf->Cell(20,3,"TRACTOR : --- SIN INFORMACION ----".$data['tractor'], $border, 0, 'C');

         $sqlTractor="SELECT * FROM caTractoresTbl where tractor=".$data['tractor']." AND compania='".substr($data['companiaTractor'], 0,2)."'";
    $rsTractor=fn_ejecuta_query($sqlTractor);

    //fwrite($flReporte660,"AutoTrasporteFederal=TPAF01|00001178|".$rsTractor['root'][0]['aseguradora']."|".$rsTractor['root'][0]['polizaSeguro'].PHP_EOL);
    //  fwrite($flReporte660,"IdentificacionVehicular=C3R3|".$rsTractor['root'][0]['placas']."|".$rsTractor['root'][0]['modelo'].PHP_EOL);


        //Placas
        $pdf->SetY(230+$offsetY);
        $pdf->SetX(135+$offsetX);
        $pdf->Cell(20,3,"PLACAS : --- SIN INFORMACION ----".$rsTractor['root'][0]['placas'], $border, 0, 'C');

        //Importe Total en Letra
        $pdf->SetY(170+$offsetY);
        $pdf->SetX(40+$offsetX);
        //$pdf->Cell(100,3,"CERO PESOS  00  M.N.", $border, 0, 'L');


        //Fecha Estimada Entrega

        $fecha = $data['fechaEvento'];
        $fechaEntrega = strtotime ( "+".$data['diasEntrega']." day" , strtotime ($fecha ));
        $fechaEntrega = date ( 'd/m/Y' , $fechaEntrega );


        $pdf->SetY(208+$offsetY);
        $pdf->SetX(30+$offsetX);
        $pdf->Cell(100,3,"FECHA ESTIMADA DE ENTREGA:  ".$fecha1, $border, 0, 'L');

        ///////Observaciones
        //Hora
        $pdf->SetY(194+$offsetY);
        $pdf->SetX(90+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            //FALTA AGREGAR LO DE PRIMER SERVICIO O LO DE CONTINUIDAD
            //$pdf->Cell(130,3,$data['centroDistribucion']." - ESP:\t"."*HORA:\t".date("H:i:s"), $border, 0, 'L');
            $pdf->SetY(205+$offsetY);
            $pdf->SetX(70+$offsetX);
            if($data['tipoTalon'] == $codigoTalonEspecial){
              //  $pdf->Cell(130,3,'CAMBIO DESTINO', $border, 0, 'L');
            }
        } else {
            //$pdf->Cell(145,3,"*HORA:\t".date("H:i:s"), $border, 0, 'L');
        }
        //Compania
        $pdf->SetY(197+$offsetY);
        $pdf->SetX(80+$offsetX);
        //$pdf->Cell(130,3,$data['companiaTractor'], $border, 0, 'L');

        //Peso Total
        $pdf->SetY(197+$offsetY);
        $pdf->SetX(130+$offsetX);
        //$pdf->Cell(130,3,"PESO NETO: ".$pesoTotal, $border, 0, 'L');

        //CONTACTO
        $pdf->SetY(200+$offsetY);
        $pdf->SetX(65+$offsetX);
        //$pdf->Cell(130,3,"CONTACTO: ".$data['contactoDist'], $border, 0, 'L');

        //telefono
        $pdf->SetY(200+$offsetY);
        $pdf->SetX(145+$offsetX);
        //$pdf->Cell(130,3,"Telefono: ".$data['telefonoDist'], $border, 0, 'L');
    }
?>
