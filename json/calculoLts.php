<?php

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("trGastosViajeTractor.php");


    switch($_REQUEST['calculoGasolinaHdn']){
        case 'GUARDALTSVALES':
            GUARDALTSVALES();
            break; 
        case 'CALCULOMES':
            CALCULOMES();
            break; 
        case 'GUARDACALCULO':
            GUARDACALCULO();
            break;
        case 'SUMGRATIFICACIONES':
            SUMGRATIFICACIONES();
            break;
        case 'COMBOREPARACIONES':
            COMBOREPARACIONES();
            break;        
        default:
            echo '';

    }
    

    function GUARDALTSVALES(){ 
        
        $sqlTractor="SELECT  * from trviajestractorestbl ".
                    "where idTractor='".$_REQUEST['tractor']."'".
                    "and fechaevento=(select max(fechaEvento) from trviajestractorestbl where idTractor='".$_REQUEST['tractor']."');";
        $rsTractor=fn_ejecuta_query($sqlTractor);

        $sqlGuardaLts="INSERT INTO trgastosviajetractortbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, claveMovimiento, litros, usuario, ip, subTotal,numeroTarjeta, observaciones) ".
        "VALUES ('".$rsTractor['root'][0]['idViajeTractor']."', '1000', '".$_REQUEST['ciaSesVal']."', '1', NOW(), '0', '".$_REQUEST['mes']."', '0', 'VG', '".$_REQUEST['litros']."', '132', '10.0.0.1', NULL,'".$_REQUEST['folio']."','".$_REQUEST['fecha']."')";
        $rsOperador=fn_ejecuta_query($sqlGuardaLts);

        echo json_encode($rsOperador);
        
    }

    function CALCULOMES(){        
        $sqlSumEst="SELECT a.rendimiento, c.mesAfectacion, (SELECT sum(b.importe) 
                                            from trviajestractorestbl a, trgastosviajetractortbl b
                                            where a.idTractor='".$_REQUEST['tractor']."'
                                            and month(a.fechaevento)='".$_REQUEST['mes']."'
                                            and year(a.fechaEvento)='2021'
                                            and a.idviajeTractor=b.idviajetractor
                                            and b.concepto=2312 )as ticketCar,
                                        (SELECT sum(b.litros) 
                                            from trviajestractorestbl a, trgastosviajetractortbl b
                                            where a.idTractor='".$_REQUEST['tractor']."'
                                            and month(a.fechaevento)='".$_REQUEST['mes']."'
                                            and year(a.fechaEvento)='2021'
                                            and a.idviajeTractor=b.idviajetractor
                                            and b.concepto in (9000) )as litrosEfectivo,
                                         (SELECT sum(b.litros) 
                                            from trviajestractorestbl a, trgastosviajetractortbl b
                                            where a.idTractor='".$_REQUEST['tractor']."'
                                            and b.mesAfectacion='".$_REQUEST['mes']."'                                            
                                            and a.idviajeTractor=b.idviajetractor
                                            AND b.claveMovimiento='VG'
                                            and b.concepto in (1000) )as litros,   
                                        (SELECT  sum(a.kilometrosComprobados) 
                                            from trviajestractorestbl a
                                            where a.idTractor='".$_REQUEST['tractor']."'
                                            and month(a.fechaevento)='".$_REQUEST['mes']."'
                                            and year(a.fechaEvento)='2021') as KmsPagados           
                    from  catractorestbl a, trviajestractorestbl b, trgastosviajetractortbl c
                    where a.idtractor='".$_REQUEST['tractor']."'
                    and a.idtractor=b.idtractor
                    and b.idviajeTractor=c.idviajeTractor
                    and c.clavemovimiento='GP'
                    and c.mesAfectacion='".$_REQUEST['mes']."' limit 1";
        $rsSum=fn_ejecuta_query($sqlSumEst);

        echo json_encode($rsSum);
    }

    function GUARDACALCULO(){ 
        
        $sqlTractor="SELECT  * from trviajestractorestbl ".
                    "where idTractor='".$_REQUEST['tractor']."'".
                    "and fechaevento=(select max(fechaEvento) from trviajestractorestbl where idTractor='".$_REQUEST['tractor']."');";
        $rsTractor=fn_ejecuta_query($sqlTractor);

        $sqlGuardaLts="INSERT INTO trgastosviajetractortbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, claveMovimiento, litros, usuario, ip, subTotal) ".
        "VALUES ('".$rsTractor['root'][0]['idViajeTractor']."', '1000', '".$_REQUEST['ciaSesVal']."', '1', NOW(), '0', '".$_REQUEST['mes']."', '0', 'RD', '".$_REQUEST['litros']."', '132', '10.0.0.1', NULL)";
        $rsOperador=fn_ejecuta_query($sqlGuardaLts);

        echo json_encode($rsOperador);
        
    }
    
?>
