<?php
    session_start();
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");
    
    $a = array();
    $a['successTitle'] = "Manejador de Archivos";
    
    if(isset($_FILES)) {
        if($_FILES["alCargaEspecialesFld"]["error"] > 0){
            $a['success'] = false;
            $a['message'] = $_FILES["alCargaEspecialesFld"]["error"];

        } else {
            $temp_file_name = $_FILES['alCargaEspecialesFld']['tmp_name']; 
            $original_file_name = $_FILES['alCargaEspecialesFld']['name'];
          
            // Find file extention 
            $ext = explode ('.', $original_file_name); 
            $ext = $ext [count ($ext) - 1]; 
            
            // Remove the extention from the original file name 
            $file_name = str_replace ($ext, '', $original_file_name); 
            
            $new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;

            if (move_uploaded_file($temp_file_name, $new_name)){

                if (!file_exists($new_name)){
                    $a['success'] = false;
                    $a['errorMessage'] = "Error al procesar el archivo " . $new_name;
                } else {
                    $a['success'] = true;
                    $a['successMessage'] = "Archivo Cargado";
                    $a['archivo'] = $file_name . $ext;
                    $a['root'] = leerXLS($new_name);
                }
            } else { 
                $a['success'] = false;
                $a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
            }
        }
    } else {
        $a['success'] = false;
        $a['errorMessage'] = "Error FILES NOT SET";
    }
    
    echo json_encode($a); 

    function leerXLS($inputFileName) {
        /** Error reporting */
        error_reporting(E_ALL);
        
        date_default_timezone_set ("America/Mexico_City");
        
        //  Include PHPExcel_IOFactory
        include '../funciones/Classes/PHPExcel/IOFactory.php';
        
        //$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
        //echo $inputFileName;
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader =
             PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        
        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        //Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
        $rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

        
        //-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

        if (ord(strtoupper($highestColumn)) <= 5 && $highestRow <= 5 ) {
            $root[] = array('descripcion' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
            return $root;
        }

        if (ord(strtoupper($highestColumn)) < 2) {
            $root[] = array('descripcion' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
            return $root;
        }

        if (strlen($rowData[0][5]) < 17) {
            $root[] = array('descripcion' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el orden correcto', 'fail'=>'Y');
            return $root;
        }

        //$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true));

        $isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
                                'Simbolo'=>array('val'=>'','size'=>true,'exist'=>true),
                                'Distribuidor' =>array('val'=>'','size'=>true,'exist'=>true),
                                'Color' =>array('val'=>'','size'=>true,'exist'=>true),
                                'Avanzada' =>array('val'=>'','size'=>true,'exist'=>true)
                            );

        

    
        
        for($row = 0; $row<sizeof($rowData); $row++){
           
            $isValidArr['VIN']['val'] = $rowData[$row][5];            
            $isValidArr['VIN']['size'] = strlen($rowData[$row][5]) == 17;
            /*$isValidArr['VINN']['val'] = $rowData[$row][5];
            $isValidArr['VINN']['size'] = strlen($rowData[$row][5]) == 17;*/
            $isValidArr['Simbolo']['val'] = $rowData[$row][6];
            $isValidArr['Simbolo']['size'] = strlen($rowData[$row][6]) > 0;
            $isValidArr['Distribuidor']['val'] = $rowData[$row][3];
            $isValidArr['Distribuidor']['size'] = strlen($rowData[$row][3]) <= 9;
            $isValidArr['Color']['val'] = $rowData[$row][7];
            $isValidArr['Color']['size'] = strlen($rowData[$row][7]) > 0;
            ///$isValidArr['Avanzada']['size'] = substr($rowData[$row][5],9,17);

            /*if (strlen($rowData[$row][4]) =='COBRO CON ST') {                
                $isValidArr['ST']['size'] = strlen($rowData[$row][5]) > 0;
                $isValidArr['ST']['val'] = $rowData[$row][5];
            }*/

                $errorMsg = '';
            $isTrue = true;



            

            foreach ($isValidArr as $key => $value) {
                if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
                    $errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
                    $isTrue = false;
                } else {// de lo contrario verifico que exista en la base
                    if ($key != 'Avanzada') {                        
                        $isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
                        if (!$isValidArr[$key]['exist']) {
                                 if ($key == 'VIN') {
                                    $siNo = ' YA TIENE RC';
                                }
                                /*else  if ($key == 'VINN') {
                                    $siNo = ' NO CUENTA CON COTIZACION';
                                }*/
                                else if ($key == 'Distribuidor') {
                                    $siNo = ' NO EXISTE';
                                }
                                else if ($key == 'Simbolo') {
                                    $siNo = ' NO EXISTE';
                                }
                                else if ($key == 'Color') {
                                    $siNo = ' NO EXISTE';
                                }
                      
                            $errorMsg .= 'El '.$key.$siNo;
                            $isTrue = false; 
                        }
                    }
                }
            }

            if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
                //Busco el idTarifa
            
                $today = date("Y-m-d H:i:s");
                $fecha = substr($today,0,10);
                $hora=substr($today,11,8);

                    
                $ciaSesVal=$_SESSION['usuCompania'];
                
                $centroDistribucion=$rowData[$row][0];
                $origen=$rowData[$row][1];
                $destino=$rowData[$row][2];
                $distribuidor=$rowData[$row][3];
                $servicio=$rowData[$row][4];
                //$ST=$rowData[$row][5];
                $vin=$rowData[$row][5];
                $simbolo=$rowData[$row][6];
                $color=$rowData[$row][7];

                $vinArr=$vin;
                $simboloArr=$simbolo;
                $colorArr=$color;
                

            


                for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {

                    $sqlNueva="SELECT * FROM alUnidadesTbl where vin='".$vin."'";
                    $rsNueva=fn_ejecuta_query($sqlNueva);

                    if ($rsNueva['root'][0]['vin']!='') {
                        $nueva='0';
                    }else{
                        $nueva='1';
                    }

                    //echo $nueva;

                    $sqlGetTarifaStr = "SELECT tf.idTarifa ".
                                       "FROM caTarifasTbl tf, caClasificacionTarifasTbl ct, caSimbolosUnidadesTbl su ".
                                       "WHERE su.simboloUnidad = '".$simbolo."' ".
                                       "AND ct.clasificacion = su.clasificacion ".
                                       "AND tf.idTarifa = ct.idTarifa ".
                                       "AND tf.tipoTarifa = 'E'";

                    $rs = fn_ejecuta_query($sqlGetTarifaStr);

                    if ($rs['records'] > 0) {
                        $tarifa = $rs['root'][0]['idTarifa'];
                    } else {
                        $a['success'] = false;
                        array_push($sinTarifaArr, $vin);
                    }
                        if($nueva == "1"){

                            //Si es nueva la unidad, ya con la tarifa se inserta en alUnidadesTbl
                            $data = addUnidad($vin,$distribuidor,$simbolo,
                                                $color,$centroDistribucion,'RC',$tarifa,
                                                'TEMP','','','');
                        }else{

                            $sqlGetDistribuidor = "SELECT distribuidor FROM alultimodetalletbl ".
                                                  "WHERE vin= '".$vin."' ";
                            $sqlRS = fn_ejecuta_query($sqlGetDistribuidor);

                            $data = addHistoricoUnidad($centroDistribucion,$vin,'RC',
                                                        $sqlRS['root'][0]['distribuidor'],$tarifa,'TEMP',
                                                      '','',$_SESSION['idUsuario']);

                            
                        }if($data['success']) {
                            //Se obtienen las direcciones y plazas del distribuidor Origen y Destino
                            $sqlGetDireccionesPlazasDistStr = "SELECT distribuidorCentro, idPlaza, direccionEntrega ".
                                                              "FROM caDistribuidoresCentrosTbl ".
                                                              "WHERE distribuidorCentro ='".$origen."' ".
                                                              "OR distribuidorCentro ='".$destino."'";

                            $rs = fn_ejecuta_query($sqlGetDireccionesPlazasDistStr);

                            for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
                                if ($rs['root'][$iInt]['distribuidorCentro'] == $origen) {
                                    $dirOrigen = $rs['root'][$iInt]['direccionEntrega'];
                                    $plazaOrigen = $rs['root'][$iInt]['idPlaza'];
                                }
                                if ($rs['root'][$iInt]['distribuidorCentro'] == $destino) {
                                    $dirDestino = $rs['root'][$iInt]['direccionEntrega'];
                                    $plazaDestino = $rs['root'][$iInt]['idPlaza'];
                                }
                            }
                            $sqlAddDestinoEspecialStr = "INSERT INTO alDestinosEspecialesTbl ".
                                                    "(idDestinoEspecial, vin, distribuidorOrigen, direccionOrigen, idPlazaOrigen, distribuidorDestino,".
                                                    "direccionDestino, idPlazaDestino, importeCob, sueldo, idTarifa,tipoServEsp,numeroST) VALUES(".
                                                    "(SELECT IFNULL(MAX(de2.idDestinoEspecial),0) + 1 FROM alDestinosEspecialesTbl de2 ".
                                                        "WHERE de2.vin = '".$vin."'),".
                                                    "'".$vin."','".$origen."',".
                                                    $dirOrigen.",".$plazaOrigen.",'".
                                                    $destino."',".$dirDestino.",".
                                                    $plazaDestino.",0.00,0.00,".$tarifa.",'".$tipoServicio."',".replaceEmptyNull("'".$_REQUEST['trap010STTxt']."'").")";

                            fn_ejecuta_Add($sqlAddDestinoEspecialStr);

                            $updLocUnidad = "UPDATE alhistoricounidadestbl".
                                            "SET localizacionUnidad = (SELECT idDestinoEspecial ".
                                            "FROM aldestinosespecialestbl ".
                                            "WHERE VIN = '".$vin."') ".
                                            "WHERE claveMovimiento = 'RC' ".
                                            "AND VIN = '".$vin."' ".
                                            "AND localizacionUnidad = 'TEMP'";
                            fn_ejecuta_query($updLocUnidad);

                            $updUdLoc = "UPDATE alUltimoDetalleTbl".
                                            "SET localizacionUnidad = (SELECT idDestinoEspecial ".
                                            "FROM aldestinosespecialestbl ".
                                            "WHERE VIN = '".$vin."') ".
                                            "WHERE claveMovimiento = 'RC' ".
                                            "AND VIN = '".$vin."' ".
                                            "AND localizacionUnidad = 'TEMP'";

                            fn_ejecuta_query($updUdLoc);

                            $sqlUpdLibera = "UPDATE alUnidadesDetenidasTbl ".
                                            "SET claveMovimiento = 'UL', ".
                                            "centroLibera = '".$_REQUEST['trap010CentroDistHdn']."', ".
                                            "fechaFinal = NOW(), ".
                                            "numeroMovimiento = 2 ".
                                            "WHERE vin = '".$vin."'";

                            fn_ejecuta_Add($sqlUpdLibera);

                                $sqlGetUltimoID =   "SELECT ".
                                                    "(SELECT MAX(idDestinoEspecial) FROM alDestinosEspecialesTbl ".
                                                        "WHERE vin = '".$vin."') AS idDestinoEspecial,".
                                                    "(SELECT MAX(idHistorico) FROM alHistoricoUnidadesTbl ".
                                                        "WHERE vin = '".$vin."') AS idHistorico";

                                $rsId = fn_ejecuta_query($sqlGetUltimoID);

                                if(sizeof($rsId['root']) > 0){
                                    $updHistorico = "UPDATE alHistoricoUnidadesTbl ".
                                                    "SET localizacionUnidad = '".$rsId['root'][0]['idDestinoEspecial']."' ".
                                                    "WHERE idHistorico = ".$rsId['root'][0]['idHistorico'];

                                    fn_ejecuta_Upd($updHistorico);

                                    $updUD = "UPDATE alUltimoDetalleTbl ".
                                                    "SET localizacionUnidad = '".$rsId['root'][0]['idDestinoEspecial']."' ".
                                                    "WHERE vin = '".$vin."';";

                                    fn_ejecuta_Upd($updUD);
                                } else {
                                    $a['success'] = false;
                                    array_push($errorArr, $vin);
                                }


                            if($a['success']){
                                if($_REQUEST['trap010ConceptosHdn'] != "" && $_REQUEST['trap010ImportesHdn'] != ""){
                                    $data = addGastosEspeciales($origen, $vin, $conceptosArr, $importesArr);
                                }

                                if ($detenidasArr[$nInt] == 1) {
                                    $sqlGetNumMovimiento = "SELECT MAX(numeroMovimiento) AS numeroMovimiento FROM alUnidadesDetenidasTbl ".
                                                           "WHERE vin = '".$vin."'";

                                    $rsLib = fn_ejecuta_query($sqlGetNumMovimiento);

                                    $sqlLiberarUnidadStr = "UPDATE alUnidadesDetenidasTbl SET ".
                                                           "claveMovimiento = 'UL', ".
                                                           "centroLibera = '".$_REQUEST['trap010CentroDistHdn']."', ".
                                                           "fechaFinal = '".date("Y-m-d H:i:s")."' ".
                                                           "WHERE vin = '".$vin."' ".
                                                           "AND numeroMovimiento = ".$rsLib['root'][0]['numeroMovimiento'];

                                    $rsLib = fn_ejecuta_Upd($sqlLiberarUnidadStr);
                                }
                            }


                        } else {
                            $a['success'] = false;
                            array_push($errorArr, $vin);
                        }
                    
                }

                //Concatenate Errors
                if (sizeof($errorArr) > 0) {
                    if ($success == true) {
                        $a['errorMessage'] =  getDestinosEspecialesSuccessMsg()."<br>".getDestinosEspecialesFailedMsg();
                        foreach ($errorArr as $error) {
                            $a['errorMessage'] .= "<br>".$error;
                        }
                    } else if ($success == false){
                        $a['errorMessage'] = getDestinosEspecialesFailedMsg();
                        foreach ($errorArr as $error) {
                            $a['errorMessage'] .= "<br>".$error;
                        }
                    }
                } else {
                    if(sizeof($sinTarifaArr) > 0){
                        if($success == true){
                            $a['errorMessage'] = getDestinosEspecialesSuccessMsg()."<br>".getDestinosEspecialesSinTarifaEspecialMsg();
                            foreach ($sinTarifaArr as $unidad) {
                                $a['errorMessage'] .= "<br>".$unidad;
                            }
                        } else {
                            $a['errorMessage'] = getDestinosEspecialesSinTarifaEspecialMsg();
                            foreach ($sinTarifaArr as $unidad) {
                                $a['errorMessage'] .= "<br>".$unidad;
                            }
                        }
                    } else {
                        $a['successMessage'] = getDestinosEspecialesSuccessMsg();
                    }
                }
            

            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
            //echo json_encode($a);
        


                if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $errorMsg = 'Agregado Correctamente';
                } else {
                    $errorMsg = 'Registro No Agregado';
                }       
            }
            $root[]= array('centroDistribucion'=>$rowData[$row][0],'origen'=>$rowData[$row][1],'destino'=>$rowData[$row][2],'distribuidorUnidad'=>$rowData[$row][3],'tipoServicio'=>$rowData[$row][4],'vin'=>$rowData[$row][5],'simbolo'=>$rowData[$row][6],'color'=>$rowData[$row][7],'descripcion'=>$errorMsg);          

            //echo json_encode($rowData[$row]);
        }
        return $root;   

    }



    function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
        $tabla = 'tabla';
        $columna = 'columna';
        switch($field) {
            case 'VIN':
                $tabla = 'alUltimoDetalleTbl'; $columna = 'vin'; 
                $sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."' AND claveMovimiento ='RC'";
                $rs = fn_ejecuta_query($sqlExist);
                return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);                
                break;                
            case 'Simbolo':
                $tabla = 'caSimbolosUnidadesTbl'; $columna = 'simboloUnidad'; 
                $sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
                $rs = fn_ejecuta_query($sqlExist);
                return strtoupper($field) == 'Simbolo' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
                break;
            case 'Distribuidor':
                $tabla = 'caDistribuidoresCentrosTbl'; $columna = 'distribuidorCentro'; 
                $sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
                $rs = fn_ejecuta_query($sqlExist);
                return strtoupper($field) == 'Distribuidorr' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
                break;
            case 'Color':
                $tabla = 'caColorUnidadestbl'; $columna = 'color'; 
                $sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
                $rs = fn_ejecuta_query($sqlExist);
                return strtoupper($field) == 'Color' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
                break;
            

        }
    }




?>