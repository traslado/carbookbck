<?php
	session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
	
    switch($_REQUEST['catIVAActionHdn']) {
        case 'getIVA':
            echo json_encode(getIVA());
            break;
        default:
            echo '';
    }

    function getIVA(){
        $sqlGetIVAStr = "SELECT iv.* ".
                        "FROM caIVATbl iv ".
                        "WHERE iv.fechaInicio = (SELECT MAX(iv2.fechaInicio) FROM caIVATbl iv2) ".
                        "AND iv.fechaFinal IS NULL ";

        $rs = fn_ejecuta_query($sqlGetIVAStr);

        return $rs;
    }
?>