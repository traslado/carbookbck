<?php
    setlocale(LC_TIME, 'es_MX.utf8');
    session_start();

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("trViajesTractores.php");

    $success = true;


        $sqlFolioTMP = "SELECT distinct folio as numFolio FROM trGastosViajeTractorTbl ".
                        "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."'".
                        "AND claveMovimiento = 'GP' ".
                        "LIMIT 1;";

        $rsTMP = fn_ejecuta_query($sqlFolioTMP);

        $folioGP = $rsTMP['root'][0]['numFolio'];

        $sqlGetFolioGS = "SELECT distinct folio ".
                            "FROM trgastosviajetractortbl ".
                            "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                            "AND claveMovimiento = 'GS'";

        $rsFolioTMPS = fn_ejecuta_query($sqlGetFolioGS);

        $folioGS = $rsTMP['root'][0]['numFolio'];

    if($_REQUEST['checarDatos'] == "1"){
        $data = getHistoricoViajes();
        if(sizeof($data['root']) == 0){
            $success = false;
        }

        //DESTINOS - TALONES
        $sqlGetDestinos = "SELECT tv.distribuidor, pl.plaza ".
                            "FROM trtalonesviajestbl tv, caPlazasTbl pl ".
                            "WHERE tv.idPlazaDestino = pl.idPlaza ".
                            "AND tv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                            "AND tv.claveMovimiento != 'TX'";

        $destinos = fn_ejecuta_query($sqlGetDestinos);

        //print_r($destinos['root']);

        //GASTOS DE POLIZA
        $sqlGetGastosPoliza = "SELECT cc.concepto, cc.cuentaContable, gv.iva, gv.subtotal, gv.importe, co.nombre, ".
                                "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                    "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                                "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioGP."' ".
                                    "AND gv.claveMovimiento = 'GP' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".
                                    "WHERE gv2.folio = '".$folioGP."' LIMIT 1) ".
                                "AND ge.tabla = 'comprobacionPoliza' ".
                                "GROUP BY ge.valor ";

        $gastos = fn_ejecuta_query($sqlGetGastosPoliza);
        //print_r($gastos['root']);


        //GASTOS DE SUELDOS
        $sqlGetSueldos = "SELECT cc.concepto, cc.cuentaContable, gv.iva, gv.subtotal, gv.importe, gv.observaciones, co.nombre, ge.columna, ".
                            "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE cc.concepto = gv.concepto ".
                                "AND cc.centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."') AS importeConcepto, ".
                            "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                            "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioGS."' ".
                                    "AND gv.claveMovimiento = 'GS' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".
                                    "WHERE gv2.folio = '".$folioGS."' LIMIT 1) ".
                                "AND ge.tabla = 'trPolizaGastosTbl' ".
                                "AND (ge.columna = 'deducciones' OR ge.columna = 'percepciones') ".
                                "ORDER BY ge.estatus ";

        $gastosSueldos = fn_ejecuta_query($sqlGetSueldos);
        //print_r($gastosSueldos);

        //ANTICIPOS
        $sqlGetAnticiposStr =   "SELECT cc.cuentaContable, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl, caDistribuidoresCentrosTbl dc ".
                                        "WHERE dc.distribuidorCentro = cc.centroDistribucion ".
                                        "AND pl.idPlaza = dc.idPlaza) AS plaza, ".
                                    "(SELECT SUM(gv.importe) FROM trGastosViajeTractorTbl gv ".
                                        "WHERE gv.concepto = cc.concepto AND gv.centroDistribucion = cc.centroDistribucion ".
                                        "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." AND gv.claveMovimiento != 'GX') AS importe, ".
                                    "(SELECT GROUP_CONCAT(gv.folio SEPARATOR '-') FROM trGastosViajeTractorTbl gv ".
                                        "WHERE gv.concepto = cc.concepto AND gv.centroDistribucion = cc.centroDistribucion ".
                                        "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." GROUP BY gv.centroDistribucion) AS folio ".
                                "FROM caConceptosCentrosTbl cc ".
                                "WHERE cc.concepto = '2222'";

        $anticipos = fn_ejecuta_query($sqlGetAnticiposStr);
        //print_r($anticipos)
        //print_r($dieselExtra);

        // MONTO DE ANTICIPOS COMPROBADOS

        $sqlAntComprobacion = "SELECT sum(g1.importe) as importe, ".
                                "(SELECT sum(g2.importe) FROM trgastosviajetractortbl g2 WHERE g2.idViajeTractor = g1.idViajeTractor AND  g2.claveMovimiento = 'GP' AND g2.concepto IN (2315,2333,2342,6002,6009,6010,6011,6012,7002,7003,7004,7005,7006,7017,7018,7019,7020)) as sumaComprobacion ".
                                "FROM trgastosviajetractortbl g1 ".
                                "WHERE g1.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                                "AND g1.concepto = '2222';";

        $rsComprobados = fn_ejecuta_query($sqlAntComprobacion);


        //SE CALCULA LA TARIFA DEL SUELDO
        if($data['root'][0]['centroDistribucionOrigen'] == 'CDSAL' && intval($data['root'][0]['claveChofer']) >= 8000 && intval($data['root'][0]['claveChofer']) < 9000){
            $conceptoSueldo = '144';
        } else {
            $conceptoSueldo = '143';
        }

        $conceptoSCargo = '145';

        //DATOS EXTRA

        $sqlGetExtra = "SELECT gv.fechaEvento, ".
                        "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7012' ) AS cuentaIva, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7013') AS cuentaCargoOperador, ".
                        "(SELECT gv2.importe FROM trGastosViajeTractorTbl gv2 WHERE gv2.idViajeTractor = gv.idViajeTractor AND gv2.folio = gv.folio AND gv2.concepto = '7013') AS importeCargoOperador, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7014') AS cuentaPagoEfectivo, ".
                        "(SELECT gv3.importe FROM trGastosViajeTractorTbl gv3 WHERE gv3.idViajeTractor = gv.idViajeTractor AND gv3.folio = gv.folio AND gv3.concepto = '7014') AS importePagoEfectivo, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '2231') AS cuentaNoComprobados, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '141') AS cuentaPagoNeto, ".
                        "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucion']."' AND concepto = '".$conceptoSueldo."') AS tarifaSueldo, ".
                        "(SELECT concat(SUBSTRING(cc.cuentaContable, 1,13),'.', ".
                                       "LPAD((SELECT ta.tractor FROM catractorestbl ta ".
                                             "WHERE idTractor = (SELECT vt.idTractor FROM trviajestractorestbl vt ".
                                                                "WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")),4,0),'.', ".
                                                                "(SELECT vt.claveChofer FROM trviajestractorestbl vt ".
                                                                " WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")) as cuentaContable ".
                        " FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucion']."' AND concepto = '".$conceptoSueldo."') AS cuentaSueldo, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucion']."' AND concepto = '145') AS cuentaSCargo ".
                        "FROM trGastosViajeTractorTbl gv ".
                        "WHERE gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                        "AND gv.folio = '".$rsTMP['root'][0]['numFolio']."' ".
                        "AND gv.claveMovimiento = 'GP' LIMIT 1;";

        $extras = fn_ejecuta_query($sqlGetExtra);

        /*$sqlGetExtra = "SELECT gv.fechaEvento, ".
                        "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7012' ) AS cuentaIva, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7013') AS cuentaCargoOperador, ".
                        "(SELECT gv2.importe FROM trGastosViajeTractorTbl gv2 WHERE gv2.idViajeTractor = gv.idViajeTractor AND gv2.folio = gv.folio AND gv2.concepto = '7013') AS importeCargoOperador, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7014') AS cuentaPagoEfectivo, ".
                        "(SELECT gv3.importe FROM trGastosViajeTractorTbl gv3 WHERE gv3.idViajeTractor = gv.idViajeTractor AND gv3.folio = gv.folio AND gv3.concepto = '7014') AS importePagoEfectivo, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '2231') AS cuentaNoComprobados, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '141') AS cuentaPagoNeto, ".
                        "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' AND concepto = '".$conceptoSueldo."') AS tarifaSueldo, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' AND concepto = '".$conceptoSueldo."') AS cuentaSueldo, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' AND concepto = '".$conceptoSCargo."') AS cuentaSCargo ".
                        "FROM trGastosViajeTractorTbl gv ".
                        "WHERE gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                        "AND gv.folio = '".$_REQUEST['trViajesTractoresFolioPolizaHdn']."' ".
                        "AND gv.claveMovimiento = 'GP' ".
                        "LIMIT 1 ";

        $extras = fn_ejecuta_query($sqlGetExtra);*/

        //print_r($extras['root'][0]);
        return;
    }

    if($_REQUEST['trViajesTractoresIdViajeHdn'] != ''){
        $pdf = new FPDF('P', 'mm', array(216, 280));

        //DATOS GENERALES DEL VIAJE
        $data = getHistoricoViajes();


        //DESTINOS - TALONES
        $sqlGetDestinos = "SELECT tv.distribuidor, pl.plaza ".
                            "FROM trtalonesviajestbl tv, caPlazasTbl pl ".
                            "WHERE tv.idPlazaDestino = pl.idPlaza ".
                            "AND tv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                            "AND tv.claveMovimiento != 'TX' ";

        $destinos = fn_ejecuta_query($sqlGetDestinos);

        //GASTOS DE POLIZA
        $sqlGetGastosPoliza = "SELECT cc.concepto, cc.cuentaContable, gv.iva, gv.subtotal, gv.importe, co.nombre, ".
                                "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                    "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                                "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioGP."' ".
                                    "AND gv.claveMovimiento = 'GP' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".
                                    "WHERE gv2.folio = '".$folioGP."' LIMIT 1) ".
                                "AND ge.tabla = 'comprobacionPoliza' ".
                                "GROUP BY ge.valor ";

        $gastos = fn_ejecuta_query($sqlGetGastosPoliza);

        //GASTOS DE SUELDOS
        $sqlGetSueldos = "SELECT cc.concepto, cc.cuentaContable, gv.iva, gv.subtotal, gv.importe, gv.observaciones, co.nombre, ge.columna, ".
                            "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE cc.concepto = gv.concepto ".
                                "AND cc.centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."') AS importeConcepto, ".
                            "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                            "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioGS."' ".
                                    "AND gv.claveMovimiento = 'GS' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".
                                    "WHERE gv2.folio = '".$folioGS."' LIMIT 1) ".
                                "AND ge.tabla = 'trPolizaGastosTbl' ".
                                "AND (ge.columna = 'deducciones' OR ge.columna = 'percepciones') ".
                                "ORDER BY ge.estatus ";

        $gastosSueldos = fn_ejecuta_query($sqlGetSueldos);


        //ANTICIPOS
        $sqlGetAnticiposStr =   "SELECT cc.cuentaContable, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl, caDistribuidoresCentrosTbl dc ".
                                        "WHERE dc.distribuidorCentro = cc.centroDistribucion ".
                                        "AND pl.idPlaza = dc.idPlaza) AS plaza, ".
                                    "(SELECT SUM(gv.importe) FROM trGastosViajeTractorTbl gv ".
                                        "WHERE gv.concepto = cc.concepto AND gv.centroDistribucion = cc.centroDistribucion ".
                                        "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." AND gv.claveMovimiento != 'GX') AS importe, ".
                                    "(SELECT GROUP_CONCAT(gv.folio SEPARATOR '-') FROM trGastosViajeTractorTbl gv ".
                                        "WHERE gv.concepto = cc.concepto AND claveMovimiento != 'GX' AND gv.centroDistribucion = cc.centroDistribucion ".
                                        "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." GROUP BY gv.centroDistribucion) AS folio ".
                                "FROM caConceptosCentrosTbl cc ".
                                "WHERE cc.concepto = '2222'".
                                                                "AND cc.centroDistribucion NOT IN('CDTSA') ";

        $anticipos = fn_ejecuta_query($sqlGetAnticiposStr);

        //DIESEL EXTRA
        $sqlGetDieselExtra = "SELECT gv.concepto, gv.importe, co.nombre ".
                             "FROM trGastosViajeTractorTbl gv, caConceptosTbl co ".
                             "WHERE gv.concepto = co.concepto ".
                             "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                             "AND gv.folio = '".$_REQUEST['trViajesTractoresFolioPolizaHdn']."' ".
                             "AND gv.claveMovimiento = 'GP' ";

        $dieselExtra = fn_ejecuta_query($sqlGetDieselExtra);

        //SE CALCULA LA TARIFA DEL SUELDO
        if($data['root'][0]['centroDistribucionOrigen'] == 'CDSAL' && intval($data['root'][0]['claveChofer']) >= 8000 && intval($data['root'][0]['claveChofer']) < 9000){
            $conceptoSueldo = '144';
        } else {
            $conceptoSueldo = '143';
        }

        $conceptoSCargo = '145';

        //DATOS EXTRA
        $sqlGetExtra = "SELECT gv.fechaEvento, ".
                        "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7012' ) AS cuentaIva, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7013') AS cuentaCargoOperador, ".
                        "(SELECT gv2.importe FROM trGastosViajeTractorTbl gv2 WHERE gv2.idViajeTractor = gv.idViajeTractor AND gv2.folio = gv.folio AND gv2.concepto = '7013') AS importeCargoOperador, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7014') AS cuentaPagoEfectivo, ".
                        "(SELECT gv3.importe FROM trGastosViajeTractorTbl gv3 WHERE gv3.idViajeTractor = gv.idViajeTractor AND gv3.folio = gv.folio AND gv3.concepto = '7014') AS importePagoEfectivo, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '2231') AS cuentaNoComprobados, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '141') AS cuentaPagoNeto, ".
                        "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucion']."' AND concepto = '".$conceptoSueldo."') AS tarifaSueldo, ".
                        "(SELECT concat(SUBSTRING(cc.cuentaContable, 1,13),'.', ".
                                       "LPAD((SELECT ta.tractor FROM catractorestbl ta ".
                                             "WHERE idTractor = (SELECT vt.idTractor FROM trviajestractorestbl vt ".
                                                                "WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")),4,0),'.', ".
                                                                "(SELECT vt.claveChofer FROM trviajestractorestbl vt ".
                                                                " WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")) as cuentaContable ".
                        " FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucion']."' AND concepto = '".$conceptoSueldo."') AS cuentaSueldo, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucion']."' AND concepto = '145') AS cuentaSCargo ".
                        "FROM trGastosViajeTractorTbl gv ".
                        "WHERE gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                        "AND gv.folio = '".$rsTMP['root'][0]['numFolio']."' ".
                        "AND gv.claveMovimiento = 'GP' LIMIT 1;";

        $extras = fn_ejecuta_query($sqlGetExtra);

        $sqlCmpOperador = "SELECT concat(cc.cuentaContable,' ',co.nombre,'     ',gt.importe) as cmpOperador, gt.importe ".
                          "FROM caconceptoscentrostbl cc, caconceptostbl co, trgastosviajetractortbl gt ".
                          "WHERE co.concepto = gt.concepto ".
                          "AND cc.concepto = co.concepto ".
                          "AND cc.centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' ".
                          "AND cc.concepto = '7025' ".
                          "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'];

        $rsCmpOperador = fn_ejecuta_query($sqlCmpOperador);


        //REVISAR VACIOS
        //PROBABLEMENTE

        //$pdf = generarPdfMacheteros($pdf);

        if($rsCmpOperador['root'][0]['importe'] == null){
          $pdf = generarPdfGastos($pdf, $data['root'][0], $destinos['root'], $gastos['root'], $anticipos['root'], $dieselExtra['root'],$extras['root'][0]);
        }else{
            $pdf = generarPdfGastos($pdf, $data['root'][0], $destinos['root'], $gastos['root'], $anticipos['root'], $dieselExtra['root'],$extras['root'][0]);
            $pdf = generaCuadre($pdf, $data['root'][0], $destinos['root'], $gastos['root'], $anticipos['root'], $dieselExtra['root'],$extras['root'][0]);
        }

        /*$pdf = generarPdfGastos($pdf, $data['root'][0], $destinos['root'], $gastos['root'], $anticipos['root'], $dieselExtra['root'],$extras['root'][0]);

        $pdf = generarPdfSueldos($pdf, $data['root'][0], $dieselExtra['root'], $gastosSueldos['root'], $extras['root'][0]);*/

        $pdf->Output('poliza.pdf', 'I');
    } else {
        echo "Error al obtener el ID del viaje";
    }

    function generarPdfGastos($pdf, $data, $destinos, $gastos, $anticipos, $diesel, $extras){
        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;

            $sqlGetCentro = "SELECT centroDistribucion FROM trgastosviajetractortbl ".
                            "WHERE idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                            "AND claveMovimiento = 'GU' ";

            $rsGetCentro = fn_ejecuta_query($sqlGetCentro);

            $sqlGetElaboro = "SELECT us.idUsuario,us.nombre FROM trgastosviajetractortbl gt, segusuariostbl us ".
                                                    "WHERE us.idUsuario = gt.usuario ".
                                                    "AND gt.claveMovimiento = 'GU' ".
                                                    "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ";

            $usuarioElaboracion = fn_ejecuta_query($sqlGetElaboro);

            $sqlCmpOperador = "SELECT concat(cc.cuentaContable,' ',co.nombre,'     ',gt.importe) as cmpOperador, gt.importe ".
                                    "FROM caconceptoscentrostbl cc, caconceptostbl co, trgastosviajetractortbl gt ".
                                    "WHERE co.concepto = gt.concepto ".
                                    "AND cc.concepto = co.concepto ".
                                    //"AND cc.centroDistribucion = '".$rsGetCentro['root'][0]['centroDistribucion']."' ".
                                    "AND cc.concepto = '7025' ".
                                    "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." limit 1";

            $rsCmpOperador = fn_ejecuta_query($sqlCmpOperador);

            /*echo json_encode($rsCmpOperador);
            echo $sqlCmpOperador;*/

            $sqlDieselExt = "SELECT concepto, SUM(importe) as impDieselExt ".
                "FROM trgastosviajetractortbl ".
                "WHERE idViajeTractor =  ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                "AND claveMovimiento = 'GD' ".
                "AND concepto = 2314;";

        $rsSqlDieselExt = fn_ejecuta_query($sqlDieselExt);

                $sqlGetFolio = "SELECT CONCAT(date_format(NOW(), '%m'),LPAD(CONCAT('0',MAX(folio) + 1),4,'0')) as folio, SUBSTR(folio,1,2) as mesFolio, SUBSTR(now(),6,2) as mesAnio, SUBSTR(now(),1,4) as anioAnio, LPAD(CONCAT('0',MAX(folio) + 1),4,'0') as foleadora ".
                        "FROM trfoliostbl ".
                        "WHERE compania = 'TR' ".
                        "AND tipoDocumento = 'PZ' ".
                        "AND centroDistribucion = '".$ciaSesVal."' ";

        //$rsGetFolio = fn_ejecuta_query($sqlGetFolio);

        $sqlFolioTMP = "SELECT distinct folio as numFolio FROM trGastosViajeTractorTbl ".
                        "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."'".
                        "AND claveMovimiento = 'GP';";

        $rsTMP = fn_ejecuta_query($sqlFolioTMP);

        $folioGP = $rsTMP['root'][0]['numFolio'];

        $diExtra =  $rsSqlDieselExt['root'][0]['impDieselExt'];

        $pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->AddPage();
        $pdf->SetFont($font,'',9);
        $pdf->SetAutoPageBreak(false);

        $fechaCompleta = date_create($extras['fechaEvento']);
        $fecha = date_format($fechaCompleta, "d/m/Y");
        $hora = date_format($fechaCompleta, "H:i:s");

        $linea = "============================================================================================================";

        //HEADER
        $pdf->SetY(10+$offsetY);
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TRANSDRIZA S.A. DE C.V.", $border, 1, 'C');
        $pdf->SetX(73+$offsetX);
        $pdf->Cell(70,3,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TOLUCA EDO. DE MEXICO", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"R.F.C. TRA-891031-SXA", $border, 0, 'C');

        $folioAumentado = $_REQUEST['trViajesTractoresFolioPolizaHdn'] + 1;        
        
        //echo "es el mes".$_REQUEST['trPolizaGastosMesAfectacionHdn'];
        $sacarMes = date("m") -1;
        $sacarAnio = date("Y");                
        if($sacarMes == '0'){
            $sacarMes = 12;
            $sacarAnio = date("Y") -1;
        }
        $sqlGetFolio = "SELECT LPAD(MAX(folio) +1,6,'0') as folio ".
                    "FROM trgastosviajetractortbl ".
                    "WHERE folio like '%".$sacarMes."%'". 
                    "AND fechaEvento BETWEEN CAST('".$sacarAnio.'-'.$sacarMes.'-'.'01'."' AS DATE) AND CAST(now() AS DATE) ".
                    "AND claveMovimiento = 'GP';";

        $rsSqlGetFolio = fn_ejecuta_query($sqlGetFolio);

        $sqlUpdF = "UPDATE trGastosViajeTractorTbl ".
                    "SET folio = '".$rsSqlGetFolio['root'][0]['folio']."' ".
                    "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' AND claveMovimiento = 'GP' AND  concepto != '7025' ";

                    fn_ejecuta_query($sqlUpdF);        
     
        if($rsGetFolio['root'][0]['mesAnio'] != date("m")){
            $pdf->SetY(8+$offsetY);
            $pdf->SetX(175+$offsetX);
            $pdf->Cell(33,4,str_pad("COMPLEMENTO", 7, " "), $border, 1, 'L');

            $pdf->SetX(175+$offsetX);
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") .$rsSqlGetFolio['root'][0]['folio'], $border, 1, 'L');
        }else{
            $pdf->SetX(175+$offsetX);
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") .$rsSqlGetFolio['root'][0]['folio'], $border, 1, 'L');

                        $sqlUpdF = "UPDATE trGastosViajeTractorTbl ".
                                                "SET folio = '".$rsGetFolio['root'][0]['folio']."' ".
                                                "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' AND claveMovimiento = 'GP' ";
                        //fn_ejecuta_query($sqlUpdF);
        }
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") . substr($_REQUEST['trViajesTractoresMesAfectacionHdn'], 0, 3), $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, $border, 1, 'L');

        //DATOS
        $reimpresion = "";

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $reimpresion = " (REIMPRESION)";
        }

        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(78+$offsetX);
        $pdf->Cell(60,4,"POLIZA DE LIQUIDACION DE GASTOS".$reimpresion, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DIST DESTINO HEADERS
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(130+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 0, 'C');
        $pdf->SetX(160+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(180+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DATOS GENERALES DEL VIAJE
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"Talones  : ".$data['foliosTalonesViaje'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"No Autos : ".$data['numeroUnidades'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(40,4,"Unidad   : ".$data['tractor']." (V-".str_pad($data['viaje'], 6, '0',STR_PAD_LEFT).")", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Rend.    : ".$data['rendimiento'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Cve. Ope.: ".$data['claveChofer'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Kms/Viaje: ".$data['kilometrosComprobados'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);

        $litrosViaje = floatval($data['kilometrosComprobados']) / floatval($data['rendimiento']);
        $ltrComplemento = $litrosViaje +$sumaDieselExtra;
        echo($sumaDieselExtra);
        $litrosViaje = ceil($ltrComplemento);

        $pdf->Cell(30,4,"Lts/Apr  : ".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Lts/Rec  : ".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30, 4,"Lts Menos --->     0", $border, 1, 'L');

        //DIST DESTINO DATOS
        $maxTalones = 10;
        $xColumn = 110;

        $pdf->SetY(55+$offsetY);

        for ($i=0; $i < $maxTalones; $i++) {
            $pdf->SetX($xColumn+$offsetX);
            if($i+1 <= sizeof($destinos)){
                $pdf->Cell(20,4,$destinos[$i]['distribuidor'], $border, 0, 'C');
                //EL ICONV SE USA PARA QUE IMPRIMA BIEN LOS ACENTOS
                $pdf->Cell(30,4,toUTF8($destinos[$i]['plaza']), $border, 1, 'C');
            } else {
                $pdf->Cell(20,4,"--------", $border, 0, 'C');
                $pdf->Cell(30,4,"--------------", $border, 1, 'C');
            }

            if(floatval($maxTalones) / floatval($i+1) == floatval(2)){
                $xColumn = 160;
                $pdf->SetY(55+$offsetY);
            }
        }

        //CARGOS
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"C A R G O S", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"===========", $border, 1, 'L');

        $total = 0.00;
        $iva = 0.00;
        foreach ($gastos as $gasto) {


            if(!in_array($gasto['concepto'],array('7013', '7014'))){

                $cuentaContable = preg_replace("/(\*+)/",sprintf('%04d',$data['tractor']), $gasto['cuentaContable'], 1);

                $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $cuentaContable, 1);


                $pdf->SetX(5+$offsetX);
                $pdf->Cell(100,4,
                    str_pad($cuentaContable,24," ")." ".
                    substr($gasto['nombre'], 0, 11)." ".
                    str_pad(number_format($gasto['subtotal'],2,'.',','), 9+(11-strlen(substr($gasto['nombre'], 0, 11))), " ", STR_PAD_LEFT),
                    $border, 1, 'L');

                $total += floatval($gasto['subtotal']);
                $iva += floatval($gasto['iva']);
            }
        }

        $pdf->SetX(75+$offsetX);
        $pdf->Cell(30,4,"==========", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(45,4,"Total de comp. gastos. ", $border, 0, 'L');
        $pdf->SetX(75.5+$offsetX);
        $pdf->Cell(29.5,4,str_pad(number_format($total, 2, '.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetY(185+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaIva'], 24, " ")." ".
            "IVA ACREDIT ".
            str_pad(number_format($iva,2,'.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');

        $sqlUpdIva = "UPDATE trGastosViajeTractorTbl ".
            "SET importe = '".$iva."', subtotal ='".$iva."' WHERE concepto = '7012' AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ";

        fn_ejecuta_query($sqlUpdIva);

        $sqlUpdTotal = "UPDATE trGastosViajeTractorTbl ".
            "SET importe = '".$total."', subtotal ='".$total."' WHERE concepto = '7009' AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ";

        fn_ejecuta_query($sqlUpdTotal);

        //OBSERVACIONES
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(140+$offsetX);
        $pdf->Cell(30,4,"OBSERVACIONES", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"==================================", $border, 1, 'C');

        $pdf->SetX(125+$offsetX);
        $pdf->MultiCell(55, 4, $extras['observacionGastos'], $border, 'L');

        //ANTICIPOS
        $pdf->SetY(170+$offsetY);
        $pdf->SetX(138+$offsetX);
        $pdf->Cell(30,4,"ABONOS", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"================================", $border, 1, 'C');
        $pdf->SetX(132+$offsetX);
        $pdf->Cell(40,4,"Anticipo de Gastos", $border, 1, 'C');
        //LA SIGUIENTE CELDA VACIA ES SOLO PARA SALTAR EXACTAMENTE UNA LINEA
        $pdf->Cell(30,4,"", $border, 1, 'C');

        $totalAnticipo = 0.00;
        $importe = '0.00';
        $foliosAnticipos = "";

        foreach ($anticipos as $anticipo) {
            if($anticipo['importe'] == ""){
                $importe = '0.00';
            }
            else{

                $importe = abs($anticipo['importe']);

            }


            $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $anticipo['cuentaContable'], 1);
            $cuentaContable = preg_replace("/(\*+)/", $data['tractor'], $cuentaContable, 1);


            $pdf->SetX(115+$offsetX);
            $pdf->Cell(80,4,
                str_pad($cuentaContable,18," ")." ".
                substr($anticipo['plaza'], 0, 10)." ".
                str_pad(number_format($importe,2,'.',','), 10+(10-strlen(substr($anticipo['plaza'], 0, 10))), " ", STR_PAD_LEFT),
                $border, 1, 'L');

            $totalAnticipo += floatval($anticipo['importe']);
            $foliosAnticipos .= $anticipo['folio'];
        }

                $pdf->SetY(217+$offsetY);
                $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,"201.01401.070.0001 OPERADORES ".str_pad(number_format($rsCmpOperador['root'][0]['importe'],2,'.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');

                $concOperadores = $rsCmpOperador['root'][0]['importe'];
                $pdf->SetY(220+$offsetY);
                $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,"TOTAL ANTIC. EN PATIOS =====> ".str_pad(number_format($totalAnticipo+$concOperadores,2,'.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');

        //CONCEPTOS DE DIESEL EXTRA QUE NO SE SUMAN PARA LO DE DIESEL EXTRA
        $conceptosNoSuman = array('9000');
        $sumaDieselExtra = 0;

        foreach ($diesel as $d) {
            if(!in_array($d['concepto'], $conceptosNoSuman)){
                $sumaDieselExtra += floatval($d['importe']);
            }
        }

        /*$pdf->SetY(78+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "DIESEL EXTRA: ".str_pad($sumaDieselExtra, 6, ' ', STR_PAD_LEFT), $border, 1, 'L');*/

        $sacarMes = date("m") -1;
        $sacarAnio = date("Y");                
        if($sacarMes == '0'){
            $sacarMes = 12;
            $sacarAnio = date("Y") -1;
        }

        $sqlGetFolio = "SELECT LPAD(MAX(folio),6,'0') as folio ".
                    "FROM trgastosviajetractortbl ".
                    "WHERE fechaEvento BETWEEN CAST('".$sacarAnio.'-'.$sacarMes.'-'.'01'."' AS DATE) AND CAST(now() AS DATE) ".
                    "AND claveMovimiento = 'GP';";

        $rsSqlGetFolio = fn_ejecuta_query($sqlGetFolio);
        
        $pdf->SetY(85+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "FOLIOS ANT: ".$rsSqlGetFolio['root'][0]['folio'], $border, 1, 'L');

        //CARGO OPERADOR, PAGO EFECTIVO, COMPROBADO
        $cargosOperador = 0.00;
        $pagoEfectivo = 0.00;


        if($extras['importeCargoOperador'] != ""){
            $cargosOperador = floatval($extras['importeCargoOperador']);
            $cargosOperador = 0.00;
        }else  if($extras['importePagoEfectivo'] != ""){
            $pagoEfectivo = floatval($extras['importePagoEfectivo']);
        }

        $pdf->SetY(225+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaCargoOperador'], 24, " ")." ".
            "CARGO OPER ".
            str_pad(number_format($cargosOperador, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

                $pdf->SetY(226+$offsetY);
                $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,
            str_pad($extras['cuentaPagoEfectivo'], 18, " ")." ".
            "PAGO EFECT".
            str_pad(number_format($pagoEfectivo, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 1, 'L');

        $comprobado = $total + $iva;
                $pdf->SetY(220+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad("COMPROBADO", 25+10, " ", STR_PAD_LEFT)." ".
            str_pad(number_format($comprobado, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

        //TOTALES
        $pdf->SetY(230+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            "TOTAL  CARGOS ".
//            str_pad(number_format($comprobado + $cargosOperador, 2, '.',','),32," ",STR_PAD_LEFT),
              str_pad(number_format($comprobado , 2, '.',','),32," ",STR_PAD_LEFT),
            $border, 0, 'L');

                //totsl diesel cargo
        /*$pdf->SetY(235+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            "700.01402.037 EDEN-R DIESEL ".
            str_pad(number_format($diExtra, 2, '.',','),18," ",STR_PAD_LEFT),
            $border, 0, 'L');*/
                //total diesel abono
                /*$pdf->SetY(235+$offsetY);
                $pdf->SetX(115+$offsetX);
                $pdf->Cell(100,4,
                        "700.01402.037 EDEN-R DIESEL ".
                        str_pad(number_format($diExtra, 2, '.',','),12," ",STR_PAD_LEFT),
                        $border, 0, 'L');*/

                $pdf->SetY(231+$offsetY);
                $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,
            "TOTAL  ABONOS ".
            str_pad(number_format($totalAnticipo + $pagoEfectivo+$concOperadores, 2, '.',','),26," ",STR_PAD_LEFT),
            $border, 1, 'L');


        //REIMPRESION
                $pdf->SetY(237+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $pdf->SetX(92+$offsetX);
            $pdf->Cell(40,4,"R E I M P R E S I O N", $border, 1, 'C');
        }


        //FIRMAS
        $pdf->SetY(260+$offsetY);
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 1, 'C');

        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"E L A B O R O",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"FIRMA DEL OPERADOR",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"R E V I S O",$border, 1, 'C');
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,$usuarioElaboracion['root'][0]['nombre'],$border, 0, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(65,4,toUTF8($_SESSION['nombreUsr']),$border, 0, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,
            toUTF8($data['nombre']." ".$data['apellidoPaterno']." ".$data['apellidoMaterno']),
            $border, 1, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,"CORC-004                     P.D. No ".$folioGP,$border, 0, 'L');


        return $pdf;
    }

function generaCuadre($pdf, $data, $destinos, $gastos, $anticipos, $diesel, $extras){

        $sqlGetCentro = "SELECT centroDistribucion FROM trgastosviajetractortbl ".
                "WHERE idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                "AND claveMovimiento = 'GU' ";

        $rsGetCentro = fn_ejecuta_query($sqlGetCentro);

        $sqlGetElaboro = "SELECT us.idUsuario,us.nombre FROM trgastosviajetractortbl gt, segusuariostbl us ".
                         "WHERE us.idUsuario = gt.usuario ".
                         "AND gt.claveMovimiento = 'GU' ".
                         "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ";

        $usuarioElaboracion = fn_ejecuta_query($sqlGetElaboro);


        $sqlGetElaboro = "SELECT us.idUsuario,us.nombre FROM trgastosviajetractortbl gt, segusuariostbl us ".
                            "WHERE us.idUsuario = gt.usuario ".
                            "AND gt.claveMovimiento = 'GU' ".
                            "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ";

        $usuarioElaboracion = fn_ejecuta_query($sqlGetElaboro);

        $sqlCmpOperador = "SELECT concat(cc.cuentaContable,' ',co.nombre,'     ',gt.importe) as cmpOperador, gt.importe, gt.folio  ".
                    "FROM caconceptoscentrostbl cc, caconceptostbl co, trgastosviajetractortbl gt ".
                    "WHERE co.concepto = gt.concepto ".
                    "AND cc.concepto = co.concepto ".
                    //"AND cc.centroDistribucion = '".$rsGetCentro['root'][0]['centroDistribucion']."' ".
                    "AND cc.centroDistribucion = 'CDTOL' ".
                    "AND cc.concepto = '7025' ".
                    "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'];

        $rsCmpOperador = fn_ejecuta_query($sqlCmpOperador);

        $sqlCmpOperador1 = "SELECT concat(cc.cuentaContable,' ',co.nombre,'     ',gt.importe) as cmpOperador, gt.importe, gt.folio  ".
                    "FROM caconceptoscentrostbl cc, caconceptostbl co, trgastosviajetractortbl gt ".
                    "WHERE co.concepto = gt.concepto ".
                    "AND cc.concepto = co.concepto ".
                    "AND cc.concepto = '7026' ".
                    "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'];

        $rsCmpOperador1 = fn_ejecuta_query($sqlCmpOperador1);

        $impOperador = $rsCmpOperador['root'][0]['importe'];
        $folioImportes = $rsCmpOperador1['root'][0]['folio'];


        // MANDAR LLAMAR FOLIO Y CONCATENARLO CON EL EMS DE AFECTACION ACTUAL
        $sqlGetFolio = "SELECT CONCAT(date_format(NOW(), '%m'),LPAD(CONCAT('0',MAX(folio) + 1),4,'0')) as folio, SUBSTR(folio,1,2) as mesFolio, SUBSTR(now(),6,2) as mesAnio, SUBSTR(now(),1,4) as anioAnio, LPAD(CONCAT('0',MAX(folio) + 1),4,'0') as foleadora ".
                        "FROM trfoliostbl ".
                        "WHERE compania = 'TR' ".
                        "AND tipoDocumento = 'PZ' ".
                        "AND centroDistribucion = '".$rsGetCentro['root'][0]['centroDistribucion']."' ";

        $rsGetFolio = fn_ejecuta_query($sqlGetFolio);

        /*$sqlUpdFolio = "UPDATE trfoliostbl ".
                "SET folio = '".$rsGetFolio['root'][0]['foleadora']."' ".
                "WHERE centroDistribucion = '".$rsGetCentro['root'][0]['centroDistribucion']."' ".
                "AND tipoDocumento = 'PZ' ".
                "AND compania = 'TR'";

        $rsUpdFolio = fn_ejecuta_query($sqlUpdFolio);*/


        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;

        $pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->AddPage();
        $pdf->SetFont($font,'',9);
        $pdf->SetAutoPageBreak(false);

        date_default_timezone_set('America/Mexico_City');
        //echo date('d-M-Y');
        $fecha_01 = setlocale(LC_TIME,"spanish");
        $mesAnio = strftime("%B");
        $mesAfecta = strtoupper ($mesAnio);


        $fechaCompleta = date_create($extras['fechaEvento']);
        $fecha = date_format($fechaCompleta, "d/m/Y");
        $hora = date_format($fechaCompleta, "H:i:s");

        $linea = "============================================================================================================";

        //HEADER
        $folioAumentado = $_REQUEST['trViajesTractoresFolioPolizaHdn'] + 1;
        $pdf->SetY(10+$offsetY);
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TRANSDRIZA S.A. DE C.V.", $border, 1, 'C');
        $pdf->SetX(73+$offsetX);
        $pdf->Cell(70,3,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TOLUCA EDO. DE MEXICO", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"R.F.C. TRA-891031-SXA", $border, 0, 'C');
        $pdf->SetX(175+$offsetX);
        //$pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioAfectacion, $border, 1, 'L');
        $pdf->cell(35,4,str_pad("FOLIO",7," ").$folioImportes,$border,1,'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") . substr($mesAfecta, 0, 3), $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, $border, 1, 'L');

        //DATOS
        $reimpresion = "";

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $reimpresion = " (REIMPRESION)";
        }

        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(78+$offsetX);
        $pdf->Cell(60,4,"POLIZA DE LIQUIDACION DE GASTOS".$reimpresion, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DIST DESTINO HEADERS
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(130+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 0, 'C');
        $pdf->SetX(160+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(180+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DATOS GENERALES DEL VIAJE
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"Talones  : ".$data['foliosTalonesViaje'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"No Autos : ".$data['numeroUnidades'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(40,4,"Unidad   : ".$data['tractor']." (V-".str_pad($data['viaje'], 6, '0',STR_PAD_LEFT).")", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Rend.    : ".$data['rendimiento'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Cve. Ope.: ".$data['claveChofer'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Kms/Viaje: ".$data['kilometrosComprobados'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);

        $litrosViaje = floatval($data['kilometrosComprobados']) / floatval($data['rendimiento']);
        $ltrComplemento = $litrosViaje +$sumaDieselExtra;
        //echo($sumaDieselExtra);
        $litrosViaje = ceil($ltrComplemento);

        $pdf->Cell(30,4,"Lts/Apr  : ".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Lts/Rec  : ".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30, 4,"Lts Menos --->     0", $border, 1, 'L');

        //DIST DESTINO DATOS
        $maxTalones = 10;
        $xColumn = 110;

        $pdf->SetY(55+$offsetY);

        for ($i=0; $i < $maxTalones; $i++) {
            $pdf->SetX($xColumn+$offsetX);
            if($i+1 <= sizeof($destinos)){
                $pdf->Cell(20,4,$destinos[$i]['distribuidor'], $border, 0, 'C');
                //EL ICONV SE USA PARA QUE IMPRIMA BIEN LOS ACENTOS
                $pdf->Cell(30,4,toUTF8($destinos[$i]['plaza']), $border, 1, 'C');
            } else {
                $pdf->Cell(20,4,"--------", $border, 0, 'C');
                $pdf->Cell(30,4,"--------------", $border, 1, 'C');
            }

            if(floatval($maxTalones) / floatval($i+1) == floatval(2)){
                $xColumn = 160;
                $pdf->SetY(55+$offsetY);
            }
        }

        //CARGOS
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"C A R G O S", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"===========", $border, 1, 'L');

        $total = 0.00;
        $iva = 0.00;
        foreach ($gastos as $gasto) {


            if(!in_array($gasto['concepto'],array('7013', '7014'))){

                $cuentaContable = preg_replace("/(\*+)/",sprintf('%04d',$data['tractor']), $gasto['cuentaContable'], 1);

                $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $cuentaContable, 1);


                $pdf->SetX(5+$offsetX);
                $pdf->Cell(100,4,
                    str_pad($cuentaContable,24," ")." ".
                    substr($gasto['nombre'], 0, 11)." ".
                    str_pad(number_format(0.00,2,'.',','), 9+(11-strlen(substr($gasto['nombre'], 0, 11))), " ", STR_PAD_LEFT),
                    $border, 1, 'L');

                $total += floatval(0.00);
                $iva += floatval(0.00);
            }
        }

        $pdf->SetX(75+$offsetX);
        $pdf->Cell(30,4,"==========", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(45,4,"Total de comp. gastos. ", $border, 0, 'L');
        $pdf->SetX(75.5+$offsetX);
        $pdf->Cell(29.5,4,str_pad(number_format($total, 2, '.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetY(185+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaIva'], 24, " ")." ".
            "IVA ACREDIT ".
            str_pad(number_format($iva,2,'.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');


        //OBSERVACIONES
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(140+$offsetX);
        $pdf->Cell(30,4,"OBSERVACIONES", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"==================================", $border, 1, 'C');

        $pdf->SetX(125+$offsetX);
        $pdf->MultiCell(55, 4, $extras['observacionGastos'], $border, 'L');

        //ANTICIPOS
        $pdf->SetY(180+$offsetY);
        $pdf->SetX(138+$offsetX);
        $pdf->Cell(30,4,"ABONOS", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"================================", $border, 1, 'C');
        $pdf->SetX(132+$offsetX);
        $pdf->Cell(40,4,"Anticipo de Gastos", $border, 1, 'C');
        //LA SIGUIENTE CELDA VACIA ES SOLO PARA SALTAR EXACTAMENTE UNA LINEA
        $pdf->Cell(30,4,"", $border, 1, 'C');

        $totalAnticipo = 0.00;
        $importe = '0.00';
        $foliosAnticipos = "";

        foreach ($anticipos as $anticipo) {
            if($anticipo['importe'] == "")
                $importe = '0.00';
            else
                $importe = $anticipo['importe'];


            $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $anticipo['cuentaContable'], 1);
            $cuentaContable = preg_replace("/(\*+)/", $data['tractor'], $cuentaContable, 1);


            $pdf->SetX(115+$offsetX);
            $pdf->Cell(80,4,
                str_pad($cuentaContable,18," ")." ".
                substr($anticipo['plaza'], 0, 10)." ".
                str_pad(number_format(0.00,2,'.',','), 10+(10-strlen(substr($anticipo['plaza'], 0, 10))), " ", STR_PAD_LEFT),
                $border, 1, 'L');

            $totalAnticipo += floatval(0.00);
            $foliosAnticipos .= $anticipo['folio'];
        }
        // COMPLEMENTO OPERADORES
        //$pdf->SetY(192+$offsetY);
        //$pdf->Cell(308,4,$rsCmpOperador['root'][0]['cmpOperador'], $border, 1, 'C');

        $pdf->SetY(225+$offsetY);
        $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,"TOTAL ANTIC. EN PATIOS =====> ".str_pad(number_format(0.00,2,'.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');

        //CONCEPTOS DE DIESEL EXTRA QUE NO SE SUMAN PARA LO DE DIESEL EXTRA
        $conceptosNoSuman = array('9000');
        $sumaDieselExtra = 0;

        foreach ($diesel as $d) {
            if(!in_array($d['concepto'], $conceptosNoSuman)){
                $sumaDieselExtra += floatval($d['importe']);
            }
        }

        $pdf->SetY(78+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "DIESEL EXTRA: ".str_pad($sumaDieselExtra, 6, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "FOLIOS ANT: ".$foliosAnticipos, $border, 1, 'L');

        //CARGO OPERADOR, PAGO EFECTIVO, COMPROBADO
        $cargosOperador = 0.00;
        $pagoEfectivo = 0.00;


        if($extras['importeCargoOperador'] != ""){
            $cargosOperador = floatval($extras['importeCargoOperador']);
        }else  if($extras['importePagoEfectivo'] != ""){
            $pagoEfectivo = floatval($extras['importePagoEfectivo']);
        }

        $pdf->SetY(225+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad("201.01401.070.0001", 24, " ")." ".
            "OPERADORES".
            //str_pad(number_format($rsCmpOperador['root'][0]['importe'], 2, '.',','),11," ",STR_PAD_LEFT),
             str_pad(number_format($impOperador, 2, '.',','),11," ",STR_PAD_LEFT),
             //$pagoEfectivo
            $border, 2, 'L');

        $pdf->SetY(230+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaCargoOperador'], 24, " ")." ".
            "CARGO OPER ".
            str_pad(number_format($cargosOperador, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

        $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,
            str_pad($extras['cuentaPagoEfectivo'], 18, " ")." ".
            "PAGO EFECT".
            str_pad(number_format($impOperador, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 1, 'L');

        $comprobado = $total + $iva;
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad("COMPROBADO", 25+10, " ", STR_PAD_LEFT)." ".
            str_pad(number_format(0.00, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

        //TOTALES
        $pdf->SetY(240+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            "TOTAL  CARGOS ".
             str_pad(number_format($pagoEfectivo + $impOperador, 2, '.',','),32," ",STR_PAD_LEFT),
            $border, 2, 'L');

        $pdf->SetY(240+$offsetY);
        $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,
            "TOTAL  ABONOS ".
            str_pad(number_format(0.00 + $pagoEfectivo + $impOperador, 2, '.',','),26," ",STR_PAD_LEFT),
            $border, 1, 'L');


        //REIMPRESION
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $pdf->SetX(92+$offsetX);
            $pdf->Cell(40,4,"R E I M P R E S I O N", $border, 1, 'C');
        }


        //FIRMAS
        $pdf->SetY(260+$offsetY);
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 1, 'C');

        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"E L A B O R O",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"FIRMA DEL OPERADOR",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"R E V I S O",$border, 1, 'C');
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,$usuarioElaboracion['root'][0]['nombre'],$border, 0, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(65,4,toUTF8($_SESSION['nombreUsr']),$border, 0, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,
            toUTF8($data['nombre']." ".$data['apellidoPaterno']." ".$data['apellidoMaterno']),
            $border, 1, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,"CORC-005                     P.D. No ",$border, 0, 'L');


        return $pdf;
    }

    function generarPdfSueldos($pdf, $data, $diesel, $gastosSueldos, $extras){
        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;
        $ciaSesVal = 'CDTOL';

        $sqlGetCenDis = "SELECT centroDistribucion ".
                        "FROM trGastosViajeTractorTbl ".
                        "WHERE idViajeTractor= '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                        "AND concepto = '7009';";

        $rsSqlGetCenDis = fn_ejecuta_query($sqlGetCenDis);

        $sqlGetFolio = "SELECT CONCAT(date_format(NOW(), '%m'),LPAD(CONCAT('0',MAX(folio) + 1),4,'0')) as folio, SUBSTR(folio,1,2) as mesFolio, SUBSTR(now(),6,2) as mesAnio, SUBSTR(now(),1,4) as anioAnio, LPAD(CONCAT('0',MAX(folio) + 1),4,'0') as foleadora ".
                        "FROM trfoliostbl ".
                        "WHERE compania = 'TR' ".
                        "AND tipoDocumento = 'PZ' ".
                        "AND centroDistribucion = '".$ciaSesVal."' ";

        $rsGetFolio = fn_ejecuta_query($sqlGetFolio);



        $sqlGetElaboro = "SELECT us.idUsuario,us.nombre FROM trgastosviajetractortbl gt, segusuariostbl us ".
                                        "WHERE us.idUsuario = gt.usuario ".
                                        "AND gt.claveMovimiento = 'GU' ".
                                        "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ";

        $usuarioElaboracion = fn_ejecuta_query($sqlGetElaboro);


        $sqlUpdFolio = "UPDATE trfoliostbl ".
                "SET folio = '".$rsGetFolio['root'][0]['foleadora']."' ".
                "WHERE centroDistribucion = '".$ciaSesVal."' ".
                "AND tipoDocumento = 'PZ' ".
                "AND compania = 'TR'";

        //$rsUpdFolio = fn_ejecuta_query($sqlUpdFolio);

        $updFolioGP = "UPDATE trgastosviajetractortbl ".
            "SET folio = '".$rsGetFolio['root'][0]['folio']."' ".
            "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
            "AND folio = 0000 ".
            "AND claveMovimiento = 'GP' ".
            "AND concepto IN ('7026','7014');";

        //$rsUpdFolio = fn_ejecuta_query($updFolioGP);

        $updFolioGS = "UPDATE trgastosviajetractortbl ".
                    "SET folio = '".$rsGetFolio['root'][0]['folio']."' ".
                    "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                    "AND claveMovimiento = 'GS' ";

        //$rsUpdFolio = fn_ejecuta_query($updFolioGS);

        $updFolioGP = "UPDATE trgastosviajetractortbl ".
                    "SET folio = '".$rsGetFolio['root'][0]['folio']."' ".
                    "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                    "AND claveMovimiento = 'GP' ";

        //$rsUpdFolio = fn_ejecuta_query($updFolioGP);

        /*$sqlUpdExtra =  "UPDATE trgastosviajetractortbl ".
                        "SET folio = '".$rsGetFolio['root'][0]['folio']."' ".
                        "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                        "AND claveMovimiento = 'GP' ".
                        "AND concepto LIKE '9%'; ";

        $rsUpdExtra = fn_ejecuta_query($sqlUpdExtra); */

        $sqlUpdMesGP = "UPDATE trgastosviajetractortbl ".
                        "SET mesAfectacion = date_format(NOW(),'%m') ".
                        "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                        "AND  claveMovimiento = 'GP' ";
                        //"AND folio = '".$rsGetFolio['root'][0]['folio']."'";

        $rsSqlUpdMesGP = fn_ejecuta_query($sqlUpdMesGP);


        //CONSULTA ULTIMO FOLIO PARA SUELDOS
        $sqlGetFolioGS = "SELECT distinct folio ".
                            "FROM trgastosviajetractortbl ".
                            "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                            "AND claveMovimiento = 'GS'";

        $rsFolioTMPS = fn_ejecuta_query($sqlGetFolioGS);

        $folioGS = $rsFolioTMPS['root'][0]['folio'];

        $mesAnio = strftime("%B");
        $mesAfecta = strtoupper ($mesAnio);


        $pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->AddPage();
        $pdf->SetFont($font,'',9);
        $pdf->SetAutoPageBreak(false);

        $fechaCompleta = date_create($extras['fechaEvento']);
        $fecha = date_format($fechaCompleta, "d/m/Y");
        $hora = date_format($fechaCompleta, "H:i:s");

        $linea = "============================================================================================================";

        //DEDUCCIONES
        //VAN AQUI POR LOGISTICA PARA MANEJAR LA LISTA DE DEDUCCIONES Y PERCEPCIONES AL MISMO TIEMPO
        $pdf->SetY(145+$offsetY);
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,6,"DEDUCCIONES", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"-----------", $border, 1, 'C');

        $totalDeducciones = 0;

        //OBTENGO LOS CONCEPTOS DE OTROS Y EXTRAS
        $otrosKm = 0;
        $otrosCosto = 0;
        $kmExtra = 0;
        $autosExtra = 0;
        $extraCosto = 0;


        foreach ($gastosSueldos as &$gasto) {
            if($gasto['concepto'] == '142') {
                $otrosKm = $gasto['importe'];
                $otrosCosto = $gasto['importeConcepto'];
            } else if($gasto['concepto'] == '140'){
                $kmExtra = $gasto['importe'];
                $extraCosto = $gasto['importeConcepto'];

                $temp = explode(' ',$gasto['observaciones']);
                $autosExtra = $temp[0];
            } else {
                $pdf->SetX(120+$offsetX);
                $pdf->Cell(90,4,
                    str_pad(substr($gasto['cuentaContable'], 0, 19),20," ")." ".
                    substr($gasto['nombre'], 0, 11)." ".
                    str_pad("$".number_format(floatval($gasto['importe'])*-1,2,'.',','), 11+(11-strlen(substr($gasto['nombre'], 0, 11))), " ", STR_PAD_LEFT),
                    $border, 1, 'L');

                $totalDeducciones += floatval($gasto['importe']) * -1;
            }
        }

        $pdf->SetX(186+$offsetX);
        $pdf->Cell(30,4,"==========", $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(45,4,"TOTAL  DEDUCCIONES ", $border, 0, 'L');
        $pdf->SetX(185.5+$offsetX);
        $pdf->Cell(29.5,4,str_pad("$".number_format($totalDeducciones, 2, '.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');

        //HEADER
        $pdf->SetY(10+$offsetY);
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TRANSDRIZA S.A. DE C.V.", $border, 1, 'C');
        $pdf->SetX(73+$offsetX);
        $pdf->Cell(70,3,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TOLUCA EDO. DE MEXICO", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"R.F.C. TRA-891031-SXA", $border, 0, 'C');
        $pdf->SetX(175+$offsetX);

        $sqlGetFolioGS = "SELECT distinct folio ".
                            "FROM trgastosviajetractortbl ".
                            "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                            "AND claveMovimiento = 'GS'";

        $rsFolioTMPS = fn_ejecuta_query($sqlGetFolioGS);

        $folioGS = $rsTMP['root'][0]['numFolio'];

        $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioGS, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") . substr($mesAfecta, 0, 3), $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, $border, 1, 'L');

        //DATOS
        $reimpresion = "";

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $reimpresion = " (REIMPRESION)";
        }

        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,"POLIZA DE LIQUIDACION DE SUELDO DE OPERADORES".$reimpresion, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"TALONES-UNIDAD-OPERADOR".$reimpresion, $border, 0, 'L');
        $pdf->SetX(150+$offsetX);
        $pdf->Cell(30,4,"IMPORTE".$reimpresion, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DATOS GENERALES DEL VIAJE
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"Talones  : ".$data['foliosTalonesViaje'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"No Autos : ".$data['numeroUnidades'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"Unidad   : ".$data['tractor']." (V-".str_pad($data['viaje'], 6, '0',STR_PAD_LEFT).")", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(40,4,"Cve. Ope.: ".$data['claveChofer'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,toUTF8($data['nombre']." ".$data['apellidoPaterno']." ".$data['apellidoMaterno']), $border, 1, 'L');

        $pdf->SetY(75+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"====================================================", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,5,"DIESEL EXTRA", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"====================================================", $border, 1, 'L');

        //DATOS DE DIESEL EXTRA
        $sqlGetDieselExtra = "SELECT gv.concepto, gv.importe, co.nombre ".
                             "FROM trGastosViajeTractorTbl gv, caConceptosTbl co ".
                             "WHERE gv.concepto = co.concepto ".
                             "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                             "AND gv.concepto like('9%') ".
                             "AND gv.claveMovimiento = 'GP' ";

        $dieselExtra = fn_ejecuta_query($sqlGetDieselExtra);


        if(sizeof($dieselExtra['root'])>=1){
            $numeroY = 90;
            $numeroVeces =  sizeof($dieselExtra) -1;
            for ($i=0; $i < $numeroVeces; $i++) {
                $pdf->SetY($numeroY+$offsetY);
                $pdf->SetX(5+$offsetX);
                $pdf->Cell(100,4,$dieselExtra['root'][$i]['concepto']." ".$dieselExtra['root'][$i]['nombre']." ".$dieselExtra['root'][$i]['importe']." lts.", $border, 1, 'L');
                $numeroY = $numeroY + 3;
            }
        }


        //ADICIONALES
        $kmsRecorridos = floatval($data['kilometrosTabulados'] * 2);
        $kmAdicionales = floatval($data['kilometrosComprobados']) - floatval($kmsRecorridos);
        $pagoNormal = floatval($data['kilometrosComprobados']) * floatval($extras['tarifaSueldo']);

        $pdf->SetY(55+$offsetY);
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Kms/Viaje      ".str_pad(number_format($kmsRecorridos,2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Kms/Adicionales".str_pad(number_format($kmAdicionales,2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Kms/Totales    ".str_pad(number_format($data['kilometrosComprobados'],2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Pago x Km/viaje".str_pad("$".number_format($extras['tarifaSueldo'],2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"============", $border, 1, 'R');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Total Pago Normal".str_pad("$".number_format($pagoNormal,2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');

        //OTROS
        $totalOtros = floatval($otrosKm) * floatval($otrosCosto);

        $pdf->SetY(85+$offsetY);
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Otros/Kms        ".str_pad(number_format($otrosKm, 2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Costo x Otros/Kms".str_pad("$".number_format($otrosCosto, 2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"============", $border, 1, 'R');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Total Otros      ".str_pad("$".number_format($totalOtros, 2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');

        //EXTRAS
        $totalExtra = floatval($autosExtra) * floatval($kmExtra) * floatval($extraCosto);

        $pdf->SetY(106+$offsetY);
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Autos Extras   ".str_pad(number_format($autosExtra, 0,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Costo x Km     ".str_pad("$".number_format($extraCosto, 2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Kms Recs.      ".str_pad(number_format($kmExtra, 2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"============", $border, 1, 'R');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Total Extras   ".str_pad("$".number_format($totalExtra, 2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');

        //TOTAL DE SUELDOS
        $totalSueldos = $pagoNormal+$totalOtros+$totalExtra;

        $pdf->SetY(135+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"====================================================", $border, 0, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Total de sueldos".str_pad("$".number_format($totalSueldos, 2,'.',','), 24, ' ', STR_PAD_LEFT), $border, 1, 'L');

        //INGRESOS
        $pdf->SetY(145+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,6,"INGRESOS", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"-----------", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4, $extras['cuentaSueldo'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4, "SUELDO  $".number_format($totalSueldos, 2,'.',','), $border, 1, 'L');

        //INSERCION DEL SUELDO

        $sqlAddPolizaIa = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, ".
                "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (".
                $_REQUEST['trViajesTractoresIdViajeHdn'].",".
                "'138',".
                "'".$rsSqlGetCenDis['root'][0]['centroDistribucion']."',".
                "'".$rsGetFolio['root'][0]['folio']."',".
                "NOW(),".
                "'".$extras['cuentaSueldo']."',".
                "date_format(NOW(), '%m'),".
                floatval($totalSueldos).",".
                "'',".
                "'GS',".
                $_SESSION['idUsuario'].",".
                "'".$_SERVER['REMOTE_ADDR']."', ".
                "'".floatval($totalSueldos)."')";

        fn_ejecuta_query($sqlAddPolizaIa);


        //GARANTIA - ESTO ES FIJO PORQUE YA NO SE USA
        $pdf->SetY(170+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"Costo dias Garantia: $0.00", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"No, Dias              0", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"SUELDO DE ", $border, 1, 'L');

        //FECHAS
        $pdf->SetY(190+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,5,"FECHAS", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"====================================================", $border, 1, 'L');
        $pdf->SetY(207+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,5,"====================================================", $border, 1, 'L');

        //SUMAS
        $sCargo = 0.00;
        $anticiposNoComprobados = 0.00;
        $pagoNeto = 0.00;

        if($extras['importeCargoOperador'] != "")
            $sCargo = floatval($extras['importeCargoOperador']);

        $pagoNeto = $totalSueldos - $totalDeducciones - $sCargo;

        if($pagoNeto < 0){
            $anticiposNoComprobados = $pagoNeto;
            $pagoNeto = 0.00;
        }
echo $anticiposNoComprobados;

        if($anticiposNoComprobados < 0){
            $anticiposNoComprobados = $anticiposNoComprobados * -1;
        }

        $pdf->SetY(220+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,
            str_pad($extras['cuentaNoComprobados'], 19, ' ').
            " ANTIC. GASTOS NO COMP.".
            str_pad("$".number_format($anticiposNoComprobados, 2,'.',','), 10, ' ', STR_PAD_LEFT), $border, 0, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(60,4,
            str_pad($extras['cuentaSCargo'], 19, ' ').
            " S/CARGO. ".
            str_pad("$".number_format($sCargo, 2,'.',','), 15, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(60,4,
            str_pad($extras['cuentaPagoNeto'], 19, ' ').
            " PAGO NETO. ".
            str_pad("$".number_format($pagoNeto, 2,'.',','), 13, ' ', STR_PAD_LEFT), $border, 1, 'L');

        //TOTALES
        $totalIngresos = $totalSueldos + $anticiposNoComprobados;
        $totalEgresos = $sCargo + $pagoNeto + $totalDeducciones;

        $pdf->SetY(235+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4, str_pad("TOTAL DE INGRESOS ", 38, ' ', STR_PAD_LEFT).
            str_pad("$".number_format($totalIngresos, 2,'.',','), 14, ' ', STR_PAD_LEFT), $border, 0, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4, "TOTAL DE EGRESOS ".
            str_pad("$".number_format($totalEgresos, 2,'.',','), 27, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //TEXTO Y REIMPRESION
        $pdf->SetX(5+$offsetX);
        $pdf->MultiCell(200, 4,
            "EL SUELDO INCLUYE LA CUOTA POR KILOMETRO RECORRIDO MAS LA PARTE PROPORCIONAL DEL SEPTIMO DIA Y DE LOS DIAS FESTIVOS"
            , $border, 'L');

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $pdf->SetX(92+$offsetX);
            $pdf->Cell(40,4,"R E I M P R E S I O N", $border, 1, 'C');
        }


        //FIRMAS
        $pdf->SetY(260+$offsetY);
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 1, 'C');

        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"E L A B O R O",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"FIRMA DEL OPERADOR",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"R E V I S O",$border, 1, 'C');
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,$usuarioElaboracion['root'][0]['nombre'],$border, 0, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(65,4,toUTF8($_SESSION['nombreUsr']),$border, 0, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,
            toUTF8($data['nombre']." ".$data['apellidoPaterno']." ".$data['apellidoMaterno']),
            $border, 1, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,"CORC-005                     P.D. No ",$border, 0, 'L');


        return $pdf;
    }

    function generarPdfMacheteros($pdf){
        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;

        $pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->AddPage();
        $pdf->SetFont($font,'',9);
        $pdf->SetAutoPageBreak(false);



        return $pdf;
    }

?>
