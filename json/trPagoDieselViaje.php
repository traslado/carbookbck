<?php
    session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    //$_REQUEST = trasformUppercase($_REQUEST);

	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['trPagoDieselHdn']){
        case 'getTractorDiesel':
            getTractorDiesel();
            break;
		case 'getDatosViaje':
			getDatosViaje();
			break;
		case 'getTractorCancel':
			getTractorCancel();
			break;
		case 'cancelaDiesel':
			cancelaDiesel();
			break;
		case 'addPagoDiesel':
			addPagoDiesel();
			break;
    case 'consultaConceptos':
      consultaConceptos();
      break;
    case 'updConcepto':
      updConcepto();
      break;
    case 'updKmsTab':
      updKmsTab();
      default:
            echo 'no esta entrando';
    }

    function getTractorDiesel(){
        $sqlGetTractor =    "SELECT distinct tr.tractor ".
                            "FROM trviajestractorestbl vt, caTractoresTbl tr, trgastosviajetractortbl gt ".
                            "WHERE vt.idTractor = tr.idtractor ".
                            "AND vt.idViajeTractor = gt.idViajeTractor ".
                            "AND vt.claveMovimiento IN ('VC','VG','VF','VE') ".
                            "AND gt.concepto NOT IN ('2312','2313','2314') ".
                            "AND vt.fechaEvento = (SELECT max(fechaEvento) FROM trviajestractorestbl v1 WHERE v1.idViajeTractor = vt.idViajeTractor) ".
                            "AND tr.compania = '".$_REQUEST['pagoDieselCompaniaCmb']."';";

        $rsGetTractor = fn_ejecuta_query($sqlGetTractor);

        echo json_encode($rsGetTractor);
    }

    function getDatosViaje(){
        $sqlGetTractor =    "SELECT concat(vt.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as claveChofer,tr.rendimiento, ".
                            "(SELECT pl1.plaza FROM caplazastbl pl1 WHERE pl1.idPlaza = vt.idPlazaOrigen) as origen, ".
                            "(SELECT pl1.plaza FROM caplazastbl pl1 WHERE pl1.idPlaza = vt.idPlazaDestino) as destino, vt.numeroRepartos, ".
                            "(SELECT COUNT(distribuidor) FROM trtalonesviajestbl tl WHERE vt.idViajeTractor =tl.idViajeTractor) as numeroTalones, vt.numeroUnidades, vt.fechaEvento, ".
                            "vt.kilometrosTabulados, round(sum(vt.kilometrosTabulados/tr.rendimiento)) as litrosViaje, ".
                            "sum((SELECT importe FROM caconceptoscentrostbl WHERE centroDistribucion = '".$_SESSION['usuCompania']."' AND concepto = 2315 ) * round((vt.kilometrosTabulados/tr.rendimiento)) ) as total, ".
                            "(SELECT importe FROM caconceptoscentrostbl WHERE centroDistribucion = '".$_SESSION['usuCompania']."' AND concepto = 2315 ) as precioDiesel, vt.idViajeTractor ".
                            "FROM trviajestractorestbl vt, caTractoresTbl tr, cachoferestbl ch ".
                            "WHERE vt.idTractor = tr.idtractor ".
                            "AND vt.claveChofer = ch.claveChofer ".
                            "AND vt.claveMovimiento IN ('VG','VF','VE','VC') ".
                            "AND vt.fechaEvento = (SELECT max(v1.fechaEvento) FROM trviajestractorestbl v1 WHERE v1.idTractor = vt.idTractor and v1.idViajePadre is null) ".
                            "AND tr.compania = '".$_REQUEST['pagoDieselCompaniaCmb']."' ".
                            "AND tr.tractor = '".$_REQUEST['pagoDieselTractorCmb']."';";

        $rsGetTractor = fn_ejecuta_query($sqlGetTractor);

        echo json_encode($rsGetTractor);
    }

    function addPagoDiesel(){
        // echo("ES EL idViajeTractor".$_REQUEST['pagoDieselIdViajeHdn1']);

        $sqlGetfolio = "SELECT SUM(FOLIO + 1) as folio FROM trfoliostbl WHERE centroDistribucion = '".$_SESSION['usuCompania']."' AND compania = 'TR' AND tipoDocumento = 'DI';";
        $rsFolio = fn_ejecuta_query($sqlGetfolio);
        $folio = str_pad((int) $rsFolio['root'][0]['folio'],4,"0",STR_PAD_LEFT);

        $sqlAdd_01 =    "INSERT INTO trgastosviajetractortbl(idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, numeroTarjeta, litros, usuario, ip, subTotal, iva) ".
                        "VALUES (".$_REQUEST['pagoDieselIdViajeHdn1'].", ".
                        "'2312', ".
                        "'".$_SESSION['usuCompania']."', ".
                        "'".$folio."', ".
                        "DATE_FORMAT(NOW(),'%Y-%m-%d'), ".
                        "NULL, ".
                        "DATE_FORMAT(NOW(),'%m'), ".
                        $_REQUEST['pagoDieselLitrosDsp'].", ".
                        "'', ".
                        "'GD', ".
                        "NULL, ".
                        "NULL, ".
                        $_SESSION['idUsuario'].", ".
                        "'".$_SERVER['REMOTE_ADDR']."', ".
                        $_REQUEST['pagoDieselLitrosDsp'].", ".
                        "0)";

        fn_ejecuta_query($sqlAdd_01);

        $sqlAdd_02 =    "INSERT INTO trgastosviajetractortbl(idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, numeroTarjeta, litros, usuario, ip, subTotal, iva) ".
                        "VALUES (".$_REQUEST['pagoDieselIdViajeHdn1'].", ".
                        "'2313', ".
                        "'".$_SESSION['usuCompania']."', ".
                        "'".$folio."', ".
                        "DATE_FORMAT(NOW(),'%Y-%m-%d'), ".
                        "NULL, ".
                        "DATE_FORMAT(NOW(),'%m'), ".
                        $_REQUEST['pagoDieselPrecioDsp'].", ".
                        "'', ".
                        "'GD', ".
                        "NULL, ".
                        "NULL, ".
                        $_SESSION['idUsuario'].", ".
                        "'".$_SERVER['REMOTE_ADDR']."', ".
                        $_REQUEST['pagoDieselPrecioDsp'].", ".
                        "0)";

        fn_ejecuta_query($sqlAdd_02);

        $sqlAdd_03 =    "INSERT INTO trgastosviajetractortbl(idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, numeroTarjeta, litros, usuario, ip, subTotal, iva) ".
                        "VALUES (".$_REQUEST['pagoDieselIdViajeHdn1'].", ".
                        "'2314', ".
                        "'".$_SESSION['usuCompania']."', ".
                        "'".$folio."', ".
                        "DATE_FORMAT(NOW(),'%Y-%m-%d'), ".
                        "NULL, ".
                        "DATE_FORMAT(NOW(),'%m'), ".
                        $_REQUEST['pagoDieselTotalDsp'].", ".
                        "'', ".
                        "'GD', ".
                        "NULL, ".
                        "NULL, ".
                        $_SESSION['idUsuario'].", ".
                        "'".$_SERVER['REMOTE_ADDR']."', ".
                        $_REQUEST['pagoDieselTotalDsp'].", ".
                        "0)";

        fn_ejecuta_query($sqlAdd_03);

        $sqlUpdFolio =  "UPDATE trfoliostbl ".
                        "SET folio = '".$rsFolio['root'][0]['folio']."' ".
                        "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                        "AND compania = 'TR' ".
                        "AND tipoDocumento = 'DI';";

        fn_ejecuta_query($sqlUpdFolio);

        $jsonFol = array('folio' => $folio);
        echo json_encode($jsonFol);
    }

        function getTractorCancel(){
        $sqlGetTractor =    "SELECT DISTINCT tr.tractor ".
                            "FROM trviajestractorestbl vt, caTractoresTbl tr, trgastosviajetractortbl gt ".
                            "WHERE vt.idTractor = tr.idtractor ".
                            "AND vt.idViajeTractor = gt.idViajeTractor ".
                            "AND vt.claveMovimiento IN ('VV','VG','VF','VE') ".
                            "AND gt.concepto IN('2312','2313','2314') ".
                            "AND gt.claveMovimiento IN ('D','GD') ".
                            "AND vt.fechaEvento= (SELECT max(fechaEvento) FROM trviajestractorestbl v1 WHERE v1.idViajeTractor = vt.idViajeTractor) ".
                            "AND tr.compania = '".$_REQUEST['pagoDieselCancelacionCompaniaCmb']."';";

        $rsGetTractor = fn_ejecuta_query($sqlGetTractor);

        echo json_encode($rsGetTractor);
    }

    function cancelaDiesel(){
        $sqlUpdDiesel =     "UPDATE trgastosviajetractortbl ".
                            "SET claveMovimiento = 'XD' ".
                            "WHERE claveMovimiento IN ('D','GD') ".
                            "AND concepto IN (2312,2313,2314) ".
                            "AND idViajeTractor = '".$_REQUEST['pagoDieselIdViajeHdn']."';";

        $rsUpdDiesel = fn_ejecuta_query($sqlUpdDiesel);

        echo json_encode($rsUpdDiesel);
    }

    function consultaConceptos(){
      // $ciaSesVal = 'CDTOL';

      $sqlGetConceptos = "SELECT distinct g1.centroDistribucion,g1.folio, (SELECT g2.importe from trgastosviajetractortbl g2 WHERE g2.idViajeTractor = g1.idViajeTractor AND concepto = 2312 AND g1.folio = g2.folio) as c2312, ".
                          "(SELECT g2.importe from trgastosviajetractortbl g2 WHERE g2.idViajeTractor = g1.idViajeTractor AND concepto = 2313 AND g1.folio = g2.folio) as c2313, ".
                          "(SELECT g2.importe from trgastosviajetractortbl g2 WHERE g2.idViajeTractor = g1.idViajeTractor AND concepto = 2314 AND g1.folio = g2.folio) as c2314, g1.fechaEvento ".
                          "FROM trgastosviajetractortbl g1 ".
                          "WHERE idViajeTractor = '".$_REQUEST['pagoDieselCancelacionIdViajeHdn']."' ".
                          "  AND CONCEPTO IN (2312,2313,2314) ".
                          "  AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                          "  AND claveMovimiento = 'GD' ".
                          "ORDER BY FOLIO, fechaEvento;";

        $rsGetCon = fn_ejecuta_query($sqlGetConceptos);

        echo json_encode($rsGetCon);
    }

    function updConcepto(){
      $sqlUpdConcepto = "UPDATE trgastosviajetractortbl ".
                        "SET claveMovimiento = 'DX' ".
                        "WHERE claveMovimiento = 'GD' ".
                        "AND idViajeTractor = '".$_REQUEST['pagoDieselCancelacionIdViajeHdn']."' ".
                        "AND folio = '".$_REQUEST['pagoDieselCancelacionFolioHdn']."';";

      $rsGetCon = fn_ejecuta_query($sqlUpdConcepto);
      echo json_encode($rsGetCon);
    }

    function updKmsTab(){
      $updKms = "UPDATE trviajestractorestbl ".
                "SET kilometrosTabulados = '".$_REQUEST['pagoDieselKilometrosDsp']."' ".
                "WHERE idViajeTractor = '".$_REQUEST['pagoDieselIdViajeHdn1']."';  ";

                //$rsGetCon = fn_ejecuta_query($updKms);
                //echo json_encode($rsGetCon);
    }



?>
