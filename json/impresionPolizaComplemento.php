<?php
    setlocale(LC_TIME, 'es_MX.utf8');
    session_start();

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("trViajesTractores.php");



    if($_REQUEST['trViajesTractoresIdViajeHdn'] != ''){

        $pdf = new FPDF('P', 'mm', array(216, 280));
        //DATOS GENERALES DEL VIAJE
        $data = getHistoricoViajes();


        //DESTINOS - TALONES
        $sqlGetDestinos = "SELECT tv.distribuidor, pl.plaza ".
                            "FROM trtalonesviajestbl tv, caPlazasTbl pl ".
                            "WHERE tv.idPlazaDestino = pl.idPlaza ".
                            "AND tv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                            "AND tv.claveMovimiento != 'TX' ";

        $destinos = fn_ejecuta_query($sqlGetDestinos);

        //GASTOS DE POLIZA (GASTOS COMPROBACION)
        $sqlGetGastosPoliza = "SELECT cc.concepto, cc.cuentaContable, gv.iva, gv.subtotal, gv.importe, co.nombre, ".
                                "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                    "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                                "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioTMP."' ".
                                    "AND gv.claveMovimiento = 'GP' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".                                    
                                    "WHERE gv2.folio = '".$folioTMP."'   AND gv2.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']. " LIMIT 1) ".
                                "AND ge.tabla = 'comprobacionPoliza' ".
                                "GROUP BY ge.estatus ";

        $gastos = fn_ejecuta_query($sqlGetGastosPoliza);

        //GASTOS DE SUELDOS
        $sqlGetSueldos = "SELECT cc.concepto, cc.cuentaContable, gv.iva, gv.subtotal, gv.importe, gv.observaciones, co.nombre, ge.columna, ".
                            "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE cc.concepto = gv.concepto ".
                                "AND cc.centroDistribucion = '".$_SESSION['usuCompania']."' ) AS importeConcepto, ".
                            "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                            "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioTMP."' ".
                                    "AND gv.claveMovimiento = 'GS' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".                                    
                                    "WHERE gv2.folio = '".$folioTMP."'   AND gv2.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']. " LIMIT 1) ".
                                "AND ge.tabla = 'trPolizaGastosTbl' ".
                                "AND (ge.columna = 'deducciones' OR ge.columna = 'percepciones') ".
                                "ORDER BY ge.estatus ";

        $gastosSueldos = fn_ejecuta_query($sqlGetSueldos);


        //ANTICIPOS
        $sqlGetAnticiposStr =   "SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(cuentaContable,4,0) FROM cachoferestbl WHERE claveChofer = (SELECT claveChofer FROM trviajestractorestbl WHERE idViajeTractor =".$_REQUEST['trViajesTractoresIdViajeHdn']." ))) as cuentaContable, ".
                                    "(SELECT nombre FROM caconceptostbl pl  WHERE  pl.concepto = cc.concepto) AS plaza, ".
                                     "(SELECT SUM(gv.importe) FROM trGastosViajeTractorTbl gv WHERE gv.concepto = cc.concepto AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." AND gv.claveMovimiento != 'XP') AS importe, ".
                                        "(SELECT GROUP_CONCAT(gv.folio SEPARATOR '-') FROM trGastosViajeTractorTbl gv ".
                                        "WHERE  claveMovimiento != 'GX' AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']."  and gv.concepto=7001 ".
                                        "GROUP BY gv.centroDistribucion) AS folio ".
                                        "FROM caConceptosCentrosTbl cc ".
                                        "WHERE cc.concepto IN ('7009','7016','7015','7023','7024') ".
                                        "AND cc.centroDistribucion NOT IN('CDTSA','CDSFE','CDLCL','CDSLP') ".
                                        "GROUP BY cc.concepto " ;

        $anticipos = fn_ejecuta_query($sqlGetAnticiposStr);


        //DIESEL EXTRA
        $sqlGetDieselExtra = "SELECT gv.concepto, gv.importe, co.nombre ".
                             "FROM trGastosViajeTractorTbl gv, caConceptosTbl co ".
                             "WHERE gv.concepto = co.concepto ".
                             "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                             "AND gv.folio = '".$folioTMP."' ".
                             "AND gv.claveMovimiento = 'GP' ";

        $dieselExtra = fn_ejecuta_query($sqlGetDieselExtra);


        
        $conceptoSueldo = '144';


        //DATOS EXTRA
        $sqlGetExtra = "SELECT gv.fechaEvento, ".
                        "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7012' ) AS cuentaIva, ".
                        "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7013') AS cuentaCargoOperador,  ".
                        "(SELECT gv2.importe FROM trGastosViajeTractorTbl gv2 WHERE gv2.idViajeTractor = gv.idViajeTractor AND gv2.folio = gv.folio AND gv2.concepto = '7013') AS importeCargoOperador, ".
                        "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7014') AS cuentaPagoEfectivo, ".
                        "(SELECT gv3.importe FROM trGastosViajeTractorTbl gv3 WHERE gv3.idViajeTractor = gv.idViajeTractor AND gv3.folio = gv.folio AND gv3.concepto = '7014') AS importePagoEfectivo, ".
                        "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '2231') AS cuentaNoComprobados, ".
                        "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '141') AS cuentaPagoNeto, ".
                        "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' AND concepto = '".$conceptoSueldo."') AS tarifaSueldo, ".
                        "(SELECT concat(SUBSTRING(cc.cuentaContable, 1,13),'.', ".
                                       "LPAD((SELECT ta.tractor FROM catractorestbl ta ".
                                             "WHERE idTractor = (SELECT vt.idTractor FROM trviajestractorestbl vt ".
                                                                "WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")),4,0),'.', ".
                                                                "(SELECT vt.claveChofer FROM trviajestractorestbl vt ".
                                                                " WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")) as cuentaContable ".
                        " FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' AND concepto = '".$conceptoSueldo."') AS cuentaSueldo, ".
                        "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' AND concepto = '145') AS cuentaSCargo ".
                        "FROM trGastosViajeTractorTbl gv ".
                        "WHERE gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                        "AND gv.folio = '".$folioTMP."' ".
                        "AND gv.claveMovimiento = 'GP' LIMIT 1;";

        $extras = fn_ejecuta_query($sqlGetExtra);

        $sqlCmpOperador = "SELECT concat(cc.cuentaContable,' ',co.nombre,'     ',gt.importe) as cmpOperador, gt.importe ".
                          "FROM caconceptoscentrostbl cc, caconceptostbl co, trgastosviajetractortbl gt ".
                          "WHERE co.concepto = gt.concepto ".
                          "AND cc.concepto = co.concepto ".
                          "AND cc.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                          "AND cc.concepto = '7025' ".
                          "AND gt.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ";

        $rsCmpOperador = fn_ejecuta_query($sqlCmpOperador);


        if($rsCmpOperador['root'][0]['importe'] == null){
                echo "imposible generar poliza";
        }
        else{

            $pdf = generarPdfComplemento($pdf, $data['root'][0], $destinos['root'], $gastos['root'], $anticipos['root'], $dieselExtra['root'],$extras['root'][0],$folioTMP);
        }

        $pdf->Output('poliza.pdf', 'I');
    } else {
        echo "Error al obtener el ID del viaje";
    }
//FALTA GENERAR EL PDF
    function generarPdfComplemento($pdf, $data, $destinos, $gastos, $anticipos, $diesel, $extras, $folio){
        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;
        $folioTMP= $folio;

            $sqlGetElaboro = "SELECT DISTINCT us.idUsuario,us.nombre FROM trgastosviajetractortbl gt, segusuariostbl us ".
                                                    "WHERE us.idUsuario = gt.usuario ".
                                                    "AND gt.claveMovimiento = 'GP' ".
                                                    "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ";

            $usuarioElaboracion = fn_ejecuta_query($sqlGetElaboro);

            $sqlCmpOperador = "SELECT concat(cc.cuentaContable,' ',co.nombre,'     ',gt.importe) as cmpOperador, gt.importe ".
                                    "FROM caconceptoscentrostbl cc, caconceptostbl co, trgastosviajetractortbl gt ".
                                    "WHERE co.concepto = gt.concepto ".
                                    "AND cc.concepto = co.concepto ".
                                    //"AND cc.centroDistribucion = '".$rsGetCentro['root'][0]['centroDistribucion']."' ".
                                    "AND cc.concepto = '7025' ".
                                    "AND gt.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."'".
                                    "AND gt.folio = '".$folioTMP."' limit 1";

            $rsCmpOperador = fn_ejecuta_query($sqlCmpOperador);

        //  echo json_encode($rsCmpOperador);
        //  echo $sqlCmpOperador;

            $sqlDieselExt = "SELECT concepto, SUM(importe) as impDieselExt ".
                "FROM trgastosviajetractortbl ".
                "WHERE idViajeTractor =  ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                "AND claveMovimiento = 'GD' ".
                "AND concepto = 2314;";

        $rsSqlDieselExt = fn_ejecuta_query($sqlDieselExt);

                $sqlGetFolio = "SELECT CONCAT(date_format(NOW(), '%m'),LPAD(CONCAT('0',MAX(folio)),4,'0')) as folio, SUBSTR(folio,1,2) as mesFolio, SUBSTR(now(),6,2) as mesAnio, SUBSTR(now(),1,4) as anioAnio, LPAD(CONCAT('0',MAX(folio) + 1),4,'0') as foleadora ".
                        "FROM trfoliostbl ".
                        "WHERE compania = 'TR' ".
                        "AND tipoDocumento = 'PZ' ".
                        "AND centroDistribucion = '".$_SESSION['usuCompania']."' ";

        $rsGetFolio = fn_ejecuta_query($sqlGetFolio);


        $diExtra =  $rsSqlDieselExt['root'][0]['impDieselExt'];

        // ---------------------- CONTAR TALONES
            $sqlNumTalones = "SELECT COUNT(t1.folio) numfolio, ".
                            "CONCAT((SELECT t2.folio FROM trtalonesviajestbl t2 WHERE t2.idViajeTractor = t1.idViajeTractor AND t2.tipoTalon IN ('TE','TI') ORDER BY t2.folio ASC LIMIT 1),' AL ', ".
                            "(SELECT t2.folio FROM trtalonesviajestbl t2 WHERE t2.idViajeTractor = t1.idViajeTractor AND t2.tipoTalon IN ('TN','TE') ORDER BY t2.folio ASC LIMIT 1)) AS desCTalon ".
                            "FROM trtalonesviajestbl t1 ".
                            "WHERE t1.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                            "AND t1.claveMovimiento != 'TX';";                                         

            $rsNumTalones = fn_ejecuta_query($sqlNumTalones);

            if($rsNumTalones['root'][0]['numfolio'] <= '7'){
                $numTalones = $data['foliosTalonesViaje'];
            }else{
                $numTalones = $rsNumTalones['root'][0]['desCTalon'];
            }

        //--------------------------------------        

        $pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->AddPage();
        $pdf->SetFont($font,'',9);
        $pdf->SetAutoPageBreak(false);

        $fechaCompleta = date_create($extras['fechaEvento']);
        $fecha = date_format($fechaCompleta, "d/m/Y");
        $hora = date_format($fechaCompleta, "H:i:s");

        $linea = "============================================================================================================";

        //HEADER
        $pdf->SetY(10+$offsetY);
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TRANSDRIZA S.A. DE C.V.", $border, 1, 'C');
        $pdf->SetX(73+$offsetX);
        $pdf->Cell(70,3,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TOLUCA EDO. DE MEXICO", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"R.F.C. TRA-891031-SXA", $border, 0, 'C');

                //$folioAumentado = $_REQUEST['trViajesTractoresFolioPolizaHdn'] + 1;

                $folioAumentado = $folioTMP;

                //echo "ESTE ES EL MES AFECTACION".$rsGetFolio['root'][0]['mesAnio'];
        if($rsGetFolio['root'][0]['mesAnio'] != date("m")){
            //echo "esta entrando AQUI";
                $pdf->SetY(8+$offsetY);
                $pdf->SetX(175+$offsetX);
                $pdf->Cell(33,4,str_pad("COMPLEMENTO", 7, " "), $border, 1, 'L');

                $sacarMes = date("m") -1;
                if($sacarMes == '0'){
                    $sacarMes = 12;
                    $sacarAnio = date("Y") -1;

                    $fConsulta = cal_days_in_month(CAL_GREGORIAN, $sacarMes, $sacarAnio);

                    $sqlGetFolio = "SELECT MAX(folio) + 1 as folio ".
                                "FROM trgastosviajetractortbl ".
                                "WHERE fechaEvento BETWEEN CAST('".$sacarAnio.'-'.$sacarMes.'-'.'01'."' AS DATE) AND CAST(now() AS DATE) ".
                                "AND claveMovimiento = 'GP';";

                    $rsSqlGetFolio = fn_ejecuta_query($sqlGetFolio);

                    $pdf->SetY(78+$offsetY);
                    $pdf->SetX(110+$offsetX);
                    $pdf->Cell(30, 4, "DIESEL EXTRA: ".str_pad($sumaDieselExtra, 6, ' ', STR_PAD_LEFT), $border, 1, 'L');

                    $pdf->SetY(235+$offsetY);
                    $pdf->SetX(115+$offsetX);
                    $pdf->Cell(100,4,
                            "700.01402.037 EDEN-R DIESEL ".
                            str_pad(number_format($diExtra, 2, '.',','),12," ",STR_PAD_LEFT),
                            $border, 0, 'L');
                }
            $pdf->SetX(175+$offsetX);
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") .$folioTMP, $border, 1, 'L');
        }else{
            $pdf->SetX(175+$offsetX);
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioTMP, $border, 1, 'L');

        }
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") . substr($_REQUEST['trViajesTractoresMesAfectacionHdn'], 0, 3), $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, $border, 1, 'L');

        //DATOS
        $reimpresion = "";

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $reimpresion = " (REIMPRESION)";
        }

        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(78+$offsetX);
        $pdf->Cell(60,4,"POLIZA DE LIQUIDACION DE GASTOS".$reimpresion, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DIST DESTINO HEADERS
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(130+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 0, 'C');
        $pdf->SetX(160+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(180+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DATOS GENERALES DEL VIAJE
        $pdf->SetX(5+$offsetX);        
        //$pdf->Cell(100,4,"Talones  : ".$data['foliosTalonesViaje'], $border, 1, 'L');
        $pdf->Cell(100,4,"Talones  : ".$numTalones, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"No Autos : ".$data['numeroUnidades'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(40,4,"Unidad   : ".$data['tractor']." (V-".str_pad($data['viaje'], 6, '0',STR_PAD_LEFT).")", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Rend.    : ".$data['rendimiento'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Cve. Ope.: ".$data['claveChofer'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Kms/Viaje: ".$data['kilometrosComprobados'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);

        $litrosViaje = floatval($data['kilometrosComprobados']) / floatval($data['rendimiento']);
        $ltrComplemento = $litrosViaje +$sumaDieselExtra;
        echo($sumaDieselExtra);
        $litrosViaje = ceil($ltrComplemento);

        $pdf->Cell(30,4,"Lts/Apr  : ".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Lts/Rec  : ".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30, 4,"Lts Menos --->     0", $border, 1, 'L');

        //DIST DESTINO DATOS
        $maxTalones = 10;
        $xColumn = 110;

        $pdf->SetY(55+$offsetY);

        for ($i=0; $i < $maxTalones; $i++) {
            $pdf->SetX($xColumn+$offsetX);
            if($i+1 <= sizeof($destinos)){
                $pdf->Cell(20,4,$destinos[$i]['distribuidor'], $border, 0, 'C');
                //EL ICONV SE USA PARA QUE IMPRIMA BIEN LOS ACENTOS
                $pdf->Cell(30,4,toUTF8($destinos[$i]['plaza']), $border, 1, 'C');
            } else {
                $pdf->Cell(20,4,"--------", $border, 0, 'C');
                $pdf->Cell(30,4,"--------------", $border, 1, 'C');
            }

            if(floatval($maxTalones) / floatval($i+1) == floatval(2)){
                $xColumn = 160;
                $pdf->SetY(55+$offsetY);
            }
        }

        //CARGOS
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"C A R G O S", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"===========", $border, 1, 'L');

        $total = 0.00;
        $iva = 0.00;
        foreach ($gastos as $gasto) {


            if(!in_array($gasto['concepto'],array('7013', '7014'))){

                $cuentaContable = preg_replace("/(\*+)/",sprintf('%04d',$data['tractor']), $gasto['cuentaContable'], 1);

                $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $cuentaContable, 1);


                $pdf->SetX(5+$offsetX);
                $pdf->Cell(100,4,
                    str_pad($cuentaContable,24," ")." ".
                    substr($gasto['nombre'], 0, 11)." ".
                    str_pad(number_format($gasto['subtotal'],2,'.',','), 9+(11-strlen(substr($gasto['nombre'], 0, 11))), " ", STR_PAD_LEFT),
                    $border, 1, 'L');

                $total += floatval($gasto['subtotal']);
                $iva += floatval($gasto['iva']);
            }
        }

        $pdf->SetX(75+$offsetX);
        $pdf->Cell(30,4,"==========", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(45,4,"Total de comp. gastos. ", $border, 0, 'L');
        $pdf->SetX(75.5+$offsetX);
        $pdf->Cell(29.5,4,str_pad(number_format($total, 2, '.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetY(185+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaIva'], 24, " ")." ".
            "IVA ACREDIT ".
            str_pad(number_format($iva,2,'.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');


        //OBSERVACIONES
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(140+$offsetX);
        $pdf->Cell(30,4,"OBSERVACIONES", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"==================================", $border, 1, 'C');

        $pdf->SetX(125+$offsetX);
        $pdf->MultiCell(55, 4, $extras['observacionGastos'], $border, 'L');

        //ANTICIPOS
        $pdf->SetY(170+$offsetY);
        $pdf->SetX(138+$offsetX);
        $pdf->Cell(30,4,"ABONOS", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"================================", $border, 1, 'C');
        $pdf->SetX(132+$offsetX);
        $pdf->Cell(40,4,"Anticipo de Gastos", $border, 1, 'C');
        //LA SIGUIENTE CELDA VACIA ES SOLO PARA SALTAR EXACTAMENTE UNA LINEA
        $pdf->Cell(30,4,"", $border, 1, 'C');

        $totalAnticipo = 0.00;
        $importe = '0.00';
        $foliosAnticipos = "";

        // MONTO DE ANTICIPOS COMPROBADOS

        $sqlAntComprobacion = "SELECT sum(importe) as importe, ".
                                "(SELECT sum(g2.importe) FROM trgastosviajetractortbl g2 WHERE g2.idViajeTractor = g1.idViajeTractor AND  g2.claveMovimiento = 'GP' AND g2.concepto IN (2315,2333,2342,6002,6009,6010,6011,6012,7002,7003,7004,7005,7006,7017,7018,7019,7020)) as sumaComprobacion ".
                                "FROM trgastosviajetractortbl g1 ".
                                "WHERE idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                                                                "AND g1.claveMovimiento != 'GX' ".
                                "AND g1.concepto = '7001';";

        $rsComprobados = fn_ejecuta_query($sqlAntComprobacion);

        if($rsComprobados['root'][0]['importe'] < $rsComprobados['root'][0]['sumaComprobacion']){
            foreach ($anticipos as $anticipo) {
                $importe = floatval($anticipo['importe']);

                //echo json_encode($anticipo);

                $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $anticipo['cuentaContable'], 1);
                $cuentaContable = preg_replace("/(\*+)/", $data['tractor'], $cuentaContable, 1);


                $pdf->SetX(115+$offsetX);
                $pdf->Cell(80,4,
                    str_pad($cuentaContable,18," ")." ".
                    substr($anticipo['plaza'], 0, 10)." ".
                    str_pad(number_format($importe,2,'.',','), 10+(10-strlen(substr($anticipo['plaza'], 0, 10))), " ", STR_PAD_LEFT),
                    $border, 1, 'L');


                /*$pdf->SetX(115+$offsetX);
                $pdf->Cell(80,4,
                    str_pad($cuentaContable,18," ")." ".
                    substr($anticipo['plaza'], 0, 10)." ".
                    str_pad(number_format($rsComprobados['root'][0]['importe'],2,'.',','), 10+(10-strlen(substr($anticipo['plaza'], 0, 10))), " ", STR_PAD_LEFT),
                    $border, 1, 'L');*/

                $totalAnticipo += floatval($anticipo['importe']);
                $foliosAnticipos = $anticipo['folio'];

                //echo(" MIRA_01".$totalAnticipo);
                //echo(" MIRA_02".$foliosAnticipos);
            }
        }else{
            foreach ($anticipos as $anticipo) {
                if($anticipo['importe'] == ""){
                    $importe = '0.00';
                }else{
                    $importe = $anticipo['importe'];
                }

                $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $anticipo['cuentaContable'], 1);
                $cuentaContable = preg_replace("/(\*+)/", $data['tractor'], $cuentaContable, 1);

                $pdf->SetX(115+$offsetX);
                $pdf->Cell(80,4,
                str_pad($cuentaContable,18," ")." ".
                substr($anticipo['plaza'], 0, 10)." ".
                str_pad(number_format($importe,2,'.',','), 10+(10-strlen(substr($anticipo['plaza'], 0, 10))), " ", STR_PAD_LEFT),
                $border, 1, 'L');

                $totalAnticipo += floatval($anticipo['importe']);
                $foliosAnticipos = $anticipo['folio'];

            }
        }


                $pdf->SetY(215+$offsetY);
                $pdf->SetX(05+$offsetX);
        $pdf->Cell(80,4,"201.01401.070.0001       OPERADORES ".str_pad(number_format($rsCmpOperador['root'][0]['importe'],2,'.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');

                $concOperadores = $rsCmpOperador['root'][0]['importe'];
                $pdf->SetY(220+$offsetY);
                $pdf->SetX(115+$offsetX);
        if($rsComprobados['root'][0]['importe'] < $rsComprobados['root'][0]['sumaComprobacion']  ){
            //$totalAnticipo = '0.00';
            //$concOperadores = '0.00';

            $pdf->Cell(80,4,"TOTAL ANTIC. EN PATIOS =====> ".str_pad(number_format($totalAnticipo+$concOperadores,2,'.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');
        }else{
            $pdf->Cell(80,4,"TOTAL ANTIC. EN PATIOS =====> ".str_pad(number_format($totalAnticipo+$concOperadores,2,'.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');
        }


        //CONCEPTOS DE DIESEL EXTRA QUE NO SE SUMAN PARA LO DE DIESEL EXTRA
        $conceptosNoSuman = array('9000');
        $sumaDieselExtra = 0;

        foreach ($diesel as $d) {
            if(!in_array($d['concepto'], $conceptosNoSuman)){
                $sumaDieselExtra += floatval($d['importe']);
            }
        }

        /*$pdf->SetY(78+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "DIESEL EXTRA: ".str_pad($sumaDieselExtra, 6, ' ', STR_PAD_LEFT), $border, 1, 'L');*/
        $pdf->SetY(85+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "FOLIOS ANT: ".$foliosAnticipos, $border, 1, 'L');

        //CARGO OPERADOR, PAGO EFECTIVO, COMPROBADO
        $cargosOperador = 0.00;
        $pagoEfectivo = 0.00;


        if($extras['importeCargoOperador'] != ""){
            $cargosOperador = floatval($extras['importeCargoOperador']);
        }else  if($extras['importePagoEfectivo'] != ""){
            $pagoEfectivo = floatval($extras['importePagoEfectivo']);
        }

        $pdf->SetY(225+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaCargoOperador'], 24, " ")." ".
            "CARGO OPER ".
            str_pad(number_format($cargosOperador, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

                $pdf->SetY(225+$offsetY);
                $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,
            str_pad($extras['cuentaPagoEfectivo'], 18, " ")." ".
            " BANCOS (SA".
            str_pad(number_format($pagoEfectivo, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 1, 'L');

        $comprobado = $total + $iva;
                $pdf->SetY(228+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad("COMPROBADO", 25+10, " ", STR_PAD_LEFT)." ".
            str_pad(number_format($comprobado, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

        //TOTALES
        $pdf->SetY(232+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            "TOTAL  CARGOS ".
            str_pad(number_format($comprobado + $cargosOperador, 2, '.',','),32," ",STR_PAD_LEFT),
            $border, 0, 'L');

                //totsl diesel cargo
        /*$pdf->SetY(235+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            "700.01402.037 EDEN-R DIESEL ".
            str_pad(number_format($diExtra, 2, '.',','),18," ",STR_PAD_LEFT),
            $border, 0, 'L');*/
                //total diesel abono
                /*$pdf->SetY(235+$offsetY);
                $pdf->SetX(115+$offsetX);
                $pdf->Cell(100,4,
                        "700.01402.037 EDEN-R DIESEL ".
                        str_pad(number_format($diExtra, 2, '.',','),12," ",STR_PAD_LEFT),
                        $border, 0, 'L');*/
                
                if($rsComprobados['root'][0]['importe'] < $rsComprobados['root'][0]['sumaComprobacion']  ){
                    $pdf->SetY(232+$offsetY);
                    $pdf->SetX(115+$offsetX);
            $pdf->Cell(80,4,
                "TOTAL  ABONOS ".
                //str_pad(number_format($totalAnticipo + $pagoEfectivo+$concOperadores, 2, '.',','),26," ",STR_PAD_LEFT),               
                str_pad(number_format($totalAnticipo+$concOperadores+$pagoEfectivo, 2, '.',','),26," ",STR_PAD_LEFT),
                $border, 1, 'L');
                }else{                    
                    $pdf->SetY(231+$offsetY);
                    $pdf->SetX(115+$offsetX);
                    $pdf->Cell(80,4,
                            "TOTAL  ABONOS ".
                            //str_pad(number_format($totalAnticipo + $pagoEfectivo+$concOperadores, 2, '.',','),26," ",STR_PAD_LEFT),}
                            //$totalAnticipo+$concOperadores
                            //echo "esto es el total de anticipos".$rsComprobados['root'][0]['importe'];
                            str_pad(number_format($totalAnticipo + $pagoEfectivo+$concOperadores, 2, '.',','),26," ",STR_PAD_LEFT),
                            $border, 1, 'L');
                }


        //REIMPRESION
                $pdf->SetY(237+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $pdf->SetX(92+$offsetX);
            $pdf->Cell(40,4,"R E I M P R E S I O N", $border, 1, 'C');
        }

        //FIRMAS
        $pdf->SetY(260+$offsetY);
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 1, 'C');

        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"E L A B O R O",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"FIRMA DEL OPERADOR",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"R E V I S O",$border, 1, 'C');
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,$usuarioElaboracion['root'][0]['nombre'],$border, 0, 'C');
        $pdf->SetX(5+$offsetX);
        //$pdf->Cell(65,4,toUTF8($_SESSION['nombreUsr']),$border, 0, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,
            toUTF8($data['nombre']." ".$data['apellidoPaterno']." ".$data['apellidoMaterno']),
            $border, 1, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,"CORC-004                     P.D. No ".$folioTMP,$border, 0, 'L');


        return $pdf;
    }

?>
