<?php
	session_start();
	$_SESSION['modulo'] = "alLocalizacionPatios";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
	
    $_REQUEST = trasformUppercase($_REQUEST);

	switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['catColoresSistemaActionHdn']){
    	case 'getColoresSistema':
    		getColoresSistema();
    		break;
    	case 'addColorSistema':
    		addColorSistema();
    		break;
    	case 'updColorSistema':
    		updColorSistema();
    		break;
    	default:
            echo '';
    }

    function getColoresSistema(){
    	$lsWhereStr = '';
    	if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catColoresSistemaClaveColorTxt'], "claveColor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catColoresSistemaColorTxt'], "color", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catColoresSistemaDescripcionTxt'], "descripcion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlColorSistema = "SELECT cs.* FROM caColoresSistemaTbl cs ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlColorSistema);

        for($i = 0; $i < count($rs['root']); $i++){
        	$rs['root'][$i]['descColor'] = $rs['root'][$i]['claveColor']." - ".$rs['root'][$i]['color'];
        }

        echo json_encode($rs);

    }

    function addColorSistema(){
    	$a['success'] = true;
    	$e =  array();

    	if($_REQUEST['catColoresSistemaClaveColorTxt'] == ''){
    		$a['success'] = false;
    		$e[] = array('id' => 'catColoresSistemaClaveColorTxt', 'msg'=>getRequerido());
    	}
    	if($_REQUEST['catColoresSistemaColorTxt'] == ''){
    		$a['success'] = false;
    		$e[] = array('id' => 'catColoresSistemaColorTxt', 'msg'=>getRequerido());
    	}
    	if($_REQUEST['catColoresSistemaDescripcionTxt'] == ''){
    		$a['success'] = false;
    		$e[] = array('id' => 'catColoresSistemaDescripcionTxt', 'msg'=>getRequerido());
    	}
    	
    	if($a['success'] == true){

			if($_REQUEST['catColoresSistemaColorHexTxt'] != ""){
				$colorClassStr =  "{\"background-image\": \"none\", ".
								  "\"background-color\": \"#".$_REQUEST['catColoresSistemaColorHexTxt']."\"}";
			}					

    		$sqlAddColor =  "INSERT INTO caColoresSistemaTbl(claveColor, color, descripcion, hex, cls) VALUES('".
    						$_REQUEST['catColoresSistemaClaveColorTxt']."', '".$_REQUEST['catColoresSistemaColorTxt']."', '".
    					    $_REQUEST['catColoresSistemaDescripcionTxt']."', ".
    					    replaceEmptyNull("'".$_REQUEST['catColoresSistemaColorHexTxt']."'").", ".
    					    replaceEmptyNull("'".$colorClassStr."'").");";

			
			$rs = fn_ejecuta_Add($sqlAddColor);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
				$a['sql'] = $sqlAddColor;
				$a['successMessage'] =  getSistemaColorSuccessMsg();
			}else{
				$a['success'] = false;
				$a['errorMessage'] = mysqlErrorMessages(mysql_errno());
			}			
 		}
    	$a['errors'] = $e;
		echo json_encode($a);
    }

	function updColorSistema(){
		$a['success'] = true;
    	$e =  array();

    	if($_REQUEST['catColoresSistemaClaveColorTxt'] == ''){
    		$a['success'] = false;
    		$e[] = array('id' => 'catColoresSistemaClaveColorTxt', 'msg'=>getRequerido());
    	}
    	if($_REQUEST['catColoresSistemaColorTxt'] == ''){
    		$a['success'] = false;
    		$e[] = array('id' => 'catColoresSistemaColorTxt', 'msg'=>getRequerido());
    	}
    	if($_REQUEST['catColoresSistemaDescripcionTxt'] == ''){
    		$a['success'] = false;
    		$e[] = array('id' => 'catColoresSistemaDescripcionTxt', 'msg'=>getRequerido());
    	}

    	if($a['success'] == true){

    		if($_REQUEST['catColoresSistemaColorHexTxt'] != ""){
				$colorClassStr =  "{\"background-image\": \"none\", ".
								  "\"background-color\": \"#".$_REQUEST['catColoresSistemaColorHexTxt']."\"}";
			}

    		$sqlUpdColor =  "UPDATE caColoresSistemaTbl SET color = '".$_REQUEST['catColoresSistemaColorTxt']."', ".
    						"descripcion = '".$_REQUEST['catColoresSistemaDescripcionTxt']."', ".
    						"cls = ".replaceEmptyNull("'".$colorClassStr."'").", ".
    						"hex = ".replaceEmptyNull("'".$_REQUEST['catColoresSistemaColorHexTxt']."'").
    						" WHERE claveColor = '".$_REQUEST['catColoresSistemaClaveColorTxt']."'";

    		$rs = fn_ejecuta_Upd($sqlUpdColor);

    		if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
				$a['sql'] = $sqlUpdColor;
				$a['successMessage'] =  getSistemaColorUpdateMsg();
			}else{
				$a['success'] = false;
				$a['errorMessage'] = mysqlErrorMessages(mysql_errno());
			}			
    	}
    	$a['errors'] = $e;
		echo json_encode($a);
    }


?>