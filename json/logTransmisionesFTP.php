<?php
  session_start();
	
	require_once($_SERVER['argv'][6]."mail/class.phpmailer.php");
	include_once($_SERVER['argv'][6]."generales.php");
	include_once($_SERVER['argv'][6]."construct.php");
			
	//var_dump($_SERVER['argv']);
	//$_SERVER['argv'][0]>Nombre del PHP
	//$_SERVER['argv'][1]>Nombre del archivo a transmitir
	//$_SERVER['argv'][2]>Nombre del archivo de error
	//$_SERVER['argv'][3]>Email a donde se notificar� el error
	//$_SERVER['argv'][4]>Acci�n a Realizar
	//$_SERVER['argv'][5]>Repositorio de los archivos
	//$_SERVER['argv'][6]>Directorio de funciones de PHP
	//$_SERVER['argv'][7]>Directorio Local donde estan los archivos a transmitir
	//$_SERVER['argv'][8 ...]>Mensaje de error

  switch($_SERVER['argv'][4]){
  		case 'enviaMail':
  				enviaMail();
  				break;
  		case 'actualiza':
  				actualiza();
  				break;  		
      default:
          echo '';
  }
  
  function enviaMail(){			
			$arrFile = file($_SERVER['argv'][1]);	
			$files = array();		

    	$contF = 0;
    	foreach($arrFile as $row)
			{
					$arch = $row;
					$arch = str_replace("\r\n", "", $arch);
					$arch = strtoupper($arch);
					$archTabla = str_replace(".TXT", "", $arch);
					//var_dump($archTabla);
					
	        $sql = "UPDATE alTransaccionUnidadTbl".
	        			 " SET hora = RPAD(hora, 50, ' ')".
	        			 " WHERE prodStatus = '".$archTabla."'";
					//echo "$sql<br>";
					fn_ejecuta_query($sql);		        
	        
	        $sql = "UPDATE alTransaccionUnidadTbl".
	        			 " SET hora = INSERT(hora, 50, 1, '0')".
	        			 " WHERE prodStatus = '".$archTabla."'";
					//echo "$sql<br>";
					fn_ejecuta_query($sql);	
					$files[] = $archTabla;
					$contF++;
			}
			$subject = ($contF > 1)?'Error en la transmision de archivos':utf8_decode('Error en la transmision del archivo: '.$files[0].".txt");
			//var_dump($files);
			
			$errorMsg = '';
			$cont = 0;
			foreach($_SERVER['argv'] as $row)
			{
					if($cont > 10)
					{
							$errorMsg .= $row." ";
					}
					$cont++;
			}
			$errorMsg = rtrim($errorMsg).".";
			if(substr($errorMsg,0,14) == "Error de conex")
			{
					$errorMsg = "Error de conexi�n.";
			}
			$errorMsg = utf8_decode('Motivo de error: <br>').$errorMsg;
			$errorMsg .= utf8_decode('</b><br><br>Favor de no responder este correo porque se genera en automatico.').
									'<br>Saludos.<br><br>Grupo Tracomex<br>Lago Nargis #34 Piso 5<br>CDMX 11520<br>';
			
			//var_dump($errorMsg);						
			$email = $_SERVER['argv'][3];
			//var_dump($email);

			//Se configura el SMTP:
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPAuth = true;
			
			//Se configura el destinatario, subject y el cuerpo del mensaje
			$mail->AddAddress($email); 																																// Direcci�n de env�o
			$mail->IsHTML(true); 																																			// El correo se env�a como HTML			
			$mail->Subject = $subject; 																																// T�tulo del email
			$mail->Body = '<b>'.$errorMsg.'</b>'; 																										// Cuerpo del Mensaje a enviar
			foreach($files as $row)
			{      
      		$archivo = $_SERVER['argv'][5].$row.".txt";      		
      		$nombreArch = $row.".txt";
      		$mail->AddAttachment($archivo, $nombreArch);																					// Se adjunta el archivo que no se transmiti�
    	}
			
			//Se env�a el email
			if($contF > 0)
			{
					//$envia = $mail->Send(); 																															// Se Env�a el correo s�lo cuando hay archivos con error en transmisi�n
			}
  }  
  
	#Actualiza a estatus 1 (Env�o OK) el archivo de la transmisi�n
	function actualiza()
	{
			$arrFile = file($_SERVER['argv'][1]);	

    	foreach($arrFile as $row)
			{
					$arch = $row;					
					$arch = str_replace("\r\n", "", $arch);
					$arch = strtoupper($arch);
					$archTabla = str_replace(".TXT", "", $arch);
					$archTabla = str_replace(".FAL", "", $archTabla);
					$archFte = $_SERVER['argv'][7]."\\".$arch;
					$archResp = $_SERVER['argv'][7]."\\respaldo\\".$arch;
					//var_dump($archTabla);
					
	        $sql = "UPDATE alTransaccionUnidadTbl".
	        			 " SET hora = RPAD(hora, 50, ' ')".
	        			 " WHERE prodStatus = '".$archTabla."'";
					//echo "$sql<br>";
					fn_ejecuta_query($sql);		        
	        
	        $sql = "UPDATE alTransaccionUnidadTbl".
	        			 " SET hora = INSERT(hora, 50, 1, '1')".
	        			 " WHERE prodStatus = '".$archTabla."'";
					//echo "$sql<br>";
					fn_ejecuta_query($sql);	

					var_dump($archFte,$archResp);
					//Mueve el archivo al directorio de respaldo
					rename($archFte, $archResp);
			}
	}
?>