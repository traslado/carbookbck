<?php

	require_once("../funciones/generales.php");
	//require_once("../funciones/construct.php");
   // require_once("../funciones/utilidades.php");


	switch($_REQUEST['actividadesPmpHdn'])
	{
		case 'insertActividadesPmp':
			insertActividadesPmp();
		break;
		case 'updActividades':
			updActividades();
		break;
		case 'insertNivelesBateria':
			insertNivelesBateria();
		break;
		case 'insertPresionLlantas':
			insertPresionLlantas();
		break;
		case 'updProdStatus':
			updProdStatus();
		break;
		case 'getActividadesPmp':
			getActividadesPmp();
		break;
		case 'validaExistaVin':
			validaExistaVin();
		break;
		case 'validaVin01':
			validaVin01();
		break;
		default:
			echo '';
		break;
	}


	 function insertActividadesPmp($vin,$usuario)
	 {
	 	//$vin = $_REQUEST['vin'];

		$SqlRegistros=" SELECT ud.centroDistribucion, ud.vin, ca.actividad, ge.nombre as frecuencia, ud.fechaEvento, 'PE' as estatus, ca.importe, '".$usuario."' as usuario ".
						"FROM alultimodetalletbl ud, cageneralestbl ge, caactividadespmptbl ca ".
						"WHERE ud.vin='".$vin."' ".
						"AND ge.valor = ca.actividad ".
						"AND ge.tabla = 'alactividadespmptbl'";

	    $rstRegistros= fn_ejecuta_query($SqlRegistros);
	
	       
	   for ($i=0; $i<sizeof($rstRegistros['root']); $i++) 
	       { 

				$fecha = strtotime("+".$rstRegistros['root'][$i]['frecuencia']." days", strtotime( $rstRegistros['root'][$i]['fechaEvento'] ) );
				$nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );				


				$insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
								 	 " VALUES ('".$rstRegistros['root'][$i]['centroDistribucion'].
									 "', '".$rstRegistros['root'][$i]['vin'].
									 "', '".$rstRegistros['root'][$i]['actividad'].
									 "', '".$rstRegistros['root'][$i]['frecuencia'].
									 "', '".$nuevafecha.
									  "', '".$rstRegistros['root'][$i]['estatus'].
									 "', '".$rstRegistros['root'][$i]['importe'].
									 "', '".$rstRegistros['root'][$i]['usuario']."')";

				$rstInsert = fn_ejecuta_query($insActividadesPmp);				

	       }


		$SqlRegistros1=" SELECT ud.centroDistribucion, ud.vin, ca.actividad, ca.frecuencia, ud.fechaEvento, 'PE' as estatus, ca.importe, '".$usuario."' as usuario ".
						"FROM alultimodetalletbl ud, caactividadespmptbl ca ".
						"WHERE ud.vin='".$vin."' ".
						"AND  ca.frecuencia != '' ".
						//"AND ca.actividad != '18' ".
						"AND ca.actividad not in (select valor from cageneralestbl where tabla='alactividadespmptbl')";

		$rstRegistros1= fn_ejecuta_query($SqlRegistros1);

		for ($i=0; $i<sizeof($rstRegistros1['root']) ; $i++) 
		{ 

			$fecha1 = strtotime("+".$rstRegistros1['root'][$i]['frecuencia']." days", strtotime( $rstRegistros1['root'][$i]['fechaEvento'] ) );
			$nuevafecha1 = date ( 'Y-m-d H:i:s' , $fecha1 );

			$insActividades1Pmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, "."fechaEvento,claveEstatus, importe, usuario) ".
							 	"VALUES ('".$rstRegistros1['root'][$i]['centroDistribucion'].
								 "', '".$rstRegistros1['root'][$i]['vin'].
								 "', '".$rstRegistros1['root'][$i]['actividad'].
								 "', '".$rstRegistros1['root'][$i]['frecuencia'].
								 "', '".$nuevafecha1.
								 "', '".$rstRegistros1['root'][$i]['estatus'].
								 "', '".$rstRegistros1['root'][$i]['importe'].
								 "', '".$rstRegistros1['root'][$i]['usuario']."') ";

			fn_ejecuta_query($insActividades1Pmp);

	    }

	}

	function updActividades(){

		    $sqlGeneral="SELECT * from alactividadespmptbl ".
						"where  vin ='".$_REQUEST['scanComoVinTxt']."' ".
						"AND centroDistribucion ='CMDAT' ".
						"AND claveEstatus='PE' ".
						"AND actividad = '".substr($_REQUEST['pmpActividadCmb'],0,2)."' ";
		    $rstGeneral=fn_ejecuta_query($sqlGeneral);

		    $sqlUpdActividadesPmp= "UPDATE alactividadespmptbl SET fechaEstatus = now(), claveEstatus='AP' ".
									"WHERE vin='".$rstGeneral['root'][0]['vin']."' ".
									"AND centroDistribucion ='".$rstGeneral['root'][0]['centroDistribucion']."' ".
									"AND claveEstatus='".$rstGeneral['root'][0]['claveEstatus']."' ".
									"AND actividad ='".$rstGeneral['root'][0]['actividad']."' ";
		    fn_ejecuta_query($sqlUpdActividadesPmp);

		    insertaNuevoPMP($_REQUEST['scanComoVinTxt'],substr($_REQUEST['pmpActividadCmb'],0,2));

		    echo json_encode($rstGeneral);
	}

	function insertNivelesBateria()
	{		
        $sqlBateria="SELECT * from alactividadespmptbl ".
					"where  vin ='".$_REQUEST['scanComoVinTxt']."' ".
					"AND centroDistribucion ='CMDAT' ".
					"AND claveEstatus='AP' ".
					"AND actividad = '".substr($_REQUEST['pmpActividadCmb'],0,2)."' ";
        $rstBateria=fn_ejecuta_query($sqlBateria);

        $sqlInsBateria= "INSERT INTO alnivelespmptbl (centroDistribucion,vin,actividad,fechaEvento,fechaEstatus,limiteInicial,limiteFinal) ".
		      			   "VALUES ('".$rstBateria['root'][0]['centroDistribucion'].
		      			   			"', '".$rstBateria['root'][0]['vin'].
		      			   			 "', '".$rstBateria['root'][0]['actividad'].
									 "', '".$rstBateria['root'][0]['fechaEvento'].
									 "', now() , '"
									 .$_REQUEST['nivelAltoTxt'].
									 "', '".$_REQUEST['nivelBajoTxt']."') ";
		fn_ejecuta_query($sqlInsBateria);

		echo json_encode($rstBateria);	


	}

	function insertPresionLlantas()

	{


	        $sqlLlantas="SELECT * from alactividadespmptbl ".
						"where  vin ='".$_REQUEST['scanComoVinTxt']."' ".
						"AND centroDistribucion ='CMDAT' ".
						"AND claveEstatus='AP' ".
						"AND actividad = '".substr($_REQUEST['pmpActividadCmb'],0,2)."' ";
	        $rstLlantas=fn_ejecuta_query($sqlLlantas);

	        $sqlInsLlantas= "INSERT INTO alnivelespmptbl (centroDistribucion,vin,actividad,fechaEvento,fechaEstatus,limiteInicial,limiteFinal) ".
			      			   "VALUES ('".$rstLlantas['root'][0]['centroDistribucion'].
			      			   			"', '".$rstLlantas['root'][0]['vin'].
			      			   			 "', '".$rstLlantas['root'][0]['actividad'].
										 "', '".$rstLlantas['root'][0]['fechaEvento'].
										 "', now() , '"
										 .$_REQUEST['presionLlantasTxt'].
										 "', '".$_REQUEST['presionLlantasTxt']."') ";
			fn_ejecuta_query($sqlInsLlantas);

			echo json_encode($rstLlantas);

	}

	 function getActividadesPmp(){

        $response = "";
        $fecha = date('Y-m-d');
            

         $sqlActividades = "SELECT concat(ca.actividad,' - ',ca.descripcion)  FROM alactividadespmptbl al, caactividadespmptbl ca ".
                            "WHERE vin ='".$_REQUEST['scanComoVinTxt']."'".
                            " AND claveEstatus ='PE' ".
                            "AND al.actividad = ca.actividad ".
                            "AND al.fechaEvento <='".$fecha."'".
                            " group by al.actividad";

        $rs = fn_ejecuta_query($sqlActividades);
       // echo json_encode($rs);

        if(sizeof($rs['root']) > 0){
        	//echo json_encode($rs);
          $response = createResponseCombos($rs['root']);
        }

        echo $response;
    }  

     /* function createResponseCombos($root){
        $response = "";
        global $spChar;
        $keys = array_keys($root[0]);

        foreach ($root as $row) {
            foreach ($keys as $key) {
                if($row[$key] == $spChar){
                    $response .= ";";
                } else {
                    $response .= $row[$key]."|";
                }
            }
        }

    
        return $response;
    }   */

    function validaVin01(){

        $fecha = date('Y-m-d');

        $sqlVin ="SELECT * FROM alactividadespmptbl ".
                    "WHERE centroDistribucion ='CMDAT' ".
                    "AND vin ='".$_REQUEST['scanComoVinTxt']."' ".
                    "AND claveEstatus ='PE' ".
                    "AND fechaEvento <='".$fecha."'";     
        $rstVin = fn_ejecuta_query($sqlVin);       
        //echo json_encode($rstVin);

        if ($rstVin['records'] != 0) {
            echo "1||||";
        }
        else{
            echo '0|Unidad sin actividades pendientes|';

        }
        //echo json_encode($rstVin);          
    }

    function validaExistaVin(){
        $sqlExisVin ="SELECT * FROM alunidadestbl al, alhistoricounidadestbl hu ".
                    "WHERE al.vin ='".$_REQUEST['scanComoVinTxt']."' ".
                    "AND al.vin =hu.vin ".
                    "AND hu.centroDistribucion = 'CMDAT' ".
                    "AND hu.claveMovimiento = 'IC' ";
        $rsExisVin = fn_ejecuta_query($sqlExisVin);
        //echo json_encode($sqlExisVin);

        if ($rsExisVin['records'] != 0) {
            echo "1||||";
        }
        else{
            echo '0|Unidad no Existente|';
        }

    }

    function insertaNuevoPMP($vin,$actividad){    	


    	$sqlSel = "SELECT * FROM caactividadespmptbl ca, alactividadespmptbl al ".
					" WHERE al.vin ='".$vin."'".
					" AND ca.actividad = al.actividad".
					" AND al.actividad ='".$actividad."'";
		$rstSel = fn_ejecuta_query($sqlSel);

		//echo json_encode($rstSel);

		if ($rstSel['root'][0]['actividad'] == 14) {
			
		}
		else if ($rstSel['root'][0]['actividad'] == 16) {
				$sqlCountAct = "SELECT count(*) as conteo FROM caactividadespmptbl ca, alactividadespmptbl al ".
							"WHERE al.vin ='".$rstSel['root'][0]['vin']."' " .
							"AND ca.actividad = al.actividad ".
							"AND al.actividad ='".$rstSel['root'][0]['actividad']."'";
				$rsCountAct = fn_ejecuta_query($sqlCountAct);
				echo json_encode($rsCountAct);

				if ($rsCountAct['root'][0]['conteo'] < '2') {
					$sqlCatAct = "SELECT * FROM caactividadespmptbl ".
									"WHERE actividad = '".$rstSel['root'][0]['actividad']."'";
				$rsCatAct = fn_ejecuta_query($sqlCatAct);
				//echo json_encode($rsCatAct);

				$fecha = strtotime("+".$rsCatAct['root'][0]['frecuencia']." days", strtotime( $rstSel['root'][0]['fechaEvento'] ) );
				$nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );		


				$insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
								 " VALUES ('".$rstSel['root'][0]['centroDistribucion'].
									 "', '".$rstSel['root'][0]['vin'].
									 "', '".$rstSel['root'][0]['actividad'].
									 "', '".$rstSel['root'][0]['frecuencia'].
									 "', '".$nuevafecha.
									  "', 'PE".
									 "', '".$rstSel['root'][0]['importe'].
									 "', '".$rstSel['root'][0]['usuario']."')";

				$rstInsert = fn_ejecuta_query($insActividadesPmp);	
				}
				else {
					
				}

										

			
		}else{
			switch ($rstSel['root'][0]['tipo']) {
			
				case '1':
					$fecha = strtotime("+".$rstSel['root'][0]['frecuencia']." days", strtotime( $rstSel['root'][0]['fechaEvento'] ) );
					$nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );					


					$insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
									 " VALUES ('".$rstSel['root'][0]['centroDistribucion'].
										 "', '".$rstSel['root'][0]['vin'].
										 "', '".$rstSel['root'][0]['actividad'].
										 "', '".$rstSel['root'][0]['frecuencia'].
										 "', '".$nuevafecha.
										  "', 'PE".
										 "', '".$rstSel['root'][0]['importe'].
										 "', '".$rstSel['root'][0]['usuario']."')";

					$rstInsert = fn_ejecuta_query($insActividadesPmp);					
				break;

				case '2':			
					$sqlCount = "SELECT count(*) as conteo FROM caactividadespmptbl ca, alactividadespmptbl al ".
								"WHERE al.vin ='".$rstSel['root'][0]['vin']."' " .
								"AND ca.actividad = al.actividad ".
								"AND al.actividad ='".$rstSel['root'][0]['actividad']."'";
					$rsCount = fn_ejecuta_query($sqlCount);
					//echo json_encode($rsCount['root'][0]['conteo']);

					if ($rsCount['root'][0]['conteo'] == '1') {

						$sqlGen = "SELECT * FROM cageneralestbl ".
									"WHERE tabla ='alactividadespmptbl' ".
									"AND valor ='".$rstSel['root'][0]['actividad']."'";
						$rsGen = fn_ejecuta_query($sqlGen);
							//echo json_encode($rsGen);	

						$sqlCatAct = "SELECT * FROM caactividadespmptbl ".
										"WHERE actividad = '".$rstSel['root'][0]['actividad']."'";
						$rsCatAct = fn_ejecuta_query($sqlCatAct);
						//echo json_encode($rsCatAct);				

						$fecha = strtotime("+".$rsCatAct['root'][0]['frecuencia']." days", strtotime( $rstSel['root'][0]['fechaEvento'] ) );
						$nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );	

						$fecha1 = strtotime("-".$rsGen['root'][0]['nombre']." days", strtotime( $nuevafecha ) );
						$nuevafecha1 = date ( 'Y-m-d H:i:s' , $fecha1 );


						$insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
										 " VALUES ('".$rstSel['root'][0]['centroDistribucion'].
											 "', '".$rstSel['root'][0]['vin'].
											 "', '".$rstSel['root'][0]['actividad'].
											 "', '".$rsCatAct['root'][0]['frecuencia'].
											 "', '".$nuevafecha1.
											  "', 'PE".
											 "', '".$rstSel['root'][0]['importe'].
											 "', '".$rstSel['root'][0]['usuario']."')";								
						$rstInsert = fn_ejecuta_query($insActividadesPmp);		
					}
					else if ($rsCount['root'][0]['conteo'] > '1') {						

						$sqlCatAct = "SELECT * FROM caactividadespmptbl ".
										"WHERE actividad = '".$rstSel['root'][0]['actividad']."'";
						$rsCatAct = fn_ejecuta_query($sqlCatAct);
						//echo json_encode($rsCatAct);				

						$fecha = strtotime("+".$rsCatAct['root'][0]['frecuencia']." days", strtotime( $rstSel['root'][0]['fechaEvento'] ) );
						$nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );						


						$insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
										 " VALUES ('".$rstSel['root'][0]['centroDistribucion'].
											 "', '".$rstSel['root'][0]['vin'].
											 "', '".$rstSel['root'][0]['actividad'].
											 "', '".$rsCatAct['root'][0]['frecuencia'].
											 "', '".$nuevafecha.
											  "', 'PE".
											 "', '".$rstSel['root'][0]['importe'].
											 "', '".$rstSel['root'][0]['usuario']."')";								
						$rstInsert = fn_ejecuta_query($insActividadesPmp);		
					}					
				break;
				
				default:
					# code...
					break;
			}
		}									
    }



	function updProdStatus()
	{
	    
		$vin1= $_REQUEST['vines'];

		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);

		$cadena = chunk_split($vin, 17,"','");

		$vines = substr($cadena,0,-2);


		$sqlUpdKz = "UPDATE al660Tbl SET prodStatus = '".$_REQUEST['prodSatus']."' ".
		"WHERE vin IN ('".$vines." ) ";

		$rsSqlUpdKz = fn_ejecuta_query($sqlUpdKz);


	 }
?>