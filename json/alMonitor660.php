<?php

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
    
    switch($_REQUEST['alMonitorActionHdn']){
        case 'getConsulta660':
            getConsulta660();
            break;
        default:
            echo '';
    }
            
    function getConsulta660() {

        switch($_REQUEST['alMonitorRdoGroup']){
            case'1':
                $sqlStr = "SELECT A.vin,A.dealerid, A.model, A.colorcode  , ". 
                                                    " CONCAT(IFNULL((SELECT '' FROM caDistribuidoresCenTrosTbl  d where A.dealerid = d.distribuidorCentro), 'NO EXISTE  DISTRIBUIDOR'), '  ') AS DescripcionEstatus  ".
                                                    " FROM al660tbl A   ".
                                                    " where  (A.dealerid not IN (SELECT distribuidorCentro ".
                                                    " FROM caDistribuidoresCenTrosTbl )) ";
            break;
            case'2':
                $sqlStr = "SELECT A.vin,A.dealerid, A.model, A.colorcode  , CONCAT(IFNULL((SELECT '' FROM caSimbolosUnidadesTbl  e WHERE A.model = e.simboloUnidad), 'NO EXISTE  SIMBOLO')) AS DescripcionEstatus ". 
                                             " FROM al660tbl A  ".
                                             " where A.model NOT IN (SELECT simboloUnidad ".
                                             " FROM CASIMBOLOSUNIDADESTBL)";

            break;
            case'3':
                $sqlStr = "SELECT A.vin,A.dealerid, A.model, A.colorcode  , ".
                                     " CONCAT(IFNULL((SELECT '' FROM caDistribuidoresCenTrosTbl  d where A.dealerid = d.distribuidorCentro), 'NO EXISTE DISTRIBUIDOR,'), '  ', ".
                                    " IFNULL((SELECT '' FROM caSimbolosUnidadesTbl  e where A.model = e.simboloUnidad), 'NO EXISTE  SIMBOLO,'),'  ', ".
                                    " IFNULL((SELECT '' FROM caColorUnidadesTbl  f, casimbolosunidadestbl r where A.colorCode = f.color and f.marca = r.marca AND A.MODEL = r.simboloUnidad), 'NO EXISTE COLOR' )) AS DescripcionEstatus  ".
                                    "FROM al660tbl A ".
                                    " where  (A.dealerid not IN (SELECT distribuidorCentro FROM caDistribuidoresCenTrosTbl )) ".
                                    " OR  (A.model NOT IN (SELECT simboloUnidad FROM CASIMBOLOSUNIDADESTBL) ) ".
                                    " OR (A.colorcode NOT IN (SELECT C.COLOR FROM cacolorunidadestbl C, CASIMBOLOSUNIDADESTBL D WHERE C.MARCA = D.MARCA))"; 
            break;
            case'4':
                $sqlStr = "SELECT A.vin,A.dealerid, A.model, A.colorcode  , ".
                                     " CONCAT(IFNULL((SELECT '' FROM caDistribuidoresCenTrosTbl  d where A.dealerid = d.distribuidorCentro), 'NO EXISTE DISTRIBUIDOR,'), '  ', ".
                                    " IFNULL((SELECT '' FROM caSimbolosUnidadesTbl  e where A.model = e.simboloUnidad), 'NO EXISTE  SIMBOLO,'),'  ', ".
                                    " IFNULL((SELECT '' FROM caColorUnidadesTbl  f, casimbolosunidadestbl r where A.colorCode = f.color and f.marca = r.marca AND A.MODEL = r.simboloUnidad), 'NO EXISTE COLOR' )) AS DescripcionEstatus  ".
                                    "FROM al660tbl A ".
                                    " where  (A.dealerid not IN (SELECT distribuidorCentro FROM caDistribuidoresCenTrosTbl )) ".
                                    " OR  (A.model NOT IN (SELECT simboloUnidad FROM CASIMBOLOSUNIDADESTBL) ) ".
                                    " OR (A.colorcode NOT IN (SELECT C.COLOR FROM cacolorunidadestbl C, CASIMBOLOSUNIDADESTBL D WHERE C.MARCA = D.MARCA))"; 
        
                
            break;
            default:
                echo'';  
        } 


        $rs = fn_ejecuta_query($sqlStr);


        if($_REQUEST['start'] != '' && $_REQUEST['limit'] != ''){
            $nvoArray = array_slice($rs['root'], $_REQUEST['start'], $_REQUEST['limit']);
            $rs['pageRecords'] = count($nvoArray);
            $rs['root'] = $nvoArray;
        }                
        echo json_encode($rs);     
    }    
?>