<?php
	session_start();
	$_SESSION['modulo'] = "catClasificacionTarifa";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);
	
    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    } 

    switch($_REQUEST['catClasificacionTarifaActionHdn']){
	    case 'getClasificacionTarifa':
	    	getClasificacionTarifa();
	    	break;
	    case 'addClasificacionTarifa':
	    	addClasificacionTarifa();
        case 'getTarifasClasificacionDisponibles':
            getTarifasClasificacionDisponibles();
	    	break;
        default:
            echo '';
    }

    function getClasificacionTarifa(){

        $lsWhereStr = "WHERE ct.idTarifa = t.idTarifa";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTarifasClasificacionHdn'], "ct.clasificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTarifasMarcaHdn'], "ct.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTarifasTipoTarifaHdn'], "t.tipoTarifa", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        
        $sqlTarifaClasificacion = "SELECT * FROM catarifastbl t, caclasificaciontarifastbl ct ".
                                   $lsWhereStr;
                                   
        $rs = fn_ejecuta_query($sqlTarifaClasificacion);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['descripcion'];
        }
        
        echo json_encode($rs);

    }

    function addClasificacionTarifa(){
    	$a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catClasificacionTarifaClasificacionHdn'] == ""){
            $e[] = array('id'=>'catClasificacionTarifaClasificacionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
         if($_REQUEST['catClasificacionTarifaTarifaHdn'] == ""){
            $e[] = array('id'=>'catClasificacionTarifaTarifaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
        	$sqlAddClasificacionMarcaStr = "INSERT INTO caClasificacionTarifasTbl ".
        								   "VALUES('".$_REQUEST['catClasificacionTarifaClasificacionHdn']."'".
        								   ",".$_REQUEST['catClasificacionTarifaTarifaHdn'].")";
			
			$rs = fn_ejecuta_Add($sqlAddClasificacionMarcaStr);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlAddClasificacionMarcaStr;
                $a['successMessage'] = getClasificacionTarifaSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddClasificacionMarcaStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }
    function getTarifasClasificacionDisponibles(){
        
        $lsWhereStr = "WHERE ct.idTarifa = t.idTarifa";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTarifasClasificacionHdn'], "ct.clasificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTarifasMarcaHdn'], "ct.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        
        $sqlTarifaClasificacion = "SELECT * FROM caTarifasTbl tf WHERE tf.idTarifa NOT IN (".
                                  "(SELECT t.idTarifa FROM caTarifasTbl t, caClasificacionTarifasTbl ct ".
                                   $lsWhereStr.")) AND tf.tipoTarifa = '".$_REQUEST['catTarifasTipoTarifaHdn']."'";
                                   
        $rs = fn_ejecuta_query($sqlTarifaClasificacion);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['descripcion'];
        }
        
        echo json_encode($rs);
    }
?>