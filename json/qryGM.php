<?php
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    date_default_timezone_set('America/Mexico_City');

    $fechaInicio='2017-12-31';
    //$today = date("Y-m-d");
    $today = '2018-10-05';
    

    for($i=$fechaInicio;$i<=$today;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
    	
    	$sql="select a.vin
		     from alhistoricounidadestbl a,alunidadestbl al,cadistribuidorescentrostbl ca, casimbolosunidadestbl si
			 where a.centroDistribucion = 'LZC02'
			  and cast(a.fechaEvento as date) between cast('2017-06-20' as date) and cast('".$i."' as date)
			  and a.claveMovimiento in ('L2')
			  and a.vin=al.vin 
			  and al.distribuidor= ca.distribuidorCentro
			  and ca.tipoDistribuidor='DX'
			  and substring(a.distribuidor,1,2) not in ('13','14','48')
  			  and substring(a.distribuidor,1,1) not in ('7')			  
			  and al.simboloUnidad= si.simbolounidad
			  and si.marca='GM'
			  and not exists(select * from alhistoricounidadestbl b
			                 where b.centroDistribucion = 'LZC02'
			                   and b.vin = a.vin
			                   and cast(b.fechaEvento as date) between cast(a.fechaEvento as date) and  cast('".$i."' as date)
			                   and b.claveMovimiento in ('L3'))
			group by 1;";

		$rs=fn_ejecuta_query($sql);

		echo sizeof($rs['root'])."-";
		
    }

   
?>