<?php
	session_start();
    $_SESSION['modulo'] = "rpAdminRepuveadm";
	
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

	switch($_REQUEST['rpAdminRepuveHdn']){
			case 'muestraPresentadas':
				muestraPresentadas();
				break;
			case 'rejillaPresentadas':
				rejillaPresentadas();
				break;
			case 'generaEliminaPresentadas':
				generaEliminaPresentadas();
				break;
			case 'insertaVerificacion':
				insertaVerificacion();
				break;
			case 'rejillaVerificacion':
				rejillaVerificacion();
				break;
			case 'envioVerificacion':
				envioVerificacion();
			break;
			case 'muestraUnidadEspecial':
				muestraUnidadEspecial();
				break;
			case 'rejillaEspecial':
				rejillaEspecial();
				break;
			case 'cancelacionFolio':
				cancelacionFolio();
				break;
			case 'reemplazoFolio':
				reemplazoFolio();
				break;
			default:
				echo '';
    }				
		
	function muestraPresentadas(){
	
		$sqlAddPresentadas = "INSERT INTO alRepuveTmp  (vin, marca, modelo, color, descripcion, centroDistribucion) ".
							 "SELECT unidad.vin AS VIN, maruni.descripcion AS MARCA, clamar.descripcion AS MODELO, coluni.descripcion AS COLOR, simuni.descripcion AS DESCRIPCION, ultdet.centroDistribucion ".
							 "FROM alultimodetalletbl ultdet, casimbolosunidadestbl simuni, alunidadestbl unidad ".
							 "INNER JOIN cacolorunidadestbl coluni ON unidad.color = coluni.color, ".
							 "caclasificacionmarcatbl clamar ".
							 "INNER JOIN camarcasunidadestbl maruni ON clamar.marca = maruni.marca, ".
							 "cadistribuidorescentrostbl discen ".
							 "WHERE ultdet.centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ".
							 "AND ultdet.claveMovimiento = 'PR' ".
							 "AND unidad.vin = ultdet.vin ".
							 "AND simuni.clasificacion = clamar.clasificacion ".
							 "AND simuni.simboloUnidad = unidad.simboloUnidad ".
							 "AND simuni.simboloUnidad NOT LIKE 'DX3L04' ".
							 "AND discen.tieneRepuve = 1 ".
							 "AND simuni.tieneRepuve = 1 ".
							 "GROUP BY ultdet.vin, unidad.vin, clamar.marca, clamar.descripcion, unidad.color, unidad.descripcionUnidad";
							
		$rsPresentadas = fn_ejecuta_query($sqlAddPresentadas);
		echo json_encode($rsPresentadas);
		
		$sqlQueryPresentadas = "SELECT vin FROM alrepuvetmp WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
		$rsPresentadas = fn_ejecuta_query($sqlQueryPresentadas);
		for ($i=0; $i<sizeof($rsPresentadas['root']); $i++){
			$sqlQueryPresentadasA = "SELECT vin FROM alrepuvetbl WHERE vin = '".$rsPresentadas['root'][$i]['vin']."'";
			$rsPresentadasA = fn_ejecuta_query($sqlQueryPresentadasA);
			if($rsPresentadasA){
				$sqlDeletePresentadas =  "DELETE alRepuveTmp WHERE vin = '".$rsPresentadas['root'][$i]['vin']."'";
				$rsPresentadas = fn_ejecuta_query($sqlDeletePresentadas);
			}
		}
		
		$sqlQueryVin = "SELECT vin FROM alRepuveTmp WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
		$rsVin = fn_ejecuta_query($sqlQueryVin);		
		for ($i=0; $i<sizeof($rsVin['root']); $i++){
			$Vin_Temp = substr($rsVin['root'][$i]['vin'], 10, 1);			
			switch ($Vin_Temp){
				Case "1":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2001' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "2":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2002' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "3":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2003' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "4":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2004' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "5":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2005' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "6":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2006' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "7":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2007' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "8":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2008' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "9":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2009' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "A":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2010' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "B":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2011' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "C":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2012' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "D":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2013' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "E":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2014' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "F":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2015' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "G":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2016' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "H":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2017' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "I":
					echo "----VIN INVALIDO----";
					break;
			}
		}
	}
	
	function rejillaPresentadas(){
	
		$sqlGetPresentadas = "SELECT vin, marca, modelo, color, descripcion, anio ".
							"FROM alrepuvetmp ".
							"WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
		$rsGetUnidades = fn_ejecuta_query($sqlGetPresentadas);
		echo json_encode($rsGetUnidades);

	}
	
	function generaEliminaPresentadas(){
				
		if ($_REQUEST['rpCentrosDistHdn'] == 'CDAGS'){
			$nombreArchivo = "Recepcion_Status_A_";
				$fechaActual = date("dmY");
				$nombreCompleto = $_REQUEST['rpCentrosDistHdn'] . "_" . $nombreArchivo . $fechaActual;
				$localIP = getHostByName(php_uname('n'));
				$miArchivo = fopen("C:\\REPUVE\\AGS\\".$nombreCompleto.".txt", "w") or die("No se puede crear el archivo!");
					$sqlQuery =  "SELECT * FROM alRepuveTmp where centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rst = fn_ejecuta_query($sqlQuery); // CONSULTA 1  
					if($rst['root']>0){
						for ($i=0;$i < sizeof($rst['root']);$i++){
							$vin=$rst['root'][$i]['vin'];
							$marca=$rst['root'][$i]['marca'];
							$modelo=$rst['root'][$i]['modelo'];
							$color=$rst['root'][$i]['color'];
							$descripcion=$rst['root'][$i]['descripcion'];
							$anio=$rst['root'][$i]['anio'];
							$txt = $vin . "|" . $marca . "|" . $modelo . "|" . $anio . "|" . $color . "|" . $descripcion . "||";
							fwrite($miArchivo, $txt);
							$txt = "\r\n";
							fwrite($miArchivo, $txt);
						}
					}
				fclose($miArchivo);
						
				$sqlDelete =  "DELETE FROM alRepuveTmp WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
				$rst = fn_ejecuta_query($sqlDelete); // CONSULTA 1
		}
		
		if ($_REQUEST['rpCentrosDistHdn'] == 'CDSAL'){
			$nombreArchivo = "Recepcion_Status_A_";
				$fechaActual = date("dmY");
				$nombreCompleto = $_REQUEST['rpCentrosDistHdn'] . "_" . $nombreArchivo . $fechaActual;
				$localIP = getHostByName(php_uname('n'));
				$miArchivo = fopen("C:\\REPUVE\\SAL\\".$nombreCompleto.".txt", "w") or die("No se puede crear el archivo!");
					$sqlQuery =  "SELECT * FROM alRepuveTmp where centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rst = fn_ejecuta_query($sqlQuery); // CONSULTA 1  
					if($rst['root']>0){
						for ($i=0;$i < sizeof($rst['root']);$i++){
							$vin=$rst['root'][$i]['vin'];
							$marca=$rst['root'][$i]['marca'];
							$modelo=$rst['root'][$i]['modelo'];
							$color=$rst['root'][$i]['color'];
							$descripcion=$rst['root'][$i]['descripcion'];
							$anio=$rst['root'][$i]['anio'];
							$txt = $vin . "|" . $marca . "|" . $modelo . "|" . $anio . "|" . $color . "|" . $descripcion . "||";
							fwrite($miArchivo, $txt);
							$txt = "\r\n";
							fwrite($miArchivo, $txt);
						}
					}
				fclose($miArchivo);
						
				$sqlDelete =  "DELETE FROM alRepuveTmp WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
				$rst = fn_ejecuta_query($sqlDelete); // CONSULTA 1
		}
		
		if ($_REQUEST['rpCentrosDistHdn'] == 'CDTOL'){
			$nombreArchivo = "Recepcion_Status_A_";
				$fechaActual = date("dmY");
				$nombreCompleto = $_REQUEST['rpCentrosDistHdn'] . "_" . $nombreArchivo . $fechaActual;
				$localIP = getHostByName(php_uname('n'));
				$miArchivo = fopen("C:\\REPUVE\\TOL\\".$nombreCompleto.".txt", "w") or die("No se puede crear el archivo!");
					$sqlQuery =  "SELECT * FROM alRepuveTmp where centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rst = fn_ejecuta_query($sqlQuery); // CONSULTA 1  
					if($rst['root']>0){
						for ($i=0;$i < sizeof($rst['root']);$i++){
							$vin=$rst['root'][$i]['vin'];
							$marca=$rst['root'][$i]['marca'];
							$modelo=$rst['root'][$i]['modelo'];
							$color=$rst['root'][$i]['color'];
							$descripcion=$rst['root'][$i]['descripcion'];
							$anio=$rst['root'][$i]['anio'];
							$txt = $vin . "|" . $marca . "|" . $modelo . "|" . $anio . "|" . $color . "|" . $descripcion . "||";
							fwrite($miArchivo, $txt);
							$txt = "\r\n";
							fwrite($miArchivo, $txt);
						}
					}
				fclose($miArchivo);
						
				$sqlDelete =  "DELETE FROM alRepuveTmp WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
				$rst = fn_ejecuta_query($sqlDelete); // CONSULTA 1
		}
	
	}
	
	function insertaVerificacion(){
	
		//******Copiar el archivo en el servidor
		session_start();
		$a = array();
		$a['successTitle'] = "Manejador de Archivos";
		$centroDistribucion = $_REQUEST['rpCentrosDistHdn'];
		
		$x = 1;
		if(isset($_FILES)){
			if($_FILES["rpRepuveBrowseBrw"]["error"] > 0){
				$a['success'] = false;
				$a['message'] = $_FILES["rpRepuveBrowseBrw"]["error"];
			}else{
				$temp_file_name = $_FILES['rpRepuveBrowseBrw']['tmp_name'];
				$original_file_name = $_FILES['rpRepuveBrowseBrw']['name'];
				
				// Buscamos la extensi�n del archivo
				$ext = explode ('.', $original_file_name); 
				$ext = $ext [count ($ext) - 1]; 

				// Removemos la extensi�n para dejar el nombre original
				$file_name = str_replace ($ext, '', $original_file_name); 
	
				//Este c�digo esta bien porque lo sube al servidor
				$new_name = $_SERVER['DOCUMENT_ROOT'].'/archivosRepuve/'.'CHRREPUVE' . $centroDistribucion . $x . '.txt';
				
				//*************************************
				if (file_exists($new_name)){
					//echo 'Si existe un archivo';
					$total_archivos = count(glob($_SERVER['DOCUMENT_ROOT'].'/archivosRepuve/'.'*.txt',GLOB_BRACE));
					$x = $x + $total_archivos;
					//$srcfile=$temp_file_name;
					$dstfile=$_SERVER['DOCUMENT_ROOT'].'/archivosRepuve/'.'CHRREPUVE' . $centroDistribucion . $x . '.txt';
					move_uploaded_file($temp_file_name, $dstfile);
					//copy($srcfile, $dstfile);
				}else{
					//echo 'No existe un archivo'."<br>";
					//$srcfile=$temp_file_name;
					//$dstfile = $_SERVER['DOCUMENT_ROOT'].'/archivosRepuve/'.'CHRREPUVE' . $centroDistribucion . $x . '.txt';
					move_uploaded_file($temp_file_name, $new_name);
					//copy($srcfile, $dstfile);
				}
				//*************************************
				/*if (move_uploaded_file($temp_file_name, $dstfile)){
				//if (move_uploaded_file($temp_file_name, $new_name)){

					if (!file_exists($dstfile)){
						$a['success'] = false;
						$a['errorMessage'] = "Error al procesar el archivo " . $dstfile;
					} else {
						$a['success'] = true;
						$a['successMessage'] = "Archivo Cargado";
						$a['archivo'] = $file_name . $ext;
						//$a['root'] = leerXLS($new_name);
					}
				} else { 
					$a['success'] = false;
					$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$dstfile;
				}*/
			}
		} else {
			$a['success'] = false;
			$a['errorMessage'] = "Error FILES NOT SET";
		}
			
		echo json_encode($a);
			
		//Lectura e inserci�n del archivo
		//$filePath = $dstfile;
		$filePath = $new_name;
        $file = fopen($filePath, "r");		
        if(file($filePath)){
            while (!feof($file)){
                $lineaStr = fgets($file);
                $datosArreglo = explode("|",$lineaStr);
                $vin =  $datosArreglo[0];
                $folio = $datosArreglo[1];
				$tid = $datosArreglo[2];
				$fecha = $datosArreglo[3];                
				$addVerificacion = "INSERT INTO alRepuveVerificacionTmp ".
								   "(vin, folio, tid, fecha, centroDistribucion) ".
                                   "VALUES ( ".
                                   "'".$datosArreglo[0]."', ".
                                   "'".$datosArreglo[1]."', ".
                                   "'".$datosArreglo[2]."', ".
                                   "'".$datosArreglo[3]."', ".
                                   "'".$_REQUEST['rpCentrosDistHdn']."') ";
                fn_ejecuta_query($addVerificacion);
			}
        }
    }
	
	function rejillaVerificacion(){
		//$centroDistribucion = $_REQUEST['rpCentrosDistHdn']."";
	
		// Mostrar en la rejilla los datos de la tabla
		$sqlMuestraVerifiacion = "SELECT vin, folio, tid, fecha, centroDistribucion ".
								 "FROM alRepuveVerificacionTmp ";
								 "WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
							 
		$rs = fn_ejecuta_query($sqlMuestraVerifiacion);
		echo json_encode($rs);
	
	}
	
	function envioVerificacion(){
	
		/*---------------------------------------------------------------------------------------
		//Mandar el archivo a \\10.1.2.19\REPUVE\
		//    "\\10.1.2.19\REPUVE\CHRREPUVE" & PatioCB.Text & x & ".txt"
		$x = 1;
		$centroDistribucion = "".$_REQUEST['rpCentrosDistHdn']."";
		$filePath = $_REQUEST['rpRepuveBrowseBrw'];
		
		if (file_exists($filePath)){
			$total_archivos = count(glob('C:\\REPUVE\\VERIFICACION\\*.txt',GLOB_BRACE));
			$x = $x + $total_archivos;
			$srcfile=$filePath;
			$dstfile='C:\\REPUVE\\VERIFICACION\\CHRREPUVE' . $centroDistribucion . $x . '.txt';
			copy($srcfile, $dstfile);
		}else{
			$srcfile=$filePath;
			$dstfile='C:\\REPUVE\\VERIFICACION\\CHRREPUVE' . $centroDistribucion . $x . '.txt';
			copy($srcfile, $dstfile);
		}
		*/
		
		$centroDistribucion = "".$_REQUEST['rpCentrosDistHdn']."";
		
		$sqlDelete =  "DELETE FROM alRepuveVerificacionTmp WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
		$rst = fn_ejecuta_query($sqlDelete);
		
	}
	
	function muestraUnidadEspecial(){
	
		$sqlAddEspecial = "INSERT INTO alRepuveTmp  (vin, marca, modelo, color, descripcion, centroDistribucion) ".
						  "SELECT unidad.vin AS VIN, maruni.descripcion AS MARCA, clamar.descripcion AS MODELO, coluni.descripcion AS COLOR, simuni.descripcion AS DESCRIPCION, ultdet.centroDistribucion ".
						  "FROM alultimodetalletbl ultdet, casimbolosunidadestbl simuni, alunidadestbl unidad ".
						  "INNER JOIN cacolorunidadestbl coluni ON unidad.color = coluni.color, ".
						  "caclasificacionmarcatbl clamar ".
						  "INNER JOIN camarcasunidadestbl maruni ON clamar.marca = maruni.marca, ".
						  "cadistribuidorescentrostbl discen ".
						  "WHERE unidad.vin = '".$_REQUEST['rpRepuveEspecialVinTxt']."' ".
						  "AND ultdet.centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ".
						  "AND unidad.vin = ultdet.vin ".
						  "AND unidad.vin NOT LIKE '?????????3N??????' ".
						  "AND unidad.vin NOT LIKE 'W????????????????' ".
						  "AND simuni.clasificacion = clamar.clasificacion ".
						  "AND simuni.simboloUnidad = unidad.simboloUnidad ".
						  "GROUP BY ultdet.vin, unidad.vin, clamar.marca, clamar.descripcion, unidad.color, unidad.descripcionUnidad";
							
		$rsPresentadas = fn_ejecuta_query($sqlAddEspecial);
		echo json_encode($sqlAddEspecial);
		
		$sqlQueryVin = "SELECT vin FROM alRepuveTmp WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
		$rsVin = fn_ejecuta_query($sqlQueryVin);		
		for ($i=0; $i<sizeof($rsVin['root']); $i++){
			$Vin_Temp = substr($rsVin['root'][$i]['vin'], 10, 1);			
			switch ($Vin_Temp){
				Case "1":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2001' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "2":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2002' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "3":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2003' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "4":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2004' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "5":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2005' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "6":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2006' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "7":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2007' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "8":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2008' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "9":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2009' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "A":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2010' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "B":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2011' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "C":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2012' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "D":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2013' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "E":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2014' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "F":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2015' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "G":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2016' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "H":
					$sqlUpdTmp = "UPDATE alRepuveTmp SET anio ='2017' WHERE vin = '".$rsVin['root'][$i]['vin']."' AND centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
					$rs = fn_ejecuta_query($sqlUpdTmp);
					break;
				Case "I":
					echo "----VIN INVALIDO----";
					break;
			}
		}	
	}
	
	function rejillaEspecial(){
	
		$sqlGetPresentadas = "SELECT vin, marca, modelo, color, descripcion, anio ".
							"FROM alrepuvetmp ".
							"WHERE centroDistribucion = '".$_REQUEST['rpCentrosDistHdn']."' ";
		$rsGetUnidades = fn_ejecuta_query($sqlGetPresentadas);
		echo json_encode($rsGetUnidades);
	
	}
	
	function cancelacionFolio(){
		$a = array();
		$e = array();
		$a['success'] = true;
		if($_REQUEST['rpRepuveFolioCancelacionTxt'] == ""){
				$e[] = array('folioRepuve'=>'rpRepuveFolioCancelacionTxt','msg'=>getRequerido());
				$a['errorMessage'] = getErrorRequeridos();
				$a['success'] = false;
		}

		$sqlQueryRepuveStr = "SELECT folioRepuve FROM alrepuvetbl WHERE folioRepuve=".$_REQUEST['rpRepuveFolioCancelacionTxt'];
		
		$rs = fn_ejecuta_query($sqlQueryRepuveStr);
		$folio = $rs['root'][0]['folioRepuve'];
		echo json_encode($rs);
		
		if($rs['root'][0]['folioRepuve'] != null){
				if ($a['success'] == true){
						$sqlUpdateRepuveStr = "UPDATE alrepuvetbl ".
												"SET observacion= '".strtoupper($_REQUEST['rpRepuveObservacionCancelacionTxa'])."', ".
												"fechaEvento = '".date("Y-m-d H:i:s")."', ".
												"estatus = 'CAN' ".
												"WHERE folioRepuve = ".$_REQUEST['rpRepuveFolioCancelacionTxt'];
						$rs = fn_ejecuta_query($sqlUpdateRepuveStr);

						if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
								$a['sql'] = $sqlUpdateRepuveStr;
								$a['successMessage'] = getRepuveUpdateMsg();
						} else {
								$a['success'] = false;
								$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdateRepuveStr;
							}			
				}
						$a['errors'] = $e;
						$a['successTitle'] = getMsgTitulo();
						echo json_encode($a);					
			}
		else
			{
				printF($rs['root'][0]['folioRepuve']);
			}
					
		
	}
	
	function reemplazoFolio(){
		$a = array();
		$e = array();
		$a['success'] = true;
		if($_REQUEST['rpRepuveReemplazoFolioReemplazoTxt'] == ""){
				$e[] = array('folioRepuve'=>'rpRepuveReemplazoFolioReemplazoTxt','msg'=>getRequerido());
				$a['errorMessage'] = getErrorRequeridos();
				$a['success'] = false;
		}

		$sqlQueryRepuveStr = "SELECT folioRepuve FROM alrepuvetbl WHERE folioRepuve=".$_REQUEST['rpRepuveReemplazoFolioReemplazoTxt'];
		
		$rs = fn_ejecuta_query($sqlQueryRepuveStr);
		$folio = $rs['root'][0]['folioRepuve'];
		echo json_encode($rs);
		
		if($rs['root'][0]['folioRepuve'] != null){
				if ($a['success'] == true){
						$sqlUpdateRepuveStr = "UPDATE alrepuvetbl ".
												"SET observacion= '".strtoupper($_REQUEST['idRepuveReemplazoObservacionesTxa'])."', ".
												"fechaEvento = '".date("Y-m-d H:i:s")."', ".
												"estatus = 'CAN' ".
												"WHERE folioRepuve = ".$_REQUEST['rpRepuveReemplazoFolioReemplazoTxt'];
						$rs = fn_ejecuta_query($sqlUpdateRepuveStr);

						if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
								$a['sql'] = $sqlUpdateRepuveStr;
								$a['successMessage'] = getRepuveUpdateMsg();
						} else {
								$a['success'] = false;
								$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdateRepuveStr;
							}			
				}
						$a['errors'] = $e;
						$a['successTitle'] = getMsgTitulo();
						echo json_encode($a);					
			}
		else
			{
				printF($rs['root'][0]['folioRepuve']);
			}
					
		
	}
	
?>