<?php
    //ESTE PHP SE USA PARA CONSULTAS ESPECIFICAS PARA LAS TERMINALES (SCANNERES)
    session_start();
    $_SESSION['modulo'] = "teConsultas";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alDanosVics.php");
    require_once("alUnidades.php");
    require_once("trViajesTractores.php");

    //SPECIAL CHARACTER
    //SE USA PARA DIFERENCIAR LOS DIFERENTES QUERYS EN LA MISMA LINEA
    $spChar = '~';

    $_REQUEST = trasformUppercase($_REQUEST);
    
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['teConsultasActionHdn']){
    	case 'checkLogin':
    		checkLogin();
    		break;
        case 'isPresentadaUnidad':
            isPresentadaUnidad();
            break;
        case 'getDatosPresentadas':
            getDatosPresentadas();
            break;
        case 'getColores':
            getColores();
            break;
        case 'presentarUnidad':
            presentarUnidad();
            break;
        case 'getUnidades':
            getUnidadesScanner();
            break;
        case 'validarEntradaPatio':
            validarEntradaPatio();
            break;
        case 'generarEntradaPatio':
            generarEntradaPatio();
            break;
        case 'validarCambioDistribuidor':
            validarCambioDistribuidor();
            break;
        case 'getDistribuidoresCombo':
            getDistribuidoresCombo();
            break;
        case 'cambiarDistribuidor':
            cambiarDistribuidor();
            break;
        case 'validarCapturaDanos':
            validarCapturaDanos();
            break;
        case 'getOrigenDanos':
            getOrigenDanos();
            break;
        case 'capturarDano':
            capturarDano();
            break;
        case 'getDatosRecepcionViaje':
            getDatosRecepcionViaje();
            break;
        case 'validarRecepcionViaje':
            validarRecepcionViaje();
            break;
        case 'recepcionViaje':
            recepcionViaje();
            break;
        case 'getTractoresEmbarcadas':
            getTractoresEmbarcadas();
            break;
        case 'validarEmbarcada':
            validarEmbarcada();
            break;
        case 'actualizarEmbarque':
            actualizarEmbarque();
            break;
        case 'validarUnidadSalidasGen':
            validarUnidadSalidasGen();
            break;
        case 'getTAT':
            getTAT();
            break;
        case 'generarTAT':
            generarTAT();
            break;
        case 'getPatios':
            getPatios();
            break;
        case 'getDatosFila':
            getDatosFila();
            break;
        case 'reordenarFila':
            reordenarFila();
            break;
        case 'getCampaniasCentrosDistribucion':
            getCampaniasCentrosDistribucion();
            break;
        case 'entradaCampania':
            entradaCampania();
            break;
        case 'setSalidaGeneral':
            setSalidaGeneral();
            break;
        case 'validarPatio':
            validarPatio();
            break;
        case 'generarEntradaSalida':
            generarEntradaSalida();
            break;
        case 'entradaContencionDB':
            entradaContencionDB();
            break;
        case 'validaVinEntradaContencion':
            validaVinEntradaContencion();
            break;
        case 'vinExisteContencion':
            vinExisteContencion();
            break;
        case 'vinNoExisteContencion':
            vinNoExisteContencion();
            break;
        case 'validaSimboloEntradaContencion':
            validaSimboloEntradaContencion();
            break;
        case 'getDatosContencion':
            getDatosContencion();
            break;
        case 'entradaContencionDB':
            entradaContencionDB();
            break;
        case 'consultarUnidad':
            consultarUnidad(); 
            break;
        default:
            echo '';
    }

    function isPresentadaUnidad(){

        $sqlHistorico = "SELECT u.vin FROM alUnidadesTbl u, alHistoricoUnidadesTbl hu ".
                        "WHERE u.vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                        "AND u.vin = hu.vin;";
        $rsHist = fn_ejecuta_query($sqlHistorico);

        if (!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
            if (intval($rsHist['records']) > 0) {
                echo '0|Unidad Ya Presentada|';
            } else { //Si no Existe se puede presentar
                //echo '1|Unidad Valida';

                $sql660 =   "SELECT a6.vin, a6.scaccode, a6.prodstatus, a6.dealerid, a6.model, ".
                            "a6.colorcode, a6.vupdate, a6.vuptime FROM al660tbl a6 ".
                            "WHERE a6.vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                            "AND (a6.dealerid IS NOT NULL OR a6.dealerid <> '') ".
                            "AND a6.von NOT LIKE '1111%' ".
                            "AND a6.von NOT LIKE '2222%' ".
                            "AND a6.von NOT LIKE '1.111%' ".
                            "AND a6.von NOT LIKE '2.222%' ".
                            "AND a6.vupdate =  (select max(a6_2.vupdate) from al660tbl a6_2 ".
                                                        "where a6_2.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                                        "and a6_2.scacCode IN ('MITS', 'XTRA') ".
                                                        "and a6_2.von NOT LIKE '1111%' AND a6_2.von NOT LIKE '2222%' AND a6_2.von NOT LIKE '1.111%' AND a6_2.von NOT LIKE '2.222%' ".
                                                        "group by a6_2.vin) ".
                            "AND a6.vuptime = (select max(a6_3.vuptime) from al660tbl a6_3 ".
                                              "where a6_3.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                              "and a6_3.von NOT LIKE '1111%' AND a6_3.von NOT LIKE '2222%' AND a6_3.von NOT LIKE '1.111%' AND a6_3.von NOT LIKE '2.222%' ".
                                              "and a6_3.vupdate = (select max(a6_4.vupdate) from al660tbl a6_4 ".
                                                                  "where a6_4.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                                                  "and a6_4.scacCode IN ('MITS', 'XTRA') ".
                                                                  "and a6_4.von NOT LIKE '1111%' AND a6_4.von NOT LIKE '2222%' AND a6_4.von NOT LIKE '1.111%' AND a6_4.von NOT LIKE '2.222%' ".
                                                                  "group by a6_4.vin) ".
                                              "and a6_3.scacCode = 'XTRA' ".
                                              "group by a6_3.vin) ".
                            "GROUP BY a6.vin ";

                $rs660 = fn_ejecuta_query($sql660);

                if($rs660['records'] > 0)
                    echo "1|".$rs660['root'][0]['model']."|".$rs660['root'][0]['dealerid']."|".$rs660['root'][0]['colorcode']."|";
                else
                    echo "1||||";

            }
        } else {

            echo '0|ERROR-NO HUBO RESPUESTA';
        }

    }
?>