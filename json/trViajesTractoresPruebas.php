<?php
    /************************************************************************
    * Autor: Alfonso César Martínez Fuertes
    * Fecha: 06-Noviembre-2014
    * Descripción: Programa para dar mantenimiento a foraneos
    *************************************************************************/
    //$_REQUEST['zonaHoraria'] = date_default_timezone_get();
    date_default_timezone_set('America/Mazatlan');
    
    session_start();

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("trGastosViajeTractor.php");
    require_once("alUnidades.php");
    require_once("alLocalizacionPatios.php");
    require_once("catGenerales.php");
    require("C:/Servidores/apache/htdocs/carbookbck/procesos/i830_CBK_AM.php");
    require("C:/Servidores/apache/htdocs/carbookbck/procesos/i343_Asignaciones.php");





    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['trViajesTractoresActionHdn']){
        case 'getTractoresDisponibles':
            getTractoresDisponiblesViajes();
            break;
        case 'getTractoresDisponiblesPreviaje':
            getTractoresDisponiblesPreviaje();
            break;            
        case 'getTractoresDisponiblesLiberacion':
            getTractoresDisponiblesLiberacion();
            break;
        case 'obtenerOperador':
            obtenerOperador();
            break;
        case 'getFacturas':
            getFacturas();
            break;
        case 'getFacturasAutorizadas':
            getFacturasAutorizadas();
            break;
        case 'getTractoresDisponiblesViaje':
            getTractoresDisponiblesViaje();
            break;
        case 'autorizarFacturasHdn':
            autorizarFacturasHdn();
            break;
        case 'preCargaFactura':
            preCargaFactura();
            break;
        case 'validaFacturasOB7':
            validaFacturasOB7();
            break;
        case 'solicitudesPendientes':
            solicitudesPendientes();
            break;
        case 'obtenerFacturasAutorizadas':
            obtenerFacturasAutorizadas();
            break;
        case 'getTractoresCancelacion':
            getTractoresCancelacion();
            break;
        case 'getChoferesDisponibles':
            getChoferesDisponibles();
            break;
        case 'getChoferesDisponiblesGastos':
            getChoferesDisponiblesGastos();
            break;
        case 'getTalonesViaje':
            echo json_encode(getTalonesViaje());
            break;

        case 'getTalonesViajeAsig':
            echo json_encode(getTalonesViajeAsig());
            break;
        case 'getUnidadesTalon':
            getUnidadesTalon();
            break;
        case 'getPreviaje':
            getPreviaje();
            break;
        case 'getViajes':
            getViajes();
            break;
        case 'getHistoricoViajes':
            echo json_encode(getHistoricoViajes());
            break;

        case 'getHistoricoViajesImpresion':
            echo json_encode(getHistoricoViajesImpresion());
            break;
        case 'getChoferesEspera':
            getChoferesEspera();
            break;
        case 'getChoferesEsperaGeneral':
            echo json_encode(getChoferesEsperaGeneral($_REQUEST['trViajesTractoresClaveMovimientoHdn']));
            break;
        case 'updChoferesEspera':
            updChoferesEspera();
            break;
        case 'addChoferEspera':
            addChoferEspera();
            break;
        case 'addViajes':
            addViajes();
            break;
        case 'updEstatusViaje':
            echo json_encode(updEstatusViaje($_REQUEST['trViajesTractoresIdViajeHdn'],
                                $_REQUEST['trViajesTractoresClaveMovimientoHdn']));
            break;
        case 'addViajeVacio':
            addViajeVacio();
            break;
        case 'addViajeAcompanante':
            addViajeAcompanante();
            break;
        case 'updTalones':
            updTalones();
            break;
        case 'modificacionTalones':
            modificacionTalones();
            break;
        case 'entregarTalon':
            entregarTalon();
            break;
        case 'liberarViaje':
            liberarViaje();
            break;
        case 'addUnidadesTalon':
            echo json_encode(addUnidadesTalon($_REQUEST['trViajesTractoresIdTalonTemporalHdn'],
                                                $_REQUEST['trViajesTractoresIdTalonHdn']));
            break;
        case 'updUnidadTalon':
            updUnidadTalon();
            break;
        case 'getDetalleTalon':
            getDetalleTalon();
            break;
        case 'cancelarViaje':
            cancelarViaje();
            break;
        case 'cancelarViajeEspecial':
            cancelarViajeEspecial();
            break;
        case 'cancelarTalon':
            cancelarTalon();
            break;
        case 'cancelarUnidadTalon':
            echo json_encode(cancelarUnidadTalon($_REQUEST['trap446IdTalonHdn'],$_REQUEST['trap446VinHdn']));
            break;
        case 'cancelarUnidadTalonModificacion':
            echo json_encode(cancelarUnidadTalonModificacion($_REQUEST['trap446IdTalonHdn']));
            break;
        case 'comprobacionViaje':
            comprobacionViaje();
            break;
        case 'getArbolTalones':
            getArbolTalones();
            break;
        case 'getTalonDetalleUnidades':
            getTalonDetalleUnidades();
            break;
        case 'addContinuidadEspecial':
            addContinuidadEspecial();
            break;
        case 'addContinuidadEspecialTalon':
            addContinuidadEspecialTalon();
            break;
        case 'addViajeTemp':
            addViajeTemp();
            break;
        case 'getViajeTemp':
            getViajeTemp();
            break;
        case 'dltViajeTemp':
            dltViajeTemp();
            break;
        case 'dltUnidadTalonTempMasivo':
            echo json_encode(dltUnidadTalonTempMasivo($_REQUEST['trap446IdTalonHdn'], $_REQUEST['trap446LiberaUnidadesHdn']));
            break;
        case 'getTalonesTemp':
            getTalonesTemp();
            break;
        case 'addUpdTalonesTemp':
            addUpdTalonesTemp();
            break;
        case 'cancelarTalonTemp':
            cancelarTalonTemp($_REQUEST['trap446IdTalonHdn']);
            break;
        case 'dltTalonesTempMasivo':
            dltTalonesTempMasivo($_REQUEST['trap446IdTalonHdn'],$_REQUEST['trap446ClaveMovimientoHdn']);
            break;
        case 'getUnidadesTalonTemp':
            echo json_encode(getUnidadesTalonTemp());
            break;
        case 'addUnidadesTalonTemp':
            addUnidadesTalonTemp();
            break;
        case 'dltUnidadTalonTemp':
            echo json_encode(dltUnidadTalonTemp($_REQUEST['trap446IdTalonHdn'], $_REQUEST['trap446VinHdn']));
            break;
        case 'recepcionTalon':
            recepcionTalon();
            break;
        case 'updChoferesEsperaReacomodo':
            updChoferesEsperaReacomodo();
            break;
        case 'addPreviaje':
            echo json_encode(addPreviaje());
            break;
        case 'addViajeTractor12':
            echo json_encode(addViajeTractor12());
            break;
        case 'getTalonesPorViajeTalon':
            getTalonesPorViajeTalon();
            break;
        case 'getReImpresionBitacora':
            getReImpresionBitacora();
            break;
        case 'verificarEmbarcadas':
            verificarEmbarcadas();
            break;
        case 'addUpdTalonesTempEsp';
             addUpdTalonesTempEsp();
            break;
        case 'getTalonesViaje1';
            getTalonesViaje1();
            break;
        case 'vaciaTemporales';
            vaciaTemporales();
            break;
        case 'foliosTalones';
            foliosTalones();
            break;
        case 'getUltimoIdViaje';
            getUltimoIdViaje();
            break;
        case 'getCentroDistribucion';
            getCentroDistribucion();
            break;
        case 'sqlGetHoldUnidades';
            sqlGetHoldUnidades();
            break;    
        case 'sqlGetAntSuc';
            sqlGetAntSuc();
            break;
        case 'sqlNumTalones';
            sqlNumTalones();
            break;
        case 'sqlGetFolio';
            sqlGetFolio();
            break;
        case 'getAlerta';
            getAlerta();
            break;            
        case 'validaCentroDisLiberacionTractor';
            validaCentroDisLiberacionTractor();
            break;
        case 'guardaPatioRetorno';
            guardaPatioRetorno();
            break;   
        case 'getTractoresDisponiblesAnt';
            getTractoresDisponiblesAnt();
            break;
        case 'getTractoresDisponiblesPend';
            getTractoresDisponiblesPend();
            break;
        case 'getTractoresDisponiblesLib';
            getTractoresDisponiblesLib();
            break;  
        case 'getTalonesCancelacion';
            getTalonesCancelacion();
            break;  
        case 'actualizaEstatus':
            actualizaEstatus();
            break;
        case 'quitarLiberacion':
            quitarLiberacion();
            break;
        case 'validaOdometro':
            validaOdometro();
            break;
        case 'validaViajeDiario':
            validaViajeDiario();
            break;
        default:
            echo '';

    }

    function getUltimoIdViaje(){
        $sqlGetIdViaje = " SELECT vt.idViajeTractor ".
                            "FROM trviajestractorestbl vt ".
                            "WHERE vt.idTractor = '".$_REQUEST['libTractorTractorCmb']."' ".
                            "AND vt.viaje = (SELECT max(vt2.viaje) FROM trviajestractorestbl vt2 where vt.idTractor = vt2.idTractor );  ";

        $rs = fn_ejecuta_query($sqlGetIdViaje);
        echo json_encode($rs);
    }

    function getTractoresDisponiblesViaje(){

   /*  if ($_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDSLP' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CMDAT') {

        $lsWhereStr = "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.idTractor = tr.idTractor AND  vt2.centroDistribucion='".$_REQUEST['trViajesTractoresCentroDistribucionHdn']."' ".
                      "AND (vt2.idViajePadre IS NULL OR vt2.idViajePadre IS NOT NULL AND vt2.claveMovimiento <> 'VX')) ".
                      "WHERE tr.estatus = 1 ".
                      "AND vt.idViajeTractor is not null ".
                      "AND vt.centroDistribucion='".$_REQUEST['trViajesTractoresCentroDistribucionHdn']."' ";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = ("vt.claveMovimiento IN (".substr($_REQUEST['trViajesTractoresMovDispHdn'], 0,-1).")");
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);            
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if($_REQUEST['trTractoresNuevosHdn'] == '1'){// 
            $isTractoresNuevos = " UNION ".
                        "SELECT 0 as idviajeTractor, ".
                        "0 as idviajepadre, ".
                        "tr.idtractor, ".
                        "tr.tractor, ".
                        "tr.rendimiento, ". 
                        "'' as clavemovimiento, ".
                        "0 as clavechofer, ".
                        "0 as numerorepartos, ".
                        "tr.tipotractor, ".
                        "'' as centrodistribucion, ".
                        "0 as viaje, ".
                        "0 as kilometrostabulados, ".
                        "0 as kilometroscomprobados, ".
                        "'' as nombreChofer, ".
                        "0 as numTalonesValidosViaje, ".
                        "'' AS descPlazaOrigen, ".
                        "'' AS descPlazaDestino, ".
                        "0  AS idPlazaOrigen, ". 
                        "0  AS idPlazaDestino, ".
                        "0  AS claveChoferAcompanante, ".
                        "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                        "FROM catractorestbl tr ".
                        "WHERE tr.idTractor NOT IN (SELECT vt.idTractor FROM trviajestractorestbl vt where centroDistribucion='".$_REQUEST['trViajesTractoresCentroDistribucionHdn']."') ".
                        "AND tr.estatus = 1 ".
                        "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ";
        }
        if($_REQUEST['trViajesTractoresListaEsperaHdn'] != ''){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isEnLista = "(SELECT centroDistribucion FROM trEsperaChoferesTbl ec ".
                         "WHERE ec.idTractor = tr.idTractor ".
                         "AND ec.centroDistribucion <> '".$_SESSION['usuCompania']."') AS centroDistribucionEspera, ";
        }
        $sqlGetTractoresDisponibles = "SELECT vt.idViajeTractor, vt.idViajePadre, tr.idTractor, tr.tractor, tr.rendimiento, vt.claveMovimiento, vt.claveChofer, vt.numeroRepartos, ".
                                      "tr.tipoTractor, vt.centroDistribucion, vt.viaje, vt.kilometrosTabulados, vt.kilometrosComprobados, ".
                                      "(SELECT CONCAT(ch.claveChofer, ' - ', ch.nombre, ' ', ch.apellidoPaterno, ' ', ch.apellidoMaterno) ".
                                      "FROM caChoferesTbl ch WHERE ch.claveChofer = vt.claveChofer) AS nombreChofer, ".
                                      $isEnLista.
                                      "(SELECT COUNT(*) FROM trTalonesViajesTbl tv WHERE vt.idViajeTractor = tv.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino, ".
                                      "(SELECT vt2.claveChofer FROM trViajesTractoresTbl vt2 WHERE vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante, ".
                                     "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                                      "FROM caTractoresTbl tr ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.idTractor = tr.idTractor ".$lsWhereStr.$isTractoresNuevos ;


        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);


     }

     else{ */  $lsWhereStr = "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.idTractor = tr.idTractor AND (vt2.idViajePadre IS NULL OR vt2.idViajePadre IS NOT NULL AND vt2.claveMovimiento <> 'VX')) ".
                      "WHERE  tr.estatus = 1 ".
                      "AND vt.idViajeTractor not in (SELECT vt2.idViajeTractor FROM trgastosviajetractortbl vt2 WHERE vt.idViajeTractor = vt2.idViajeTractor AND vt2.claveMovimiento='GU')  ".
                      "AND vt.idViajeTractor is not null";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = ("vt.claveMovimiento IN (".substr($_REQUEST['trViajesTractoresMovDispHdn'], 0,-1).")");
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);            
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if($_REQUEST['trTractoresNuevosHdn'] == '1'){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isTractoresNuevos = " UNION ".
                        "SELECT 0 as idviajeTractor, ".
                        "0 as idviajepadre, ".
                        "tr.idtractor, ".
                        "tr.tractor, ".
                        "tr.rendimiento, ". 
                        "'' as clavemovimiento, ".
                        "0 as clavechofer, ".
                        "0 as numerorepartos, ".
                        "tr.tipotractor, ".
                        "'' as centrodistribucion, ".
                        "0 as viaje, ".
                        "0 as kilometrostabulados, ".
                        "0 as kilometroscomprobados, ".
                        "'' as nombreChofer, ".
                        "0 as numTalonesValidosViaje, ".
                        "'' AS descPlazaOrigen, ".
                        "'' AS descPlazaDestino, ".
                        "0  AS idPlazaOrigen, ". 
                        "0  AS idPlazaDestino, ".
                        "0  AS claveChoferAcompanante, ".
                        "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                        "FROM catractorestbl tr ".
                        //"WHERE tr.idTractor NOT IN (SELECT vt.idTractor FROM trviajestractorestbl vt) ".
                        "where tr.estatus = 1 ".
                        "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ";
        }
        if($_REQUEST['trViajesTractoresListaEsperaHdn'] != ''){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isEnLista = "(SELECT centroDistribucion FROM trEsperaChoferesTbl ec ".
                         "WHERE ec.idTractor = tr.idTractor ".
                         "AND ec.centroDistribucion <> '".$_SESSION['usuCompania']."') AS centroDistribucionEspera, ";
        }
        $sqlGetTractoresDisponibles = "SELECT vt.idViajeTractor, vt.idViajePadre, tr.idTractor, tr.tractor, tr.rendimiento, vt.claveMovimiento, vt.claveChofer, vt.numeroRepartos, ".
                                      "tr.tipoTractor, vt.centroDistribucion, vt.viaje, vt.kilometrosTabulados, vt.kilometrosComprobados, ".
                                      "(SELECT CONCAT(ch.claveChofer, ' - ', ch.nombre, ' ', ch.apellidoPaterno, ' ', ch.apellidoMaterno) ".
                                      "FROM caChoferesTbl ch WHERE ch.claveChofer = vt.claveChofer) AS nombreChofer, ".
                                      $isEnLista.
                                      "(SELECT COUNT(*) FROM trTalonesViajesTbl tv WHERE vt.idViajeTractor = tv.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino, ".
                                      "(SELECT vt2.claveChofer FROM trViajesTractoresTbl vt2 WHERE vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante, ".
                                      "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                                      "FROM caTractoresTbl tr ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.idTractor = tr.idTractor ".$lsWhereStr.$isTractoresNuevos ;


        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);

    //}


        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
          $rs['root'][$nInt]['centroDistSesion'] = $_SESSION['usuCompania'];
        }

        echo json_encode($rs);
    }

    function getTractoresDisponiblesViajes(){

   /*  if ($_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDSLP' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CMDAT') {

        $lsWhereStr = "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.idTractor = tr.idTractor AND  vt2.centroDistribucion='".$_REQUEST['trViajesTractoresCentroDistribucionHdn']."' ".
                      "AND (vt2.idViajePadre IS NULL OR vt2.idViajePadre IS NOT NULL AND vt2.claveMovimiento <> 'VX')) ".
                      "WHERE tr.estatus = 1 ".
                      "AND vt.idViajeTractor is not null ".
                      "AND vt.centroDistribucion='".$_REQUEST['trViajesTractoresCentroDistribucionHdn']."' ";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = ("vt.claveMovimiento IN (".substr($_REQUEST['trViajesTractoresMovDispHdn'], 0,-1).")");
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);            
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if($_REQUEST['trTractoresNuevosHdn'] == '1'){// 
            $isTractoresNuevos = " UNION ".
                        "SELECT 0 as idviajeTractor, ".
                        "0 as idviajepadre, ".
                        "tr.idtractor, ".
                        "tr.tractor, ".
                        "tr.rendimiento, ". 
                        "'' as clavemovimiento, ".
                        "0 as clavechofer, ".
                        "0 as numerorepartos, ".
                        "tr.tipotractor, ".
                        "'' as centrodistribucion, ".
                        "0 as viaje, ".
                        "0 as kilometrostabulados, ".
                        "0 as kilometroscomprobados, ".
                        "'' as nombreChofer, ".
                        "0 as numTalonesValidosViaje, ".
                        "'' AS descPlazaOrigen, ".
                        "'' AS descPlazaDestino, ".
                        "0  AS idPlazaOrigen, ". 
                        "0  AS idPlazaDestino, ".
                        "0  AS claveChoferAcompanante, ".
                        "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                        "FROM catractorestbl tr ".
                        "WHERE tr.idTractor NOT IN (SELECT vt.idTractor FROM trviajestractorestbl vt where centroDistribucion='".$_REQUEST['trViajesTractoresCentroDistribucionHdn']."') ".
                        "AND tr.estatus = 1 ".
                        "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ";
        }
        if($_REQUEST['trViajesTractoresListaEsperaHdn'] != ''){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isEnLista = "(SELECT centroDistribucion FROM trEsperaChoferesTbl ec ".
                         "WHERE ec.idTractor = tr.idTractor ".
                         "AND ec.centroDistribucion <> '".$_SESSION['usuCompania']."') AS centroDistribucionEspera, ";
        }
        $sqlGetTractoresDisponibles = "SELECT vt.idViajeTractor, vt.idViajePadre, tr.idTractor, tr.tractor, tr.rendimiento, vt.claveMovimiento, vt.claveChofer, vt.numeroRepartos, ".
                                      "tr.tipoTractor, vt.centroDistribucion, vt.viaje, vt.kilometrosTabulados, vt.kilometrosComprobados, ".
                                      "(SELECT CONCAT(ch.claveChofer, ' - ', ch.nombre, ' ', ch.apellidoPaterno, ' ', ch.apellidoMaterno) ".
                                      "FROM caChoferesTbl ch WHERE ch.claveChofer = vt.claveChofer) AS nombreChofer, ".
                                      $isEnLista.
                                      "(SELECT COUNT(*) FROM trTalonesViajesTbl tv WHERE vt.idViajeTractor = tv.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino, ".
                                      "(SELECT vt2.claveChofer FROM trViajesTractoresTbl vt2 WHERE vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante, ".
                                     "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                                      "FROM caTractoresTbl tr ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.idTractor = tr.idTractor ".$lsWhereStr.$isTractoresNuevos ;


        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);


     }

     else{ */  $lsWhereStr = "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.idTractor = tr.idTractor AND (vt2.idViajePadre IS NULL OR vt2.idViajePadre IS NOT NULL AND vt2.claveMovimiento <> 'VX')) ".
                      "WHERE  tr.estatus = 1 ".
                      /*"AND vt.idViajeTractor not in (SELECT vt2.idViajeTractor FROM trgastosviajetractortbl vt2 WHERE vt.idViajeTractor = vt2.idViajeTractor AND vt2.claveMovimiento='GU')  ".*/
                      "AND vt.idViajeTractor is not null";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = ("vt.claveMovimiento IN (".substr($_REQUEST['trViajesTractoresMovDispHdn'], 0,-1).")");
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);            
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if($_REQUEST['trTractoresNuevosHdn'] == '1'){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isTractoresNuevos = " UNION ".
                        "SELECT 0 as idviajeTractor, ".
                        "0 as idviajepadre, ".
                        "tr.idtractor, ".
                        "tr.tractor, ".
                        "tr.rendimiento, ". 
                        "'' as clavemovimiento, ".
                        "0 as clavechofer, ".
                        "0 as numerorepartos, ".
                        "tr.tipotractor, ".
                        "'' as centrodistribucion, ".
                        "0 as viaje, ".
                        "0 as kilometrostabulados, ".
                        "0 as kilometroscomprobados, ".
                        "'' as nombreChofer, ".
                        "0 as numTalonesValidosViaje, ".
                        "'' AS descPlazaOrigen, ".
                        "'' AS descPlazaDestino, ".
                        "0  AS idPlazaOrigen, ". 
                        "0  AS idPlazaDestino, ".
                        "0  AS claveChoferAcompanante, ".
                        "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                        "FROM catractorestbl tr ".
                        //"WHERE tr.idTractor NOT IN (SELECT vt.idTractor FROM trviajestractorestbl vt) ".
                        "where tr.estatus = 1 ".
                        "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ";
        }
        if($_REQUEST['trViajesTractoresListaEsperaHdn'] != ''){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isEnLista = "(SELECT centroDistribucion FROM trEsperaChoferesTbl ec ".
                         "WHERE ec.idTractor = tr.idTractor ".
                         "AND ec.centroDistribucion <> '".$_SESSION['usuCompania']."') AS centroDistribucionEspera, ";
        }
        $sqlGetTractoresDisponibles = "SELECT vt.idViajeTractor, vt.idViajePadre, tr.idTractor, tr.tractor, tr.rendimiento, vt.claveMovimiento, vt.claveChofer, vt.numeroRepartos, ".
                                      "tr.tipoTractor, vt.centroDistribucion, vt.viaje, vt.kilometrosTabulados, vt.kilometrosComprobados, ".
                                      "(SELECT CONCAT(ch.claveChofer, ' - ', ch.nombre, ' ', ch.apellidoPaterno, ' ', ch.apellidoMaterno) ".
                                      "FROM caChoferesTbl ch WHERE ch.claveChofer = vt.claveChofer) AS nombreChofer, ".
                                      $isEnLista.
                                      "(SELECT COUNT(*) FROM trTalonesViajesTbl tv WHERE vt.idViajeTractor = tv.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino, ".
                                      "(SELECT vt2.claveChofer FROM trViajesTractoresTbl vt2 WHERE vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante, ".
                                      "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                                      "FROM caTractoresTbl tr ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.idTractor = tr.idTractor ".$lsWhereStr.$isTractoresNuevos ;


        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);

    //}


        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
          $rs['root'][$nInt]['centroDistSesion'] = $_SESSION['usuCompania'];
        }

        echo json_encode($rs);
    }

    function getTractoresDisponiblesPreviaje(){

        
        $sqlGetTractoresDisponibles = " select vt.idViajeTractor, vt.idViajePadre, vt.idTractor, ca.tractor, ca.rendimiento, vt.claveMovimiento, vt.claveChofer,
                                                vt.numeroRepartos,ca.tipoTractor,vt.centroDistribucion,
                                                    vt.viaje,
                                                    vt.kilometrosTabulados,
                                                    vt.kilometrosComprobados, (SELECT 
                                                            CONCAT(ch.claveChofer,
                                                                        ' - ',
                                                                        ch.nombre,
                                                                        ' ',
                                                                        ch.apellidoPaterno,
                                                                        ' ',
                                                                        ch.apellidoMaterno)
                                                        FROM
                                                            caChoferesTbl ch
                                                        WHERE
                                                            ch.claveChofer = vt.claveChofer) AS nombreChofer,
                                                    (SELECT 
                                                            COUNT(*)
                                                        FROM
                                                            trTalonesViajesTbl tv
                                                        WHERE
                                                            vt.idViajeTractor = tv.idViajeTractor
                                                                AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje,
                                                    (SELECT 
                                                            p.plaza
                                                        FROM
                                                            caPlazasTbl p
                                                        WHERE
                                                            p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen,
                                                    (SELECT 
                                                            p.plaza
                                                        FROM
                                                            caPlazasTbl p
                                                        WHERE
                                                            p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino,
                                                    (SELECT 
                                                            p.idPlaza
                                                        FROM
                                                            caPlazasTbl p
                                                        WHERE
                                                            p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen,
                                                    (SELECT 
                                                            p.idPlaza
                                                        FROM
                                                            caPlazasTbl p
                                                        WHERE
                                                            p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino,
                                                    (SELECT 
                                                            vt2.claveChofer
                                                        FROM
                                                            trViajesTractoresTbl vt2
                                                        WHERE
                                                            vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante,
                                                    (SELECT 
                                                            1 as bloqueado
                                                        FROM
                                                            trviajestractorestmp tmp
                                                        WHERE
                                                            tmp.idTractor = vt.idTractor) as bloqueado      
                                                from trviajestractorestbl vt, catractorestbl ca
                                                WHERE vt.claveMovimiento IN ('VU', 'VC', 'VE', 'VF')
                                                AND vt.idTractor=ca.idTractor
                                                AND ca.estatus=1
                                                AND ca.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."'
                                                AND vt.viaje = (SELECT 
                                                            MAX(vt2.viaje)
                                                        FROM
                                                            trViajesTractoresTbl vt2
                                                        WHERE
                                                            vt2.idTractor = VT.idTractor
                                                                AND (vt2.idViajePadre IS NULL
                                                                OR vt2.idViajePadre IS NOT NULL
                                                                AND vt2.claveMovimiento IN ('VU', 'VC', 'VE', 'VF')));";


        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);

    //}


        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
          $rs['root'][$nInt]['centroDistSesion'] = $_SESSION['usuCompania'];
        }

        echo json_encode($rs);
        
    }

    function getTractoresCancelacion(){
        $lsWhereStr = "WHERE tr.idTractor = vt.idTractor ".
                      "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.idTractor = tr.idTractor) ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct(substr($_REQUEST['trap481MovDispHdn'], 0,-1), "vt.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap481CompaniaHdn'], "tr.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $lsWhereStr .=  "AND tr.idTractor NOT IN  ".
                        "(SELECT tmp.idTractor from trViajesTractoresTmp tmp) ".
                        "AND tr.estatus = 1 ";

        $sqlGetTractoresCancelacionStr = "SELECT vt.idViajeTractor,tr.idTractor, tr.tractor, vt.claveMovimiento, ".
                                         "tr.tipoTractor, vt.viaje ".
                                         "FROM caTractoresTbl tr, trViajesTractoresTbl vt ".
                                         $lsWhereStr;


        $rs = fn_ejecuta_query($sqlGetTractoresCancelacionStr);

        echo json_encode($rs);
    }



    function getChoferesDisponibles(){
        /* claves de Movimiento Chofer Válidas
        * 'VP' - PAGADO
        * 'VU' - COMPROBADO 
        * 'VX' - CANCELADO
        *  NULL - SIN VIAJE
        */
        //Establece la zona horaria
                date_default_timezone_set($_REQUEST['zonaHoraria']);
            $fechaActual = date('Y-m-d');

        $lsWhereStr = "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.claveChofer = ch.claveChofer) ".
                      "WHERE ch.claveChofer NOT IN  ".
                      "(SELECT IFNULL(tmp.claveChofer,'') FROM trViajesTractoresTmp tmp where tmp.claveChofer=ch.claveChofer) ".
                      "AND ch.estatus = 1 ".
                      "AND (vt.claveMovimiento IN ('VP', 'VU', 'VX', 'VC','VF','VE') OR vt.claveMovimiento IS NULL)";

        /*if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct(substr($_REQUEST['trViajesTractoresMovDispHdn'], 0,-1), "vt.claveMovimiento", 1);
            $lsWhereStr .= $lsCondicionStr." OR vt.claveMovimiento IS NULL) ";
        }*/
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveChoferHdn'], "ch.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTractorHdn'], "vt.idTractor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTipoChoferHdn'], "ch.tipoChofer", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetTractoresDisponibles = "SELECT ch.*, vt.claveMovimiento as claveMovimientoViaje, vt.idTractor, vt.idViajePadre, ".
                                      " '' AS msgVigLic, '' AS msgVigCer, '' AS msgVigCon ".
                                      /* "(SELECT COUNT(hu.vin) FROM alHistoricoUnidadesTbl hu ".
                                        "WHERE ch.claveChofer = hu.claveChofer ".
                                        "AND hu.claveMovimiento = 'RA') AS numRodadasAsignadas ". */
                                      "FROM caChoferesTbl ch ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.claveChofer = ch.claveChofer ".
                                      $lsWhereStr.
                                      " ORDER BY ch.claveChofer ";

        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);
       // echo json_encode($sqlGetTractoresDisponibles);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $vigenciaLicencia = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaLicencia']));
            $vigenciaCertificado = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaCertificado']));
            $vigenciaContrato = date('Y-m-d',strtotime($rs['root'][$iInt]['vigenciaContrato']));
            
            if($fechaActual > $vigenciaLicencia && !empty($rs['root'][$iInt]['vigenciaLicencia']) && $rs['root'][$iInt]['vigenciaLicencia'] != '0000-00-00')
            {
                    $rs['root'][$iInt]['msgVigLic'] = 'La vigencia de la licencia ya est&aacute vencida.';
            }
            if($fechaActual > $vigenciaCertificado && !empty($rs['root'][$iInt]['vigenciaCertificado']) && $rs['root'][$iInt]['vigenciaCertificado'] != '0000-00-00')
            {
                    $rs['root'][$iInt]['msgVigCer'] = 'La vigencia del certificado ya est&aacute vencida.';
            }            
            if($fechaActual > $vigenciaContrato && !empty($rs['root'][$iInt]['vigenciaContrato']) && $rs['root'][$iInt]['vigenciaContrato'] != '0000-00-00')
            {
                    $rs['root'][$iInt]['msgVigCon'] = 'La vigencia del contrato ya est&aacute vencida.';
            }            
            $rs['root'][$iInt]['nombreChofer'] = $rs['root'][$iInt]['claveChofer']." - ".
                                                 $rs['root'][$iInt]['nombre']." ".
                                                 $rs['root'][$iInt]['apellidoPaterno']." ".
                                                 $rs['root'][$iInt]['apellidoMaterno'];
        }

        echo json_encode($rs);
    }

    function getChoferesDisponiblesGastos(){
        $lsWhereStr = "WHERE (vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.claveChofer = ch.claveChofer) ".
                      "AND ch.claveChofer NOT IN  ".
                      "(SELECT tmp.claveChofer from trViajesTractoresTmp tmp) ";
                      "AND tr.estatus = 1 ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct(substr($_REQUEST['trViajesTractoresMovDispHdn'], 0,-1), "vt.claveMovimiento", 1);
            $lsWhereStr .= $lsCondicionStr." OR vt.claveMovimiento IS NULL) ";
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        if ($_REQUEST['trViajesTractoresChoferHdn'] != "") {
            $lsWhereStr .= "OR ch.claveChofer = ".$_REQUEST['trViajesTractoresChoferHdn']." ";
        }

        $sqlGetTractoresDisponibles = "SELECT ch.*,vt.claveMovimiento as claveMovimientoViaje ".
                                      "FROM caChoferesTbl ch ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.claveChofer = ch.claveChofer ".
                                      $lsWhereStr.
                                      "ORDER BY ch.claveChofer ";

        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['nombreChofer'] = $rs['root'][$iInt]['claveChofer']." - ".
                                                 $rs['root'][$iInt]['nombre']." ".
                                                 $rs['root'][$iInt]['apellidoPaterno']." ".
                                                 $rs['root'][$iInt]['apellidoMaterno'];
        }

        echo json_encode($rs);
    }

    function getTalonesViaje(){
        if ($_SESSION['usuario']=='HARUMY') {
            $lsWhereStr = "WHERE tv.distribuidor = dc.distribuidorCentro ".
                      "AND tv.idViajeTractor = vt.idViajeTractor ".
                      "AND vt.idTractor = tr.idTractor ".
                      "AND ch.claveChofer = vt.claveChofer ".
                      "AND es.idPais = pa.idPais ".
                      "AND mu.idEstado = es.idEstado ".
                      "AND cl.idMunicipio = mu.idMunicipio ".
                      "AND dr.idColonia = cl.idColonia ".
                      "AND tv.maniobrasCarga is null ".
                      "AND dr.direccion = tv.direccionEntrega ".
                      "AND year(tv.fechaEvento) in ('2024','2023') ".
                      //"AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].", ".
                      "AND tv.claveMovimiento != 'TX' ";
        }else{
            $lsWhereStr = "WHERE tv.distribuidor = dc.distribuidorCentro ".
                      "AND tv.idViajeTractor = vt.idViajeTractor ".
                      "AND vt.idTractor = tr.idTractor ".
                      "AND ch.claveChofer = vt.claveChofer ".
                      "AND es.idPais = pa.idPais ".
                      "AND mu.idEstado = es.idEstado ".
                      "AND cl.idMunicipio = mu.idMunicipio ".
                      "AND dr.idColonia = cl.idColonia ".
                      "AND dr.direccion = tv.direccionEntrega ".
                      "AND year(tv.fechaEvento) in ('2024','2023') ".
                      //"AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].", ".
                      "AND tv.claveMovimiento != 'TX' ";     
        }
        

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "vt.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresViajeHdn'], "vt.viaje", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTalonHdn'], "tv.idTalon", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresDistribuidorHdn'], "tv.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresFolioTxt'], "tv.folio", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovimientoHdn'], "tv.claveMovimiento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "tv.centroDistribucion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "tr.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresNumTractorHdn'], "tr.tractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovViajeHdn'], "vt.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTipoDoctoHdn'], "tv.tipoDocumento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaRemitenteHdn'], "tv.companiaRemitente", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }


        $sqlGetTalones = "SELECT 0 AS temp, tv.*, dc.descripcionCentro, dc.tipoDistribuidor, tr.idTractor, tr.rendimiento, ".
                                 "vt.fechaEvento AS fechaViaje, vt.numeroRepartos, vt.kilometrosTabulados, vt.viaje, ".
                                 "vt.claveMovimiento AS claveMovimientoViaje, vt.numeroUnidades AS numeroUnidadesViaje, ".
                                 " tr.tractor, tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                 "(SELECT dc3.descripcionCentro FROM caDistribuidoresCentrosTbl dc3 ".
                                    "WHERE dc3.distribuidorCentro = tv.companiaRemitente) AS descripcionCompania, ".
                                 "(SELECT COUNT(*) FROM trTalonesViajesTbl tv2 WHERE tv2.idViajeTractor = vt.idViajeTractor ".
                                    "AND tv2.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                 "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tv.idTalon) AS numeroUnidadesTmp, ".
                                 "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                 "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                 "(SELECT pl2.plaza FROM caPlazasTbl pl2 WHERE pl2.idPlaza = tv.idPlazaDestino) AS nombrePlazaDestino,  ".
                                 "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' ".
                                    "AND cg.columna='claveMovimiento' AND cg.valor = tv.claveMovimiento) AS descClaveMovTalon, ".
                                 "(SELECT cg2.nombre FROM caGeneralesTbl cg2 WHERE cg2.tabla='trTalonesViajesTbl' ".
                                    "AND cg2.columna='claveMovimiento' AND cg2.valor = tv.claveMovimiento) AS descTipoDocto, ".
                                 "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' ".
                                    "AND cg.columna='tipoTalon' AND cg.valor = tv.tipoTalon) AS nombreTipoTalon, ".
                                 "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                    "WHERE dc2.distribuidorCentro = vt.centroDistribucion) AS nombreCentroDist, ".
                                 "pa.pais, es.estado, mu.municipio, cl.colonia, cl.cp, dr.calleNumero ".
                                 "FROM trTalonesViajesTbl tv, caDistribuidoresCentrosTbl dc, trViajesTractoresTbl vt, ".
                                 "caTractoresTbl tr, caChoferesTbl ch, caPaisesTbl pa, caEstadosTbl es, ".
                                 "caMunicipiosTbl mu, caColoniasTbl cl, caDireccionesTbl dr ";


        if($_REQUEST['trViajesTractoresAsignacionHdn'] != ''){
            $sqlGetTalones .=   " UNION ".
                                "SELECT 1 AS temp, tt.numeroTalon, tt.distribuidor, 'ESP' AS folio, tt.idViajeTractor, tt.companiaRemitente,".
                                "-1 AS plazaOrigen, -1 AS plazaDestino, tt.direccionEntrega, tt.centroDistribucion, ".
                                "tt.tipoTalon, '".date("Y-m-d H:i:s")."', tt.observaciones, tt.numeroUnidades, ".
                                "0 AS importe, 0 AS seguro, 0 AS tarifaCobrar, 0 AS kilometrosCobrar, 0 AS impuesto, ".
                                "0 AS retencion, 'TV' AS claveMovimiento, 'TV' AS tipoDocumento, ".
                                "dc.descripcionCentro, dc.tipoDistribuidor, tr.idTractor, tr.rendimiento, vt.fechaEvento AS fechaViaje, vt.numeroRepartos,".
                                "vt.kilometrosTabulados, vt.viaje, vt.claveMovimiento AS claveMovimientoViaje, vt.numeroUnidades AS numeroUnidadesViaje, tr.tractor, ".
                                "tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                "(SELECT dc3.descripcionCentro FROM caDistribuidoresCentrosTbl dc3 WHERE dc3.distribuidorCentro = tt.companiaRemitente) AS descripcionCompania, ".
                                "0 AS numTalonesValidosViaje, ".
                                "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tt.numeroTalon) AS numeroUnidadesTmp, ".
                                "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                "'' AS nombrePlazaOrigen, ".
                                "(SELECT pl2.plaza FROM caPlazasTbl pl2, caDistribuidoresCentrosTbl dc WHERE dc.distribuidorCentro = tt.distribuidor ".
                                    "AND pl2.idPlaza = dc.idPlaza) AS nombrePlazaDestino, ".
                                "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' AND cg.columna='claveMovimiento' ".
                                    "AND cg.valor = claveMovimiento) AS descClaveMovTalon, ".
                                "(SELECT cg2.nombre FROM caGeneralesTbl cg2 WHERE cg2.tabla='trTalonesViajesTbl' AND cg2.columna='claveMovimiento' ".
                                    "AND cg2.valor = claveMovimiento) AS descTipoDocto, ".
                                "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' AND cg.columna='tipoTalon' ".
                                    "AND cg.valor = tt.tipoTalon) AS nombreTipoTalon, ".
                                "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 WHERE dc2.distribuidorCentro = vt.centroDistribucion) AS nombreCentroDist, ".
                                "pa.pais, es.estado, mu.municipio, cl.colonia, cl.cp, dr.calleNumero ".
                                "FROM trtalonesviajestmp tt, caDistribuidoresCentrosTbl dc, trViajesTractoresTbl vt, caTractoresTbl tr, caChoferesTbl ch, caPaisesTbl pa, ".
                                "caEstadosTbl es, caMunicipiosTbl mu, caColoniasTbl cl, caDireccionesTbl dr ".
                                "WHERE tt.distribuidor = dc.distribuidorCentro AND tt.idViajeTractor = vt.idViajeTractor AND vt.idTractor = tr.idTractor ".
                                "AND ch.claveChofer = vt.claveChofer AND es.idPais = pa.idPais AND mu.idEstado = es.idEstado ".
                                "AND cl.idMunicipio = mu.idMunicipio AND dr.idColonia = cl.idColonia AND dr.direccion = tt.direccionEntrega AND claveMovimiento != 'TX' ".
                                "AND vt.idViajeTractor = 229 ";
        } else {
            $sqlGetTalones .= $lsWhereStr;
        }

        $rs = fn_ejecuta_query($sqlGetTalones);
       // echo json_encode($sqlGetTalones);
        $dist = array();
        $talonesValidos = 0; //TALONES NO CANCELADOS DEL VIAJE

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['descDistribuidor'] = $rs['root'][$nInt]['distribuidor']." - ".$rs['root'][$nInt]['descripcionCentro'];
            $rs['root'][$nInt]['descCompania'] = $rs['root'][$nInt]['companiaRemitente']." - ".$rs['root'][$nInt]['descripcionCompania'];
            $rs['root'][$nInt]['descTractorCia'] = $rs['root'][$nInt]['ciaTractor']." - ".$rs['root'][$nInt]['descCiaTractor'];
            $rs['root'][$nInt]['nombreChofer'] = $rs['root'][$nInt]['claveChofer']." - ".
                                                 $rs['root'][$nInt]['nombre']." ".
                                                 $rs['root'][$nInt]['apellidoPaterno']." ".
                                                 $rs['root'][$nInt]['apellidoMaterno'];
            $rs['root'][$nInt]['direccionCompleta'] = $rs['root'][$nInt]['calleNumero'].", ".
                                                      $rs['root'][$nInt]['colonia'].", ".
                                                      $rs['root'][$nInt]['municipio'].", ".
                                                      $rs['root'][$nInt]['estado'].", ".
                                                      $rs['root'][$nInt]['pais'].", ".
                                                      $rs['root'][$nInt]['cp'];

            $rs['root'][$nInt]['fechaActual'] = date('d-m-Y');
            $rs['root'][$nInt]['fechaEvento'] = date('Y-m-d', strtotime($rs['root'][$nInt]['fechaEvento']));


            if(!in_array($rs['root'][$nInt]['distribuidor'], $dist)) {
                array_push($dist, $rs['root'][$nInt]['distribuidor']);
            }
        }

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['numeroDistribuidores'] = sizeof($dist);
        }
        return $rs;
    }

    function getUnidadesTalon(){
        $lsWhereStr = "WHERE ut.idTalon = tv.idTalon ".
                        "AND ut.vin = un.vin ".
                        "AND un.simboloUnidad = su.simboloUnidad ".
                        "AND hu.vin = un.vin ".
                        "AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) ".
                                "FROM alHistoricoUnidadesTbl hu2 WHERE hu2.vin = un.vin) ".
                        "AND hu.idTarifa = tf.idTarifa ".
                        "AND un.vin NOT IN  (SELECT tmp.vin from alUnidadesTmp tmp)".
                        "AND hu.claveMovimiento='RP'" ;

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTalonHdn'], "ut.idTalon", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "tv.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresAvanzadaHdn'], "un.avanzada", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresVinHdn'], "ut.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovimientoTalonHdn'], "tv.claveMovimiento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "tv.centroDistribucion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        /*if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaRemitenteHdn'], "tv.companiaRemitente", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }*/
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresFolioTxt'], "tv.folio", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetUnidadesTalonStr = "SELECT tv.*, ut.*, un.color, un.simboloUnidad, su.descripcion AS descSimbolo, 0 AS aplicaValor, ".
                                    "hu.idTarifa, hu.claveMovimiento AS claveMovUnidad, tf.tarifa, tf.descripcion AS descTarifa, su.marca, ".
                                    "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu ".
                                        "WHERE mu.marca = su.marca) AS descMarca, ".
                                    "(SELECT co.descripcion FROM caColorUnidadesTbl co ".
                                        "WHERE co.color = un.color AND co.marca = su.marca) AS descColor ".
                                    "FROM trTalonesViajesTbl tv, trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, ".
                                        "caSimbolosUnidadesTbl su, alHistoricoUnidadesTbl hu, caTarifasTbl tf ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetUnidadesTalonStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['avanzada'] = substr($rs['root'][$iInt]['vin'], -8);
        }

        echo json_encode($rs);
    }

    function getPreviaje(){
        $lsWhereStr = "WHERE cc.claveChofer = vt.claveChofer ".
                      "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                        "WHERE vt2.idTractor = vt.idTractor) ".
                      "AND tr.idTractor = vt.idTractor ".
                      "AND vt.idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn']." ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveChoferHdn'], "cc.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetPreviajeStr = "SELECT vt.idViajeTractor, vt.idTractor, cc.claveChofer, cc.apellidoPaterno, cc.apellidoMaterno,".
                                "cc.nombre, vt.viaje, tr.rendimiento, ".
                                "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                    "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."') AS plazaOrigen ".
                                "FROM caChoferesTbl cc, trViajesTractoresTbl vt, caTractoresTbl tr ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetPreviajeStr);

        $sqlGetDatosTractor = "SELECT tr.rendimiento FROM caTractoresTbl tr ".
                              "WHERE tr.idTractor=".$_REQUEST['trViajesTractoresIdTractorHdn'];

        $rsTr = fn_ejecuta_query($sqlGetDatosTractor);

        if (isset($rs['root'])) {
            for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
                if($rs['root'][$iInt]['nombre'] != ""){
                    $rs['root'][$iInt]['nombreChofer'] = $rs['root'][$iInt]['nombre'].' '.
                                                         $rs['root'][$iInt]['apellidoPaterno'].' '.
                                                         $rs['root'][$iInt]['apellidoMaterno'];

                    $rs['root'][$iInt]['origen'] = $_SESSION['usuCiaAut'];
                }
                if ($rs['root'][$iInt]['viaje'] == "") {
                    $rs['root'][$iInt]['viaje'] = 1;
                    $rs['root'][$iInt]['idTractor'] = $_REQUEST['trViajesTractoresIdTractorHdn'];
                    $rs['records'] = 1;
                } else {
                    $rs['root'][$iInt]['viaje'] += 1;
                }
            }
        } else {
            $rs['root'][0]['viaje'] = 1;
            $rs['root'][0]['idTractor'] = $_REQUEST['trViajesTractoresIdTractorHdn'];
            $rs['root'][0]['rendimiento'] = $rsTr['root'][0]['rendimiento'];
            $rs['records'] = 1;
        }
        echo json_encode($rs);
    }

    function getViajes(){
        $lsWhereStr = "WHERE vt3.claveChofer = cc.claveChofer ".
                      "AND vt3.idTractor = tr.idTractor ";
                      if($_REQUEST['trViajesTractoresAcompananteMovHdn']== ''){
                        $lsWhereStr = fn_concatena_condicion($lsWhereStr,"vt3.viaje = (SELECT MAX(vt4.viaje) FROM trViajesTractoresTbl vt4 WHERE vt4.idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'].") ");
                      }else{
                        $lsWhereStr = fn_concatena_condicion($lsWhereStr,"vt3.viaje =(SELECT MAX(vt4.viaje) FROM trViajesTractoresTbl vt4 WHERE vt4.idTractor = vt3.idTractor AND vt4.claveMovimiento  !=".
                        " '".$_REQUEST['trViajesTractoresAcompananteMovHdn']."') ");
                      }

        //--------------------------------------------------------------------------------------------------------------
        if($_REQUEST['trViajesTractoresMaxViajeNoCanceladoHdn'] != ""){ //Trae el último viaje del tractor NO Cancelado VX de Acompañante
            $lsWhereStr = "WHERE vt3.claveChofer = cc.claveChofer ".
                          "AND vt3.idTractor = tr.idTractor ";
            $lsWhereStr = fn_concatena_condicion($lsWhereStr,"vt3.viaje =(SELECT MAX(vt4.viaje) ".
                                                                 "FROM trViajesTractoresTbl vt4 ".
                                                                 "WHERE vt4.idTractor = vt3.idTractor ".
                                                                 "AND vt4.idViajePadre IS NULL ".
                                                                 "OR (vt4.idViajePadre IS NOT NULL AND vt4.claveMovimiento <> 'VX')) ");
        }
        //--------------------------------------------------------------------------------------------------------------

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "vt3.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "vt3.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdPlazaOrigenHdn'], "vt3.idPlazaOrigen", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdPlazaDestinoHdn'], "vt3.idPlazaDestino", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "vt3.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresViajeTxt'], "vt3.viaje", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresFechaTxt'], "vt3.fechaEvento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmTabuladosTxt'], "vt3.kilometrosTabulados", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmComprobadosTxt'], "vt3.kilometrosComprobados", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmSinUnidadTxt'], "vt3.kilometrosSinUnidad", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresNumUnidadesTxt'], "vt3.numeroUnidades", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresRepartosTxt'], "vt3.numeroRepartos", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajePadreHdn'], "vt3.idViajePadre", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovimientoHdn'], "vt3.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveChoferHdn'], "cc.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTractorHdn'], "tr.tractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetViajesStr = "SELECT vt3.*,cc.claveChofer, cc.apellidoPaterno, cc.apellidoMaterno,vt3.hojaInstrucciones, cc.nombre, ".
                                "vt3.claveMovimiento, tr.tractor, tr.idTractor, tr.rendimiento, tr.compania, ".
                            "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."') AS plazaOrigen, ".
                            "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                "WHERE dc2.distribuidorCentro = vt3.centroDistribucion) AS nombreCentroDist, ".
                            "(SELECT COUNT(*) FROM trTalonesViajesTbl tv ".
                                "WHERE tv.idViajeTractor = vt3.idViajeTractor) AS numeroTalones, ".
                            "(SELECT COUNT(*) FROM trTalonesViajesTbl tv ".
                                "WHERE tv.idViajeTractor = vt3.idViajeTractor ".
                                "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                            "(SELECT pl.plaza FROM caplazastbl pl WHERE pl.idPlaza = vt3.idPlazaOrigen) AS descPlazaOrigen, ".
                            "(SELECT pl2.plaza FROM caplazastbl pl2 WHERE pl2.idPlaza = vt3.idPlazaDestino) AS descPlazaDestino, ".
                            "(SELECT claveChofer FROM trviajestractorestbl WHERE idViajePadre = vt3.idViajeTractor) AS claveChoferAcompanante, ".
                            "(SELECT idViajeTractor FROM trviajestractorestbl WHERE idViajePadre = vt3.idViajeTractor) AS idViajeAcompanante, ".
                            "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE vt3.claveMovimiento = cg.valor ".
                            "AND cg.tabla = 'trViajesTractoresTbl' AND cg.columna = 'claveMovimiento') AS descClaveMovimientoViaje ".
                            "FROM caChoferesTbl cc, trViajesTractoresTbl vt3, caTractoresTbl tr ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetViajesStr);
        //echo json_encode($sqlGetViajesStr);

        $sqlGetNumDistribuidoresStr = "SELECT distribuidor FROM trTalonesViajesTbl ".
                                        "WHERE idViajeTractor = ".$rs['root'][0]['idViajeTractor']." ".
                                        "AND claveMovimiento != 'TX'";

        $rsTalones = fn_ejecuta_query($sqlGetNumDistribuidoresStr);
        $dist = array();
        for ($nInt=0; $nInt < sizeof($rsTalones['root']); $nInt++) {
            if(!in_array($rsTalones['root'][$nInt]['distribuidor'], $dist)){
                array_push($dist, $rsTalones['root'][$nInt]['distribuidor']);
            }
        }

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['nombreChofer'] = $rs['root'][$nInt]['claveChofer']." - ".
                                                 $rs['root'][$nInt]['nombre']." ".
                                                 $rs['root'][$nInt]['apellidoPaterno']." ".
                                                 $rs['root'][$nInt]['apellidoMaterno'];

            $rs['root'][$nInt]['fechaActual'] = date('d-m-Y');

            $rs['root'][$nInt]['numeroDistribuidores'] = sizeof($dist);
        }

        echo json_encode($rs);
    }

    function getHistoricoViajes(){


        if ($_REQUEST['trViajesTractoresEsPolizaHdn'] === '1') {
                $lsWhereStr = "WHERE vt.claveChofer = ch.claveChofer ".
                      /*"AND vt.viaje= (SELECT min(vi.viaje) FROM trViajesTractoresTbl vi WHERE vi.claveChofer = vt.claveChofer AND ( vi.claveMovimiento = 'VC' OR  vi.claveMovimiento = 'VE' OR  vi.claveMovimiento = 'VO' OR  vi.claveMovimiento = 'VU')) ".*/
                      "AND vt.viaje= (SELECT min(vi.viaje) FROM trViajesTractoresTbl vi WHERE vi.claveChofer = vt.claveChofer AND ( vi.claveMovimiento in ('VU','UV'))) ".
                      "AND vt.idTractor = tr.idTractor ";
        }else{
                $lsWhereStr = "WHERE vt.claveChofer = ch.claveChofer ".
                              "AND vt.idTractor = tr.idTractor ";
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "vt.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "vt.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdPlazaOrigenHdn'], "vt.idPlazaOrigen", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdPlazaDestinoHdn'], "vt.idPlazaDestino", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "vt.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresViajeTxt'], "vt.viaje", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmTabuladosTxt'], "vt.kilometrosTabulados", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmComprobadosTxt'], "vt.kilometrosComprobados", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmSinUnidadTxt'], "vt.kilometrosSinUnidad", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresNumUnidadesTxt'], "vt.numeroUnidades", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresRepartosTxt'], "vt.numeroRepartos", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajePadreHdn'], "vt.idViajePadre", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            //$lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovimientoHdn'], "vt.claveMovimiento", 1);

            $lsCondicionStr = " vt.claveMovimiento in ('VU','UV')";
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
         /*if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveAcompañanteHdn'], "vt.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }*/
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveChoferHdn'], "ch.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTractorHdn'], "tr.tractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        //FECHA
        //Desde
        if ($_REQUEST['trViajesTractoresFechaDesdeTxt'] != "") {
            if ($lsWhereStr == "") {
                   $lsWhereStr .= " WHERE vt.fechaEvento >= '".$_REQUEST['trViajesTractoresFechaDesdeTxt']."' ";
            } else {
                $lsWhereStr .= " AND vt.fechaEvento >= '".$_REQUEST['trViajesTractoresFechaDesdeTxt']."' ";
            }
        }
        //Hasta
        if ($_REQUEST['trViajesTractoresFechaHastaTxt'] != "") {
            if ($lsWhereStr == "") {
                $lsWhereStr .= " WHERE date_format(vt.fechaEvento, '%Y-%m-%d') <= '".$_REQUEST['trViajesTractoresFechaHastaTxt']."' ";
            } else {
                $lsWhereStr .= " AND date_format(vt.fechaEvento, '%Y-%m-%d') <= '".$_REQUEST['trViajesTractoresFechaHastaTxt']."' ";
            }
        }

        //CHOFERES
        //Desde
        if ($_REQUEST['trViajesTractoresChoferDesdeTxt'] != "") {
            if ($lsWhereStr == "") {
                   $lsWhereStr .= " WHERE vt.claveChofer >= '".$_REQUEST['trViajesTractoresChoferDesdeTxt']."' ";
            } else {
                $lsWhereStr .= " AND vt.claveChofer >= '".$_REQUEST['trViajesTractoresChoferDesdeTxt']."' ";
            }
        }
        //Hasta
        if ($_REQUEST['trViajesTractoresChoferHastaTxt'] != "") {
            if ($lsWhereStr == "") {
                $lsWhereStr .= " WHERE vt.claveChofer <= '".$_REQUEST['trViajesTractoresChoferHastaTxt']."' ";
            } else {
                $lsWhereStr .= " AND vt.claveChofer <= '".$_REQUEST['trViajesTractoresChoferHastaTxt']."' ";
            }
        }
        //Tractor, descripcion clave mov
        $sqlGetHistoricoViajesStr = "SELECT vt.*, ch.apellidoPaterno, ch.apellidoMaterno, ch.nombre, ch.centroDistribucionOrigen, tr.tractor, ".
                                    "COUNT(tv.idTalon) AS numeroTalones, SUM(tv.numeroUnidades) AS totalUnidades, ".
                                    "tr.rendimiento, tr.compania, ".
                                    "(SELECT COUNT(*) FROM trTalonesViajesTbl tv ".
                                        "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                    "(SELECT co.descripcion FROM caCompaniasTbl co WHERE co.compania = tr.compania) AS nombreCompania, ".
                                    "(SELECT tr.tractor FROM caTractoresTbl tr WHERE tr.idTractor = vt.idTractor) AS tractor, ".
                                    "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trViajesTractoresTbl' ".
                                    "AND cg.columna='claveMovimiento' AND cg.valor = vt.claveMovimiento) AS nombreClaveMov, ".
                                    "(SELECT COUNT(*) FROM trTalonesViajesTbl tv ".
                                        "WHERE tv.idViajeTractor = vt.idViajeTractor) AS numeroTalones, ".
                                    "(SELECT COUNT(DISTINCT tv.distribuidor) FROM trTalonesViajesTbl tv ".
                                        "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numDistribuidorValidos, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = vt.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                    "(SELECT pl2.plaza FROM caPlazasTbl pl2 WHERE pl2.idPlaza = vt.idPlazaDestino) AS nombrePlazaDestino, ".
                                    "(SELECT 1 FROM trviajestractorestbl vt1, trgastosviajetractortbl gt WHERE vt1.idViajeTractor = gt.idViajeTractor AND vt1.claveMovimiento  in ('VU','UV')  AND gt.claveMovimiento = 'GP' AND vt1.idViajeTractor = vt.idViajeTractor LIMIT 1 ) as polizaTotal ".
                                    "FROM caChoferesTbl ch, caTractoresTbl tr, trViajesTractoresTbl vt ".
                                    "LEFT JOIN trtalonesviajestbl tv ON tv.idViajeTractor = vt.idViajeTractor ".
                                    $lsWhereStr." GROUP BY vt.idViajeTractor ";

        $rs = fn_ejecuta_query($sqlGetHistoricoViajesStr);

        $sqlGetNumDistribuidoresStr = "SELECT folio, distribuidor FROM trTalonesViajesTbl ".
                                        "WHERE idViajeTractor = ".$rs['root'][0]['idViajeTractor']." ".
                                        "AND claveMovimiento !='TX' ";

        $rsTalones = fn_ejecuta_query($sqlGetNumDistribuidoresStr);
        $dist = array();

        if(sizeof($rsTalones['root']) > 0){
            for ($nInt=0; $nInt < sizeof($rsTalones['root']); $nInt++) {
                if(!in_array($rsTalones['root'][$nInt]['distribuidor'], $dist)){
                    array_push($dist, $rsTalones['root'][$nInt]['distribuidor']);
                }
            }

            $distNum = sizeof($dist);
        } else {
            $distNum = 0;
        }

        if ($rs['records'] > 0) {
            $rs['idCombustible'] = 0;
            $rs['concepto'] = '';
            $rs['litros'] = 0;
            $selStr = "SELECT * FROM trCombustiblePendienteTbl ".
                      "WHERE claveChofer = ".$_REQUEST['trViajesTractoresClaveChoferHdn']." AND claveMovimiento = 'CP'";
            $combRs = fn_ejecuta_query($selStr);
            if ($combRs['records'] > 0) {
                $rs['idCombustible'] = $combRs['root'][0]['idCombustible'];
                $rs['concepto'] = $combRs['root'][0]['concepto'];
                $rs['litros'] = $combRs['root'][0]['litros'];
            }
        }

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['nombreChofer'] = $rs['root'][$nInt]['claveChofer']." - ".
                                                 $rs['root'][$nInt]['nombre']." ".
                                                 $rs['root'][$nInt]['apellidoPaterno']." ".
                                                 $rs['root'][$nInt]['apellidoMaterno'];

            $rs['root'][$nInt]['fechaEvento'] = date('Y-m-d', strtotime($rs['root'][$nInt]['fechaEvento']));
            $rs['root'][$nInt]['descCompania'] = $rs['root'][$nInt]['compania']." - ".$rs['root'][$nInt]['nombreCompania'];

           // $rs['root'][$nInt]['numeroDistribuidores'] = sizeof($dist);
$rs['root'][$nInt]['numeroDistribuidores'] = $rs['root'][$nInt]['numDistribuidorValidos'];
            //KILOMETROS ADICIONALES (COMPROBADOS - TABULADOS)
            $kmAdicionales = floatval($rs['root'][$nInt]['kilometrosComprobados']) - floatval($rs['root'][$nInt]['kilometrosTabulados']);
            $rs['root'][$nInt]['kilometrosAdicionales'] =  $kmAdicionales < 0 ? 0 : $kmAdicionales;

            //Obtiene y concatena todos los talones del viaje
            $sqlGetTalonesViajeStr = "SELECT folio FROM trTalonesViajesTbl ".
                                     "WHERE claveMovimiento != 'TX' ".
                                     "AND idViajeTractor = ".$rs['root'][$nInt]['idViajeTractor'];

            $rsTalones = fn_ejecuta_query($sqlGetTalonesViajeStr);
            $temp = "";

            for ($mInt=0; $mInt < sizeof($rsTalones['root']); $mInt++) {
                if ($mInt != 0) {
                  $temp .= "-";
                }
                $temp .= $rsTalones['root'][$mInt]['folio'];
            }

            $rs['root'][$nInt]['foliosTalonesViaje'] = $temp;
            
        }

        return $rs;
    }

    function getChoferesEspera(){

        $a['success'] = true;
        if($_REQUEST['trViajesTractoresClaveMovimientoHdn'] == ''){
            $a['success'] = false;
            $a['errors'] = array('id'=>'trViajesTractoresClaveMovimientoHdn', msg=>getRequerido());
            $a['errorMessage'] = getRequerido();
        }
        if($a['success'] == true){
            $sqlUpdChoferEspera = "SELECT DISTINCT te.*, vt3.idViajeTractor, ".
                                    "(SELECT tt.tractor FROM catractorestbl tt WHERE te.idtractor = tt.idtractor) AS tractor, ".
                                    "(SELECT tt.compania FROM catractorestbl tt WHERE te.idtractor = tt.idtractor) AS compania, ".
                                    "(SELECT MAX(vt4.viaje) FROM trViajesTractoresTbl vt4 ".
                                    "WHERE te.idTractor = vt4.idTractor ".
                                    "GROUP BY vt4.idtractor) AS viaje ".
                                    "FROM trEsperaChoferesTbl te ".
                                    "LEFT JOIN ".
                                    "(SELECT vt.idTractor, ".
                                    "(SELECT MAX(vt2.idViajeTractor) ".
                                    "FROM trviajestractorestbl vt2 ".
                                    "WHERE vt2.idTractor = vt.idTractor ".
                                    "GROUP BY vt2.idTractor) AS idViajeTractor ".
                                    "FROM trViajesTractoresTbl vt) AS vt3 ".
                                    "ON te.idTractor = vt3.idTractor ".
                                    "WHERE te.consecutivo = (SELECT MIN(consecutivo) ".
                                                            "FROM tresperachoferestbl te2 ".
                                                            "WHERE te2.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                                            "AND te2.claveMovimiento = '".$_REQUEST['trViajesTractoresClaveMovimientoHdn']."' ".
                                                            "AND te2.idTractor NOT IN(SELECT idTractor FROM trViajesTractoresTmp tt WHERE te2.idTractor = tt.idTractor)) ".
                                    "AND te.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                    "AND te.claveMovimiento = '".$_REQUEST['trViajesTractoresClaveMovimientoHdn']."'";

            $rs = fn_ejecuta_query($sqlUpdChoferEspera);

            //Agrega si pasa de 24 horas que está en la Lista
            $dateNow = date_create(date('Y-m-d H:i:s'));
            for ($i=0; $i < count($rs['root']) ; $i++) {
                $dateOld = date_create($rs['root'][$i]['fechaEvento']);
                $diff =  date_diff($dateNow, $dateOld);
                $rs['root'][$i]['enLista24Hrs'] = $diff->days > 0 ? '1': '0';
            }
        }
        echo json_encode($rs);

    }

    function getChoferesEsperaGeneral( $claveMovimiento = ''){
       $lsWhereStr = "WHERE te.idTractor = ct.idTractor ".
                     "AND te.centroDistribucion = cd.distribuidorCentro AND cc.compania = ct.compania ".
                     "AND ch.claveChofer = te.claveChofer ";

      if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "te.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "te.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveChoferHdn'], "te.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if($_REQUEST['trViajesTractoresMaxHdn'] != ''){
            $aCon = "";
            if($_REQUEST['trViajesTractoresCentroDistHdn'] != ''){
                $aCon = " AND te2.centroDistribucion = '".$_REQUEST['trViajesTractoresCentroDistHdn']."'";
            }
            $lsWhereStr =  fn_concatena_condicion($lsWhereStr, " te.consecutivo = (SELECT MAX(te2.consecutivo) FROM tresperachoferestbl te2 WHERE ".
                                                            "te2.idTractor NOT IN(select idTractor from trViajesTractoresTmp tt WHERE ".
                                                            "te2.idTractor = tt.idTractor)".$aCon.")");
        }else{
          if ($gb_error_filtro == 0){
          $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresConsecutivoTxt'], "te.consecutivo", 0);
          $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
          }
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresFechaHdn'], "te.fechaEvento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($claveMovimiento, "te.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        $sqlGetChoferesEsperaGeneralStr = "SELECT te.*, ct.compania, ct.tractor, ct.tipoTractor, cd.descripcionCentro, cc.descripcion, ".
                                          "ch.apellidoPaterno, ch.apellidoMaterno, ch.nombre ".
                                          "FROM tresperachoferestbl te, catractorestbl ct, cadistribuidorescentrostbl cd, ".
                                          "cacompaniastbl cc, cachoferestbl ch ".$lsWhereStr." ORDER BY (te.consecutivo) ";

        $rs = fn_ejecuta_query($sqlGetChoferesEsperaGeneralStr);
        //echo json_encode($sqlGetChoferesEsperaGeneralStr);
        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['nombreChofer'] = $rs['root'][$iInt]['claveChofer']." - ".
                                                 $rs['root'][$iInt]['nombre']." ".
                                                 $rs['root'][$iInt]['apellidoPaterno']." ".
                                                 $rs['root'][$iInt]['apellidoMaterno'];
            $rs['root'][$iInt]['descCentroDistribucion'] = $rs['root'][$iInt]['centroDistribucion']." - ".
                                                           $rs['root'][$iInt]['descripcionCentro'];
            $rs['root'][$iInt]['descCompania'] = $rs['root'][$iInt]['compania']." - ".$rs['root'][$iInt]['descripcion'];
        }

        return $rs;
    }

    function updChoferesEspera(){
        $a = array();
        $e = array();
        $a['success'] = true;

        //trViajesTractoresIdTractorNuevoHdn
        //trViajesTractoresClaveMovimientoHdn
        //trViajesTractoresIdTractorHdn
        //tiempoEsperaChoferHdn
        //tiempoEsperaActualizadoHdn

        $sqlUpdchoferEspera = "UPDATE trEsperaChoferesTbl SET claveChofer = ".$_REQUEST['tiempoEsperaChoferCmb'].
                              ", idTractor = ".$_REQUEST['tiempoEsperaTractorCmb'].
                              ", actualizado = ".$_REQUEST['tiempoEsperaActualizadoHdn'].
                              " WHERE centroDistribucion = '".$_SESSION['usuCompania']."'".
                              " AND idTractor = ".$_REQUEST['trViajesTractoresIdTractorOldHdn'].
                              " AND claveChofer = ".$_REQUEST['trViajesTractoresClaveChoferOldHdn'];

        $rs = fn_ejecuta_query($sqlUpdchoferEspera);

        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
            $a['successMessage'] = getEsperaChoferesUpdateMsg();
        } else {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdateEsperaChoferesStr;
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function addChoferEspera(){

        $a = array();
        $e = array();
        $a['success'] = true;
        if($_REQUEST['tiempoEsperaTractorCmb'] == ""){
            $e[] = array('id'=>'tiempoEsperaChoferHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['tiempoEsperaChoferCmb'] == ""){
            $e[] = array('id'=>'tiempoEsperaChoferCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true){

            $sqlMaxConsecutivo = "SELECT IFNULL(MAX(consecutivo) + 1, '1') as siguiente FROM trEsperachoferesTbl ".
                                 "WHERE centroDistribucion = '".$_SESSION['usuCompania']."';";

            $rsSiguiente = fn_ejecuta_query($sqlMaxConsecutivo);


            $sqlAddEsperaChoferStr = "INSERT INTO trEsperaChoferesTbl (centroDistribucion, idTractor, ".
                "claveChofer, consecutivo, fechaEvento, claveMovimiento, actualizado) ".
                "VALUES ('".$_SESSION['usuCompania']."','".
                $_REQUEST['tiempoEsperaTractorCmb']."','".
                $_REQUEST['tiempoEsperaChoferCmb']."','".
                $rsSiguiente['root'][0]['siguiente']."','".
                date("Y-m-d H:i:s")."','VD', 0)";

            $rs = fn_ejecuta_query($sqlAddEsperaChoferStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlAddEsperaChoferStr;
                $a['successMessage'] = getEsperaChoferesAddMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddEsperaChoferStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function addViajes(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trViajesTractoresIdTractorHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresIdTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trViajesTractoresClaveChoferHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresClaveChoferHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trViajesTractoresClaveMovimientoHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if ($_REQUEST['trViajesTractoresClaveMovimientoTalonHdn'] == "") {
            $e[] = array('id'=>'trViajesTractoresClaveMovimientoTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if ($_REQUEST['trViajesTractoresTipoDoctoHdn'] == "") {
            $e[] = array('id'=>'trViajesTractoresTipoDoctoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if ($_REQUEST['trViajesTractoresCompaniaHdn'] == "") {
            $e[] = array('id'=>'trViajesTractoresCompaniaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if ($_REQUEST['trViajesTractoresTipoTalonHdn'] == "") {
            $e[] = array('id'=>'trViajesTractoresTipoTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        //TALONES -- Inserción Masiva
        $distArr = explode("|", substr($_REQUEST['trViajesTractoresDistribuidorHdn'], 0, -1));
        if (in_array("", $distArr)) {
            $e[] = array('id'=>'trViajesTractoresDistribuidorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $direccionArr = explode("|", substr($_REQUEST['trViajesTractoresDireccionHdn'], 0, -1));
        if (in_array("", $direccionArr)) {
            $e[] = array('id'=>'trViajesTractoresDireccionHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $destinoArr = explode("|", substr($_REQUEST['trViajesTractoresDestinoHdn'], 0, -1));
        if (in_array("", $destinoArr)) {
            $e[] = array('id'=>'trViajesTractoresDestinoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $unidadesArr = explode("|", substr($_REQUEST['trViajesTractoresUnidadesHdn'], 0, -1));
        if (in_array("", $unidadesArr)) {
            $e[] = array('id'=>'trViajesTractoresUnidadesHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $remitenteArr = explode("|", substr($_REQUEST['trViajesTractoresRemitenteHdn'], 0, -1));
        if (in_array("", $remitenteArr)) {
            $e[] = array('id'=>'trViajesTractoresRemitenteHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $kilometrosArr = explode("|", substr($_REQUEST['trViajesTractoresKmTabuladosTxt'], 0, -1));
        if (in_array("", $kilometrosArr)) {
            $e[] = array('id'=>'trViajesTractoresKmTabuladosTxt','msg'=>getRequerido());
            $a['success'] = false;
        }
        //Checar el destino más largo
        $plazaDestino = -1;
        $kmTabulados = 0;
        for ($iInt=0; $iInt < sizeof($kilometrosArr); $iInt++) {
            if ($kmTabulados < $kilometrosArr[$iInt]) {
                $plazaDestino = $destinoArr[$iInt];
                $kmTabulados = $kilometrosArr[$iInt];
            }
        }

        if($a['success'] == true){
            if($plazaDestino > 0){
                $sqlAddViajeTractorStr = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                         "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                                         "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                         "VALUES(".
                                            $_REQUEST['trViajesTractoresIdTractorHdn'].",".
                                            $_REQUEST['trViajesTractoresClaveChoferHdn'].",".
                                            "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                            $plazaDestino.",".
                                            "'".$_SESSION['usuCompania']."',".
                                            "IFNULL((SELECT viaje FROM trViajesTractoresTbl vt1 ".
                                                "WHERE vt1.viaje = ".
                                                    "(SELECT MAX(vt2.viaje) ".
                                                        "FROM trViajesTractoresTbl vt2 WHERE vt2.idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'].")".
                                                "AND vt1.idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'].")+1,1),".
                                            "'".date("Y-m-d H:i:s")."',".
                                            replaceEmptyDec($kmTabulados).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmComprobadosTxt']).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmSinUnidadTxt']).",".
                                            array_sum($unidadesArr).",".
                                            //sizeof($unidadesArr)
                                            sizeof(array_unique($distArr,SORT_STRING)).",".
                                            replaceEmptyNull($_REQUEST['trViajesTractoresIdViajePadreHdn']).",".
                                            "'".$_REQUEST['trViajesTractoresClaveMovimientoHdn']."',".
                                            "'".$_SESSION['idUsuario']."',".
                                            "'".$_SERVER['REMOTE_ADDR']."')";
                $rs = fn_ejecuta_query($sqlAddViajeTractorStr);
                $idViajeInt = mysql_insert_id();

                if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    //Obtener el folio
                    $sqlGetFolioStr = "SELECT sum(folio + 1) as folio FROM trFoliosTbl ".
                                      //"WHERE tipoDocumento='".$_REQUEST['trViajesTractoresTipoDoctoHdn']."' ".
                                      "WHERE tipoDocumento = 'TR' ".
                                      "AND centroDistribucion='".$_SESSION['usuCompania']."' ".
                                      "AND compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."'";

                    $rs = fn_ejecuta_query($sqlGetFolioStr);
                    $folio = $rs['root'][0]['folio'];

                    if(isset($folio) && $folio != ""){
                        for ($iInt=0; $iInt < sizeof($unidadesArr); $iInt++) {
                            if ((integer) $folio < 9) {
                                $folio = '0'.(string)((integer)$folio+1);
                            } else {
                                $folio = (string)((integer)$folio+1);
                            }

                            $sqlAddTalonesViajeStr = "INSERT INTO trTalonesViajesTbl (distribuidor,folio,idViajeTractor,companiaRemitente,".
                                                    "idPlazaOrigen,idPlazaDestino,direccionEntrega,centroDistribucion,tipoTalon,".
                                                    "fechaEvento,observaciones,numeroUnidades,importe,seguro,tarifaCobrar,kilometrosCobrar,".
                                                    "impuesto,retencion,claveMovimiento,tipoDocumento) VALUES(".
                                                    "'".$distArr[$iInt]."',".
                                                    "'".$folio."',".
                                                    $idViajeInt.",".
                                                    "'".$remitenteArr[$iInt]."',".
                                                    "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                        "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                                    $destinoArr[$iInt].",".
                                                    $direccionArr[$iInt].",".
                                                    "'".$_SESSION['usuCompania']."',".
                                                    "'".$_REQUEST['trViajesTractoresTipoTalonHdn']."',".
                                                    "'".date("Y-m-d")."',".
                                                    "'".$observacionesArr[$iInt]."',".
                                                    $unidadesArr[$iInt].",".
                                                    replaceEmptyDec($_REQUEST['trViajesTractoresImporteTxt']).",".
                                                    replaceEmptyDec($_REQUEST['trViajesTractoresSeguroTxt']).",".
                                                    replaceEmptyDec($_REQUEST['trViajesTractoresTarifaCobrarTxt']).",".
                                                    replaceEmptyDec($_REQUEST['trViajesTractoresKmCobrarTxt']).",".
                                                    replaceEmptyDec($_REQUEST['trViajesTractoresImpuestoTxt']).",".
                                                    replaceEmptyDec($_REQUEST['trViajesTractoresRetencionTxt']).",".
                                                    "'".$_REQUEST['trViajesTractoresClaveMovimientoTalonHdn']."',".
                                                    "'".$_REQUEST['trViajesTractoresTipoDoctoHdn']."')";

                            $rs = fn_ejecuta_query($sqlAddTalonesViajeStr);

                            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                                $sqlUpdFolioStr = "UPDATE trFoliosTbl ".
                                                  "SET folio = '".$folio."' ".
                                                  "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                                  "AND compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ".
                                                  "AND tipoDocumento = 'TR' ";
                                                  //"AND tipoDocumento = '".$_REQUEST['trViajesTractoresTipoDoctoHdn']."'";

                                $rs = fn_ejecuta_query($sqlUpdFolioStr);

                                if($_REQUEST['trViajesTractoresVinHdn'] !== ''){
                                    $idViajeArr = explode('|', substr($_REQUEST['trViajesTractoresIdViajeHdn'], 0, -1));
                                    $movimientoArr = explode('|', substr($_REQUEST['trViajesTractoresClaveMovimientoHdn'], 0, -1));
                                    $vinArr = explode('|', substr($_REQUEST['trViajesTractoresVinHdn'], 0, -1));

                                  for($nInt = 0; $nInt < sizeof($vinArr);$nInt++){
                                      $sqlAddUnidadesEmbarcadasStr = "INSERT INTO trUnidadesEmbarcadasTbl ".
                                                            "(distribuidorCentro, idViajeTractor, vin, fechaEmbarque, claveMovimiento)".
                                                            "VALUES(".
                                                            "'".$_SESSION['usuCompania']."',".
                                                            $idViajeInt.",".
                                                            replaceEmptyNull("'".$vinArr[$nInt]."'").",".
                                                            "'".date("Y-m-d H:i:s")."',".
                                                            "'".$_REQUEST['trViajesTractoresClaveMovimientoHdn']."')";

                                      $rs = fn_ejecuta_query($sqlAddUnidadesEmbarcadasStr);
                                  }
                                }

                                if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                                    $a['successMessage'] = getPreViajeSuccessMsg();
                                    $a['sql'] = $sqlAddTalonesViajeStr;
                                } else {
                                    $a['success'] = false;
                                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $a['sql'];
                                }
                            } else {
                                $a['success'] = false;
                                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $a['sql'];
                                break;
                            }
                        }
                       if($_REQUEST['trViajesTractoresVinHdn'] !== ''){
                              //$idViajeArr = explode('|', substr($_REQUEST['trViajesTractoresIdViajeHdn'], 0, -1));
                              //$movimientoArr = explode('|', substr($_REQUEST['trViajesTractoresClaveMovimientoHdn'], 0, -1));
                              $vinArr = explode('|', substr($_REQUEST['trViajesTractoresVinHdn'], 0, -1));

                              for($nInt = 0; $nInt < sizeof($vinArr);$nInt++){
                                  $sqlAddUnidadesEmbarcadasStr = "INSERT INTO trUnidadesEmbarcadasTbl ".
                                                        "(centroDistribucion, idViajeTractor, vin, fechaEmbarque, claveMovimiento)".
                                                        "VALUES(".
                                                        "'".$_SESSION['usuCompania']."',".
                                                        $idViajeInt.",".
                                                        replaceEmptyNull("'".$vinArr[$nInt]."'").",".
                                                        "'".date("Y-m-d H:i:s")."',".
                                                        "'".$_REQUEST['trViajesTractoresClaveMovimientoHdn']."')";

                                  $rs = fn_ejecuta_query($sqlAddUnidadesEmbarcadasStr);
                              }
                            }
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = "Folio no existente";
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddViajeTractorStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = "Error al obtener la Plaza Destino";
            }
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function updEstatusViaje($idViaje, $claveMovimiento){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idViaje == ""){
            $e[] = array('id'=>'IdViajeHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($claveMovimiento == ""){
            $e[] = array('id'=>'ClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success']){
            $sqlUpdEstatusViajeStr = "UPDATE trViajesTractoresTbl SET ".
                                        "claveMovimiento = '".$claveMovimiento."' ".
                                        "WHERE idViajeTractor = ".$idViaje;

            fn_ejecuta_query($sqlUpdEstatusViajeStr);

            $AddGasto = "INSERT INTO  trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio,fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip) ".
                        "SELECT idViajeTractor, '0' AS concepto, '".$_SESSION['usuCompania']."' as centroDistribucion, '0' as folio, now() as fechaEvento, null as cuentaContable, null as mesAfectacion, '0.00' as importe, concat('viajeLiberado: ',now()) as observaciones, 'GU' as claveMovimiento, '".$_SESSION['idUsuario']."' as usuario, '".$_SERVER['REMOTE_ADDR']."' as ip ".
                        "FROM trviajestractorestbl ".
                        "WHERE idViajeTractor = ".$idViaje;
            $rs = fn_ejecuta_Add($AddGasto);

            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] = getViajeEstatusUpdMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdEstatusViajeStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function addViajeVacio(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap484TractorHdn'] == ""){
            $e[] = array('id'=>'trap484TractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484ChoferHdn'] == ""){
            $e[] = array('id'=>'trap484ChoferHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484ClaveMovimientoHdn'] == ""){
            $e[] = array('id'=>'trap484ClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484DestinoHdn'] == ""){
            $e[] = array('id'=>'trap484DestinoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484CompaniaHdn'] == ""){
            $e[] = array('id'=>'trap484CompaniaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484TipoDoctoHdn'] == ""){
            $e[] = array('id'=>'trap484TipoDoctoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484GastosClaveMovimientoHdn'] == ""){
            $e[] = array('id'=>'trap484GastosClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484ImportesHdn'] == ""){
            $e[] = array('id'=>'trap484ImportesHdn','msg'=>getRequerido());
            $a['success'] = false;
        }


        if($a['success'] == true){
            $sqlAddViajeVacioStr = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                     "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                                     "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                     "VALUES(".
                                        $_REQUEST['trap484TractorHdn'].",".
                                        $_REQUEST['trap484ChoferHdn'].",".
                                        "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                            "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                        "(SELECT dc2.idPlaza FROM caDistribuidoresCentrosTbl dc2 ".
                                            "WHERE dc2.distribuidorCentro = '".$_REQUEST['trap484DestinoHdn']."'),".
                                        "'".$_SESSION['usuCompania']."',".
                                        "IFNULL((SELECT viaje FROM trViajesTractoresTbl vt1 ".
                                            "WHERE vt1.viaje = ".
                                                "(SELECT MAX(vt2.viaje) ".
                                                    "FROM trViajesTractoresTbl vt2 WHERE vt2.idTractor=vt1.idTractor)".
                                            "AND vt1.idTractor = ".$_REQUEST['trap484TractorHdn'].")+1,1),".
                                        "'".date("Y-m-d H:i:s")."',".
                                        replaceEmptyDec($_REQUEST['trap484KmTabuladosTxt']).",".
                                        replaceEmptyDec($_REQUEST['trap484KmComprobadosTxt']).",".
                                        replaceEmptyDec($_REQUEST['trap484KmSinUnidadTxt']).",".
                                        "0,".
                                        "0,".
                                        replaceEmptyNull($_REQUEST['trap484IdViajePadreHdn']).",".
                                        "'".$_REQUEST['trap484ClaveMovimientoHdn']."',".
                                        "'".$_SESSION['usuario']."',".
                                        "'".$_SERVER['REMOTE_ADDR']."')";

            $rs = fn_ejecuta_query($sqlAddViajeVacioStr);
            $idViajeInt = mysql_insert_id();

            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {


                $data = addGastosViajeTractor($idViajeInt, $_REQUEST['trap484CompaniaHdn'],$_REQUEST['trap484ConceptosHdn'], $_REQUEST['trap484ImportesHdn'], $_REQUEST['trap484ObservacionesTxa'],$_REQUEST['trap484GastosClaveMovimientoHdn'], $_REQUEST['trap484TipoDoctoHdn']);
                $a['folio'] = $data['folio'];

                if ($data['success'] == true) {
                    $a['successMessage'] = getGastosViajeVacioSuccessMsg();
                } else {
                    $a['success'] = $data['success'];
                    $a['errorMessage'] = $data['errorMessage'];
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddViajeVacioStr;
            }
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function addViajeAcompanante(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap484TractorHdn'] == ""){
            $e[] = array('id'=>'trap484TractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484ChoferHdn'] == ""){
            $e[] = array('id'=>'trap484ChoferHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484ClaveMovimientoHdn'] == ""){
            $e[] = array('id'=>'trap484ClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484OrigenHdn'] == ""){
            $e[] = array('id'=>'trap484OrigenHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484DestinoHdn'] == ""){
            $e[] = array('id'=>'trap484DestinoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484ViajeHdn'] == ""){
            $e[] = array('id'=>'trap484ViajeHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484CompaniaHdn'] == ""){
            $e[] = array('id'=>'trap484CompaniaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484TipoDoctoHdn'] == ""){
            $e[] = array('id'=>'trap484TipoDoctoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484GastosClaveMovimientoHdn'] == ""){
            $e[] = array('id'=>'trap484GastosClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap484ImportesHdn'] == ""){
            $e[] = array('id'=>'trap484ImportesHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success'] == true){
            $sqlAddViajeAcompananteStr = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                     "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                                     "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                     "VALUES(".
                                        $_REQUEST['trap484TractorHdn'].",".
                                        $_REQUEST['trap484ChoferHdn'].",".
                                        $_REQUEST['trap484OrigenHdn'].",".
                                        $_REQUEST['trap484DestinoHdn'].",'".
                                        $_SESSION['usuCompania']."',".
                                        $_REQUEST['trap484ViajeHdn'].",'".
                                        date("Y-m-d H:i:s")."',".
                                        replaceEmptyDec($_REQUEST['trap484KmTabuladosTxt']).",".
                                        replaceEmptyDec($_REQUEST['trap484KmComprobadosTxt']).",".
                                        replaceEmptyDec($_REQUEST['trap484KmSinUnidadTxt']).",".
                                        replaceEmptyDec($_REQUEST['trap484numeroUnidadesTxt']).",".
                                        replaceEmptyDec($_REQUEST['trap484numeroRepartosTxt']).",".
                                        replaceEmptyNull($_REQUEST['trap484IdViajePadreHdn']).",".
                                        "'".$_REQUEST['trap484ClaveMovimientoHdn']."',".
                                        "'".$_SESSION['usuCompania']."',".
                                        "'".$_SERVER['REMOTE_ADDR']."')";

            $rs = fn_ejecuta_query($sqlAddViajeAcompananteStr);
            $idViajeInt = mysql_insert_id();

            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {

                $data = addGastosViajeTractor($idViajeInt, $_REQUEST['trap484CompaniaHdn'],$_REQUEST['trap484ConceptosHdn'], $_REQUEST['trap484ImportesHdn'], '',$_REQUEST['trap484GastosClaveMovimientoHdn'], $_REQUEST['trap484TipoDoctoHdn']);
                $a['folio'] = $data['folio'];

                if ($data['success'] == true) {
                    $a['successMessage'] = getGastosViajeAcompananteSuccessMsg();
                } else {
                    $a['success'] = $data['success'];
                    $a['errorMessage'] = $data['errorMessage'];
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddViajeAcompananteStr;
            }
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function updTalones(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap446IdViajeHdn'] == ""){
            $e[] = array('id'=>'trap446IdViajeHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap446TipoTalonHdn'] == ""){
            $e[] = array('id'=>'trap446TipoTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap446CompaniaHdn'] == ""){
            $e[] = array('id'=>'trap446CompaniaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        $distArr = explode('|', substr($_REQUEST['trap446DistribuidorHdn'], 0, -1));
        if(in_array("", $distArr)){
            $e[] = array('id'=>'trap446DistribuidorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $remitenteArr = explode('|', substr($_REQUEST['trap446RemitenteHdn'], 0, -1));
        if(in_array("", $remitenteArr)){
            $e[] = array('id'=>'trap446RemitenteHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $direccionArr = explode('|', substr($_REQUEST['trap446DireccionEntregaHdn'], 0, -1));
        if(in_array("", $direccionArr)){
            $e[] = array('id'=>'trap446DireccionEntregaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $numUnidadesArr = explode('|', substr($_REQUEST['trap446NumeroUnidadesHdn'], 0, -1));
        if(in_array("", $numUnidadesArr)){
            $e[] = array('id'=>'trap446NumeroUnidadesHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $claveMovArr = explode('|', substr($_REQUEST['trap446ClaveMovimientoHdn'], 0, -1));
        if(in_array("", $claveMovArr)){
            $e[] = array('id'=>'trap446ClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $tipoDocumentoArr = explode('|', substr($_REQUEST['trap446TipoDocumentoHdn'], 0, -1));
        if(in_array("", $tipoDocumentoArr)){
            $e[] = array('id'=>'trap446TipoDocumentoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if ($a['success'] == true) {
          $idTalonArr = explode('|', substr($_REQUEST['trap446IdTalonHdn'], 0, -1));
          $destinoArr = explode('|', substr($_REQUEST['trap446PlazaDestinoHdn'], 0, -1));
          $importeArr = explode('|', substr($_REQUEST['trap446ImportesHdn'], 0, -1));
          $seguroArr = explode('|', substr($_REQUEST['trap446SeguroHdn'], 0, -1));
          $tarifaCobrarArr = explode('|', substr($_REQUEST['trap446TarifaCobrarHdn'], 0, -1));
          $kmCobrarArr = explode('|', substr($_REQUEST['trap446KmCobrarHdn'], 0, -1));
          $impuestoArr = explode('|', substr($_REQUEST['trap446ImpuestoHdn'], 0, -1));
          $retencionArr = explode('|', substr($_REQUEST['trap446RetencionHdn'], 0, -1));
          $observacionesArr = explode('|', substr($_REQUEST['trap446ObservacionesHdn'], 0, -1));

          for ($nInt=0; $nInt < sizeof($distArr); $nInt++) {

              if ($idTalonArr[$nInt] != "" && $idTalonArr[$nInt] > 0) {
                  $sqlUpdTalonStr = "UPDATE trTalonesViajesTbl ".
                                    "SET companiaRemitente = '".$remitenteArr[$nInt]."',".
                                    "direccionEntrega = ".$direccionArr[$nInt].",".
                                    "tipoTalon = '".$_REQUEST['trap446TipoTalonHdn']."',".
                                    "observaciones = '".$observacionesArr[$nInt]."',".
                                    "numeroUnidades = ".$numUnidadesArr[$nInt].",".
                                    "importe = ".replaceEmptyDec($importeArr[$nInt]).",".
                                    "seguro = ".replaceEmptyDec($seguroArr[$nInt]).",".
                                    "tarifaCobrar = ".replaceEmptyDec($tarifaCobrarArr[$nInt]).",".
                                    "kilometrosCobrar = ".replaceEmptyDec($kmCobrarArr[$nInt]).",".
                                    "impuesto = ".replaceEmptyDec($impuestoArr[$nInt]).",".
                                    "retencion = ".replaceEmptyDec($retencionArr[$nInt]).",".
                                    "claveMovimiento = '".$claveMovArr[$nInt]."',".
                                    "tipoDocumento = '".$tipoDocumentoArr[$nInt]."' ".
                                    "WHERE idTalon = ".$idTalonArr[$nInt];

                  $rs = fn_ejecuta_query($sqlUpdTalonStr);

                  if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {

                  } else {
                      $a['success'] = false;
                      $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddTalonesViajeStr;
                      break;
                  }

              } else {
                  //Obtener el folio
                  $sqlGetFolioStr = "SELECT sum(folio + 1) as folio FROM trFoliosTbl ".
                                    "WHERE tipoDocumento='TR' ".
                                    //"WHERE tipoDocumento='".$tipoDocumentoArr[$nInt]."' ".
                                    "AND centroDistribucion='".$_SESSION['usuCompania']."' ".
                                    "AND compania = '".$_REQUEST['trap446CompaniaHdn']."'";

                  $rs = fn_ejecuta_query($sqlGetFolioStr);
                  $folio = $rs['root'][0]['folio'];

                  if(isset($folio) && $folio != ""){
                    if ((integer) $folio < 9) {
                        $folio = '0'.(string)((integer)$folio+1);
                  } else {
                      $folio = (string)((integer)$folio+1);
                  }

                  $sqlAddTalonesViajeStr = "INSERT INTO trTalonesViajesTbl (distribuidor,folio,idViajeTractor,companiaRemitente,".
                                                    "idPlazaOrigen,idPlazaDestino,direccionEntrega,centroDistribucion,tipoTalon,".
                                                    "fechaEvento,observaciones,numeroUnidades,importe,seguro,tarifaCobrar,kilometrosCobrar,".
                                                    "impuesto,retencion,claveMovimiento,tipoDocumento) VALUES(".
                                                    "'".$distArr[$nInt]."',".
                                                    "'".$folio."',".
                                                    $_REQUEST['trap446IdViajeHdn'].",".
                                                    "'".$remitenteArr[$nInt]."',".
                                                    "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                        "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                                    "(SELECT dc2.idPlaza FROM caDistribuidoresCentrosTbl dc2 ".
                                                        "WHERE dc2.distribuidorCentro = '".$distArr[$nInt]."'),".
                                                    $direccionArr[$nInt].",".
                                                    "'".$_SESSION['usuCompania']."',".
                                                    "'".$_REQUEST['trap446TipoTalonHdn']."',".
                                                    "'".date("Y-m-d")."',".
                                                    "'".$observacionesArr[$nInt]."',".
                                                    $numUnidadesArr[$nInt].",".
                                                    replaceEmptyDec($importeArr[$nInt]).",".
                                                    replaceEmptyDec($seguroArr[$nInt]).",".
                                                    replaceEmptyDec($tarifaCobrarArr[$nInt]).",".
                                                    replaceEmptyDec($kmCobrarArr[$nInt]).",".
                                                    replaceEmptyDec($impuestoArr[$nInt]).",".
                                                    replaceEmptyDec($retencionArr[$nInt]).",".
                                                    "'".$claveMovArr[$nInt]."',".
                                                    "'".$tipoDocumentoArr[$nInt]."')";

                    $rs = fn_ejecuta_query($sqlAddTalonesViajeStr);

                    if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $sqlUpdFolioStr = "UPDATE trFoliosTbl ".
                                          "SET folio = '".$folio."' ".
                                          "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                          "AND compania = '".$_REQUEST['trap446CompaniaHdn']."' ".
                                          "AND tipoDocumento = 'TR' ";
                                          //"AND tipoDocumento = '".$tipoDocumentoArr[$nInt]."'";

                        $rs = fn_ejecuta_query($sqlUpdFolioStr);
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddTalonesViajeStr;
                        break;
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlGetFolioStr;
                    break;
                }
              }
          }
          if ($a['success'] == true) {
              $a['successMessage'] = getTalonesViajeUpdMsg();
          }
        } else {
          $a['errorMessage'] = getErrorRequeridos();
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function modificacionTalones(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap446IdViajeHdn'] == ""){
            $e[] = array('id'=>'trap446IdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['trap446CentroDistHdn'] == ""){
            $e[] = array('id'=>'trap446CentroDistHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($_REQUEST['trap446ChoferHdn'] == ""){
            $e[] = array('id'=>'trap446ChoferHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['trap446IdTractorHdn'] == ""){
            $e[] = array('id'=>'trap446IdTractorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }


        //No es requerido el id, ni el folio ni clave del talon porque hay talones nuevos sin estos datos
        $idTalonArr = explode('|', substr($_REQUEST['trap446IdTalonHdn'], 0, -1));
        $folioArr = explode('|', substr($_REQUEST['trap446FolioHdn'], 0, -1));
        $claveMovArr = explode('|', substr($_REQUEST['trap446ClaveMovimientoHdn'], 0, -1));
        $observacionesArr = explode('|', substr($_REQUEST['trap446ObservacionesHdn'], 0, -1));
        $detenidasArr = explode('|', substr($_REQUEST['trap446TalonDetenidasHdn'], 0, -1));
        $detenidasServEspArr = explode('|', substr($_REQUEST['trap446TalonDetenidasServEspHdn'], 0, -1));


        $distArr = explode('|', substr($_REQUEST['trap446DistribuidorHdn'], 0, -1));
        if(in_array('', $distArr)){
            $e[] = array('id'=>'trap446DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $centroTalonArr = explode('|', substr($_REQUEST['trap446CentrosTalonesHdn'], 0, -1));
        if(in_array('', $centroTalonArr)){
            $e[] = array('id'=>'trap446CentrosTalonesHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $plazaDestinoArr = explode('|', substr($_REQUEST['trap446PlazaDestinoHdn'], 0, -1));
        if(in_array('', $plazaDestinoArr)){
            $e[] = array('id'=>'trap446PlazaDestinoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $numUnidadesArr = explode('|', substr($_REQUEST['trap446NumeroUnidadesHdn'], 0, -1));
        if(in_array('', $numUnidadesArr)){
            $e[] = array('id'=>'trap446NumeroUnidadesHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $direccionArr = explode('|', substr($_REQUEST['trap446DireccionEntregaHdn'], 0, -1));
        if(in_array('', $direccionArr)){
            $e[] = array('id'=>'trap446DireccionEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $remitenteArr = explode('|', substr($_REQUEST['trap446RemitenteHdn'], 0, -1));
        if(in_array('', $remitenteArr)){
            $e[] = array('id'=>'trap446RemitenteHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $tipoTalonArr = explode('|', substr($_REQUEST['trap446TipoTalonHdn'], 0, -1));
        if(in_array('', $tipoTalonArr)){
            $e[] = array('id'=>'trap446TipoTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $tarifaUnidadesArr = explode('|', substr($_REQUEST['trap446TarifaUnidadesTalonHdn'], 0, -1));
        if(in_array('', $tarifaUnidadesArr)){
            $e[] = array('id'=>'trap446TarifaUnidadesTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        //Obtiene los kilometros tabulados si es que se encuentran registrados
        //Para verificar si se puede realizar el viaje
        if($a['success']){
            $kmArr = array();
            $kilometrosActuales = floatval($_REQUEST['trap446KmTabuladosHdn']);

            if($_REQUEST['trap446CentroDistActualHdn'] == $_REQUEST['trap446CentroDistHdn']){
                for ($i=0; $i < sizeof($centroTalonArr); $i++) {
                    // SI TALON DEL CENTRO DE ORIGEN DEL VIAJE
                    $sqlGetKmTabuladosStr = "SELECT kp.kilometros, kp.idPlazaDestino FROM caKilometrosPlazaTbl kp ".
                                            "WHERE kp.idPlazaOrigen = (SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                            "WHERE dc.distribuidorCentro = '".$_REQUEST['trap446CentroDistHdn']."') ".
                                            "AND kp.idPlazaDestino = (SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                            "WHERE dc.distribuidorCentro = '".$distArr[$i]."') ";

                    $rs = fn_ejecuta_query($sqlGetKmTabuladosStr);


                    if(sizeof($rs['root']) > 0){
                        $kmArr[$rs['root'][0]['idPlazaDestino']] = $rs['root'][0]['kilometros'];
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = getNoKmPlazaRegistradoMsg($_REQUEST['trap446CentroDistHdn'], $distArr[$i]);
                        break;
                    }
                }

                $kmTabulados = 0;
                $idPlazaDestino = 0;

                foreach ($kmArr as $plaza => $km) {
                    if(floatval($km) > $kmTabulados) {
                        $kmTabulados = floatval($km);
                        $idPlazaDestino = $plaza;
                    }
                }

            } else {
                for ($i=0; $i < sizeof($centroTalonArr); $i++) {
                    // SI TALON NO ES DEL CENTRO DEL ORIGEN DEL VIAJE
                    if($folioArr[$i] == ""){
                            $sqlGetKmTabuladosStr = "SELECT kp.kilometros, kp.idPlazaDestino, ".
                                                    "(SELECT 1 FROM caKilometrosPlazaTbl kp2 WHERE kp2.idPlazaOrigen = (".
                                                    "SELECT dc2.idPlaza FROM caDistribuidoresCentrosTbl dc2 ".
                                                    "WHERE dc2.distribuidorCentro = '".$_REQUEST['trap446CentroDistHdn']."') ".
                                                    "AND kp2.idPlazaDestino = kp.idPlazaDestino ".
                                                    ") AS kmOrigenViaje ".
                                                    "FROM caKilometrosPlazaTbl kp ".
                                                    "WHERE kp.idPlazaOrigen = (SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                    "WHERE dc.distribuidorCentro = '".$_REQUEST['trap446CentroDistActualHdn']."') ".
                                                    "AND kp.idPlazaDestino = (SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                    "WHERE dc.distribuidorCentro = '".$distArr[$i]."') ";
                            //echo "$sqlGetKmTabuladosStr<br>";
                            $rs = fn_ejecuta_query($sqlGetKmTabuladosStr);
                            if(sizeof($rs['root']) > 0){
                                if($rs['root'][0]['kmOrigenViaje'] != ""){
                                    $kmArr[$rs['root'][0]['idPlazaDestino']] = $rs['root'][0]['kilometros'];
                                } else {
                                    $a['success'] = false;
                                    $a['errorMessage'] = getNoKmPlazaRegistradoMsg($_REQUEST['trap446CentroDistHdn'], $distArr[$i]);
                                    break;
                                }
                            } else {
                                $a['success'] = false;
                                $a['errorMessage'] = getNoKmPlazaRegistradoMsg($_REQUEST['trap446CentroDistActualHdn'], $distArr[$i]);
                                break;
                            }                           
                    }                                                            
                }

                $kmTabulados = 0;
                $idPlazaDestino = 0;

                foreach ($kmArr as $plaza => $km) {
                    if(floatval($km) > $kmTabulados) {
                        $kmTabulados = floatval($km);
                        $idPlazaDestino = $plaza;
                    }
                }

                //$kmTabulados += $kilometrosActuales;
            }
        } else {
            $a['errors'] = $e;
        }

        //Si el tractor es numeracion menor a 40 y su ultimo viaje
        //estaba pagado o estaba cancelado se le crea un viaje nuevo
        if($a['success'] && $_REQUEST['trap446EsPlataformaHdn'] != '' && $_REQUEST['trap446EsPlataformaHdn'] != 0){
            $sqlCreateViajeMenor40Str = "INSERT INTO trViajesTractoresTbl (idTractor, claveChofer, idPlazaOrigen, idPlazaDestino, ".
                                            "centroDistribucion, viaje, fechaEvento, kilometrosTabulados, kilometrosComprobados, ".
                                            "kilometrosSinUnidad, numeroUnidades, numeroRepartos, claveMovimiento, usuario, ip) ".
                                        "SELECT idTractor, claveChofer, idPlazaOrigen, idPlazaDestino, centroDistribucion, ".
                                            "viaje+1, '".date("Y-m-d H:i:s")."', 0, 0, 0, 0, 0, 'VF', ".
                                            "'".$_SESSION['usuario']."', '".$_SERVER['REMOTE_ADDR']."' ".
                                            "FROM trViajesTractoresTbl ".
                                            "WHERE idViajeTractor = (SELECT MAX(idViajeTractor) FROM trViajesTractoresTbl ".
                                                "WHERE idTractor = ".$_REQUEST['trap446IdTractorHdn'].")";

            fn_ejecuta_query($sqlCreateViajeMenor40Str);

            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlCreateViajeMenor40Str;
            }
        }

        if($a['success']){
            //Se usa para guardar los ID de los talones nuevos
            $talonesNuevos = array();
            $idViaje = $_REQUEST['trap446IdViajeHdn'];
            $numeroTotalUnidades = 0;
            $foliosImpresion = "";
//var_dump($numUnidadesArr);
            for ($nInt=0; $nInt < sizeof($idTalonArr); $nInt++) {
                if($folioArr[$nInt] == ""){
                    //Obtiene el folio siguiente
                    $sqlGetFolioStr = "SELECT sum(folio + 1) AS folio FROM trfoliostbl ".
                                        "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                        "AND compania = '".$_REQUEST['trap446CompaniaHdn']."' ".
                                        "AND tipoDocumento = 'TR' ";
                    $rs = fn_ejecuta_query($sqlGetFolioStr);
                    $folio = $rs['root'][0]['folio'];

                    if($folio != '' && $folio != null){

                        //Si los talones son para el tractor 12 se hacen actualizaciones a las observaciones
                        if($_REQUEST['trap446NumTractorHdn'] == '12'){
                            if($detenidasArr[$nInt] == '1'){
                                $observacionesArr[$nInt] = "UNIDADES DETENIDAS";
                            } else {
                                $observacionesArr[$nInt] = "ENTREGADAS A REPRESENTANTE";
                            }
                        } else {
                            //Si no es para el 12 entonces se checa
                            //Si el talón lleva solo 13 o 00 se agrega a las observaciones
                            if($tarifaUnidadesArr[$nInt] == '13'){
                                //Se revisa si alguna vez estuvo DETENIDA
                                if($detenidasServEspArr[$nInt] == '1'){
                                    $observacionesArr[$nInt] = "UNIDAD SER ESP DETENIDA. ".date("Y-m-d H:i:s");
                                } else {
                                    $observacionesArr[$nInt] = "ESP. ".date("Y-m-d H:i:s");
                                }
                            } else if($tarifaUnidadesArr[$nInt] == '00'){
                                $observacionesArr[$nInt] = "CAMB DEST. ".date("Y-m-d H:i:s");
                            } else {
                                $observacionesArr[$nInt] = date("Y-m-d H:i:s");
                            }
                        }

                        //Se obtiene el ultimo id de viaje
                        $sqlGetIdViajeStr = "SELECT MAX(idViajeTractor) AS idViajeTractor FROM trViajesTractoresTbl ".
                                                "WHERE idTractor = ".$_REQUEST['trap446IdTractorHdn'];

                        $rs = fn_ejecuta_query($sqlGetIdViajeStr);
                        $idViaje = $rs['root'][0]['idViajeTractor'];

                        //Se inserta el talon nuevo de los datos del talon temporal
                        $sqlAddTalonesViajeStr = "INSERT INTO trTalonesViajesTbl (distribuidor,folio,idViajeTractor,companiaRemitente,".
                                                "idPlazaOrigen,idPlazaDestino,direccionEntrega,centroDistribucion,tipoTalon,".
                                                "fechaEvento,observaciones,numeroUnidades,importe,seguro,tarifaCobrar,kilometrosCobrar,".
                                                "impuesto,retencion,claveMovimiento,tipoDocumento) VALUES(".
                                                "'".$distArr[$nInt]."',".
                                                $folio.",".
                                                $idViaje.",".
                                                "'".$remitenteArr[$nInt]."',".
                                                "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                    "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                                $plazaDestinoArr[$nInt].",".
                                                $direccionArr[$nInt].",".
                                                "'".$_SESSION['usuCompania']."',".
                                                "'".$tipoTalonArr[$nInt]."',".
                                                "'".date("Y-m-d H:i:s")."',".
                                                "'".$observacionesArr[$nInt]."',".
                                                $numUnidadesArr[$nInt].",".
                                                replaceEmptyDec($_REQUEST['trap446ImporteTxt']).",".
                                                replaceEmptyDec($_REQUEST['trap446SeguroTxt']).",".
                                                replaceEmptyDec($_REQUEST['trap446TarifaCobrarTxt']).",".
                                                $kmTabulados.",".
                                                replaceEmptyDec($_REQUEST['trap446ImpuestoTxt']).",".
                                                replaceEmptyDec($_REQUEST['trap446RetencionTxt']).",".
                                                "'TF',".
                                                "'TF')";
//echo "$sqlAddTalonesViajeStr<br>";
                        fn_ejecuta_query($sqlAddTalonesViajeStr);



                        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                            //Se guarda el folio para solo imprimir talones que se crearon en la modificacion
                            $foliosImpresion .= "'".$folio."',";

                            //Se actualiza el folio
                            $sqlUpdFolioStr = "UPDATE trFoliosTbl ".
                                              "SET folio = '".$folio."' ".
                                              "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                              "AND compania = '".$_REQUEST['trap446CompaniaHdn']."' ".
                                              "AND tipoDocumento = 'TR'";

                            $rs = fn_ejecuta_query($sqlUpdFolioStr);

                            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                                //Se agregan las unidades de cada talon y se insertan en historico su estatus
                                $sqlGetIDTalonStr = "SELECT MAX(idTalon) AS idTalon FROM trTalonesViajesTbl ".
                                                        "WHERE idViajeTractor = ".$idViaje;

                                $rsTalon = fn_ejecuta_query($sqlGetIDTalonStr);
                                array_push($talonesNuevos, $rsTalon['root'][0]['idTalon']);
                                $data = addUnidadesTalon($idTalonArr[$nInt],
                                                        $rsTalon['root'][0]['idTalon'],
                                                        $_SESSION['usuCompania'],
                                                        $distArr[$nInt],
                                                        $_REQUEST['trap446ChoferHdn'],
                                                        'AM');
                               // echo json_encode($data);

                                if($data['success']){
                                    //Se eliminan las unidades del talon temporal
                                    $sqlDltUnidadesTempStr = "DELETE FROM trUnidadesDetallesTalonesTmp ".
                                                             "WHERE numeroTalon = ".$idTalonArr[$nInt]." ".
                                                             "AND centroDistribucion = '".$_REQUEST['trap446CentroDistActualHdn']."'";

                                    fn_ejecuta_query($sqlDltUnidadesTempStr);

                                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                                        //Se elimina el talon temporal
                                        $sqlDltTalonTempStr = "DELETE FROM trTalonesViajesTmp ".
                                                                                    "WHERE numeroTalon = ".$idTalonArr[$nInt]." ".
                                                                                  "AND centroDistribucion = '".$_REQUEST['trap446CentroDistActualHdn']."'";

                                        fn_ejecuta_query($sqlDltTalonTempStr);

                                        if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                                            $a['success'] = false;
                                            $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltTalonTempStr;
                                        }
                                    } else {
                                        $a['success'] = false;
                                        $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltUnidadesTempStr;
                                    }
                                } else {
                                    $a = $data;
                                }
                            } else {
                                $a['success'] = false;
                                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltTalonTempStr;
                            }
                        } else {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddTalonesViajeStr;
                        }
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = getNoFolioMsg();
                        break;
                    }

                }
            }
                        
            if($a['success']){
                  $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl SET claveMovimiento = 'VF' ".
                                    "WHERE idViajeTractor = ".$idViaje;
                  //echo "$sqlUpdViajeStr<br>";
                  fn_ejecuta_query($sqlUpdViajeStr);
            }
            
            //VIAJE ACTUALIZACION
            if($a['success']){
                //Actualiza datos al viaje
                //Obtiene el numero total de unidades nuevo
                $sqlGetUnidadesViajeStr = "SELECT numeroUnidades FROM trTalonesViajesTbl ".
                                            "WHERE idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ".
                                            "AND claveMovimiento != 'TX' ";

                $rs = fn_ejecuta_query($sqlGetUnidadesViajeStr);

                if(sizeof($rs['root']) > 0){
                    $numTotalUnidadesViaje = 0;
                    foreach ($rs['root'] as $talon) {
                        $numTotalUnidadesViaje += (int)$talon['numeroUnidades'];
                    }


                    $getNumeroRepartos =    "SELECT count(distinct distribuidor) as numeroRepartos ".
                                            "FROM trtalonesviajestbl ".
                                            "WHERE idViajeTractor = '".$_REQUEST['trap446IdViajeHdn']."'";

                    $rsNumeroRepartos = fn_ejecuta_query($getNumeroRepartos);

                    $nRepartos =  $rsNumeroRepartos['root'][0]['numeroRepartos'];

                    $sqlGetKmTabulados = "SELECT max(kp.kilometros) as kpTabulados  ".
                                         "FROM trtalonesviajestbl tl, cakilometrosplazatbl kp ".
                                         "WHERE tl.idPlazaOrigen = kp.idPlazaOrigen ".
                                         "AND tl.idPlazaDestino = kp.idPlazaDestino ".
                                         "AND tl.idViajeTractor = '".$_REQUEST['trap446IdViajeHdn']."' ".
                                         "AND tl.claveMovimiento != 'TX' ";

                    $rsKmTabulados = fn_ejecuta_query($sqlGetKmTabulados);

                    $kmTab = $rsKmTabulados['root'][0]['kpTabulados'];


                    $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl SET ".
                                        "idPlazaDestino = ".$idPlazaDestino.", ".
                                        "kilometrosTabulados = ".$kmTab.", ".
                                        "numeroUnidades = ".$numTotalUnidadesViaje.", ".
                                        "numeroRepartos = ".$nRepartos." ".
                                        "WHERE idViajeTractor = ".$idViaje;

                    fn_ejecuta_query($sqlUpdViajeStr);

                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        //Se elimina la unidad de su posicion en el patio}
                        $sqlGetUnidadesViajeStr = "SELECT dt.vin, ".
                                "(SELECT h.localizacionUnidad FROM alHistoricoUnidadesTbl h WHERE h.vin = dt.vin AND h.fechaEvento = (".
                                    "SELECT MAX(h2.fechaEvento) FROM alHistoricoUnidadesTbl h2 WHERE h2.vin = h.vin)) AS patio FROM trunidadesdetallestalonestbl dt WHERE dt.idTalon IN ".
                                                    "(SELECT tv.idTalon FROM trtalonesviajestbl tv ".
                                                        "WHERE tv.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn'].")";

                        $data = fn_ejecuta_query($sqlGetUnidadesViajeStr);

                        foreach ($data['root'] as $unidad) {
                            dltLocalizacionPatios($unidad['vin'], $unidad['patio']);
                        }

                        //MENSAJE DE PRUEBA
                        $a['successMessage'] = 'Modificación de los talones del viaje completada correctamente';
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlUpdViajeStr;
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = "ERROR: No se encontraron talones del viaje";
                }
            }
        }

        $a['successTitle'] = getMsgTitulo();
        $a['foliosImpresion'] = substr($foliosImpresion, 0, -1);
        echo json_encode($a);
    }

    function entregarTalon(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap455IdViajeHdn'] == ""){
            $e[] = array('id'=>'trap455IdViajeHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap455IdTalonHdn'] == ""){
            $e[] = array('id'=>'trap455IdTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap455CompaniaRemitenteHdn'] == ""){
            $e[] = array('id'=>'trap455CompaniaRemitenteHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap455FolioTxt'] == ""){
            $e[] = array('id'=>'trap455FolioTxt','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap455CentroDistHdn'] == ""){
            $e[] = array('id'=>'trap455CentroDistHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $distribuidorArr = explode('|', substr($_REQUEST['trap455DistribuidorHdn'], 0, -1));
        if(in_array('', $distribuidorArr)){
            $e[] = array('id'=>'trap455DistribuidorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap455LocalizacionHdn'] == ""){
            $e[] = array('id'=>'trap455LocalizacionHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap455ChoferHdn'] == ""){
            $e[] = array('id'=>'trap455ChoferHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap455TractorHdn'] == ""){
            $e[] = array('id'=>'trap455TractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($_REQUEST['trap455TipoRecepcionHdn'] == ""){
            $e[] = array('id'=>'trap455TipoRecepcionHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['recepcionViajeTipoRecepcionCmb'] == ""){
            $e[] = array('id'=>'recepcionViajeTipoRecepcionCmb','msg'=>getRequerido());
            $a['success'] = false;
        }

        $vinArr = explode('|', substr($_REQUEST['trap455VinHdn'], 0, -1));
        if(in_array('', $vinArr)){
            $e[] = array('id'=>'trap455VinHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $estatusArr = explode('|', substr($_REQUEST['trap455UnidadEstatusHdn'], 0, -1));
        if(in_array('', $estatusArr)){
            $e[] = array('id'=>'trap455UnidadEstatusHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $tarifaArr = explode('|', substr($_REQUEST['trap455TarifaHdn'], 0, -1));
        if(in_array('', $tarifaArr)){
            $e[] = array('id'=>'trap455TarifaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success'] = true) {

            for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                $selStr = "SELECT ud.vin, ud.idtarifa, ud.distribuidor, ud.localizacionUnidad, tl.centroDistribucion, vi.claveChofer, vi.idViajeTractor, tl.idTalon, tl.clavemovimiento, tl.numerounidades, dist.tipodistribuidor, ".
                        "CASE WHEN sim.marca IN ('AR','CD','DG','FI','JP', 'RA') THEN 'FCA' ".
                        "WHEN sim.marca IN ('PG') THEN 'PGT' ".
                        "ELSE '' ".
                        "END  AS marca ".                     
                        "FROM trtalonesviajestbl tl, catractorestbl tt, trviajestractorestbl vi, trunidadesdetallestalonestbl ts, alultimodetalletbl ud, cadistribuidorescentrostbl dist, alunidadestbl un, casimbolosunidadestbl sim ".
                        "WHERE tl.centroDistribucion IN ('CDSLP','CMDAT','CDTOL','CDSAL','CDAGS','CDVER','CDSFE','CDLZC','CDANG','CDLCL') ".
                        "AND tl.distribuidor NOT IN ('CDTOL','CDSAL','CDAGS','CDVER','CDSFE','CDLZC','CDSLP','CMDAT','CDANG') ".
                        "AND tl.folio = '".$_REQUEST['recepcionViajeNumeroTalonTxt']."' ".
                        "AND tt.tractor = ".$_REQUEST['trap455TractorHdn']." ".
                        "AND vi.idTractor = tt.idTractor ".
                        "AND vi.idViajeTractor = tl.idViajeTractor ".
                        "AND ts.idTalon = tl.idTalon ".
                        "AND ud.centroDistribucion = tl.centroDistribucion ".
                        "AND ud.vin = ts.vin ".
                        "AND tl.clavemovimiento = 'TF' ".
                        "AND ud.idtarifa != 5 ".
                        "AND dist.distribuidorcentro = ud.distribuidor ".
                        "AND un.vin = ud.vin ".
                        "AND sim.simboloUnidad = un.simboloUnidad ".
                        "AND ud.vin not in (select vin from alTransportacionEstandarTrackingObtTbl where vin ='".$vinArr[$nInt]."') ".
                        "AND ud.vin not in (select vin from alTransExportacionTrackingObtTbl where vin ='".$vinArr[$nInt]."') ".
                        "AND ud.vin = '".$vinArr[$nInt]."'";
                $alTransExpRst = fn_ejecuta_query($selStr);
                if ($alTransExpRst['records'] > 0) {
                    if ($alTransExpRst['root'][0]['marca'] == 'FCA') {
                        $insStr = "INSERT INTO ";
                        if ($alTransExpRst['root'][0]['tipodistribuidor'] == 'DI') {
                            $insStr .= "alTransportacionEstandarTrackingObtTbl";
                        } else {
                            $insStr .= "alTransExportacionTrackingObtTbl";
                        }
                        //$insStr .= " (vin, fechaom, estatusHistorico) VALUES ('".$vinArr[$nInt]."',NOW(),'OM')";
                        $insStr .= " (vin, fechaom,fechaupload , estatusHistorico, marca) VALUES ('".$vinArr[$nInt]."',NOW(),NOW(),'EP','".$alTransExpRst['root'][0]['marca']."')";
                        fn_ejecuta_query($insStr);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['errorMessage'] = $_SESSION['error_sql'];
                            $a['sql']         = $insStr;
                            break;
                        }
                    }
                    if ($alTransExpRst['root'][0]['marca'] == 'PGT') {

                        $sql540="SELECT * FROM alTransportacionEstandarTrackingObtTbl WHERE vin ='".$vinArr[$nInt]."'";
                        $rs540=fn_ejecuta_query($sql540);

                        if (sizeof($rs540['root']) >'0') {
                            $insStr = "INSERT INTO ";
                            if ($alTransExpRst['root'][0]['tipodistribuidor'] == 'DI') {
                                $insStr .= "alTransportacionEstandarTrackingObtTbl";
                            } else {
                                $insStr .= "alTransportacionEstandarTrackingObtTbl";
                            }
                            //$insStr .= " (vin, fechaom, estatusHistorico) VALUES ('".$vinArr[$nInt]."',NOW(),'OM')";
                            $insStr .= " (vin, fechaom,fechaupload , estatusHistorico, marca) VALUES ('".$vinArr[$nInt]."',NOW(),NOW(),'OM','PG')";
                            fn_ejecuta_query($insStr);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['errorMessage'] = $_SESSION['error_sql'];
                                $a['sql']         = $insStr;
                                break;
                            }
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['errorMessage'] = $_SESSION['error_sql'];
                                $a['sql']         = $insStr;
                                break;
                            }
                        }else{

                            $insStr = "INSERT INTO ";
                            if ($alTransExpRst['root'][0]['tipodistribuidor'] == 'DI') {
                                $insStr .= "alTransportacionEstandarTrackingObtTbl";
                            } else {
                                $insStr .= "alTransportacionEstandarTrackingObtTbl";
                            }
                            //$insStr .= " (vin, fechaom, estatusHistorico) VALUES ('".$vinArr[$nInt]."',NOW(),'OM')";
                            $insStr .= " (vin, fechaom,fechaupload , estatusHistorico) VALUES ('".$vinArr[$nInt]."',NOW(),NOW(),'EN')";
                            fn_ejecuta_query($insStr);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['errorMessage'] = $_SESSION['error_sql'];
                                $a['sql']         = $insStr;
                                break;
                            }

                        }

                        
                    }
                }
            }
            if ($a['success'] == false) {
                echo json_encode($a);
                die();
            }

                        

            ///


            //if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                //Se actualizan los estatus de las unidades del talon
                for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                    if ($_REQUEST['recepcionViajeTipoRecepcionCmb'] == 'P' && $estatusArr[$nInt] == 'OP') {
                        if ($_SESSION['usuCompania']=='APIEX' && $distribuidorArr[$nInt]=='APIEX') {
                                 $estatus='EN';
                             //i830_AM($vinArr[$nInt],'RP');

                             $sqlLoc="SELECT * FROM aldestinosespecialestbl a
                                        WHERE a.vin ='".$vinArr[$nInt]."'
                                        AND a.idDestinoEspecial=(SELECT max(idDestinoEspecial) FROM aldestinosespecialestbl b WHERE a.vin=b.vin)";
                             $rsLoc=fn_ejecuta_query($sqlLoc);

                             if ($rsLoc['root']!==null) {
                                $localizacion=$rsLoc['root'][0]['idDestinoEspecial'];
                             }else{
                                $localizacion=$_REQUEST['trap455LocalizacionHdn'];
                             }

                                $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                                "claveChofer, observaciones, usuario, ip) ".
                                                "VALUES(".
                                                "'CDLZC',".
                                                "'".$vinArr[$nInt]."',".
                                                "NOW() + INTERVAL 1 SECOND,".
                                                "'".$estatus."',".
                                                "'".$distribuidorArr[$nInt]."',".
                                                "'".$tarifaArr[$nInt]."',".
                                                "'".$localizacion."',".
                                                replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                                "'".$RQobservaciones."',".
                                                "'".$_SESSION['idUsuario']."',".
                                                "'".getClientIP()."')";

                                $rs = fn_ejecuta_query($sqlAddCambioHistoricoStr);

                                $sqlAddUpdUltimoDetalleStr = "UPDATE alUltimoDetalleTbl SET ".
                                                "centroDistribucion = 'CDLZC', ".
                                                //"fechaEvento = '".$fechaEvento."',".
                                                "fechaEvento = now(),".
                                                "claveMovimiento = '".$estatus."',".
                                                "distribuidor = '".$distribuidorArr[$nInt]."',".
                                                "idTarifa = ".$tarifaArr[$nInt].",".
                                                "localizacionUnidad = '".$localizacion."',".
                                                "claveChofer = ".replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                                "observaciones = '".$RQobservaciones."',".
                                                "usuario = ".$_SESSION['idUsuario'].",".
                                                "ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                                "WHERE vin = '".$vinArr[$nInt]."'";
                                $rs = fn_ejecuta_Upd($sqlAddUpdUltimoDetalleStr);

                                 $data['success'] = true;
                                 
                                            //Se actualiza el estatus del talon
                $sqlUpdEstatusTalonStr = "UPDATE trTalonesViajesTbl ".
                                         "SET claveMovimiento = '".$_REQUEST['trap455TipoRecepcionHdn']."', fechaEntrega=NOW() ".
                                         "WHERE idTalon = ".$_REQUEST['trap455IdTalonHdn'];

                $rs = fn_ejecuta_query($sqlUpdEstatusTalonStr);

                            }
                            else    
                            {
                                
                            /*$data = addHistoricoUnidad($_SESSION['usuCompania'], $vinArr[$nInt], $estatusArr[$nInt],
                                                       $distribuidorArr[$nInt], $tarifaArr[$nInt],
                                                       $_REQUEST['trap455LocalizacionHdn'],
                                                       $_REQUEST['trap455ChoferHdn'], '',
                                                       $_SESSION['idUsuario'], $nInt*2);*/

                            

                            $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                                "claveChofer, observaciones, usuario, ip) ".
                                                "VALUES(".
                                                "'".$_SESSION['usuCompania']."',".
                                                "'".$vinArr[$nInt]."',".
                                                "NOW() + INTERVAL 1 SECOND,".
                                                "'".$estatusArr[$nInt]."',".
                                                "'".$distribuidorArr[$nInt]."',".
                                                "'".$tarifaArr[$nInt]."',".
                                                "'".$_REQUEST['trap455LocalizacionHdn']."',".
                                                replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                                "'".$RQobservaciones."',".
                                                "'".$_SESSION['idUsuario']."',".
                                                "'".getClientIP()."')";

                            
                            $rs = fn_ejecuta_query($sqlAddCambioHistoricoStr);

                             $estatus='RP';
                             i830_AM($vinArr[$nInt],'RP');

                             $sqlLoc="SELECT * FROM aldestinosespecialestbl a
                                        WHERE a.vin ='".$vinArr[$nInt]."'
                                        AND a.idDestinoEspecial=(SELECT max(idDestinoEspecial) FROM aldestinosespecialestbl b WHERE a.vin=b.vin)";
                             $rsLoc=fn_ejecuta_query($sqlLoc);

                             if ($rsLoc['root']!==null) {
                                $localizacion=$rsLoc['root'][0]['idDestinoEspecial'];
                             }else{
                                $localizacion=$_REQUEST['trap455LocalizacionHdn'];
                             }

                                $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                                "claveChofer, observaciones, usuario, ip,idTalon) ".
                                                "VALUES(".
                                                "'".$_SESSION['usuCompania']."',".
                                                "'".$vinArr[$nInt]."',".
                                                "NOW() + INTERVAL 60 SECOND,".
                                                "'".$estatus."',".
                                                "'".$distribuidorArr[$nInt]."',".
                                                "'".$tarifaArr[$nInt]."',".
                                                "'".$localizacion."',".
                                                replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                                "'".$RQobservaciones."',".
                                                "'".$_SESSION['idUsuario']."',".
                                                "'".getClientIP()."',".
                                                "'".replaceEmptyNull($_REQUEST['trap455IdTalonHdn'])."')";

                                $rs = fn_ejecuta_query($sqlAddCambioHistoricoStr);

                                $sqlAddUpdUltimoDetalleStr = "UPDATE alUltimoDetalleTbl SET ".
                                                "centroDistribucion = '".$_SESSION['usuCompania']."', ".
                                                //"fechaEvento = '".$fechaEvento."',".
                                                "fechaEvento = now(),".
                                                "claveMovimiento = '".$estatus."',".
                                                "distribuidor = '".$distribuidorArr[$nInt]."',".
                                                "idTarifa = ".$tarifaArr[$nInt].",".
                                                "localizacionUnidad = '".$localizacion."',".
                                                "claveChofer = ".replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                                "observaciones = '".$RQobservaciones."',".
                                                "usuario = ".$_SESSION['idUsuario'].",".
                                                "ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                                "WHERE vin = '".$vinArr[$nInt]."'";
                                $rs = fn_ejecuta_Upd($sqlAddUpdUltimoDetalleStr);

                            $data['success'] = true;

                            $updEmbarcadas="UPDATE trUnidadesDetallesTalonesTbl set estatus ='OM' WHERE estatus='AM' AND vin='".$vinArr[$nInt]."'";
                            fn_ejecuta_upd($updEmbarcadas);


                            $sqlCot="SELECT * FROM alcotizacionesespecialestbl WHERE vin ='".$vinArr[$nInt]."' AND numCotizacion=(SELECT MAX(numCotizacion) FROM alcotizacionesespecialestbl where vin='".$vinArr[$nInt]."')";
                            $rsCot=fn_ejecuta_query($sqlCot);


                            if (sizeof($rsCot['root']) !='0') {
                                $upd="UPDATE alcotizacionesespecialestbl SET estatus='0Z' WHERE idCotizacion='".$rsCot['root'][0]['idCotizacion']."'";
                                fn_ejecuta_query($upd);
                            }
                                       //Se actualiza el estatus del talon
                $sqlUpdEstatusTalonStr = "UPDATE trTalonesViajesTbl ".
                                         "SET claveMovimiento = '".$_REQUEST['trap455TipoRecepcionHdn']."', fechaEntrega=NOW() ".
                                         "WHERE idTalon = ".$_REQUEST['trap455IdTalonHdn'];

                $rs = fn_ejecuta_query($sqlUpdEstatusTalonStr);

                        }
                    } else {

                        $selCentroUd="SELECT centroDistribucion from alUltimoDetalleTbl where vin='".$vinArr[$nInt]."'";
                        $centroUD= fn_ejecuta_upd($selCentroUd);

                        /*$data = addHistoricoUnidad($centroUD['root'][0]['centroDistribucion'],$vinArr[$nInt],$estatusArr[$nInt],
                                                        $distribuidorArr[$nInt],$tarifaArr[$nInt],
                                                        $_REQUEST['trap455LocalizacionHdn'],
                                                        $_REQUEST['trap455ChoferHdn'], '',
                                                        $_SESSION['idUsuario'], $nInt*2);*/

                        $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                                            "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                            "claveChofer, observaciones, usuario, ip,idTalon) ".
                                            "VALUES(".
                                            "'".$centroUD['root'][0]['centroDistribucion']."',".
                                            "'".$vinArr[$nInt]."',".
                                            "NOW() + INTERVAL 1 SECOND,".
                                            "'OM',".
                                            "'".$distribuidorArr[$nInt]."',".
                                            "'".$tarifaArr[$nInt]."',".
                                            "'".$_REQUEST['trap455LocalizacionHdn']."',".
                                            replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                            "'".$RQobservaciones."',".
                                            "'".$_SESSION['idUsuario']."',".
                                            "'".getClientIP()."',".
                                            "'".replaceEmptyNull($_REQUEST['trap455IdTalonHdn'])."')";
                        $rs = fn_ejecuta_query($sqlAddCambioHistoricoStr);

                         $sqlAddUpdUltimoDetalleStr = "UPDATE alUltimoDetalleTbl SET ".
                                            "centroDistribucion = '".$centroUD['root'][0]['centroDistribucion']."', ".
                                            //"fechaEvento = '".$fechaEvento."',".
                                            "fechaEvento = now(),".
                                            "claveMovimiento = 'OM',".
                                            "distribuidor = '".$distribuidorArr[$nInt]."',".
                                            "idTarifa = ".$tarifaArr[$nInt].",".
                                            "localizacionUnidad = '".$_REQUEST['trap455LocalizacionHdn']."',".
                                            "claveChofer = ".replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                            "observaciones = '".$RQobservaciones."',".
                                            "usuario = ".$_SESSION['idUsuario'].",".
                                            "ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                            "WHERE vin = '".$vinArr[$nInt]."'";
                        $rs = fn_ejecuta_Upd($sqlAddUpdUltimoDetalleStr);

                        $data['success'] = true;


                        $updEmbarcadas="UPDATE trUnidadesDetallesTalonesTbl set estatus ='OM' WHERE estatus='AM' AND vin='".$vinArr[$nInt]."'";
                        fn_ejecuta_upd($updEmbarcadas);

                                   //Se actualiza el estatus del talon
                $sqlUpdEstatusTalonStr = "UPDATE trTalonesViajesTbl ".
                                         "SET claveMovimiento = '".$_REQUEST['trap455TipoRecepcionHdn']."', fechaEntrega=NOW() ".
                                         "WHERE idTalon = ".$_REQUEST['trap455IdTalonHdn'];

                $rs = fn_ejecuta_query($sqlUpdEstatusTalonStr);

                        if ($_REQUEST['recepcionViajeTipoRecepcionCmb'] == 'P') {

                            /*$data = addHistoricoUnidad($_SESSION['usuCompania'], $vinArr[$nInt], 'RP',
                                                        $distribuidorArr[$nInt], $tarifaArr[$nInt],
                                                        $_REQUEST['trap455LocalizacionHdn'],
                                                        $_REQUEST['trap455ChoferHdn'], '',
                                                        $_SESSION['idUsuario'], ($nInt*2)+2);*/

                            $estatus='RP';

                            $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                                            "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                            "claveChofer, observaciones, usuario, ip) ".
                                            "VALUES(".
                                            "'".$_SESSION['usuCompania']."',".
                                            "'".$vinArr[$nInt]."',".
                                            "NOW() + INTERVAL 1 SECOND,".
                                            "'".$estatus."',".
                                            "'".$distribuidorArr[$nInt]."',".
                                            "'".$tarifaArr[$nInt]."',".
                                            "'".$_REQUEST['trap455LocalizacionHdn']."',".
                                            replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                            "'".$RQobservaciones."',".
                                            "'".$_SESSION['idUsuario']."',".
                                            "'".getClientIP()."')";

                            $rs = fn_ejecuta_query($sqlAddCambioHistoricoStr);

                            $sqlAddUpdUltimoDetalleStr = "UPDATE alUltimoDetalleTbl SET ".
                                            "centroDistribucion = '".$_SESSION['usuCompania']."', ".
                                            //"fechaEvento = '".$fechaEvento."',".
                                            "fechaEvento = now(),".
                                            "claveMovimiento = '".$estatus."',".
                                            "distribuidor = '".$distribuidorArr[$nInt]."',".
                                            "idTarifa = ".$tarifaArr[$nInt].",".
                                            "localizacionUnidad = '".$_REQUEST['trap455LocalizacionHdn']."',".
                                            "claveChofer = ".replaceEmptyNull($_REQUEST['trap455ChoferHdn']).",".
                                            "observaciones = '".$RQobservaciones."',".
                                            "usuario = ".$_SESSION['idUsuario'].",".
                                            "ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                            "WHERE vin = '".$vinArr[$nInt]."'";
                            $rs = fn_ejecuta_Upd($sqlAddUpdUltimoDetalleStr);

                            $data['success'] = true;

                                       //Se actualiza el estatus del talon
                $sqlUpdEstatusTalonStr = "UPDATE trTalonesViajesTbl ".
                                         "SET claveMovimiento = '".$_REQUEST['trap455TipoRecepcionHdn']."', fechaEntrega=NOW() ".
                                         "WHERE idTalon = ".$_REQUEST['trap455IdTalonHdn'];

                $rs = fn_ejecuta_query($sqlUpdEstatusTalonStr);


                            
                            //COMENTADO EL DIA 27/03/2020 PARA EL FLUJO DE OPERACION AUTORIZADO POR OSCAR

                          /*  $sqlGetOrigen = "SELECT 1 as bandera ".
                                            "FROM trtalonesviajestbl ".
                                            "WHERE IDTALON = '".$_REQUEST['trap455IdTalonHdn'] ."' ".
                                            "AND centroDistribucion IN ('CDLZC','CDLCL');";

                            $rsOrigen = fn_ejecuta_query($sqlGetOrigen);

                            $sqlGetTarifa = "SELECT ta.tipoTarifa ".
                                            "FROM alultimodetalletbl ud, catarifastbl ta ".
                                            "WHERE VIN = '".$vinArr[$nInt]."' ".
                                            "AND ud.idTarifa = ta.idTarifa " ;
                                            
                            $rsSqlGetTarifa = fn_ejecuta_query($sqlGetTarifa);

                            if($rsOrigen['root'][0]['bandera'] == '1' && $_SESSION['usuCompania'] == 'CDAGS' && $rsSqlGetTarifa['root'][0]['tipoTarifa'] == 'E'){
                                $data = addHistoricoUnidad($_SESSION['usuCompania'], $vinArr[$nInt], 'SL',
                                                          $distribuidorArr[$nInt], $tarifaArr[$nInt],
                                                          $_REQUEST['trap455LocalizacionHdn'],
                                                          $_REQUEST['trap455ChoferHdn'], '',
                                                          $_SESSION['idUsuario'], ($nInt*2)+2);

                                $updUd = "UPDATE alultimodetalletbl ".
                                          "SET claveMovimiento = 'SL', ".
                                             "fechaEvento = now() ".
                                           "WHERE vin = '".$vinArr[$nInt]."';  ";

                                fn_ejecuta_query($updUd);

                            }*/

                            /*$data = addHistoricoUnidad($_SESSION['usuCompania'], $vinArr[$nInt], 'OP',
                                                        $distribuidorArr[$nInt], $tarifaArr[$nInt],
                                                        $_REQUEST['trap455LocalizacionHdn'],
                                                        $_REQUEST['trap455ChoferHdn'], '',
                                                        $_SESSION['idUsuario'], ($nInt*2)+2);*/
                        }
                    }

                    ///////////////////////**************************/////////////////

                        //Se actualiza el estatus del talon
                /*$sqlUpdEstatusTalonStr = "UPDATE trTalonesViajesTbl ".
                                         "SET claveMovimiento = '".$_REQUEST['trap455TipoRecepcionHdn']."', fechaEntrega=NOW() ".
                                         "WHERE idTalon = ".$_REQUEST['trap455IdTalonHdn'];

                $rs = fn_ejecuta_query($sqlUpdEstatusTalonStr);*/

                //Se actualiza el estatus del viaje
                //Pero se revisa que todos los talones del viaje esten entregados
               
                ///

                 $sqlTalonesViaje = "SELECT * FROM trTalonesViajesTbl ".
                        " WHERE idViajeTractor = ".$_REQUEST['trap455IdViajeHdn'].
                        " AND claveMovimiento = 'TF'";
                //echo "$sql<br>";
                $movTalonesRst = fn_ejecuta_query($sqlTalonesViaje);
                //var_dump($movTalonesRst);
                if($movTalonesRst['records'] == 0)
                {
                        $sql = "UPDATE trViajesTractoresTbl SET claveMovimiento = 'VE' WHERE idViajeTractor = ".$_REQUEST['trap455IdViajeHdn'];
                        //echo "$sql<br>";
                        fn_ejecuta_query($sql);                             
                                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sql;
                    }       
                }  


                /////////////////////////////////////////////*******/////////////


                    if($data['success'] == true){
                        //Revisa si es Mercedes Benz la Unidad
                        $a['successMessage'] = getEntregaTalonSuccessMsg();
                    }
                    else {
                        $a = $data;
                    }
                }
            /*} else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdEstatusTalonStr;
            }*/
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function liberarViaje(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['liberacionTractorIdViajeTractorHdn'] == ""){
            $e[] = array('id'=>'IdViajeTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if ($a['success'] == true) {

            if ($_SESSION['usuario']=='HARUMY') {
                
            }else{
                $data = updEstatusViaje($_REQUEST['liberacionTractorIdViajeTractorHdn'], 'VU');
                $a = $data;    
            }

            



            if ($_SESSION['usuario']=='HARUMY') {
                
            }else{

                    $AddGasto = "INSERT INTO  trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio,fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip) ".
                "SELECT idViajeTractor, '0' AS concepto, '".$_SESSION['usuCompania']."' as centroDistribucion, '0' as folio, now() as fechaEvento, null as cuentaContable, null as mesAfectacion, '0.00' as importe, concat('viajeLiberado: ',now()) as observaciones, 'GU' as claveMovimiento, '".$_SESSION['idUsuario']."' as usuario, '".$_SERVER['REMOTE_ADDR']."' as ip ".
                "FROM trviajestractorestbl ".
                "WHERE idViajeTractor = ".$_REQUEST['liberacionTractorIdViajeTractorHdn'];

            // $AddGasto = "UPDATE trGastosViajeTractorTbl set claveMovimiento='GU', fechaEvento=now() ".
              //          "WHERE claveMovimiento='CA' AND idViajeTractor = ".$_REQUEST['liberacionTractorIdViajeTractorHdn'];                                
                $rs = fn_ejecuta_Add($AddGasto);                
            }

            //echo json_encode($AddGasto);

            //// suma kms vacios desde ultima entrega al patio de liberacion 

            $sqlDistribuidor="SELECT distinct distribuidor from trtalonesviajestbl ".
                                " where idViajeTractor=".$_REQUEST['liberacionTractorIdViajeTractorHdn'];
            $rsDist=fn_ejecuta_query($sqlDistribuidor);

            $cadenaVin='';
        

        for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
            $cadenaVin=$cadenaVin.$rsDist['root'][$i]['distribuidor']."','";
        }


        $rsCotizacion['data']['VIN']=$cadenaVin;
        $distArr = explode("|", substr($cadenaVin, 0, -1));


         $sqlKilCD="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$_SESSION['usuCompania']."') ";
         $rsKilCD=fn_ejecuta_query($sqlKilCD);

         $cadena= substr($cadenaVin,  0, -2);

         $sqlKil="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$cadena.") ";
         $rsKil=fn_ejecuta_query($sqlKil);

            $cadenaVin='';
        

        for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
            $cadenaVin=$cadenaVin.$rsKil['root'][$i]['idPlaza'].",";
        }
        $plazas=substr($cadenaVin,0,-1);

         $sqlKmsPlaza="SELECT * FROM caKilometrosPlazaTbl".
                        " WHERE idPlazaOrigen=".$rsKilCD['root'][0]['idPlaza'].
                        " AND idPlazaDestino in (".$plazas.")";
        $rsKms=fn_ejecuta_query($sqlKmsPlaza);

        $kmsV='';

        for ($i=0; $i <sizeof($rsKms['root']) ; $i++) {
            $kmsV=$kmsV.$rsKms['root'][$i]['kilometros']."|";
        }

        $plazas=explode("|", substr($kmsV,0,-1));

            $plazaDestino = -1;
            $kmTabulados = 0;
            for ($iInt=0; $iInt < sizeof($plazas); $iInt++) {
                if ($kmTabulados <= $plazas[$iInt]) {
                    $plazaDestino = $destinoArr[$iInt];
                    $kmTabulados = $plazas[$iInt];
                }
            }

            //////////////suma los Kms vacios de la ultima entrega a el patio de liberacion

             $sqlDistribuidor="SELECT * from trViajesTractoresTbl ".
//                                " where idViajeTractor=".$_REQUEST['liberacionTractorIdViajeTractorHdn'];
            $rsDist=fn_ejecuta_query($sqlDistribuidor);

            $nvosKmsVacios=$rsDist['root'][0]['kilometrosSinUnidad'] + $kmTabulados;


            $updNvosVacios="UPDATE trViajesTractoresTbl set kilometrosSinUnidad=".$nvosKmsVacios.", patioRegreso1='".$_SESSION['usuCompania']."', fechaRegreso1=NOW() WHERE idViajeTractor=".$_REQUEST['liberacionTractorIdViajeTractorHdn'];
            fn_ejecuta_Upd($updNvosVacios);

            ////actualiza viaje acompañante

            $selViajeAcom="SELECT * FROM trViajesTractoresTbl where idViajePadre=".$_REQUEST['liberacionTractorIdViajeTractorHdn'];
            $rsAcompanante=fn_ejecuta_query($selViajeAcom);

            $updNvosVacios="UPDATE trViajesTractoresTbl set kilometrosSinUnidad=".$nvosKmsVacios.", patioRegreso1='".$_SESSION['usuCompania']."', fechaRegreso1=NOW() WHERE idViajeTractor=".$rsAcompanante['root'][0]['idViajePadre'];
            fn_ejecuta_Upd($updNvosVacios);

            
            if ($_SESSION['usuario']=='HARUMY') {
            }else{
                updEstatusViaje($rsAcompanante['root'][0]['idViajeTractor'], 'VU');
            }

             

            if ($_SESSION['usuario']=='HARUMY') {
                //echo $_SESSION['usuario'];
                $updOdometro="UPDATE trTalonesViajesTbl set odometro=, maniobrasCarga=,maniobrasDescarga=, semillero= where idtalon in();";
            }else{
                //echo $_SESSION['usuario'];
                $AddGasto = "INSERT INTO  trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio,fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip) ".
                        "SELECT idViajeTractor, '0' AS concepto, '".$_SESSION['usuCompania']."' as centroDistribucion, '0' as folio, now() as fechaEvento, null as cuentaContable, null as mesAfectacion, '0.00' as importe, concat('viajeLiberado: ',now()) as observaciones, 'GU' as claveMovimiento, '".$_SESSION['idUsuario']."' as usuario, '".$_SERVER['REMOTE_ADDR']."' as ip ".
                        "FROM trviajestractorestbl ".
                        "WHERE idViajeTractor = ".$rsAcompanante['root'][0]['idViajePadre'];
                $rs = fn_ejecuta_Add($AddGasto);
            }


        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    //Agrega las unidades del talon temporal al talon real
    //$data = addUnidadesTalon($idTalonArr[$nInt],$rsTalon['root'][0]['idTalon'], $_SESSION['usuCompania'], $distArr[$nInt], $_REQUEST['trap446ChoferHdn'],'AM');
    function addUnidadesTalon($idTalonTemporal, $idTalon, $centroDistribucion, $distribuidor, $chofer, $claveMovimiento){
        /*echo $idTalonTemporal;
        echo $idTalonTemporal;
        echo $idTalon;
        echo $centroDistribucion;
        echo $distribuidor;
        echo $chofer;
        echo $claveMovimiento;*/

        $a = array();
        $e = array();
        $a['success'] = true;

        if($idTalonTemporal == ""){
            $e[] = array('id'=>'IdTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($idTalon == ""){
            $e[] = array('id'=>'IdTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($centroDistribucion == ""){
            $e[] = array('id'=>'CentroDistHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($distribuidor == ""){
            $e[] = array('id'=>'DistribuidorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($chofer == ""){
            $e[] = array('id'=>'ChoferHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($claveMovimiento == ""){
            $e[] = array('id'=>'ClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success']){
            $sqlGetUnidadesTalonTempStr = "SELECT tt.vin, hu.idTarifa ".
                                        "FROM trUnidadesDetallesTalonesTmp tt, alHistoricoUnidadesTbl hu ".
                                        "WHERE tt.vin = hu.vin ".
                                        "AND hu.claveMovimiento in (SELECT valor FROM cageneralestbl WHERE tabla ='unidadAsignar' AND columna ='claveValida') ".
                                        "AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) FROM alHistoricoUnidadesTbl hu2 ".
                                        "WHERE hu2.vin = tt.vin) ".
                                        "AND tt.centroDistribucion = '".$_REQUEST['trap446CentroDistActualHdn']."' ".
                                        "AND tt.idUsuario = ".$_SESSION['idUsuario']." ".
                                        "AND numeroTalon = ".$idTalonTemporal;

            $rs = fn_ejecuta_query($sqlGetUnidadesTalonTempStr);

            //echo json_encode($sqlGetUnidadesTalonTempStr);

            if(sizeof($rs['root']) > 0){
                for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
                    $sql = "SELECT * FROM trUnidadesDetallesTalonesTbl".
                           " WHERE idTalon = ".$idTalon.
                           " AND vin = '".$rs['root'][$nInt]['vin']."'".
                           " AND estatus = '1'";
                    $rsAux = fn_ejecuta_query($sql);  
                    if($rsAux['records'] == 0)
                    {
                            $sqlAddUnidadTalonStr = "INSERT INTO trUnidadesDetallesTalonesTbl ".
                                                    "(idTalon, vin, estatus) VALUES (".
                                                    //($nInt+1).",".
                                                    $idTalon.",".
                                                    "'".$rs['root'][$nInt]['vin']."',".
                                                    "1)";                    
                            //echo "$sqlAddUnidadTalonStr<br>";
                            fn_ejecuta_query($sqlAddUnidadTalonStr);
                            
                            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                                if(substr($distribuidor, 0, 2) == 'CD'){
                                    $sqlGetDist = "SELECT h2.distribuidor FROM alHistoricoUnidadesTbl h2 ".
                                                    "WHERE h.vin = '".$rs['root'][$nInt]['vin']."' ".
                                                    "AND h.fechaEvento = (SELECT MAX(h2.fechaEvento) ".
                                                        "FROM alHistoricoUnidadesTbl h2 WHERE h2.vin = h.vin)";
        
                                    $rsDist = fn_ejecuta_query($sqlGetDist);
        
                                    if(sizeof($rsDist['root']) > 0){
                                        $distribuidor = $rsDist['root'][0]['distribuidor'];
                                    }
                                }

                                                        $sql = "SELECT DISTINCT distribuidor FROM alUltimoDetalleTbl ".
                                                               " WHERE vin = '".$rs['root'][$nInt]['vin']."'".
                                                               " AND centroDistribucion = '".$_SESSION['usuCompania']."'";
                                                        //echo "$sql<br>";
                                                        $udAuxRst = fn_ejecuta_query($sql);                                                     
                                                        $distribuidor = ($udAuxRst['records'] == 0)?$distribuidor:$udAuxRst['root'][0]['distribuidor'];                             

        
                                $data = addHistoricoUnidad($centroDistribucion,
                                                            $rs['root'][$nInt]['vin'],
                                                            $claveMovimiento,
                                                            $distribuidor,
                                                            $rs['root'][$nInt]['idTarifa'],
                                                            $centroDistribucion,
                                                            $chofer, '',
                                                            $_SESSION['idUsuario'], $nInt*2);

                                //343 para AM P01
                                i343a($rs['root'][$nInt]['vin'],'AM');
        
        
                                if($data['success']){
                                    $data = liberarUnidad($rs['root'][$nInt]['vin']);
        
                                    if($data['success']){
                                        $data = dltLocalizacionPatios($rs['root'][$nInt]['vin']);
        
                                        if(!$data['success']){
                                            $a = $data;
                                            break;
                                        }
                                    } else {
                                        $a = $data;
                                        break;
                                    }
                                } else {
                                    $a = $data;
                                    break;
                                }
                            } else {
                                $a['success'] = false;
                                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddUnidadTalonStr;
                                break;
                            }                           
                    }                    
                }
            }
        } else {

            $a['errorMessage'] = getErrorRequeridos();
            $a['errors'] = $e;
        }

        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function updUnidadTalon(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap446IdTalonHdn'] == ""){
            $e[] = array('id'=>'trap446IdTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $vinArr = explode('|', substr($_REQUEST['trap446VinHdn'], 0, -1));
        if(in_array('', $vinArr)){
            $e[] = array('id'=>'trap446VinHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $estatusArr = explode('|', substr($_REQUEST['trap446EstatusHdn'], 0, -1));
        if(in_array('', $estatusArr)){
            $e[] = array('id'=>'trap446EstatusHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                if ($estatusArr[$nInt] == 0) {
                    $sqlAddUnidadTalonStr = "INSERT INTO trUnidadesDetallesTalonesTbl ".
                                            "VALUES(".
                                                $_REQUEST['trap446IdTalonHdn'].",".
                                                "'".$vinArr[$nInt]."', ".
                                                "1)"; //Siempre es 1 cuando se inserta porque entra activa la unidad;

                    $rs = fn_ejecuta_query($sqlAddUnidadTalonStr);

                    if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        $a['successMessage'] = getUnidadTalonUpdateMsg();
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUnidadTalonStr;
                    }
                }
            }
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getDetalleTalon(){
        $lsWhereStr = "WHERE tv.idTalon = td.idTalon ".
                      "AND vt.idTractor = tt.idTractor ".
                      "AND vt.idViajeTractor = tv.idViajeTractor ".
                      "AND co.compania = tt.compania ".
                      "AND co.estatus = 1 ".
                      "AND td.vin = u.vin ".
                      "AND u.vin = h.vin ".
                      "AND h.fechaEvento=(".
                      "SELECT MAX(h1.fechaEvento) ".
                      "FROM alHistoricoUnidadesTbl h1 ".
                      "WHERE h1.vin = u.vin) ".
                      "AND su.simboloUnidad = u.simboloUnidad ".
                      "AND mu.marca = su.marca ".
                      "AND td.estatus != 'CA' ".
                      "AND h.idTarifa = tf.idTarifa ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap453IdTalonTxt'], "td.idTalon", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap453FolioTxt'], "tv.folio", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap453VinTxt'], "td.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap453EstatusHdn'], "td.estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap453CentroDistHdn'], "tv.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap453RemitenteHdn'], "tv.companiaRemitente", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap453CompaniaTractorHdn'], "co.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetDetalleTalonStr = "SELECT td.*, tv.*, tv.fechaEvento AS fechaTalon, h.*, date_format(h.fechaEvento, '%Y-%m-%d') AS fechaUltimoMov, ".
                                 "u.simboloUnidad, tf.tarifa, tf.tipoTarifa, tf.descripcion AS nombreTarifa, mu.marca, ".
                                 "mu.descripcion AS nombreMarca, u.color, ".
                                 "(SELECT co.descripcion FROM caColorUnidadesTbl co WHERE co.marca = su.marca AND co.color = u.color) AS nombreColor, ".
                                 "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h.claveMovimiento ".
                                    "AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreClaveMov, ".
                                 "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d ".
                                    "WHERE d.distribuidorCentro=h.distribuidor) AS nombreDistribuidor, ".
                                 "(SELECT d2.tipoDistribuidor FROM caDistribuidoresCentrosTbl d2 ".
                                    "WHERE tv.distribuidor = d2.distribuidorCentro) AS tipoDistribuidor,".
                                    "(SELECT d3.sucursalDe FROM caDistribuidoresCentrosTbl d3 ".
                                    "WHERE tv.distribuidor = d3.distribuidorCentro) AS sucursalDe,".
                                 "(SELECT su.descripcion FROM caSimbolosUnidadesTbl su WHERE su.simboloUnidad = u.simboloUnidad) AS nombreSimbolo ".
                                 "FROM trViajesTractoresTbl vt, trUnidadesDetallesTalonesTbl td, trTalonesViajesTbl tv, alHistoricoUnidadesTbl h, ".
                                 "alUnidadesTbl u, caTarifasTbl tf, caSimbolosUnidadesTbl su, caMarcasUnidadesTbl mu, caCompaniasTbl co, ".
                                 "caTractoresTbl tt ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetDetalleTalonStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['avanzada'] = substr($rs['root'][$iInt]['vin'], 9);
            $rs['root'][$iInt]['descDistribuidor'] = $rs['root'][$iInt]['distribuidor']." - ".$rs['root'][$iInt]['nombreDistribuidor'];
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['nombreTarifa'];
            $rs['root'][$iInt]['descClaveMov'] = $rs['root'][$iInt]['claveMovimiento']." - ".$rs['root'][$iInt]['nombreClaveMov'];
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['nombreSimbolo'];
            $rs['root'][$iInt]['descMarca'] = $rs['root'][$iInt]['marca']." - ".$rs['root'][$iInt]['nombreMarca'];

            //Para la pantalla de Recepcion de Estatus
            $rs['root'][$iInt]['estatusEntrega'] = 'OM';
        }

        echo json_encode($rs);
    }

    function cancelarTalon(){
        //Cancelacion del talon y sus unidades
        $a = array();
        $e = array();
        $a['success'] = true;

        //Unitarios
        if ($_REQUEST['trap446CentroDistHdn'] == "") {
            $e[] = array('id'=>'trap446CentroDistHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if ($_REQUEST['trap446DistribuidorHdn'] == "") {
            $e[] = array('id'=>'trap446DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if ($_REQUEST['trap446ChoferHdn'] == "") {
            $e[] = array('id'=>'trap446ChoferHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if ($_REQUEST['trap446IdTalonHdn'] == "") {
            $e[] = array('id'=>'trap446IdTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            //Se cancela el Talón
            $sqlCancelarTalonStr = "UPDATE trTalonesViajesTbl ".
                                   "SET claveMovimiento = 'TX' ".
                                   "WHERE idTalon = ".$_REQUEST['trap446IdTalonHdn'];

            $rs = fn_ejecuta_query($sqlCancelarTalonStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                //Se cancelan las unidades del Talón
                $vinArr = explode('|', substr($_REQUEST['trap446VinHdn'], 0, -1));
                if($vinArr[$nInt] != ""){
                $timeAdd = 0;
                for ($nInt=0; $nInt < sizeof($vinArr); $nInt++){
                        //Se cancela la unidad del detalle del talon
                        $data = cancelarUnidadTalon($_REQUEST['trap446IdTalonHdn'], $vinArr[$nInt]);
                        if ($data['success'] == true) {
                            $sqlUpdNumUnidadesViaje =   "UPDATE trViajesTractoresTbl vt ".
                                                        "SET vt.numeroUnidades = (SELECT SUM(tv.numeroUnidades) FROM trTalonesViajesTbl tv ".
                                                            "WHERE tv.claveMovimiento != 'TX' AND tv.idViajeTractor = vt.idViajeTractor) ".
                                                        "WHERE vt.idViajeTractor = (SELECT tv2.idViajeTractor FROM trTalonesViajesTbl tv2 ".
                                                            "WHERE tv2.idTalon = ".$_REQUEST['trap446IdTalonHdn'].")";

                            fn_ejecuta_query($sqlUpdNumUnidadesViaje);

                            if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                                $a['successMessage'] = getTalonCanceladoSuccessMsg();
                            }
                        } else {
                            $a['success'] = false;
                            $a['errorMessage'] = $data['errorMessage'];
                            $a['errors'] = $data['errors'];
                            break;
                        }
                        //Para que el siguiente vin tenga 2 segundos más de diferencia respecto al primero
                        $timeAdd += 4;
                    }
                }
                else
                {
                   $data = cancelarUnidadTalonModificacion($_REQUEST['trap446IdTalonHdn']); 
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelarTalonStr;
            }
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }
        //Si ya no existen talones abiertos se cierra el viaje
        if ($a['success']) {
                $sql = "SELECT * FROM trTalonesViajesTbl ".
                        " WHERE idViajeTractor = ".$_REQUEST['trap446IdViajeHdn'].
                        " AND claveMovimiento = 'TF'";
                //echo "$sql<br>";
                $movTalonesRst = fn_ejecuta_query($sql);
                //var_dump($movTalonesRst);
                if($movTalonesRst['records'] == 0)
                {
                        $sql = "UPDATE trViajesTractoresTbl SET claveMovimiento = 'VE' WHERE idViajeTractor = ".$_REQUEST['trap446IdViajeHdn'];
                        //echo "$sql<br>";
                        fn_ejecuta_query($sql);                             
                                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sql;
                    }       
                }                           
        }
        
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function cancelarUnidadTalon($idTalon, $vin){
        //Cancelacion unitaria de unidad en un talón
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idTalon == ""){
            $e[] = array('id'=>'trap446IdTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($vin == ""){
            $e[] = array('id'=>'trap446VinHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success'] == true){
            $sqlGetUltimoAM = "SELECT hu.idHistorico FROM alHistoricoUnidadesTbl hu ".
                                "WHERE hu.vin = '".$vin."' ".
                                "AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) FROM alHistoricoUnidadesTbl hu2 ".
                                    "WHERE hu2.vin = hu.vin AND hu2.claveMovimiento = 'AM')";

            $rsAM = fn_ejecuta_query($sqlGetUltimoAM);

            if(sizeof($rsAM['root']) > 0){
                $sqlUpdHistorico = "UPDATE alHistoricoUnidadesTbl SET ".
                                    "claveMovimiento = 'CA', ".
                                    "fechaEvento = '".date("Y-m-d H:i:s")."' ".
                                    "WHERE idHistorico = ".$rsAM['root'][0]['idHistorico'];

                fn_ejecuta_query($sqlUpdHistorico);

                if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $sqlUpdHistorico = "UPDATE alUltimoDetalleTbl SET ".
                                        "claveMovimiento = (SELECT hu.claveMovimiento FROM alhistoricounidadestbl hu ".
                                            "WHERE hu.claveMovimiento IN (SELECT g2.valor FROM caGeneralesTbl g2 ".
                                                "WHERE g2.tabla = 'alHistoricoUnidadesTbl' AND g2.columna = 'validos' ".
                                                "AND g2.valor = hu.claveMovimiento) ".
                                            "AND hu.vin = '".$vin."' ORDER BY hu.fechaEvento DESC LIMIT 1), ".
                                        "fechaEvento = '".date("Y-m-d H:i:s")."' ".
                                        "WHERE vin = '".$vin."'";

                    fn_ejecuta_query($sqlUpdHistorico);

                    if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        $sqlCancelarUnidadTalonStr = "UPDATE trUnidadesDetallesTalonesTbl ".
                                                     "SET estatus = 'C' ".
                                                     "WHERE idTalon = ".$idTalon." ".
                                                     "AND vin = '".$vin."'";

                        $rs = fn_ejecuta_query($sqlCancelarUnidadTalonStr);

                        if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                            $sqlUpdNumUnidadesTalon =   "UPDATE trTalonesViajesTbl tv ".
                                                        "SET tv.numeroUnidades = (SELECT COUNT(*) FROM trUnidadesDetallesTalonesTbl ut ".
                                                            "WHERE ut.idTalon = ".$idTalon." AND ut.estatus != 'C') ".
                                                        "WHERE tv.idTalon = ".$idTalon;

                            fn_ejecuta_query($sqlUpdNumUnidadesTalon);

                            if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                                $a['successMessage'] = getCancelarUnidadSuccessMsg();
                            }
                        } else {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelarUnidadTalonStr;
                        }
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelarUnidadTalonStr;
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelarUnidadTalonStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = "Estatus de Asignación a Madrina (AM) no encontrado";
            }
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

        function cancelarUnidadTalonModificacion($idTalon){
        //Cancelacion unitaria de unidad en un talón
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idTalon == ""){
            $e[] = array('id'=>'trap446IdTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success'] == true){


            $sqlConsultaUnidadesTalon = "SELECT udt.vin FROM trUnidadesDetallesTalonesTbl udt ".
                                "WHERE udt.idTalon = '".$idTalon."' ";

            $rsCU = fn_ejecuta_query($sqlConsultaUnidadesTalon);

            for ($i=0; $i < sizeof($rsCU['root']) ; $i++) { 

            $sqlGetUltimoAM = "SELECT hu.idHistorico FROM alHistoricoUnidadesTbl hu ".
                                "WHERE hu.vin = '".$rsCU['root'][$i]['vin']."' ".
                                "AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) FROM alHistoricoUnidadesTbl hu2 ".
                                    "WHERE hu2.vin = hu.vin AND hu2.claveMovimiento = 'AM')";

            $rsAM = fn_ejecuta_query($sqlGetUltimoAM);

            $sqlCot="SELECT * FROM alcotizacionesespecialestbl WHERE vin ='".$rsCU['root'][$i]['vin']."' AND numCotizacion=(SELECT MAX(numCotizacion) FROM alcotizacionesespecialestbl where vin='".$rsCU['root'][$i]['vin']."')";
            $rsCot=fn_ejecuta_query($sqlCot);


            if (sizeof($rsCot['root']) !='0') {
                $upd="UPDATE alcotizacionesespecialestbl SET estatus='CZ' WHERE idCotizacion='".$rsCot['root'][0]['idCotizacion']."'";
                fn_ejecuta_query($upd);
            }

            if(sizeof($rsAM['root']) > 0){
                $sqlUpdHistorico = "UPDATE  alHistoricoUnidadesTbl  SET claveMovimiento='CA',usuario='".$_SESSION['idUsuario']."' ".
                                    "WHERE vin='".$rsCU['root'][$i]['vin']."' and claveMovimiento = 'AM' ".
                                    //"fechaEvento = '".date("Y-m-d H:i:s")."' ".
                                    "AND idHistorico = ".$rsAM['root'][0]['idHistorico'];

                fn_ejecuta_query($sqlUpdHistorico);

                if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $sqlUpdHistorico = "UPDATE alUltimoDetalleTbl SET ".
                                        "claveMovimiento = (SELECT hu.claveMovimiento FROM alhistoricounidadestbl hu ".
                                            "WHERE hu.claveMovimiento IN (SELECT g2.valor FROM caGeneralesTbl g2 ".
                                                "WHERE g2.tabla = 'alHistoricoUnidadesTbl' AND g2.columna = 'validos' ".
                                                "AND g2.valor = hu.claveMovimiento) ".
                                            "AND hu.vin = '".$rsCU['root'][$i]['vin']."' ORDER BY hu.fechaEvento DESC LIMIT 1), ".
                                        "fechaEvento = '".date("Y-m-d H:i:s")."' ".
                                        "WHERE vin = '".$rsCU['root'][$i]['vin']."'";

                    fn_ejecuta_query($sqlUpdHistorico);

                    if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        $sqlCancelarUnidadTalonStr = "UPDATE trUnidadesDetallesTalonesTbl ".
                                                     "SET estatus = 'C' ".
                                                     "WHERE idTalon = ".$idTalon." ".
                                                     "AND vin = '".$rsCU['root'][$i]['vin']."'";

                        $rs = fn_ejecuta_query($sqlCancelarUnidadTalonStr);

                        if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                            $sqlUpdNumUnidadesTalon =   "UPDATE trTalonesViajesTbl tv ".
                                                        "SET tv.numeroUnidades = (SELECT COUNT(*) FROM trUnidadesDetallesTalonesTbl ut ".
                                                            "WHERE ut.idTalon = ".$idTalon." AND ut.estatus != 'C') ".
                                                        "WHERE tv.idTalon = ".$idTalon;

                            fn_ejecuta_query($sqlUpdNumUnidadesTalon);

                            if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                                $a['successMessage'] = getCancelarUnidadSuccessMsg();
                            }
                        } else {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelarUnidadTalonStr;
                        }
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelarUnidadTalonStr;
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelarUnidadTalonStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = "Estatus de Asignación a Madrina (AM) no encontrado";
            }

          }
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function comprobacionViaje(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap460IdViajeHdn'] == ""){
            $e[] = array('id'=>'trap460IdViajeHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap460ConceptoHdn'] == ""){
            $e[] = array('id'=>'trap460ConceptoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap460CompaniaTractorHdn'] == ""){
            $e[] = array('id'=>'trap460CompaniaTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap460ImporteHdn'] == ""){
            $e[] = array('id'=>'trap460ImporteHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap460ObservacionesHdn'] == ""){
            $e[] = array('id'=>'trap460ObservacionesHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap460ClaveMovimientoHdn'] == ""){
            $e[] = array('id'=>'trap460ClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap460CuentaContableHdn'] == ""){
            $e[] = array('id'=>'trap460CuentaContableHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap460UsuarioAsignarHdn'] == ""){
            $e[] = array('id'=>'trap460UsuarioAsignarHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap460FolioHdn'] == ""){
            $e[] = array('id'=>'trap460FolioHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if ($a['success'] ==  true) {
            $sqlAddGastoStr = "INSERT INTO trGastosViajeTractorTbl ".
                              "(idViajeTractor,concepto,centroDistribucion,folio,fechaEvento,cuentaContable,".
                                "mesAfectacion,importe,observaciones,claveMovimiento,usuario,ip) ".
                              "VALUES (".
                                $_REQUEST['trap460IdViajeHdn'].",".
                                "'".$_REQUEST['trap460ConceptoHdn']."',".
                                "'".$_SESSION['usuCompania']."',".
                                "'".$_REQUEST['trap460FolioHdn']."',".
                                "'".date("Y-m-d H:i:s")."',".
                                "'".$_REQUEST['trap460CuentaContableHdn']."',".
                                "'0',".
                                $_REQUEST['trap460ImporteHdn'].",".
                                "'".$_REQUEST['trap460ObservacionesHdn']."',".
                                "'".$_REQUEST['trap460ClaveMovimientoHdn']."',".
                                $_REQUEST['trap460UsuarioAsignarHdn'].",".
                                "'".$_SERVER['REMOTE_ADDR']."')";

            $rs = fn_ejecuta_query($sqlAddGastoStr);

            if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $sqlUpdEstatusViajeStr = "UPDATE trViajesTractoresTbl ".
                                         "SET claveMovimiento = '".$_REQUEST['trap460ClaveMovimientoHdn']."' ".
                                         "WHERE idViajeTractor = ".$_REQUEST['trap460IdViajeHdn'];

                $rs = fn_ejecuta_query($sqlUpdEstatusViajeStr);

                if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['successMessage'] = getComprobacionViajeSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdEstatusViajeStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddGastoStr;
            }
        } else {
          $a['errorMessage'] = getErrorRequeridos();
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getArbolTalones(){
        $lsWhereStr = "";

        $sqlGetMenuDetalleStr = "SELECT  B.idViajeTractor,A.*,B.folio,B.distribuidor,c.nombre
                                FROM trunidadesdetallestalonestbl A,trtalonesviajestbl B, cageneralestbl C
                                WHERE A.IDTALON = B.IDTALON
                                AND B.tipoTalon = C.valor
                                AND B.claveMovimiento != 'TX'
                                AND C.tabla = 'trTalonesViajesTbl'
                                AND C.columna = 'tipoTalon'
                                AND A.estatus != 'CA'
                                AND B.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']."
                                ORDER BY folio;";

      $rs = fn_ejecuta_query($sqlGetMenuDetalleStr);
      echo json_encode($rs);
    }

    function getTalonDetalleUnidades(){
      $lsWhereStr = "WHERE te.vin = au.vin AND te.idViajeTractor = tt.idViajeTractor ".
                    "AND au.distribuidor = tt.distribuidor AND cd.distribuidorCentro = au.distribuidor";

      if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "te.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      }
      if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTalonHdn'], "tt.idTalon", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      }
      if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresFolioTxt'], "tt.folio", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      }
      if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresDistribuidorHdn'], "au.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      }


      $sqlGetTalonDetalleUnidadesStr = "SELECT te.*, au.distribuidor, tt.folio AS folioTalon, tt.idTalon, cd.descripcionCentro ".
                                       "FROM trunidadesembarcadastbl te, ".
                                       "alunidadestbl au, trtalonesviajestbl tt, caDistribuidoresCentrosTbl cd ".$lsWhereStr;

      $rs = fn_ejecuta_query($sqlGetTalonDetalleUnidadesStr);
      echo json_encode($rs);
    }

    function addContinuidadEspecial(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap446IdViajeHdn'] == ""){
            $e[] = array('id'=>'trap446IdViajeHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap446IdTalonHdn'] == ""){
            $e[] = array('id'=>'trap446IdTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap446DistribuidorHdn'] == ""){
            $e[] = array('id'=>'trap446DistribuidorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap446NumeroUnidadesHdn'] == ""){
            $e[] = array('id'=>'trap446NumeroUnidadesHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap446DireccionEntregaHdn'] == ""){
            $e[] = array('id'=>'trap446DireccionEntregaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap446CompaniaRemitente'] == ""){
            $e[] = array('id'=>'trap446CompaniaRemitente','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trap446TipoTalonHdn'] == ""){
            $e[] = array('id'=>'trap446TipoTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $vinArr = explode('|', substr($_REQUEST['trap446VinHdn'], 0, -1));
        if(in_array('', $vinArr)){
            $e[] = array('id'=>'trap446VinHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $simboloArr = explode('|', substr($_REQUEST['trap446SimboloUnidadHdn'], 0, -1));
        if(in_array('', $simboloArr)){
            $e[] = array('id'=>'trap446SimboloUnidadHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $colorArr = explode('|', substr($_REQUEST['trap446ColorHdn'], 0, -1));
        if(in_array('', $colorArr)){
            $e[] = array('id'=>'trap446ColorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success']){
            //Que tipo de tarifa maneja el talon
            $sqlGetTarifaUnidadesStr = "SELECT tf.tarifa ".
                                        "FROM trUnidadesDetallesTalonesTbl ut, alUltimoDetalleTbl hu, caTarifasTbl tf ".
                                        "WHERE ut.vin = hu.vin ".
                                        "AND hu.fechaEvento = (SELECT hu2.fechaEvento ".
                                            "FROM alUltimoDetalleTbl hu2 ".
                                            "WHERE hu2.vin = ut.vin) ".
                                        "AND hu.idTarifa = tf.idTarifa ";

            $rs = fn_ejecuta_query($sqlGetTarifaUnidadesStr);
            $tarifaUnidades = $rs['root'][0]['tarifa'];

            if($tarifaUnidades == '13'){// || $tarifaUnidades == '00'
                $tipoTalon = 'TE';
            } else {
                $tipoTalon = 'TN';
            }

            $sqlGetNumeroTalon = "SELECT ifnull(SUM(numeroTalon + 1),1) AS numeroTalon ".
                                    "FROM trtalonesviajestmp tmp1 ".
                                    "WHERE tmp1.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ".
                                    "AND numeroTalon = (SELECT max(numeroTalon) FROM trtalonesviajestmp tmp2 WHERE tmp2.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ) ";

            //$_REQUEST['trap446IdViajeHdn']

            $rs = fn_ejecuta_query($sqlGetNumeroTalon);
            $numeroTalon = $rs['root'][0]['numeroTalon'];

            //Se insertan los datos en los talones temporales relacionados al viaje
            $sqlAddTalonTempStr = "INSERT INTO trTalonesViajesTmp (idViajeTractor, numeroTalon, distribuidor, numeroUnidades,".
                                    "direccionEntrega, companiaRemitente,centroDistribucion, observaciones, tipoTalon, tarifaUnidades, bloqueado, idUsuario) VALUES(".
                                    $_REQUEST['trap446IdViajeHdn'].",".
                                    $numeroTalon.",".
                                    "'".$_REQUEST['trap446DistribuidorHdn']."',".
                                    $_REQUEST['trap446NumeroUnidadesHdn'].",".
                                    $_REQUEST['trap446DireccionEntregaHdn'].",".
                                    "'".$_REQUEST['trap446CompaniaRemitente']."',".
                                    "'".$_REQUEST['centroDistribucion']."',".
                                    "'".$_REQUEST['trap446ObservacionesHdn']."',".
                                    "'".$tipoTalon."',".
                                    "'".$tarifaUnidades."',".
                                    "1, ". //El talon es temporal pero esta bloqueado
                                    $_SESSION['idUsuario'].")";

            fn_ejecuta_query($sqlAddTalonTempStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                if(!isset($_REQUEST['centroDistribucion']))
                {
                        $_REQUEST['centroDistribucion'] = ' ';
                }
                for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                    $sqlAddUnidadesTalonTempStr =   "INSERT INTO trUnidadesDetallesTalonesTmp (numeroTalon, avanzada, vin,".
                                                        "simbolo, color, centroDistribucion, idUsuario) VALUES (".
                                                    $numeroTalon.",".
                                                    "'".substr($vinArr[$nInt], -8)."',".
                                                    "'".$vinArr[$nInt]."',".
                                                    "'".$simboloArr[$nInt]."',".
                                                    "'".$colorArr[$nInt]."', '".
                                                    $_REQUEST['centroDistribucion']."',".
                                                    $_SESSION['idUsuario'].")";

                    fn_ejecuta_query($sqlAddUnidadesTalonTempStr);

                    if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ''){
                        $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddUnidadesTalonTempStr;
                        unset($a['successMessage']);
                        break;
                    } else {
                       $sqlBloquearUnidadStr = "INSERT INTO alUnidadesTmp (vin,avanzada,modulo,idUsuario,ip,fecha) VALUES (".
                                                "'".$vinArr[$nInt]."', ".
                                                "'".substr($vinArr[$nInt], -8)."',".
                                                "'TRAP 446',".
                                                "'".$_SESSION['idUsuario']."',".
                                                "'".$_SERVER['REMOTE_ADDR']."',".
                                                "'".date('Y-m-d H:i:s')."')";

                        fn_ejecuta_query($sqlBloquearUnidadStr);

                        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                            if($a['successMessage'] == ""){
                                $a['successMessage'] = getTalonesViajeAddMsg();
                            }
                        } else {
                            $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlBloquearUnidadStr;
                            unset($a['successMessage']);
                            break;
                        }
                    }
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddTalonTempStr;
            }

        } else {
          $a['errorMessage'] = getErrorRequeridos();
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);

    }

    //VIAJES TEMPORALES
    function addViajeTemp() {
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trViajesTractoresIdTractorHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresIdTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trViajesTractoresClaveChoferHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresClaveChoferHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success'] == true){
            $sqlGetNextViajeTemp = "SELECT IFNULL(MAX(idViajeTractor)+1, 1) AS viaje FROM trViajesTractoresTmp ";

            $rs = fn_ejecuta_query($sqlGetNextViajeTemp);
            $viaje = $rs['root'][0]['viaje'];
            //Revisa si el tractor ya estaba bloqueado con anterioridad
            $sqlCheckTractorBloqueadoStr = "SELECT 1 FROM trViajesTractoresTmp ".
                                            "WHERE idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'];

            $rs = fn_ejecuta_query($sqlCheckTractorBloqueadoStr);
            //echo sizeof($rs['root']);
            if(sizeof($rs['root']) == 0){
                $sqlAddUpdViajeTempStr = "INSERT INTO trViajesTractoresTmp (idViajeTractor, idTractor, claveChofer, ".
                                            "centroDistribucion, modulo, ip, fechaEvento) VALUES (".
                                            $viaje.",".
                                            $_REQUEST['trViajesTractoresIdTractorHdn'].",".
                                            $_REQUEST['trViajesTractoresClaveChoferHdn'].",".
                                            replaceEmptyNull("'".$_REQUEST['trViajesTractoresCentroDistHdn']."'").",".
                                            replaceEmptyNull("'".$_REQUEST['catTractoresModuloTxt']."'").",".
                                            $_SESSION['idUsuario'].",".
                                            "'".$_SERVER['REMOTE_ADDR']."',".
                                            "'".date("Y-m-d H:i:s")."')";

                $rs = fn_ejecuta_query($sqlAddUpdViajeTempStr);
            } else {
                $sqlAddUpdViajeTempStr = "UPDATE trViajesTractoresTmp SET ".
                                        "idViajeTractor = ".$viaje.", ".
                                        "claveChofer = ".$_REQUEST['trViajesTractoresClaveChoferHdn'].", ".
                                        "centroDistribucion = ".replaceEmptyNull("'".$_REQUEST['trViajesTractoresCentroDistHdn']."'")." ".
                                        "WHERE idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'];

                $rs = fn_ejecuta_query($sqlAddUpdViajeTempStr);

            }

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlAddUpdViajeTempStr;
                $a['successMessage'] = getViajeSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUpdViajeTempStr;
            }
        } else {
          $a['errorMessage'] = getErrorRequeridos();
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getViajeTemp(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "idTractor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveChoferHdn'], "claveChofer", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetViajeTempStr = "SELECT * FROM trViajesTractoresTmp ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetViajeTempStr);

        echo json_encode($rs);
    }

    function dltViajeTemp(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trViajesTractoresIdTractorHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresIdTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }

        if($a['success'] == true){
            $sqlDltViajeTempStr = "DELETE FROM trViajesTractoresTmp WHERE idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'];

            fn_ejecuta_query($sqlDltViajeTempStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlDltViajeTempStr;
                $a['successMessage'] = getViajeTempDltMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltViajeTempStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    //OBTIENE TANTO LOS TALONES EN TEMPORAL COMO LOS QUE EXISTAN
    function getTalonesTemp() {
        $sqlGetTalonesTempStr = "";
        if(!isset($_REQUEST['centroDistribucion']))
        {
                $_REQUEST['centroDistribucion'] = '';
        }

        if($_REQUEST['trap446SoloTalonesTemp'] == '0' || $_REQUEST['trap446SoloTalonesTemp'] == ''){
            $sqlGetTalonesTempStr .= "SELECT tv.idViajeTractor, tv.idTalon, tv.distribuidor, tv.centroDistribucion, ".
                                "(SELECT tf.tarifa ".
                                    "FROM trUnidadesDetallesTalonesTbl ut, alHistoricoUnidadesTbl hu, catarifastbl tf ".
                                    "WHERE ut.vin = hu.vin ".
                                    "AND hu.idHistorico = ( ".
                                        "SELECT MAX(hu2.idHistorico) FROM alHistoricoUnidadesTbl hu2 WHERE hu2.vin = ut.vin) ".
                                    "AND tf.idTarifa = hu.idTarifa ".
                                    "AND ut.idTalon = tv.idTalon ".
                                "LIMIT 1) AS tarifaUnidades, tv.direccionEntrega, dc.tipoDistribuidor, ".
                                "dc.descripcionCentro, dc.idPlaza, pl.plaza, tv.companiaRemitente, tv.observaciones, ".
                                "tv.tipoTalon, tv.folio, tv.claveMovimiento, 1 AS bloqueado, ".
                                "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTbl tv2 WHERE tv2.idTalon = tv.idTalon) + ".
                                "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tv.idTalon ".
                                    "AND tv.claveMovimiento != 'C' AND tt2.idUsuario AND tt2.idUsuario = ".$_SESSION['idUsuario']." AND tt2.centroDistribucion = '".$_SESSION['usuCompania']."') AS numeroUnidades, ". 
                                "(SELECT kp.kilometros FROM caKilometrosPlazaTbl kp WHERE kp.idPlazaOrigen = (".
                                    "SELECT dc2.idPlaza FROM caDistribuidoresCentrosTbl dc2 WHERE dc2.distribuidorCentro = tv.centroDistribucion) ".
                                    "AND kp.idPlazaDestino = tv.idPlazaDestino) AS kilometrosTabulados, 1 AS previos ".
                                /*"(SELECT 1 ".
                                    "FROM alUnidadesDetenidasTbl ud, trUnidadesDetallesTalonesTbl ut ".
                                    "WHERE ud.vin = ut.vin AND ut.idTalon = tv.idTalon AND ud.centroLibera IS NULL) AS detenidas, ".
                                "(SELECT 1 ".
                                    "FROM alUnidadesDetenidasTbl ud, trUnidadesDetallesTalonesTbl ut ".
                                    "WHERE ud.vin = ut.vin AND ut.idTalon = tv.idTalon AND ud.centroLibera IS NOT NULL) AS detenidasServEsp ".*/
                                "FROM trTalonesViajesTbl tv, caDistribuidoresCentrosTbl dc, caPlazasTbl pl ".
                                "WHERE dc.distribuidorCentro = tv.distribuidor ".
                                "AND dc.idPlaza = pl.idPlaza ".
                                "AND tv.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ".
                                "AND tv.claveMovimiento != 'TX' ".
                                "UNION ALL ";
        }

        $sqlGetTalonesTempStr .= "SELECT tt.idViajeTractor, tt.numeroTalon AS idTalon, tt.distribuidor, tt.centroDistribucion, ".
                            "(SELECT tf.tarifa ".
                                "FROM trUnidadesDetallesTalonesTmp ut, alHistoricoUnidadesTbl hu, catarifastbl tf ".
                                "WHERE ut.vin = hu.vin ".
                                "AND hu.idHistorico = ( ".
                                    "SELECT MAX(hu2.idHistorico) FROM alHistoricoUnidadesTbl hu2 WHERE hu2.vin = ut.vin) ".
                                "AND tf.idTarifa = hu.idTarifa ".
                                "AND ut.numeroTalon = tt.numeroTalon ".
                            "LIMIT 1) AS tarifaUnidades, ".
                            "tt.direccionEntrega, dc.tipoDistribuidor, dc.descripcionCentro, dc.idPlaza, pl.plaza, ".
                            "tt.companiaRemitente, tt.observaciones, tt.tipoTalon, '' AS folio, '' AS claveMovimiento, tt.bloqueado, ".
                            "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt3 WHERE tt3.numeroTalon = tt.numeroTalon AND tt3.idUsuario = ".$_SESSION['idUsuario']." AND tt3.centroDistribucion = '".$_REQUEST['centroDistribucion']."') AS numeroUnidades, ".
                            "(SELECT kp.kilometros FROM caKilometrosPlazaTbl kp WHERE kp.idPlazaOrigen = (".
                                "SELECT dc2.idPlaza FROM caDistribuidoresCentrosTbl dc2 WHERE dc2.distribuidorCentro = tt.centroDistribucion) ".
                                "AND kp.idPlazaDestino = pl.idPlaza) AS kilometrosTabulados, 0 AS previos ".
                            /*"(SELECT 1 ".
                                "FROM alUnidadesDetenidasTbl ud, trUnidadesDetallesTalonesTmp ut ".
                                "WHERE ud.vin = ut.vin AND ut.numeroTalon = tt.numeroTalon AND ud.centroLibera IS NULL) AS detenidas, ".
                            "(SELECT 1 ".
                                "FROM alUnidadesDetenidasTbl ud, trUnidadesDetallesTalonesTmp ut ".
                                "WHERE ud.vin = ut.vin AND ut.numeroTalon = tt.numeroTalon AND ud.centroLibera IS NOT NULL) AS detenidasServEsp ".*/
                            "FROM trtalonesviajestmp tt, caDistribuidoresCentrosTbl dc, caPlazasTbl pl ".
                            "WHERE dc.distribuidorCentro = tt.distribuidor ".
                            "AND dc.idPlaza = pl.idPlaza ".
                            "AND tt.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn'];
                //echo "$sqlGetTalonesTempStr<br>";
        $rs = fn_ejecuta_query($sqlGetTalonesTempStr);
        //echo json_encode($sqlGetTalonesTempStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['distDesc'] = $rs['root'][$iInt]['distribuidor']." - ".$rs['root'][$iInt]['descripcionCentro'];
        }

        echo json_encode($rs);
    }

    function addUpdTalonesTemp() {
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap446IdViajeHdn'] == ""){
            $e[] = array('id'=>'trap446IdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        //No es requerido el id del talon porque hay talones nuevos sin ID
        $idTalonArr = explode('|', substr($_REQUEST['trap446IdTalonHdn'], 0, -1));
        $folioArr = explode('|', substr($_REQUEST['trap446FolioHdn'], 0, -1));
        $numUnidadesArr = explode('|', substr($_REQUEST['trap446NumeroUnidadesHdn'], 0, -1));

        $distArr = explode('|', substr($_REQUEST['trap446DistribuidorHdn'], 0, -1));
        if(in_array('', $distArr)){
            $e[] = array('id'=>'trap446DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $direccionArr = explode('|', substr($_REQUEST['trap446DireccionEntregaHdn'], 0, -1));
        if(in_array('', $direccionArr)){
            $e[] = array('id'=>'trap446DireccionEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $remitenteArr = explode('|', substr($_REQUEST['trap446RemitenteHdn'], 0, -1));
        if(in_array('', $remitenteArr)){
            $e[] = array('id'=>'trap446RemitenteHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            /*$sqlGetNumeroTalon = "SELECT ifnull(SUM(numeroTalon + 1),1) AS numeroTalon ".
                                    "FROM trtalonesviajestmp tmp1 ".
                                    "WHERE tmp1.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ".
                                    "AND numeroTalon = (SELECT max(numeroTalon) FROM trtalonesviajestmp tmp2 WHERE tmp2.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ) ";*/
            $sqlGetNumeroTalon =  "SELECT SUM(numeroTalon +1) as numeroTalon ".
                                    "FROM trtalonesviajestmp; "; 

            $rs = fn_ejecuta_query($sqlGetNumeroTalon);
            $numeroTalon = $rs['root'][0]['numeroTalon'];
            echo ("ESTE es el talon: ".$numeroTalon);

            for($nInt = 0; $nInt < sizeof($distArr); $nInt++){
                if($idTalonArr[$nInt] != ""){
                    $sqlAddUpdTalonStr = "INSERT INTO trTalonesViajesTmp (idViajeTractor, numeroTalon, distribuidor, ".
                                            "numeroUnidades, direccionEntrega, companiaRemitente, centroDistribucion, observaciones, tipoTalon, bloqueado, idUsuario) VALUES (".
                                            $_REQUEST['trap446IdViajeHdn'].",".
                                            $numeroTalon.", ".
                                            "'".$distArr[$nInt]."',".
                                            replaceEmptyDec($numUnidadesArr[$nInt]).",".
                                            $direccionArr[$nInt].",".
                                            "'".$remitenteArr[$nInt]."',".
                                            "'".$_SESSION['usuCompania']."',".
                                            "'".$observacionesArr[$nInt]."',".
                                            "'TN',".
                                            "0, ".
                                            $_SESSION['idUsuario'].")";

                    $rs = fn_ejecuta_query($sqlAddUpdTalonStr);

                    $numeroTalon++;
                } else {
                    if($folioArr[$nInt] == ""){
                        $sqlAddUpdTalonStr = "UPDATE trTalonesViajesTmp SET ".
                                            "distribuidor = '".$distArr[$nInt]."',".
                                            "direccionEntrega = ".$direccionArr[$nInt].",".
                                            "numeroUnidades = ".replaceEmptyDec($numUnidadesArr[$nInt]).",".
                                            "companiaRemitente = '".$remitenteArr[$nInt]."',".
                                            "observaciones = '".$observacionesArr[$nInt]."',".
                                            "tipoTalon = '".$tipoTalonArr[$nInt]."' ".
                                            "WHERE numeroTalon = ".$idTalonArr[$nInt];
                    } else {
                        $sqlAddUpdTalonStr = "UPDATE trTalonesViajesTbl SET ".
                                            "distribuidor = '".$distArr[$nInt]."',".
                                            "direccionEntrega = ".$direccionArr[$nInt].",".
                                            "numeroUnidades = ".replaceEmptyDec($numUnidadesArr[$nInt]).",".
                                            "companiaRemitente = '".$remitenteArr[$nInt]."',".
                                            "observaciones = '".$observacionesArr[$nInt]."' ".
                                            "WHERE idTalon = ".$idTalonArr[$nInt];
                    }

                    $rs = fn_ejecuta_query($sqlAddUpdTalonStr);
                }

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlAddUpdTalonStr;
                    $a['successMessage'] = getTalonesViajeAddMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUpdTalonStr;
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function cancelarTalonTemp($idTalon){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idTalon == ""){
            $e[] = array('id'=>'IdTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }
        if(!isset($_REQUEST['centroDistribucion']))
        {
                $_REQUEST['centroDistribucion'] = '';
        }

        if($a['success'] == true) {
            $sqlGetUnidadesTalonTempStr = "SELECT vin FROM trUnidadesDetallesTalonesTmp ".
                                            "WHERE numeroTalon = ".$idTalon.
                                            " AND centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
                                            " AND idUsuario = ".$_SESSION['idUsuario'];

            $rs = fn_ejecuta_query($sqlGetUnidadesTalonTempStr);

            foreach ($rs['root'] as $unidad) {
                $sqlDltTempStr = "DELETE FROM alUnidadesTmp WHERE vin = '".$unidad['vin']."'";
                fn_ejecuta_query($sqlDltTempStr);

                $sqlDltDetalleTempStr = "DELETE FROM trUnidadesDetallesTalonesTmp ".
                                        "WHERE numeroTalon = ".$idTalon." ".
                                        " AND vin = '".$unidad['vin']."'".
                                        " AND centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
                                        " AND idUsuario = ".$_SESSION['idUsuario'];
                fn_ejecuta_query($sqlDltDetalleTempStr);
            }

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $sqlDltTalonTempStr = "DELETE FROM trTalonesViajesTmp WHERE numeroTalon = ".$_REQUEST['trap446IdTalonHdn'].
                                                                            " AND centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
                                                            " AND idUsuario = ".$_SESSION['idUsuario'];

                $rs = fn_ejecuta_query($sqlDltTalonTempStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlUpdUnidadStr;
                    $a['successMessage'] = getTalonesViajeDltMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltTalonTempStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'];
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function dltTalonesTempMasivo($talones, $claveMovimiento){
        $a = array();
        $e = array();
        $a['success'] = true;

        $idTalonArr = explode('|', substr($talones, 0, -1));
        if(in_array('', $idTalonArr)){
            $e[] = array('id'=>'IdTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }
        $claveMovArr = explode('|', substr($claveMovimiento, 0, -1));

        if($a['success'] == true){
            for ($nInt=0; $nInt < sizeof($idTalonArr); $nInt++) {
                $sqlGetUnidadesTalonStr = "SELECT vin FROM trUnidadesDetallesTalonesTmp ".
                                            "WHERE numeroTalon = ".$idTalonArr[$nInt].
                                            " AND centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
                                            " AND idUsuario = ".$_SESSION['idUsuario'];

                $rs = fn_ejecuta_query($sqlGetUnidadesTalonStr);

                if(sizeof($rs['root']) > 0){
                    for ($mInt=0; $mInt < sizeof($rs['root']); $mInt++) {
                        $data = dltUnidadTalonTemp($idTalonArr[$nInt], $rs['root'][$mInt]['vin']);
                    }
                } else {
                    $data = array('success' => true);
                }

                if($data['success'] == true){
                    if($claveMovArr[$nInt] == ""){
                        $sqlDltTalonTempStr = "DELETE FROM trTalonesViajesTmp ".
                                                "WHERE numeroTalon = ".$idTalonArr[$nInt].
                                                " AND centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
                                                " AND idUsuario = ".$_SESSION['idUsuario'];

                        fn_ejecuta_query($sqlDltTalonTempStr);

                        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                            $a['sql'] = $sqlDltTalonTempStr;
                        } else {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltTalonTempStr;
                        }
                    } else {
                        $a['sql'] = $sqlDltUnidadesTalonTempStr;
                        $a['successMessage'] = getTalonesViajeDltMsg();
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltUnidadesTalonTempStr;
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    //Obtiene unidades del talon de la tabla correspondiente, dependiendo de la bandera
    function getUnidadesTalonTemp() {
        if($_REQUEST['trap446FolioHdn'] != ''){
            $sqlGetUnidadesTalonStr = "SELECT ud.idTalon, ud.vin, un.avanzada, un.simboloUnidad, un.color, ".
                                      "co.descripcion AS nombreColor, su.descripcion AS nombreSimbolo, ".
                                      "tf.tarifa, tf.tipoTarifa, tf.descripcion AS nombreTarifa, CONCAT(tf.tarifa, ' - ', tf.descripcion) AS descrTarifa, 1 AS bandera ".
                                      "FROM trUnidadesDetallesTalonesTbl ud, alUnidadesTbl un, caColorUnidadesTbl co, ".
                                      "caSimbolosUnidadesTbl su, caTarifasTbl tf, alHistoricoUnidadesTbl hu ".
                                      "WHERE ud.vin = un.vin ".
                                      "AND hu.vin = ud.vin ".
                                      "AND hu.idHistorico = (SELECT MAX(hu2.idHistorico) FROM alHistoricoUnidadesTbl hu2 ".
                                      "WHERE hu2.vin = hu.vin) ".
                                      "AND co.color = un.color ".
                                      "AND su.marca = co.marca ".
                                      "AND su.simboloUnidad = un.simboloUnidad ".
                                      "AND tf.idTarifa = hu.idTarifa ".
                                      "AND ud.idTalon = ".$_REQUEST['trap446IdTalonHdn']." ".
                                      "AND ud.estatus != 'C' ".
                                      "ORDER BY ud.idTalon ";
        } else {
            $addTabla = "";
            $andAux = "";
            if(isset($_REQUEST['centroDistribucion']) && $_REQUEST['centroDistribucion'] != '')
            {
                    $addTabla = ", trTalonesViajesTmp tv ";
                    $andAux = "AND tv.numeroTalon = ud.numeroTalon ".
                                        "AND tv.centroDistribucion = '".$_REQUEST['centroDistribucion']."' ".
                                        "AND tv.centroDistribucion = ud.centroDistribucion ".
                                        "AND tv.idUsuario = ".$_SESSION['idUsuario']." ".
                                        "AND tv.idUsuario = ud.idUsuario ";
                    if(isset($_REQUEST['modulo']) && $_REQUEST['modulo'] != '')
                    {
                            $addTabla .= ", alUnidadesTmp ut ";
                            $andAux .= " AND ut.modulo = '".$_REQUEST['modulo']."' ".
                                                    "AND ut.vin = ud.vin ".
                                                    "AND ut.idUsuario = ".$_SESSION['idUsuario']." ".
                                                    "AND ut.idUsuario = ud.idUsuario ";
                    }
                    //echo "$andAux<br>";
            }
            $sqlGetUnidadesTalonStr = "SELECT ud.numeroTalon AS idTalon, ud.vin, un.avanzada, un.simboloUnidad, un.color, ".
                                      "co.descripcion AS nombreColor, su.descripcion AS nombreSimbolo, ".
                                      "tf.tarifa, tf.tipoTarifa, tf.descripcion AS nombreTarifa, CONCAT(tf.tarifa, ' - ', tf.descripcion) AS descrTarifa, 0 AS bandera ".
                                      "FROM trUnidadesDetallesTalonesTmp ud, alUnidadesTbl un, caColorUnidadesTbl co, ".
                                      "caSimbolosUnidadesTbl su, caTarifasTbl tf, alHistoricoUnidadesTbl hu ". $addTabla.
                                      "WHERE ud.vin = un.vin ".
                                      "AND hu.vin = ud.vin ".
                                      "AND hu.idHistorico = (SELECT MAX(hu2.idHistorico) FROM alHistoricoUnidadesTbl hu2 ".
                                      "WHERE hu2.vin = hu.vin) ".
                                      "AND co.color = un.color ".
                                                                            $andAux.
                                      "AND su.marca = co.marca ".
                                      "AND su.simboloUnidad = un.simboloUnidad ".
                                      "AND tf.idTarifa = hu.idTarifa ".
                                      "AND ud.numeroTalon = ".$_REQUEST['trap446IdTalonHdn']." ".
                                      "ORDER BY ud.numeroTalon ";
        }

        $rs = fn_ejecuta_query($sqlGetUnidadesTalonStr);

        for ($i = 0; $i < count($rs['root']); $i++) {
            $rs['root'][$i]['descColor'] = $rs['root'][$i]['color']." - ".$rs['root'][$i]['nombreColor'];
            $rs['root'][$i]['descSimbolo'] = $rs['root'][$i]['simboloUnidad']." - ".$rs['root'][$i]['nombreSimbolo'];
            $rs['root'][$i]['descTarifa'] = $rs['root'][$i]['tarifa']." - ".$rs['root'][$i]['nombreTarifa'];
        }

        return $rs;
    }

    function addUnidadesTalonTemp() {
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap446IdTalonHdn'] == ""){
            $e[] = array('id'=>'trap446IdTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }
        if($_REQUEST['trap446VinHdn'] == ""){
            $e[] = array('id'=>'trap446VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }
        if($_REQUEST['trap446SimboloUnidadHdn'] == ""){
            $e[] = array('id'=>'trap446SimboloUnidadHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }
        if($_REQUEST['trap446ColorHdn'] == ""){
            $e[] = array('id'=>'trap446ColorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }

        if ($a['success'] == true) {
            $data = bloquearUnidad($_REQUEST['trap446VinHdn'],substr($_REQUEST['trap446VinHdn'], -8));
            if($data['success'] == true) {
                if(!isset($_REQUEST['centroDistribucion']))
                {
                        $_REQUEST['centroDistribucion'] = ' ';
                }                
                $sqlAddUnidadesTalonTempStr =   "INSERT INTO trUnidadesDetallesTalonesTmp (numeroTalon, avanzada, vin,".
                                                        "simbolo, color, centroDistribucion, idUsuario) VALUES (".
                                                $_REQUEST['trap446IdTalonHdn'].",".
                                                "'".substr($_REQUEST['trap446VinHdn'], -8)."',".
                                                "'".$_REQUEST['trap446VinHdn']."',".
                                                "'".$_REQUEST['trap446SimboloUnidadHdn']."',".
                                                "'".$_REQUEST['trap446ColorHdn']."',".
                                                "'".$_REQUEST['centroDistribucion']."',".
                                                $_SESSION['idUsuario'].")";
                $rs = fn_ejecuta_query($sqlAddUnidadesTalonTempStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlAddUnidadesTalonTempStr;
                    $a['successMessage'] = getUnidadTalonSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUnidadesTalonTempStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $data['sql'];
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function dltUnidadTalonTemp($idTalon, $vin){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idTalon == ""){
            $e[] = array('id'=>'IdTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }
        if($vin == ""){
            $e[] = array('id'=>'VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }

        if ($a['success'] == true) {
            $sqlDltUnidadesTalonTempStr = "DELETE FROM trUnidadesDetallesTalonesTmp ".
                                            "WHERE numeroTalon = ".$idTalon." ".
                                            "AND vin = '".$vin."'".
                                            "AND idUsuario = ".$_SESSION['idUsuario']." ".
                                            "AND centroDistribucion = '".$_SESSION['usuCompania']."'";

            $rs = fn_ejecuta_query($sqlDltUnidadesTalonTempStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $data = liberarUnidad($vin);
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltUnidadesTalonTempStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }
    function dltUnidadTalonTempMasivo($idTalonArr, $liberaUnidades){
        $a = array();
        $e = array();
        $a['success'] = true;

        $idTalonArr = explode('|', $idTalonArr);
        if(in_array('', $idTalonArr)){
            $e[] = array('id'=>'IdTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = true;
        }
        if ($a['success'] == true) {


            $sqlLiberaUnidades = "DELETE FROM alUnidadesTmp WHERE vin IN (SELECT vin FROM trUnidadesDetallesTalonesTmp ".
                                 "WHERE numeroTalon IN (";

            $sqlDltUnidadesTempStr = "DELETE FROM trUnidadesDetallesTalonesTmp WHERE numeroTalon IN (";

            for ($i=0; $i < count($idTalonArr) ; $i++) {
                if($i > 0){
                    $sqlDltUnidadesTempStr .= ",";
                    $sqlLiberaUnidades .= ",";
                }
                $sqlDltUnidadesTempStr .= $idTalonArr[$i];
                $sqlLiberaUnidades .= $idTalonArr[$i];
            }
            $sqlDltUnidadesTempStr .= ");";
            $sqlLiberaUnidades .= "));";

            if($liberaUnidades != ""){
                $rs = fn_ejecuta_query($sqlLiberaUnidades);
            }
            $rs = fn_ejecuta_query($sqlDltUnidadesTempStr);



            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] = 'Se Elimino Correctamente las unidades';
                $a['sql'] = $sqlDltUnidadesTempStr;
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltUnidadesTempStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function updChoferesEsperaReacomodo(){

        $a['success'] = true;
        $e = array();


        $centroDistArr = explode('|', $_REQUEST['tiempoEsperaCentroDistribucionTxt']);
        $claveChoferArr = explode('|', $_REQUEST['tiempoEsperaClaveChoferTxt']);
        $idTractorArr = explode('|', $_REQUEST['tiempoEsperaIdTractorTxt']);

        if(in_array('', $centroDistArr)){
            $e[] = array('id'=>'centroDistribucion', 'msg'=>getRequerido());
            $a['success'] = false;
        }
        if(in_array('', $claveChoferArr)){
            $e[] = array('id'=>'claveChofer', 'msg'=>getRequerido());
            $a['success'] = false;
        }
        if(in_array('', $idTractor)){
            $e[] = array('id'=>'idTractor', 'msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success'] == true){
            for($i = 0, $consec = 0; $i < count($idTractorArr); $i++){
                if($idTractorArr[$i] != 'GASTO'){

                    $sqlUpdateMassive = "UPDATE trEsperaChoferesTbl SET consecutivo = ".strval($consec + 1).
                                        " WHERE centroDistribucion = '".$centroDistArr[$i].
                                        "' AND idTractor = ".$idTractorArr[$i].
                                        " AND claveChofer = ".$claveChoferArr[$i];
                    $consec++;
                    $rs = fn_ejecuta_query($sqlUpdateMassive);
                }
                //VG Si es con Gastos no avanza consecutivo y ahí se queda
            }
        }
        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
            $a['success'] = true;
            $a['successMessage'] = getReacomodoChofer();
        } else {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltUnidadesTalonTempStr;
        }
        $a['errors'] = $e;
        echo json_encode($a);

    }

    function addPreviaje(){
        $a = array();
        $e = array();
        $a['success'] = true;

        //echo 'uni '.$_REQUEST['trViajesTractoresUnidadesHdn'].'</br>';
        //return;

        if($_REQUEST['trViajesTractoresIdTractorHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresIdTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['trViajesTractoresClaveChoferHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresClaveChoferHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        /*if($_REQUEST['trViajesTractoresClaveMovimientoHdn'] == ""){
            $e[] = array('id'=>'trViajesTractoresClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if ($_REQUEST['trViajesTractoresClaveMovimientoTalonHdn'] == "") {
            $e[] = array('id'=>'trViajesTractoresClaveMovimientoTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if ($_REQUEST['trViajesTractoresTipoDoctoHdn'] == "") {
            $e[] = array('id'=>'trViajesTractoresTipoDoctoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }*/
        if ($_REQUEST['trViajesTractoresCompaniaHdn'] == "") {
            $e[] = array('id'=>'trViajesTractoresCompaniaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        /*if ($_REQUEST['trViajesTractoresTipoTalonHdn'] == "") {
            $e[] = array('id'=>'trViajesTractoresTipoTalonHdn','msg'=>getRequerido());
            $a['success'] = false;
        }*/

        //TALONES -- Inserción Masiva
        $distArr = explode("|", $_REQUEST['trViajesTractoresDistribuidorHdn']);
        if (in_array("", $distArr)) {
            $e[] = array('id'=>'trViajesTractoresDistribuidorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $obsArr = explode("|", $_REQUEST['trViajesTractoresObservacionesTxt']);
        if (in_array("", $obsArr)) {
            $e[] = array('id'=>'trViajesTractoresObservacionesTxt','msg'=>getRequerido());
            $a['success'] = false;
        }
        $direccionArr = explode("|", $_REQUEST['trViajesTractoresDireccionHdn']);
        if (in_array("", $direccionArr)) {
            $e[] = array('id'=>'trViajesTractoresDireccionHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $destinoArr = explode("|", $_REQUEST['trViajesTractoresDestinoHdn']);
        if (in_array("", $destinoArr)) {
            $e[] = array('id'=>'trViajesTractoresDestinoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $unidadesArr = explode("|", $_REQUEST['trViajesTractoresUnidadesHdn']);
        if (in_array("", $unidadesArr)) {
            $e[] = array('id'=>'trViajesTractoresUnidadesHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $remitenteArr = explode("|", $_REQUEST['trViajesTractoresRemitenteHdn']);
        if (in_array("", $remitenteArr)) {
            $e[] = array('id'=>'trViajesTractoresRemitenteHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $kilometrosArr = explode("|", $_REQUEST['trViajesTractoresKmTabuladosTxt']);
        if (in_array("", $kilometrosArr)) {
            $e[] = array('id'=>'trViajesTractoresKmTabuladosTxt','msg'=>getRequerido());
            $a['success'] = false;
        }
        $vinArr = explode ("|", $_REQUEST['trViajesTractoresVinHdn']);
        $distArr = explode ("|", $_REQUEST['trViajesTractoresDistribuidorHdn']);
        /*if(in_array("", $vinArr)){
            $e[] = array('id'=> "trViajesTractoresVinHdn", 'msg'=>getRequerido());
            $a['success'] = false;
        }*/
        //Checar el destino más largo
        $plazaDestino = -1;
        $kmTabulados = 0;
        for ($iInt=0; $iInt < sizeof($kilometrosArr); $iInt++) {
            if ($kmTabulados <= $kilometrosArr[$iInt]) {
                $plazaDestino = $destinoArr[$iInt];
                $kmTabulados = $kilometrosArr[$iInt];
            }
        }

        $totalUnidades = '';
        if($_REQUEST['trViajesTractoresTotalUnidadesHdn'] == 0){
            $totalUnidades =  array_sum($unidadesArr);
        } else {
            $totalUnidades = $_REQUEST['trViajesTractoresTotalUnidadesHdn'];
        }
        //1.- Insertar en trViajesTractoresTbl
        if($a['success'] == true){
            if($plazaDestino > 0){

                  $sqlIdViaje="SELECT max(idViajeTractor) as idViajeTractor from trViajesTractoresTbl where idtractor='".$_REQUEST['trViajesTractoresIdTractorHdn']."' AND claveMovimiento!='VX'";
                    $rsV=fn_ejecuta_query($sqlIdViaje);
                    $idViajeInt=$rsV['root'][0]['idViajeTractor'];

                    $sqlSelUlt="SELECT * from trViajesTractoresTbl where idViajeTractor=".$idViajeInt=$rsV['root'][0]['idViajeTractor'];
                    $rsUlt=fn_ejecuta_query($sqlSelUlt);


                    if ($rsUlt['root'][0]['claveMovimiento']=='VF' || $rsUlt['root'][0]['claveMovimiento']=='VC' || $rsUlt['root'][0]['claveMovimiento']=='VE' || $rsUlt['root'][0]['claveMovimiento']=='VU') {
                        $inserta='0';
                        
                    }

                if ($_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDSLP' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CMDAT' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDLAR' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDMTY' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDKAN' || $inserta=='0')
                {

                               $sqlAddViajeTractorStr = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                         "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                                         "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                         "VALUES(".
                                            $_REQUEST['trViajesTractoresIdTractorHdn'].",".
                                            $_REQUEST['trViajesTractoresClaveChoferHdn'].",".
                                            "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                            $plazaDestino.",".
                                            "'".$_SESSION['usuCompania']."',".
                                            "(SELECT IFNULL(MAX(vt.viaje) + 1,1) AS VIAJE FROM trViajesTractoresTbl vt WHERE vt.idTractor =".$_REQUEST['trViajesTractoresIdTractorHdn']."),".
                                            "'".date("Y-m-d H:i:s")."',".
                                            replaceEmptyDec($kmTabulados).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmComprobadosTxt']).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmSinUnidadTxt']).",".
                                            $totalUnidades.",".
                                            //sizeof($unidadesArr)
                                            sizeof(array_unique($distArr,SORT_STRING)).",".
                                            replaceEmptyNull($_REQUEST['trViajesTractoresIdViajePadreHdn']).",".
                                            "'VG',".
                                            "'".$_SESSION['idUsuario']."',".
                                            "'".$_SERVER['REMOTE_ADDR']."')";

                $kilSumados = $rsUlt['root'][0]['kilometrostabulados'] + $kmTabulados;

                $rs="UPDATE trViajesTractoresTbl set kilometrostabulados=".$kilSumados." WHERE idViajeTractor=".$rsUlt['root'][0]['idViajeTractor'];

                fn_ejecuta_query($rs); 


            }
            else
            {
                    
                $sqlAddViajeTractorStr = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                         "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                                         "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                         "VALUES(".
                                            $_REQUEST['trViajesTractoresIdTractorHdn'].",".
                                            $_REQUEST['trViajesTractoresClaveChoferHdn'].",".
                                            "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                            $plazaDestino.",".
                                            "'".$_SESSION['usuCompania']."',".
                                            "(SELECT IFNULL(MAX(vt.viaje) + 1,1) AS VIAJE FROM trViajesTractoresTbl vt WHERE vt.idTractor =".$_REQUEST['trViajesTractoresIdTractorHdn']."),".
                                            "'".date("Y-m-d H:i:s")."',".
                                            replaceEmptyDec($kmTabulados).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmComprobadosTxt']).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmSinUnidadTxt']).",".
                                            $totalUnidades.",".
                                            //sizeof($unidadesArr)
                                            sizeof(array_unique($distArr,SORT_STRING)).",".
                                            replaceEmptyNull($_REQUEST['trViajesTractoresIdViajePadreHdn']).",".
                                            "'VG',".
                                            "'".$_SESSION['idUsuario']."',".
                                            "'".$_SERVER['REMOTE_ADDR']."')";

                $rs = fn_ejecuta_query($sqlAddViajeTractorStr);

            }
                if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                    $a['success'] = false;
                    $a['sqlError'] = $_SESSION['error_sql'];
                    $a['sql'] = $sqlAddViajeTractorStr;
                    $a['errorMessage'] = 'No se pud&oacute; Generar el Viaje. </br> Comun&iacute;quese '.
                                         'con el Administrador del Sistema </br>'.$a['sql'];
                    return $a;
                }

                // Obtiene el idViaje de lo que se acaba de insertar
                $idViajeInt = mysql_insert_id();
                $a['elID'] = $idViajeInt;


                if ($idViajeInt=='0') {

                    $sqlIdViaje="SELECT max(idViajeTractor) as idViajeTractor from trViajesTractoresTbl where idtractor='".$_REQUEST['trViajesTractoresIdTractorHdn']."' AND claveMovimiento!='VX'";
                    $rsV=fn_ejecuta_query($sqlIdViaje);
                    $idViajeInt=$rsV['root'][0]['idViajeTractor'];
                    # code...
                }

                //*2.- ACTUALIZAR LA TABLA DE trEsperaChoferesTbl -- Si es CDTOL o CDSAL u otro válido
                $_REQUEST['catGeneralesTablaTxt'] = 'trViajesTractoresTbl';
                $_REQUEST['catGeneralesColumnaTxt'] = 'cdValidos';
                $_REQUEST['catGeneralesValorTxt'] = $_SESSION['usuCompania'];
                $rsCdValido = getGenerales();

                $esCentroValido = ($rsCdValido['records'] == 1); //Variable si es un centro valido

                if($esCentroValido){ //Si es un CentroDistribución Válido actualiza la Lista de Espera
                    $sqlUpdateEsperaChoferesStr = "UPDATE tresperachoferestbl ".
                                                  " SET claveMovimiento = 'VV', ".
                                                  "fechaEvento = NOW() ".
                                                  "WHERE idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'].
                                                  " AND claveChofer = ".$_REQUEST['trViajesTractoresClaveChoferHdn'].
                                                  " AND centroDistribucion = '".$_SESSION['usuCompania']."';";

                    fn_ejecuta_query($sqlUpdateEsperaChoferesStr);
                    if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                        $a['success'] = false;
                        $a['sqlError'] = $_SESSION['error_sql'];
                        $a['sql'] = $sqlUpdateEsperaChoferesStr;
                        $a['errorMessage'] = 'No se pud&oacute; Actualizar la Lista de Espera. </br> Comun&iacute;quese '.
                                             'con el Administrador del Sistema </br>'.$a['sql'];
                        return $a;
                    }
                }
                //3.- INSERTAR EN LA TABLA trTalonesViajesTbl

                for ($i = 0; $i < count($distArr); $i++) {

                    /////////////////
                     $sqlTarifa="SELECT * FROM alUltimoDetalleTbl where vin ='".$vinArr[$i]."'";
                        $rsTarifa=fn_ejecuta_query($sqlTarifa);

                        if ($rsTarifa['root'][0]['idTarifa'] !='5') {
                            $tipoFolio='TR';
                            $patio=$_SESSION['usuCompania'];
                            $compania=$_REQUEST['trViajesTractoresCompaniaHdn'];
                        }else {
                            $tipoFolio='TE';
                            $patio='CDTOL';
                            $compania='TR';
                        }
                        if($_REQUEST['trViajesTractoresIdTractorHdn'] =='256') {
                            $tipoFolio='TV';
                            $patio=$_SESSION['usuCompania'];
                            $compania=$_REQUEST['trViajesTractoresCompaniaHdn'];
                        }

                        //////////////////

                   $sqlAddTalonesViajeStr = "INSERT INTO trTalonesViajesTbl (distribuidor,folio,idViajeTractor,companiaRemitente,".
                                                    "idPlazaOrigen,idPlazaDestino,direccionEntrega,centroDistribucion,tipoTalon,".
                                                    "fechaEvento,observaciones,numeroUnidades,importe,seguro,tarifaCobrar,kilometrosCobrar,".
                                                    "impuesto,retencion,claveMovimiento,tipoDocumento) VALUES('".$distArr[$i]."', (select SUM(folio + 1) as folio from trfoliostbl where centroDistribucion='".$patio."' and compania='".$compania."' and tipoDocumento='".$tipoFolio."'), ".$idViajeInt.", '".$remitenteArr[$i]."', ".
                                             "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                    "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'), ".
                                             $destinoArr[$i].", ".$direccionArr[$i].", '".$_SESSION['usuCompania']."', ".
                                             "'TN', now(), '*Hora :".$obsArr[$i]."', ".$unidadesArr[$i].", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresImporteTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresSeguroTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresTarifaCobrarTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresKmCobrarTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresImpuestoTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresRetencionTxt']).", 'TV', 'TV')";

                        $rs = fn_ejecuta_query($sqlAddTalonesViajeStr);

                    $sqlUpdFolioPreViaje=" UPDATE trFoliosTbl set folio = folio+1 WHERE centroDistribucion='".$patio."' and compania='".$compania."' and tipoDocumento='".$tipoFolio."' ";
                    fn_ejecuta_query($sqlUpdFolioPreViaje);

                }


                if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                    $a['success'] = false;

                    $a['sqlError'] = mysql_errno()." - ".mysql_error();
                    $a['sql'] = $sqlAddTalonesViajeStr;
                    $a['errorMessage'] = 'No se pud&oacute; Generar los talones. </br> Comun&iacute;quese '.
                                         'con el Administrador del Sistema </br>'.$a['sql'];
                    return $a;
                }
                
                
                        if($a['success'] && $_REQUEST['msgLog'] != ''){
                                $sql = "INSERT INTO trLogMantenimientoTbl (idViajeTractor, centroLibera, idTractor, movimiento, pantalla, idUsuario, ip, fechaEvento, tipoDocumento) VALUES(".
                                           $idViajeInt.", '".
                                           $_SESSION['usuCompania']."', ".
                                           $_REQUEST['trViajesTractoresIdTractorHdn'].", ".
                                           $_REQUEST['movAux'].", '".
                                           $_REQUEST['titleWin']."', ".
                                           $_SESSION['idUsuario'].", '".
                                           $_SERVER['REMOTE_ADDR']."', ".
                                           "now(), '".
                                           $_REQUEST['msgLog']."')";
                            //echo "$sql<br>";
                            fn_ejecuta_query($sql);
                
                            if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                                $a['success'] = false;
                                $a['sql'] = $sql;
                                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sql;
                                return $a;
                            }                          
                        }                
                
                
                //4.- INSERTAR EN LA TABLA  trUnidadesEmbarcadasTbl
                if($esCentroValido){ //Si es un CdValido Inserta en Embarcadas


                         $valCenDis =  "SELECT 1  as bandera from cageneralestbl where tabla='trunidadesembarcadastbl'
                          AND columna='centroDisValidos'
                          AND valor=  '".$_REQUEST['trViajesTractoresCentroDistribucionHdn']."' ";

                        $rsvalCenDis = fn_ejecuta_query($valCenDis);


                        if($rsvalCenDis['root'][0]['bandera'] != 1 ){
                          $valorEstatus='E';
                        }else{
                          $valorEstatus='L';
                        }

                    $sqlAddUnidadesEmbarcadasStr = "INSERT INTO trUnidadesEmbarcadasTbl ".
                                            "(centroDistribucion, idViajeTractor, vin, fechaEmbarque, claveMovimiento,distribuidor,tipoTraslado, idTalon) VALUES";

                    for($i = 0; $i < count($vinArr); $i++){

                        $sqlTarifa="SELECT * FROM alUltimoDetalleTbl where vin ='".$vinArr[$i]."'";
                        $rsTarifa=fn_ejecuta_query($sqlTarifa);



                        if ($rsTarifa['root'][0]['idTarifa'] =='5') {
                            $sqlDist="SELECT * FROM aldestinosespecialestbl a where a.vin ='".$vinArr[$i]."' AND a.idDestinoEspecial=(SELECT max(b.idDestinoEspecial) FROM aldestinosespecialestbl b where a.vin=b.vin)";
                            $reDist=fn_ejecuta_query($sqlDist);

                            $distribuidor=$reDist['root'][0]['distribuidorDestino'];
                        }else{
                            $distribuidor=$rsTarifa['root'][0]['distribuidor'];
                        }

                         if ($rsTarifa['root'][0]['claveMovimiento'] =='CT') {
                            $sqlDist="SELECT * FROM aldestinosespecialestbl a where a.vin ='".$vinArr[$i]."' AND a.idDestinoEspecial=(SELECT max(b.idDestinoEspecial) FROM aldestinosespecialestbl b where a.vin=b.vin)";
                            $reDist=fn_ejecuta_query($sqlDist);

                            $distribuidor=$reDist['root'][0]['distribuidorDestino'];
                        }


                        if ($rsTarifa['root'][0]['idTarifa'] !='5') {
                            $tipoTalon='TN';
                        }else{
                            $tipoTalon='TE';
                        }

                          //$distribuidor=$distArr;
                        //echo json_encode($distArr);

                        $sqlDX="SELECT * FROM  caDistribuidoresCentrosTbl where distribuidorCentro='".$rsTarifa['root'][0]['distribuidor']."'";
                        $rsDX=fn_ejecuta_query($sqlDX);

                         

                        if ($rsDX['root'][0]['tipoDistribuidor'] =='DX') {
                            $tipoTalon='TX';
                            $distribuidor=$distArr[0];
                        }
                        if ($rsDX['root'][0]['tipoDistribuidor'] =='CD') {                            
                            $distribuidor=$distArr[0];
                        }
                        //for ($i = 0; $i < count($distArr); $i++) {
                            $selIdTalon="SELECT * FROM trTalonesViajesTbl WHERE idViajeTractor='".$idViajeInt."' AND claveMovimiento='TV' AND distribuidor ='".$distribuidor."'";
                            $rsTalon=fn_ejecuta_query($selIdTalon);
                        //}

                        



                        if($i > 0){
                           $sqlAddUnidadesEmbarcadasStr .= ", ";
                        }
                        $sqlAddUnidadesEmbarcadasStr .= "('".$_SESSION['usuCompania']."', ".$idViajeInt.", ".
                                                        replaceEmptyNull("'".$vinArr[$i]."'").", ".
                                                        "'".date("Y-m-d H:i:s", time() + $i*2)."','".$valorEstatus."','".$distribuidor."','".$tipoTalon."','".$rsTalon['root'][0]['idTalon']."')";
                    }
                    $rs = fn_ejecuta_query($sqlAddUnidadesEmbarcadasStr);

                    if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                        $a['success'] = false;
                        $a['sqlError'] = mysql_errno()." - ".mysql_error();
                        $a['sql'] = $sqlAddUnidadesEmbarcadasStr;
                        $a['errorMessage'] = 'No se pud&oacute; Embarcar las Unidades. </br> Comun&iacute;quese '.
                                             'con el Administrador del Sistema </br>'.$a['sql'];
                        return $a;
                    }

                    $selIdTalon="SELECT * FROM trUnidadesEmbarcadasTbl WHERE idViajeTractor='".$idViajeInt."' AND idTalon ='' ";
                    $rsTalon=fn_ejecuta_query($selIdTalon);

                    $selTalon="SELECT * FROM trTalonesViajesTbl WHERE idViajeTractor=".$idViajeInt." AND claveMovimiento='TV' AND distribuidor in (SELECT distribuidorCentro FROM cadistribuidorescentrostbl WHERE tipoDistribuidor='CD')";
                    $rsIdTalon=fn_ejecuta_query($selTalon);

                    $updIdTalon="UPDATE trUnidadesEmbarcadasTbl SET IDTALON=".$rsIdTalon['root'][0]['idTalon']." WHERE idViajeTractor='".$idViajeInt."' AND idTalon ='' AND distribuidor in (SELECT distribuidor FROM caDistribuidoresCentrosTbl WHERE tipodistribuidor='CD')";
                    fn_ejecuta_query($updIdTalon);

                }
            }
        } 
        
        $a['successMessage'] = getPreViajeSuccessMsg();
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function addViajeTractor12(){
        $a['success'] = true;
        $e['errors'] = array();

        $vinArr = explode('|', $_REQUEST['asignacionUnidadesVinHdn']);
        if(in_array("", $vinArr)){
            $a['success'] = false;
            $e[] = array('id' => 'asignacionUnidadesVinHdn', 'msg' => getRequerido());
            $e['errorMessage'] = getErrorRequeridos();
        }
        $tarifaArr = explode('|', $_REQUEST['asignacionUnidadesTarifaHdn']);
        if(in_array("", $tarifaArr)){
            $a['success'] = false;
            $e[] = array('id' => 'asignacionUnidadesTarifaHdn', 'msg' => getRequerido());
            $e['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['asignacionUnidadesDistribuidorHdn'] == ""){
            $a['success'] = false;
            $e[] = array('id' => 'asignacionUnidadesDistribuidorHdn', 'msg' => getRequerido());
            $e['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['asignacionUnidadesNumUnidadesHdn'] == ""){
            $a['success'] = false;
            $e[] = array('id' => 'asignacionDeUnidadesNumUnidadesHdn', 'msg' => getRequerido());
            $e['errorMessage'] = getErrorRequeridos();
        }
        if($a['success'] == true){
            //OBTIENE LOS idPlaza Origen-Destino
            $sqlIdPlazas = "SELECT (SELECT idPlaza FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro ='".$_SESSION['usuCompania']."') AS idPlazaOrigen, ".
                            "(SELECT idPlaza FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$_REQUEST['asignacionUnidadesDistribuidorHdn']."') as idPlazaDestino;";

            $rsPlazas = fn_ejecuta_query($sqlIdPlazas);

            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ''){
                $a['success'] = false;
                $a['errorMsg'] = $_SESSION['error_sql']." Error en obtener la plaza";
                $a['errorMessage'] = "Error Al Asignar el Viaje";
                return $a;
            }

            //OBTIENE EL FOLIO
            $sqlGetFolioStr = "SELECT sum(folio + 1) as folio FROM trFoliosTbl ".
                              "WHERE tipoDocumento = 'TR' ".
                              "AND centroDistribucion='".$_SESSION['usuCompania']."' ".
                              "AND compania = '".$_REQUEST['asignacionUnidadesCompaniaCmb']."';";

            $rsFolio = fn_ejecuta_query($sqlGetFolioStr);
            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ''){
                $a['success'] = false;
                $a['errorMsg'] = $_SESSION['error_sql']." Error en obtener el Folio";
                $a['errorMessage'] = "Error Al Asignar el Viaje";
                return $a;
            }

            $folio = $rsFolio['root'][0]['folio'];
            //INCREMENTO EL # DE FOLIO LO INSERTA EN EL TALÓN Y LO ACTUALIZA EN LA TABLA DE FOLIOS
            if ((integer) $folio < 9) {
                $folio = '0'.(string)((integer)$folio+1);
            } else {
                $folio = (string)((integer)$folio+1);
            }

            //CREO UN VIAJE
            //CREO UN TALON
            //INSERTA EN HISTORICO ETC

            $sqlAddViaje =  "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                             "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                                             "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                             "VALUES(";

            $sqlAddViaje .= "(SELECT idTractor FROM caTractoresTbl WHERE tractor = 12 AND compania = 'TR'), ".
                            "NULL, ".
                            $rsPlazas['root'][0]['idPlazaOrigen'].", ".
                            $rsPlazas['root'][0]['idPlazaDestino'].", '".
                            $_SESSION['usuCompania']."', 0, '".date("Y-m-d H:i:s")."', 0, 0, 0, ".$_REQUEST['asignacionUnidadesNumUnidadesHdn'].", ".
                            "1, NULL, 'VE', '13', '".$_SERVER['REMOTE_ADDR']."');";


            fn_ejecuta_query($sqlAddViaje);
            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ''){
                $a['success'] = false;
                $a['errorMsg'] = $_SESSION['error_sql']."Error Insertar el Viaje";
                $a['errorMessage'] = "Error Al Asignar el Viaje";
                return $a;
            }
            $idViajeTractor = mysql_insert_id();
            $a['idViajeTractor'] = $idViajeTractor;


            $sqlAddTalonViaje = "INSERT INTO trTalonesViajesTbl (distribuidor,folio,idViajeTractor,companiaRemitente,".
                                                        "idPlazaOrigen,idPlazaDestino,direccionEntrega,centroDistribucion,tipoTalon,".
                                                        "fechaEvento,observaciones,numeroUnidades,importe,seguro,tarifaCobrar,kilometrosCobrar,".
                                                        "impuesto,retencion,claveMovimiento,tipoDocumento) VALUES(";

            $sqlAddTalonViaje .= "'".$_REQUEST['asignacionUnidadesDistribuidorHdn']."', '".$folio."', ".$idViajeTractor.", 'TCO', ".
                                 $rsPlazas['root'][0]['idPlazaOrigen'].", ".$rsPlazas['root'][0]['idPlazaDestino'].", ".
                                 "(SELECT direccionEntrega FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$_SESSION['usuCompania']."'), '".
                                 $_SESSION['usuCompania']."', 'NA', '".date("Y-m-d H:i:s", time() + 1)."','', ".$_REQUEST['asignacionUnidadesNumUnidadesHdn'].
                                ", 0.00, 0, 0, 0, 0, 0, 'TE', 'TE')";

            fn_ejecuta_query($sqlAddTalonViaje);
            $idTalon = mysql_insert_id();

            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ''){
                $a['success'] = false;
                $a['errorMsg'] = $_SESSION['error_sql']."Error Insertar el Viaje";
                $a['errorMessage'] = "Error Al Generar el Tal&oacute;n";
                return $a;
            }


            //INSERTA EN TRUNIDADESDETALLESTALONESTBL
            $sqlInsertUnidadesTalon = "INSERT INTO trUnidadesDetallesTalonesTbl(idTalon, vin, estatus) VALUES";
            //INSERTA LAS UNIDADES EN EL HISTORICO
            for ($i = 0; $i < count($vinArr); $i++) {
                if($i > 0){
                    $sqlInsertUnidadesTalon .= ", ";
                }
                $sqlInsertUnidadesTalon .= "(".$idTalon.", '".$vinArr[$i]."', "."'ER')";

                addHistoricoUnidad($_SESSION['usuCompania'], $vinArr[$i], 'ER',
                                    $_REQUEST['asignacionUnidadesDistribuidorHdn'], $tarifaArr[$i],
                                    $_REQUEST['asignacionUnidadesDistribuidorHdn'],
                                    'NULL','',$_SESSION['usuario'], $i*2);
            }

            fn_ejecuta_query($sqlInsertUnidadesTalon);

            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ''){
                $a['success'] = false;
                $a['errorMsg'] = $_SESSION['error_sql']."Error Insertar en el Historico";
                $a['errorMessage'] = "Error Al Actualizar Las Unidades";
                return $a;
            }
            $a['successMessage'] = "Unidades Asignadas al Tractor 12 Correctamente";

        }
        return $a;

    }


    function getTalonesPorViajeTalon(){

        if ($_REQUEST['liberacionTractorNoTalonTxt'] != "") {
            $sqlGetIdViaje = "SELECT idViajeTractor FROM trTalonesViajesTbl ".
                            "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                            //"AND companiaRemitente = '".$_REQUEST['liberacionTractorCompaniaCmb']."' ".
                            "AND folio = '".$_REQUEST['liberacionTractorNoTalonTxt']."'";
        }

        if ($_REQUEST['liberacionTractorNoViajeTxt'] != "") {
            $sqlGetIdViaje = "SELECT vt.idViajeTractor FROM trViajesTractoresTbl vt, caTractoresTbl tr ".
                             "WHERE vt.idTractor = tr.idTractor ".
                             "AND vt.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                             //"AND tr.tractor = '".$_REQUEST['liberacionTractorNoTractorTxt']."' ".
                             "AND vt.viaje = '".$_REQUEST['liberacionTractorNoViajeTxt']."'";
        }

        $rsIdViaje = fn_ejecuta_query($sqlGetIdViaje);

        if (count($rsIdViaje['root']) > 0){
            $_REQUEST['trViajesTractoresIdViajeHdn'] = $rsIdViaje['root'][0]['idViajeTractor'];
            $_REQUEST['trViajesTractoresClaveMovimientoHdn'] = "!=TC";
            echo json_encode(getTalonesViaje());
        } else {
            //echo '';
        }
    }

    function getReImpresionBitacora(){
        $sqlGetBitacora = "SELECT vt.idViajeTractor, vt.viaje, ch.claveChofer, concat(ch.nombre, ' ',ch.apellidoPaterno, ' ', ch.apellidoMaterno) as nombreCompleto, tr.tractor, concat(ct.compania, ' - ',ct.descripcion) as companiaTractor ".
                          " FROM trviajestractorestbl vt, caChoferesTbl ch, catractorestbl tr, cacompaniastbl ct ".
                          " WHERE vt.claveChofer = '".$_REQUEST['reBitacoraChoferCmb']."' ".
                          " AND vt.claveMovimiento = 'VF' ".
                          " AND vt.claveChofer = ch.claveChofer ".
                          " AND vt.idTractor = tr.idTractor ".
                          " AND ct.compania = tr.compania ".
                          " AND vt.viaje = (SELECT MAX(vt1.viaje) FROM trviajestractorestbl vt1 WHERE vt1.claveChofer = vt.claveChofer) ";

    $rs = fn_ejecuta_query($sqlGetBitacora);

    echo json_encode($rs);

    }

    function verificarEmbarcadas(){
        $sqlVerifica = "SELECT 1 AS unidadesVerificadas ".
                       "FROM trunidadesembarcadastbl ".
                       "WHERE claveMovimiento = 'E' ".
                       "AND idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].";";

    $rs = fn_ejecuta_query($sqlVerifica);

    echo json_encode($rs);
    }

function addUpdTalonesTempEsp() {
       $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap446IdViajeHdn'] == ""){
            $e[] = array('id'=>'trap446IdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        //No es requerido el id del talon porque hay talones nuevos sin ID
        $idTalonArr = explode('|', substr($_REQUEST['trap446IdTalonHdn'], 0, -1));
        $folioArr = explode('|', substr($_REQUEST['trap446FolioHdn'], 0, -1));
        $numUnidadesArr = explode('|', substr($_REQUEST['trap446NumeroUnidadesHdn'], 0, -1));
        $previosArr = explode('|', substr($_REQUEST['previos'], 0, -1));

        $distArr = explode('|', substr($_REQUEST['trap446DistribuidorHdn'], 0, -1));
        if(in_array('', $distArr)){
            $e[] = array('id'=>'trap446DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $direccionArr = explode('|', substr($_REQUEST['trap446DireccionEntregaHdn'], 0, -1));
        if(in_array('', $direccionArr)){
            $e[] = array('id'=>'trap446DireccionEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $remitenteArr = explode('|', substr($_REQUEST['trap446RemitenteHdn'], 0, -1));
        if(in_array('', $remitenteArr)){
            $e[] = array('id'=>'trap446RemitenteHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        /*if($_REQUEST['trap446NumeroTalonHdn'] == ""){
            $e[] = array('id'=>'trap446NumeroTalonHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        } */
        if ($a['success'] == true) {
            $sqlGetNumeroTalon = "SELECT ifnull(SUM(numeroTalon + 1),1) AS numeroTalon ".
                                    "FROM trtalonesviajestmp tmp1 ".
                                    "WHERE tmp1.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ".
                                    "AND numeroTalon = (SELECT max(numeroTalon) FROM trtalonesviajestmp tmp2 WHERE tmp2.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ) ";

            $rs = fn_ejecuta_query($sqlGetNumeroTalon);


            //$numeroTalon = $_REQUEST['trap446NumeroTalonHdn'];

            //echo json_encode($rs['root'][0]['numeroTalon']);

            //$_SESSION['usuCompania'] = "CDLZC";
            for($nInt = 0; $nInt < sizeof($distArr); $nInt++){
                #$previosArr = -1 => Registro que apenas se acaba de insertar en el grid
                #$previosArr = 0 => Registro que se insertó previamente en el grid
                #$previosArr = 1 => Registro que se insertó con anterioridad y ya tiene folio en el grid                                
                //var_dump($previosArr[$nInt]);
                if($previosArr[$nInt] == 0)
                {
                        //Son registros que están actualizando, por eso lo borra y posteriormente lo inserta
                        $sql = "DELETE FROM trTalonesViajesTmp WHERE numeroTalon = ".$_REQUEST['idTalon'].
                                     " AND centroDistribucion = '".$_SESSION['usuCompania']."'".
                                     " AND idUsuario = ".$_SESSION['idUsuario'];
                        //echo "$sql<br>";
                        fn_ejecuta_query($sql);
                        $previosArr[$nInt] = -1;
                        if(isset($_REQUEST['idTalon']) && $_REQUEST['idTalon'] != '')
                        {
                                $rs['root'][0]['numeroTalon'] = $_REQUEST['idTalon'];
                          }
                }
                
                if($previosArr[$nInt] == -1 || empty($previosArr[$nInt]))
                {
                        //Son registros que están actualizando, por eso lo borra y posteriormente lo inserta
                        $sql = "DELETE FROM trTalonesViajesTmp WHERE numeroTalon = ".$rs['root'][0]['numeroTalon'].
                                     " AND centroDistribucion = '".$_SESSION['usuCompania']."'".
                                     " AND idUsuario = ".$_SESSION['idUsuario'];
                        //echo "$sql<br>";
                        fn_ejecuta_query($sql);
                                                
                        $sql = "INSERT INTO trTalonesViajesTmp (idViajeTractor, numeroTalon, distribuidor, ".
                                "numeroUnidades, direccionEntrega, companiaRemitente, centroDistribucion, observaciones, tipoTalon, bloqueado, idUsuario) VALUES (".
                                $_REQUEST['trap446IdViajeHdn'].",".
                                $rs['root'][0]['numeroTalon'].", ".
                                "'".$distArr[$nInt]."',".
                                replaceEmptyDec($numUnidadesArr[$nInt]).",".
                                $direccionArr[$nInt].",".
                                "'".$remitenteArr[$nInt]."',".
                                "'".$_SESSION['usuCompania']."',".
                                "'".$observacionesArr[$nInt]."',".
                                "'NA',".
                                "0,".
                                $_SESSION['idUsuario'].")";
                            //echo "$sql<br>";                      
                        $rs = fn_ejecuta_query($sql);                               
                }
                /*
                if($previosArr[$nInt] == 0)
                {
                        //UPDATE
                        $sql = "UPDATE trTalonesViajesTmp ".
                                     " SET distribuidor = '".$distArr[$nInt]."', ".
                                     " numeroUnidades = ".replaceEmptyDec($numUnidadesArr[$nInt]).",".
                                     " direccionEntrega = '".$direccionArr[$nInt]."', ".
                                     " companiaRemitente = '".$remitenteArr[$nInt]."', ".
                                     " observaciones = '".$observacionesArr[$nInt]."', ".
                                     " tipoTalon = 'NA', ".
                                     " tarifaUnidades = NULL, ".
                                     " bloqueado = 0".
                                     " WHERE numeroTalon = ".$rs['root'][0]['numeroTalon'].
                                     " AND centroDistribucion = '".$_SESSION['usuCompania']."'".
                                     " AND idUsuario = ".$_SESSION['idUsuario'];                        
                } 
                */               

                $numeroTalon++;

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sql;
                    $a['successMessage'] = getTalonesViajeAddMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sql;
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function addUpdTalonesTempNorm() {
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap446IdViajeHdn'] == ""){
            $e[] = array('id'=>'trap446IdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        //No es requerido el id del talon porque hay talones nuevos sin ID
        $idTalonArr = explode('|', substr($_REQUEST['trap446IdTalonHdn'], 0, -1));
        $folioArr = explode('|', substr($_REQUEST['trap446FolioHdn'], 0, -1));
        $numUnidadesArr = explode('|', substr($_REQUEST['trap446NumeroUnidadesHdn'], 0, -1));

        $distArr = explode('|', substr($_REQUEST['trap446DistribuidorHdn'], 0, -1));
        if(in_array('', $distArr)){
            $e[] = array('id'=>'trap446DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $direccionArr = explode('|', substr($_REQUEST['trap446DireccionEntregaHdn'], 0, -1));
        if(in_array('', $direccionArr)){
            $e[] = array('id'=>'trap446DireccionEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $remitenteArr = explode('|', substr($_REQUEST['trap446RemitenteHdn'], 0, -1));
        if(in_array('', $remitenteArr)){
            $e[] = array('id'=>'trap446RemitenteHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            $sqlGetNumeroTalon = "SELECT IFNULL(MAX(numeroTalon)+1, 1) AS numeroTalon FROM trtalonesviajestmp ";

            $rs = fn_ejecuta_query($sqlGetNumeroTalon);

            $numeroTalon = $rs['root'][0]['numeroTalon'];
            $_SESSION['usuCompania'] = "CDLZC";
            for($nInt = 0; $nInt < sizeof($distArr); $nInt++){
                if($folioArr[$nInt] == ""){
                    $sqlAddUpdTalonStr = "UPDATE trTalonesViajesTmp SET ".
                                        "distribuidor = '".$distArr[$nInt]."',".
                                        "direccionEntrega = ".$direccionArr[$nInt].",".
                                        "numeroUnidades = ".replaceEmptyDec($numUnidadesArr[$nInt]).",".
                                        "companiaRemitente = '".$remitenteArr[$nInt]."',".
                                        "observaciones = '".$observacionesArr[$nInt]."',".
                                        "tipoTalon = '".$tipoTalonArr[$nInt]."' ".
                                        "WHERE numeroTalon = ".$idTalonArr[$nInt];
                } else {
                    $sqlAddUpdTalonStr = "UPDATE trTalonesViajesTbl SET ".
                                        "distribuidor = '".$distArr[$nInt]."',".
                                        "direccionEntrega = ".$direccionArr[$nInt].",".
                                        "numeroUnidades = ".replaceEmptyDec($numUnidadesArr[$nInt]).",".
                                        "companiaRemitente = '".$remitenteArr[$nInt]."',".
                                        "observaciones = '".$observacionesArr[$nInt]."' ".
                                        "WHERE idTalon = ".$idTalonArr[$nInt];
                }

                $rs = fn_ejecuta_query($sqlAddUpdTalonStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlAddUpdTalonStr;
                    $a['successMessage'] = getTalonesViajeAddMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUpdTalonStr;
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getTalonesViaje1(){
            $lsWhereStr = "WHERE tv.distribuidor = dc.distribuidorCentro ".
                          "AND tv.idViajeTractor = vt.idViajeTractor ".
                          "AND vt.idTractor = tr.idTractor ".
                          "AND ch.claveChofer = vt.claveChofer ".
                          "AND es.idPais = pa.idPais ".
                          "AND mu.idEstado = es.idEstado ".
                          "AND cl.idMunicipio = mu.idMunicipio ".
                          "AND dr.idColonia = cl.idColonia ".
                          "AND dr.direccion = tv.direccionEntrega ".
                          "AND vt.viaje = (SELECT max(vt3.viaje) as viaje FROM trviajestractorestbl vt3 WHERE vt3.idTractor = '".$_REQUEST['trViajesTractoresIdTractorHdn']."')".
                          "AND tv.claveMovimiento != 'TX' ";

            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "vt.idViajeTractor", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresViajeHdn'], "vt.viaje", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTalonHdn'], "tv.idTalon", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresDistribuidorHdn'], "tv.distribuidor", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresFolioTxt'], "tv.folio", 1, 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 2);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovimientoHdn'], "tv.claveMovimiento", 2);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "tv.centroDistribucion", 2);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "tr.idTractor", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresNumTractorHdn'], "tr.tractor", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovViajeHdn'], "vt.claveMovimiento", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTipoDoctoHdn'], "tv.tipoDocumento", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaRemitenteHdn'], "tv.companiaRemitente", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }


            $sqlGetTalones = "SELECT 0 AS temp, tv.*, concat(dc.distribuidorCentro,' - ',dc.descripcionCentro) as descripcionCentro, dc.tipoDistribuidor, tr.idTractor, tr.rendimiento, ".
                                     "vt.fechaEvento AS fechaViaje, vt.numeroRepartos, vt.kilometrosTabulados, vt.viaje, ".
                                     "vt.claveMovimiento AS claveMovimientoViaje, vt.numeroUnidades AS numeroUnidadesViaje, ".
                                     " tr.tractor, tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                     "(SELECT dc3.descripcionCentro FROM caDistribuidoresCentrosTbl dc3 ".
                                        "WHERE dc3.distribuidorCentro = tv.companiaRemitente) AS descripcionCompania, ".
                                     "(SELECT COUNT(*) FROM trTalonesViajesTbl tv2 WHERE tv2.idViajeTractor = vt.idViajeTractor ".
                                        "AND tv2.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                     "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tv.idTalon AND tt2.centroDistribucion = '".$_SESSION['usuCompania']."' AND tt2.idUsuario = ".$_SESSION['idUsuario'].") AS numeroUnidadesTmp, ".
                                     "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                     "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                     "(SELECT pl2.plaza FROM caPlazasTbl pl2 WHERE pl2.idPlaza = tv.idPlazaDestino) AS nombrePlazaDestino,  ".
                                     "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' ".
                                        "AND cg.columna='claveMovimiento' AND cg.valor = tv.claveMovimiento) AS descClaveMovTalon, ".
                                     "(SELECT cg2.nombre FROM caGeneralesTbl cg2 WHERE cg2.tabla='trTalonesViajesTbl' ".
                                        "AND cg2.columna='claveMovimiento' AND cg2.valor = tv.claveMovimiento) AS descTipoDocto, ".
                                     "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' ".
                                        "AND cg.columna='tipoTalon' AND cg.valor = tv.tipoTalon) AS nombreTipoTalon, ".
                                     "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                        "WHERE dc2.distribuidorCentro = vt.centroDistribucion) AS nombreCentroDist, ".
                                     "pa.pais, es.estado, mu.municipio, cl.colonia, cl.cp, dr.calleNumero ".
                                     "FROM trTalonesViajesTbl tv, caDistribuidoresCentrosTbl dc, trViajesTractoresTbl vt, ".
                                     "caTractoresTbl tr, caChoferesTbl ch, caPaisesTbl pa, caEstadosTbl es, ".
                                     "caMunicipiosTbl mu, caColoniasTbl cl, caDireccionesTbl dr ";//.$lsWhereStr;

            //echo $_REQUEST['trViajesTractoresAsignacionHdn'];

            if($_REQUEST['trViajesTractoresAsignacionHdn'] != ''){
                $sqlGetTalones .=   " UNION ".
                                    "SELECT 1 AS temp, tt.numeroTalon, tt.distribuidor, 'ESP' AS folio, tt.idViajeTractor, tt.companiaRemitente,".
                                    "-1 AS plazaOrigen, -1 AS plazaDestino, tt.direccionEntrega, tt.centroDistribucion, ".
                                    "tt.tipoTalon, '".date("Y-m-d H:i:s")."', tt.observaciones, tt.numeroUnidades, ".
                                    "0 AS importe, 0 AS seguro, 0 AS tarifaCobrar, 0 AS kilometrosCobrar, 0 AS impuesto, ".
                                    "0 AS retencion, 'TV' AS claveMovimiento, 'TV' AS tipoDocumento, ".
                                    "dc.descripcionCentro, dc.tipoDistribuidor, tr.idTractor, tr.rendimiento, vt.fechaEvento AS fechaViaje, vt.numeroRepartos,".
                                    "vt.kilometrosTabulados, vt.viaje, vt.claveMovimiento AS claveMovimientoViaje, vt.numeroUnidades AS numeroUnidadesViaje, tr.tractor, ".
                                    "tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                    "(SELECT dc3.descripcionCentro FROM caDistribuidoresCentrosTbl dc3 WHERE dc3.distribuidorCentro = tt.companiaRemitente) AS descripcionCompania, ".
                                    "0 AS numTalonesValidosViaje, ".
                                    "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tt.numeroTalon AND tt2.centroDistribucion = '".$_SESSION['usuCompania']."' AND tt2.idUsuario = ".$_SESSION['idUsuario'].") AS numeroUnidadesTmp, ".
                                    "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                    "'' AS nombrePlazaOrigen, ".
                                    "(SELECT pl2.plaza FROM caPlazasTbl pl2, caDistribuidoresCentrosTbl dc WHERE dc.distribuidorCentro = tt.distribuidor ".
                                        "AND pl2.idPlaza = dc.idPlaza) AS nombrePlazaDestino, ".
                                    "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' AND cg.columna='claveMovimiento' ".
                                        "AND cg.valor = claveMovimiento) AS descClaveMovTalon, ".
                                    "(SELECT cg2.nombre FROM caGeneralesTbl cg2 WHERE cg2.tabla='trTalonesViajesTbl' AND cg2.columna='claveMovimiento' ".
                                        "AND cg2.valor = claveMovimiento) AS descTipoDocto, ".
                                    "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' AND cg.columna='tipoTalon' ".
                                        "AND cg.valor = tt.tipoTalon) AS nombreTipoTalon, ".
                                    "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 WHERE dc2.distribuidorCentro = vt.centroDistribucion) AS nombreCentroDist, ".
                                    "pa.pais, es.estado, mu.municipio, cl.colonia, cl.cp, dr.calleNumero ".
                                    "FROM trtalonesviajestmp tt, caDistribuidoresCentrosTbl dc, trViajesTractoresTbl vt, caTractoresTbl tr, caChoferesTbl ch, caPaisesTbl pa, ".
                                    "caEstadosTbl es, caMunicipiosTbl mu, caColoniasTbl cl, caDireccionesTbl dr ".
                                    "WHERE tt.distribuidor = dc.distribuidorCentro AND tt.idViajeTractor = vt.idViajeTractor AND vt.idTractor = tr.idTractor ".
                                    "AND ch.claveChofer = vt.claveChofer AND es.idPais = pa.idPais AND mu.idEstado = es.idEstado ".
                                    "AND cl.idMunicipio = mu.idMunicipio AND dr.idColonia = cl.idColonia AND dr.direccion = tt.direccionEntrega AND claveMovimiento != 'TX' ".
                                    "AND vt.idViajeTractor = 229 ";
            } else {
                $sqlGetTalones .= $lsWhereStr;
            }

            $rs = fn_ejecuta_query($sqlGetTalones);
            echo json_encode($rs);
            $dist = array();
            $talonesValidos = 0; //TALONES NO CANCELADOS DEL VIAJE

            for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
                $rs['root'][$nInt]['descDistribuidor'] = $rs['root'][$nInt]['distribuidor']." - ".$rs['root'][$nInt]['descripcionCentro'];
                $rs['root'][$nInt]['descCompania'] = $rs['root'][$nInt]['companiaRemitente']." - ".$rs['root'][$nInt]['descripcionCompania'];
                $rs['root'][$nInt]['descTractorCia'] = $rs['root'][$nInt]['ciaTractor']." - ".$rs['root'][$nInt]['descCiaTractor'];
                $rs['root'][$nInt]['nombreChofer'] = $rs['root'][$nInt]['claveChofer']." - ".
                                                     $rs['root'][$nInt]['nombre']." ".
                                                     $rs['root'][$nInt]['apellidoPaterno']." ".
                                                     $rs['root'][$nInt]['apellidoMaterno'];
                $rs['root'][$nInt]['direccionCompleta'] = $rs['root'][$nInt]['calleNumero'].", ".
                                                          $rs['root'][$nInt]['colonia'].", ".
                                                          $rs['root'][$nInt]['municipio'].", ".
                                                          $rs['root'][$nInt]['estado'].", ".
                                                          $rs['root'][$nInt]['pais'].", ".
                                                          $rs['root'][$nInt]['cp'];

                $rs['root'][$nInt]['fechaActual'] = date('d-m-Y');
                $rs['root'][$nInt]['fechaEvento'] = date('Y-m-d', strtotime($rs['root'][$nInt]['fechaEvento']));


                if(!in_array($rs['root'][$nInt]['distribuidor'], $dist)) {
                    array_push($dist, $rs['root'][$nInt]['distribuidor']);
                }
            }

            for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
                $rs['root'][$nInt]['numeroDistribuidores'] = sizeof($dist);
            }
            return $rs;
        }

    function vaciaTemporales(){

        $sqlDltTs = "DELETE FROM trunidadesdetallestalonestmp ".
                    "WHERE vin IN(SELECT VIN FROM (SELECT ts.vin FROM trunidadesdetallestalonestmp ts,  trtalonesviajestmp tl WHERE tl.numeroTalon = ts.numeroTalon AND tl.idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ) AS vin) ";

        fn_ejecuta_query($sqlDltTs);

        $sqlDltTl = "DELETE FROM trTalonesViajesTmp ".
                    "WHERE idViajeTractor = ".$_REQUEST['trap446IdViajeHdn']." ".

        fn_ejecuta_query($sqlDltTl);
    }

    function foliosTalones(){

        $sqlConsultarTalones = "SELECT * FROM  trtalonesviajestbl ".
                                " WHERE idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'].
                                //" AND tipotalon='".$_REQUEST['tipoTalon']."'" .
                                " AND claveMovimiento ='TF'";
        $rstTalon = fn_ejecuta_query($sqlConsultarTalones);
        //echo json_encode($rstTalon);

        for ($i=0; $i <sizeof($rstTalon['root']) ; $i++) {
            $folios[] = $rstTalon['root'][$i]['folio'];
        }
        //$rstTalon['records'] = $folios;

        echo json_encode($folios);

    }

    function insertar510($claveMovimiento,$vin,$centroDistribucion){


        $selHistorico= "SELECT MAX(idTarifa), centroDistribucion as idTarifa FROM alHistoricoUnidadesTbl WHERE VIN ='".$vin."'";
        $rsSelHistorico= fn_ejecuta_query($selHistorico);

            if ($rsSelHistorico['root'][0]['idTarifa'] != '13') {

                $insFacturacion="UPDATE fafacturaciontransportaciontbl SET fechaOm = (SELECT max(fechaEvento) from alhistoricounidadestbl where vin ='".$vin."' AND claveMovimiento='OM') ".
                                "where vin ='".$vin."'";
                fn_ejecuta_query($insFacturacion);
            }


        $selInstrucciones="SELECT * FROM alinstruccionesmercedesTbl WHERE VIN ='".$vin."'";
        $rsSelInstrucciones=fn_ejecuta_query($selInstrucciones);

        if ($rsSelInstrucciones['records'] =='0') {

            $sel510tbl="SELECT * FROM al510tbl where vin ='".$vin."'";
            $rsSel510=fn_ejecuta_query($sel510tbl);

            if ($rsSel510['records']!='0') {

                $upd510="UPDATE al510tbl set estatus='".$claveMovimiento."' ,fechaEntrega='(SELECT max(fechaEvento) from alhistoricounidadestbl where vin ='".$vin."' and claveMovimiento='OM')".")'";
                fn_ejecuta_query($upd510);

            }else{

                $ins510="INSERT INTO al510tbl (centroDistribucion, avanzada, vin, estatus, fechaEntrega) ".
                            "VALUES ('".$centroDistribucion.
                                     "', '".substr($vin, 9,16).
                                     "', '".$vin."', '".$claveMovimiento.
                                     "', "."(SELECT max(fechaEvento) from alhistoricounidadestbl where vin ='".$vin."' and claveMovimiento='OM')".");";
                fn_ejecuta_query($ins510);
            }

        }

    }

    function getCentroDistribucion(){
        $sqlCD="SELECT * FROM trtalonesviajestbl ".
                "WHERE folio=".$_REQUEST['recepcionViajeDestinoidTalonHdn'];
        $rsSqlCD=fn_ejecuta_query($sqlCD);

        echo json_encode($rsSqlCD);
    }


    function sqlGetAntSuc(){
        $sqlGetEstatus = "SELECT claveMovimiento AS ultimo, (select hu.claveMovimiento from alhistoricounidadestbl hu where hu.vin = h1.vin order by fechaEvento desc limit 1,1) as anterior ".
                            "FROM alhistoricounidadestbl h1 ".
                            "WHERE h1.vin = '".$_REQUEST['catPreViajesUnidadesVinCmb']."' ".
                            "AND h1.claveMovimiento = 'RC' ".
                            "LIMIT 0,1; ";
        $rsSqlGetEstatus = fn_ejecuta_query($sqlGetEstatus);

        echo json_encode($rsSqlGetEstatus);
    }

    function sqlGetHoldUnidades(){
        $sqlGetHold = "SELECT hu.vin, (SELECT count(DISTINCT(ge.valor)) FROM cageneralestbl ge WHERE ge.tabla = 'alHoldsUnidadesTbl' AND ge.columna = 'CDTOL' AND ge.valor = hu.claveMovimiento ) as hold, ".
                "(SELECT COUNT(distinct(ge.estatus)) FROM cageneralestbl ge WHERE ge.tabla = 'alHoldsUnidadesTbl' AND ge.columna = 'CDTOL' AND ge.estatus = hu.claveMovimiento ) as libHold ".
                "FROM alhistoricounidadestbl hu ".
                "WHERE hu.vin = '".$_REQUEST['catPreViajesUnidadesVinCmb']."' ".
                "AND claveMovimiento in (SELECT VALOR FROM cageneralestbl ge WHERE ge.tabla = 'alHoldsUnidadesTbl' AND ge.columna = 'claveHold'); ";

        $rsHolds = fn_ejecuta_query($sqlGetHold);        

        for ($i=0; $i < $rsHolds['records'] ; $i++) { 

            if ($rsHolds['root'][$i]['hold'] == '1') {
                $totalHold[] = $rsHolds['root'][$i]['hold'];
            }if ($rsHolds['root'][$i]['libHold'] == '1') {
                $totalLib[] = $rsHolds['root'][$i]['libHold'];
            }
        }              
                                    
        if(array_sum($totalHold) == array_sum($totalLib)){
            $numHold = 0;
            echo json_encode($numHold);                        
        }else{
            $numHold = 1;
            echo json_encode($numHold);
        }
    } 

    function sqlNumTalones(){
        // cuenta el numero de Talones
            $sqlNumTalones = "SELECT COUNT(t1.folio) numfolio, ".
                            "CONCAT((SELECT t2.folio FROM trtalonesviajestbl t2 WHERE t2.idViajeTractor = t1.idViajeTractor AND t2.tipoTalon IN ('TE','TI','TN') ORDER BY t2.folio ASC LIMIT 1),' AL ', ".
                            "(SELECT t2.folio FROM trtalonesviajestbl t2 WHERE t2.idViajeTractor = t1.idViajeTractor AND t2.tipoTalon IN ('TN') ORDER BY t2.folio DESC LIMIT 1)) AS desCTalon ".
                            "FROM trtalonesviajestbl t1 ".
                            "WHERE t1.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViasjeHdn']."' ".
                            "AND t1.claveMovimiento != 'TX';";                                         

            $rsNumTalones = fn_ejecuta_query($sqlNumTalones);

            echo json_encode($rsNumTalones);
    }

    function sqlGetFolio(){
        $sqlGetNumFolio = "SELECT LPAD(SUM(folio + 1),8,0) as folioActual, SUM(folio + 1) AS nvoFolio FROM trFoliosTbl ".
                        "WHERE centrodistribucion = '".$_REQUEST['trViajesTractoresCentroHdn']."' ".        
                        "AND tipoDocumento = '".$_REQUEST['trViajesTractoresTipoDoctoHdn']."'; ";

        $rsNumFolio = fn_ejecuta_query($sqlGetNumFolio);
        echo json_encode($rsNumFolio);

        $sqlUpdFolio = "UPDATE trFoliosTbl ".
                        "SET folio = '".$rsNumFolio['root'][0]['nvoFolio']."' ".
                        "WHERE centrodistribucion = '".$_REQUEST['trViajesTractoresCentroHdn']."' ".
                        "AND tipoDocumento = '".$_REQUEST['trViajesTractoresTipoDoctoHdn']."'; ";

        fn_ejecuta_query($sqlUpdFolio);

    }
    
    function getAlerta(){
                $a = array();
                $a['success'] = true;
                $a['msjResponse'] = '';
                $a['msjPreventivo'] = '';
                $a['msjCorrectivo'] = '';
                
            $sql = "SELECT a.odometro AS maxKm FROM caMantenimientoTractoresDetalleTbl a".
                         " WHERE a.idTractor = ".$_REQUEST['idTractor'].
                         " AND a.movimiento = ".$_REQUEST['movimiento'].
                         " AND a.fechaEvento = (SELECT MAX(b.fechaEvento) FROM caMantenimientoTractoresDetalleTbl b ".
                         " WHERE b.idTractor = ".$_REQUEST['idTractor'].
                         " AND b.movimiento = ".$_REQUEST['movimiento'].")";
            //echo "$sql<br>";
            $kmRst = fn_ejecuta_query($sql);
            if($kmRst['records'] > 0)
            {
                    $kmRecorridos = (int) $kmRst['root'][0]['maxKm'] - (int) $_REQUEST['odometro'];     //501600 - 479079 = 22521
                    //var_dump($kmRecorridos);
                    
                    $sql = "SELECT kilometrosMantenimiento, kilometrosAvisos FROM caMantenimientoAlertaTbl".
                                 " WHERE idTractor = ".$_REQUEST['idTractor'];
                    //echo "$sql<br>";
                    $alertRst = fn_ejecuta_query($sql);
                    $kmMtto  = (empty($alertRst['root'][0]['kilometrosMantenimiento']))?0:$alertRst['root'][0]['kilometrosMantenimiento'];  //25000
                    $kmAviso = (empty($alertRst['root'][0]['kilometrosAvisos']))?0:$alertRst['root'][0]['kilometrosAvisos'];                                //22500
                    //var_dump($kmMtto);
                    //var_dump($kmAviso);
                                        
                    //Mantenimiento Correctivo
                    $difMtto = abs($kmRecorridos - $kmMtto);
                    if($kmRecorridos >= $kmMtto)
                    {                           
                            $a['success'] = false;
                            $a['msjCorrectivo'] = ($difMtto != 1)?'Ya se pasaron de '.$difMtto.' Km. para el servicio del tractor.':'Ya se pas&oacute de '.$difMtto.' Km. para el servicio del tractor.';
                            if($kmRecorridos == $kmMtto)
                            {
                                    $a['msjCorrectivo'] = 'Ya le toca servicio al tractor.';
                            }
                    }
                    
                    //Mantenimiento Preventivo
                    if($a['success'] && $kmRecorridos >= $kmAviso)
                    {
                            $a['success'] = false;
                            $a['msjPreventivo'] = ($difMtto != 1)?'Faltan '.$difMtto.' Km. para el servicio del tractor.':'Falta '.$difMtto.' Km. para el servicio del tractor.';                           
                    }
            }
            $a['msjResponse'] = ($a['msjPreventivo'] == '')?$a['msjCorrectivo']:$a['msjPreventivo'];
            echo json_encode($a);
        }
        
function validaCentroDisLiberacionTractor(){

        $sqlvalidaCentro = "SELECT CASE WHEN columna='validosConOdometro' THEN 'C' WHEN columna='validosSinOdometro' THEN 'S' END as tipoLiberacion, valor 
from cageneralestbl 
where tabla='pantallaLiberacionTractores' 
and valor='".$_REQUEST['ciasesval']."' ";

      $rsSqlvalidaCentro=fn_ejecuta_query($sqlvalidaCentro);

      echo json_encode($rsSqlvalidaCentro);
}

    function guardaPatioRetorno(){

        $selViaje="SELECT * FROM trViajesTractoresTbl WHERE idTractor='".$_REQUEST['tractor']."' AND claveMovimiento in ('VC','VG','VE')";
        $rsViaje=fn_ejecuta_query($selViaje);


        $updPatio="UPDATE trviajestractorestbl SET patioRegreso='".$_REQUEST['centroDistribucion']."', ruta='".$_REQUEST['ruta']."' WHERE idViajeTractor='".$rsViaje['root'][0]['idViajeTractor']."';";
        fn_ejecuta_query($updPatio);
    }

    function getTractoresDisponiblesAnt(){

   $lsWhereStr = "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.idTractor = tr.idTractor AND (vt2.idViajePadre IS NULL OR vt2.idViajePadre IS NOT NULL AND vt2.claveMovimiento <> 'VX')) ".
                      "WHERE  tr.estatus = 1 ".
                      "AND vt.idViajeTractor is not null";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = ("vt.claveMovimiento IN ('VV','VC','VF','VE','VG','VP')");
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);            
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if($_REQUEST['trTractoresNuevosHdn'] == '1'){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isTractoresNuevos = " UNION ".
                        "SELECT 0 as idviajeTractor, ".
                        "0 as idviajepadre, ".
                        "tr.idtractor, ".
                        "tr.tractor, ".
                        "tr.rendimiento, ". 
                        "'' as clavemovimiento, ".
                        "0 as clavechofer, ".
                        "0 as numerorepartos, ".
                        "tr.tipotractor, ".
                        "'' as centrodistribucion, ".
                        "0 as viaje, ".
                        "0 as kilometrostabulados, ".
                        "0 as kilometroscomprobados, ".
                        "'' as nombreChofer, ".
                        "0 as numTalonesValidosViaje, ".
                        "'' AS descPlazaOrigen, ".
                        "'' AS descPlazaDestino, ".
                        "0  AS idPlazaOrigen, ". 
                        "0  AS idPlazaDestino, ".
                        "0  AS claveChoferAcompanante, ".
                        "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                        "FROM catractorestbl tr ".
                        "WHERE tr.idTractor NOT IN (SELECT vt.idTractor FROM trviajestractorestbl vt) ".
                        "AND tr.estatus = 1 ".
                        "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ";
        }
        if($_REQUEST['trViajesTractoresListaEsperaHdn'] != ''){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isEnLista = "(SELECT centroDistribucion FROM trEsperaChoferesTbl ec ".
                         "WHERE ec.idTractor = tr.idTractor ".
                         "AND ec.centroDistribucion <> '".$_SESSION['usuCompania']."') AS centroDistribucionEspera, ";
        }
        $sqlGetTractoresDisponibles = "SELECT vt.idViajeTractor, vt.idViajePadre, tr.idTractor, tr.tractor, tr.rendimiento, vt.claveMovimiento, vt.claveChofer, vt.numeroRepartos, ".
                                      "tr.tipoTractor, vt.centroDistribucion, vt.viaje, vt.kilometrosTabulados, vt.kilometrosComprobados, ".
                                      "(SELECT CONCAT(ch.claveChofer, ' - ', ch.nombre, ' ', ch.apellidoPaterno, ' ', ch.apellidoMaterno) ".
                                      "FROM caChoferesTbl ch WHERE ch.claveChofer = vt.claveChofer) AS nombreChofer, ".
                                      $isEnLista.
                                      "(SELECT COUNT(*) FROM trTalonesViajesTbl tv WHERE vt.idViajeTractor = tv.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino, ".
                                      "(SELECT vt2.claveChofer FROM trViajesTractoresTbl vt2 WHERE vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante, ".
                                      "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                                      "FROM caTractoresTbl tr ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.idTractor = tr.idTractor ".$lsWhereStr.$isTractoresNuevos ;


        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);

    //}


        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
          $rs['root'][$nInt]['centroDistSesion'] = $_SESSION['usuCompania'];
        }

        echo json_encode($rs);
    }

     function getTalonesViajeAsig(){
        $lsWhereStr = "WHERE tv.distribuidor = dc.distribuidorCentro ".
                      "AND tv.idViajeTractor = vt.idViajeTractor ".
                      "AND vt.idTractor = tr.idTractor ".
                      "AND ch.claveChofer = vt.claveChofer ".
                      "AND es.idPais = pa.idPais ".
                      "AND mu.idEstado = es.idEstado ".
                      "AND cl.idMunicipio = mu.idMunicipio ".
                      "AND dr.idColonia = cl.idColonia ".
                      "AND dr.direccion = tv.direccionEntrega ".
                      //"AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].", ".
                      "AND tv.claveMovimiento = 'TV' ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "vt.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresViajeHdn'], "vt.viaje", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTalonHdn'], "tv.idTalon", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresDistribuidorHdn'], "tv.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresFolioTxt'], "tv.folio", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovimientoHdn'], "tv.claveMovimiento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "tv.centroDistribucion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "tr.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresNumTractorHdn'], "tr.tractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovViajeHdn'], "vt.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTipoDoctoHdn'], "tv.tipoDocumento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaRemitenteHdn'], "tv.companiaRemitente", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }


        $sqlGetTalones = "SELECT 0 AS temp, tv.*, dc.descripcionCentro, dc.tipoDistribuidor, tr.idTractor, tr.rendimiento, ".
                                 "vt.fechaEvento AS fechaViaje, vt.numeroRepartos, vt.kilometrosTabulados, vt.viaje, ".
                                 "vt.claveMovimiento AS claveMovimientoViaje, vt.numeroUnidades AS numeroUnidadesViaje, ".
                                 " tr.tractor, tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                 "(SELECT dc3.descripcionCentro FROM caDistribuidoresCentrosTbl dc3 ".
                                    "WHERE dc3.distribuidorCentro = tv.companiaRemitente) AS descripcionCompania, ".
                                 "(SELECT COUNT(*) FROM trTalonesViajesTbl tv2 WHERE tv2.idViajeTractor = vt.idViajeTractor ".
                                    "AND tv2.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                 "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tv.idTalon) AS numeroUnidadesTmp, ".
                                 "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                 "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                 "(SELECT pl2.plaza FROM caPlazasTbl pl2 WHERE pl2.idPlaza = tv.idPlazaDestino) AS nombrePlazaDestino,  ".
                                 "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' ".
                                    "AND cg.columna='claveMovimiento' AND cg.valor = tv.claveMovimiento) AS descClaveMovTalon, ".
                                 "(SELECT cg2.nombre FROM caGeneralesTbl cg2 WHERE cg2.tabla='trTalonesViajesTbl' ".
                                    "AND cg2.columna='claveMovimiento' AND cg2.valor = tv.claveMovimiento) AS descTipoDocto, ".
                                 "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' ".
                                    "AND cg.columna='tipoTalon' AND cg.valor = tv.tipoTalon) AS nombreTipoTalon, ".
                                 "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                    "WHERE dc2.distribuidorCentro = vt.centroDistribucion) AS nombreCentroDist, ".
                                 "pa.pais, es.estado, mu.municipio, cl.colonia, cl.cp, dr.calleNumero ".
                                 "FROM trTalonesViajesTbl tv, caDistribuidoresCentrosTbl dc, trViajesTractoresTbl vt, ".
                                 "caTractoresTbl tr, caChoferesTbl ch, caPaisesTbl pa, caEstadosTbl es, ".
                                 "caMunicipiosTbl mu, caColoniasTbl cl, caDireccionesTbl dr ";


        if($_REQUEST['trViajesTractoresAsignacionHdn'] != ''){
            $sqlGetTalones .=   " UNION ".
                                "SELECT 1 AS temp, tt.numeroTalon, tt.distribuidor, 'ESP' AS folio, tt.idViajeTractor, tt.companiaRemitente,".
                                "-1 AS plazaOrigen, -1 AS plazaDestino, tt.direccionEntrega, tt.centroDistribucion, ".
                                "tt.tipoTalon, '".date("Y-m-d H:i:s")."', tt.observaciones, tt.numeroUnidades, ".
                                "0 AS importe, 0 AS seguro, 0 AS tarifaCobrar, 0 AS kilometrosCobrar, 0 AS impuesto, ".
                                "0 AS retencion, 'TV' AS claveMovimiento, 'TV' AS tipoDocumento, ".
                                "dc.descripcionCentro, dc.tipoDistribuidor, tr.idTractor, tr.rendimiento, vt.fechaEvento AS fechaViaje, vt.numeroRepartos,".
                                "vt.kilometrosTabulados, vt.viaje, vt.claveMovimiento AS claveMovimientoViaje, vt.numeroUnidades AS numeroUnidadesViaje, tr.tractor, ".
                                "tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                "(SELECT dc3.descripcionCentro FROM caDistribuidoresCentrosTbl dc3 WHERE dc3.distribuidorCentro = tt.companiaRemitente) AS descripcionCompania, ".
                                "0 AS numTalonesValidosViaje, ".
                                "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tt.numeroTalon) AS numeroUnidadesTmp, ".
                                "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                "'' AS nombrePlazaOrigen, ".
                                "(SELECT pl2.plaza FROM caPlazasTbl pl2, caDistribuidoresCentrosTbl dc WHERE dc.distribuidorCentro = tt.distribuidor ".
                                    "AND pl2.idPlaza = dc.idPlaza) AS nombrePlazaDestino, ".
                                "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' AND cg.columna='claveMovimiento' ".
                                    "AND cg.valor = claveMovimiento) AS descClaveMovTalon, ".
                                "(SELECT cg2.nombre FROM caGeneralesTbl cg2 WHERE cg2.tabla='trTalonesViajesTbl' AND cg2.columna='claveMovimiento' ".
                                    "AND cg2.valor = claveMovimiento) AS descTipoDocto, ".
                                "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' AND cg.columna='tipoTalon' ".
                                    "AND cg.valor = tt.tipoTalon) AS nombreTipoTalon, ".
                                "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 WHERE dc2.distribuidorCentro = vt.centroDistribucion) AS nombreCentroDist, ".
                                "pa.pais, es.estado, mu.municipio, cl.colonia, cl.cp, dr.calleNumero ".
                                "FROM trtalonesviajestmp tt, caDistribuidoresCentrosTbl dc, trViajesTractoresTbl vt, caTractoresTbl tr, caChoferesTbl ch, caPaisesTbl pa, ".
                                "caEstadosTbl es, caMunicipiosTbl mu, caColoniasTbl cl, caDireccionesTbl dr ".
                                "WHERE tt.distribuidor = dc.distribuidorCentro AND tt.idViajeTractor = vt.idViajeTractor AND vt.idTractor = tr.idTractor ".
                                "AND ch.claveChofer = vt.claveChofer AND es.idPais = pa.idPais AND mu.idEstado = es.idEstado ".
                                "AND cl.idMunicipio = mu.idMunicipio AND dr.idColonia = cl.idColonia AND dr.direccion = tt.direccionEntrega AND claveMovimiento != 'TX' ".
                                "AND vt.idViajeTractor = 229 ";
        } else {
            $sqlGetTalones .= $lsWhereStr;
        }

        $rs = fn_ejecuta_query($sqlGetTalones);
       // echo json_encode($sqlGetTalones);
        $dist = array();
        $talonesValidos = 0; //TALONES NO CANCELADOS DEL VIAJE

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['descDistribuidor'] = $rs['root'][$nInt]['distribuidor']." - ".$rs['root'][$nInt]['descripcionCentro'];
            $rs['root'][$nInt]['descCompania'] = $rs['root'][$nInt]['companiaRemitente']." - ".$rs['root'][$nInt]['descripcionCompania'];
            $rs['root'][$nInt]['descTractorCia'] = $rs['root'][$nInt]['ciaTractor']." - ".$rs['root'][$nInt]['descCiaTractor'];
            $rs['root'][$nInt]['nombreChofer'] = $rs['root'][$nInt]['claveChofer']." - ".
                                                 $rs['root'][$nInt]['nombre']." ".
                                                 $rs['root'][$nInt]['apellidoPaterno']." ".
                                                 $rs['root'][$nInt]['apellidoMaterno'];
            $rs['root'][$nInt]['direccionCompleta'] = $rs['root'][$nInt]['calleNumero'].", ".
                                                      $rs['root'][$nInt]['colonia'].", ".
                                                      $rs['root'][$nInt]['municipio'].", ".
                                                      $rs['root'][$nInt]['estado'].", ".
                                                      $rs['root'][$nInt]['pais'].", ".
                                                      $rs['root'][$nInt]['cp'];

            $rs['root'][$nInt]['fechaActual'] = date('d-m-Y');
            $rs['root'][$nInt]['fechaEvento'] = date('Y-m-d', strtotime($rs['root'][$nInt]['fechaEvento']));


            if(!in_array($rs['root'][$nInt]['distribuidor'], $dist)) {
                array_push($dist, $rs['root'][$nInt]['distribuidor']);
            }
        }

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['numeroDistribuidores'] = sizeof($dist);
        }
        return $rs;
    }

    function getTalonesCancelacion(){
        $lsWhereStr = "WHERE tv.distribuidor = dc.distribuidorCentro ".
                      "AND tv.idViajeTractor = vt.idViajeTractor ".
                      "AND vt.idTractor = tr.idTractor ".
                      "AND ch.claveChofer = vt.claveChofer ".
                      "AND es.idPais = pa.idPais ".
                      "AND mu.idEstado = es.idEstado ".
                      "AND cl.idMunicipio = mu.idMunicipio ".
                      "AND dr.idColonia = cl.idColonia ".
                      "AND dr.direccion = tv.direccionEntrega ".
                      //"AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].", ".
                      "AND tv.claveMovimiento = 'TV' ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "vt.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresViajeHdn'], "vt.viaje", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTalonHdn'], "tv.idTalon", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresDistribuidorHdn'], "tv.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresFolioTxt'], "tv.folio", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovimientoHdn'], "tv.claveMovimiento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "tv.centroDistribucion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "tr.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresNumTractorHdn'], "tr.tractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovViajeHdn'], "vt.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTipoDoctoHdn'], "tv.tipoDocumento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaRemitenteHdn'], "tv.companiaRemitente", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }


        $sqlGetTalones = "SELECT 0 AS temp, tv.*, dc.descripcionCentro, dc.tipoDistribuidor, tr.idTractor, tr.rendimiento, ".
                                 "vt.fechaEvento AS fechaViaje, vt.numeroRepartos, vt.kilometrosTabulados, vt.viaje, ".
                                 "vt.claveMovimiento AS claveMovimientoViaje, vt.numeroUnidades AS numeroUnidadesViaje, ".
                                 " tr.tractor, tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                 "(SELECT dc3.descripcionCentro FROM caDistribuidoresCentrosTbl dc3 ".
                                    "WHERE dc3.distribuidorCentro = tv.companiaRemitente) AS descripcionCompania, ".
                                 "(SELECT COUNT(*) FROM trTalonesViajesTbl tv2 WHERE tv2.idViajeTractor = vt.idViajeTractor ".
                                    "AND tv2.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                 "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tv.idTalon) AS numeroUnidadesTmp, ".
                                 "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                 "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                 "(SELECT pl2.plaza FROM caPlazasTbl pl2 WHERE pl2.idPlaza = tv.idPlazaDestino) AS nombrePlazaDestino,  ".
                                 "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' ".
                                    "AND cg.columna='claveMovimiento' AND cg.valor = tv.claveMovimiento) AS descClaveMovTalon, ".
                                 "(SELECT cg2.nombre FROM caGeneralesTbl cg2 WHERE cg2.tabla='trTalonesViajesTbl' ".
                                    "AND cg2.columna='claveMovimiento' AND cg2.valor = tv.claveMovimiento) AS descTipoDocto, ".
                                 "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' ".
                                    "AND cg.columna='tipoTalon' AND cg.valor = tv.tipoTalon) AS nombreTipoTalon, ".
                                 "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                    "WHERE dc2.distribuidorCentro = vt.centroDistribucion) AS nombreCentroDist, ".
                                 "pa.pais, es.estado, mu.municipio, cl.colonia, cl.cp, dr.calleNumero ".
                                 "FROM trTalonesViajesTbl tv, caDistribuidoresCentrosTbl dc, trViajesTractoresTbl vt, ".
                                 "caTractoresTbl tr, caChoferesTbl ch, caPaisesTbl pa, caEstadosTbl es, ".
                                 "caMunicipiosTbl mu, caColoniasTbl cl, caDireccionesTbl dr ";


        if($_REQUEST['trViajesTractoresAsignacionHdn'] != ''){
            $sqlGetTalones .=   " UNION ".
                                "SELECT 1 AS temp, tt.numeroTalon, tt.distribuidor, 'ESP' AS folio, tt.idViajeTractor, tt.companiaRemitente,".
                                "-1 AS plazaOrigen, -1 AS plazaDestino, tt.direccionEntrega, tt.centroDistribucion, ".
                                "tt.tipoTalon, '".date("Y-m-d H:i:s")."', tt.observaciones, tt.numeroUnidades, ".
                                "0 AS importe, 0 AS seguro, 0 AS tarifaCobrar, 0 AS kilometrosCobrar, 0 AS impuesto, ".
                                "0 AS retencion, 'TV' AS claveMovimiento, 'TV' AS tipoDocumento, ".
                                "dc.descripcionCentro, dc.tipoDistribuidor, tr.idTractor, tr.rendimiento, vt.fechaEvento AS fechaViaje, vt.numeroRepartos,".
                                "vt.kilometrosTabulados, vt.viaje, vt.claveMovimiento AS claveMovimientoViaje, vt.numeroUnidades AS numeroUnidadesViaje, tr.tractor, ".
                                "tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                "(SELECT dc3.descripcionCentro FROM caDistribuidoresCentrosTbl dc3 WHERE dc3.distribuidorCentro = tt.companiaRemitente) AS descripcionCompania, ".
                                "0 AS numTalonesValidosViaje, ".
                                "(SELECT COUNT(*) FROM trUnidadesDetallesTalonesTmp tt2 WHERE tt2.numeroTalon = tt.numeroTalon) AS numeroUnidadesTmp, ".
                                "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                "'' AS nombrePlazaOrigen, ".
                                "(SELECT pl2.plaza FROM caPlazasTbl pl2, caDistribuidoresCentrosTbl dc WHERE dc.distribuidorCentro = tt.distribuidor ".
                                    "AND pl2.idPlaza = dc.idPlaza) AS nombrePlazaDestino, ".
                                "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' AND cg.columna='claveMovimiento' ".
                                    "AND cg.valor = claveMovimiento) AS descClaveMovTalon, ".
                                "(SELECT cg2.nombre FROM caGeneralesTbl cg2 WHERE cg2.tabla='trTalonesViajesTbl' AND cg2.columna='claveMovimiento' ".
                                    "AND cg2.valor = claveMovimiento) AS descTipoDocto, ".
                                "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trTalonesViajesTbl' AND cg.columna='tipoTalon' ".
                                    "AND cg.valor = tt.tipoTalon) AS nombreTipoTalon, ".
                                "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 WHERE dc2.distribuidorCentro = vt.centroDistribucion) AS nombreCentroDist, ".
                                "pa.pais, es.estado, mu.municipio, cl.colonia, cl.cp, dr.calleNumero ".
                                "FROM trtalonesviajestmp tt, caDistribuidoresCentrosTbl dc, trViajesTractoresTbl vt, caTractoresTbl tr, caChoferesTbl ch, caPaisesTbl pa, ".
                                "caEstadosTbl es, caMunicipiosTbl mu, caColoniasTbl cl, caDireccionesTbl dr ".
                                "WHERE tt.distribuidor = dc.distribuidorCentro AND tt.idViajeTractor = vt.idViajeTractor AND vt.idTractor = tr.idTractor ".
                                "AND ch.claveChofer = vt.claveChofer AND es.idPais = pa.idPais AND mu.idEstado = es.idEstado ".
                                "AND cl.idMunicipio = mu.idMunicipio AND dr.idColonia = cl.idColonia AND dr.direccion = tt.direccionEntrega AND claveMovimiento != 'TX' ".
                                "AND vt.idViajeTractor = 229 ";
        } else {
            $sqlGetTalones .= $lsWhereStr;
        }

        $rs = fn_ejecuta_query($sqlGetTalones);
        //echo json_encode($rs);
        $dist = array();
        $talonesValidos = 0; //TALONES NO CANCELADOS DEL VIAJE

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['descDistribuidor'] = $rs['root'][$nInt]['distribuidor']." - ".$rs['root'][$nInt]['descripcionCentro'];
            $rs['root'][$nInt]['descCompania'] = $rs['root'][$nInt]['companiaRemitente']." - ".$rs['root'][$nInt]['descripcionCompania'];
            $rs['root'][$nInt]['descTractorCia'] = $rs['root'][$nInt]['ciaTractor']." - ".$rs['root'][$nInt]['descCiaTractor'];
            $rs['root'][$nInt]['nombreChofer'] = $rs['root'][$nInt]['claveChofer']." - ".
                                                 $rs['root'][$nInt]['nombre']." ".
                                                 $rs['root'][$nInt]['apellidoPaterno']." ".
                                                 $rs['root'][$nInt]['apellidoMaterno'];
            $rs['root'][$nInt]['direccionCompleta'] = $rs['root'][$nInt]['calleNumero'].", ".
                                                      $rs['root'][$nInt]['colonia'].", ".
                                                      $rs['root'][$nInt]['municipio'].", ".
                                                      $rs['root'][$nInt]['estado'].", ".
                                                      $rs['root'][$nInt]['pais'].", ".
                                                      $rs['root'][$nInt]['cp'];

            $rs['root'][$nInt]['fechaActual'] = date('d-m-Y');
            $rs['root'][$nInt]['fechaEvento'] = date('Y-m-d', strtotime($rs['root'][$nInt]['fechaEvento']));            


            if(!in_array($rs['root'][$nInt]['distribuidor'], $dist)) {
                array_push($dist, $rs['root'][$nInt]['distribuidor']);                
            }
        }

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['numeroDistribuidores'] = sizeof($dist);            
        }
        echo json_encode($rs);
        //return $rs;
    }

    function actualizaEstatus(){
        $sqlViaje="SELECT * FROM trViajesTractoresTbl where idTractor='".$_REQUEST['tractor']."' AND viaje=(SELECT MAX(viaje) from trViajesTractoresTbl where idtractor='".$_REQUEST['tractor']."')";
        $rsViaje=fn_ejecuta_query($sqlViaje);

        if ($rsViaje['root'][0]['claveMovimiento']!='PC') {

            $updViaje="UPDATE trViajesTractoresTbl set claveMovimiento='PC' where idViajeTractor='".$rsViaje['root'][0]['idViajeTractor']."' ";
            fn_ejecuta_query($updViaje);
        }else{

            $sqlViaje="SELECT * FROM trViajesTractoresTbl where idTractor='".$_REQUEST['tractor']."' AND claveMovimiento='PC' ";
            $rsViaje=fn_ejecuta_query($sqlViaje);


            $updViaje="UPDATE trViajesTractoresTbl set claveMovimiento='VU' where idViajeTractor='".$rsViaje['root'][0]['idViajeTractor']."' ";
            fn_ejecuta_query($updViaje);

            $insGU="INSERT INTO trgastosviajetractortbl (idGastoTractor, idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, importe, observaciones, claveMovimiento, usuario, ip) ".
                "VALUES (NULL, '".$rsViaje['root'][0]['idViajeTractor']."', '0', '".$_REQUEST['ciasesval']."', '0', now(), '0.00', 'viajeLiberado: CuentasTol', 'GU', '".$_SESSION['idUsuario']."', '172.106.0.254')";
            fn_ejecuta_query($insGU);

            
        }

    }

    function quitarLiberacion(){
        $sqlViaje="SELECT * FROM trViajesTractoresTbl where idTractor='".$_REQUEST['tractor']."' AND viaje=(SELECT MAX(viaje) from trViajesTractoresTbl where idtractor='".$_REQUEST['tractor']."')";
        $rsViaje=fn_ejecuta_query($sqlViaje);

        if ($rsViaje['root'][0]['claveMovimiento']!='VU') {

            
        }else{

            $sqlViaje="SELECT * FROM trViajesTractoresTbl where idTractor='".$_REQUEST['tractor']."' AND claveMovimiento='VU' ";
            $rsViaje=fn_ejecuta_query($sqlViaje);


            $updViaje="UPDATE trViajesTractoresTbl set claveMovimiento='VE' where idViajeTractor='".$rsViaje['root'][0]['idViajeTractor']."' ";
            fn_ejecuta_query($updViaje);

            $updViaje="DELETE FROM  trGastosViajeTractorTbl WHERE claveMovimiento='GU' AND idViajeTractor='".$rsViaje['root'][0]['idViajeTractor']."' ";
            fn_ejecuta_query($updViaje);           

            
        }

    }

    function getTractoresDisponiblesPend(){

   $lsWhereStr = "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.idTractor = tr.idTractor AND (vt2.idViajePadre IS NULL OR vt2.idViajePadre IS NOT NULL AND vt2.claveMovimiento <> 'VX')) ".
                      "WHERE  tr.estatus = 1 ".
                      "AND vt.idViajeTractor is not null";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = ("vt.claveMovimiento IN ('VC','VF','VE','PC')");
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);            
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if($_REQUEST['trTractoresNuevosHdn'] == '1'){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isTractoresNuevos = " UNION ".
                        "SELECT 0 as idviajeTractor, ".
                        "0 as idviajepadre, ".
                        "tr.idtractor, ".
                        "tr.tractor, ".
                        "tr.rendimiento, ". 
                        "'' as clavemovimiento, ".
                        "0 as clavechofer, ".
                        "0 as numerorepartos, ".
                        "tr.tipotractor, ".
                        "'' as centrodistribucion, ".
                        "0 as viaje, ".
                        "0 as kilometrostabulados, ".
                        "0 as kilometroscomprobados, ".
                        "'' as nombreChofer, ".
                        "0 as numTalonesValidosViaje, ".
                        "'' AS descPlazaOrigen, ".
                        "'' AS descPlazaDestino, ".
                        "0  AS idPlazaOrigen, ". 
                        "0  AS idPlazaDestino, ".
                        "0  AS claveChoferAcompanante, ".
                        "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                        "FROM catractorestbl tr ".
                        "WHERE tr.idTractor NOT IN (SELECT vt.idTractor FROM trviajestractorestbl vt) ".
                        "AND tr.estatus = 1 ".
                        "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ";
        }
        if($_REQUEST['trViajesTractoresListaEsperaHdn'] != ''){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isEnLista = "(SELECT centroDistribucion FROM trEsperaChoferesTbl ec ".
                         "WHERE ec.idTractor = tr.idTractor ".
                         "AND ec.centroDistribucion <> '".$_SESSION['usuCompania']."') AS centroDistribucionEspera, ";
        }
        $sqlGetTractoresDisponibles = "SELECT vt.idViajeTractor, vt.idViajePadre, tr.idTractor, tr.tractor, tr.rendimiento, vt.claveMovimiento, vt.claveChofer, vt.numeroRepartos, ".
                                      "tr.tipoTractor, vt.centroDistribucion, vt.viaje, vt.kilometrosTabulados, vt.kilometrosComprobados, ".
                                      "(SELECT CONCAT(ch.claveChofer, ' - ', ch.nombre, ' ', ch.apellidoPaterno, ' ', ch.apellidoMaterno) ".
                                      "FROM caChoferesTbl ch WHERE ch.claveChofer = vt.claveChofer) AS nombreChofer, ".
                                      $isEnLista.
                                      "(SELECT COUNT(*) FROM trTalonesViajesTbl tv WHERE vt.idViajeTractor = tv.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino, ".
                                      "(SELECT vt2.claveChofer FROM trViajesTractoresTbl vt2 WHERE vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante, ".
                                      "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                                      "FROM caTractoresTbl tr ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.idTractor = tr.idTractor ".$lsWhereStr.$isTractoresNuevos ;


        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);

    //}


        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
          $rs['root'][$nInt]['centroDistSesion'] = $_SESSION['usuCompania'];
        }

        echo json_encode($rs);
    }

    function getTractoresDisponiblesLib(){

   $lsWhereStr = "AND vt.viaje = (SELECT MAX(vt2.viaje) FROM trViajesTractoresTbl vt2 ".
                      "WHERE vt2.idTractor = tr.idTractor AND (vt2.idViajePadre IS NULL OR vt2.idViajePadre IS NOT NULL AND vt2.claveMovimiento <> 'VX')) ".
                      "WHERE  tr.estatus = 1 ".
                      "AND vt.idViajeTractor is not null";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = ("vt.claveMovimiento IN ('VU')");
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);            
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCompaniaHdn'], "tr.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if($_REQUEST['trTractoresNuevosHdn'] == '1'){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isTractoresNuevos = " UNION ".
                        "SELECT 0 as idviajeTractor, ".
                        "0 as idviajepadre, ".
                        "tr.idtractor, ".
                        "tr.tractor, ".
                        "tr.rendimiento, ". 
                        "'' as clavemovimiento, ".
                        "0 as clavechofer, ".
                        "0 as numerorepartos, ".
                        "tr.tipotractor, ".
                        "'' as centrodistribucion, ".
                        "0 as viaje, ".
                        "0 as kilometrostabulados, ".
                        "0 as kilometroscomprobados, ".
                        "'' as nombreChofer, ".
                        "0 as numTalonesValidosViaje, ".
                        "'' AS descPlazaOrigen, ".
                        "'' AS descPlazaDestino, ".
                        "0  AS idPlazaOrigen, ". 
                        "0  AS idPlazaDestino, ".
                        "0  AS claveChoferAcompanante, ".
                        "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                        "FROM catractorestbl tr ".
                        "WHERE tr.idTractor NOT IN (SELECT vt.idTractor FROM trviajestractorestbl vt) ".
                        "AND tr.estatus = 1 ".
                        "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ";
        }
        if($_REQUEST['trViajesTractoresListaEsperaHdn'] != ''){// Pregunta si está en la Lista de Espera de otro Centro Distribución
            $isEnLista = "(SELECT centroDistribucion FROM trEsperaChoferesTbl ec ".
                         "WHERE ec.idTractor = tr.idTractor ".
                         "AND ec.centroDistribucion <> '".$_SESSION['usuCompania']."') AS centroDistribucionEspera, ";
        }
        $sqlGetTractoresDisponibles = "SELECT vt.idViajeTractor, vt.idViajePadre, tr.idTractor, tr.tractor, tr.rendimiento, vt.claveMovimiento, vt.claveChofer, vt.numeroRepartos, ".
                                      "tr.tipoTractor, vt.centroDistribucion, vt.viaje, vt.kilometrosTabulados, vt.kilometrosComprobados, ".
                                      "(SELECT CONCAT(ch.claveChofer, ' - ', ch.nombre, ' ', ch.apellidoPaterno, ' ', ch.apellidoMaterno) ".
                                      "FROM caChoferesTbl ch WHERE ch.claveChofer = vt.claveChofer) AS nombreChofer, ".
                                      $isEnLista.
                                      "(SELECT COUNT(*) FROM trTalonesViajesTbl tv WHERE vt.idViajeTractor = tv.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, ".
                                      "(SELECT p.plaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen, ".
                                      "(SELECT p.idPlaza FROM caPlazasTbl p WHERE p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino, ".
                                      "(SELECT vt2.claveChofer FROM trViajesTractoresTbl vt2 WHERE vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante, ".
                                      "(SELECT 1 as bloqueado FROM trviajestractorestmp tmp WHERE  tmp.idTractor= tr.idTractor) as bloqueado ".
                                      "FROM caTractoresTbl tr ".
                                      "LEFT JOIN trViajesTractoresTbl vt ON vt.idTractor = tr.idTractor ".$lsWhereStr.$isTractoresNuevos ;


        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);

    //}


        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
          $rs['root'][$nInt]['centroDistSesion'] = $_SESSION['usuCompania'];
        }

        echo json_encode($rs);
    }
////////////////////impresion poliza
    function getHistoricoViajesImpresion(){


        if ($_REQUEST['trViajesTractoresEsPolizaHdn'] === '1') {
                $lsWhereStr = "WHERE vt.claveChofer = ch.claveChofer ".
                      /*"AND vt.viaje= (SELECT min(vi.viaje) FROM trViajesTractoresTbl vi WHERE vi.claveChofer = vt.claveChofer AND ( vi.claveMovimiento = 'VC' OR  vi.claveMovimiento = 'VE' OR  vi.claveMovimiento = 'VO' OR  vi.claveMovimiento = 'VU')) ".*/
                      "AND vt.viaje= (SELECT min(vi.viaje) FROM trViajesTractoresTbl vi WHERE vi.claveChofer = vt.claveChofer AND ( vi.claveMovimiento in ('VU','UV'))) ".
                      "AND vt.idTractor = tr.idTractor ";
        }else{
                $lsWhereStr = "WHERE vt.claveChofer = ch.claveChofer ".
                              "AND vt.idTractor = tr.idTractor ";
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajeHdn'], "vt.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdTractorHdn'], "vt.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdPlazaOrigenHdn'], "vt.idPlazaOrigen", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdPlazaDestinoHdn'], "vt.idPlazaDestino", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresCentroDistHdn'], "vt.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresViajeTxt'], "vt.viaje", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmTabuladosTxt'], "vt.kilometrosTabulados", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmComprobadosTxt'], "vt.kilometrosComprobados", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresKmSinUnidadTxt'], "vt.kilometrosSinUnidad", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresNumUnidadesTxt'], "vt.numeroUnidades", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresRepartosTxt'], "vt.numeroRepartos", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresIdViajePadreHdn'], "vt.idViajePadre", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            //$lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveMovimientoHdn'], "vt.claveMovimiento", 1);

            $lsCondicionStr = " vt.claveMovimiento in ('VP')";
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
         if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveAcompañanteHdn'], "vt.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresClaveChoferHdn'], "ch.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractoresTractorHdn'], "tr.tractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        //FECHA
        //Desde
        if ($_REQUEST['trViajesTractoresFechaDesdeTxt'] != "") {
            if ($lsWhereStr == "") {
                   $lsWhereStr .= " WHERE vt.fechaEvento >= '".$_REQUEST['trViajesTractoresFechaDesdeTxt']."' ";
            } else {
                $lsWhereStr .= " AND vt.fechaEvento >= '".$_REQUEST['trViajesTractoresFechaDesdeTxt']."' ";
            }
        }
        //Hasta
        if ($_REQUEST['trViajesTractoresFechaHastaTxt'] != "") {
            if ($lsWhereStr == "") {
                $lsWhereStr .= " WHERE date_format(vt.fechaEvento, '%Y-%m-%d') <= '".$_REQUEST['trViajesTractoresFechaHastaTxt']."' ";
            } else {
                $lsWhereStr .= " AND date_format(vt.fechaEvento, '%Y-%m-%d') <= '".$_REQUEST['trViajesTractoresFechaHastaTxt']."' ";
            }
        }

        //CHOFERES
        //Desde
        if ($_REQUEST['trViajesTractoresChoferDesdeTxt'] != "") {
            if ($lsWhereStr == "") {
                   $lsWhereStr .= " WHERE vt.claveChofer >= '".$_REQUEST['trViajesTractoresChoferDesdeTxt']."' ";
            } else {
                $lsWhereStr .= " AND vt.claveChofer >= '".$_REQUEST['trViajesTractoresChoferDesdeTxt']."' ";
            }
        }
        //Hasta
        if ($_REQUEST['trViajesTractoresChoferHastaTxt'] != "") {
            if ($lsWhereStr == "") {
                $lsWhereStr .= " WHERE vt.claveChofer <= '".$_REQUEST['trViajesTractoresChoferHastaTxt']."' ";
            } else {
                $lsWhereStr .= " AND vt.claveChofer <= '".$_REQUEST['trViajesTractoresChoferHastaTxt']."' ";
            }
        }
        //Tractor, descripcion clave mov
        $sqlGetHistoricoViajesStr = "SELECT vt.*, ch.apellidoPaterno, ch.apellidoMaterno, ch.nombre, ch.centroDistribucionOrigen, tr.tractor, ".
                                    "COUNT(tv.idTalon) AS numeroTalones, SUM(tv.numeroUnidades) AS totalUnidades, ".
                                    "tr.rendimiento, tr.compania, ".
                                    "(SELECT COUNT(*) FROM trTalonesViajesTbl tv ".
                                        "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                                    "(SELECT co.descripcion FROM caCompaniasTbl co WHERE co.compania = tr.compania) AS nombreCompania, ".
                                    "(SELECT tr.tractor FROM caTractoresTbl tr WHERE tr.idTractor = vt.idTractor) AS tractor, ".
                                    "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trViajesTractoresTbl' ".
                                    "AND cg.columna='claveMovimiento' AND cg.valor = vt.claveMovimiento) AS nombreClaveMov, ".
                                    "(SELECT COUNT(*) FROM trTalonesViajesTbl tv ".
                                        "WHERE tv.idViajeTractor = vt.idViajeTractor) AS numeroTalones, ".
                                    "(SELECT COUNT(DISTINCT tv.distribuidor) FROM trTalonesViajesTbl tv ".
                                        "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                        "AND tv.claveMovimiento != 'TX') AS numDistribuidorValidos, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = vt.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                    "(SELECT pl2.plaza FROM caPlazasTbl pl2 WHERE pl2.idPlaza = vt.idPlazaDestino) AS nombrePlazaDestino, ".
                                    "(SELECT 1 FROM trviajestractorestbl vt1, trgastosviajetractortbl gt WHERE vt1.idViajeTractor = gt.idViajeTractor AND vt1.claveMovimiento  in ('VU','UV')  AND gt.claveMovimiento = 'GP' AND vt1.idViajeTractor = vt.idViajeTractor LIMIT 1 ) as polizaTotal ".
                                    "FROM caChoferesTbl ch, caTractoresTbl tr, trViajesTractoresTbl vt ".
                                    "LEFT JOIN trtalonesviajestbl tv ON tv.idViajeTractor = vt.idViajeTractor ".
                                    $lsWhereStr." GROUP BY vt.idViajeTractor ";

        $rs = fn_ejecuta_query($sqlGetHistoricoViajesStr);

        $sqlGetNumDistribuidoresStr = "SELECT folio, distribuidor FROM trTalonesViajesTbl ".
                                        "WHERE idViajeTractor = ".$rs['root'][0]['idViajeTractor']." ".
                                        "AND claveMovimiento !='TX' ";

        $rsTalones = fn_ejecuta_query($sqlGetNumDistribuidoresStr);
        $dist = array();

        if(sizeof($rsTalones['root']) > 0){
            for ($nInt=0; $nInt < sizeof($rsTalones['root']); $nInt++) {
                if(!in_array($rsTalones['root'][$nInt]['distribuidor'], $dist)){
                    array_push($dist, $rsTalones['root'][$nInt]['distribuidor']);
                }
            }

            $distNum = sizeof($dist);
        } else {
            $distNum = 0;
        }

        if ($rs['records'] > 0) {
            $rs['idCombustible'] = 0;
            $rs['concepto'] = '';
            $rs['litros'] = 0;
            $selStr = "SELECT * FROM trCombustiblePendienteTbl ".
                      "WHERE claveChofer = ".$_REQUEST['trViajesTractoresClaveChoferHdn']." AND claveMovimiento = 'CP'";
            $combRs = fn_ejecuta_query($selStr);
            if ($combRs['records'] > 0) {
                $rs['idCombustible'] = $combRs['root'][0]['idCombustible'];
                $rs['concepto'] = $combRs['root'][0]['concepto'];
                $rs['litros'] = $combRs['root'][0]['litros'];
            }
        }

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['nombreChofer'] = $rs['root'][$nInt]['claveChofer']." - ".
                                                 $rs['root'][$nInt]['nombre']." ".
                                                 $rs['root'][$nInt]['apellidoPaterno']." ".
                                                 $rs['root'][$nInt]['apellidoMaterno'];

            $rs['root'][$nInt]['fechaEvento'] = date('Y-m-d', strtotime($rs['root'][$nInt]['fechaEvento']));
            $rs['root'][$nInt]['descCompania'] = $rs['root'][$nInt]['compania']." - ".$rs['root'][$nInt]['nombreCompania'];

           // $rs['root'][$nInt]['numeroDistribuidores'] = sizeof($dist);
$rs['root'][$nInt]['numeroDistribuidores'] = $rs['root'][$nInt]['numDistribuidorValidos'];
            //KILOMETROS ADICIONALES (COMPROBADOS - TABULADOS)
            $kmAdicionales = floatval($rs['root'][$nInt]['kilometrosComprobados']) - floatval($rs['root'][$nInt]['kilometrosTabulados']);
            $rs['root'][$nInt]['kilometrosAdicionales'] =  $kmAdicionales < 0 ? 0 : $kmAdicionales;

            //Obtiene y concatena todos los talones del viaje
            $sqlGetTalonesViajeStr = "SELECT folio FROM trTalonesViajesTbl ".
                                     "WHERE claveMovimiento != 'TX' ".
                                     "AND idViajeTractor = ".$rs['root'][$nInt]['idViajeTractor'];

            $rsTalones = fn_ejecuta_query($sqlGetTalonesViajeStr);
            $temp = "";

            for ($mInt=0; $mInt < sizeof($rsTalones['root']); $mInt++) {
                if ($mInt != 0) {
                  $temp .= "-";
                }
                $temp .= $rsTalones['root'][$mInt]['folio'];
            }

            $rs['root'][$nInt]['foliosTalonesViaje'] = $temp;
            
        }

        return $rs;
    }
    function obtenerOperador(){
        sleep(5);
        $sqlOperador="SELECT  a.viaje,a.centroDistribucion, concat(b.nombre,' ',b.apellidoPaterno,' ',b.apellidoMaterno) as nombre, (select f.plaza from caplazastbl f where f.idPlaza=a.idPlazaDestino) as destino
                        FROM trviajestractorestbl a, cachoferestbl b
                        where a.idTractor=".$_REQUEST['tractor']."
                        and a.claveMovimiento in('VU','VE')
                        and a.clavechofer=b.clavechofer";
        $rsOperador=fn_ejecuta_query($sqlOperador);
        echo json_encode($rsOperador);
    }


    function getFacturas(){
        sleep(3);
        $sqlFacturas="SELECT  folioFiscal, rfc,fechaEvento,desConcepto AS concepto,iva,subTotal,total
                        FROM trpolizafacturastbl
                        where idViajeTractor=".$_REQUEST['idViajeTractorHdn']."
                        AND estatus='CARGADO'";
        $rsFacturas=fn_ejecuta_query($sqlFacturas);
        echo json_encode($rsFacturas);
    }

    function autorizarFacturasHdn(){

        $folioFiscalArr = explode('|', substr($_REQUEST['folioFiscal'], 0, -1));
        if(in_array('', $vinArr)){
            $e[] = array('id'=>'folioFiscal','msg'=>getRequerido());
            $a['success'] = false;
        }
         //echo json_encode($folioFiscalArr);

        for ($i=0; $i <sizeof($folioFiscalArr) ; $i++) { 
            $updFacturas="UPDATE trpolizafacturastbl SET ESTATUS='AUTORIZADO' WHERE folioFiscal='".$folioFiscalArr[$i]."' ";
            $rsUpFacturas=fn_ejecuta_query($updFacturas);
        }
        

         $sqlFacturas="SELECT  folioFiscal, rfc,fechaEvento,desConcepto AS concepto,iva,subTotal,total
                        FROM trpolizafacturastbl
                        where idViajeTractor=".$_REQUEST['idViajeTractorHdn']."
                        AND estatus='AUTORIZADO'";
        $rsFacturas=fn_ejecuta_query($sqlFacturas);
        echo json_encode($rsFacturas);
    }
    function obtenerFacturasAutorizadas(){

    //sleep(3);
        $sqlOperador="SELECT  a.viaje,a.centroDistribucion, concat(b.nombre,' ',b.apellidoPaterno,' ',b.apellidoMaterno) as nombre, (select f.plaza from caplazastbl f where f.idPlaza=a.idPlazaDestino) as destino
                        FROM trviajestractorestbl a, cachoferestbl b
                        where a.idViajeTractor=".$_REQUEST['idViajeTractorHdn']."
                        and a.claveMovimiento='VU'
                        and a.clavechofer=b.clavechofer";
        $rsOperador=fn_ejecuta_query($sqlOperador);
        echo json_encode($rsOperador);
    }

    function getFacturasAutorizadas(){
        sleep(3);
        $sqlFacturas="SELECT  folioFiscal, rfc,fechaEvento,desConcepto AS concepto,iva,subTotal,total,descuento, ISH, ISR, IEPS
                        FROM trpolizafacturastbl
                        where idViajeTractor=".$_REQUEST['idViajeTractorHdn']."
                        AND estatus='AUTORIZADO'";
        $rsFacturas=fn_ejecuta_query($sqlFacturas);
        echo json_encode($rsFacturas);
    }

    function preCargaFactura(){
        $sqlFacturas="UPDATE trpolizafacturastbl set estatus='PRECARGADA'
                        WHERE folioFiscal='".$_REQUEST['folioFiscal']."'".
                        " AND estatus='AUTORIZADO'";
        $rsFacturas=fn_ejecuta_query($sqlFacturas);
        echo json_encode($rsFacturas);
    }

    function solicitudesPendientes(){
        $sqlFacturas="SELECT * from trviajestractorestbl a, autorizacionesespecialestbl b ".
                        "where a.idTractor='".$_REQUEST['tractor']."'".
                        "and a.claveMovimiento='VU' ".
                        "and a.claveChofer=b.claveChofer ".
                        "and b.estatus='SOLICITADO'";
        $rsFacturas=fn_ejecuta_query($sqlFacturas);
        echo json_encode($rsFacturas);
    }

    function validaFacturasOB7(){
        //sleep(3);
        $sqlFacturas="SELECT  folioFiscal, rfc,fechaEvento,desConcepto AS concepto,iva,subTotal,total,descuento, ISH, ISR, IEPS
                        FROM trpolizafacturastbl
                        where idViajeTractor=".$_REQUEST['idViajeTractor']."
                        AND estatus='CARGADO'";
        $rsFacturas=fn_ejecuta_query($sqlFacturas);
        echo json_encode($rsFacturas);
    }

    function getTractoresDisponiblesLiberacion(){
        $sqlGetTractoresDisponibles = "SELECT 
                                        vt.idViajeTractor,
                                        vt.idViajePadre,
                                        tr.idTractor,
                                        tr.tractor,
                                        tr.rendimiento,
                                        vt.claveMovimiento,
                                        vt.claveChofer,
                                        vt.numeroRepartos,
                                        tr.tipoTractor,
                                        vt.centroDistribucion,
                                        vt.viaje,
                                        vt.kilometrosTabulados,
                                        vt.kilometrosComprobados,
                                        (SELECT 
                                                CONCAT(ch.claveChofer,
                                                            ' - ',
                                                            ch.nombre,
                                                            ' ',
                                                            ch.apellidoPaterno,
                                                            ' ',
                                                            ch.apellidoMaterno)
                                            FROM
                                                caChoferesTbl ch
                                            WHERE
                                                ch.claveChofer = vt.claveChofer) AS nombreChofer,
                                        (SELECT 
                                                COUNT(*)
                                            FROM
                                                trTalonesViajesTbl tv
                                            WHERE
                                                vt.idViajeTractor = tv.idViajeTractor
                                                    AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje,
                                        (SELECT 
                                                p.plaza
                                            FROM
                                                caPlazasTbl p
                                            WHERE
                                                p.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen,
                                        (SELECT 
                                                p.plaza
                                            FROM
                                                caPlazasTbl p
                                            WHERE
                                                p.idPlaza = vt.idPlazaDestino) AS descPlazaDestino,
                                        (SELECT 
                                                p.idPlaza
                                            FROM
                                                caPlazasTbl p
                                            WHERE
                                                p.idPlaza = vt.idPlazaOrigen) AS idPlazaOrigen,
                                        (SELECT 
                                                p.idPlaza
                                            FROM
                                                caPlazasTbl p
                                            WHERE
                                                p.idPlaza = vt.idPlazaDestino) AS idPlazaDestino,
                                        (SELECT 
                                                vt2.claveChofer
                                            FROM
                                                trViajesTractoresTbl vt2
                                            WHERE
                                                vt.idViajePadre = vt2.idViajeTractor) AS claveChoferAcompanante,
                                        (SELECT 
                                                1 as bloqueado
                                            FROM
                                                trviajestractorestmp tmp
                                            WHERE
                                                tmp.idTractor = tr.idTractor) as bloqueado
                                    FROM
                                        caTractoresTbl tr, trviajestractorestbl vt, trgastosviajetractortbl tg
                                    WHERE
                                        tr.estatus = 1
                                            AND vt.idViajeTractor is not null
                                            AND vt.claveMovimiento IN ('VU','VE')
                                            AND vt.idTractor = tr.idTractor
                                            AND tg.idViajeTractor=vt.idViajeTractor
                                             AND tg.claveMovimiento='CA'
                                             AND tg.usuario=".$_SESSION['idUsuario']."
                                            AND vt.viaje = (SELECT 
                                                MAX(vt2.viaje)
                                            FROM
                                                trViajesTractoresTbl vt2
                                            WHERE
                                                vt2.idTractor = tr.idTractor
                                                    AND (vt2.idViajePadre IS NULL
                                                    OR vt2.idViajePadre IS NOT NULL
                                                    AND vt2.claveMovimiento <> 'VX'))
                                            AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."'";

        $rs = fn_ejecuta_query($sqlGetTractoresDisponibles);

    //}


        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
          $rs['root'][$nInt]['centroDistSesion'] = $_SESSION['usuCompania'];
        }

        echo json_encode($rs);
    }

    function validaOdometro(){
        $sqlOdom="SELECT * from catractorestbl a, camantenimientotractoresdetalletbl b
                where a.idtractor=b.idtractor
            and b.idViajeTractor=".$_REQUEST['idViajeTractor'];
        $rsOdom=fn_ejecuta_query($sqlOdom);

        echo json_encode($rsOdom);
    }

    function validaViajeDiario(){
        if ($_SESSION['idUsuario']=='130' || $_SESSION['idUsuario']=='459' || $_SESSION['idUsuario']=='102' || $_SESSION['idUsuario']=='195') {
            $sqlFechaViaje="SELECT * from catractorestbl a, trviajestractorestbl b, trtalonesviajestbl c
                            where a.idtractor=".$_REQUEST['idTractor']."
                            and a.idtractor=b.idtractor
                            and b.clavemovimiento not in('VP','VX')
                            and c.idviajetractor=b.idviajetractor
                            and c.claveMovimiento!='TX'
                            and DATE(c.fechaEvento) >= NOW()
                            and c.tipotalon='TE'";
            $rsFecha=fn_ejecuta_query($sqlFechaViaje);
            echo json_encode($rsFecha);            
        } else if($_SESSION['idUsuario']=='162' || $_SESSION['idUsuario']=='132' || $_SESSION['idUsuario']=='156') {
            $sqlFechaViaje="SELECT * from catractorestbl a, trviajestractorestbl b, trtalonesviajestbl c
                            where a.idtractor=".$_REQUEST['idTractor']."
                            and a.idtractor=b.idtractor
                            and b.clavemovimiento not in('VP','VX')
                            and c.idviajetractor=b.idviajetractor
                            and c.claveMovimiento not in('TX','TF')
                            and DATE(c.fechaEvento) >= NOW()
                            and c.tipotalon='TE'";
            $rsFecha=fn_ejecuta_query($sqlFechaViaje);
            echo json_encode($rsFecha);            
        }

        else{
            $sqlFechaViaje="SELECT * from catractorestbl a, trviajestractorestbl b, trtalonesviajestbl c
                            where a.idtractor=".$_REQUEST['idTractor']."
                            and c.centrodistribucion='".$_SESSION['usuCompania']."' ".
                            "and a.idtractor=b.idtractor
                            and b.clavemovimiento not in('VP','VX')
                            and c.idviajetractor=b.idviajetractor                            
                            and c.claveMovimiento!='TX'
                            and DATE(c.fechaEvento) >= DATE(NOW())
                            and c.tipotalon='TN'";
            $rsFecha=fn_ejecuta_query($sqlFechaViaje);

            echo json_encode($rsFecha);      
        }

       
        
    }


  /*  function addContinuidadEspecialTalon(){
         $a = array();
        $e = array();
        $a['success'] = true;

        //echo 'uni '.$_REQUEST['trViajesTractoresUnidadesHdn'].'</br>';
        //return;

  
        }
        $vinArr = explode ("|", $_REQUEST['trap446VinHdn']);

        $idTalon=$_REQUEST['trap446IdTalonHdn'];
        /*if(in_array("", $vinArr)){
            $e[] = array('id'=> "trViajesTractoresVinHdn", 'msg'=>getRequerido());
            $a['success'] = false;
        }*/
        //Checar el destino más largo
     /*   $plazaDestino = -1;
        $kmTabulados = 0;
        for ($iInt=0; $iInt < sizeof($kilometrosArr); $iInt++) {
            if ($kmTabulados <= $kilometrosArr[$iInt]) {
                $plazaDestino = $destinoArr[$iInt];
                $kmTabulados = $kilometrosArr[$iInt];
            }
        }

        $totalUnidades = '';
        if($_REQUEST['trViajesTractoresTotalUnidadesHdn'] == 0){
            $totalUnidades =  array_sum($unidadesArr);
        } else {
            $totalUnidades = $_REQUEST['trViajesTractoresTotalUnidadesHdn'];
        }
        //1.- Insertar en trViajesTractoresTbl
       /* if($a['success'] == true){
            if($plazaDestino > 0){

                  $sqlIdViaje="SELECT max(idViajeTractor) as idViajeTractor from trViajesTractoresTbl where idtractor='".$_REQUEST['trViajesTractoresIdTractorHdn']."' AND claveMovimiento!='VX'";
                    $rsV=fn_ejecuta_query($sqlIdViaje);
                    $idViajeInt=$rsV['root'][0]['idViajeTractor'];

                    $sqlSelUlt="SELECT * from trViajesTractoresTbl where idViajeTractor=".$idViajeInt=$rsV['root'][0]['idViajeTractor'];
                    $rsUlt=fn_ejecuta_query($sqlSelUlt);


                    if ($rsUlt['root'][0]['claveMovimiento']=='VF' || $rsUlt['root'][0]['claveMovimiento']=='VC' || $rsUlt['root'][0]['claveMovimiento']=='VE' || $rsUlt['root'][0]['claveMovimiento']=='VU') {
                        $inserta='0';
                        
                    }

                if ($_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDSLP' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CMDAT' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDLAR' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDMTY' || $_REQUEST['trViajesTractoresCentroDistribucionHdn'] == 'CDKAN' || $inserta=='0')
                {

                               $sqlAddViajeTractorStr = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                         "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                                         "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                         "VALUES(".
                                            $_REQUEST['trViajesTractoresIdTractorHdn'].",".
                                            $_REQUEST['trViajesTractoresClaveChoferHdn'].",".
                                            "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                            $plazaDestino.",".
                                            "'".$_SESSION['usuCompania']."',".
                                            "(SELECT IFNULL(MAX(vt.viaje) + 1,1) AS VIAJE FROM trViajesTractoresTbl vt WHERE vt.idTractor =".$_REQUEST['trViajesTractoresIdTractorHdn']."),".
                                            "'".date("Y-m-d H:i:s")."',".
                                            replaceEmptyDec($kmTabulados).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmComprobadosTxt']).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmSinUnidadTxt']).",".
                                            $totalUnidades.",".
                                            //sizeof($unidadesArr)
                                            sizeof(array_unique($distArr,SORT_STRING)).",".
                                            replaceEmptyNull($_REQUEST['trViajesTractoresIdViajePadreHdn']).",".
                                            "'VG',".
                                            "'".$_SESSION['idUsuario']."',".
                                            "'".$_SERVER['REMOTE_ADDR']."')";

                $kilSumados = $rsUlt['root'][0]['kilometrostabulados'] + $kmTabulados;

                $rs="UPDATE trViajesTractoresTbl set kilometrostabulados=".$kilSumados." WHERE idViajeTractor=".$rsUlt['root'][0]['idViajeTractor'];

                fn_ejecuta_query($rs); 


            }
            else
            {
                    
                $sqlAddViajeTractorStr = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                         "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                                         "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                         "VALUES(".
                                            $_REQUEST['trViajesTractoresIdTractorHdn'].",".
                                            $_REQUEST['trViajesTractoresClaveChoferHdn'].",".
                                            "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                                            $plazaDestino.",".
                                            "'".$_SESSION['usuCompania']."',".
                                            "(SELECT IFNULL(MAX(vt.viaje) + 1,1) AS VIAJE FROM trViajesTractoresTbl vt WHERE vt.idTractor =".$_REQUEST['trViajesTractoresIdTractorHdn']."),".
                                            "'".date("Y-m-d H:i:s")."',".
                                            replaceEmptyDec($kmTabulados).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmComprobadosTxt']).",".
                                            replaceEmptyDec($_REQUEST['trViajesTractoresKmSinUnidadTxt']).",".
                                            $totalUnidades.",".
                                            //sizeof($unidadesArr)
                                            sizeof(array_unique($distArr,SORT_STRING)).",".
                                            replaceEmptyNull($_REQUEST['trViajesTractoresIdViajePadreHdn']).",".
                                            "'VG',".
                                            "'".$_SESSION['idUsuario']."',".
                                            "'".$_SERVER['REMOTE_ADDR']."')";

                $rs = fn_ejecuta_query($sqlAddViajeTractorStr);

            }
                if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                    $a['success'] = false;
                    $a['sqlError'] = $_SESSION['error_sql'];
                    $a['sql'] = $sqlAddViajeTractorStr;
                    $a['errorMessage'] = 'No se pud&oacute; Generar el Viaje. </br> Comun&iacute;quese '.
                                         'con el Administrador del Sistema </br>'.$a['sql'];
                    return $a;
                }

                // Obtiene el idViaje de lo que se acaba de insertar
                $idViajeInt = mysql_insert_id();
                $a['elID'] = $idViajeInt;


                if ($idViajeInt=='0') {

                    $sqlIdViaje="SELECT max(idViajeTractor) as idViajeTractor from trViajesTractoresTbl where idtractor='".$_REQUEST['trViajesTractoresIdTractorHdn']."' AND claveMovimiento!='VX'";
                    $rsV=fn_ejecuta_query($sqlIdViaje);
                    $idViajeInt=$rsV['root'][0]['idViajeTractor'];
                    # code...
                }

                //*2.- ACTUALIZAR LA TABLA DE trEsperaChoferesTbl -- Si es CDTOL o CDSAL u otro válido
                $_REQUEST['catGeneralesTablaTxt'] = 'trViajesTractoresTbl';
                $_REQUEST['catGeneralesColumnaTxt'] = 'cdValidos';
                $_REQUEST['catGeneralesValorTxt'] = $_SESSION['usuCompania'];
                $rsCdValido = getGenerales();

                $esCentroValido = ($rsCdValido['records'] == 1); //Variable si es un centro valido

                if($esCentroValido){ //Si es un CentroDistribución Válido actualiza la Lista de Espera
                    $sqlUpdateEsperaChoferesStr = "UPDATE tresperachoferestbl ".
                                                  " SET claveMovimiento = 'VV', ".
                                                  "fechaEvento = NOW() ".
                                                  "WHERE idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'].
                                                  " AND claveChofer = ".$_REQUEST['trViajesTractoresClaveChoferHdn'].
                                                  " AND centroDistribucion = '".$_SESSION['usuCompania']."';";

                    fn_ejecuta_query($sqlUpdateEsperaChoferesStr);
                    if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                        $a['success'] = false;
                        $a['sqlError'] = $_SESSION['error_sql'];
                        $a['sql'] = $sqlUpdateEsperaChoferesStr;
                        $a['errorMessage'] = 'No se pud&oacute; Actualizar la Lista de Espera. </br> Comun&iacute;quese '.
                                             'con el Administrador del Sistema </br>'.$a['sql'];
                        return $a;
                    }
                }
                //3.- INSERTAR EN LA TABLA trTalonesViajesTbl

                for ($i = 0; $i < count($distArr); $i++) {

                    /////////////////
                     $sqlTarifa="SELECT * FROM alUltimoDetalleTbl where vin ='".$vinArr[$i]."'";
                        $rsTarifa=fn_ejecuta_query($sqlTarifa);

                        if ($rsTarifa['root'][0]['idTarifa'] !='5') {
                            $tipoFolio='TR';
                            $patio=$_SESSION['usuCompania'];
                            $compania=$_REQUEST['trViajesTractoresCompaniaHdn'];
                        }else {
                            $tipoFolio='TE';
                            $patio='CDTOL';
                            $compania='TR';
                        }
                        if($_REQUEST['trViajesTractoresIdTractorHdn'] =='256') {
                            $tipoFolio='TV';
                            $patio=$_SESSION['usuCompania'];
                            $compania=$_REQUEST['trViajesTractoresCompaniaHdn'];
                        }*/

                        //////////////////
                 /*       $patio=$_SESSION['usuCompania'];
                        $tipoFolio='TV';
                        $numeroUni=count($vinArr);

                        $sqlTalon="select * from trtalonesviajestbl a, trviajestractorestbl b, catractorestbl c
                                        where a.idtalon=".$idTalon."
                                        and a.idViajeTractor=b.idViajeTractor
                                        and b.idTractor=c.idtractor";
                        $rsTalon=fn_ejecuta_query($sqlTalon);

                   $sqlAddTalonesViajeStr = "INSERT INTO trTalonesViajesTbl (distribuidor,folio,idViajeTractor,companiaRemitente,".
                                                    "idPlazaOrigen,idPlazaDestino,direccionEntrega,centroDistribucion,tipoTalon,".
                                                    "fechaEvento,observaciones,numeroUnidades,importe,seguro,tarifaCobrar,kilometrosCobrar,".
                                                    "impuesto,retencion,claveMovimiento,tipoDocumento,idTalonContinuidad) VALUES('".$rsTalon['root'][0]['distribuidor']."', (select SUM(folio + 1) as folio from trfoliostbl where centroDistribucion='".$patio."' and compania='".$rsTalon['root'][0]['compania']."' and tipoDocumento='".$tipoFolio."'), ".$rsTalon['root'][0]['idViajeTractor'].", '".$rsTalon['root'][0]['companiaRemitente']."', ".
                                             "(SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                                                    "WHERE dc.distribuidorCentro = '".$_SESSION['usuCompania']."'), ".
                                             $rsTalon['root'][0]['idPlazaOrigen'].", ".$rsTalon['root'][0]['idPlazaDestino'].", '".$_SESSION['usuCompania']."', ".
                                             "'TE', now(), '*Hora :".$idTalon."', ".$numeroUni.", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresImporteTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresSeguroTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresTarifaCobrarTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresKmCobrarTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresImpuestoTxt']).", ".
                                             replaceEmptyDec($_REQUEST['trViajesTractoresRetencionTxt']).", 'TV', 'TV',".$idTalon.")";

                        $rs = fn_ejecuta_query($sqlAddTalonesViajeStr);

                    $sqlUpdFolioPreViaje=" UPDATE trFoliosTbl set folio = folio+1 WHERE centroDistribucion='".$patio."' and compania='".$compania."' and tipoDocumento='".$tipoFolio."' ";
                    fn_ejecuta_query($sqlUpdFolioPreViaje);

                //}


                if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                    $a['success'] = false;

                    $a['sqlError'] = mysql_errno()." - ".mysql_error();
                    $a['sql'] = $sqlAddTalonesViajeStr;
                    $a['errorMessage'] = 'No se pud&oacute; Generar los talones. </br> Comun&iacute;quese '.
                                         'con el Administrador del Sistema </br>'.$a['sql'];
                    return $a;
                }
                
                                  

                    $sqlAddUnidadesEmbarcadasStr = "INSERT INTO trUnidadesEmbarcadasTbl ".
                                            "(centroDistribucion, idViajeTractor, vin, fechaEmbarque, claveMovimiento,distribuidor,tipoTraslado, idTalon) VALUES";

                    for($i = 0; $i < count($vinArr); $i++){

                       /* $sqlTarifa="SELECT * FROM alUltimoDetalleTbl where vin ='".$vinArr[$i]."'";
                        $rsTarifa=fn_ejecuta_query($sqlTarifa);



                        if ($rsTarifa['root'][0]['idTarifa'] =='5') {
                            $sqlDist="SELECT * FROM aldestinosespecialestbl a where a.vin ='".$vinArr[$i]."' AND a.idDestinoEspecial=(SELECT max(b.idDestinoEspecial) FROM aldestinosespecialestbl b where a.vin=b.vin)";
                            $reDist=fn_ejecuta_query($sqlDist);

                            $distribuidor=$reDist['root'][0]['distribuidorDestino'];
                        }else{
                            $distribuidor=$rsTarifa['root'][0]['distribuidor'];
                        }

                         if ($rsTarifa['root'][0]['claveMovimiento'] =='CT') {
                            $sqlDist="SELECT * FROM aldestinosespecialestbl a where a.vin ='".$vinArr[$i]."' AND a.idDestinoEspecial=(SELECT max(b.idDestinoEspecial) FROM aldestinosespecialestbl b where a.vin=b.vin)";
                            $reDist=fn_ejecuta_query($sqlDist);

                            $distribuidor=$reDist['root'][0]['distribuidorDestino'];
                        }


                        if ($rsTarifa['root'][0]['idTarifa'] !='5') {
                            $tipoTalon='TN';
                        }else{
                            $tipoTalon='TE';
                        }

                          //$distribuidor=$distArr;
                        //echo json_encode($distArr);

                        $sqlDX="SELECT * FROM  caDistribuidoresCentrosTbl where distribuidorCentro='".$rsTarifa['root'][0]['distribuidor']."'";
                        $rsDX=fn_ejecuta_query($sqlDX);

                         

                        if ($rsDX['root'][0]['tipoDistribuidor'] =='DX') {
                            $tipoTalon='TX';
                            $distribuidor=$distArr[0];
                        }
                        if ($rsDX['root'][0]['tipoDistribuidor'] =='CD') {                            
                            $distribuidor=$distArr[0];
                        }
                                      

                        if($i > 0){
                           $sqlAddUnidadesEmbarcadasStr .= ", ";
                        }*/
                       /* $sqlAddUnidadesEmbarcadasStr .= "('".$_SESSION['usuCompania']."', ".$rsTalon['root'][0]['idViajeTractor'].", ".
                                                        replaceEmptyNull("'".$vinArr[$i]."'").", ".
                                                        "'".date("Y-m-d H:i:s", time() + $i*2)."','".$valorEstatus."','".$rsTalon['root'][0]['distribuidor']."','".$$rsTalon['root'][0]['tipoTalon']."','".$rsTalon['root'][0]['idTalon']."')";
                    //}
                    $rs = fn_ejecuta_query($sqlAddUnidadesEmbarcadasStr);

                    if($_SESSION['error_sql'] || $_SESSION['error_sql'] != ""){
                        $a['success'] = false;
                        $a['sqlError'] = mysql_errno()." - ".mysql_error();
                        $a['sql'] = $sqlAddUnidadesEmbarcadasStr;
                        $a['errorMessage'] = 'No se pud&oacute; Embarcar las Unidades. </br> Comun&iacute;quese '.
                                             'con el Administrador del Sistema </br>'.$a['sql'];
                        return $a;
                    }

                  /*  $selIdTalon="SELECT * FROM trUnidadesEmbarcadasTbl WHERE idViajeTractor='".$idViajeInt."' AND idTalon ='' ";
                    $rsTalon=fn_ejecuta_query($selIdTalon);

                    $selTalon="SELECT * FROM trTalonesViajesTbl WHERE idViajeTractor=".$idViajeInt." AND claveMovimiento='TV' AND distribuidor in (SELECT distribuidorCentro FROM cadistribuidorescentrostbl WHERE tipoDistribuidor='CD')";
                    $rsIdTalon=fn_ejecuta_query($selTalon);

                    $updIdTalon="UPDATE trUnidadesEmbarcadasTbl SET IDTALON=".$rsIdTalon['root'][0]['idTalon']." WHERE idViajeTractor='".$idViajeInt."' AND idTalon ='' AND distribuidor in (SELECT distribuidor FROM caDistribuidoresCentrosTbl WHERE tipodistribuidor='CD')";
                    fn_ejecuta_query($updIdTalon);*/

                
            //}
        //} 
        
      /* 
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }*/



?>