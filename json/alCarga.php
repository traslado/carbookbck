<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");

	switch ($_REQUEST['alCargaActionHdn']) {
		case 'cargaHolds':
			cargaHolds();
			break;
		default:
            echo '';
	}

	function cargaHolds(){
		$a = array();
		$a['success'] = true;

		if($_REQUEST['alHoldsHoldCmb'] == ""){
            $e[] = array('id'=>'alHoldsHoldCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

		if($a['success']){
			$tempFileName = $_FILES['alHoldsCargaFile']['tmp_name']; 
			$originalFileName = $_FILES['alHoldsCargaFile']['name'];

			if($tempFileName != ''){
				$ext = explode ('.', $originalFileName); 
				$ext = $ext [count ($ext) - 1]; 
			 	
				$fileName = str_replace ($ext, '', $originalFileName); 
			 	
			  	$newName = $_SERVER['DOCUMENT_ROOT'].'/Temp/'. $fileName . $ext;
			  
				if (move_uploaded_file($tempFileName, $newName)){
					
					if (file_exists($newName)){

						$holdFile = fopen($newName, 'r');
						if($holdFile){
							$unidadesNuevas = array();
							$errorUnidades = array();
							$message = "";

							while (($vin = fgets($holdFile)) !== false) {
								$vin = rtrim($vin);
								$sqlCheckUnidadesStr = "SELECT h.* FROM alHistoricoUnidadesTbl h ".
														"WHERE h.vin = '".$vin."' ".
														"AND h.fechaEvento = (SELECT MAX(h2.fechaEvento) FROM alHistoricoUnidadesTbl h2 ".
															"WHERE h2.vin = h.vin) ";
								$rs = fn_ejecuta_query($sqlCheckUnidadesStr);
								
								if(sizeof($rs['root']) > 0){
									$data = addHistoricoUnidad($rs['root'][0]['centroDistribucion'],$vin,
																$_REQUEST['alHoldsHoldCmb'], $rs['root'][0]['distribuidor'],
																$rs['root'][0]['idTarifa'], $rs['root'][0]['localizacionUnidad'],
																'','', $_SESSION['usuValor']);

									if($data['success'] == false){
										$a['success'] = false;
										array_push($errorUnidades, $vin);
									}
								} else {
									array_push($unidadesNuevas, $vin);
								}
							}
							fclose($holdFile);

							if(sizeof($unidadesNuevas) > 0){
								$listaVin = "";
								$listaHolds = "";

								$message = "Unidades no existentes:</br></br>";

								foreach ($unidadesNuevas as $unidad) {
									$message .= $unidad."</br>";
									$listaVin .= $unidad."|";
									$listaHolds .= $_REQUEST['alHoldsHoldCmb']."|";
								}

								$data = holdUnidades($listaVin, $listaHolds);

								if($data['success'] == false){
									$message .= "</br></br>".$data['errorMessage']."</br></br>";
									$a['errors'] = $data['errors'];
								}
							}

							if($a['success'] == false){
								$message .= "</br></br>Error al asignar HOLD a:</br></br>";

								foreach ($errorUnidades as $unidad) {
									$message .= "\t".$unidad."</br>";
								}

								$a['errorMessage'] = $message;
							} else {
								$a['successMessage'] = $message;
							}
						} else {
							$a['success'] = false;
							$a['errorMessage'] = "Error al abrir el archivo " . $newName;
						}
					} else {
						$a['success'] = false;
						$a['errorMessage'] = "Error al procesar el archivo " . $newName;
					}
			 	} else { 
					$a['success'] = false;
					$a['errorMessage'] = "No se pudo mover el archivo " . $tempFileName. '</br> a '.$newName;
				}
			}
		}

		$a['successTitle'] = "Estatus";
		echo json_encode($a);
	}
?>