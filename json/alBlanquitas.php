<?php
  session_start();

  require_once("../funciones/generales.php");
  require_once("../funciones/construct.php");
  require_once("../funciones/utilidades.php");

  $a                 = array();
  $a['successTitle'] = "Manejador de Archivos";
  if (isset($_FILES)) {
      if ($_FILES["genBlanquitasFrmArchivoFld"]["error"] > 0) {
          $a['success'] = false;
          $a['message'] = $_FILES["genBlanquitasFrmArchivoFld"]["error"];
      } else {
          $temp_file_name     = $_FILES['genBlanquitasFrmArchivoFld']['tmp_name'];
          $original_file_name = $_FILES['genBlanquitasFrmArchivoFld']['name'];
          // Find file extention
          $ext                = explode('.', $original_file_name);
          $ext                = $ext[count($ext) - 1];
          // Remove the extention from the original file name
          $file_name          = str_replace($ext, '', $original_file_name);
          $new_name           = $_SERVER['DOCUMENT_ROOT'] . '/blanquitas/' . $file_name . $ext;
          if (move_uploaded_file($temp_file_name, $new_name)) {
              if (!file_exists($new_name)) {
                  $a['success']      = false;
                  $a['errorMessage'] = "Error al procesar el archivo " . $new_name;
              } else {
                  // es necesario saber que procedimiento es lo que se requiere para escoger la funcion que es necesaria                
                  if($_REQUEST['genBlanquitasProcesoHdn'] == '1'){
                    $a['success']        = true;
                    $a['successMessage'] = "Archivo Cargado";
                    $a['archivo']        = $file_name . $ext;
                    $a['root']           = leerXLS($new_name);
                  }else{
                    $a['success']        = true;
                    $a['successMessage'] = "Archivo Cargado";
                    $a['archivo']        = $file_name . $ext;
                    $a['root']           = leerCsv($new_name);
                  }
              }
          } else {
              $a['success']      = false;
              $a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name . '</br> a ' . $new_name;
          }
      }
  } else {
      $a['success']      = false;
      $a['errorMessage'] = "Error FILES NOT SET";
  }
  //echo json_encode($a);

  function leerXLS($inputFileName) {

      // archivo txt
      $filas       = file($inputFileName);
      // iniciamos contador y la fila a cero
      $i           = 0;
      $numero_fila = 0;
      // mientras exista una fila
      while ($filas[$i] != NULL) {
          // incremento contador de la fila
          //echo "numero".$filas[$i+1];

          // genero array con por medio del separador "," que es el que tiene el archivo txt
          $sql = explode('|', $filas[$i]);
          // incrementamos contador
          $i++;
          $numero_fila++;

          /*echo "Esto es el distribuidor ".$sql[0]."\n";
          echo "Esto es el vin".$sql[1]."\n";*/

          // ---------------------- duplicado en la temporal

          $sqlGetUndBlq = "SELECT IFNULL(vin,0) as existe,IFNULL((SELECT 1 FROM alblanquitastbl bq WHERE bq.vin = bt.vin),0) as existe2, DATE_FORMAT(fechaFacturacion,'%d') as dia ".
                          "FROM alBlanquitasTmp bt WHERE vin = '" . $sql[1] . "';";
          $rsGetBlq     = fn_ejecuta_query($sqlGetUndBlq);


          if ($rsGetBlq['root'][0]['existe'] == 1 && $rsGetBlq['root'][0]['existe2'] == 0 && $rsGetBlq['root'][0]['existe2'] != substr($sql[4], 4, 2)) {

              $sqlGetVin = "SELECT vin, IFNULL((SELECT DATE_FORMAT(fechaFacturacion,'%m') FROM alBlanquitasTbl blq WHERE blq.vin = bp.vin),1) as mes,DATE_FORMAT(fechaFacturacion,'%d') as dia, fechaFacturacion, ".
                            "(SELECT 1 FROM alblanquitastbl bq WHERE bq.vin = bp.vin AND bq.fechaBaja is not null ) as undBj " .
                            "FROM alBlanquitasTmp bp WHERE vin = '" .$sql[1]. "';";

              $rsGetVin  = fn_ejecuta_query($sqlGetVin);

              // ---------------------- mes y dia de la consulta
              $numMes = $rsGetVin['root'][0]['mes'];
              $numDia = $rsGetVin['root'][0]['dia'];
              $undBj  = $rsGetVin['root'][0]['undBj'];

              // ---------------------- mes y dia del archivo de la unidad
              $undMes = substr($sql[4], 2, 2);
              $undDia = substr($sql[4], 4, 2);

              if ($numDia !== $undDia && $numDia !=='' ) {
                  $fechaFact = '20' . $sql[4];

                  $sqlUpdBlq = "UPDATE alBlanquitasTmp SET fechaBaja = '" . $fechaFact . "', claveMovimiento = 'BJ' WHERE vin = '" . $sql[1] . "';";
                  //fn_ejecuta_query($sqlUpdBlq);

                  if (strlen(ltrim($sql[3], 0)) == 8) {
                      $undSep_01 = substr(ltrim($sql[3], 0), 0, 6);
                      $undSep_02 = substr(ltrim($sql[3], 0), 6, 2);
                      $precioUnd = $undSep_01 . '.' . $undSep_02;
                  } elseif (strlen(ltrim($sql[3], 0)) == 9) {
                      $undSep_01 = substr(ltrim($sql[3], 0), 0, 7);
                      $undSep_02 = substr(ltrim($sql[3], 0), 7, 2);
                      $precioUnd = $undSep_01 . '.' . $undSep_02;
                  } if (strlen(ltrim($sql[5], 0)) == 1) {
                      $undSeguro = $sql[5];
                  } elseif (strlen(ltrim($sql[5], 0)) == 5) {
                      $sep_01    = substr(ltrim($sql[5], 0), 0, 3);
                      $sep_02    = substr(ltrim($sql[5], 0), 3, 2);
                      $undSeguro = $sep_01 . '.' . $sep_02;
                  } elseif (strlen(ltrim($sql[5], 0)) == 6) {
                      $sep_01    = substr(ltrim($sql[5], 0), 0, 4);
                      $sep_02    = substr(ltrim($sql[5], 0), 4, 2);
                      $undSeguro = $sep_01 . '.' . $sep_02;
                  } elseif (strlen(ltrim($sql[5], 0)) == 7) {
                      $sep_01    = substr(ltrim($sql[5], 0), 0, 5);
                      $sep_02    = substr(ltrim($sql[5], 0), 5, 2);
                      $undSeguro = $sep_01 . '.' . $sep_02;
                  }
                  $sep_03      = substr(ltrim($sql[7], 0), 0, 3);
                  $sep_04      = substr(ltrim($sql[7], 0), 3, 2);
                  $undSindical = $sep_03 . '.' . $sep_04;

                  $sqlAddUndBlq = "INSERT INTO alBlanquitasTmp (compania, distribuidorCentro, vin, simboloUnidad, fechaFacturacion, importeSeguro, importeFactura, importeCuota, importeCredito, importeGasto, importeIva, fechaBaja, fechaCancelacion, claveMovimiento) ".
                                  " VALUES(" . "'".$_SESSION['usuCompania']."', " . "'" . $sql[0] . "', " . "'" . $sql[1] . "', " . "'" . $sql[2] . "', " . "'" . $fechaFact . "', " . "'" . $undSeguro . "', " . "'" . $precioUnd . "', " . "'" . $undSindical . "', " . "'00.00', " . "'00.00', " . "'00.00', " . "null," . "null," . "'DL');";

                  fn_ejecuta_query($sqlAddUndBlq);

              }
          }elseif ($rsGetBlq['root'][0]['existe'] == 1 && $rsGetBlq['root'][0]['existe2'] == 0) {
            $fechaFact = '20' . $sql[4];
            $sqlUpdBlq = "UPDATE alBlanquitasTmp SET fechaCancelacion = '" . $fechaFact . "', claveMovimiento = 'CN' WHERE vin = '" . $sql[1] . "';";
            fn_ejecuta_query($sqlUpdBlq);
          }elseif ($rsGetBlq['root'][0]['existe'] == 0 && $rsGetBlq['root'][0]['existe2'] == 0) {

              $sqlDist           = "SELECT 1 as existe FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '" . $sql[0] . "';";
              $rsGenDistribuidor = fn_ejecuta_query($sqlDist);

              if (strlen(ltrim($sql[3], 0)) == 8) {
                  $undSep_01 = substr(ltrim($sql[3], 0), 0, 6);
                  $undSep_02 = substr(ltrim($sql[3], 0), 6, 2);
                  $precioUnd = $undSep_01 . '.' . $undSep_02;
              } elseif (strlen(ltrim($sql[3], 0)) == 9) {
                  $undSep_01 = substr(ltrim($sql[3], 0), 0, 7);
                  $undSep_02 = substr(ltrim($sql[3], 0), 7, 2);
                  $precioUnd = $undSep_01 . '.' . $undSep_02;
              }
              $fechaFact = '20' . $sql[4];
              if (strlen(ltrim($sql[5], 0)) == 1) {
                  $undSeguro = $sql[5];
              } elseif (strlen(ltrim($sql[5], 0)) == 5) {
                  $sep_01    = substr(ltrim($sql[5], 0), 0, 3);
                  $sep_02    = substr(ltrim($sql[5], 0), 3, 2);
                  $undSeguro = $sep_01 . '.' . $sep_02;
              } elseif (strlen(ltrim($sql[5], 0)) == 6) {
                  $sep_01    = substr(ltrim($sql[5], 0), 0, 4);
                  $sep_02    = substr(ltrim($sql[5], 0), 4, 2);
                  $undSeguro = $sep_01 . '.' . $sep_02;
              } elseif (strlen(ltrim($sql[5], 0)) == 7) {
                  $sep_01    = substr(ltrim($sql[5], 0), 0, 5);
                  $sep_02    = substr(ltrim($sql[5], 0), 5, 2);
                  $undSeguro = $sep_01 . '.' . $sep_02;
              }
              $sep_03      = substr(ltrim($sql[7], 0), 0, 3);
              $sep_04      = substr(ltrim($sql[7], 0), 3, 2);
              $undSindical = $sep_03 . '.' . $sep_04;

              $sqlAddUndBlq = "INSERT INTO alBlanquitasTmp (compania, distribuidorCentro, vin, simboloUnidad, fechaFacturacion, importeSeguro, importeFactura, importeCuota, importeCredito, importeGasto, importeIva, fechaBaja, fechaCancelacion, claveMovimiento) VALUES(" . "'".$_SESSION['usuCompania']."', " . "'" . $sql[0] . "', " . "'" . $sql[1] . "', " . "'" . $sql[2] . "', " . "'" . $fechaFact . "', " . "'" . $undSeguro . "', " . "'" . $precioUnd . "', " . "'" . $undSindical . "', " . "'00.00', " . "'00.00', " . "'00.00', " . "null," . "null," . "'DL');";

              fn_ejecuta_query($sqlAddUndBlq);
          }
      }
      //FIN DEL WHILE
      $row = $filas[$i + 1];
  }

  function leerCsv($inputFileName){
    /** Error reporting */
    error_reporting(E_ALL);    
    date_default_timezone_set ("America/Mexico_City");
    
    //  Include PHPExcel_IOFactory
    include '../funciones/Classes/PHPExcel/IOFactory.php';
    
    //$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
    //echo $inputFileName;
    //  Read your Excel workbook
    try {
      $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
      $objReader =
       PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($inputFileName);
    } catch(Exception $e) {

      die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
    }
    
    //  Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet(0); 
    $highestRow = $sheet->getHighestRow(); 
    $highestColumn = $sheet->getHighestColumn();

    //Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
    $rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

    
    //-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

    if (ord(strtoupper($highestColumn)) <= 67 && $highestRow <= 10 ) {
      $root[] = array('descripcion' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
      return $root;
    }

    if (ord(strtoupper($highestColumn)) < 12) {
      $root[] = array('descripcion' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
      return $root;
    }

    if (strlen($rowData[0][0]) < 12) {
      $root[] = array('descripcion' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [vin-Simbolo-fechaFacturacion-200-D-distribuidor-DIS-monto-monto-200-monto-monto]', 'fail'=>'Y');
      return $root;
    }
    //------------------------------------------------------------------------------------------------
    
    //arreglo de valores
          
        $isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
              'Simbolo'=>array('val'=>'','size'=>true,'exist'=>true),
              'Distribuidor' =>array('val'=>'','size'=>true,'exist'=>true),
              'color' =>array('val'=>'','size'=>true,'exist'=>true)
            );    
    
    for($row = 0; $row<sizeof($rowData); $row++){
      //Validar si las unidades estan bien o no
      //Verifico si la longitud de loas campos sea la correcta...
      $isValidArr['vin']['val'] = $rowData[$row][0];
      $isValidArr['vin']['size'] = strlen($rowData[$row][0]) == 17;
      $isValidArr['Simbolo']['val'] = $rowData[$row][1];
      $isValidArr['fechaFacturacion']['val'] = $rowData[$row][2];
      $isValidArr['fechaFacturacion']['val'] = $rowData[$row][2];
      $isValidArr['cancela']['val'] = $rowData[$row][4];
      $isValidArr['Distribuidor']['val'] = $rowData[$row][5];
      $isValidArr['importeFactura']['val'] = $rowData[$row][11];      

      $numDistribuidor = substr($isValidArr['Distribuidor']['val'], 2,5);
      echo "";
      // FORMATo DE FECHA COMO VIENE EN EL ARCHIVO 26/07/2019
      $numAnio = substr($isValidArr['fechaFacturacion']['val'], 6,4);
      $numMes = substr($isValidArr['fechaFacturacion']['val'], 3,2);
      $numDia = substr($isValidArr['fechaFacturacion']['val'], 0,2);
      $numfecha = $numAnio."/".$numMes."/".$numDia;


      $sqlAddUndBlq = "INSERT INTO alBlanquitasTmp (compania, distribuidorCentro, vin, simboloUnidad, fechaFacturacion, importeSeguro, importeFactura, importeCuota, importeCredito, importeGasto, importeIva, fechaBaja, fechaCancelacion, claveMovimiento) VALUES(" . "'".$_SESSION['usuCompania']."', " . 
                                "'" .$numDistribuidor. "', " . 
                                "'" .$isValidArr['vin']['val']. "', " . 
                                "'" .$isValidArr['Simbolo']['val']. "', " . 
                                "'" .$numfecha. "', " . 
                                "'0.00', " . 
                                "'".$isValidArr['importeFactura']['val']."', " . 
                                "'0.00', " . 
                                "'00.00', " . 
                                "'00.00', " . 
                                "'00.00', " . 
                                "null," . 
                                "null," . 
                                "'".$isValidArr['cancela']['val']."');";

      fn_ejecuta_query($sqlAddUndBlq);      


      $errorMsg = '';
      $isTrue = true;

      /*foreach ($isValidArr as $key => $value) {
        if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
          $errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
          $isTrue = false;
        }else {// de lo contrario verifico que exista en la base
          if ($key != 'Avanzada') {            
            $isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
            if (!$isValidArr[$key]['exist']) {
              $siNo = ($key == 'VIN') ? ' ya ' : ' no ';
              $errorMsg .= 'El '.$key.$siNo.'Existe!';
              $isTrue = false; 

            }
          }         
        }
      }*/

      if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
        //Busco el idTarifa

        //Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
        //$_SESSION['usuCompania'] = 'LZC02';
        


      
        $today = date("Y-m-d H:i:s");
        $fecha = substr($today,0,10);
        $hora=substr($today,11,8);
        

        /*$numNomFac = $rowData[$row][3];
        $numDirEnt = $rowData[$row][4];
        $numFped = $rowData[$row][5];*/

        if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
          $errorMsg = 'Agregado Correctamente';
        } else {
          $errorMsg = 'Registro No Agregado';
        }   
      }
      $root[]= array('vin'=>$rowData[$row][0],'simbolo'=>$rowData[$row][1], 'distribuidor'=>$rowData[$row][2],'buque'=>$rowData[$row][3],'fechaArribo'=>$rowData[$row][4],'peso'=>$rowData[$row][5],'Origen'=>$rowData[$row][6],'Destino Final'=>$rowData[$row][7],'Madrina o Furgon'=>$rowData[$row][8],'Demand Area'=>$rowData[$row][9],'descripcion'=>$errorMsg);      

      //echo json_encode($rowData[$row]);
    }
    return $root;     
  }

  $sqlGetBlqTmp = "SELECT blq.distribuidorCentro,blq.vin,blq.simboloUnidad,blq.importeFactura,blq.fechaFacturacion,blq.importeSeguro,blq.importeCuota, ".
                  "concat(IFNULL((SELECT 'Dist. Correcto,' FROM cadistribuidorescentrostbl di WHERE di.distribuidorCentro =blq.distribuidorCentro),concat(blq.distribuidorCentro,'Dist. no existe,')), ".
                  "IFNULL((SELECT 'Simbolo Correcto,' FROM casimbolosunidadestbl su WHERE su.simboloUnidad = blq.simboloUnidad), 'Simbolo no existe,'), ".
                  "IF(LENGTH(blq.vin) = 17,'VIN correcto,','vin incorrecto,' )) as nose ".
                  "FROM alblanquitastmp blq; ";

  $rsSqlBlqTmp = fn_ejecuta_query($sqlGetBlqTmp);

  echo json_encode($rsSqlBlqTmp);  
?>
