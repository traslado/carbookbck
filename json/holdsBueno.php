<?php

	require_once("../funciones/generales.php");
	//require_once("../funciones/construct.php");
   // require_once("../funciones/utilidades.php");	
	
	switch($_REQUEST['alCmdHoldHdn'])
	{
		case 'addHoldUnidad':
			addHoldUnidad();
			break;
		case 'getSalida':
			getSalida();
			break;
		case 'updTipoSalida':
			updTipoSalida();	
			break;
		case 'updBajaUnds':
			updBajaUnds();	
			break;
		case 'addLiberarUnidad':
			addLiberarUnidad();	
			break;
		default:		
			echo '';			
		break;
	}

	$vin1 = $_REQUEST['vines'];
	$cMovimiento = $_REQUEST['claveMovimiento'];

	function addHoldUnidad(){
		$vin1 = $_REQUEST['vines'];
		$cMovimiento = $_REQUEST['claveMovimiento'];

		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);

		$cadena = chunk_split($vin, 17,",");
		//$cadena_01[] = chunk_split($vin, 17,",");

		$vines = substr($cadena,0,-1);

		$numeroCiclos = explode(",", $vines);




		for ($i=0; $i < count($numeroCiclos); $i++) {
		//echo "numeroUnidad:".$numeroCiclos[$i];	


			$sqlGetValida = "SELECT hu.vin, hu.claveMovimiento,
							(SELECT count(hu1.claveMovimiento) FROM cageneralestbl ge, alhistoricounidadestbl hu1 
							WHERE hu1.vin = hu.vin 
							AND ge.valor = hu1.claveMovimiento 
							AND hu1.claveMovimiento = '".$cMovimiento."'
							AND ge.tabla = 'alHoldsUnidadesTbl' 
							AND ge.columna = 'CDTOL' ) as numHolds,
							(SELECT count(hu1.claveMovimiento) FROM cageneralestbl ge, alhistoricounidadestbl hu1 
							WHERE hu1.vin = hu.vin 
							AND ge.estatus = hu1.claveMovimiento
							AND ge.estatus = (SELECT ge2.estatus FROM cageneralestbl ge2 WHERE ge2.valor = hu.claveMovimiento AND ge2.tabla = 'alHoldsUnidadesTbl' AND ge2.columna = 'CDTOL' AND ge2.valor = 'GE') 
							AND ge.tabla = 'alHoldsUnidadesTbl' 
							AND ge.columna = 'CDTOL') as numLiberado,
							(SELECT ge2.estatus FROM cageneralestbl ge2 WHERE ge2.valor = hu.claveMovimiento AND ge2.tabla = 'alHoldsUnidadesTbl' AND ge2.columna = 'CDTOL') as liberado
														FROM alhistoricounidadestbl hu
														WHERE hu.claveMovimiento  IN (SELECT ge.valor FROM cageneralestbl ge WHERE ge.tabla = 'alHoldsUnidadesTbl' AND ge.columna = 'CDTOL')
														AND hu.claveMovimiento = '".$cMovimiento."'
														AND hu.vin = '".$numeroCiclos[$i]."'
														group by hu.vin, hu.claveMovimiento;   ";   

			
			$rs_01 = fn_ejecuta_query($sqlGetValida);

			//echo $rs_01['root'][0]['vin'];


				$rsSqlGetUnds = "SELECT ud.vin,ud.claveMovimiento, ".
								"concat(claveMovimiento,' - ',(SELECT ge.nombre FROM cageneralestbl ge WHERE ge.tabla = 'alHistoricoUnidadesTbl' AND ge.columna = 'claveMovimiento' AND ge.valor = ud.claveMovimiento)) AS descripcion ".
								"FROM alultimodetalletbl ud ".
								"WHERE ud.centroDistribucion = 'CMDAT' ".						
								"AND vin IN('".$numeroCiclos[$i]."')";

				$rsGetUnds = fn_ejecuta_query($rsSqlGetUnds);	

			

			if($rs_01['root'][0]['vin'] == null){				

				echo "si entro:".$rsGetUnds['root'][$i]['claveMovimiento'];			

				if($rsGetUnds['root'][0]['claveMovimiento'] == 'IC' || $rsGetUnds['root'][0]['claveMovimiento'] == 'DA' || $rsGetUnds['root'][0]['claveMovimiento'] == 'DI' ){					
						$sqlAddHu = "INSERT INTO alhistoricounidadestbl ". 
									"(centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
									"SELECT centroDistribucion,vin,NOW(),'".$cMovimiento."' as claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,'".$_REQUEST['observacion']."' AS observaciones,usuario,ip ".
									"FROM alultimodetalletbl ".
									"WHERE vin in('".$numeroCiclos[$i]."') ";

						$rsSqlAddHu = fn_ejecuta_query($sqlAddHu);			
					}else{
						//echo json_encode("No se inserto esta Unidad:".$rsGetUnds['root'][$i]['vin']);

						$fileUnds="E:/carbook/i728/log/log_".date('ymdHis').".txt";
				    	//$fileDir = '../generacion_550/'. $keys[$i] .'.txt';
					    $fileLogUnds = fopen($fileUnds, 'a+');

					    fwrite($fileLogUnds,$rsGetUnds['root'][$i]['vin'].",".$rsGetUnds['root'][$i]['descripcion'].PHP_EOL); 
					}

					fclose($fileLogUnds); 
					//genArchivoRAC500();	

			}else if(sizeof($rs_01['root'][0]) > 0 ){
				if($rs_01['root'][0]['numHolds'] == $rs_01['root'][0]['numLiberado'] ){
					echo "se vuelve a insertar";

					if($rsGetUnds['root'][0]['claveMovimiento'] == 'IC' || $rsGetUnds['root'][0]['claveMovimiento'] == 'DA' || $rsGetUnds['root'][0]['claveMovimiento'] == 'DI' ){					
						$sqlAddHu = "INSERT INTO alhistoricounidadestbl ". 
									"(centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
									"SELECT centroDistribucion,vin,NOW(),'".$cMovimiento."' as claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,'".$_REQUEST['observacion']."' AS observaciones,usuario,ip ".
									"FROM alultimodetalletbl ".
									"WHERE vin in('".$numeroCiclos[$i]."') ";

						$rsSqlAddHu = fn_ejecuta_query($sqlAddHu);			
					}else{
						//echo json_encode("No se inserto esta Unidad:".$rsGetUnds['root'][$i]['vin']);

						$fileUnds="E:/carbook/i728/log/log_".date('ymdHis').".txt";
				    	//$fileDir = '../generacion_550/'. $keys[$i] .'.txt';
					    $fileLogUnds = fopen($fileUnds, 'a+');

					    fwrite($fileLogUnds,$rsGetUnds['root'][$i]['vin'].",".$rsGetUnds['root'][$i]['descripcion'].PHP_EOL); 
					}

					fclose($fileLogUnds); 
					genArchivoRAC500();	

										
				}else{
					echo "duplicado";
				}
			}
		}					
	}

	function addLiberarUnidad(){
		$vin1 = $_REQUEST['vines'];
		$cMovimiento = $_REQUEST['claveMovimiento'];

		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);

		$cadena = chunk_split($vin, 17,",");
		//$cadena_01[] = chunk_split($vin, 17,",");

		$vines = substr($cadena,0,-1);

		$numeroCiclos = explode(",", $vines);




		for ($i=0; $i < count($numeroCiclos); $i++) {			
		//echo "numeroUnidad:".$numeroCiclos[$i]; 	

			$sqlGenHold = "SELECT ge.valor 
					FROM cageneralestbl ge 
					WHERE ge.tabla = 'alHoldsUnidadesTbl' 
					AND ge.columna = 'CDTOL'
					AND ge.estatus = '".$cMovimiento."';";
						
			$rsHold = fn_ejecuta_query($sqlGenHold); 						

			$sqlGetValida = "SELECT hu.vin, hu.claveMovimiento,
							(SELECT count(hu1.claveMovimiento) FROM cageneralestbl ge, alhistoricounidadestbl hu1 
							WHERE hu1.vin = hu.vin 
							AND ge.valor = hu1.claveMovimiento 
							AND hu1.claveMovimiento = '".$rsHold['root'][0]['valor']."'
							AND ge.tabla = 'alHoldsUnidadesTbl' 
							AND ge.columna = 'CDTOL' ) as numHolds,
							(SELECT count(hu1.claveMovimiento) FROM cageneralestbl ge, alhistoricounidadestbl hu1 
							WHERE hu1.vin = hu.vin 
							AND ge.estatus = hu1.claveMovimiento
							AND ge.estatus = '".$cMovimiento."'
							AND ge.tabla = 'alHoldsUnidadesTbl' 
							AND ge.columna = 'CDTOL') as numLiberado,
							(SELECT ge2.estatus FROM cageneralestbl ge2 WHERE ge2.valor = hu.claveMovimiento AND ge2.tabla = 'alHoldsUnidadesTbl' AND ge2.columna = 'CDTOL') as liberado
														FROM alhistoricounidadestbl hu
														WHERE hu.claveMovimiento  IN (SELECT ge.valor FROM cageneralestbl ge WHERE ge.tabla = 'alHoldsUnidadesTbl' AND ge.columna = 'CDTOL')
														AND hu.claveMovimiento = '".$rsHold['root'][0]['valor']."'
														AND hu.vin = '".$numeroCiclos[$i]."'
														group by hu.vin, hu.claveMovimiento;";   

			
			$rs_01 = fn_ejecuta_query($sqlGetValida);

			//echo $rs_01['root'][0]['vin'];


				$rsSqlGetUnds = "SELECT ud.vin,ud.claveMovimiento, ".
								"concat(claveMovimiento,' - ',(SELECT ge.nombre FROM cageneralestbl ge WHERE ge.tabla = 'alHistoricoUnidadesTbl' AND ge.columna = 'claveMovimiento' AND ge.valor = ud.claveMovimiento)) AS descripcion ".
								"FROM alultimodetalletbl ud ".
								"WHERE ud.centroDistribucion = 'CMDAT' ".						
								"AND vin IN('".$numeroCiclos[$i]."')";

				$rsGetUnds = fn_ejecuta_query($rsSqlGetUnds);	

			

			if($rs_01['root'][0]['vin'] != null){				

				echo "si entro:".$rsGetUnds['root'][$i]['claveMovimiento'];			

				if($rsGetUnds['root'][0]['claveMovimiento'] == 'IC' || $rsGetUnds['root'][0]['claveMovimiento'] == 'DA' || $rsGetUnds['root'][0]['claveMovimiento'] == 'DI' ){
					if($rs_01['root'][0]['liberado'] == $cMovimiento){
						if($rs_01['root'][0]['numHolds'] > $rs_01['root'][0]['numLiberado'] ){
							$sqlAddHu = "INSERT INTO alhistoricounidadestbl ". 
										"(centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
										"SELECT centroDistribucion,vin,NOW(),'".$cMovimiento."' as claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,'".$_REQUEST['observacion']."' AS observaciones,usuario,ip ".
										"FROM alultimodetalletbl ".
										"WHERE vin in('".$numeroCiclos[$i]."') ";

							$rsSqlAddHu = fn_ejecuta_query($sqlAddHu);
						}		
					}						
				}else{
					//echo json_encode("No se inserto esta Unidad:".$rsGetUnds['root'][$i]['vin']);

					$fileUnds="E:/carbook/i728/log/log_".date('ymdHis').".txt";
			    	//$fileDir = '../generacion_550/'. $keys[$i] .'.txt';
				    $fileLogUnds = fopen($fileUnds, 'a+');

				    fwrite($fileLogUnds,$rsGetUnds['root'][$i]['vin'].",".$rsGetUnds['root'][$i]['descripcion'].PHP_EOL); 
				}

				fclose($fileLogUnds); 
				genArchivoRAC500();	

			}/*else if(sizeof($rs_01['root'][0]) > 0 ){
				if($rs_01['root'][0]['numHolds'] == $rs_01['root'][0]['numLiberado'] ){
					echo "se vuelve a insertar";

					if($rsGetUnds['root'][0]['claveMovimiento'] == 'IC' || $rsGetUnds['root'][0]['claveMovimiento'] == 'DA' || $rsGetUnds['root'][0]['claveMovimiento'] == 'DI' ){					
						$sqlAddHu = "INSERT INTO alhistoricounidadestbl ". 
									"(centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
									"SELECT centroDistribucion,vin,NOW(),'".$cMovimiento."' as claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,'".$_REQUEST['observacion']."' AS observaciones,usuario,ip ".
									"FROM alultimodetalletbl ".
									"WHERE vin in('".$numeroCiclos[$i]."') ";

						$rsSqlAddHu = fn_ejecuta_query($sqlAddHu);			
					}else{
						//echo json_encode("No se inserto esta Unidad:".$rsGetUnds['root'][$i]['vin']);

						$fileUnds="E:/carbook/i728/log/log_".date('ymdHis').".txt";
				    	//$fileDir = '../generacion_550/'. $keys[$i] .'.txt';
					    $fileLogUnds = fopen($fileUnds, 'a+');

					    fwrite($fileLogUnds,$rsGetUnds['root'][$i]['vin'].",".$rsGetUnds['root'][$i]['descripcion'].PHP_EOL); 
					}

					fclose($fileLogUnds); 
					//genArchivoRAC500();	

										
				}else{
					echo "duplicado";
				}
			}*/
		}					
	}	

	function genArchivoRAC500(){

		$vin1 = $_REQUEST['vines'];
		$cMovimiento = $_REQUEST['claveMovimiento'];

		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$vin1);

		$cadena = chunk_split($vin, 17,"','");
		$vines = substr($cadena,0,-2);

		$sqlGetUnds = "SELECT DISTINCT hu.vin,hu.claveMovimiento, hu.fechaEvento ".
						"FROM alhistoricounidadestbl hu ".
						"WHERE hu.claveMovimiento = '".$_REQUEST['claveMovimiento']."' ".
						"AND hu.claveMovimiento NOT IN('SV','ST','SP','SX','EL','SI','PB','SE') ".
						"AND hu.fechaEvento = (SELECT MAX(hu1.fechaEvento) FROM alhistoricounidadestbl hu1 WHERE HU1.VIN AND hu1.claveMovimiento = hu.claveMovimiento ) ".
						"AND hu.centroDistribucion = 'CMDAT' ".
						"AND hu.vin IN ('".$vines.") ";

		$rsSqlGetUnds = fn_ejecuta_query($sqlGetUnds);

		$numTotal =sizeof($rsSqlGetUnds['root']); 

	    for ($i=0;$i < sizeof($rsSqlGetUnds['root']);$i++) {

			$numVin = $rsSqlGetUnds['root'][$i]['vin'];
			$cMovimiento = $rsSqlGetUnds['root'][$i]['claveMovimiento'];
			$fMovimiento = $rsSqlGetUnds['root'][$i]['fechaEvento'];

   			$fileDir="E:/carbook/i728/RAC550.FAL";
	    	//$fileDir = '../generacion_550/'. $keys[$i] .'.FAL';
		    $fileLog = fopen($fileDir, 'a+');
			
			$segmento = 1;
			$countUnds = 1;
			$countSegSc = 1;
			
			
			fwrite($fileLog,'ISA*03*RA550    *00*          *ZZ*XTRA           *ZZ*ADMISDCC'.PHP_EOL); 
			fwrite($fileLog,'*'.date("ymd*Hi").'1304*U*00200*'.str_pad(count($numTotal),9,"0",STR_PAD_LEFT).'*0*P>'.PHP_EOL);
		    fwrite($fileLog,'GS*VI*XTRA*VISTA*'.date("ymd*Hi").'*00001*T*1'.PHP_EOL);

		    // Por Unidad ...
		    for ($e=0; $e < sizeof($rsSqlGetUnds['root']); $e++) { 
		    	fwrite($fileLog,'ST*550*000010001'.PHP_EOL);
		    	fwrite($fileLog, 'BV5*E*XTRA*000000000*'.str_pad($numTotal,3,"0",STR_PAD_LEFT).'*'.$cMovimiento.'*'.date("ymd***Hi",strtotime($fMovimiento[$i])).PHP_EOL);
				fwrite($fileLog,'VI*'.$numVin.'*'.substr($numVin, 10,1).'*M1*****'.PHP_EOL);
				fwrite($fileLog,'SE*'.str_pad(count($numTotal),6,"0",STR_PAD_LEFT).'*00001'.str_pad($numTotal,4,"0",STR_PAD_LEFT).PHP_EOL);
		    	$e ++;
		    }
						 
			$segmento ++;	
		}	
		
		fwrite($fileLog,'GE*'.str_pad(count($numTotal),6,"0",STR_PAD_LEFT).'*'.str_pad(count($numTotal),6,"0",STR_PAD_LEFT).PHP_EOL);
		fwrite($fileLog,'IEA*'.str_pad(count($numTotal),2,"0",STR_PAD_LEFT).'*'.str_pad(count($numTotal),9,"0",STR_PAD_LEFT).PHP_EOL);
 		
 		fclose($fileLog);
 		//subirFtp();
			
	}

	function subirFtp(){
		if(file_exists("E:/carbook/i728/RAC550.FAL")){
			# Definimos las variables
			$host="ftp.iclfca.com";
			$port=21;
			$user="IG";
			$password="dBJY76ig";
			$ruta="/ig/SC/";
			$file = "E:/carbook/i728/RAC550.FAL";//tobe uploaded 
			$remote_file = "RAC550.FAL"; 
			$nuevo_fichero = "E:/carbook/i728/respComodato/RAC550_".date('ymdHis').".FAL";
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}
		/*if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}*/

		if(file_exists("E:/carbook/i728/RAC550.FAL")){
			# Definimos las variables
			$host="ftp.transentric.com";
			$port=21;
			$user="xtraftp";
			$password="xtraftp";
			$ruta="/";
			$file = "E:/carbook/i728/RAC550.FAL";//tobe uploaded 
			$remote_file = "RAC550.FAL"; 
			$nuevo_fichero = "E:/carbook/i728/respComodato/RAC550_".date('ymdHis').".FAL";
					 
			# Realizamos la conexion con el servidor
			$conn_id=@ftp_connect($host);//,$port);
			if($conn_id){
				# Realizamos el login con nuestro usuario y contraseña
				if(@ftp_login($conn_id,$user,$password)){
					# Canviamos al directorio especificado
					if(@ftp_chdir($conn_id,$ruta)){
						# Subimos el fichero
						if(@ftp_put($conn_id,$remote_file,$file,FTP_BINARY)){
							echo json_encode(array('succes'=>true,'msjResponse'=>"Archivo generado y subido Correctamente"));
						}else{
							echo "No ha sido posible subir el fichero";
						}	
					}else
						echo "No existe el directorio especificado";
				}else
					echo "El usuario o la contraseña son incorrectos";
				# Cerramos la conexion ftp
				ftp_close($conn_id);
			}else
				echo "No ha sido posible conectar con el servidor";
		}else{
			echo "no existe el archivo";
		}if(!copy($file, $nuevo_fichero)){
			echo "Error al copiar $fichero...\n";
		}else{
			unlink($file);
			echo "si se copio el archivo";
		}				
	}

    function getSalida(){
    
        $sqlGetMercado = "SELECT au.vin, concat(au.simboloUnidad,' - ',tu.descripcion) as simUnidad, concat(hu.distribuidor ,' - ',di.descripcionCentro) as distUnidad, ".
                        "concat(tm.tarifa,' - ',tm.descripcion) as tipoMercado,hu.claveMovimiento, CONCAT(hu.claveMovimiento,' - ',ge.nombre) as cveMovimeinto  ".
                            "FROM alunidadestbl au, alultimodetalletbl hu, casimbolosunidadestbl tu, cadistribuidorescentrostbl di, catarifastbl tm, cageneralestbl ge ".
                            "WHERE au.vin = hu.vin ".
                            "AND hu.idTarifa = tm.idTarifa ".
                            "AND hu.distribuidor = di.distribuidorCentro ".
                            "AND au.simboloUnidad = tu.simboloUnidad ".
                            "AND hu.claveMovimiento = ge.valor ".
                            "AND au.vin = '".$_REQUEST['numUnidades']."' ";

        $rsSqlGetMercado = fn_ejecuta_query($sqlGetMercado);

        echo json_encode($rsSqlGetMercado);                 

    }

    function updTipoSalida(){
    	echo "si entro";

        $updHuMercado = "UPDATE alhistoricounidadestbl ".
                        "SET claveMovimiento = '".$_REQUEST['tSalida']."', ".
                        "observaciones = 'LIBERADA DE HOLD' ".
                        "WHERE vin = '".$_REQUEST['numUnidades']."' ".
                        "AND claveMovimiento != 'IC' ";

        $rsupdHuMercado = fn_ejecuta_query($updHuMercado);
        
        $updUdMercado = "UPDATE alultimodetalletbl ".
				        "SET claveMovimiento = '".$_REQUEST['tSalida']."' ".
				        "WHERE vin = '".$_REQUEST['numUnidades']."' ";

        $rsupdUdMercado = fn_ejecuta_query($updUdMercado);

        echo json_encode($rsupdUdMercado);
        
    }

    function updBajaUnds(){    	

	    $updHuMercado = "UPDATE alhistoricounidadestbl ".
	                    "SET claveMovimiento = 'UC', ".
	                    "centroDistribucion = 'CDTSA', ".
	                    "observaciones= 'bajaUnidad CMDAT, ".date('ymd')."' ".
	                    "WHERE vin = '".$_REQUEST['numUnidades']."' ";

	    $rsupdHuMercado = fn_ejecuta_query($updHuMercado);
	    
	    $updUdMercado = "UPDATE alultimodetalletbl ".
	                    "SET claveMovimiento = 'UC', ".
	                    "centroDistribucion = 'CDTSA', ".
	                    "observaciones= 'bajaUnidad CMDAT, ".date('ymd')."' ".
	                    "WHERE vin = '".$_REQUEST['numUnidades']."' ";

	    $rsupdUdMercado = fn_ejecuta_query($updUdMercado);

	    $updLocUnd = "UPDATE allocalizacionpatiostbl ".
	                    "SET vin = null, ".
	                    "estatus = 'DI' ".
	                    "WHERE vin = '".$_REQUEST['numUnidades']."' ".
	                    "AND patio = 'CMDAT' ";

	    $rsUpdLocUnd = fn_ejecuta_query($updLocUnd);

	    echo json_encode($rsupdUdMercado);

	    $dltActividad = "DELETE FROM alactividadespmptbl ".
						"WHERE vin = '".$_REQUEST['numUnidades']."' ".
						"AND claveEstatus = 'PE';";

		$rsDltActividad = fn_ejecuta_query($dltActividad);
    }

?>	