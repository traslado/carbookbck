	<?php

		session_start();
		require_once("../funciones/generales.php");
		require_once("../funciones/construct.php");
		require_once("../funciones/utilidades.php");

		switch($_REQUEST['alGlobalDamageHdn']){
			case 'selectGridDamage':
				selectGridDamage();
				break;    
			case 'comboTipoDano':
				comboTipoDano();
				break;  
			case 'comboRequiereParte':
				comboRequiereParte();
				break;
			case 'comboResponsableDano':
				comboResponsableDano();
				break;   
			case 'comboConceptoReparacion':
				comboConceptoReparacion();
				break;       
		    default: 
		     echo '';
		}

     function selectGridDamage(){

			$sqlGetUnidad = "SELECT al.idDanoUnidad,al.idDano,al.vin,cm.descripcion as modelo ,cs.tipoUnidad,CONCAT(cd.areaDano,'-',cd.tipoDano,'-',cd.severidadDano) as dano,cd.severidadDano,al.observacion as descripcionDano
FROM aldanosUnidadesTbl al , alunidadestbl au, casimbolosunidadestbl cs, caclasificacionmarcatbl cm, cadanostbl cd
WHERE al.centroDistribucion='QRO02'
AND al.idDano=cd.idDano
AND al.vin=au.vin
AND au.simboloUnidad= cs.simboloUnidad
AND cs.clasificacion= cm.clasificacion
AND cs.marca=cm.marca
AND al.cveStatus='SR' 
AND al.idDanoUnidad not in (select idDanoUnidad from alGlobalDamageTbl) ";

			$rsSqlGetUnidad= fn_ejecuta_query($sqlGetUnidad);

echo json_encode($rsSqlGetUnidad);

}

     function comboTipoDano(){

			$sqlGetTipoDano = "SELECT valor,nombre from cageneralestbl where tabla='alglobaldamagetbl' and columna='tipoDano' ";

			$rssqlGetTipoDano= fn_ejecuta_query($sqlGetTipoDano);

echo json_encode($rssqlGetTipoDano);

}

     function comboRequiereParte(){

			$sqlGetRequiereParte = "SELECT valor,nombre from cageneralestbl where tabla='alglobaldamagetbl' and columna='requierePieza' ";

			$rssqlGetRequiereParte= fn_ejecuta_query($sqlGetRequiereParte);

echo json_encode($rssqlGetRequiereParte);

}

     function comboResponsableDano(){


     	if ($_REQUEST["tipoCompania"] === "T")
		{
			$sqlcargaCompaniasTransporte = "SELECT CONCAT(compania,'-',descripcion) AS descCompania , compania from cacompaniastbl where tipoCompania='G' AND compania in ('ATR','CPV','GM','JCC','KCSM','TLE','TYT') ";

			$rssqlcargaCompaniasTransporte= fn_ejecuta_query($sqlcargaCompaniasTransporte);

			echo json_encode($rssqlcargaCompaniasTransporte);
		}
     	if ($_REQUEST["tipoCompania"] === "P")
		{

			$rssqlcargaCompaniasPatio = "SELECT CONCAT(compania,'-',descripcion) AS descCompania , compania from cacompaniastbl where tipoCompania='G' AND compania in ('GM','CSI','TRA') ";

			$rsrssqlcargaCompaniasPatio= fn_ejecuta_query($rssqlcargaCompaniasPatio);

			echo json_encode($rsrssqlcargaCompaniasPatio);
		}

}



     function comboConceptoReparacion(){



			$sqlconceptoReparacion = "SELECT claveConcepto,descripcionPO from caconeptosreparacionestbl where tipoAuto='".$_REQUEST["tipoAuto"]."' and severidad='".$_REQUEST["severidadDano"]."'
UNION
select * from caconeptosreparacionestbl where tipoAuto='T' ";

			$rssqlconceptoReparacion= fn_ejecuta_query($sqlconceptoReparacion);

			echo json_encode($rssqlconceptoReparacion);


}

?>