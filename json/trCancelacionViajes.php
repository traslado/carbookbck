<?php

  /************************************************************************
  * Autor: Carlos Alberto Sierra Mayoral
  * Fecha: 07-Octubre-2014
  * Tablas afectadas: trViajesTractoresTbl, trTalonesViajesTbl, trFoliosTbl
  * Descripción: Programa para dar mantenimiento a viajes Foraneos
  *************************************************************************/
    session_start();
      
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("catGenerales.php");

    $_REQUEST = trasformUppercase($_REQUEST);
  
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['trCancelacionViajesActionHdn']){
        case 'getCancelacionPreviaje':
            echo json_encode(getCancelacionPreviaje());
            break;
        case 'getCancelacionAsignacion':
            getCancelacionAsignacion();    
            break;
        case 'getCancelacionGastosViaje':
            echo json_encode(getCancelacionGastosViaje());
            break;
        case 'getCancelacionGastosFolio':
            getCancelacionGastosFolio();
            break;
        default:
            echo '';
    }

    function getCancelacionGastosViaje(){
        $a = array();
        $e = array();
        $a['success'] = true;
        $a['successMsg'] = 'Cancelaci&oacuten efectuada.'; 


        if($_REQUEST['trCancelacionViajesIdViajeHdn'] == ""){
            $e[] = array('id'=>'trCancelacionViajesIdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['trViajesTractoresIdTractorHdn'] == ""){
            $e[] = array('id'=>'trCancelacionGastosTractorViajesCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            //1.- Actualiza Viajes Tractores

            if($_REQUEST['trCancelacionViajesCveMovHdn'] == 'VA'){ // Si tiene Viaje de Acompañante
                
                $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl SET claveMovimiento = 'VX' ".
                                "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";

                $rs = fn_ejecuta_query($sqlUpdViajeStr);                
                
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
									$a['success'] = false;
									$a['sql'] = $sqlUpdViajeStr;
									$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del viaje.";
								} 
								
								if($a['success'])
								{
		                $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl vt3 SET vt3.claveMovimiento = 'VX' ".
		                                    "WHERE vt3.idViajeTractor = (SELECT tb.idViajePadre FROM ".
		                                        "(SELECT vt.idViajePadre FROM trViajesTractoresTbl vt ".
		                                    "WHERE vt.idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."') as tb)";
		
		                $rs = fn_ejecuta_query($sqlUpdViajeStr);		
		                
		                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
											$a['success'] = false;
											$a['sql'] = $sqlUpdViajeStr;
											$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del viaje.";
										}		                								
								}
            }
            else if($_REQUEST['trCancelacionViajesCveMovHdn'] == 'VC'){ // Si tiene Viaje Vacio

                $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl SET claveMovimiento = 'VX' ".
                                	"WHERE idViajeTractor = ".$_REQUEST['trCancelacionViajesIdViajeHdn'];
                //echo "$sqlUpdViajeStr<br>";
                $rs = fn_ejecuta_query($sqlUpdViajeStr);

                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
									$a['success'] = false;
									$a['sql'] = $sqlUpdViajeStr;
									$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del viaje.";
								} 
								
								if($a['success'])
								{
		                $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl vt3 SET vt3.claveMovimiento = 'VX' ".
		                                    "WHERE vt3.idViajeTractor = (SELECT tb.idViajePadre FROM ".
		                                    "(SELECT vt.idViajePadre FROM trViajesTractoresTbl vt ".
		                                    "WHERE vt.idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."') as tb)";
										//echo "$sqlUpdViajeStr<br>";
		                $rs = fn_ejecuta_query($sqlUpdViajeStr);	
		                
		                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
											$a['success'] = false;
											$a['sql'] = $sqlUpdViajeStr;
											$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del viaje.";
										}
								}
            } else {
                // Viaje con Gastos Normales                
                $sqlUpdViajeStr =   "UPDATE trViajesTractoresTbl ".
                                    "SET claveMovimiento = 'VV' ".
                                    "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
								//echo "$sqlUpdViajeStr<br>";
                $rs = fn_ejecuta_query($sqlUpdViajeStr);
                
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
									$a['success'] = false;
									$a['sql'] = $sqlUpdViajeStr;
									$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del viaje.";
								}                

								if($a['success'])
								{
		                //se hace este update por si en los gastos se habian modificado los kilometros los regenere a los que le debe de tocar a ese viaje originalmente
		
		                $sqlKilometrosOriginales= "SELECT kp.kilometros from cakilometrosplazatbl kp, trviajestractorestbl vt ".
		                                          " where kp.idplazaOrigen=vt.idplazaOrigen and kp.idplazaDestino=vt.idplazaDestino ".
		                                          "and vt.idViajeTractor='".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
		                $rssqlKilometrosOriginales=fn_ejecuta_query($sqlKilometrosOriginales);
		
		                $sqlUpdKilometros="UPDATE trviajestractorestbl ".
		                                  "set kilometrosTabulados = '".$rssqlKilometrosOriginales['root'][0]['kilometros']."' ".
		                                   "where idViajeTractor='".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
	
		                fn_ejecuta_query($sqlUpdKilometros);	
		                
		                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
											$a['success'] = false;
											$a['sql'] = $sqlUpdKilometros;
											$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del kilometraje del viaje.";
										}		                									
								}
            }

            /*
            if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == '')){
                $a['sql'] = $sqlUpdViajeStr;
                $a['successMsg'] = 'Se ha actualizado correctamente el viaje a VV-PREVIAJE |'; 
            } else{
                $a['errorMessage'] = 'No se pudo regresar el Viaje a PRE-VIAJE </br>'.$_SESSION['error_sql'].$sqlUpdViajeStr;
                $a['success'] = false;
                return $a;
            }
            */

						if($a['success'])
						{
		            //2.- Actualiza trtalonesViajestbl
		            $sqlUpdTalonStr =   "UPDATE trTalonesViajesTbl ".
		                                "SET claveMovimiento = 'TV', ".
		                                "tipoDocumento = 'TV', ".
		                                "tipoTalon = 'NA' ".  
		                                "WHERE idViajeTractor = ".$_REQUEST['trCancelacionViajesIdViajeHdn']." ".
		                                "AND claveMovimiento = 'TG'";
		            //echo "$sqlUpdTalonStr<br>";
		            $rs = fn_ejecuta_query($sqlUpdTalonStr);
		
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
									$a['success'] = false;
									$a['sql'] = $sqlUpdTalonStr;
									$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del tal&oacuten del viaje.";
								}						
						}

						if($a['success'])
						{
		            //3.- Actualiza trgastosviajetractortbl
		            $sqlUpdGastosViajeFolioStr =    "UPDATE trGastosViajeTractorTbl ".
		                                            "SET claveMovimiento = 'GX' ".
		                                            "WHERE idViajeTractor = ".$_REQUEST['trCancelacionViajesIdViajeHdn'];
								//echo "$sqlUpdGastosViajeFolioStr<br>";
		            $rs = fn_ejecuta_query($sqlUpdGastosViajeFolioStr);								
		            
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
									$a['success'] = false;
									$a['sql'] = $sqlUpdGastosViajeFolioStr;
									$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del gasto del viaje.";
								}					            
						}

            /*  
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlUpdGastosViajeFolioStr;
                $a['successMsg'] .= 'Se ha actualizado correctamente los Gastos a GX-GASTO CANCELADO';
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdGastosViajeFolioStr;
                return $a;
            }
            */

						if($a['success'])
						{
		            //*4.- Si es un Centro de Distribución con cdValidos actualiza la Lista de Espera a VV
		            $sqlUpdListaEsperaStr = "UPDATE trEsperaChoferesTbl ".
		                                    "SET claveMovimiento = 'VV', ".
		                                    "fechaEvento = '".date('Y-m-d H:i:s')."' ".
		                                    "WHERE idTractor = ".$_REQUEST['trViajesTractoresIdTractorHdn'].
		                                    " AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
		                                    "AND '".$_SESSION['usuCompania']."' = (SELECT ge.valor FROM caGeneralesTbl ge ".
                                        "WHERE ge.tabla = 'trViajesTractoresTbl' ".
                                        "AND ge.columna = 'cdValidos' ".
                                        "AND ge.valor = '".$_SESSION['usuCompania']."');";
								//echo "$sqlUpdListaEsperaStr<br>";
		            $rs = fn_ejecuta_query($sqlUpdListaEsperaStr);		
		            
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
									$a['success'] = false;
									$a['sql'] = $sqlUpdListaEsperaStr;
									$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten de la fecha.";
								}				            						
						}
              
            /*
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlUpdListaEsperaStr;
                $a['successMsg'] .= 'Se ha actualizado correctamente la Lista de Espera VV';
                $a['successMessage'] = getViajeCanceladoSuccessMsg('VG');
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdGastosViajeFolioStr;
                return $a;
            }
            */
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;  
    }

    function getCancelacionGastosFolio(){
        $a = array();
        $e = array();
        $a['success'] = true;
        $a['successMsg'] = 'Cancelaci&oacuten efectuada.';

        if($_REQUEST['trCancelacionGastosFoliosHdn'] == ""){
            $e[] = array('id'=>'trCancelacionGastosFoliosHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {

            //1.-Actualiza trgastosviajetractortbl
            $sqlUpdGastosViajeFolioStr =    "UPDATE trGastosViajeTractorTbl ".
                                            "SET claveMovimiento = 'GX' ".
                                            "WHERE folio = '".$_REQUEST['trCancelacionGastosFoliosHdn']."'";
						//echo "$sqlUpdGastosViajeFolioStr<br>";
            $rs = fn_ejecuta_query($sqlUpdGastosViajeFolioStr);
            
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
							$a['success'] = false;
							$a['sql'] = $sqlUpdGastosViajeFolioStr;
							$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del gasto del viaje.";
						}	            

						if($a['success'])
						{
		            //2.-Si es el único folio de GA actualiza a GX en trViajesTractoresTbl
		            //var_dump($_REQUEST['claveMovimiento']);
		            if($_REQUEST['claveMovimiento'] == 'VC' || $_REQUEST['claveMovimiento'] == 'VG')
		            {
				            $sql =  "SELECT DISTINCT folio FROM trGastosViajeTractorTbl ".
                            "WHERE claveMovimiento != 'GX'".
                            "AND idViajeTractor = ".$_REQUEST['idViajeTractor']." ".
                            "AND folio != '".$_REQUEST['trCancelacionGastosFoliosHdn']."'";
										//echo "$sql<br>";
										$rsMovAux = fn_ejecuta_query($sql);
										//Cuando ya no hay folios por cancelar
										if($rsMovAux['records'] == 0)
										{
												$claveMovimiento = ($_REQUEST['claveMovimiento'] == 'VC')?'VX':'VV';
												$sql = "UPDATE trViajesTractoresTbl ".
	                            "SET claveMovimiento = '".$claveMovimiento."' ".
	                            "WHERE idViajeTractor = ".$_REQUEST['idViajeTractor'];
							          //echo "$sql<br>"; 
												fn_ejecuta_query($sql);
												
						            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
													$a['success'] = false;
													$a['sql'] = $sql;
													$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del viaje.";
												}													    
										}										
		            }
		            if($_REQUEST['trCancelacionGastosFoliosCveMovHdn'] == 'GA'){
		                $sqlUpdViajesTractoresStr = "UPDATE trViajesTractoresTbl SET claveMovimiento = 'VX' ".
		                                            "WHERE idViajeTractor = (SELECT DISTINCT gv1.idViajeTractor ".
		                                            "FROM trGastosViajeTractorTbl  gv1 ".
		                                            "WHERE gv1.folio = '".$_REQUEST['trCancelacionGastosFoliosHdn']."') ".
		                                            "AND (SELECT count(gv2.idViajeTractor) as numFolios FROM 23trGastosViajeTractorTbl gv2 ".
                                                "WHERE gv2.idViajeTractor = (SELECT DISTINCT gv3.idViajeTractor ".
                                                "FROM trGastosViajeTractorTbl  gv3 ".
                                                "WHERE gv3.folio = '".$_REQUEST['trCancelacionGastosFoliosHdn']."') ".
		                                            "GROUP BY gv2.idViajeTractor ) = 1";
										//echo "$sqlUpdViajesTractoresStr<br>";
		                fn_ejecuta_query($sqlUpdViajesTractoresStr);
		                
				            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
											$a['success'] = false;
											$a['sql'] = $sqlUpdViajesTractoresStr;
											$a['errorMessage'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del viaje.";
										}	 		                
		            }
		
		            /*
		            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
		                $a['sql'] = $sqlUpdViajesTractoresStr;
		                $a['successMessage'] = getViajeCanceladoSuccessMsg('FO');
		            } else {
		                $a['success'] = false;
		                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdViajesTractoresStr;
		            }	
		            */						
						}
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getCancelacionPreviaje() {
        $a['success'] =  true;
        $e = array();
    
        if($_REQUEST['trCancelacionViajesIdViajeHdn'] == ""){
            $e[] = array('id'=>'trCancelacionViajesIdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }     

        if($a['success'] == true){
            //1.- Cancelo el Viaje
            $sqlUpdViajeStr = "UPDATE trViajesTractoresTbl ".
                            "SET claveMovimiento = 'VX' ".
                            "WHERE idViajeTractor = ".$_REQUEST['trCancelacionViajesIdViajeHdn'];
          
            //$rs = fn_ejecuta_query($sqlUpdViajeStr);

            if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == '')){
                $a['sql'] = $sqlUpdViajeStr;
                $a['successMsg'] = 'Se actualizó correctamente trViajesTractoresTbl |';
            } else {
                $a['success'] = false;
                $a['errorMessage'] = 'No se pudo Cancelar el Viaje. </br>'.$_SESSION['sql'].$sqlUpdViajeStr;
                return $a;
            }

            //2.-Cancelo los Talones
            $sqlUpdTalonStr = "UPDATE trTalonesViajesTbl ".
                                "SET claveMovimiento = 'TX' ".
                                "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ". 
                                "AND claveMovimiento='TV' ";

            $rs = fn_ejecuta_query($sqlUpdTalonStr);

            if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == '')){
                $a['sql'] = $sqlUpdViajeStr;
                $a['successMsg'] .= 'Se actualizó correctamente trTalonesViajesTbl |';
            } else {
                $a['success'] = false;
                $a['errorMessage'] = 'No se pudo Cancelar los Talones. </br>'.$_SESSION['sql'].$sqlUpdViajeStr;
                return $a;
            }

            //*3.- Si está en la Lista de Espera pone Disponible el Tractor VD                      

            //SI ES UN CENTRO DE DITRIBUCIÓN VÁLIDO ACTUALIZA LA LISTA DE ESPERA
            $_REQUEST['catGeneralesTablaTxt'] = 'trViajesTractoresTbl';
            $_REQUEST['catGeneralesColumnaTxt'] = 'cdValidos';
            $_REQUEST['catGeneralesValorTxt'] = $_SESSION['usuCompania'];
            $rs = getGenerales();
         
            if(count($rs['root']) > 0){ //Es un Centro Válido pone Disponible el Tractor VD
                $sqlUpdEsperaChoferStr =    "UPDATE trEsperaChoferesTbl ".
                                            "SET claveMovimiento = 'VD' ".
                                            "WHERE idTractor = '".$_REQUEST['trCancelacionIdTractorHdn']."' ";                                             
                                           
                $rs = fn_ejecuta_query($sqlUpdEsperaChoferStr);

                if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == '')){
                    $a['sql'] = $sqlUpdEsperaChoferStr;
                    $a['successMsg'] .= 'Se actualizó correctamente trChoferesEsperaTbl |';
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = 'No se pudo Actualizar la Lista de Espera. </br>'.$_SESSION['sql'].$sqlUpdEsperaChoferStr;
                    return $a;
                }

                //*4.- Elimina las Unidades Embarcadas

                $sqlUnidadesEm="SELECT distinct folio FROM trUnidadesEmbarcadasTbl ".
                				"WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
                $rsEmbarcadas=fn_ejecuta_query($sqlUnidadesEm);


                for ($i=0; $i < sizeof($rsEmbarcadas['root']); $i++) { 
                	$folioCan[]=$rsEmbarcadas['root'][$i]['folio'];

                	 if (sizeof($rsEmbarcadas['root'][0]['folio']) >= 1) {
                		$a['successMessage'] = "FAVOR DE PEDIR LA CANCELACION EN SICA ".$folioCan[$i];                	
	                }else{
	                	$a['successMessage'] = 'PreViaje Cancelado Correctamente';
	                }
                }

               //ECHO sizeof($rsEmbarcadas['root'][0]['folio']);

               


                $sqlDelUnidadesEmbarcadas =  "DELETE FROM trUnidadesEmbarcadasTbl ".
                                             "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";

                fn_ejecuta_query($sqlDelUnidadesEmbarcadas);

                if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == '')){
                    $a['sql'] = $sqlDelUnidadesEmbarcadas;
                    $a['successMsg'] .= 'Se eliminaron Unidades correctamente de trUnidadesEmbarcadasTbl';
                    $a['successMessage'] = $a['successMessage'];
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = 'No se Eliminaron las Unidades Embarcadas. </br>'.$_SESSION['sql'].$sqlUpdViajeStr;
                    return $a;
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }
  
    function getCancelacionAsignacion(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trCancelacionViajesIdViajeHdn'] == ""){
            $e[] = array('id'=>'trCancelacionViajesIdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['trCancelacionIdTractorHdn'] == ""){
            $e[] = array('id'=>'trCancelacionIdTractorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionCompaniaHdn'] == ""){
            $e[] = array('id'=>'cancelacionAsignacionCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionContadorHdn'] == ""){
            $e[] = array('id'=>'cancelacionAsignacionContadorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionVinHdn'] == ""){
            $e[] = array('id'=>'cancelacionAsignacionVinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['cancelacionAsignacionChoferHdn'] == ""){
            $e[] = array('id'=>'cancelacionAsignacionChoferHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }        
        
        if ($a['success'] == true) {   



            $talon= $_REQUEST['cancelacionAsignacionTalonHdn'];
            $talon1= rtrim($talon,"','");
           



            $sqlAddTemporal = "INSERT INTO trtalonesviajestmp (idViajeTractor, numeroTalon, distribuidor, numeroUnidades, direccionEntrega, companiaRemitente,centroDistribucion, observaciones, tipoTalon) ".
                                "SELECT idViajeTractor, folio , distribuidor, numeroUnidades, direccionEntrega, companiaRemitente,centroDistribucion, observaciones, tipoTalon  ".
                                "FROM trtalonesviajestbl ".
                                "WHERE idViajeTractor = ".$_REQUEST['trCancelacionViajesIdViajeHdn']." ".
                                "AND claveMovimiento = 'TF' ".
                                "AND tipoTalon != 'TE' ".
                                "AND tipoDocumento = 'TF'";
          
            fn_ejecuta_query($sqlAddTemporal); 


         $sqlUpdTalonStr = "UPDATE trTalonesViajesTbl ".
                            "SET claveMovimiento = 'TX' ".
                            "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ".
                            "AND idTalon in (".$talon1.") "; 
                                
         $rs = fn_ejecuta_query($sqlUpdTalonStr);


        /*$sqlAddTalon = "INSERT INTO trtalonesviajestbl (distribuidor, folio, idViajeTractor, companiaRemitente, idPlazaOrigen, idPlazaDestino, ".
                        "direccionEntrega, centroDistribucion, tipoTalon, fechaEvento, observaciones, numeroUnidades, importe, seguro, tarifaCobrar, ". 
                        "kilometrosCobrar, impuesto, retencion, claveMovimiento, tipoDocumento) ".
                         "SELECT distribuidor, (SELECT folio ".
                                                "FROM trfoliostbl ".
                                                "WHERE centroDistribucion ='".$_REQUEST['trCancelacionViajesCdistribucionHdn']."' ".
                                                "AND compania ='".$_REQUEST['cancelacionAsignacionCompaniaHdn']."' AND tipoDocumento='TF') as folio, ".
                        "idViajeTractor, companiaRemitente, idPlazaOrigen, idPlazaDestino, direccionEntrega, centroDistribucion, ".
                        " 'NA' as tipoTalon, now() AS fechaEvento, concat('FECHATALON : ',now()) as observaciones, numeroUnidades, importe, seguro, tarifaCobrar, kilometrosCobrar, ".
                        "impuesto, retencion, 'TG'as claveMovimiento, 'TG' as tipoDocumento ".
                        "FROM trtalonesviajestbl ".
                        "WHERE idViajeTractor =".$_REQUEST['trCancelacionViajesIdViajeHdn']." ".
                        "AND idTalon in(".$talon1.") ";*/




        $sqlAddTalon =  "INSERT INTO trtalonesviajestbl (distribuidor, folio, idViajeTractor, companiaRemitente, idPlazaOrigen, idPlazaDestino, ".
                        "direccionEntrega, centroDistribucion, tipoTalon, fechaEvento, observaciones, numeroUnidades, importe, seguro, tarifaCobrar, ". 
                        "kilometrosCobrar, impuesto, retencion, claveMovimiento, tipoDocumento) ".
                        "SELECT tl.distribuidor, ".
                        "(SELECT folio FROM trfoliostbl WHERE centroDistribucion ='".$_REQUEST['trCancelacionViajesCdistribucionHdn']."' ".
                        " AND compania = '".$_REQUEST['cancelacionAsignacionCompaniaHdn']."' AND tipoDocumento='TF') as folio, ".
                        " tl.idViajeTractor, tl.companiaRemitente, tl.idPlazaOrigen, tl.idPlazaDestino, tl.direccionEntrega, tl.centroDistribucion, ".
                        " 'NA' as tipoTalon, now() AS fechaEvento, concat('FECHATALON : ',now()) as observaciones, tl.numeroUnidades, tl.importe, tl.seguro, tl.tarifaCobrar, tl.kilometrosCobrar, ".
                        " tl.impuesto, tl.retencion, 'TG'as claveMovimiento, 'TG' as tipoDocumento ".
                        " FROM trtalonesviajestbl tl, trtalonesviajestmp tmp ".
                        " WHERE tl.idViajeTractor = tmp.idViajeTractor ".
                        " AND tl.folio = tmp.numeroTalon ".
                        " AND tl.idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";                        
                        
        fn_ejecuta_Add($sqlAddTalon);


       /* $sqlUpdFolios =  "UPDATE trfoliostbl SET folio=(SELECT folio FROM (SELECT sum(folio +1) AS folio FROM trfoliostbl ".
                                                                            "WHERE centroDistribucion ='".$_REQUEST['trCancelacionViajesCdistribucionHdn']."' ".
                                                                            "AND compania ='".$_REQUEST['cancelacionAsignacionCompaniaHdn']."' ".
                                                                            "AND tipoDocumento='TF') AS folio) ".
                         " WHERE compania ='".$_REQUEST['cancelacionAsignacionCompaniaHdn']."' ".
                         "and centroDistribucion ='".$_REQUEST['trCancelacionViajesCdistribucionHdn']."' ".
                         "AND tipoDocumento ='TF' ";
        fn_ejecuta_Upd($sqlUpdFolios);

*/

         $valCenDis =  "SELECT 1  as bandera from cageneralestbl where tabla='trunidadesembarcadastbl'
                          AND columna='centroDisValidos'
                          AND valor=  '".$_REQUEST['trCancelacionViajesCdistribucionHdn']."' ";
                          
                        $rsvalCenDis = fn_ejecuta_query($valCenDis);  


        if($rsvalCenDis['root'][0]['bandera'] != 1 ){
            $valorEstatus='L'; // ANTES 'E' DE EMBARCADO
        }else{
            $valorEstatus='L';
        }


        $addEmbarcadas = "INSERT INTO trunidadesembarcadastbl (centroDistribucion,idViajeTractor,vin,fechaEmbarque,claveMovimiento) ".
                             "SELECT distinct a.centroDistribucion,t.idViajeTractor,t2.vin,NOW() AS fechaEmbarque,'".$valorEstatus."' AS claveMovimiento ".
                             "FROM trviajestractorestbl t,trtalonesviajestbl t1,trunidadesdetallestalonestbl t2,alultimodetalletbl a ".
                             "WHERE t.idViajeTractor = t1.idViajeTractor ".
                             "AND t1.idTalon = t2.idTalon ".
                             "AND t2.vin= a.vin ".
                             "AND a.claveMovimiento='AM' ".
                             "AND t.idViajeTractor =".$_REQUEST['trCancelacionViajesIdViajeHdn']." ";

            //fn_ejecuta_Add($addEmbarcadas);
        fn_ejecuta_Add($addEmbarcadas);
            
            //Actualiza Viajes Tractores
        $sqlUpdViajeStr =   "UPDATE trViajesTractoresTbl ".
                                "SET claveMovimiento = 'VG' ".
                                "WHERE idViajeTractor = ".$_REQUEST['trCancelacionViajesIdViajeHdn']." ";

          fn_ejecuta_Add($sqlUpdViajeStr);

            // INSERTAR EN LA TEMPORAL LOS TALONES ACTIVOS

         
            //echo json_encode($addEmbarcadas); 

                            
            //Actualiza Talones Viajes
            //SI ES UN CENTRO DE DISTRIBUCIÓN VÁLIDO ACTUALIZA LAS UNIDADES A 0
            $_REQUEST['catGeneralesTablaTxt'] = 'trViajesTractoresTbl';
            $_REQUEST['catGeneralesColumnaTxt'] = 'cdValidos';
            $_REQUEST['catGeneralesValorTxt'] = $_SESSION['usuCompania'];
            $rs = getGenerales();
            $sqlCdValido  = count($rs['root']) > 0 ? "numeroUnidades = 0, " : "";

         

            //Actualizar  alhistoricounidadestbl
            if ($_REQUEST['cancelacionAsignacionVinHdn'] != "|") {
                $vinAsignados = explode('|', substr($_REQUEST['cancelacionAsignacionVinHdn'], 0, -1));
                for($i = 0; $i<sizeof($vinAsignados);$i++) {
                    $sqlHistorico = "SELECT MAX(idHistorico) FROM alhistoricounidadestbl ".
                                    "WHERE vin ='".$vinAsignados[$i]."'";
                
                    $his = fn_ejecuta_query($sqlHistorico);
                    $historico =  ($his['root'][0]['MAX(idHistorico)']);

                    $sqlUpdLlavesRecepcionStr = "UPDATE alhistoricounidadestbl ".
                                                "SET claveMovimiento = 'CA' ".
                                                "WHERE idHistorico = ".$historico;

                    $rs = fn_ejecuta_query($sqlUpdLlavesRecepcionStr);
                }
            }
            
            //Actualiza alultimodetalle
            if ($_REQUEST['cancelacionAsignacionVinHdn'] != "|") {
                $vinAsignados = explode('|', substr($_REQUEST['cancelacionAsignacionVinHdn'], 0, -1));

                for($i = 0; $i<sizeof($vinAsignados);$i++) {
                    $sqlUpdUltimoDetalle =  "UPDATE alultimodetalletbl ".
                                                "SET claveMovimiento = (SELECT hu.claveMovimiento FROM alhistoricounidadestbl hu ".
                                            "WHERE hu.claveMovimiento  IN (SELECT g2.valor FROM caGeneralesTbl g2 ".
                                                "WHERE g2.tabla = 'alHistoricoUnidadesTbl' AND g2.columna = 'validos' ".
                                                "AND g2.valor = hu.claveMovimiento) ".
                                            "AND hu.vin = '".$vinAsignados[$i]."' ORDER BY hu.fechaEvento DESC LIMIT 1), ".
                                                "fechaEvento = '".date("Y-m-d H:i:s")."' ".
                                                "WHERE vin='".$vinAsignados[$i]."'";

                    fn_ejecuta_query($sqlUpdUltimoDetalle);
 
                    //Actualiza la tabla de trUnidadesDetallesTalonesTbl por VIN
                    $sqlUpdUnidadesDetallesStr = "UPDATE trUnidadesDetallesTalonesTbl ".
                                                    "SET estatus = 'CA' ".
                                                    "WHERE vin='".$vinAsignados[$i]."'";   

                    fn_ejecuta_query($sqlUpdUnidadesDetallesStr);
                }


            } 
          
            //Obtiene el máximo consecutivo de la Lista de Espera
            $sqlMaxConsecutivo = "SELECT IFNULL(MAX(consecutivo) + 1, 1) AS consecutivoMax FROM trEsperaChoferesTbl";
            $rsConsecutivoMax = fn_ejecuta_query($sqlMaxConsecutivo);
            $consecutivoMax =  $rsConsecutivoMax['root'][0]['consecutivoMax'];
          
            //Insertar tresperaChoferestbl 
           /* $sqlAddEsperaChoferStr = "INSERT INTO trEsperaChoferesTbl (centroDistribucion, idTractor, ".
                                        "claveChofer, consecutivo, fechaEvento, claveMovimiento) ".
                                        "VALUES ('".$_SESSION['usuCompania']."','".$_REQUEST['trCancelacionIdTractorHdn']."','".
                                        $_REQUEST['cancelacionAsignacionChoferHdn']."', ".$consecutivoMax.", '".date("Y-m-d H:i:s")."','VG')";

            $rs = fn_ejecuta_query($sqlAddEsperaChoferStr);
            */
            
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlAddEsperaChoferStr;
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddEsperaChoferStr;
            }
        }

        // INSERCION DE LOS NUEVOS TALONES ACTIVOS CON CLAVE MOVIMIENTO TG Y TIPO DE DOCUMENTO TG
        $sqlAddTalones = "INSERT INTO trtalonesviajestbl (distribuidor, folio, idViajeTractor, companiaRemitente, idPlazaOrigen, idPlazaDestino, direccionEntrega, centroDistribucion, tipoTalon, fechaEvento, observaciones, numeroUnidades, importe, seguro, tarifaCobrar, kilometrosCobrar, impuesto, retencion, claveMovimiento, tipoDocumento) ".
                         "SELECT tt.distribuidor, tt.numeroTalon, tt.idViajeTractor, tt.companiaRemitente, tl.idPlazaOrigen, tl.idPlazaDestino, tt.direccionEntrega, ".
                         "tl.centroDistribucion,'NA' as tipoTalon,tl.fechaEvento, tl.observaciones,'0' as numeroUnidades,tl.importe,tl.seguro,tl.tarifaCobrar, tl.kilometrosCobrar,  ".
                         "tl.impuesto, tl.retencion,'TG','TG' ".
                         "FROM trtalonesviajestmp tt, trtalonesviajestbl tl ".
                         "WHERE tt.numeroTalon = tl.folio ".
                         "AND tt.distribuidor = tl.distribuidor ".
                         "AND tt.idViajeTractor = tl.idViajeTractor ".
                         "GROUP BY tl.folio "; 

        $rs = fn_ejecuta_query($sqlAddTalones);

        // BORRAR LOS TALONES DE LA TEMPORAL
        $dltTemporal =  "DELETE FROM trtalonesviajestmp ".
                        "WHERE idViajeTractor = '".$_REQUEST['trCancelacionViajesIdViajeHdn']."' ";
        
        $rs = fn_ejecuta_query($dltTemporal);
        
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();

        echo json_encode($a);    
    }
?>    