<?php
    session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");


    switch($_SESSION['idioma']){
          case 'ES':
              include("../funciones/idiomas/mensajesES.php");
              break;
          case 'EN':
              include("../funciones/idiomas/mensajesEN.php");
              break;
          default:
              include("../funciones/idiomas/mensajesES.php");
      }

      switch($_REQUEST['dieselExtraHdn']){
          case 'getTractor':
                  getTractor();
              break;
          case 'getDatosViaje':
              getDatosViaje();
              break;
          case 'addConcepto':
              addConcepto();
              break;
          case 'sqlGetConsultas':
              sqlGetConsultas();
              break;
          case 'sqlDltDiesel';
              sqlDltDiesel();
              break;
          default:
              echo 'no esta entrando a ningun proceso';
      }

      function getTractor(){
        $sqlGetTractor = "SELECT tr.tractor, vt.idTractor, vt.idViajeTractor, ".
                          "(SELECT pl1.plaza FROM caplazastbl pl1 WHERE pl1.idPlaza = vt.idPlazaOrigen) as plazaOrigen, ".
                          "(SELECT pl2.plaza FROM caplazastbl pl2 WHERE pl2.idPlaza = vt.idPlazaDestino) as plazaDestino ".
                          "FROM trviajestractorestbl vt, catractorestbl tr ".
                          "WHERE vt.idTractor = tr.idTractor ".
                          "AND vt.claveMovimiento IN ('VG','VF','VU') ".
                          "AND tr.compania = '".$_REQUEST['dieselExtraCompaniaCmb']."' ";
                          //"AND tr.tractor = 772 ;";

        $rsGetTractor = fn_ejecuta_query($sqlGetTractor);

        echo json_encode($rsGetTractor);
      }

      function getDatosViaje(){
        $sqlGetViaje = "SELECT tr.tractor, vt.idTractor, vt.idViajeTractor,vt.viaje, CONCAT(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoMaterno,' ',ch.apellidoPaterno) as chofer, vt.kilometrosTabulados, ".
                        "(SELECT pl1.plaza FROM caplazastbl pl1 WHERE pl1.idPlaza = vt.idPlazaOrigen) as plazaOrigen, ".
                        "(SELECT pl2.plaza FROM caplazastbl pl2 WHERE pl2.idPlaza = vt.idPlazaDestino) as plazaDestino ".
                        "FROM trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch ".
                        "WHERE vt.idTractor = tr.idTractor ".
                        "AND vt.claveChofer = ch.claveChofer ".
                        "AND vt.claveMovimiento IN ('VG','VF','VU') ".
                        "AND tr.idTractor = '".$_REQUEST['dieselExtraTractorCmb']."';";



                                  "AND tr.idTractor = '' ";
                                  //"AND tr.tractor = 772 ;";

        $rsGetViajer = fn_ejecuta_query($sqlGetViaje);
        echo json_encode($rsGetViajer);
      }

      function addConcepto(){

        $sqlAddDextra = "INSERT INTO trgastosviajetractortbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, numeroTarjeta, litros, usuario, ip, subTotal, iva) VALUES(".
                        "'".$_REQUEST['dieselExtraIdViajeHdn']."',".
                        "'".$_REQUEST['dieselExtraConceptoCmb']."',".
                        "'".$_SESSION['usuCompania']."',".
                        "'001',".
                        "NOW(),".
                        "'0',".
                        "'1',".
                        "'".$_REQUEST['dieselExtraLitrosTxt']."',".
                        "'DIESEL EXTRA',".
                        "'GD',".
                        "'0.00',".
                        "'".$_REQUEST['dieselExtraLitrosTxt']."',".
                        $_SESSION['idUsuario'].",".
                        "'10.1.2.1',".
                        "'0.00',".
                        "'0.00');";

        fn_ejecuta_query($sqlAddDextra);


      }
      function sqlGetConsultas(){
        $sqlGetDiesel ="SELECT concat(tr.compania,' - ',tr.tractor) as tractor, concat(pe.concepto,' - ',co.nombre) as concepto, pe.fechaEvento, pe.litros FROM trgastosviajetractortbl pe, trviajestractorestbl vt, catractorestbl tr, caconceptostbl co WHERE pe.idViajeTractor = vt.idViajeTractor AND pe.concepto = co.concepto AND vt.idTractor = tr.idTractor AND pe.concepto in (SELECT cc.concepto FROM caConceptosCentrosTbl cc, caDistribuidoresCentrosTbl cd, caConceptosTbl cp WHERE cc.centroDistribucion = cd.distribuidorCentro AND cp.concepto = cc.concepto  AND cc.centroDistribucion='CDTOL' AND cc.concepto LIKE '%90%') AND vt.idViajeTractor = ".$_REQUEST['dieselExtraIdViajeHdn']." and (pe.claveMovimiento !='XD' AND pe.claveMovimiento !='XC'); ";

        $rsGetDiesel = fn_ejecuta_query($sqlGetDiesel);

        echo json_encode($rsGetDiesel);
      }

      function sqlDltDiesel(){

        $sqlDltCombustible = "DELETE FROM trgastosviajetractortbl ".
                              "WHERE idViajeTractor = '".$_REQUEST['dieselExtraIdViajeHdn']."' ".
                              "AND concepto = '".$_REQUEST['dieselExtraConceptoCmb']."' ".
                              "AND claveMovimiento='GD' ";

        fn_ejecuta_query($sqlDltCombustible);
      }

