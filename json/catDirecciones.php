<?php
    session_start();
    $_SESSION['modulo'] = "catDirecciones";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);
	
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    } 

    switch($_REQUEST['catDireccionesActionHdn']){
        case 'getDireccionesCombo':
            getDireccionesCombo();
            break;
        case 'getDirecciones':
        	getDirecciones();
        	break;
        case 'getDireccionesGrid':
            getDireccionesGrid();
            break;
        case 'getDireccionDistribuidor':
            getDireccionDistribuidor();
            break;
        case 'addDirecciones':
        	addDirecciones($_REQUEST['catDireccionesCalleNumeroHdn'], $_REQUEST['catDireccionesIdColoniaHdn'], $_REQUEST['catDireccionesDistribuidorHdn'], $_REQUEST['catDireccionesTipoHdn']);
        	break;
        case 'updDirecciones':
            updDirecciones($_REQUEST['catDireccionesDireccionHdn'], $_REQUEST['catDireccionesCalleNumeroHdn'], $_REQUEST['catDireccionesIdColoniaHdn'], $_REQUEST['catDireccionesDistribuidorHdn'], $_REQUEST['catDireccionesTipoHdn']);
            break;
        case 'dltDireccion':
            dltDireccion($_REQUEST['catDireccionesDireccionHdn']);
            break;
        case 'dltDireccionesMult':
            dltDireccionesMult();
        case 'dltDireccionesNull':
            dltDireccionesNull();
            break;
        case 'addUpdateDirecciones':
            addUpdateDirecciones();
            break;
        default:
            echo '';         
    }

   	function dltDireccionesNull(){

        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catDistCentroTipoDireccionHdn'] == ''){
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
            $e[] = array('id'=>'catDistCentroTipoDireccionHdn', 'msg'=>getRequerido());
        }

        if($a['success']){
            $sqlDeleteNull = 'DELETE FROM caDireccionesTbl WHERE distribuidor IS NULL AND tipoDireccion = '.
                              "'".$_REQUEST['catDistCentroTipoDireccionHdn']."'";

            $rs = fn_ejecuta_query($sqlDeleteNull);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = 'Se borraron correctamente las direcciones';    
            }
            else{
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDeleteNull;
                $a['success'] = false;
            }
        }

        $a['sql']= $sqlDeleteNull;
        $a['errors'] = $e;
        echo json_encode($a);
        
    }


    function getDireccionesCombo(){
		$lsWhereStr = "WHERE d.idColonia = c.idColonia ". 
		              "AND c.idMunicipio = m.idMunicipio ".
		              "AND m.idEstado = e.idEstado ".
		              "AND e.idPais = p.idPais ".
                      "AND (tipoDireccion='".$_REQUEST['catDistCentroTipoDireccionHdn']."' ".
                      "AND distribuidor='".$_REQUEST['catDistCentroDistribuidorHdn']."' ".
                      "OR distribuidor IS NULL ".
                      "AND tipoDireccion='".$_REQUEST['catDistCentroTipoDireccionHdn']."')";


		$sqlGetDireccionesComboStr = "SELECT d.calleNumero, d.idColonia, c.colonia, m.municipio, ".
									 "e.estado, p.pais, c.cp, d.direccion " .
		       						 "FROM cadireccionestbl d, cacoloniastbl c, camunicipiostbl m, capaisestbl p, ".
		       						 "caestadostbl e  " . $lsWhereStr;       
		
		$rs = fn_ejecuta_query($sqlGetDireccionesComboStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['direccionCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];
        }
			
		echo json_encode($rs);
	}

	function getDirecciones(){
		$lsWhereStr = "WHERE d.idColonia = c.idColonia ". 
		              "AND c.idMunicipio = m.idMunicipio ".
		              "AND m.idEstado = e.idEstado ".
		              "AND e.idPais = p.idPais ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDistribuidorHdn'], "d.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTipoDireccionHdn'], "d.tipoDireccion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

		$sqlGetDireccionesStr = "SELECT d.direccion, d.calleNumero, c.idColonia, c.colonia, d.distribuidor, ".
								"m.idMunicipio, m.municipio, e.idEstado, e.estado, p.idPais, p.pais, d.tipoDireccion ".
								"FROM cadireccionestbl d, cacoloniastbl c, camunicipiostbl m, caestadostbl e, capaisestbl p ".$lsWhereStr;

		$rs = fn_ejecuta_query($sqlGetDireccionesStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['direccionCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];
        }
        
		echo json_encode($rs);
	}

    function getDireccionesGrid(){
        $lsWhereStr = "WHERE d.idColonia = c.idColonia ". 
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ".
                      "AND (tipoDireccion='".$_REQUEST['catDistCentroTipoDireccionHdn']."' ".
                      "AND distribuidor='".$_REQUEST['catDistCentroDistribuidorHdn']."' ".
                      "OR distribuidor IS NULL ".
                      "AND tipoDireccion='".$_REQUEST['catDistCentroTipoDireccionHdn']."')";
        
        
        $sqlGetDirGridStr = "SELECT d.direccion, d.calleNumero, c.idColonia, c.colonia, d.distribuidor, ".
                            "m.idMunicipio, m.municipio, e.idEstado, e.estado, p.idPais, p.pais, d.tipoDireccion ".
                            "FROM cadireccionestbl d, cacoloniastbl c, camunicipiostbl m, caestadostbl e, capaisestbl p ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetDirGridStr);
            
        echo json_encode($rs);
    }

    function getDireccionDistribuidor(){
        $lsWhereStr = "WHERE d.idColonia = c.idColonia ".
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ";

        if ($_REQUEST['catDireccionesTipoDireccionHdn'] == 'entrega') {
            $lsWhereStr .= "AND dc.direccionEntrega = d.direccion ";
        } elseif ($_REQUEST['catDireccionesTipoDireccionHdn'] == 'fiscal') {
            $lsWhereStr .= "AND dc.direccionFiscal = d.direccion ";
        }

        $sqlGetDireccionDistribuidorStr = "SELECT d.calleNumero, c.colonia, m.municipio, e.estado, p.pais, c.cp, d.direccion " .
                                          "FROM cadireccionestbl d, cacoloniastbl c, camunicipiostbl m, capaisestbl p, ".
                                          "caestadostbl e, cadistribuidorescentrostbl dc " . $lsWhereStr.
                                          "AND dc.distribuidorCentro = d.distribuidor ".
                                          "AND dc.direccionEntrega=d.direccion ".
                                          "AND d.distribuidor = '".$_REQUEST['catDireccionesDistribuidorHdn']."'";  
        
        $rs = fn_ejecuta_query($sqlGetDireccionDistribuidorStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['direccionCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];
        }
            
        echo json_encode($rs);
    }

	function addDirecciones($RQcalleNumero, $RQidColonia, $RQdistribuidor, $RQtipo){

    $a = array();
        $e = array();
        $a['success'] = true;
        $massive=0;
        
        if(substr($RQtipo, 0, 2) != 'DI' && substr($RQtipo, 0, 2) != 'DE'){
      if($RQcalleNumero == "") {
                $e[] = array('id'=>'RQcalleNumero','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($RQidColonia == "") {
                $e[] = array('id'=>'RQidColonia','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
        } else {
            $massive = 1;
            //Revisar que ningun dato del grid esté vacio
            $calleNumeroArr = explode('|', substr($RQcalleNumero, 0, -1));
            if(in_array('', $calleNumeroArr)){
             $e[] = array('id'=>'RQcalleNumeroArr','msg'=>getRequerido());
               $a['errorMessage'] = getErrorRequeridos();
               $a['success'] = false;
            }
            $coloniaArr = explode('|', substr($RQidColonia, 0, -1));
            if(in_array('', $coloniaArr)){
             $e[] = array('id'=>'RQidColoniaArr','msg'=>getRequerido());
               $a['errorMessage'] = getErrorRequeridos();
               $a['success'] = false;
            }
            $tipoDireccionArr = explode('|', substr($RQtipo, 0, -1));
            if(in_array('', $tipoDireccionArr)){
                $e[] = array('id'=>'RQtipoArr','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            //Explode al campo que puede ser vacio
            $distribuidorArr = explode('|', substr($RQdistribuidor, 0, -1));
        }

        if ($a['success'] == true) {
          $sqlAddDireccionesStr = "INSERT INTO cadireccionestbl (calleNumero, idColonia, distribuidor, tipoDireccion) VALUES";

          if(count($calleNumeroArr)){
                for($nInt=0; $nInt<count($calleNumeroArr); $nInt++){
              $sqlAddDireccionesStr .= "('".$calleNumeroArr[$nInt]."', ".
                            $coloniaArr[$nInt].", ".
                                            replaceEmptyNull("'".$distribuidorArr[$nInt]."'").", ".
                                            "'".$tipoDireccionArr[$nInt]."')";

    
            if($nInt+1<count($calleNumeroArr)){
                    $sqlAddDireccionesStr .= ",";
                 }
             }
            } else {
                $sqlAddDireccionesStr .= "('".$RQcalleNumero."', ".
                                         $RQidColonia.", ".
                                         replaceEmptyNull("'".$RQdistribuidor."'").", ".
                                         "'".$RQtipo."')";

            }

          $rs = fn_ejecuta_Add($sqlAddDireccionesStr);
          
          if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
          $a['sql'] = $sqlAddDireccionesStr;
                $a['successMessage'] = getDireccionesSuccessMsg($massive);
                $a['id'] = $RQcalleNumero;
      } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddDireccionesStr;
      }
    }
        $a['errors'] = $e;
    $a['successTitle'] = getMsgTitulo();
        if(substr($RQtipo, 0, 2) == 'DI' || substr($RQtipo, 0, 2) == 'DE'){
            $a['root'][0]['success'] = $a['success'];
            $a['root'][0]['errorMessage'] = $a['errorMessage'];
            $a['root'][0]['successMessage'] = $a['successMessage'];
            echo json_encode($a);
        }
  }


	function updDirecciones($RQdireccion, $RQcalleNumero, $RQidColonia, $RQdistribuidor, $RQtipo){
		  $a = array();
      $e = array();
      $a['success'] = true;
      $arrSize = 0;
      $massive = 0;
      $new = 0;
      $sqlCompletoStr = "";
		
		  if(substr($RQtipo, 0, 2) != 'DI' && substr($RQtipo, 0, 2) != 'DE'){
          if($RQcalleNumero == "") {
              $e[] = array('id'=>'RQcalleNumero','msg'=>getRequerido());
              $a['errorMessage'] = getErrorRequeridos();
              $a['success'] = false;
          }
          if($RQidColonia == "") {
              $e[] = array('id'=>'RQidColonia','msg'=>getRequerido());
              $a['errorMessage'] = getErrorRequeridos();
              $a['success'] = false;
          } 
          $arrSize = 1;           
      } else {
          $massive = 1;
          //Revisar que ningun dato del grid esté vacio
          $calleNumeroArr = explode('|', substr($RQcalleNumero, 0, -1));
          if(in_array('', $calleNumeroArr)){
             $e[] = array('id'=>'RQcalleNumero','msg'=>getRequerido());
             $a['errorMessage'] = getErrorRequeridos();
             $a['success'] = false;
          }
          $coloniaArr = explode('|', substr($RQidColonia, 0, -1));
          if(in_array('', $coloniaArr)){
             $e[] = array('id'=>'RQidColonia','msg'=>getRequerido());
             $a['errorMessage'] = getErrorRequeridos();
             $a['success'] = false;
          }
          $tipoDireccionArr = explode('|', substr($RQtipo, 0, -1));
          if(in_array('', $tipoDireccionArr)){
              $e[] = array('id'=>'RQtipo','msg'=>getRequerido());
              $a['errorMessage'] = getErrorRequeridos();
              $a['success'] = false;
          }            
          //Explode al campo que puede ser vacio
          $direccionArr = explode('|', substr($RQdireccion, 0, -1));
          $distribuidorArr = explode('|', substr($RQdistribuidor, 0, -1));
          
          $arrSize = count($calleNumeroArr);
      }

      if ($a['success'] == true) {
      	 $sqlAddDireccionesStr = "INSERT INTO cadireccionestbl (calleNumero, idColonia, distribuidor, tipoDireccion) VALUES";

      	 for($nInt=0; $nInt<$arrSize; $nInt++){
              if($massive == 1 && $a['success'] == true){
            		  //Si es nueva y no tiene ID registrado
            		  if($direccionArr[$nInt] == ''){
                      
                      if($new != 0){
                          $sqlAddDireccionesStr .= ",";
                      }
                      $new++;

                      $sqlAddDireccionesStr .= "('".$calleNumeroArr[$nInt]."', ".
      								 	                        $coloniaArr[$nInt].", ".
      								 	                        replaceEmptyNull("'".$distribuidorArr[$nInt]."'").", ".
                                                "'".$tipoDireccionArr[$nInt]."')";                       
              	   //Si ya tiene le da UPDATE a sus campos
            	   } else {
                      $sqlUpdDireccionesStr = "UPDATE cadireccionestbl ".
      									                      "SET calleNumero='".$calleNumeroArr[$nInt]."', ".
                                              "idColonia='".$coloniaArr[$nInt]."', ".
                                              "tipoDireccion='".$tipoDireccionArr[$nInt]."', ".
                                              "distribuidor= ".replaceEmptyNull("'".$distribuidorArr[$nInt]."'")." ".
      									                      "WHERE direccion=".$direccionArr[$nInt];
                     
          			     $rs = fn_ejecuta_Upd($sqlUpdDireccionesStr);
                          
                      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                          $a['success'] = false;
                          $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdDireccionesStr;
                      } else {
                          $sqlCompletoStr .= $sqlUpdDireccionesStr." && ";
                      }
          	     }
      	     } else if($massive == 0 && $a['success'] == true){
                  if ($RQdireccion == '') {
                      $sqlAddDireccionesStr .= "('".$RQcalleNumero."', ".
                                            $RQidColonia.", ".
                                            replaceEmptyNull("'".$RQdistribuidor."'").", ".
                                            "'".$RQtipo."')";
                      
                      $rs = fn_ejecuta_Add($sqlAddDireccionesStr);
                      $sqlCompletoStr .= $sqlAddDireccionesStr." && ";
                  } else {

                      $sqlUpdDireccionesStr = "UPDATE cadireccionestbl ".
                                            "SET calleNumero='".$RQcalleNumero."', ".
                                            "idColonia=".$RQidColonia.", ".
                                            "tipoDireccion='".$RQtipo."', ".
                                            "distribuidor= ".replaceEmptyNull("'".$RQdistribuidor."'")." ". 
                                            "WHERE direccion=".$RQdireccion;

                      $rs = fn_ejecuta_Upd($sqlUpdDireccionesStr);
                      $sqlCompletoStr .= $sqlUpdDireccionesStr." && ";
                  }
             }
          }

      	 if ($new > 0) {
              $rs = fn_ejecuta_Add($sqlAddDireccionesStr);
              $sqlCompletoStr .= $sqlAddDireccionesStr." && ";
          }
      	
      	 if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
		          $a['sql'] = $sqlCompletoStr;
              $a['successMessage'] = getDireccionesUpdateMsg($massive);
              $a['id'] = $RQcalleNumero;
		     } else {
              $a['success'] = false;
              $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCompleto;
		     }
		  }
        
      $a['errors'] = $e;
  		$a['successTitle'] = getMsgTitulo();
        
      if(substr($RQtipo, 0, 2) == 'DI' || substr($RQtipo, 0, 2) == 'DE'){
          echo json_encode($a);
      }
	}

    function dltDireccion($RQdireccion){
        //Se usa en addDistribuidor si el distribuidor no se crea bien
        $sqlDltDireccionStr = "DELETE FROM cadireccionestbl ".
                              "WHERE direccion=".$RQdireccion;
        
        $rs = fn_ejecuta_query($sqlDltDireccionStr);
    }

    function dltDireccionesMult(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $idDireccionArr = explode('|', substr($_REQUEST['catDireccionesDireccionHdn'], 0, -1));
        if(in_array('', $idDireccionArr)) {
            $e[] = array('id'=>'catDireccionesDireccionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            for ($nInt=0; $nInt < sizeof($idDireccionArr); $nInt++) { 
                $sqlDltDireccionStr = "DELETE FROM caDireccionesTbl ".
                                      "WHERE direccion=".$idDireccionArr[$nInt];
        
                $rs = fn_ejecuta_query($sqlDltDireccionStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    if (!isset($a['successMessage'])) {
                        $a['successMessage'] = getDireccionesDltMult();
                    }
                    $a['successMessage'] .= $idDireccionArr[$nInt]." ";
                } else {
                    $a['success'] = false;
                    if (!isset($a['errorMessage'])) {
                        $a['errorMessage'] = "No se pudieron borrar las direcciones con ID: ";
                    }
                    $a['errorMessage'] .= $idDireccionArr[$nInt]." ";
                }
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function addUpdateDirecciones(){
        $a = array();
        $a['success'] = true;
        $e = array();

        if($_REQUEST['catDireccionesCalleNumeroHdn'] == ''){
          $e[] = array('id' => 'catDireccionesCalleNumeroHdn', 'msg' => getRequerido());
          $a['errorMessage'] = getErrorRequeridos();
          $a['success'] = false;
        }
        if($_REQUEST['catDireccionesIdColoniaHdn'] == ''){
          $e[] = array('id' => 'catDireccionesIdColoniaHdn', 'msg' => getRequerido());
          $a['errorMessage'] = getErrorRequeridos();
          $a['success'] = false;
        }
        if($_REQUEST['catDireccionesTipoHdn'] == ''){
          $e[] = array('id' => 'catDireccionesTipoHdn', 'msg' => getRequerido());
          $a['errorMessage'] = getErrorRequeridos();
          $a['success'] = false;
        }
        if($_REQUEST['catDireccionesAddUpdHdn'] == ''){
          $e[] = array('id' => 'catDireccionesAddUpdHdn', 'msg' => getRequerido());
          $a['errorMessage'] = getErrorRequeridos();
          $a['success'] = false;
        }

        if($a['success']== true){
          //idDireccion no es requerido si se hace un INSERT
          $idDireccionArr = explode('|', substr($_REQUEST['catDireccionesDireccionHdn'] , 0, -1));

          $distribuidorArr = explode('|', substr($_REQUEST['catDireccionesDistribuidorHdn'] , 0, -1));

          $addUpdArr = explode('|', substr($_REQUEST['catDireccionesAddUpdHdn'] , 0, -1));
          
          if(in_array('', $addUpdArr)){
            $e[] = array('id' => 'catDireccionesAddUpdHdn', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
          }
          $tipoDirArr = explode('|', substr($_REQUEST['catDireccionesTipoHdn'] , 0, -1));
          if(in_array('', $tipoDirArr)){
            $e[] = array('id' => 'catDireccionesTipoHdn', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
          }
          $calleNumeroArr = explode('|', substr($_REQUEST['catDireccionesCalleNumeroHdn'] , 0, -1));
          if(in_array('', $calleNumeroArr)){
            $e[] = array('id' => 'catDireccionesCalleNumeroHdn', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
          }
          $idColoniaArr = explode('|', substr($_REQUEST['catDireccionesIdColoniaHdn'] , 0, -1));
          if(in_array('', $idColoniaArr)){
            $e[] = array('id' => 'catDireccionesIdColoniaHdn', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
          }

          if($a['success'] == true){
            $sqlAddUpdateStr = "";
            
            for($i= 0; $i< sizeof($addUpdArr); $i++){
              if($addUpdArr[$i] == 'A'){
                $sqlAddUpdateStr = "INSERT INTO cadireccionestbl (calleNumero, idColonia, ".
                                   "distribuidor, tipoDireccion) VALUES('".
                                   $calleNumeroArr[$i]."', '".$idColoniaArr[$i]."', ".
                                   replaceEmptyNull("'".$distribuidorArr[$i]."'").", '".$tipoDirArr[$i]."'); ";

                $rs = fn_ejecuta_Add($sqlAddUpdateStr);
              }
              else if($addUpdArr[$i] == 'U'){
                $sqlAddUpdateStr =  "UPDATE cadireccionestbl ".
                                   "SET calleNumero='".$calleNumeroArr[$i]."', ".
                                   "idColonia='".$idColoniaArr[$i]."', ".
                                   "tipoDireccion='".$tipoDirArr[$i]."', ".
                                   "distribuidor=".replaceEmptyNull("'".$distribuidorArr[$i]."'")." ".
                                   "WHERE direccion=".$idDireccionArr[$i]."; ";
              
              $rs = fn_ejecuta_Upd($sqlAddUpdateStr);
              }
              else{//Por si alguna razón se llega enviar otra cosa diferente que 'A' o 'U'
                $e[] = array('id' => 'catDireccionesAddUpdHdn', 'msg' => 'Tipo de Dato NO Esperado');
                $a['errorMessage'] = 'Tipo de Dato NO Esperado: '.$addUpdArr[$i];
                $a['success'] = false;

              }              
            }
            
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = getDireccionesSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = "No se pudieron agregar las direcciones ".$_SESSION['error_sql'];
                $e[] =array('id'=>'algo', 'sql'=>$sqlAddUpdateStr);
            }

          }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        $a['root'][0]['success'] = $a['success'];
        $a['root'][0]['errorMessage'] = $a['errorMessage'];
        $a['root'][0]['successMessage'] = $a['successMessage'];
        echo json_encode($a);   
    }

?>