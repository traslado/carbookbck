<?php
    /************************************************************************
    * Autor: Carlos Alberto Sierra Mayoral
    * Fecha: 27- Agosto -2018
    * Descripción: Programa para Cancelar el estatus de entrega
    *************************************************************************/
    session_start();

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['trCanTalonesHdn']){
        case 'sqlGetDatos':
            sqlGetDatos();
            break;
        case 'addUndTmp':
            addUndTmp();
            break;
        case 'sqlGetTmp':
            sqlGetTmp();
            break;
        case 'modTalon':
            modTalon();
            break;
        case 'dltTmp':
            dltTmp();
            break;
        default:
            echo '';
    }

    $_REQUEST = trasformUppercase($_REQUEST);

    function sqlGetDatos(){
        $sqlDatos = "SELECT tr.tractor, concat(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as nombre,vt.viaje, ".
                    "(SELECT p1.plaza FROM caplazastbl p1 WHERE p1.idPlaza = vt.idPlazaOrigen ) as plazaOrigen, ".
                    "(SELECT p2.plaza FROM caplazastbl p2 WHERE p2.idPlaza = vt.idPlazaDestino ) as plazaDestino,ud.vin, concat(ud.distribuidor,' - ',di.descripcionCentro ) as distribuidorDI,vt.idViajeTractor,tl.idTalon ".
                    "FROM trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trunidadesdetallestalonestbl ts, alultimodetalletbl ud, cadistribuidorescentrostbl di ".
                    "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                    "AND tl.idTalon = ts.idTalon ".
                    "AND ts.vin = ud.vin ".
                    "AND ud.distribuidor = di.distribuidorCentro ".
                    "AND vt.idTractor = tr.idTractor ".
                    "AND vt.claveChofer = ch.claveChofer ".
                    "AND ud.claveMovimiento = 'OM' ".
                    "AND tl.claveMovimiento = 'TE' ".
                    "AND vt.centroDistribucion = '".$_REQUEST['trCanTalonesCentroCmb']."' ".
                    "AND tr.compania = '".$_REQUEST['trCanTalonesCompaniaCmb']."' ".
                    "AND vt.claveMovimiento IN ('VE','VF') ".
                    "AND tl.folio = '".$_REQUEST['trCanTalonesTalonTxt']."';";

        $rs = fn_ejecuta_query($sqlDatos);
        echo json_encode($rs);
    }

    function addUndTmp(){
        $addUnd = "INSERT alunidadestmp (vin, avanzada, modulo, idUsuario, ip, fecha) ".
                    "SELECT ud.vin, substr(ud.vin,10) as avanzada,'Cancelacion Estatus Talon' as modulo, ".$_SESSION['idUsuario']." as idUsuario,'".$_SERVER['REMOTE_ADDR']."' as ip,now() ".
                    "FROM trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trunidadesdetallestalonestbl ts, alultimodetalletbl ud, cadistribuidorescentrostbl di ".
                    "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                    "AND tl.idTalon = ts.idTalon ".
                    "AND ud.distribuidor = di.distribuidorCentro ".
                    "AND ts.vin = ud.vin ".
                    "AND vt.idTractor = tr.idTractor ".
                    "AND vt.claveChofer = ch.claveChofer ".
                    "AND ud.claveMovimiento = 'OM' ".
                    "AND tl.claveMovimiento = 'TE' ".
                    "AND vt.centroDistribucion = '".$_REQUEST['trCanTalonesCentroCmb']."' ".
                    "AND tr.compania = '".$_REQUEST['trCanTalonesCompaniaCmb']."' ".
                    "AND vt.claveMovimiento IN ('VE','VF') ".
                    "AND tl.folio = '".$_REQUEST['trCanTalonesTalonTxt']."';";

        fn_ejecuta_query($addUnd);
    }

    function sqlGetTmp(){
        $sqlGetUnd = "SELECT au.vin, au.distribuidor, au.simboloUnidad, au.color ".
                        "FROM alunidadestmp tmp, alunidadestbl au ".
                        "WHERE tmp.vin = au.vin ".
                        "AND tmp.modulo = 'Cancelacion Estatus Talon';";

        $rs = fn_ejecuta_query($sqlGetUnd);
        echo json_encode($rs);
    }

    function modTalon(){

        $a = array();
        $a['success'] = true;
        $a['msjResponse'] = 'Cancelaci&oacuten efectuada.';
        
        $sqlDatos = "SELECT DISTINCT tl.idTalon, vt.idViajeTractor ".
                    "FROM trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trunidadesdetallestalonestbl ts, alultimodetalletbl ud, cadistribuidorescentrostbl di ".
                    "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                    "AND tl.idTalon = ts.idTalon ".
                    "AND ts.vin = ud.vin ".
                    "AND ud.distribuidor = di.distribuidorCentro ".
                    "AND vt.idTractor = tr.idTractor ".
                    "AND vt.claveChofer = ch.claveChofer ".
                    "AND ud.claveMovimiento = 'OM' ".
                    "and tl.claveMovimiento = 'TE' ".
                    "AND vt.centroDistribucion = '".$_REQUEST['trCanTalonesCentroCmb']."' ".
                    "AND tr.compania = '".$_REQUEST['trCanTalonesCompaniaCmb']."' ".
                    "AND vt.claveMovimiento IN ('VE','VF') ".
                    "AND tl.folio = '".$_REQUEST['trCanTalonesTalonTxt']."';";

        $rsSqlDatos = fn_ejecuta_query($sqlDatos);

        $sqlGetHistorico = "SELECT vin ".
                            "FROM alunidadestmp ".
                            "WHERE modulo = 'Cancelacion Estatus Talon' ".
                            "AND idUsuario = ".$_SESSION['idUsuario'];

        $rs = fn_ejecuta_query($sqlGetHistorico);        

        for ($i=0; $i < sizeof($rs['root']) ; $i++) {

            $sqlGetOM = "SELECT MAX(idHistorico) as numHistorico ".
                        "FROM alhistoricounidadestbl ".
                        "WHERE vin = '".$rs['root'][$i]['vin']."' ".
                        "AND claveMovimiento = 'OM';";

            $rsHistorico = fn_ejecuta_query($sqlGetOM);

            $sqlGetUnd = "SELECT h1.centroDistribucion,h1.vin,h1.fechaEvento,h1.claveMovimiento,h1.distribuidor,h1.idTarifa,h1.claveChofer, h1.localizacionUnidad ".
                          "FROM alhistoricounidadestbl h1 ".
                          "WHERE h1.vin = '".$rs['root'][$i]['vin']."' ".
                          "AND h1.claveMovimiento = 'AM' ".
                          "AND h1.fechaEvento = (SELECT max(h2.fechaevento) FROM alhistoricounidadestbl h2 WHERE h2.vin = h1.vin AND h2.claveMovimiento = h1.claveMovimiento);";
						//echo "<br>$sqlGetUnd<br>";
            $rsGetUnds = fn_ejecuta_query($sqlGetUnd);
            
            $sqlUpdUD = "UPDATE alultimodetalletbl ".
                        "SET centroDistribucion = '".$rsGetUnds['root'][0]['centroDistribucion']."', ".
                        "fechaEvento = '".$rsGetUnds['root'][0]['fechaEvento']."', ".
                        "claveMovimiento = '".$rsGetUnds['root'][0]['claveMovimiento']."', ".
                        "distribuidor = '".$rsGetUnds['root'][0]['distribuidor']."', ".
                        "idTarifa = '".$rsGetUnds['root'][0]['idTarifa']."', ".
                        "claveChofer = '".$rsGetUnds['root'][0]['claveChofer']."', ".
                        "localizacionUnidad = '".$rsGetUnds['root'][0]['localizacionUnidad']."', ".
                        "observaciones = 'Modificado por Can. Est. Talon' ".
                        "WHERE vin = '".$rs['root'][$i]['vin']."';";
						//echo "<br>$sqlUpdUD<br>";
            $rsUpdUD = fn_ejecuta_query($sqlUpdUD);

            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                $a['success'] = false;
                $a['msjResponse'] = $_SESSION['error_sql']."<br>".$sqlUpdUD;
                break;
            }
            
            if($a['success'])
            {
		            $dltUnd = "DELETE FROM alhistoricounidadestbl ".
		                        "WHERE vin = '".$rs['root'][$i]['vin']."' ".
		                        "AND idHistorico = '".$rsHistorico['root'][0]['numHistorico']."' ".
		                        "AND claveMovimiento = 'OM'; ";
								//echo "<br>$dltUnd<br>";		
		            $rsDltUnd = fn_ejecuta_query($dltUnd);               		
		            
		            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
		                $a['success'] = false;
		                $a['msjResponse'] = $_SESSION['error_sql']."<br>".$dltUnd;
		                break;
		            }		            
            }
        }

        if($a['success'])
        {
		        $sqlUpdVT = "UPDATE trviajestractorestbl ".
		                    "SET claveMovimiento = 'VF' ".
		                    "WHERE idViajeTractor = '".$rsSqlDatos['root'][0]['idViajeTractor']."';";
						//echo "<br>$sqlUpdVT<br>";
		        fn_ejecuta_query($sqlUpdVT);        		
		        
            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                $a['success'] = false;
                $a['msjResponse'] = $_SESSION['error_sql']."<br>".$sqlUpdVT;
            }			        
        }

        if($a['success'])
        {
		        $sqlUpdTL = "UPDATE trtalonesviajestbl ".
		                    "SET claveMovimiento = 'TF' ".
		                    "WHERE idViajeTractor = '".$rsSqlDatos['root'][0]['idViajeTractor']."' ".
		                    "AND idTalon = '".$rsSqlDatos['root'][0]['idTalon']."';";
						//echo "<br>$sqlUpdTL<br>";
		        fn_ejecuta_query($sqlUpdTL);        		
		        
            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                $a['success'] = false;
                $a['msjResponse'] = $_SESSION['error_sql']."<br>".$sqlUpdTL;
            }		        
        }

        if($a['success'])
        {
		        $sqlDltTMP = "DELETE FROM alunidadestmp ".
		                    "WHERE modulo = 'Cancelacion Estatus Talon' ".
		                    "AND idUsuario = ".$_SESSION['idUsuario']; 
						//echo "<br>$sqlDltTMP<br>";
		        fn_ejecuta_query($sqlDltTMP);        		
		        
            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                $a['success'] = false;
                $a['msjResponse'] = $_SESSION['error_sql']."<br>".$sqlDltTMP;
            }
        }
        
        echo json_encode($a);

    }

    function dltTmp(){
        $sqlDltTMP = "DELETE FROM alunidadestmp ".
                    "WHERE modulo = 'Cancelacion Estatus Talon';";

        fn_ejecuta_query($sqlDltTMP);
    }
