<?php
    /************************************************************************
    * Autor: Carlos Sierra Mayoral
    * Fecha: 19-Julio-2019
    *************************************************************************/
    session_start();

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    //$_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['susTalonesHdn']){
        case 'getSusTalones':
            getSusTalones();
            break;
        case 'getUnidades':
            getUnidades();
            break;
        case 'addUndTmp':
            addUndTmp();
            break;
        case 'validaUnidad':
            validaUnidad();
            break;
        case 'limpiaGrd':
            limpiaGrd();
            break;
        case 'vaciaGrid':
          vaciaGrid();
          break;
        case 'marcaUnd':
          marcaUnd();
          break;
        case 'addNvoTalon':
          addNvoTalon();
          break;
        default:
            echo '';
    }

    function getSusTalones(){
        $sqlGetTalon = "SELECT tr.tractor, vt.claveChofer,vt.viaje,tl.numeroUnidades,vt.numeroRepartos, ".
                        "(SELECT p1.plaza FROM caplazastbl p1 WHERE p1.idPlaza = vt.idPlazaOrigen ) as plazaOrigen, ".
                        "concat(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as nombre, ".
                        "(SELECT p2.plaza FROM caplazastbl p2 WHERE p2.idPlaza = vt.idPlazaDestino ) as plazaDestino,ud.vin, concat(ud.distribuidor,' - ',di.descripcionCentro ) as distribuidorDI,vt.idViajeTractor,tl.idTalon, ".
                        "(SELECT max(fo.folio + 1) FROM trFoliosTbl fo WHERE fo.centroDistribucion = vt.centroDistribucion AND fo.compania = tr.compania AND fo.tipoDocumento = 'TR' ) as nvoFolio ".
                        "FROM trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trunidadesdetallestalonestbl ts, alultimodetalletbl ud, cadistribuidorescentrostbl di ".
                        "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                        "AND tl.idTalon = ts.idTalon ".
                        "AND ts.vin = ud.vin ".
                        "AND ud.distribuidor = di.distribuidorCentro ".
                        "AND vt.idTractor = tr.idTractor ".
                        "AND vt.claveChofer = ch.claveChofer ".
                        "AND ud.claveMovimiento = 'OM' ".
                        "AND vt.centroDistribucion = '".$_REQUEST['susTalonesCentroCmb']."' ".
                        "AND tr.compania = '".$_REQUEST['susTalonesCompaniaCmb']."' ".
                        "AND tl.folio = '".$_REQUEST['susTalonesFolioTxt']."';";

        $rsGetTalones = fn_ejecuta_query($sqlGetTalon);
        echo json_encode($rsGetTalones);

    }

    function getUnidades(){
        $sqlGetUnds = "SELECT (SELECT CONCAT(d2.distribuidorCentro,' - ', d2.descripcionCentro) FROM cadistribuidorescentrostbl d2 WHERE d2.distribuidorCentro = ud.centroDistribucion ) as centroDistribucion,ud.vin,ud.fechaEvento, ".
                      "(SELECT concat(ud.claveMovimiento,' - ',ge.nombre) FROM cageneralestbl ge WHERE tabla = 'alHistoricoUnidadesTbl' AND ge.columna = 'claveMovimiento' AND ge.valor = ud.claveMovimiento) AS claveMovimiento, CONCAT(ud.distribuidor,' - ',di.descripcionCentro) as distribuidor, ".
                      "(SELECT CONCAT(ta.tarifa,' - ', ta.descripcion) FROM catarifastbl ta WHERE ta.idtarifa = ud.idTarifa) AS tarifa,ud.localizacionUnidad, ".
                      "(SELECT CONCAT(ud.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) FROM cachoferestbl ch WHERE ud.claveChofer = ch.claveChofer) as claveChofer ".
                      "FROM trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trunidadesdetallestalonestbl ts, alhistoricounidadestmp ud, cadistribuidorescentrostbl di ".
                      "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                      "AND tl.idTalon = ts.idTalon ".
                      "AND ts.vin = ud.vin ".
                      "AND ud.distribuidor = di.distribuidorCentro ".
                      "AND vt.idTractor = tr.idTractor ".
                      "AND vt.claveChofer = ch.claveChofer ".
                      "AND ud.claveMovimiento = 'OM' ".
                      "AND vt.centroDistribucion = '".$_REQUEST['susTalonesCentroCmb']."' ".
                      "AND tr.compania = '".$_REQUEST['susTalonesCompaniaCmb']."' ".
                      "AND tl.folio = '".$_REQUEST['susTalonesFolioTxt']."' ".
                      "UNION ".
                      "SELECT (SELECT CONCAT(d2.distribuidorCentro,' - ', d2.descripcionCentro) FROM cadistribuidorescentrostbl d2 WHERE d2.distribuidorCentro = tmp.centroDistribucion ) as centroDistribucion,tmp.vin,tmp.fechaEvento, ".
                      "(SELECT concat(tmp.claveMovimiento,' - ',ge.nombre) FROM cageneralestbl ge WHERE tabla = 'alHistoricoUnidadesTbl' AND ge.columna = 'claveMovimiento' AND ge.valor = tmp.claveMovimiento) AS claveMovimiento, CONCAT(tmp.distribuidor,' - ',di.descripcionCentro) as distribuidor, ".
                      "(SELECT CONCAT(ta.tarifa,' - ', ta.descripcion) FROM catarifastbl ta WHERE ta.idtarifa = tmp.idTarifa) AS tarifa,tmp.localizacionUnidad, ".
                      "(SELECT CONCAT(tmp.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) FROM cachoferestbl ch WHERE tmp.claveChofer = ch.claveChofer) as claveChofer ".
                      "FROM alhistoricounidadestmp tmp, cadistribuidorescentrostbl di ".
                      "WHERE tmp.distribuidor = di.distribuidorCentro ".
                      "AND tmp.claveMovimiento != 'OM';";

                    $rsGetUnds = fn_ejecuta_query($sqlGetUnds);
                    echo json_encode($rsGetUnds);
    }

    function addUndTmp(){
        $sqlAddTmp = "INSERT INTO alhistoricounidadestmp (centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
                        "SELECT ud.centroDistribucion,ud.vin,ud.fechaEvento,ud.claveMovimiento,ud.distribuidor,ud.idTarifa,ud.localizacionUnidad,ud.claveChofer,'sustitucionTalon' as observaciones, ".$_SESSION['idUsuario']." as usuario, '".$_SERVER['REMOTE_ADDR']."' as ip ".
                        "FROM trtalonesviajestbl tl, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trunidadesdetallestalonestbl ts, alultimodetalletbl ud, cadistribuidorescentrostbl di ".
                        "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                        "AND tl.idTalon = ts.idTalon ".
                        "AND ts.vin = ud.vin ".
                        "AND ud.distribuidor = di.distribuidorCentro ".
                        "AND vt.idTractor = tr.idTractor ".
                        "AND vt.claveChofer = ch.claveChofer ".
                        "AND ud.claveMovimiento = 'OM' ".
                        "AND vt.centroDistribucion = '".$_REQUEST['susTalonesCentroCmb']."' ".
                        "AND tr.compania = '".$_REQUEST['susTalonesCompaniaCmb']."' ".
                        "AND tl.folio = '".$_REQUEST['susTalonesFolioTxt']."';";

                        

        fn_ejecuta_query($sqlAddTmp);

        $sqlValidAdd = "SELECT IFNULL(count(vin),0) as numeroUnidades FROM alhistoricounidadestmp WHERE observaciones = 'sustitucionTalon' ";

        $rsVld = fn_ejecuta_query($sqlValidAdd);
        echo json_encode($rsVld);
    }

    function validaUnidad(){

        $sqlGetValida = "SELECT 1 as bandera, ". 
                        "(SELECT ta.tipoTarifa FROM catarifastbl ta WHERE ta.idTarifa = ud.idtarifa) as taUnd, ".
                        "(SELECT ta.tipoTarifa FROM alhistoricounidadestmp tmp, catarifastbl ta WHERE tmp.idTarifa = ta.idTarifa AND tmp.observaciones = 'sustitucionTalon' LIMIT 1) as taTmp ".
                        "FROM alultimodetalletbl ud ".
                        "WHERE vin = '".$_REQUEST['susTalonesVinTxt']."' ".
                        "AND claveMovimiento IN (SELECT valor FROM caGeneralesTbl WHERE tabla ='unidadAsignar') ".
                        "AND centroDistribucion = (SELECT centroDistribucion FROM trTalonesViajesTbl WHERE idTalon = '".$_REQUEST['susTalonesIdTalonHdn']."' ) ".
                        "AND distribuidor = (SELECT distribuidor FROM trtalonesviajestbl WHERE idTalon = '".$_REQUEST['susTalonesIdTalonHdn']."');";

        $rsGetUnds = fn_ejecuta_query($sqlGetValida);        

        if($rsGetUnds['root'][0]['bandera'] == '1' && $rsGetUnds['root'][0]['taUnd'] === $rsGetUnds['root'][0]['taTmp']){
          $sqlAddUnd = "INSERT INTO alhistoricounidadestmp (centroDistribucion, vin, fechaEvento, claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones, usuario,  ip) ".
                        "SELECT centroDistribucion, vin, fechaEvento, claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,'sustitucionTalon' as observaciones, 'nvoUnidad' as usuario, ip ".
                        "FROM alultimodetalletbl ".
                        "WHERE vin = '".$_REQUEST['susTalonesVinTxt']."' ".
                        "AND claveMovimiento IN (SELECT valor FROM caGeneralesTbl WHERE tabla ='unidadAsignar') ".
                        "AND centroDistribucion = (SELECT centroDistribucion FROM trTalonesViajesTbl WHERE idTalon = '".$_REQUEST['susTalonesIdTalonHdn']."' ) ".
                        "AND distribuidor = (SELECT distribuidor FROM trtalonesviajestbl WHERE idTalon = '".$_REQUEST['susTalonesIdTalonHdn']."');";

          $rsAddUnd = fn_ejecuta_query($sqlAddUnd); 

          echo json_encode(true);         
        }else{
          if($rsGetUnds['root'][0]['bandera'] !== '1'){
            echo json_encode('3');
          }else{
            echo json_encode('2');
          }
        }        
    }

    function limpiaGrd(){
           
      $sqlGetTmp = "SELECT * ".
                    "FROM alhistoricounidadestmp ".
                    "WHERE observaciones = 'paraEliminar';";

      $rsEliminar = fn_ejecuta_query($sqlGetTmp);
                  

      for ($i=0; $i < sizeof($rsEliminar['root']) ; $i++) {

        $fechaOM = "SELECT MAX(h2.fechaEvento) as fechaOM FROM alhistoricounidadestbl h2 WHERE h2.vin = '".$rsEliminar['root'][$i]['vin']."' AND h2.claveMovimiento = 'OM';";

        $rsFechaOM = fn_ejecuta_query($fechaOM);
      
        $dltHistoricoOM = "DELETE ".
                          "FROM alhistoricounidadestbl ".
                          "WHERE vin = '".$rsEliminar['root'][$i]['vin']."' ".
                          "AND claveMovimiento = 'OM' ".
                          "AND fechaEvento = '".$rsFechaOM['root'][0]['fechaOM']."';";

        fn_ejecuta_query($dltHistoricoOM);

        $fechaAM = "SELECT MAX(h2.fechaEvento) as fechaAM FROM alhistoricounidadestbl h2 WHERE h2.vin = '".$rsEliminar['root'][$i]['vin']."' AND h2.claveMovimiento = 'AM';";

        $rsFechaAM = fn_ejecuta_query($fechaAM);      

        $dltHistoricoAM = "DELETE ".
                          "FROM alhistoricounidadestbl ".
                          "WHERE vin = '".$rsEliminar['root'][$i]['vin']."' ".
                          "AND claveMovimiento = 'AM' ".
                          "AND fechaEvento = '".$rsFechaAM['root'][0]['fechaAM']."';";

        fn_ejecuta_query($dltHistoricoAM);

        $sqlUD = "SELECT centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ".
                  "FROM alhistoricounidadestbl hu ".
                  "WHERE hu.vin = '".$rsEliminar['root'][$i]['vin']."' ".
                  "AND hu.claveMovimiento IN (SELECT valor FROM cageneralestbl WHERE tabla = 'alHistoricoUnidadesTbl' AND columna = 'validos') ".
                  "AND hu.fechaEvento = (SELECT MAX(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.vin = hu.vin) ;";

        $rsUD = fn_ejecuta_query($sqlUD);

        $updUD =  "UPDATE alultimodetalletbl ".
                    "SET centroDistribucion = '".$rsUD['root'][0]['centroDistribucion']."', ".
                    "fechaEvento = '".$rsUD['root'][0]['fechaEvento']."', ".
                    "claveMovimiento = '".$rsUD['root'][0]['claveMovimiento']."', ".
                    "distribuidor = '".$rsUD['root'][0]['distribuidor']."', ".
                    "idTarifa = '".$rsUD['root'][0]['idTarifa']."', ".
                    "localizacionUnidad = '".$rsUD['root'][0]['localizacionUnidad']."', ".
                    "claveChofer ='".$rsUD['root'][0]['claveChofer']."', ".
                    "observaciones = '".$rsUD['root'][0]['observaciones']."', ". 
                    "usuario = '".$rsUD['root'][0]['usuario']."', ".
                    "ip = '".$rsUD['root'][0]['ip']."' ".
                    "WHERE vin = '".$rsEliminar['root'][$i]['vin']."';";

        fn_ejecuta_query($updUD);

        $updTalon = "UPDATE trunidadesdetallestalonestbl ".
                    "SET estatus = 'CA' ".
                    "WHERE idTalon = '".$_REQUEST['susTalonesIdTalonHdn']."' ".
                    "AND vin = '".$rsEliminar['root'][$i]['vin']."';";
        
      }      
    }

    function vaciaGrid(){
      $sqlDltTmp = "DELETE FROM alhistoricounidadestmp WHERE observaciones IN ('sustitucionTalon','paraEliminar') ".
      						 "AND usuario = ".$_SESSION['idUsuario'].";";

      $rsDltTmp = fn_ejecuta_query($sqlDltTmp);
      echo json_encode($rsDltTmp);
    }

    function marcaUnd(){
      $undTmp = "UPDATE alhistoricounidadestmp ".
                "SET observaciones = 'paraEliminar' ".
                "WHERE vin = '".$_REQUEST['susTalonesVinTxt']."' ".
                "AND usuario = ".$_SESSION['idUsuario'].";";

      $rsDltTmp = fn_ejecuta_query($undTmp);
      echo json_encode($rsDltTmp);
    }

    function addNvoTalon(){      
      $a = array();
      $a['success'] = true;
      $a['msjResponse'] = 'Sustituci&oacuten efectuada.';
      
      $vjoTalon        = $_REQUEST['susTalonesFolioTxt'];
      $nvoTalon        = $_REQUEST['susTalonesNvoFolioDsp'];
      $vjoIdTalon      = $_REQUEST['susTalonesIdTalonHdn'];
      $numIdViaje      = $_REQUEST['susTalonesIdviajeHdn'];
      $cDistribucion   = $_REQUEST['susTalonesCentroCmb'];
      $companiaTractor = $_REQUEST['susTalonesCompaniaCmb'];
      $numeroUnidades  = $_REQUEST['numeroUnidades'];

      // SE INSERTA EL NUEVO TALON
      $sqlAddTalon = "INSERT INTO trtalonesviajestbl (distribuidor, folio, idViajeTractor, companiaRemitente, idPlazaOrigen, idPlazaDestino, direccionEntrega, centroDistribucion, tipoTalon, ". 
                      "fechaEvento, observaciones, numeroUnidades, importe, seguro, tarifaCobrar, kilometrosCobrar, impuesto, retencion, claveMovimiento, tipoDocumento, firmaElectronica) ".
                      "SELECT distribuidor, '".$nvoTalon."' AS folio, idViajeTractor, companiaRemitente, idPlazaOrigen, idPlazaDestino, direccionEntrega, centroDistribucion, tipoTalon, fechaEvento, ". 
                      "observaciones, ${numeroUnidades} AS numeroUnidades, importe, seguro, tarifaCobrar, kilometrosCobrar, ". //(SELECT count(vin) FROM alhistoricounidadestmp WHERE observaciones = 'sustitucionTalon')
                      "impuesto, retencion, claveMovimiento, tipoDocumento, firmaElectronica  FROM trtalonesviajestbl WHERE folio = '".$vjoTalon."' AND idViajeTractor = '".$numIdViaje."';";
			//echo "$sqlAddTalon<br>";      
      fn_ejecuta_query($sqlAddTalon);
      
      if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
					$a['success'] = false;
					$a['sql'] = $sqlAddTalon;
					$a['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del Tal&oacuten.";
			}
			
      if($a['success'])
      {
		      // SE PIDE EL IDTALON DEL NUEVO TALON INSERTADO
		      $sqlGetTalon = "SELECT idTalon FROM trTalonesViajesTbl WHERE folio = '".$nvoTalon."' AND idViajeTractor = '".$numIdViaje."';";
					//echo "$sqlGetTalon<br>";      
		      $rsGetIdTalon = fn_ejecuta_query($sqlGetTalon);      
		      //var_dump($rsGetIdTalon);
		
		      // SE INSERTAN LAS UNIDADES AL NUEVO TALON
		      $addUndTalon = "INSERT INTO trUnidadesDetallesTalonesTbl (idTalon, vin, estatus, imagen01, imagen02, imagen03) ".
		                      "SELECT '".$rsGetIdTalon['root'][0]['idTalon']."' AS idTalon, vin,'OM' AS estatus, null as imagen01, null as imagen02, null as imagen03 ".
		                      "FROM alhistoricounidadestmp ".
		                      "WHERE observaciones = 'sustitucionTalon';";
					//echo "$addUndTalon<br>";      
		      fn_ejecuta_query($addUndTalon);  

		      if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
							$a['success'] = false;
							$a['sql'] = $addUndTalon;
							$a['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del Detalle del Tal&oacuten.";
					}
      }

      if($a['success'])
      {
		      $sqlGetTmp = "SELECT * ".
		                    "FROM alhistoricounidadestmp ".
		                    "WHERE observaciones = 'sustitucionTalon' ".
		                    "AND usuario = 'nvoUnidad';";
					//echo "$sqlGetTmp<br>";      
		      $rsGetTmp = fn_ejecuta_query($sqlGetTmp);
		
		      for ($i=0; $i < sizeof($rsGetTmp['root']);$i++) {
		        $sqlGet = "INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
		                  "SELECT centroDistribucion, '".$rsGetTmp['root'][0]['vin']."' as vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ".
		                  "FROM alhistoricounidadestbl ".
		                  "WHERE vin = (SELECT VIN FROM alhistoricounidadestmp WHERE observaciones = 'sustitucionTalon' AND usuario != 'nvoUnidad' LIMIT 1) ".
		                  "AND claveMovimiento IN('AM','OM') ".
		                  "ORDER BY fechaEvento ASC ".
		                  "LIMIT 2;";
						//echo "$sqlGet<br>";
		        fn_ejecuta_query($sqlGet);

			      if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
								$a['success'] = false;
								$a['sql'] = $sqlGet;								
								$a['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del Hist&oacuterico.";
								break;
						}

			      if($a['success'])
			      {
				        $sqlUD = "SELECT centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip ".
							          "FROM alhistoricounidadestbl hu ".
							          "WHERE hu.vin = '".$rsGetTmp['root'][0]['vin']."' ".
							          "AND hu.claveMovimiento IN (SELECT valor FROM cageneralestbl WHERE tabla = 'alHistoricoUnidadesTbl' AND columna = 'validos') ".
							          "AND hu.fechaEvento = (SELECT MAX(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.vin = hu.vin) ;";
				
				        $rsUD = fn_ejecuta_query($sqlUD);
				
				        $updUD =  "UPDATE alultimodetalletbl ".
				                    "SET centroDistribucion = '".$rsUD['root'][0]['centroDistribucion']."', ".
				                    "fechaEvento = '".$rsUD['root'][0]['fechaEvento']."', ".
				                    "claveMovimiento = '".$rsUD['root'][0]['claveMovimiento']."', ".
				                    "distribuidor = '".$rsUD['root'][0]['distribuidor']."', ".
				                    "idTarifa = '".$rsUD['root'][0]['idTarifa']."', ".
				                    "localizacionUnidad = '".$rsUD['root'][0]['localizacionUnidad']."', ".
				                    "claveChofer ='".$rsUD['root'][0]['claveChofer']."', ".
				                    "observaciones = '".$rsUD['root'][0]['observaciones']."', ". 
				                    "usuario = '".$rsUD['root'][0]['usuario']."', ".
				                    "ip = '".$rsUD['root'][0]['ip']."' ".
				                    "WHERE vin = '".$rsGetTmp['root'][0]['vin']."';";
								//echo "$updUD<br>";	
				        fn_ejecuta_query($updUD);			
				        
					      if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
										$a['success'] = false;
										$a['sql'] = $updUD;								
										$a['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del Ultimo detalle.";
										break;
								}				              		
			      }		
		      }      		
      }      

      if($a['success'])
      {
      		limpiaGrd();
      }
      
      if($a['success'])
      {
		      //SE CANCELA EL TALON ANTERIOR
		      $updTalonVjo = "UPDATE trTalonesViajesTbl SET ".
		                      "claveMovimiento = 'TX' ".
		                      "WHERE idTalon = ".$vjoIdTalon." ".
		                      "AND idViajeTractor = '".$numIdViaje."';";
					//echo "$updTalonVjo<br>";	
		      fn_ejecuta_query($updTalonVjo);    
		      
		      if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
							$a['success'] = false;
							$a['sql'] = $updTalonVjo;								
							$a['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten del Tal&oacuten anterior.";
					}
      }                                 
      
      if($a['success'])
      {
		      //SE CANCELA LAS UNIDADES DEL TALON
		      $updUndTalon = "UPDATE trUnidadesDetallesTalonesTbl SET ".
		                      "estatus = 'CA' ".
		                      "WHERE idTalon = '".$vjoIdTalon."';";
					//echo "$updUndTalon<br>";	
		      fn_ejecuta_query($updUndTalon); 
		      
		      if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
							$a['success'] = false;
							$a['sql'] = $updTalonVjo;								
							$a['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del detalle del Tal&oacuten anterior.";
					}		           		
      }   
      
      if($a['success'])
      {
		      $updFolio = "UPDATE trfoliostbl ".
		                  "SET folio = '".$nvoTalon."' ".
		                  "WHERE centroDistribucion = '".$cDistribucion."' ".
		                  "AND compania = '".$companiaTractor."' ".
		                  "AND tipoDocumento = 'TR';";
					//echo "$updFolio<br>";	
		      fn_ejecuta_query($updFolio);      
		      
		      if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
							$a['success'] = false;
							$a['sql'] = $updFolio;								
							$a['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten del Folio.";
					}				      		
      }

      echo json_encode($a);
    }
