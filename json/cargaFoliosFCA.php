<?php
  session_start();
	
	include_once("../funciones/generales.php");
	include_once("../funciones/construct.php");
	require_once('../funciones/Classes/PHPExcel/IOFactory.php');

  switch($_REQUEST['actionHdn']){
      case 'leeArchivo':
          leeArchivo();
          break;
      case 'addFolios':
          addFolios();
          break;          
      default:
          echo '';
  }

	function leeArchivo(){
		  $a = array();
			$a['success'] 		= true;
			$a['msjResponse'] = "";
			$a['folios'] 		= '';
			
			if(isset($_FILES)) {
				if($_FILES["cargaFoliosFCAArchivoFld"]["error"] > 0){
					$a['success'] = false;
					$a['message'] = $_FILES["cargaFoliosFCAArchivoFld"]["error"];
					$a['msjResponse'] = "Falta seleccionar el archivo." ;
		
				} else {
					$temp_file_name = $_FILES['cargaFoliosFCAArchivoFld']['tmp_name']; 
					$original_file_name = $_FILES['cargaFoliosFCAArchivoFld']['name'];
				  
					// Find file extention 
					$ext = explode ('.', $original_file_name); 
					$ext = $ext [count ($ext) - 1]; 
				 	
					// Remove the extention from the original file name 
					$file_name = str_replace ($ext, '', $original_file_name); 
					$nombre = $file_name . $ext;
				 	
				  $new_name = $_SERVER['DOCUMENT_ROOT'].'/archFoliosRepuve/'. $file_name . $ext;
				  //$new_name = $_SERVER['DOCUMENT_ROOT'].'archFoliosRepuve/foliosRepuve.xlsx';				//CHK solo para pruebas local
					///*
					if (move_uploaded_file($temp_file_name, $new_name)){		
						if (!file_exists($new_name)){
							$a['success'] = false;
							$a['msjResponse'] = "Error al procesar el archivo " . $new_name;
						} else {
							$a['success'] = true;
							$a['msjResponse'] = "Archivo Cargado";
							$a['archivo'] = $file_name . $ext;
							$a = leerXLS($new_name);
						}
				 	} else { 
						$a['success'] = false;
						$a['msjResponse'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name.'. Favor de avisar a Sistemas.';
					}
					//*/
					//$a = leerXLS($new_name);																														//CHK solo para pruebas local
				}
			} else {
				$a['success'] = false;
				$a['msjResponse'] = "Error al leer el archivo.";
			}
			
			echo json_encode($a);			
	}
	
	function leerXLS($nombreArch){
		  $a = array();
		  $a['success'] 		= true;
			$a['msjResponse'] = "Archivo cargado.<br>";
			$a['folios'] 		= '';
			$error = 0;
			
		  date_default_timezone_set('America/Mexico_City');
		
			$ruta = "../../archFoliosRepuve/";
			$rutaRespal = "../../respaldoarchFoliosRepuve/";
			//var_dump($nombreArch);

		  try {
		      $inputFileType 	= PHPExcel_IOFactory::identify($nombreArch);
		      $objReader 			= PHPExcel_IOFactory::createReader($inputFileType);
		      $objPHPExcel = $objReader->load($nombreArch);
		  } catch(Exception $e) {
		      $a['success'] = false;
		      $a['msjResponse'] = "Error al leer el archivo ".pathinfo($inputFileName,PATHINFO_BASENAME);
		  }
		  
		  if($a['success'])
		  {
				  $sheet = $objPHPExcel->getSheet(0);
				  $highestRow = $sheet->getHighestRow();
				  $highestColumn = $sheet->getHighestColumn();
				
				  //Se obtiene el valor de cada registro de la columna "A" sin tomar en cuenta el cabecero
				  $rowData 	= $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);
	        $j = 0;
	        //var_dump($rowData);	        
	        for($i = 0; $i<sizeof($rowData); $i++){
	        		$folio = $rowData[$i][0];
	        		$cDist = $rowData[$i][1];
	        		if(!empty($folio))
	        		{
									$encontro = false;
									foreach($a['folios']['root'] as $item)
									{
											if(isset($item['folio']) && $item['folio'] == $folio && isset($item['centroDistribucion']) && $item['centroDistribucion'] == $cDist)
											{
													$encontro = true;
													break;
											}
									}
									if(!$encontro)
									{
			        				$a['folios']['root'][$j]['folio'] = $folio;
			        				$a['folios']['root'][$j]['centroDistribucion'] = $cDist;
			        				$j++;											
									}
	        		}
	        }				   		  		
		  }
			//var_dump($a['folios']);	        		  
		  if($j > 0)
		  {
        	$k = 0;
        	foreach($a['folios']['root'] as $item)
					{
							$folio = $item['folio'];
	        		$cDist = $item['centroDistribucion'];

			        $sql = "SELECT m.descripcion AS descrMarca, ".
			        			 "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE d.distribuidorCentro = '".$cDist."') AS descrCentroDist ".
                     "FROM caMarcasUnidadesTbl m ".
		        				 "WHERE m.marca = 'CD'";   
			        //echo "$sql<br>";
			        $rsCat = fn_ejecuta_query($sql);
	        		
			        //Busca si existe el folio			        
			        $sql = "SELECT r.folioRepuve FROM alRepuveTbl r ".
		        				 "WHERE r.folioRepuve = '".$folio."' ".
                      "AND r.marca = 'CD' ".
                      "AND r.centroDistribucion = '".$cDist."'";
			        //echo "$sql<br>";
			        $rs = fn_ejecuta_query($sql);
			        if($rs['records'] > 0)
			        {
			        		$a['folios']['root'][$k]['folioRepuve'] 				= $folio;
			        		$a['folios']['root'][$k]['marca'] 							= 'CD';
			        		$a['folios']['root'][$k]['descrMarca'] 					= 'CD - '.$rsCat['root'][0]['descrMarca'];
			        		$a['folios']['root'][$k]['centroDistribucion'] 	= $cDist;		        		
			        		$a['folios']['root'][$k]['descrCentroDist'] 		= $cDist." - ".$rsCat['root'][0]['descrCentroDist'];	        		
			        		$a['folios']['root'][$k]['valido'] 							= 0;
			        		$a['folios']['root'][$k]['error'] 							= 'Folio ya registrado.';
					        if(empty($rsCat['root'][0]['descrMarca']))
					        {
			        				$a['folios']['root'][$k]['descrMarca'] = 'CD - NO EXISTE LA MARCA';
					        }	
					        if(empty($rsCat['root'][0]['descrCentroDist']))
					        {
			        				$a['folios']['root'][$k]['descrCentroDist'] = $cDist.' - NO EXISTE EL CENTRO DE DISTRIBUCION';
					        }
					        $error++;		        			        		
			        }
			        else
			        {			        		
			        		$a['folios']['root'][$k]['folioRepuve'] 				= $folio;
			        		$a['folios']['root'][$k]['marca'] 							= 'CD';
			        		$a['folios']['root'][$k]['descrMarca'] 					= 'CD - '.$rsCat['root'][0]['descrMarca'];
			        		$a['folios']['root'][$k]['centroDistribucion'] 	= $cDist;		        		
			        		$a['folios']['root'][$k]['descrCentroDist'] 		= $cDist." - ".$rsCat['root'][0]['descrCentroDist'];	        		
			        		$a['folios']['root'][$k]['valido'] 							= 1;
			        		$a['folios']['root'][$k]['error'] 							= '';
					        if(empty($rsCat['root'][0]['descrMarca']))
					        {
			        				$a['folios']['root'][$k]['descrMarca'] = 'CD - NO EXISTE LA MARCA';
					        }	
					        if(empty($rsCat['root'][0]['descrCentroDist']))
					        {
			        				$a['folios']['root'][$k]['descrCentroDist'] = $cDist.' - NO EXISTE EL CENTRO DE DISTRIBUCION';
					        }
			        }
			        $k++;
					}
					if($error > 0)
					{
							for($i=0; $i<$j; $i++)
							{
									$arrValidos[$i]['valido'] = $a['folios']['root'][$i]['valido'];
									$arrCDist[$i]['centroDistribucion'] = $a['folios']['root'][$i]['centroDistribucion'];
									$arrFolio[$i]['folioRepuve'] = $a['folios']['root'][$i]['folioRepuve'];
							}
							//var_dump($arrValidos);
							array_multisort($arrValidos,SORT_DESC,$arrCDist,SORT_ASC,$arrFolio,SORT_ASC,$a['folios']['root']);	
							$a['msjResponse'] .= " Existen Folios ya registrados.";
					}
		  }
		  else
		  {
		      $a['success'] = false;
		      $a['msjResponse'] = "<b>No existen folios. Favor de revisar el archivo de excel.</b>";		  		
		  }
		  $a['folios']['records'] = $j;
		  //var_dump($a['folios']['root']);	
		  
			//copy($nombreArch, $rutaRespal.'verificacion'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
			unlink($nombreArch);
		
			return $a;		
}

function addFolios()
{
    $a = array();
    $a['success'] = true;
    $a['successMessage'] = '';

    $arrFolios = json_decode($_POST['arrFolios'],true);						//CHK
   	//$arrFolios = json_decode($_GET['arrFolios'],true);					//CHK
    //var_dump($arrFolios);
    foreach($arrFolios as $index => $row)
		{
        //var_dump($row);
        $sql = "INSERT INTO alRepuveTbl VALUES('".
        				$row['folioRepuve']."', '".
        				$row['marca']."', '".
        				$row['centroDistribucion']."', ".
        				"NULL, NULL, NULL, NULL, NULL, NULL)";
				//echo "$sql<br>";
				fn_ejecuta_query($sql);

	      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
	          $a['success']       = false;
	          $a['errorMessage']   = $_SESSION['error_sql']. " En la inserci&oacuten del Folio.";
	          $a['sql']           = $sql;
	          break;
	      }
	      else {
            $a['successMessage'] = 'Folios cargados correctamente.';
        }
    } 

    $a['successTitle'] = 'Carga de Folios REPUVE';

	  echo json_encode($a);
}
?>