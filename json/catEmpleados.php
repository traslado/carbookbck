<?php

    session_start();
	$_SESSION['modulo'] = "catEmpleados";
    
    require("../funciones/generales.php");
    require("../funciones/construct.php");

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['catEmpleadosActionHdn']){
        case 'getEmpleados':
            getEmpleados();
            break;
        default:
            echo '';
    }


    function getEmpleados(){

        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catEmpleadosClaveEmpleadoHdn'], "claveEmpleado", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catEmpleadosNombreHdn'], "nombres", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catEmpleadosApellidoPaternoHdn'], "apellidoPaterno", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        
        $sqlGetEmpleadosStr = "SELECT * FROM caEmpleadosTbl ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetEmpleadosStr);

        echo json_encode($rs);
    }
?>