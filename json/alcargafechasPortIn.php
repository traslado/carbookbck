<?php
    session_start();
	
	//include 'alCargaArchivoLzc.php';
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");
    require_once("../procesos/iREC.php");
	
    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["portInArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["portInArchivoFld"]["error"];

		} else {
			$temp_file_name = $_FILES['portInArchivoFld']['tmp_name']; 
			$original_file_name = $_FILES['portInArchivoFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
		  
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLS($new_name);
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

    function leerXLS($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) < 4 && $highestRow > 2 ) {
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) <= 2) {
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17) {
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN-FECHA]', 'fail'=>'Y');
			return $root;
		}
		//------------------------------------------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
							'FECHA'=>array('val'=>'','size'=>true,'exist'=>true));
		
		for($row = 0; $row<sizeof($rowData); $row++){
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['VINN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['VINN']['val'] = $rowData[$row][0];
			$isValidArr['FECHA']['val'] = $rowData[$row][1];
			$isValidArr['FECHA']['size'] = strlen($rowData[$row][1]) == 19;
			//$isValidArr['Simbolo']['size'] = strlen($rowData[$row][1]) > 0;
			//$isValidArr['Distribuidor']['size'] = strlen($rowData[$row][2]) == 5;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {
				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}
				if($rowData[$row][0] == null) {
					$errorMsg .='EL '.$key.' esta vacio|';
					echo json_encode("VIN".$rowData[$row][0]);
					$isTrue = false;
				}
				if($rowData[$row][1] == null) {
					$errorMsg .='La '.$key.' esta vacia|';
					echo json_encode("FECHA".$rowData[$row][1]);
					$isTrue = false;
				}else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						if (!$isValidArr[$key]['exist']) {

							 	if ($key == 'VIN') {
							 		$siNo = ' No ha sido precargado en Desembarque Unidades|';
							 	}

							 	 if ($key == 'VINN') {
							 		$siNo = ' Ya cuenta con transmision|';
							 	}
							 	if ($key == 'FECHA') {
							 		$siNo = ' no tiene formato correcto (YYYY-MM-DD HH:MM:SS)|';
							 	}
						
							$errorMsg .= 'El '.$key.$siNo;
							$isTrue = false; 
			
						}
					}
				}
			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
				$_SESSION['usuCompania'] = 'LZC02';
				

				$numVin = $rowData[$row][0];
				$fechaEvento = $rowData[$row][1];


				$addHist = "UPDATE alHistoricoUnidadesTbl set fechaEvento='".$fechaEvento."' , ".
							"observaciones='UNIDAD INGRESO L1 LC' where vin='".$numVin."' and claveMovimiento='L1' ";
					fn_ejecuta_query($addHist);	

				    $updUldet="SELECT centroDistribucion,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip 
						FROM alhistoricounidadestbl hu 
						WHERE  hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) 
                      							FROM alhistoricounidadestbl hu2 
                      							WHERE hu2.vin = hu.vin 
                      							AND hu2.claveMovimiento not in (SELECT ge.valor 
                                                  FROM cageneralestbl ge 
                                                  WHERE ge.tabla = 'alHistoricoUnidadesTbl' 
                                                  AND ge.columna = 'nvalidos')) 
												AND  hu.vin = '".$numVin."'  ";
				
				$rsupdUldet	= fn_ejecuta_query($updUldet);	

				if ($rsupdUldet['root'][$i]['claveChofer'] == null)
		{

			$claveChofer= "null";
		}
		else 
		{
			$claveChofer=$rsupdUldet['root'][$i]['claveChofer'];

		}

			$sqlUpdUnd = "UPDATE alultimodetalletbl ".
						"SET centroDistribucion= '".$rsupdUldet['root'][0]['centroDistribucion']."', ".
						"fechaEvento = '".$rsupdUldet['root'][0]['fechaEvento']."', ".
						"claveMovimiento = '".$rsupdUldet['root'][0]['claveMovimiento']."', ".
						"distribuidor = '".$rsupdUldet['root'][0]['distribuidor']."', ".
						"idTarifa = '".$rsupdUldet['root'][0]['idTarifa']."', ".
						"localizacionUnidad = '".$rsupdUldet['root'][0]['localizacionUnidad']."', ".
						"claveChofer =".$claveChofer.",".
						"usuario = '".$rsupdUldet['root'][0]['usuario']."', ".
						"observaciones = '".$rsupdUldet['root'][0]['observaciones']."', ".
						"ip = '".$rsupdUldet['root'][0]['ip']."' ".
						"WHERE vin = '".$rsupdUldet['root'][0]['vin']."'";

					fn_ejecuta_query($sqlUpdUnd);
            
                 
					
				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					$errorMsg = 'Agregado Correctamente';
					
				} else {
					$errorMsg = 'Registro No Agregado';
				}		
			}
			$root[]= array('vin'=>$rowData[$row][0],'fecha'=>$rowData[$row][1],'nose'=>$errorMsg);	
				

			//echo json_encode($rowData[$row]);
		}
		return $root;

	}


	function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
		$tabla = 'tabla';
		$columna = 'columna';
		switch(strtoupper($field)) {
			case 'VIN':
					$sqlExist = "SELECT vin FROM alHistoricoUnidadesTbl WHERE vin = '".$value."' and claveMovimiento='L1'";
					$rs1 = fn_ejecuta_query($sqlExist);

					if (sizeof($rs1['root']) > 0) {
						
						$rs=0;
					}
					else
					{
						$rs=1;

					}	
		    break;
			case 'FECHA':


           if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/",$value)) 
           	{ 
           		$rs=1;
           	}
           	else
             { 
           		$rs=0;
              } 

		    break;

		    case 'VINN':

				$sqlExist = "SELECT vin FROM alHistoricoUnidadesTbl WHERE vin = '".$value."' and claveMovimiento='L1' and vin in (select vin from altransaccionunidadtbl where tipoTransaccion='REC')";

				$rs1 = fn_ejecuta_query($sqlExist);

				if (sizeof($rs1['root']) > 0) {
						
						$rs=0;
					}
					else
					{
						$rs=1;

					}	
				
			break;

		}

		return strtoupper($field) == 'VIN' ? !($rs > 0) : ($rs > 0);
}		

?>