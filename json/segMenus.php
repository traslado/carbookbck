<?php
	session_start();
	$_SESSION['modulo'] = "segMenusDetalle";
    require("../funciones/generales.php");
    require("../funciones/construct.php");

    switch($_REQUEST['segMenusDetalleActionHdn']){
        case 'getArbolMenu':
        	getArbolMenu();
        	break;
        default:
            echo '';
    }

    function getArbolMenu(){
    	//$lsWhereStr = "WHERE m.idModulo = md.idModulo ";

    	$lsWhereStr = "ORDER BY 5 ";

	    $sqlGetMenuDetalleStr = "SELECT md.*,m.tipoModulo, m.nombreMenuArbol, m.modulo, 0 as alta, 0 as baja,0 as cambio,0 as consulta,0 as ejecutar,0 as reporte,0 as habilitada , m.icono ".
                                "FROM segMenusDetalleTbl md, sisModulosTbl m ".
                                "WHERE m.idModulo = md.idModulo ".
                                "AND md.idModulo not in (SELECT idModulo ".
                                                        "FROM segUsuariosModulosTbl  ".
                                                        "WHERE idUsuario =". $_REQUEST['segUsuariosModulosIdUsuarioHdn'] ." ) ".
                                "UNION ". 
                                "SELECT md.*, m.tipoModulo ,m.nombreMenuArbol, m.modulo,um.alta,um.baja,um.cambio,um.consulta,um.ejecutar,um.reporte, 1 as habilitada, m.icono ".
                                "FROM segMenusDetalleTbl md, sisModulosTbl m ,segUsuariosModulosTbl um ".
                                "WHERE m.idModulo = md.idModulo ".
                                "AND md.idModulo = um.idModulo ".
                                "AND um.idUsuario = " . $_REQUEST['segUsuariosModulosIdUsuarioHdn'] ." ".
                                "AND md.idModulo in (SELECT idModulo ".
                                                    "FROM segUsuariosModulosTbl ".
                                                    "WHERE idUsuario =". $_REQUEST['segUsuariosModulosIdUsuarioHdn'] .") ".
                                $lsWhereStr;  
		//echo "$sqlGetMenuDetalleStr<br>";
		$rs = fn_ejecuta_query($sqlGetMenuDetalleStr);
		$menu = $rs['root'];
        $tree = array();
		for ($iInt=0; $iInt < sizeof($menu); $iInt++) { 
			if ($menu[$iInt]['tipoModulo'] == 0) {
				$tree['text'] = $menu[$iInt]['nombreMenuArbol'];
                $tree["expanded"] = "true";
                $tree["iconCls"]=$menu[$iInt]['icono'];
                $tree['claveMenu'] = $menu[$iInt]['modulo'];
                $tree['id'] = $menu[$iInt]['idModulo'];
                $tree["alta"] = $menu[$iInt]['alta'];
                $tree["baja"] = $menu[$iInt]['baja'];
                $tree["cambio"] = $menu[$iInt]['cambio'];
                $tree["consulta"] = $menu[$iInt]['consulta'];
                $tree["ejecutar"] = $menu[$iInt]['ejecutar'];
                $tree["habilitada"] = $menu[$iInt]['habilitada'];
                $tree["reporte"] = $menu[$iInt]['reporte'];
				$tree['children'] = array();
			} elseif ($menu[$iInt]['tipoModulo'] == 1) {
				$tempChildOne = array("id"=>$menu[$iInt]['idModulo'],
                                    "text"=>$menu[$iInt]['nombreMenuArbol'],
                                    "iconCls"=>$menu[$iInt]['icono'],
                                    "claveMenu"=>$menu[$iInt]['modulo'],
                                    "alta" => $menu[$iInt]['alta'],
                                    "baja" => $menu[$iInt]['baja'],
                                    "cambio" => $menu[$iInt]['cambio'],
                                    "consulta" => $menu[$iInt]['consulta'],
                                    "ejecutar" => $menu[$iInt]['ejecutar'],
                                    "habilitada" => $menu[$iInt]['habilitada'],
                                    "reporte" => $menu[$iInt]['reporte']
                                    );
				for ($jInt=0; $jInt < sizeof($menu); $jInt++) {
					if ($menu[$jInt]['idModuloPadre'] != 1 && ($menu[$jInt]['idModuloPadre'] == $menu[$iInt]['idModulo'])) {
						if (!isset($tempChildOne['children'])) {
							$tempChildOne['children'] = array(); 							
						}
						if ($menu[$jInt]['tipoModulo'] == 2) {
							array_push($tempChildOne['children'],array("id"=>$menu[$jInt]['idModulo'],
                                                                        "text"=>$menu[$jInt]['nombreMenuArbol'],
                                                                        "iconCls"=>$menu[$jInt]['icono'],
                                                                        "claveMenu"=>$menu[$jInt]['modulo'],
                                                                        "alta" => $menu[$jInt]['alta'],
                                                                        "baja" => $menu[$jInt]['baja'],
                                                                        "cambio" => $menu[$jInt]['cambio'],
                                                                        "consulta" => $menu[$jInt]['consulta'],
                                                                        "ejecutar" => $menu[$jInt]['ejecutar'],
                                                                        "reporte" => $menu[$jInt]['reporte'],
                                                                        "habilitada" => $menu[$jInt]['habilitada'],
                                                                        "children"=>checkLevelTwo($menu, $jInt)));
						} else {
                            array_push($tempChildOne['children'],array("id"=>$menu[$jInt]['idModulo'],
                                                                        "leaf"=>'true',
                                                                        "iconCls"=>$menu[$jInt]['icono'],
                                                                        'expandable'=>false,
                                                                        "text"=>$menu[$jInt]['nombreMenuArbol'],
                                                                        "claveMenu"=>$menu[$jInt]['modulo'],
                                                                        "alta" => $menu[$jInt]['alta'],
                                                                        "baja" => $menu[$jInt]['baja'],
                                                                        "cambio" => $menu[$jInt]['cambio'],
                                                                        "consulta" => $menu[$jInt]['consulta'],
                                                                        "ejecutar" => $menu[$jInt]['ejecutar'],
                                                                        "reporte" => $menu[$jInt]['reporte'],
                                                                        "habilitada" => $menu[$jInt]['habilitada'],
                                                                        ));
						}
					}	
				}
				//echo "-".json_encode($tempChildOne)."<br>";
				array_push($tree['children'], $tempChildOne);
			} elseif ($menu[$iInt]['tipoModulo'] == 3 && $menu[$iInt]['idModuloPadre'] == 1) {
				array_push($tree['children'], array("id"=>$menu[$iInt]['idModulo'],
                                                    "text"=>$menu[$iInt]['nombreMenuArbol'],
                                                    "iconCls"=>$menu[$iInt]['icono'],
                                                    "alta" => $menu[$iInt]['alta'],
                                                    "baja" => $menu[$iInt]['baja'],
                                                    "cambio" => $menu[$iInt]['cambio'],
                                                    "consulta" => $menu[$iInt]['consulta'],
                                                    "ejecutar" => $menu[$iInt]['ejecutar'],
                                                    "reporte" => $menu[$iInt]['reporte'],
                                                    "habilitada" => $menu[$iInt]['habilitada'],
                                                    "claveMenu"=>$menu[$iInt]['modulo']
                                                    ));
			}
		}
			
		echo json_encode(array($tree));
    }

    function checkLevelTwo($menu, $parent){
    	$temp = array();
    	for ($iInt=0; $iInt < sizeof($menu); $iInt++) { 
    		if ($menu[$iInt]['idModuloPadre'] == $menu[$parent]['idModulo']) {
    			//echo $menu[$iInt]['nombreMenuArbol']."<br>";
    			if ($menu[$iInt]['tipoModulo'] == 2) {
    				array_push($temp, array("id"=>$menu[$iInt]['idModulo'],
                                            "text"=>$menu[$iInt]['nombreMenuArbol'],
                                            "iconCls"=>$menu[$iInt]['icono'],
                                            "claveMenu"=>$menu[$iInt]['modulo'],
                                            "alta" => $menu[$iInt]['alta'],
                                            "baja" => $menu[$iInt]['baja'],
                                            "cambio" => $menu[$iInt]['cambio'],
                                            "consulta" => $menu[$iInt]['consulta'],
                                            "ejecutar" => $menu[$iInt]['ejecutar'],
                                            "reporte" => $menu[$iInt]['reporte'],
                                            "habilitada" => $menu[$iInt]['habilitada'],
                                            "claveMenu"=>$menu[$iInt]['modulo'],
                                            "children"=>checkLevelTwo($menu, $iInt)
                                            ));
    			} else {
    				array_push($temp, array("id"=>$menu[$iInt]['idModulo'],
                                            "text"=>$menu[$iInt]['nombreMenuArbol'],
                                            "iconCls"=>$menu[$iInt]['icono'],
                                            "claveMenu"=>$menu[$iInt]['modulo'],
                                            "alta" => $menu[$iInt]['alta'],
                                            "baja" => $menu[$iInt]['baja'],
                                            "cambio" => $menu[$iInt]['cambio'],
                                            "consulta" => $menu[$iInt]['consulta'],
                                            "ejecutar" => $menu[$iInt]['ejecutar'],
                                            "reporte" => $menu[$iInt]['reporte'],
                                            "habilitada" => $menu[$iInt]['habilitada'],
                                            "claveMenu"=>$menu[$iInt]['modulo'],
                                            "leaf"=>'true',
                                            'expandable'=>false
                                            ));
    			}
    		}
    	}
    	//echo "2: ".json_encode($temp)."<br>";
    	return $temp;
    }
?>