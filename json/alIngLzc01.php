<?php

	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");

	switch($_REQUEST['alIngresoUnidadesLzcHdn']){
		case 'addLazaroL1':
			addLazaroL1();
			break;        
	     case 'addLazaroL2':
	        addLazaroL2(); 
	        break;
	    case 'addLazaroL3':
	        addLazaroL3(); 
	        break;
	     case 'getLogin':
	        getLogin();
	        	break;   
	     case 'comboVines':
	        comboVines();
	        	break;  
	     case 'PDI':
	        PDI();
	        	break;   
	     case 'comboVinesL2':
	        comboVinesL2();
	        	break; 
	     case 'comboPDI':
	        comboPDI();
	        	break; 
	    default:
	        echo '';
	}

	function addLazaroL1(){

		$sqlGetUnidad = "SELECT vin FROM alUltimoDetalleTbl WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ";
		$rsSqlGetUnidad= fn_ejecuta_Add($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] != '0'){
			
			echo '0|Unidad Ya Existente|';

		}else{
			
			$getConsultaIM = "SELECT modelDesc,cveDisFac ".
							"FROM alinstruccionesmercedestbl ".
							"WHERE VIN = '".$_REQUEST['alIngresoVinHdn']."' ";

			$rsgetConsultaIM = fn_ejecuta_Add($getConsultaIM);

			$disIM = $rsgetConsultaIM['root'][[0]['cveDisFac'];
			$simIM = $rsgetConsultaIM['root'][[0]['modelDesc'];


			$sqlAddUnidadStr =  "INSERT INTO alUnidadesTbl ".
			                                    "(vin, avanzada, distribuidor, simboloUnidad, color, folioRepuve, descripcionUnidad) ".
			                                    "VALUES(".
			                                    "'".$_REQUEST['alIngresoVinHdn']."',".
			                                    "'".substr($_REQUEST['alIngresoVinHdn'], -8)."',".
			                                    "'".$disIM."',".
			                                    "'".$simIM."',".
			                                    "'PBR',".
			                                    "(SELECT folioRepuve FROM alRepuveTbl WHERE vin = '".$_REQUEST['alIngresoVinHdn']."'),".
			                                    replaceEmptyNull("'LZC02_ingreso01'").")";

	        $rs_01 = fn_ejecuta_Add($sqlAddUnidadStr);


		    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
		                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
		                                "claveChofer, observaciones, usuario, ip) ".
		                                "VALUES(".
		                                "'LZC02',".
		                                "'".$_REQUEST['alIngresoVinHdn']."',".
		                                "NOW(),".
		                                "'L1',".
		                                "'".$disIM."',".
		                                "'1',".
		                                "'LZC02',".
		                                replaceEmptyNull($RQchofer).",".
		                                "'UNIDAD INGRESO L1',".
		                                "'98',".
		                                "'".getClientIP()."')";
		    
		    $rs_02 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);     

		    $sqlAddUpdUltimoDetalleStr = "INSERT INTO alUltimoDetalleTbl ".
		                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
		                                "claveChofer, observaciones, usuario, ip) ".
		                                "VALUES(".
		                                "'LZC02',".
		                                "'".$_REQUEST['alIngresoVinHdn']."',".
		                                "NOW(),".
		                                "'L1',".
		                                "'".$disIM."',".
		                                "'1',".
		                                "'LZC02',".
		                                replaceEmptyNull($RQchofer).",".
		                                "'UNIDAD INGRESO L1',".
		                                "'98',".
		                                "'".getClientIP()."')";
		    
		    $rs_03 = fn_ejecuta_Add($sqlAddUpdUltimoDetalleStr);

		    echo "1||||";
		}	     	
	}

	function addLazaroL2(){
	    
	/*	$sqlGetUnidad = "SELECT vin FROM alinstruccionesmercedestbl dy ".
							"WHERE dy.cveStatus='DR' ".
							"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ".
							"AND dy.vin in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='L1')";
		$rsSqlGetUnidad= fn_ejecuta_Add($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] == '0'){
			
			echo json_encode($rsSqlGetUnidad);

		}else{*/
		    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
		                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
		                                "claveChofer, observaciones, usuario, ip) ".
		                                "VALUES(".
		                                "'LZC02',".
		                                "'".$_REQUEST['alIngresoVinHdn']."',".
		                                "NOW(),".
		                                "'L2',".
		                                "'DIPRU',".
		                                "'1',".
		                                "'LZC02',".
		                                replaceEmptyNull($RQchofer).",".
		                                "'UNIDAD INGRESO L2',".
		                                "'98',".
		                                "'".getClientIP()."')";
		    
		    $rs_01 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);

		    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
		    							"SET fechaEvento = NOW(), ".
		    							"claveMovimiento = 'L2' ".
		    							"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
		    							"AND claveMovimiento = 'L1' ";

		    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);

		   	echo "1||||";


		//	echo json_encode($rsSqlGetUnidad);
		//}    	
	}

function addLazaroL3(){
	    
	/*	$sqlGetUnidad = "SELECT vin FROM alinstruccionesmercedestbl dy ".
							"WHERE dy.cveStatus='DR' ".
							"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ".
							"AND dy.vin in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='L1')";
		$rsSqlGetUnidad= fn_ejecuta_Add($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] == '0'){
			
			echo json_encode($rsSqlGetUnidad);

		}else{*/
		    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
		                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
		                                "claveChofer, observaciones, usuario, ip) ".
		                                "VALUES(".
		                                "'LZC02',".
		                                "'".$_REQUEST['alIngresoVinHdn']."',".
		                                "NOW(),".
		                                "'L3',".
		                                "'DIPRU',".
		                                "'1',".
		                                "'LZC02',".
		                                replaceEmptyNull($RQchofer).",".
		                                "'UNIDAD SALIDA TREN L3',".
		                                "'98',".
		                                "'".getClientIP()."')";
		    
		    $rs_01 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);

		    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
		    							"SET fechaEvento = NOW(), ".
		    							"claveMovimiento = 'L3' ".
		    							"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
		    							"AND claveMovimiento = 'L2' ";

		    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);

		   	echo "1||||";


		//	echo json_encode($rsSqlGetUnidad);
		//}    	
	}

	
	function getLogin(){

		$psw = md5($_REQUEST['lzcLoginPwdTxt']);

		$getUsr = "SELECT 1 as valor FROM segusuariostbl ".
					"WHERE usuario ='".$_REQUEST['lzcLoginUsrTxt']."' ".
					"AND password ='".$psw."'";
		$rsUsr =fn_ejecuta_query($getUsr);
		echo json_encode($rsUsr);	   

	}

	function comboVines(){
		$sqlInstrucciones ="SELECT concat(dy.natType,"."  '  vin: ' " .",dy.vin) as descripcion, vin   FROM alinstruccionesmercedestbl dy ".
							"WHERE dy.cveStatus='DR' ".
							"AND vin not in (SELECT ud.vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin)";
		$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
		echo json_encode($rsInstrucciones);
	}

	function comboVinesL2(){
		$sqlInstrucciones ="SELECT concat(dy.natType,"."  '  vin: ' " .",dy.vin) as descripcion, vin   FROM alinstruccionesmercedestbl dy ".
							"WHERE dy.cveStatus='DR' ".
							"AND vin in (SELECT ud.vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin and ud.claveMovimiento = 'L1')";
		$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
		echo json_encode($rsInstrucciones);
	}

	function comboPDI(){
		$sqlInstrucciones ="SELECT concat(dy.natType,"."  '  vin: ' " .",dy.vin) as descripcion, vin   FROM alinstruccionesmercedestbl dy ".
							"WHERE dy.cveStatus='DR' ".
							"AND dy.vin in (SELECT ud.vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='L2') ".
							"AND dy.vin not in (SELECT al.vin FROM alhistoricounidadestbl al WHERE dy.vin = al.vin AND claveMovimiento='PDI') ";
		$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
		echo json_encode($rsInstrucciones);
	}


	function PDI(){
		$sqlGetUnidad = "SELECT vin FROM alinstruccionesmercedestbl dy ".
							"WHERE dy.cveStatus='DR' ".
							"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ".
							"AND dy.vin in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='L2')";
		$rsSqlGetUnidad= fn_ejecuta_Add($sqlGetUnidad);

		if($rsSqlGetUnidad['records'] == '0'){
			
			echo json_encode($rsSqlGetUnidad);

		}else{
			 $sqlAddPDI = "INSERT INTO alHistoricoUnidadesTbl ".
			                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
			                                "claveChofer, observaciones, usuario, ip) ".
			                                "VALUES(".
			                                "'LZC02',".
			                                "'".$_REQUEST['alIngresoVinHdn']."',".
			                                "NOW(),".
			                                "'PDI',".
			                                "'DIPRU',".
			                                "'1',".
			                                "'LZC02',".
			                                replaceEmptyNull($RQchofer).",".
			                                "'UNIDAD INGRESO PDI',".
			                                "'98',".
			                                "'".getClientIP()."')";
			    
			$rs = fn_ejecuta_Add($sqlAddPDI);

			echo json_encode($rsSqlGetUnidad);
		}
	}

?>