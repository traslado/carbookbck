<?php

/*********************************************
Sólo necesita reciboGastosIdViajeTractor para funcionar
*********************************************/
require_once("../funciones/fpdf/fpdf.php");
require_once("../funciones/generales.php");
require_once("../funciones/construct.php");
require_once("../funciones/utilidades.php");
require_once("../funciones/funcionesGlobales.php");
require_once("trGastosViajeTractor.php");

switch($_SESSION['idioma']){
    case 'ES':
        include_once("../funciones/idiomas/mensajesES.php");
        break;
    case 'EN':
        include_once("../funciones/idiomas/mensajesEN.php");
        break;
    default:
        include_once("../funciones/idiomas/mensajesES.php");
}
$a = array();
$e = array();
$gFolio = $_REQUEST['folio'];
    
$a['success'] = true;
if($_REQUEST['reciboGastosIdViajeTractorHdn'] == "" && $_REQUEST['reciboGastosTractorHdn'] == ""){
    $e[] = array('id'=>'reciboGastosIdViajeTractorHdn o reciboGastosTractorHdn','msg'=>getRequerido());
    $a['success'] = false;
}

$pdf = new FPDF('P', 'mm', array(100, 150));

if ($a['success']) {
    $viaje = array();

    if($_REQUEST['reciboGastosIdViajeTractorHdn'] == ""){
        $sqlGetUltimoViajeTractor = "SELECT MAX(vt.viaje) AS viaje,vt.idViajeTractor, null AS idViajeAcomp ".
                                    "FROM trViajesTractoresTbl vt ".
                                    "WHERE vt.idTractor =".$_REQUEST['reciboGastosTractorHdn']." ".
                                    "AND vt.viaje = (SELECT MAX(viaje) FROM trviajestractorestbl ".
                                                    "WHERE idTractor=".$_REQUEST['reciboGastosTractorHdn'].") ";

        $rs = fn_ejecuta_query($sqlGetUltimoViajeTractor);
        //echo json_encode($sqlGetUltimoViajeTractor);

        if($rs['records'] > 0){
            array_push($viaje,$rs['root'][0]['idViajeTractor']);

            if(isset($rs['root'][0]['idViajeAcomp']) && $rs['root'][0]['idViajeAcomp'] != "")
                array_push($viaje, $rs['root'][0]['idViajeAcomp']);
        } else {
            $a['errorMessage'] = "Error al obtener viaje del tractor ". $_REQUEST['reciboGastosTractorHdn'];
            $a['errors'] = $e;
            $a['success'] = false;
            echo json_encode($a);
        }
    } else {
        array_push($viaje, $_REQUEST['reciboGastosIdViajeTractorHdn']);
    }

    if($a['success']){
        foreach ($viaje as $idViaje) {
            $sqlGetDataStr = "SELECT tv.idViajeTractor,  tv.claveChofer, cc.apellidoPaterno,cc.apellidoMaterno,cc.nombre, ".
                            "tv.viaje,ct.tractor, ct.compania, tv.claveMovimiento, ".
                            "${gFolio} AS folio, ".
                            "(SELECT plaza FROM caplazastbl cp WHERE tv.idPlazaOrigen = cp.idPlaza) AS plazaOrigen, ".
                            "(SELECT plaza FROM caplazastbl cp WHERE tv.idPlazaDestino = cp.idPlaza) AS plazaDestino, ".
                            "tv.kilometrosTabulados, ct.rendimiento, tv.numeroRepartos, tv.numeroUnidades, ".
                            "(SELECT count(*)FROM trtalonesviajestbl tt WHERE tt.idViajeTractor = tv.idViajeTractor) AS talonesTotal, ".
                            "(SELECT valor FROM cageneralestbl where tabla = 'trgastosviajetractortbl' AND columna = 'ISO') AS ISO ".
                            "FROM trViajesTractoresTbl tv, caChoferesTbl cc, caTractoresTbl ct ". 
                            "WHERE tv.claveChofer = cc.claveChofer ".
                            "AND tv.idTractor = ct.idTractor ".
                            "AND tv.idViajeTractor = ".$idViaje;

            $rs = fn_ejecuta_query($sqlGetDataStr);

            $data = $rs['root'][0];

            if (sizeof($data) > 0) {
                $pdf = generarPdf($idViaje, $data, $pdf, $nInt); 
            }
        }

        $pdf->Output('RECIBO DE '.$data['claveChofer'].' - '.$data['apellidoPaterno'].' '.$data['apellidoMaterno'].' '.$data['nombre'].'.pdf', 'I');
    }
} else {
    $a['errorMessage'] = getErrorRequeridos();
    $a['errors'] = $e;
    echo json_encode($a);
}

function generarPdf($viaje, $data, $pdf, $n){
    global $gFolio;
    //Solo para pruebas, por defecto 0
    /* $lsWhereStr = "";
    if($_REQUEST['CveMovHdn'] != "" && $_REQUEST['CveMovHdn'] == "GM"){
        $lsWhereStr = "AND tg.claveMovimiento = 'GM' AND tg.fechaEvento = ".
                      "(SELECT MAX(tg3.fechaEvento) FROM trGastosViajeTractorTbl tg3) AND tg.claveMovimiento = 'GM'";

        $lsWhereStr2 = "AND tg2.claveMovimiento = 'GM' AND tg2.fechaEvento = ".
                      "(SELECT MAX(tg3.fechaEvento) FROM trGastosViajeTractorTbl tg3) AND tg2.claveMovimiento = 'GM'";
    }

    $sqlGetCalculosGastosComplementosStr = "SELECT tg.concepto AS id, tg.observaciones, ".
                                        "(SELECT tg2.importe FROM trgastosviajetractortbl tg2 ".
                                            "WHERE tg2.idViajeTractor = tg.idViajeTractor AND tg2.concepto = tg.concepto AND tg2.claveMovimiento <> 'GX' $lsWhereStr2 ) AS cantidad, ".
                                          "(SELECT nombre FROM caconceptostbl cc WHERE cc.concepto = tg.concepto) AS DescConcepto ". 
                                          "FROM trgastosviajetractortbl tg ".
                                          "WHERE tg.idViajeTractor = ".$viaje." ".$lsWhereStr.
                                          " GROUP BY tg.concepto;";

    
    $rsConcepto = fn_ejecuta_query($sqlGetCalculosGastosComplementosStr);
    */
    if( $_REQUEST['CveMovHdn'] == "GM"){

        $sqlGetCalculosGastosComplementosStr = "SELECT tg.concepto AS id, tg.observaciones, ".
                "(SELECT tg2.importe FROM trgastosviajetractortbl tg2 ".
                    "WHERE tg2.idViajeTractor = tg.idViajeTractor AND tg2.concepto = tg.concepto ".
                    "AND tg2.claveMovimiento = 'GM' and tg2.folio = ${gFolio}) AS cantidad, ".
                "(SELECT nombre FROM caconceptostbl cc WHERE cc.concepto = tg.concepto) AS DescConcepto ".
                "FROM trgastosviajetractortbl tg ".
                "WHERE tg.idViajeTractor = ".$viaje." ".
                "AND tg.folio = ${gFolio} ".
                "GROUP BY tg.concepto";

        $rsConcepto = fn_ejecuta_query($sqlGetCalculosGastosComplementosStr);
    }
    else
    {
        /*$sqlGetCalculosGastosComplementosStr = "SELECT tg.concepto AS id, tg.observaciones, ".
                "(SELECT tg2.importe FROM trgastosviajetractortbl tg2 ".
                "WHERE tg2.idViajeTractor = tg.idViajeTractor AND tg2.concepto = tg.concepto AND tg2.claveMovimiento <> 'GX' $lsWhereStr2 ) AS cantidad, ".
                "(SELECT nombre FROM caconceptostbl cc WHERE cc.concepto = tg.concepto) AS DescConcepto ". 
                "FROM trgastosviajetractortbl tg ".
                "WHERE tg.idViajeTractor = ".$viaje." ".
                " GROUP BY tg.concepto;";

        $rsConcepto = fn_ejecuta_query($sqlGetCalculosGastosComplementosStr);*/

        $sqlGetCalculosGastosComplementosStr = "SELECT * FROM trconceptosdieselbombastbl tg WHERE tg.idViajeTractor = ".$viaje." AND fechaRegistro=(SELECT MAX(fechaRegistro) FROM trconceptosdieselbombastbl where idViajeTractor=".$viaje.");";

        $rsConcepto = fn_ejecuta_query($sqlGetCalculosGastosComplementosStr);

        
    }

    $border = 0;

    if(sizeof($data) > 0){
        $pdf->AddPage();
        $pdf->SetMargins(0.2, 0.2, 0.2, 0.2);
        if ($n > 0) {
            $pdf->SetY(10);
            $pdf->SetX(10);
        }
        //Etiqueta dependiendo de la clave Movimiento
        //echo print_r($data);
        //return;
        ///echo $data['claveMovimiento'];
         $pdf->setY(5);
              $pdf->SetX(0);
              $pdf->SetFont('Arial','',5);
              $pdf->Cell(0,10,utf8_decode('RECIBO CARGA DE DIESEL EN BOMBAS'), $border,0, 'C');
        
        //numeroTalon  
        $pdf->setY(5);
        $pdf->SetX(0);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(93,10,$rsConcepto['root'][0]['folio'], $border,0, 'R');  
        //FechaOriginal
        $pdf->setY(8);
        $pdf->SetX(0);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(93,10,date("d/m/Y H:i:s"), $border,0, 'R');  

        //OPERADOR
        $pdf->SetY(12);
        $pdf->SetX(5);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'OPERADOR:   '.$data['claveChofer'].' - '.$data['apellidoPaterno'].' '.$data['apellidoMaterno'].' '.
        $data['nombre'], $border,1, 'L');

        //TRACTOR
        $pdf->SetY(12);
        $pdf->SetX(60);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'TRACTOR:   '.$data['tractor'], $border,1, 'L');

        //VIAJE
        $pdf->SetY(12);
        $pdf->SetX(84);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'VIAJE:   '.$data['viaje'], $border,1, 'L');

        //ORIGEN
        $pdf->SetY(15);
        $pdf->SetX(5);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'ORIGEN:   '.$data['plazaOrigen'], $border,1, 'L');

        //DESTINO
        $pdf->SetY(15);
        $pdf->SetX(40);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'DESTINO:   '.$data['plazaDestino'], $border,1, 'L');

        //KILOMETROS TABULADOS
        $pdf->SetY(15);
        $pdf->SetX(81);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'KMS:   '.$data['kilometrosTabulados'], $border,1, 'L');

        //KILOMETROS TABULADOS  
        $pdf->SetY(18);
        $pdf->SetX(5);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'RENDIMIENTO:   '.$data['rendimiento'], $border,1, 'L');

        //NUMERO DE REPARTOS
        $pdf->SetY(18);
        $pdf->SetX(30);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'NO. REPARTOS:   '.$data['numeroRepartos'], $border,1, 'L');

        //NUMERO DE TALONES
        $pdf->SetY(18);
        $pdf->SetX(53);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'NO. TALONES:   '.$data['talonesTotal'], $border,1, 'L');

        //NUMERO DE UNIDADES
        $pdf->SetY(18);
        $pdf->SetX(76);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'NO. UNIDADES:   '.$data['numeroUnidades'], $border,1, 'L');
        /**
         * CONCEPTOS
         */
        $y = 19;
        $x = 8;
        //for ($nInt=0; $nInt < sizeof($concepto); $nInt++) { 
        $totalImporte = 0;
        /*for ($i=0; $i < sizeof($rsConcepto['root']); $i++){
            if($rsConcepto['root'][$i]['id'] != '7001' && $rsConcepto['root'][$i]['id'] != '7000'){
                $totalImporte += $rsConcepto['root'][$i]['cantidad'];
                
                if($i == 3 || $i == 6){
                    $x = $x+34;
                    $y = 20;
                }
                $y = $y+3;
                $pdf->SetY($y);
                $pdf->SetX($x);
                $pdf->SetFont('Arial','',5);
                if($rsConcepto['root'][$i]['id'] != '2315'){    
                    $pdf->Cell(25,8,$rsConcepto['root'][$i]['DescConcepto'], 0,0, 'L');
                }else{
                    $pdf->Cell(25,8,$rsConcepto['root'][$i]['DescConcepto'].': '.$rsConcepto['root'][$i]['observaciones'].'  lts.', 0,0, 'L');
                }
                $pdf->Cell(18,8,'$'.$rsConcepto['root'][$i]['cantidad'], 0,0, 'R');
            }
        }*/
        $pdf->SetY(34);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'LTS VIAJE:  '.$rsConcepto['root'][0]['litrosViaje'].'  lts.', 0,0, 'L');

        $pdf->SetY(37);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'POLIZA PENDIENTE:  '.$rsConcepto['root'][0]['polizaPendiente'].'  lts.', 0,0, 'L');

        $pdf->SetY(40);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'AUT TALLER:  '.$rsConcepto['root'][0]['autTaller'].'  lts.', 0,0, 'L');

        $pdf->SetY(43);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'ACAPULCO OAX:  '.$rsConcepto['root'][0]['acapulcoOax'].'  lts.', 0,0, 'L');

        $pdf->SetY(46);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'REGRESO CARGADOS:  '.$rsConcepto['root'][0]['regresoCargados'].'  lts.', 0,0, 'L');

        $pdf->SetY(49);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'RESGUARDOS:  '.$rsConcepto['root'][0]['resguardos'].'  lts.', 0,0, 'L');

         $pdf->SetY(52);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'POSTURA:  '.$rsConcepto['root'][0]['postura'].'  lts.', 0,0, 'L');

         $pdf->SetY(55);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'1.5 MES:  '.$rsConcepto['root'][0]['1.5Mes'].'  lts.', 0,0, 'L');

         $pdf->SetY(58);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'LIB. MEXIQUENSE:  '.$rsConcepto['root'][0]['libMexiquense'].'  lts.', 0,0, 'L');

         $pdf->SetY(61);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'TOTAL LITROS:  '.$rsConcepto['root'][0]['litrosTotal'].'  lts.', 0,0, 'L');

         $pdf->SetY(61);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'TOTAL LITROS:  '.$rsConcepto['root'][0]['litrosTotal'].'  lts.', 0,0, 'L');

         $pdf->SetY(61);
        $pdf->SetX(12);
        $pdf->Cell(25,8,'TOTAL LITROS:  '.$rsConcepto['root'][0]['litrosTotal'].'  lts.', 0,0, 'L');


        //TOTAL KILOMETROS
        /*$TotalKms = $data['kilometrosTabulados'] ;
        $TotalKms = number_format($TotalKms, 2, ".", ",");
        $pdf->SetY(70);
        $pdf->SetX(10);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'RECORRIDO TOTAL: '.$TotalKms.' Kms.', $border,1, 'L');*/


       

        //FIRMAS
        //CAPUTURADOR
        $pdf->SetY(90);
        $pdf->SetX(12);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'----------------------------------------------', $border,1, 'L');
        $pdf->SetY(92);
        $pdf->SetX(12);
        $pdf->Cell(40,10,$_SESSION['nombreUsr'] , $border,1, 'L');
        //OPERADOR
        $pdf->SetY(90);
        $pdf->SetX(60);
        $pdf->SetFont('Arial','',5);    
        $pdf->Cell(40,10,'----------------------------------------------', $border,1, 'L');
        $pdf->SetY(92);
        $pdf->SetX(64);
        $pdf->Cell(40,10,'FIRMA DEL OPERADOR' , $border,1, 'L');
        

        return $pdf;
    } else {
      echo json_encode(array('success'=>false, 'errorMessage'=>$_SESSION['error_sql']." <br> ".$sqlGetDataStr));
    }

       
}
?>