<?php

	require_once("../funciones/fpdf/pdf_js.php");
	require_once("../funciones/fpdf/fpdf.php");

	class PDF_AutoPrint extends PDF_JavaScript
	{
	function AutoPrint($dialog=false,$server='10.1.2.232', $printer= 'B-EX4T1')
	{
		//Open the print dialog or start printing immediately on the standard printer
		$param=($dialog ? 'true' : 'false');
		$script="print($param);";
		$this->IncludeJS($script);
		$script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
		$script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
		$script .= "print(pp);";
		$this->IncludeJS($script);
	}

	function AutoPrintToPrinter($server='10.1.2.232', $printer= 'B-EX4T1', $dialog=false)
	{
		//Print on a shared printer (requires at least Acrobat 6)
		$script = "var pp = getPrintParams();";
		if($dialog)
			$script .= "pp.interactive = pp.constants.interactionLevel.full;";
		else
			$script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
		$script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
		$script .= "print(pp);";
		$this->IncludeJS($script);
	}
	}

	$pdf=new PDF_AutoPrint();
	$pdf->AddPage();
	$pdf->SetFont('Arial','',20);
	$pdf->Text(90, 50, 'Print me!');
	//Open the print dialog
	$pdf->AutoPrint(false);
	$pdf->Output();

	/*$file1='archivo.pdf';
	header('Content-type:application/pdf');
	header('Content-Description:inline:filename"'.$file1.'"');
	header('Content-Transfer-Encoding:binary');
	header('Accept-Ranges:bytes');
	@readfile($file1);*/


?>