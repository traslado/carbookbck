<?php
    /**
    * </>: ~/02/2020
    * @category Polizas
    * @package Complemento de Póliza
    */
    setlocale(LC_TIME , 'es_MX.UTF-8');
    $gfecha = strftime("%Y-%m-%d %H:%M:%S");

    if (!isset($_SESSION)) {
        session_start();
    }
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['complementoActionHdn']) {
        case 'validaExistencia':
            validaExistencia();
            break;
        case 'sqlGetConsulta':
            sqlGetConsulta();
            break;
        case 'getDatosComprobacion':
            getDatosComprobacion();
            break;
        case 'getDatosPoliza':
            getDatosPoliza();
            break;
        case 'montoLavada':
            montoLavada();
            break;
        case 'getFacturasPoliza':
            getFacturasPoliza();
            break;
        case 'getTalonesVeracruzMacheteros':
            getTalonesVeracruzMacheteros();
            break;
        case 'addFacturaGastos':
            addFacturaGastos();
            break;
        case 'dltFacturaGastos':
            dltFacturaGastos();
            break;
        case 'dltFacturasViaje':
            dltFacturasViaje();
            break;
        case 'dltViajeTmp':
            dltViajeTmp();
            break;
        case 'addPoliza':
            addPoliza();
            break;
        case 'generaPoliza':
            generaPoliza();
            break;
    }

    function validaExistencia(){

        $sqlValidaFolio = "SELECT idViajeTractor from trgastosviajetractortbl ". 
                          "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                          "AND folio = '".$_REQUEST['complementoPolizaFolioTxt']."' ".
                          "AND CAST(fechaEvento AS date) = '".$_REQUEST['complementoPolizaFechaDt']."' ".
                          "AND claveMovimiento = 'GP' ".
                          "limit 1";
        $rsSqlValidaFolio = fn_ejecuta_query($sqlValidaFolio);
        echo json_encode($rsSqlValidaFolio);    
    }

    function sqlGetConsulta() {
        $sqlGetFolio = "SELECT vt.*, ch.apellidoPaterno, ch.apellidoMaterno, ch.nombre, ch.centroDistribucionOrigen, tr.tractor, ".
                        "COUNT(tv.idTalon) AS numeroTalones, SUM(tv.numeroUnidades) AS totalUnidades, ".
                        "tr.rendimiento, tr.compania, ".
                        "(SELECT COUNT(*) FROM trTalonesViajesTbl tv ".
                            "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                            "AND tv.claveMovimiento != 'TX') AS numTalonesValidosViaje, ".
                        "(SELECT co.descripcion FROM caCompaniasTbl co WHERE co.compania = tr.compania) AS nombreCompania, ".
                        "(SELECT tr.tractor FROM caTractoresTbl tr WHERE tr.idTractor = vt.idTractor) AS tractor, ".
                        "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='trViajesTractoresTbl' ".
                        "AND cg.columna='claveMovimiento' AND cg.valor = vt.claveMovimiento) AS nombreClaveMov, ".
                        "(SELECT COUNT(*) FROM trTalonesViajesTbl tv ".
                            "WHERE tv.idViajeTractor = vt.idViajeTractor) AS numeroTalones, ".
                        "(SELECT COUNT(DISTINCT tv.distribuidor) FROM trTalonesViajesTbl tv ".
                            "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                            "AND tv.claveMovimiento != 'TX') AS numDistribuidorValidos, ".
                        "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = vt.idPlazaOrigen) AS nombrePlazaOrigen, ".
                        "(SELECT pl2.plaza FROM caPlazasTbl pl2 WHERE pl2.idPlaza = vt.idPlazaDestino) AS nombrePlazaDestino, ".
                        "(SELECT 1 FROM trviajestractorestbl vt1, trgastosviajetractortbl gt WHERE vt1.idViajeTractor = gt.idViajeTractor AND vt1.claveMovimiento = 'VU' AND gt.claveMovimiento = 'GP' AND vt1.idViajeTractor = vt.idViajeTractor LIMIT 1) as polizaTotal, ".
                        "(SELECT SUM(subtotal) FROM  trgastosviajetractortbl gvt, caGeneralesTbl gen WHERE gvt.concepto = gen.valor and gen.tabla = 'comprobacionPoliza' and gen.columna = '".$_SESSION['usuCompania']."' AND gvt.idViajeTractor = gt.idViajeTractor AND gvt.folio = gt.folio AND gvt.claveMovimiento='GP') AS totCompGtos, ".
                        "(SELECT subtotal FROM  trgastosviajetractortbl  WHERE concepto='7012' AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento='GP') AS totIVAAcreditable, ".
                        "(SELECT subtotal FROM  trgastosviajetractortbl  WHERE concepto='7025' AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento='GP') AS totOperadores, ".
                        "(SELECT subtotal FROM  trgastosviajetractortbl  WHERE concepto='7013' AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento='GP') AS totCargoOpe, ".
                        "0 AS totComprobado, 0 AS totCargos,".
                        "(SELECT SUM(importe) FROM trGastosViajeTractorTbl WHERE concepto IN ('7009','7016','7015','7023','7024') AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento != 'XP') AS totAnticipo,".
                        "(SELECT subtotal FROM  trgastosviajetractortbl  WHERE concepto='7014' AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento='GP') AS totPagoEfectivo, ".
                        "0 AS totAbonos ".
                        "FROM trgastosviajetractortbl gt, catractorestbl tr, caChoferesTbl ch, trviajestractorestbl vt ".
                        "LEFT JOIN trtalonesviajestbl tv ON tv.idViajeTractor = vt.idViajeTractor ".
                        "WHERE  gt.folio = '".$_REQUEST['complementoPolizaFolioTxt']."' ".
                        "AND cast(gt.fechaEvento as date) = '".$_REQUEST['complementoPolizaFechaDt']."' ".
                        "AND gt.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                        "AND vt.idTractor = tr.idTractor ".
                        "AND vt.claveChofer = ch.claveChofer ".
                        "AND gt.claveMovimiento IN ('GP','GS') ".
                        "AND gt.idViajeTractor = vt.idViajeTractor ".
                        "LIMIT 1;";
        $rsGetFolio = fn_ejecuta_query($sqlGetFolio);

        if ($rsGetFolio['records'] > 0) {
            $rsGetFolio['idCombustible'] = 0;
            $rsGetFolio['concepto'] = '';
            $rsGetFolio['litros'] = 0;
            $selStr = "SELECT * FROM trCombustiblePendienteTbl ".
                      "WHERE claveChofer = ".$rsGetFolio['root'][0]['claveChofer']." AND claveMovimiento = 'CP'";
            $combRs = fn_ejecuta_query($selStr);
            if ($combRs['records'] > 0) {
                $rsGetFolio['idCombustible'] = $combRs['root'][0]['idCombustible'];
                $rsGetFolio['concepto'] = $combRs['root'][0]['concepto'];
                $rsGetFolio['litros'] = $combRs['root'][0]['litros'];
            }

            $rsGetFolio['root'][0]['fechaEvento'] = date('Y-m-d', strtotime($rsGetFolio['root'][0]['fechaEvento']));
            $rsGetFolio['root'][0]['descCompania'] = $rsGetFolio['root'][0]['compania']." - ".$rsGetFolio['root'][0]['nombreCompania'];
            $rsGetFolio['root'][0]['numeroDistribuidores'] = $rsGetFolio['root'][0]['numDistribuidorValidos'];
            //KILOMETROS ADICIONALES (COMPROBADOS - TABULADOS)
            $kmAdicionales = floatval($rsGetFolio['root'][0]['kilometrosComprobados']) - floatval($rsGetFolio['root'][0]['kilometrosTabulados']);
            $rsGetFolio['root'][0]['kilometrosAdicionales'] =  $kmAdicionales < 0 ? 0 : $kmAdicionales;
            //Obtiene y concatena todos los talones del viaje
            $sqlGetTalonesViajeStr = "SELECT folio FROM trTalonesViajesTbl ".
                                     "WHERE claveMovimiento != 'TX' ".
                                     "AND idViajeTractor = ".$rsGetFolio['root'][0]['idViajeTractor'];
            $rsTalones = fn_ejecuta_query($sqlGetTalonesViajeStr);
            $temp = "";
            for ($mInt=0; $mInt < sizeof($rsTalones['root']); $mInt++) {
                if ($mInt != 0) {
                  $temp .= "-";
                }
                $temp .= $rsTalones['root'][$mInt]['folio'];
            }
            $rsGetFolio['root'][0]['foliosTalonesViaje'] = $temp;

            $rsGetFolio['root'][0]['totCompGtos'] = floatval($rsGetFolio['root'][0]['totCompGtos']);
            $rsGetFolio['root'][0]['totIVAAcreditable'] = floatval($rsGetFolio['root'][0]['totIVAAcreditable']);
            $rsGetFolio['root'][0]['totOperadores'] = floatval($rsGetFolio['root'][0]['totOperadores']);
            $rsGetFolio['root'][0]['totCargoOpe'] = floatval($rsGetFolio['root'][0]['totCargoOpe']);
            $rsGetFolio['root'][0]['totAnticipo'] = floatval($rsGetFolio['root'][0]['totAnticipo']);
            $rsGetFolio['root'][0]['totPagoEfectivo'] = floatval($rsGetFolio['root'][0]['totPagoEfectivo']);
            $rsGetFolio['root'][0]['totComprobado'] = $rsGetFolio['root'][0]['totCompGtos'] + $rsGetFolio['root'][0]['totIVAAcreditable'];
            $rsGetFolio['root'][0]['totCargos'] = number_format($rsGetFolio['root'][0]['totComprobado'] + $rsGetFolio['root'][0]['totCargoOpe'],2,'.',',');
            $rsGetFolio['root'][0]['totAbonos'] = number_format($rsGetFolio['root'][0]['totAnticipo'] + $rsGetFolio['root'][0]['totPagoEfectivo'],2,'.',',');
            $rsGetFolio['root'][0]['totCompGtos'] = number_format($rsGetFolio['root'][0]['totCompGtos'],2,'.',',');
            $rsGetFolio['root'][0]['totIVAAcreditable'] = number_format($rsGetFolio['root'][0]['totIVAAcreditable'],2,'.',',');
            $rsGetFolio['root'][0]['totOperadores'] = number_format($rsGetFolio['root'][0]['totOperadores'],2,'.',',');
            $rsGetFolio['root'][0]['totPagoEfectivo'] = number_format($rsGetFolio['root'][0]['totPagoEfectivo'],2,'.',',');
            $rsGetFolio['root'][0]['totAnticipo'] = number_format($rsGetFolio['root'][0]['totAnticipo'],2,'.',',');
            $rsGetFolio['root'][0]['totCargoOpe'] = number_format($rsGetFolio['root'][0]['totCargoOpe'],2,'.',',');
            $rsGetFolio['root'][0]['totComprobado'] = number_format($rsGetFolio['root'][0]['totComprobado'],2,'.',',');
        }

        echo json_encode($rsGetFolio);
    }

    function getDatosComprobacion(){
        $lsWhereStr = "WHERE ge.tabla = 'comprobacionPoliza' ".
                      "AND ge.columna = cc.centroDistribucion ".
                      "AND ge.valor = cc.concepto ".
                      "AND cc.concepto = co.concepto ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['centroDistribucionHdn'], "cc.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['conceptoHdn'], "gt.concepto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetDatosComprobacion = "SELECT cc.concepto,co.nombre, ".
                                   "concat(SUBSTRING(cc.cuentaContable, 1,13),'.',LPAD((SELECT ta.tractor ".
                                   "FROM catractorestbl ta ".
                                   "WHERE idTractor = (SELECT vt.idTractor ".
                                   "FROM trviajestractorestbl vt ".
                                   "WHERE vt.idViajeTractor = ".$_REQUEST['idViajeTractorHdn'].")),4,0),'.',(SELECT vt.claveChofer ".
                                   "FROM trviajestractorestbl vt ".
                                   "WHERE vt.idViajeTractor = ".$_REQUEST['idViajeTractorHdn'].")) as cuentaContable ".
                                   "FROM caGeneralesTbl ge, caConceptosCentrosTbl cc, caConceptosTbl co ".$lsWhereStr.
                                   "ORDER BY ge.estatus;";
        $rs = fn_ejecuta_query($sqlGetDatosComprobacion);

        for($i = 0; $i < sizeof($rs['root']); $i++){
            $rs['root'][$i]['subtotal'] = '0.00';
            $rs['root'][$i]['iva'] = '0.00';
            $rs['root'][$i]['total'] = '0.00';
        }

        $sqlGetAnticipos = "SELECT gv.concepto, gv.importe ".
                           "FROM trGastosViajeTractorTbl gv ".
                           "WHERE gv.idViajeTractor = ".$_REQUEST['idViajeTractorHdn']." ".
                           "AND (gv.claveMovimiento = 'GP' OR gv.claveMovimiento = 'GM') ";
        $rsAnticipo = fn_ejecuta_query($sqlGetAnticipos);
        $anticiposArr = array();

        for($i = 0; $i < sizeof($rsAnticipo['root']); $i++){
            if(!isset($anticiposArr[$rsAnticipo['root'][$i]['concepto']])){
                $anticiposArr[$rsAnticipo['root'][$i]['concepto']] = $rsAnticipo['root'][$i]['importe'];
            } else {
                $anticiposArr[$rsAnticipo['root'][$i]['concepto']] += $rsAnticipo['root'][$i]['importe'];
            }
        }

        for($i = 0; $i < sizeof($rs['root']); $i++){
            if(isset($anticiposArr[$rs['root'][$i]['concepto']])) {
                $rs['root'][$i]['anticipo'] = $anticiposArr[$rs['root'][$i]['concepto']];
            }
            else {
                $rs['root'][$i]['anticipo'] = '0.00';
            }
        }

        echo json_encode($rs);
    }

    function getDatosPoliza() {
        $sqlGetDatosPoliza = "SELECT (SELECT cc.importe FROM caConceptosCentrosTbl cc ".
                            "WHERE cc.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                            "AND cc.concepto = ".$_REQUEST['conceptoHdn'].") AS importe";
        $rs = fn_ejecuta_query($sqlGetDatosPoliza);

        echo json_encode($rs);
    }

    function montoLavada(){
        $sqlGetMonto = "SELECT * FROM cageneralestbl ".
                        "WHERE tabla = 'trPolizaGastos' ".
                        "AND columna = '".$_REQUEST['montoConcepto']."'";
        $rsGetMonto = fn_ejecuta_query($sqlGetMonto);

        echo json_encode($rsGetMonto);
    }

    function getFacturasPoliza(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trcomplementoPolizaFolioFiscalHdn'], "fp.folioFiscal", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if (isset($_REQUEST['trcomplementoPolizaFolioRFCHdn'])) {
            $lsCondicionStr = fn_construct($_REQUEST['trcomplementoPolizaFolioRFCHdn'], "fp.rfc", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        $lsWhereStr .= "AND folio != '".$_REQUEST['folioHdn']."'";

        $sqlGetFacturasStr = "SELECT fp.* ".
                             "FROM trPolizaFacturasTbl fp ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetFacturasStr);

        echo json_encode($rs);
    }

    function getTalonesVeracruzMacheteros(){
        $sqlConsultaTalones = " SELECT * FROM trtalonesViajesTbl where idViajeTractor='".$_REQUEST['trcomplementoPolizaIdViajeHdn']."' and centroDistribucion='CDVER'";
        $rsConsultaTalones = fn_ejecuta_query($sqlConsultaTalones);
        echo json_encode($rsConsultaTalones);
    }

    function addFacturaGastos(){
        $rsLavada = array('success' => true);
        if (!isset($_REQUEST['opcionHdn'])) {
            $_REQUEST['opcionHdn'] = '3';
        }

        switch ($_REQUEST['opcionHdn']) {
            case '2'://obten concepto
                $selFoliosStr = "SELECT DISTINCT a.folio ".
                                "FROM trgastosviajetractortbl a, trviajestractorestbl b ".
                                "WHERE a.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                "AND b.claveMovimiento = 'VP' ".
                                "AND a.idViajeTractor = b.idViajeTractor ".
                                "AND b.idViajeTractor = ".$_REQUEST['idViajeTractorHdn']." ".
                                "AND a.folio != 0 ".
                                "AND a.claveMovimiento = 'GC' ".
                                "AND a.claveMovimiento NOT IN ('XP','XC') ".
                                "ORDER BY a.centroDistribucion, a.fechaEvento DESC";

                $lsWhereStr = "WHERE idViajeTractor = ".$_REQUEST['idViajeTractorHdn']." ";
                $lsCondicionStr = fn_construct($_REQUEST['conceptoHdn'], "concepto", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
                if (isset($_REQUEST['mesAfectacionHdn']) && !empty($_REQUEST['mesAfectacionHdn'])) {
                    $lsCondicionStr = fn_construct($_REQUEST['mesAfectacionHdn'], "mesAfectacion", 1);
                    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
                }
                $lsWhereStr .= " AND folio != '".$_REQUEST['folioHdn']."'".
                               " AND folio NOT IN (${selFoliosStr})";
                $rsLavada = fn_ejecuta_query("SELECT * FROM trpolizafacturastbl ".$lsWhereStr);
                if ($_REQUEST['conceptoHdn'] == '2342') {
                    $selStr = "SELECT * FROM tralimentospendientetbl ".
                              "WHERE idViajeTractor = ".$_REQUEST['idViajeTractorHdn']." ".
                              "AND concepto = '2342' ".
                              "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND folio = '".$_REQUEST['folioHdn']."' ".
                              "AND claveMovimiento = 'PA'";
                    $totPend = fn_ejecuta_query($selStr);
                    $rsLavada['totDiferencia'] = 0;
                    if ($totPend['records'] > 0) {
                        $rsLavada['totTabulado'] = floatval($totPend['root'][0]['facturadoDiferencia']);
                    } else {
                        $selStr = "SELECT * FROM trgastosviajetractortbl ".//fecha Act < 01.04.2020
                                  "WHERE idViajeTractor = ".$_REQUEST['idViajeTractorHdn']." ".
                                  "AND concepto = '2342' ".
                                  "AND folio = ".$_REQUEST['folioHdn'];
                        $totPend = fn_ejecuta_query($selStr);
                        $rsLavada['totTabulado'] = floatval($totPend['root'][0]['observaciones']);
                    }
                    if ($rsLavada['totTabulado'] > 0) {
                        $selFoliosStr = "SELECT DISTINCT a.folio ".
                                        "FROM trgastosviajetractortbl a, trviajestractorestbl b ".
                                        "WHERE a.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                        "AND b.claveMovimiento = 'VP' ".
                                        "AND a.idViajeTractor = b.idViajeTractor ".
                                        "AND b.idViajeTractor = ".$_REQUEST['idViajeTractorHdn']." ".
                                        "AND a.folio != 0 ".
                                        "AND a.claveMovimiento = 'GC' ".
                                        "AND a.claveMovimiento NOT IN ('XP','XC') ".
                                        "ORDER BY a.centroDistribucion, a.fechaEvento DESC";

                        $selStr = "SELECT SUM(facturadoDiferencia) AS facturado FROM tralimentospendientetbl ".
                                  "WHERE idViajeTractor = ".$_REQUEST['idViajeTractorHdn']." ".
                                  "AND concepto = '2342' ".
                                  "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                  "AND folio IN (${selFoliosStr}) ".
                                  "AND claveMovimiento = 'CA'";
                        $totFact = fn_ejecuta_query($selStr);
                        if ($totFact['records'] > 0) {
                            $rsLavada['totFacturado'] = floatval($totFact['root'][0]['facturado']);
                            $rsLavada['totDiferencia'] = $rsLavada['totTabulado'] - $rsLavada['totFacturado'];
                        }
                    }
                }
                break;
            case '3'://insert concepto
                if (isset($_REQUEST['trcomplementoPolizaConceptoHdn']) && $_REQUEST['trcomplementoPolizaConceptoHdn'] == '6002') {
                    //MACHETEROS
                    $delStr = "DELETE FROM trComplementosConceptoTbl ".
                              "WHERE idViajeTractor = ".$_REQUEST['trcomplementoPolizaIdViajeHdn']." ".
                              "AND concepto = '6002' ".
                              "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND folio = 'CA001'";
                    fn_ejecuta_query($delStr);
                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                        $rsLavada['success'] = false;
                        $rsLavada['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $delStr;
                    } else {
                        if ($_REQUEST['total'] > 0) {
                            $arrMacheteros = json_decode($_POST['arrMacheteros'],true);
                            $totMacheteros = sizeof($arrMacheteros) +1;
                            $sec = 0;
                            for ($idx=0; $idx < $totMacheteros; $idx++) { 
                                if ($idx == ($totMacheteros -1)) {
                                    $parametros = $_REQUEST['claveChofer']."|".$_REQUEST['nombreChofer']."|".$_REQUEST['unidad']."|".$_REQUEST['macheteros']."|".$_REQUEST['taxi']."|".$_REQUEST['total'];
                                } else {
                                    $row = $arrMacheteros[$idx];
                                    $parametros = $row['cantidad']."|".$row['concepto']."|".$row['importe'];
                                }
                                $sec++;
                                $insStr = "INSERT INTO trComplementosConceptoTbl (idViajeTractor, concepto, centroDistribucion, folio, secuencia, parametros, fecha, idUsuario) VALUES (" . 
                                          $_REQUEST['trcomplementoPolizaIdViajeHdn'].", ".
                                          "'6002', ".
                                          "'".$_SESSION['usuCompania']."', ".
                                          "'CA001', ".
                                          $sec.", ".
                                          "'${parametros}', ".
                                          "NOW(), ".
                                          $_SESSION['idUsuario'].")";
                                fn_ejecuta_query($insStr);
                                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                                    $rsLavada['success'] = false;
                                    $rsLavada['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    if($_REQUEST['trcomplementoPolizaFechaHdn'] == null){
                        $_REQUEST['trcomplementoPolizaFechaHdn'] = date("Y-m-d H:i:s");
                    } else {
                        if (!empty($_REQUEST['trcomplementoPolizaFechaHdn'])) {
                            $_REQUEST['trcomplementoPolizaFechaHdn'] = date('Y-m-d H:i:s', strtotime($_REQUEST['trcomplementoPolizaFechaHdn']));
                        }
                    }
                    if (!isset($_REQUEST['lavadaHdn'])) {
                        $_REQUEST['lavadaHdn'] = '';
                    }
                    if (!empty($_REQUEST['trcomplementoPolizaObservacionesHdn'])) {
                        $_REQUEST['trcomplementoPolizaObservacionesHdn'] = strtoupper($_REQUEST['trcomplementoPolizaObservacionesHdn']);
                    }
                    $sqlAddFacturaStr = "INSERT INTO trPolizaFacturasTbl (idViajeTractor, concepto, rfc, folio, folioFiscal, ".
                                            "subtotal, iva, total, voucher, mesAfectacion, fechaEvento, observaciones, diferencia, idUsuario, lavada) VALUES (".
                                        $_REQUEST['trcomplementoPolizaIdViajeHdn'].",".
                                        "'".$_REQUEST['trcomplementoPolizaConceptoHdn']."',".
                                        "'".$_REQUEST['trcomplementoPolizaRFCHdn']."',".
                                        "'".$_REQUEST['trcomplementoPolizaFolioHdn']."',".
                                        "'".$_REQUEST['trcomplementoPolizaFolioFiscalHdn']."',".
                                        $_REQUEST['trcomplementoPolizaSubtotalHdn'].",".
                                        $_REQUEST['trcomplementoPolizaIvaHdn'].",".
                                        $_REQUEST['trcomplementoPolizaTotalHdn'].",".
                                        replaceEmptyNull("'".$_REQUEST['trcomplementoPolizaVoucherHdn']."'").",".
                                        "'".$_REQUEST['trcomplementoPolizaMesAfectacionHdn']."',".
                                        "'".$_REQUEST['trcomplementoPolizaFechaHdn']."',".
                                        replaceEmptyNull("'".$_REQUEST['trcomplementoPolizaObservacionesHdn']."'").",".
                                        replaceEmptyNull("'".$_REQUEST['trcomplementoPolizaDiferenciaHdn']."'").",".
                                        $_SESSION['idUsuario'].",".
                                        replaceEmptyNull("'".$_REQUEST['lavadaHdn']."'").")";
                    fn_ejecuta_query($sqlAddFacturaStr);

                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        $rsLavada['successMessage'] = getFacturaGastosSuccessMsg();
                    } else {
                        $rsLavada['success'] = false;
                        $rsLavada['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddFacturaStr;
                    }
                }
                break;
        }
        echo json_encode($rsLavada);
    }

    function dltFacturaGastos(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trcomplementoPolizaFolioFiscalHdn'] == ""){
            $e[] = array('id'=>'trcomplementoPolizaFolioFiscalHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        //VOUCHER NO REQUERIDO
        if($a['success']){
            $sqlDltFacturaStr = "DELETE FROM trPolizaFacturasTbl ".
                                "WHERE folioFiscal = '".$_REQUEST['trcomplementoPolizaFolioFiscalHdn']."' ".
                                "AND folio != ".$_REQUEST['folioHdn'];
            fn_ejecuta_query($sqlDltFacturaStr);
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = getFacturaGastosDltMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltFacturaStr;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }

    function dltFacturasViaje(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($a['success']){
            $selFoliosStr = "SELECT DISTINCT a.folio ".
                            "FROM trgastosviajetractortbl a, trviajestractorestbl b ".
                            "WHERE a.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                            "AND b.claveMovimiento = 'VP' ".
                            "AND a.idViajeTractor = b.idViajeTractor ".
                            "AND b.idViajeTractor = ".$_REQUEST['trcomplementoPolizaIdViajeHdn']." ".
                            "AND a.folio != 0 ".
                            "AND a.claveMovimiento = 'GC' ".
                            "AND a.claveMovimiento NOT IN ('XP','XC') ".
                            "ORDER BY a.centroDistribucion, a.fechaEvento DESC";

            $sqlDltFacturasStr = "DELETE FROM trPolizaFacturasTbl ".
                                "WHERE idViajeTractor = ".$_REQUEST['trcomplementoPolizaIdViajeHdn']." ".
                                "AND mesAfectacion = '".$_REQUEST['trcomplementoPolizaMesAfectacionHdn']."' ".
                                "AND folio != '".$_REQUEST['complementoPolizaFolioTxt']."' ".
                                "AND folio NOT IN (${selFoliosStr})";

            fn_ejecuta_query($sqlDltFacturasStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = getFacturaGastosDltMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltFacturasStr;
            }
        }

        if ($a['success']) {
            $delStr = "DELETE FROM trComplementosConceptoTbl ".
                      "WHERE idViajeTractor = ".$_REQUEST['trcomplementoPolizaIdViajeHdn']." ".
                      "AND concepto = '6002' ".
                      "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                      "AND folio = 'CA001'";
            fn_ejecuta_query($delStr);
            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $delStr;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }

    function dltViajeTmp(){
        $sqlDltViajeTmp = "DELETE FROM trviajestractorestmp ".
                            "WHERE idViajeTractor = '".$_REQUEST['trcomplementoPolizaIdViajeHdn']."' ";
        $rsDltTmp = fn_ejecuta_query($sqlDltViajeTmp);

        echo json_encode($rsDltTmp);
    }

    function addPoliza(){
        $a = array();
        $e = array();
        $a['success'] = true;

        //CON ESTA CLAVE DE MOVIMIENTO SE GUARDARAN LOS DATOS EN LA TABLA
        $claveGastos = 'GC';

        /****************         POLIZA DE GASTOS         ******************/
        if($_REQUEST['trcomplementoPolizaIdViajeHdn'] == ""){
            $e[] = array('id'=>'trcomplementoPolizaIdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_SESSION['usuCompania'] == ""){
            $e[] = array('id'=>'trcomplementoPolizaCentroDistHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['trcomplementoPolizaMesAfectacionHdn'] == ""){
            $e[] = array('id'=>'trcomplementoPolizaMesAfectacionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['trcomplementoPolizaParcialHdn'] == ""){
            $e[] = array('id'=>'trcomplementoPolizaParcialHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $conceptoGastosArr = explode('|', substr($_REQUEST['trcomplementoPolizaConceptoHdn'], 0, -1));
        if(in_array('', $conceptoGastosArr)){
            $e[] = array('id'=>'trcomplementoPolizaConceptoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $cuentaContArr = explode('|', substr($_REQUEST['trcomplementoPolizaCuentaContableHdn'], 0, -1));
        if(in_array('', $cuentaContArr)){
            $e[] = array('id'=>'trcomplementoPolizaCuentaContableHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $subtotalArr = explode('|', substr($_REQUEST['trcomplementoPolizaSubtotalHdn'], 0, -1));
        if(in_array('', $subtotalArr)){
            $e[] = array('id'=>'trcomplementoPolizaSubtotalHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $ivaArr = explode('|', substr($_REQUEST['trcomplementoPolizaIvaHdn'], 0, -1));
        if(in_array('', $ivaArr)){
            $e[] = array('id'=>'trcomplementoPolizaIvaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $totalArr = explode('|', substr($_REQUEST['trcomplementoPolizaImporteHdn'], 0, -1));
        if(in_array('', $totalArr)){
            $e[] = array('id'=>'trcomplementoPolizaImporteHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['trcomplementoPolizaKmPagadosHdn'] == ""){
            $e[] = array('id'=>'trcomplementoPolizaKmPagadosHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        /*******************************************************************************/
        $total = true;
        $folioLength = 4;
        $cuentaPagoEfectivo = "";

        /************* FOLIO ******************/
        $sqlGetNvoFolio = "SELECT  month(now()) as mes, folio ".
                        "FROM trfoliostbl ".
                        "WHERE tipoDocumento = 'PZ' ".
                        "AND centroDistribucion = '".$_SESSION['usuCompania']."';";
        $rsNvoFolio = fn_ejecuta_sql($sqlGetNvoFolio);

       $folioRellenado= sprintf('%06d', $rsNvoFolio['root'][0]['folio']);
       $mesFolio=substr($folioRellenado, 0,2);

        if($mesFolio == $rsNvoFolio['root'][0]['mes'] ){
            $nvoFolio = $rsNvoFolio['root'][0]['folio'] + 1;
        }else{
            $nvoFolio = $rsNvoFolio['root'][0]['mes']."0001";
        }

        $nvoFolio= $_SESSION['usuCompania'].$nvoFolio;

        $selFoliosStr = "SELECT DISTINCT a.folio ".
                        "FROM trgastosviajetractortbl a, trviajestractorestbl b ".
                        "WHERE a.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                        "AND b.claveMovimiento = 'VP' ".
                        "AND a.idViajeTractor = b.idViajeTractor ".
                        "AND b.idViajeTractor = ".$_REQUEST['trcomplementoPolizaIdViajeHdn']." ".
                        "AND a.folio != '0' ".
                        "AND a.claveMovimiento = 'GC' ".
                        "AND a.claveMovimiento NOT IN ('XP','XC') ".
                        "ORDER BY a.centroDistribucion, a.fechaEvento DESC";

        $sqlUpdF = "UPDATE trpolizafacturastbl ".
                    "SET folio = '".$nvoFolio."' ".
                    "WHERE idViajeTractor ='". $_REQUEST['trcomplementoPolizaIdViajeHdn']."' ".
                    "AND folio != '".$_REQUEST['complementoPolizaFolioTxt']."' ".
                    "AND folio NOT IN (${selFoliosStr})";

        fn_ejecuta_sql($sqlUpdF);
        if((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != ""){
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlUpdF;
        } else {
            //macheteros
            $updStr = "UPDATE trComplementosConceptoTbl ".
                      "SET folio = '".$nvoFolio."' ".
                      "WHERE idViajeTractor = ".$_REQUEST['trcomplementoPolizaIdViajeHdn']." ".
                      "AND concepto = '6002' ".
                      "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                      "AND folio = 'CA001'";
            fn_ejecuta_sql($updStr);
            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $updStr;
            } else {
                $sqlUpdFolio = "UPDATE trfoliostbl ".
                            "SET FOLIO = '".SUBSTR($nvoFolio,5,20)
                            ."' ".
                            "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                            "AND tipoDocumento = 'PZ'";
                fn_ejecuta_sql($sqlUpdFolio);
                if((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != ""){
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlUpdFolio;
                }
            }
        }

        //----------------------------------------------------
        $folio = $nvoFolio;
        /*********************************************************/

        //PAGO EFECTIVO CHECK
        $sqlGetCuenta = "SELECT cuentaContable FROM caConceptosCentrosTbl ".
                        "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                        "AND concepto = '7014'";

        $rsCuenta = fn_ejecuta_sql($sqlGetCuenta);

        if(sizeof($rsCuenta['root']) > 0){
            $cuentaPagoEfectivo = $rsCuenta['root'][0]['cuentaContable'];
        } else {
            $a['success'] = false;
            $a['errorMessage'] = "No existe cuenta contable registrada ".
                                "para el concepto de PAGO EN EFECTIVO para ".$_SESSION['usuCompania'];
        }

        /***************************
        /   POLIZA DE GASTOS
        /***************************/
        if($a['success']){
            $fecha = date("Y-m-d H:i:s");
            $taxiExtra = 0.00;
            $totComprobado = 0;
            for ($i=0; $i < sizeof($conceptoGastosArr); $i++) {
                if(floatval($totalArr[$i]) != 0){
                    $totComprobado += floatval($totalArr[$i]);
                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, ".
                                        "mesAfectacion, subtotal, iva, importe, observaciones, claveMovimiento, usuario, ip) VALUES (".
                                        $_REQUEST['trcomplementoPolizaIdViajeHdn'].",".
                                        "'".$conceptoGastosArr[$i]."',".
                                        "'".$_SESSION['usuCompania']."',".
                                        "'".$folio."',".
                                        "'".$fecha."',".
                                        "'".$cuentaContArr[$i]."',".
                                        "'".$_REQUEST['trcomplementoPolizaMesAfectacionHdn']."',".
                                        $subtotalArr[$i].",".
                                        $ivaArr[$i].",".
                                        $totalArr[$i].",".
                                        "'',".
                                        "'".$claveGastos."',".
                                        $_SESSION['idUsuario'].",".
                                        "'".$_SERVER['REMOTE_ADDR']."')";
                    fn_ejecuta_sql($sqlAddPoliza);

                    if((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != ""){
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddPoliza;
                        break;
                    }
                }

                if ($a['success'] && $conceptoGastosArr[$i] == '2342') { //ALIMENTOS
                    $actObservacion = floatval($_REQUEST['totTabAlimentos']);
                    $insStr = "INSERT INTO tralimentospendientetbl (idViajeTractor, concepto, centroDistribucion, folio, facturadoDiferencia, claveMovimiento, idUsuario) VALUES (".
                              $_REQUEST['trcomplementoPolizaIdViajeHdn'].", ".
                              "'2342', ".
                              "'".$_SESSION['usuCompania']."', ".
                              "'".$folio . "', ".
                              $actObservacion.", ".
                              "'CA', ".
                              $_SESSION['idUsuario'].")";
                    fn_ejecuta_sql($insStr);
                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                    }
                }

                if ($a['success'] && floatval($totalArr[$i]) == 0 && $conceptoGastosArr[$i] == '6002') {
                    //macheteros
                    $updStr = "DELETE FROM trComplementosConceptoTbl ".
                              "WHERE idViajeTractor = ".$_REQUEST['trcomplementoPolizaIdViajeHdn']." ".
                              "AND concepto = '6002' ".
                              "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND folio = '".$nvoFolio."'";
                    fn_ejecuta_sql($updStr);
                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $updStr;
                    }
                }
            }

            if ($a['success'])
            {
                // COMBUSTIBLE: DESCUENTO EN BOMBAS Ó PAGO X POLIZA PEND
                $idCombustibleAct = 0;
                if (floatval($_REQUEST['trcomplementoPolizaDifCombustibleHdn']) != 0)
                {
                    if (floatval($_REQUEST['trcomplementoPolizaDifCombustibleHdn']) < 0)
                    {
                        $_REQUEST['trcomplementoPolizaDifCombustibleHdn'] = str_replace('-', '', $_REQUEST['trcomplementoPolizaDifCombustibleHdn']);
                        $conceptoAux = "9012";
                    }
                    else if (floatval($_REQUEST['trcomplementoPolizaDifCombustibleHdn']) > 0) {
                        $conceptoAux = "9013";
                    }

                    $insStr = "INSERT INTO trCombustiblePendienteTbl (idViajeTractor, claveChofer, litros, concepto, claveMovimiento) VALUES (" .
                            $_REQUEST['trcomplementoPolizaIdViajeHdn'] . ", " . 
                            $_REQUEST['trcomplementoPolizaChoferHdn'] . ", " . 
                            $_REQUEST['trcomplementoPolizaDifCombustibleHdn'] . ", " . 
                            "'${conceptoAux}', 'CP')";
                    fn_ejecuta_sql($insStr);
                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                    } else {
                        $idCombustibleAct = mysql_insert_id();
                    }
                }

                if ($a['success']) {
                    $sqlUpdPdt = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CL' " . 
                                 "WHERE idCombustible = " . $_REQUEST['idCombustibleHdn'] . " ";
                    fn_ejecuta_sql($sqlUpdPdt);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'];
                        $a['sql'] = $sqlUpdPdt;
                    } else {
                        if ((!empty($_REQUEST['idCombustibleHdn']) && $_REQUEST['idCombustibleHdn'] != 0) || $idCombustibleAct != 0) {
                            $insStr = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip, subTotal) VALUES (" .
                                $_REQUEST['trcomplementoPolizaIdViajeHdn'] . "," . 
                                "'${conceptoAux}'," . 
                                "'" . $_SESSION['usuCompania'] . "'," . 
                                "'" . $folio . "'," . 
                                "'" . $fecha . "'," . 
                                "''," . 
                                "'" . $_REQUEST['trcomplementoPolizaMesAfectacionHdn'] . "'," . 
                                floatval($_REQUEST['trcomplementoPolizaDifCombustibleHdn']) . "," . 
                                "'".$_REQUEST['idCombustibleHdn']."|".$idCombustibleAct."'," . 
                                "'GC'," . 
                                $_SESSION['idUsuario'] . "," . "'" . 
                                $_SERVER['REMOTE_ADDR'] . "'," . 
                                floatval($_REQUEST['trcomplementoPolizaDifCombustibleHdn']) . ")";
                            fn_ejecuta_sql($insStr);
                            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                            {
                                $a['success'] = false;
                                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                            }
                        }
                    }
                }
            }
            foreach ($ivaArr as $ivaArr) {
                $iva += floatval($ivaArr);
            }
            if($a['success']){
                $sqlAddPolizaIa = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, ".
                    "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (".
                    $_REQUEST['trcomplementoPolizaIdViajeHdn'].",".
                    "'7012',".
                    "'".$_SESSION['usuCompania']."',".
                    "'".$folio."',".
                    "'".$fecha."',".
                    "'125.01401.001',".
                    "'".$_REQUEST['trcomplementoPolizaMesAfectacionHdn']."',".
                    $iva.",".
                    "'',".
                    "'".$claveGastos."',".
                    $_SESSION['idUsuario'].",".
                    "'".$_SERVER['REMOTE_ADDR']."', ".
                    $iva.")";

                fn_ejecuta_sql($sqlAddPolizaIa);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'];
                    $a['sql'] = $sqlAddPolizaIa;
                } else {
                    // Total de Comprobacion
                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, ".
                                    "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (".
                                    $_REQUEST['trcomplementoPolizaIdViajeHdn'].",".
                                    "'7014',".
                                    "'".$_SESSION['usuCompania']."',".
                                    "'".$folio."',".
                                    "'".$fecha."',".
                                    "'".$cuentaPagoEfectivo."',".
                                    "'".$_REQUEST['trcomplementoPolizaMesAfectacionHdn']."',".
                                    $totComprobado.",".
                                    "'',".
                                    "'GP',".
                                    $_SESSION['idUsuario'].",".
                                    "'".$_SERVER['REMOTE_ADDR']."',".
                                    $totComprobado.");";

                    fn_ejecuta_sql($sqlAddPoliza);

                    if((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != ""){
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddPoliza;
                    }
                }
            }
        }

        //OBSERVACIONES
        if($a['success']){
            if($_REQUEST['trcomplementoPolizaObservacionesTxa'] != ""){
                //OBSERVACIONES GASTOS
                $sqlAddObservGastos = "INSERT INTO trObservacionesViajeTbl (idViajeTractor, folio, fechaEvento, tipo, observaciones) ".
                                        "VALUES (".
                                        $_REQUEST['trcomplementoPolizaIdViajeHdn'].",".
                                        "'".$folio."',".
                                        "'".$fecha."',".
                                        "'G',".
                                        "'".$_REQUEST['trcomplementoPolizaObservacionesTxa']."')";

                fn_ejecuta_sql($sqlAddObservGastos);

                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddObservGastos;
                }
            }
        }
        if ($a['success']) {
            $a['successMessage'] = 'Complemento Poliza Generada Correctamente';
            if (isset($_REQUEST['vistaPreviaHdn']) && $_REQUEST['vistaPreviaHdn'] == 1) {
                $_REQUEST['trViajesTractoresIdViajeHdn'] = $_REQUEST['trcomplementoPolizaIdViajeHdn'];
                $_REQUEST['trViajesTractoresFolioPolizaHdn'] = $folio;
                $_REQUEST['trViajesTractoresMesAfectacionHdn'] = $_REQUEST['trcomplementoPolizaMesAfectacionHdn'];
                $a = generaPoliza();
                sleep(1);
                fn_ejecuta_sql(false);
            } else {
                $a['errors'] = $e;
                $a['folio'] = $folio;
                // Elimina archivos preview por usuario >:1:@aha
                $archivosArr = array('pdf','PDF');
                $ps_inputfile = "../documentos/";
                $files = array_diff(scandir($ps_inputfile), array('.', '..'));
                foreach ($files as $file) {
                    $data     = explode(".", $file);
                    $fileName = $data[0];
                    $extName  = end($data);
                    if(in_array($extName, $archivosArr)) {
                        $ps_filename = $ps_inputfile.$fileName.'.'.$extName;
                        if (substr($fileName, 0,strlen('complementopoliza_'.$_SESSION['idUsuario'])) != 'complementopoliza_'.$_SESSION['idUsuario']) {
                            continue;
                        }
                        if (file_exists($ps_filename)) {
                            unlink($ps_filename);
                        }
                    }
                }
                fn_ejecuta_sql($a['success']);
            }
        }

        echo json_encode($a);
    }

    function generaPoliza() {
        require_once("../funciones/fpdf/fpdf.php");
        require_once("trViajesTractores.php");

        $folioTMP = $_REQUEST['trViajesTractoresFolioPolizaHdn'];

        if($_REQUEST['trViajesTractoresIdViajeHdn'] != ''){
            $pdf = new FPDF('P', 'mm', array(216, 280));
            //DATOS GENERALES DEL VIAJE
            $data = getHistoricoViajesImpresion();

            // ---------------------- CONTAR TALONES
            $sqlNumTalones = "SELECT COUNT(t1.folio) numfolio, ".
                            "CONCAT((SELECT t2.folio FROM trtalonesviajestbl t2 WHERE t2.idViajeTractor = t1.idViajeTractor AND t2.tipoTalon IN ('TN','TE') AND t2.claveMovimiento != 'TX' ORDER BY t2.folio ASC LIMIT 1),' AL ', ".
                            "(SELECT t3.folio FROM trtalonesviajestbl t3 WHERE t3.idViajeTractor = t1.idViajeTractor AND t3.tipoTalon IN ('TN','TE') AND t3.claveMovimiento != 'TX' ORDER BY t3.folio DESC LIMIT 1)) AS desCTalon ".
                            "FROM trtalonesviajestbl t1 ".
                            "WHERE t1.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                            "AND t1.claveMovimiento != 'TX';";
            $rsNumTalones = fn_ejecuta_query($sqlNumTalones);

            if($rsNumTalones['root'][0]['numfolio'] <= '7'){
                $numTalones = $data['root'][0]['foliosTalonesViaje'];
            }else{
                $numTalones = $rsNumTalones['root'][0]['desCTalon'];
            }

            //DIESEL EXTRA
            $sqlGetDieselExtra = "SELECT gv.concepto, gv.importe, co.nombre ".
                                 "FROM trGastosViajeTractorTbl gv, caConceptosTbl co, caGeneralesTbl ge ".
                                 "WHERE gv.concepto = co.concepto ".
                                 "AND co.concepto=ge.valor ".
                                 "AND ge.tabla='conceptos' ".
                                 "AND ge.columna='9000' ". 
                                 "AND ge.idioma='+' ". 
                                 "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                 "AND claveMovimiento !='XD' AND claveMovimiento = 'GC' ";
            $dieselExtra = fn_ejecuta_query($sqlGetDieselExtra);
            
            $conceptoSueldo = '143';
            //DATOS EXTRA
            $sqlGetExtra = "SELECT gv.fechaEvento, ".
                            "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos, ".
                            "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7012' ) AS cuentaIva, ".
                            "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7013') AS cuentaCargoOperador,  ".
                             "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7025') AS cuentaOperadores,  ".
                            "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7014') AS cuentaPagoEfectivo, ".
                            "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '2231') AS cuentaNoComprobados, ".
                            "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '141') AS cuentaPagoNeto, ".
                            "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$_SESSION['usuCompania']."' AND concepto = '".$conceptoSueldo."') AS tarifaSueldo, ".
                            "(SELECT concat(SUBSTRING(cc.cuentaContable, 1,13),'.', ".
                                           "LPAD((SELECT ta.tractor FROM catractorestbl ta ".
                                                 "WHERE idTractor = (SELECT vt.idTractor FROM trviajestractorestbl vt ".
                                                                    "WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")),4,0),'.', ".
                                                                    "(SELECT vt.claveChofer FROM trviajestractorestbl vt ".
                                                                    " WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")) as cuentaContable ".
                            " FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$_SESSION['usuCompania']."' AND concepto = '".$conceptoSueldo."') AS cuentaSueldo, ".
                            "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' AND concepto = '145') AS cuentaSCargo ".
                            "FROM trGastosViajeTractorTbl gv ".
                            "WHERE gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                            "AND gv.folio = '".$folioTMP."' ".
                            "AND gv.claveMovimiento = 'GC' LIMIT 1;";

            $extras = fn_ejecuta_query($sqlGetExtra);

            // VER SI TIENE POLIZA PUENTE EN CASO DE REIMPRESION
            $sqlCmpOperador = "SELECT concat(cc.cuentaContable,' ',co.nombre,'     ',gt.importe) as cmpOperador, gt.importe ".
                              "FROM caconceptoscentrostbl cc, caconceptostbl co, trgastosviajetractortbl gt ".
                              "WHERE co.concepto = gt.concepto ".
                              "AND cc.concepto = co.concepto ".
                              "AND cc.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND cc.concepto = '7025' ".
                              "AND gt.claveMovimiento = 'GC' ".
                              "AND gt.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ";

            $rsCmpOperador = fn_ejecuta_query($sqlCmpOperador);

            $pdf = generarPdfGastos($pdf, $data['root'][0], $dieselExtra['root'],$extras['root'][0],$folioTMP,$numTalones);

            $pdf = generarPdfMacheteros($pdf, $_REQUEST['trViajesTractoresIdViajeHdn'], $folioTMP, $extras['root'][0]['fechaEvento']);

            if (isset($_REQUEST['vistaPreviaHdn']) && $_REQUEST['vistaPreviaHdn'] == 1) {
                $hoy = getdate();
                $fechaStr = date('YmdHis', $hoy[0]);
                $nombreArchivo = 'documentos/complementopoliza_'.$_SESSION['idUsuario'].$fechaStr . '.pdf';
                $archivo = '../'.$nombreArchivo;
                $pdf->Output($archivo,'F');
                $pdf->Close();
                $a['success'] = (file_exists($archivo))?true:false;
                $a['archivo'] = "../carbookbck/".$nombreArchivo;
                return $a;
            } else {
                $pdf->Output('poliza.pdf', 'I');
                $pdf->Close();
            }
        } else {
            echo "Error al obtener el ID del viaje";
        }
    }

    function generarPdfGastos($pdf, $data, $diesel, $extras, $folio,$numTal){
        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;
        $folioTMP= $folio;
        $numTalones=$numTal;

        //CONVERTIR EL MES AFECTACION A LETRA Y ESPAÑOL
        setlocale(LC_TIME, 'spanish');
        $mes = $_REQUEST['trViajesTractoresMesAfectacionHdn'];
        $mesAfecta = strtoupper(substr(strftime("%B",mktime(0, 0, 0, $mes)),0,3));

        //USUARIO QUIEN GENERO LA POLIZA
          $sqlGetElaboro = "SELECT DISTINCT us.idUsuario,us.nombre FROM trgastosviajetractortbl gt, segusuariostbl us ".
                            "WHERE us.idUsuario = gt.usuario ".
                            "AND gt.claveMovimiento = 'GC' ".
                            "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ";

        $usuarioElaboracion = fn_ejecuta_query($sqlGetElaboro);


        //GASTOS DE POLIZA (GASTOS COMPROBACION)
        $sqlGetGastosPoliza = "SELECT cc.concepto, cc.cuentaContable, gv.iva, gv.subtotal, gv.importe, co.nombre, ".
                                "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                    "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                                "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioTMP."' ".
                                    "AND gv.claveMovimiento = 'GC' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".
                                "WHERE gv2.folio = '".$folioTMP."' AND gv2.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']. " LIMIT 1) ".
                                "AND ge.tabla = 'comprobacionPoliza' ".
                                "GROUP BY ge.estatus ";

        $gastos1 = fn_ejecuta_query($sqlGetGastosPoliza);

        $gastos= $gastos1['root'];

        //ANTICIPOS
        $sqlGetAnticiposStr =   "SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(cuentaContable,4,0) FROM cachoferestbl WHERE claveChofer = (SELECT claveChofer FROM trviajestractorestbl WHERE idViajeTractor =".$_REQUEST['trViajesTractoresIdViajeHdn']." ))) as cuentaContable, ".
                                "(SELECT nombre FROM caconceptostbl pl  WHERE  pl.concepto = cc.concepto) AS plaza, ".
                                 "(SELECT SUM(gv.importe) FROM trGastosViajeTractorTbl gv WHERE gv.concepto = cc.concepto AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." AND gv.claveMovimiento != 'XP' AND gv.claveMovimiento = 'GC') AS importe, ".
                                    "(SELECT GROUP_CONCAT(gv.folio SEPARATOR '-') FROM trGastosViajeTractorTbl gv ".
                                    "WHERE claveMovimiento != 'GX' AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']."  and gv.concepto=7001 ".
                                    "GROUP BY gv.centroDistribucion) AS folio ".
                                    "FROM caConceptosCentrosTbl cc ".
                                    "WHERE cc.concepto IN ('7009','7016','7015','7023','7024') ".
                                    "AND cc.centroDistribucion NOT IN('CDTSA','CDSFE','CDLCL','CDSLP') ".
                                    "GROUP BY cc.concepto ";

        $anticipos1 = fn_ejecuta_query($sqlGetAnticiposStr);

        $anticipos= $anticipos1['root'];

        //DESTINOS - TALONES
        $sqlGetDestinos = "SELECT tv.distribuidor, pl.plaza ".
                            "FROM trtalonesviajestbl tv, caPlazasTbl pl ".
                            "WHERE tv.idPlazaDestino = pl.idPlaza ".
                            "AND tv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                            "AND tv.claveMovimiento != 'TX' ";

        $destinos1 = fn_ejecuta_query($sqlGetDestinos);

        $destinos= $destinos1['root'];

        //CONCEPTO DE IVA ACREDITABLE

        $sqlIvaAcreditable = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='7012' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GC' ";

        $iva1 = fn_ejecuta_query($sqlIvaAcreditable);

        if($iva1['root'][0]['subtotal'] == null){
            $iva = 0.00;
        }
        else{
            $iva= $iva1['root'][0]['subtotal'];
        }

        $sqlCargoOperador = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='7013' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GC' ";

        $cargoOperador1 = fn_ejecuta_query($sqlCargoOperador);

        if($cargoOperador1['root'][0]['subtotal'] == null){
            $cargosOperador = 0.00;
        }
        else{
            $cargosOperador= $cargoOperador1['root'][0]['subtotal'];
        }
        $sqlpagoEfectivo = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='7014' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GP' ";

        $pagoEfectivo1 = fn_ejecuta_query($sqlpagoEfectivo);

        if($pagoEfectivo1['root'][0]['subtotal'] == null){
            $pagoEfectivo = 0.00;
        }
        else{
            $pagoEfectivo= $pagoEfectivo1['root'][0]['subtotal'];
        }


       $sqlOperadores = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='7025' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GC' ";

       $rssqlOperadores = fn_ejecuta_query($sqlOperadores);


        if($rssqlOperadores['root'][0]['subtotal'] == null){
            $operadores = 0.00;
        }
        else{
            $operadores= $rssqlOperadores['root'][0]['subtotal'];
        }

        $pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->AddPage();
        $pdf->SetFont($font,'',9);
        $pdf->SetAutoPageBreak(false);
        $fechaCompleta = date_create($extras['fechaEvento']);
        $fecha = date_format($fechaCompleta, "d/m/Y");
        $hora = date_format($fechaCompleta, "H:i:s");
        //HEADER
        $pdf->SetY(10+$offsetY);
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TRANSDRIZA S.A. DE C.V.", $border, 1, 'C');
        $pdf->SetX(73+$offsetX);
        $pdf->Cell(70,3,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TOLUCA EDO. DE MEXICO", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"R.F.C. TRA-891031-SXA", $border, 0, 'C');

        $pdf->SetX(175+$offsetX);
        if (!isset($_REQUEST['vistaPreviaHdn']) || $_REQUEST['vistaPreviaHdn'] != 1) {
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioTMP, $border, 1, 'L');
        } else {
            $pdf->SetTextColor(255,0,0);
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioTMP, $border, 1, 'L');
            $pdf->SetTextColor(0,0,0);
        }

        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") .$mesAfecta, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, $border, 1, 'L');

        $linea = "============================================================================================================";
        //DATOS
        $reimpresion = "";
        $descTitulo = "POLIZA DE LIQUIDACION DE GASTOS";
        if (isset($_REQUEST['reImpresionHdn']) && $_REQUEST['reImpresionHdn'] == 1) {
            $descTitulo .= " (REIMPRESION)";
        }

        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(78+$offsetX);
        $pdf->Cell(60,4,$descTitulo, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DIST DESTINO HEADERS
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(130+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 0, 'C');
        $pdf->SetX(160+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(180+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DATOS GENERALES DEL VIAJE
          $pdf->SetX(5+$offsetX);        
        $pdf->Cell(100,4,"Talones  : ".$numTalones, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"No Autos : ".$data['numeroUnidades'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(40,4,"Unidad   : ".$data['tractor']." (V-".str_pad($data['viaje'], 6, '0',STR_PAD_LEFT).")", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Rend.    : ".$data['rendimiento'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Cve. Ope.: ".$data['claveChofer'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Kms/Viaje: ".$data['kilometrosComprobados'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);

        $litrosViaje1 = floatval($data['kilometrosComprobados']) / floatval($data['rendimiento']);
        $litrosViaje = ceil($litrosViaje1);

        $pdf->Cell(30,4,"Lts/Apr  : ".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Lts/Rec  : ".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30, 4,"Lts Menos --->     0", $border, 1, 'L');

        //DIST DESTINO DATOS
        $maxTalones = 10;
        $xColumn = 110;

        $pdf->SetY(55+$offsetY);
        ////////////////////////////////////////////PENDIENTE DE REVISAR////////////////////////////////////

        for ($i=0; $i < $maxTalones; $i++) {
            $pdf->SetX($xColumn+$offsetX);
            if($i+1 <= sizeof($destinos)){
                $pdf->Cell(20,4,$destinos[$i]['distribuidor'], $border, 0, 'C');
                //EL ICONV SE USA PARA QUE IMPRIMA BIEN LOS ACENTOS
                $pdf->Cell(30,4,toUTF8($destinos[$i]['plaza']), $border, 1, 'C');
            } else {
                $pdf->Cell(20,4,"--------", $border, 0, 'C');
                $pdf->Cell(30,4,"--------------", $border, 1, 'C');
            }
            if(floatval($maxTalones) / floatval($i+1) == floatval(2)){
                $xColumn = 160;
                $pdf->SetY(55+$offsetY);
            }
        }
        ////////////////////////////////////////////PENDIENTE DE REVISAR////////////////////////////////////

        //CARGOS
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"C A R G O S", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"===========", $border, 1, 'L');

        $total = 0.00;

        foreach ($gastos as $gasto) {
            $cuentaContable = preg_replace("/(\*+)/",sprintf('%04d',$data['tractor']), $gasto['cuentaContable'], 1);
            $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $cuentaContable, 1);

            $pdf->SetX(5+$offsetX);
            $pdf->Cell(100,4,
                str_pad($cuentaContable,24," ")." ".
                substr($gasto['nombre'], 0, 11)." ".
                str_pad(number_format($gasto['subtotal'],2,'.',','), 9+(11-strlen(substr($gasto['nombre'], 0, 11))), " ", STR_PAD_LEFT),
                $border, 1, 'L');

            $total += floatval($gasto['subtotal']);
        }

        $pdf->SetX(75+$offsetX);
        $pdf->Cell(30,4,"==========", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(45,4,"Total de comp. gastos. ", $border, 0, 'L');
        $pdf->SetX(75.5+$offsetX);
        $pdf->Cell(29.5,4,str_pad(number_format($total, 2, '.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetY(185+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaIva'], 24, " ")." ".
            "IVA ACREDIT ".
            str_pad(number_format($iva,2,'.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');

        //OBSERVACIONES
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(140+$offsetX);
        $pdf->Cell(30,4,"OBSERVACIONES", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"==================================", $border, 1, 'C');

        $pdf->SetX(125+$offsetX);
        $pdf->MultiCell(55, 4, $extras['observacionGastos'], $border, 'L');

        //ANTICIPOS
        $pdf->SetY(170+$offsetY);
        $pdf->SetX(138+$offsetX);
        $pdf->Cell(30,4,"ABONOS", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"================================", $border, 1, 'C');
        $pdf->SetX(132+$offsetX);
        $pdf->Cell(40,4,"Anticipo de Gastos", $border, 1, 'C');
        //LA SIGUIENTE CELDA VACIA ES SOLO PARA SALTAR EXACTAMENTE UNA LINEA
        $pdf->Cell(30,4,"", $border, 1, 'C');

        $totalAnticipo = 0.00;
        $importe = '0.00';
        $foliosAnticipos = "";

        // MONTO DE ANTICIPOS COMPROBADOS
        foreach ($anticipos as $anticipo) {
            if($anticipo['importe'] == ""){
                $importe = '0.00';
            }else{
                $importe = $anticipo['importe'];
            }

            $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $anticipo['cuentaContable'], 1);
            $cuentaContable = preg_replace("/(\*+)/", $data['tractor'], $cuentaContable, 1);

            $pdf->SetX(115+$offsetX);
            $pdf->Cell(80,4,
            str_pad($cuentaContable,18," ")." ".
            substr($anticipo['plaza'], 0, 10)." ".
            str_pad(number_format($importe,2,'.',','), 10+(10-strlen(substr($anticipo['plaza'], 0, 10))), " ", STR_PAD_LEFT),
            $border, 1, 'L');

            $totalAnticipo += floatval($anticipo['importe']);
            $foliosAnticipos = $anticipo['folio'];

        }
    
        $pdf->SetY(205+$offsetY);
        $pdf->SetX(05+$offsetX);
        $pdf->Cell(80,4,
                str_pad($extras['cuentaOperadores'], 24, " ")." ".
                "OPERADORES ".
                str_pad(number_format($operadores, 2, '.',','),10," ",STR_PAD_LEFT),
                $border, 0, 'L');

        $pdf->SetY(210+$offsetY);
        $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,"TOTAL ANTIC. EN PATIOS =====> ".str_pad(number_format($totalAnticipo,2,'.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');

        //CONCEPTOS DE DIESEL EXTRA QUE NO SE SUMAN PARA LO DE DIESEL EXTRA
        $sumaDieselExtra = 0;

        foreach ($diesel as $d) {
            $sumaDieselExtra += floatval($d['importe']);
        }

        $pdf->SetY(80+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "DIESEL EXTRA: ".str_pad($sumaDieselExtra, 1, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetY(84+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "FOLIOS ANT:   ".$foliosAnticipos, $border, 1, 'L');

        //CARGO OPERADOR, PAGO EFECTIVO, COMPROBADO
        $pdf->SetY(215+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaCargoOperador'], 24, " ")." ".
            "CARGO OPER ".
            str_pad(number_format($cargosOperador, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

        $pdf->SetY(215+$offsetY);
        $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,
            str_pad($extras['cuentaPagoEfectivo'], 18, " ")." ".
            "PAGO EFECT ".
            str_pad(number_format($pagoEfectivo, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 1, 'L');

        $comprobado = $total + $iva;
        $pdf->SetY(219+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad("COMPROBADO", 25+10, " ", STR_PAD_LEFT)." ".
            str_pad(number_format($comprobado, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

        //TOTALES
        $pdf->SetY(225+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            "TOTAL  CARGOS ".
            str_pad(number_format($comprobado + $cargosOperador, 2, '.',','),32," ",STR_PAD_LEFT),
            $border, 0, 'L');
                   
        $pdf->SetY(225+$offsetY);
        $pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,
                "TOTAL  ABONOS ".
                str_pad(number_format($totalAnticipo + $pagoEfectivo+$concOperadores, 2, '.',','),26," ",STR_PAD_LEFT),
                $border, 1, 'L');

        //REIMPRESION
        $pdf->SetY(230+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        if(isset($_REQUEST['reImpresionHdn']) && $_REQUEST['reImpresionHdn'] == 1){
            $pdf->SetX(92+$offsetX);
            $pdf->Cell(40,4,"R E I M P R E S I O N", $border, 1, 'C');
        }

        //FIRMAS
        $pdf->SetY(245+$offsetY);
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 1, 'C');

        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"E L A B O R O",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"FIRMA DEL OPERADOR",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"R E V I S O",$border, 1, 'C');
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,$usuarioElaboracion['root'][0]['nombre'],$border, 0, 'C');
        $pdf->SetX(5+$offsetX);
        //$pdf->Cell(65,4,toUTF8($_SESSION['nombreUsr']),$border, 0, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,
            toUTF8($data['apellidoPaterno']." ".$data['apellidoMaterno']." ".$data['nombre']),
            $border, 1, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,"CORC-004                     P.D. No ".$folioTMP,$border, 0, 'L');

        return $pdf;
    }

    function generarPdfMacheteros($pdf, $idViajeTractorPrm, $folioPrm, $fechaEventoPrm){
        $selStr = "SELECT * FROM trComplementosConceptoTbl ".
                 " WHERE idViajeTractor = ${idViajeTractorPrm}".
                 " AND concepto = '6002'".
                 // " AND centroDistribucion = '".$_SESSION['usuCompania']."'".
                 " AND folio = ${folioPrm}".
                 " ORDER BY secuencia";
        $Rst = fn_ejecuta_query($selStr);
        if($Rst['records'] > 0){
            $pdf->SetMargins(10, 15, 10);
            $pdf->SetAutoPageBreak(true, 15);
            $pdf->AddPage();

            $mes = $_REQUEST['trViajesTractoresMesAfectacionHdn'];
            setlocale(LC_TIME , 'es_MX.UTF-8');
            $mesAfecta = strtoupper(substr(strftime("%B",mktime(0, 0, 0, $mes)),0,3));
            $fecha = date('d/m/Y', strtotime($fechaEventoPrm));
            $hora = date('H:i:s', strtotime($fechaEventoPrm));

            $pdf->SetFont('Courier', '', 9);
            $pdf->Cell(29);
            $pdf->Cell(140,4,"TRANSDRIZA S.A. DE C.V.", 0, 0, 'C');
            $pdf->Ln(4);
            $pdf->SetFont('Courier', '', 9);
            $pdf->Cell(14);
            $pdf->Cell(170,4,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", 0, 0, 'C');
            $pdf->Ln(4);
            $pdf->SetFont('Courier', '', 9);
            $pdf->Cell(198,4,"TOLUCA EDO. DE MEXICO", 0, 0, 'C');
            $pdf->Ln(4);
            $pdf->Cell(198,4,"R.F.C. TRA-891031-SXA", 0, 0, 'C');
            $pdf->Ln(6);

            $pdf->Cell(163);
            if (!isset($_REQUEST['vistaPreviaHdn']) || $_REQUEST['vistaPreviaHdn'] != 1) {
                $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioPrm, 0, 0, 'L');
            } else {
                $pdf->SetTextColor(255,0,0);
                $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioPrm, 0, 0, 'L');
                $pdf->SetTextColor(0,0,0);
            }
            $pdf->Ln(4);
            $pdf->Cell(163);
            $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") .$mesAfecta, 0, 0, 'L');
            $pdf->Ln(4);
            $pdf->Cell(163);
            $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, 0, 0, 'L');
            $pdf->Ln(4);
            $pdf->Cell(163);
            $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, 0, 0, 'L');
            $pdf->Ln(8);

            // $linea = "======================================================================================================";
            // $pdf->Cell(198,4,$linea, 0, 0, 'L');
            // $pdf->Ln(8);

            $reimpresion = "";
            if($_REQUEST['reImpresionHdn'] == "1"){//////////CHK
                $reimpresion = " (REIMPRESION)";
            }

            $rowAux = array();
            $idxAux = $Rst['records']-1;
            $rowAux = explode('|', $Rst['root'][$idxAux]['parametros']);

            $pdf->SetFont('Courier', 'B', 10);
            $pdf->Cell(198,5,"NOTAS DE REMISION POR MANIOBRAS".$reimpresion,0,0,'C');
            $pdf->Ln(12);
            $pdf->SetFont('Courier','B',10);
            $pdf->Cell(5);$pdf->Cell(22,5,"No. NOMINA",0,0,'L');
            $pdf->SetFont('Courier','',10);
            $pdf->Cell(130,5,": ".$rowAux[0],0,0,'L');
            $pdf->Ln(5);
            $pdf->SetFont('Courier','B',10);
            $pdf->Cell(5);$pdf->Cell(22,5,"CHOFER",0,0,'L');
            $pdf->SetFont('Courier','',10);
            $pdf->Cell(130,5,": ".utf8_decode($rowAux[1]),0,0,'L');
            $pdf->Ln(5);
            $pdf->SetFont('Courier','B',10);
            $pdf->Cell(5);$pdf->Cell(22,5,"UNIDAD",0,0,'L');
            $pdf->SetFont('Courier','',10);
            $pdf->Cell(130,5,": ".$rowAux[2],0,0,'L');
            $pdf->Ln(10);

            $pdf->SetFont('Courier','B',9);
            $pdf->SetFillColor(224,224,222);
            $pdf->SetDrawColor(130,130,130);
            $pdf->SetLineWidth(0.3);
            $pdf->Cell(7);
            $pdf->Cell(30,6, "CANTIDAD" ,1,0,'C',true);
            $pdf->Cell(116,6,'CONCEPTO DE MANIOBRA',1,0,'C',true);
            $pdf->Cell(36,6, "IMPORTE" ,1,0,'C',true);
            $pdf->Ln(6.22);
            $pdf->SetFont('Courier','',9);
            $pdf->SetFillColor(222, 244, 222);
            $pdf->SetDrawColor(130,130,130);
            $pdf->SetLineWidth(0.02);

            $fill = false;
            foreach ($Rst['root'] as $i => $row) {
                $rowAux = explode('|', $row['parametros']);
                if ($i == $idxAux) {
                    break;
                }
                $border = ($pdf->GetY() <= 92)?0:'TB';
                $pdf->Cell(7,4);
                $pdf->Cell(30,4, $rowAux[0], $border, 0, 'C', $fill);
                $pdf->Cell(116,4, $rowAux[1], $border, 0, 'L', $fill);
                $pdf->Cell(36,4, number_format($rowAux[2],2,'.',','), $border, 0, 'R', $fill);
                $pdf->Ln(4);
                $fill = !$fill;
            }
            if (sizeof($rowAux) > 0) {
                $pdf->Ln(2);
                $pdf->SetFont('Courier','B',8.5);
                $pdf->Cell(123);$pdf->Cell(30,4,"MACHETEROS:",0,0,'L');
                $pdf->Cell(36,4,"$ ".number_format($rowAux[3],2,'.',','),0,0,'R');
                $pdf->Ln(4);
                $pdf->Cell(123);$pdf->Cell(30,4,"TAXI:",0,0,'L');
                $pdf->Cell(36,4,"$ ".number_format($rowAux[4],2,'.',','),0,0,'R');
                $pdf->Ln(4);
                $pdf->Cell(123);$pdf->Cell(30,4,"TOTAL:",0,0,'L');
                $pdf->Cell(36,4,"$ ".number_format($rowAux[5],2,'.',','),0,0,'R');
                $pdf->SetFont('Courier','',8.5);
            }
        }

        return $pdf;
    }
?>