<?php
    session_start();    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
  
    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
    
    switch($_REQUEST['catMovimientosActionHdn']){
        case 'getDestinosMovimiento':            
            getDestinosMovimiento();
            break;
        case 'getEstadosPorNombre':
            getEstadosPorNombre();
            break;
        case 'getKilometraje':
            getKilometraje();    
            break;
        default:
            echo '';
    }

    function getDestinosMovimiento(){        
    	$lsWhereStr = "";
	
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDestinosDistribuidorTxt'], "distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDestinosRsocialTxt'], "razonSocial", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDestinosCiudadTxt'], "cdsChrysler", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
	    if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDestinosRegionCmb'], "region", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
	    $sqlGetDestinosMovimientoStr = "SELECT distribuidor, razonSocial, cdsChrysler, region  FROM alMovimientosDestinosTbl " . $lsWhereStr;     
		
		$rs = fn_ejecuta_query($sqlGetDestinosMovimientoStr);
			
		echo json_encode($rs);
    }
function addDestino(){
            $sqlAddDestinoStr = "INSERT INTO alMovimientosDestinosTbl (distribuidor, razonSocial, cdsChrysler, region) ".
                               "VALUES (".
                               $_REQUEST['catDestinosDistribuidorTxt'].", ".
                               $_REQUEST['catDestinosRsocialTxt'].", ".
                               $_REQUEST['catDestinosCiudadTxt'].", ".
                               "'".$_REQUEST['catDestinosRegionCmb']."')";
            
            $rs = fn_ejecuta_Add($sqlAddDestinoStr);


            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlAddDestinoStr;
                $a['successMessage'] = "Insertado Correctamente";
                $a['id'] = $_REQUEST['catDestinosDistribuidorTxt'];;
            } else {
                $a['success'] = false;
                $a['errorMessage'] = "Error al Insertar los Destinos";
            }    
}

function getKilometraje(){
        $lsWhereStr = "";
    
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajeOrigenTxt'], "puntoDestino", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajeRegionCmb'], "region", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajePtolucaTxt'], "patioToluca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajePsaltilloTxt'], "patioSaltillo", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajePchicaloteTxt'], "patioChicalote", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajePtamsaTxt'], "patioTamsa", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajePlzcTxt'], "patioLazaroCardenas", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajePsfeTxt'], "patioSantaFe", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catKilometrajePsritaTxt'], "patioSantaRita", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        $sqlGetKilometrajeMovimientoStr = "SELECT puntoDestino, region, patioToluca, patioSaltillo, patioChicalote, patioTamsa, ".
                                          " patioLazaroCardenas, patioSantaFe, patioSantaRita  FROM alMovimientosKilometrajeTbl " . $lsWhereStr;     
        
        $rs = fn_ejecuta_query($sqlGetKilometrajeMovimientoStr);
            
        echo json_encode($rs);    
}
    
?>