	<?php

		session_start();
		require_once("../funciones/generales.php");
		require_once("../funciones/construct.php");
		require_once("../funciones/utilidades.php");

		switch($_REQUEST['alprogramacionDanosHdn']){
			case 'selectGridDanos':
				selectGridDanos();
				break;        
		    case 'insertProgramacion':
		    	insertProgramacion();		
		    	break;
			case 'selectfechaProgramada':
		    	selectfechaProgramada();		
		    	break;
			case 'countDatos':
		    	countDatos();		
		    	break;
			case 'comboArea':
		    	comboArea();		
		    	break;
			case 'comboTipo':
		    	comboTipo();		
		    	break;
			case 'comboSeveridad':
		    	comboSeveridad();		
		    	break;
			case 'validaTiempo':
		    	validaTiempo();		
		    	break;
			case 'insertCatalogo':
		    	insertCatalogo();		
		    	break;
			case 'updateCatalogo':
		    	updateCatalogo();		
		    	break;
		    default:
		        echo '';
		}

     function selectGridDanos(){

			$sqlGetUnidad = "SELECT al.idDanoUnidad,al.idDano,al.vin,al.observacion,(SELECT ca.tiempoReparacion from catiemposrepdanostbl ca where ca.idDano=al.idDano) as tiempoReparacion, al.cveStatus
							FROM aldanosUnidadesTbl al
							WHERE al.centroDistribucion='QRO02'
							AND al.cveStatus='SR' 
							AND al.idDanoUnidad not in (select idDanoUnidad from alGlobalDamageTbl)
							ORDER BY 5 ASC ";

			$rsSqlGetUnidad= fn_ejecuta_query($sqlGetUnidad);

echo json_encode($rsSqlGetUnidad);


}

    function insertProgramacion(){

   	 $json = json_decode($_REQUEST["datos"],true);

   	 $horasExtra= $_REQUEST["horasExtra"] ;

   	 if($horasExtra== null)
   	 {
   	 	$horasExtra="null";
   	 }

   	  for ($i=0; $i < sizeof($json); $i++) { 

        $concat.=  "(".$json[$i]["idDanoUnidad"].",'".$_REQUEST["fecha"]."',".$_REQUEST["numPersonas"].",".$horasExtra."),";

        }    
        $values = rtrim($concat, ",");


       $addProg =  "INSERT INTO alprogramacionreparacionestbl (idDanoUnidad,fechaProgramacion,numeroPersonas,horasExtraporPersona) VALUES ".$values."; ";

       $rsaddProg = fn_ejecuta_Add($addProg);

       echo json_encode($rsaddProg);
        
    }


    function selectfechaProgramada(){

     	$fecha= str_replace("T", " ", $_REQUEST["fecha"]);


        $selFecha =  "SELECT pr.idProgramacionUnidad,pr.idDanoUnidad,pr.fechaProgramacion,pr.numeroPersonas,pr.horasExtraporPersona,ad.idDano,ad.observacion,ct.tiempoReparacion from alprogramacionreparacionestbl pr , aldanosunidadestbl ad  , catiemposrepdanostbl ct
					where pr.idDanoUnidad=ad.idDanoUnidad
					AND ad.idDano=ct.idDano
					AND fechaProgramacion='".$fecha."' ";

		$rsselFecha = fn_ejecuta_query($selFecha);

		echo json_encode($rsselFecha);

        }   

    function countDatos(){
			$sqlGetCount = "SELECT count(al.idDanoUnidad) as totalDanos, COUNT(DISTINCT(al.vin)) as totalVines FROM aldanosUnidadesTbl al
							WHERE al.centroDistribucion='QRO02'
							AND al.cveStatus='SR' 
							AND al.idDanoUnidad not in (select idDanoUnidad from alGlobalDamageTbl)";
			$rssqlGetCount= fn_ejecuta_query($sqlGetCount);

		echo json_encode($rssqlGetCount);

        }  



    function comboArea(){
			$sqlComboArea = "SELECT DISTINCT cd.areaDano, CONCAT(cd.areaDano,'-', cg.nombre) as descAreaDano 
from cadanostbl cd , cageneralestbl cg
where  cd.areaDano=cg.valor  and cg.columna='areaDano' ";
			$rssqlComboArea= fn_ejecuta_query($sqlComboArea);

		echo json_encode($rssqlComboArea);

        }    

    function comboTipo(){

      	$comboArea= $_REQUEST["comboArea"] ;

			$sqlcomboTipo = "SELECT DISTINCT cd.tipoDano, CONCAT(cd.tipoDano,'-', cg.nombre) as descTipoDano 
from cadanostbl cd , cageneralestbl cg
WHERE  cd.tipoDano=cg.valor  and cg.columna='tipoDano' 
AND cd.areaDano = '".$comboArea."' ";

			$rssqlcomboTipo= fn_ejecuta_query($sqlcomboTipo);

		echo json_encode($rssqlcomboTipo);

        }       

    function comboSeveridad(){

      	$comboArea= $_REQUEST["comboArea"];
      	$comboTipo= $_REQUEST["comboTipo"];

			$sqlcomboTipo = "SELECT DISTINCT cd.severidadDano, CONCAT(cd.severidadDano,'-', cg.nombre) as descSeveridadDano 
from cadanostbl cd , cageneralestbl cg
WHERE  cd.severidadDano=cg.valor  and cg.columna='severidadDano' 
AND cd.areaDano = '".$comboArea."' 
AND cd.tipoDano = '".$comboTipo."' ";

			$rssqlcomboTipo= fn_ejecuta_query($sqlcomboTipo);

		echo json_encode($rssqlcomboTipo);

        }    

    function validaTiempo(){

      	$comboArea= $_REQUEST["comboArea"];
      	$comboTipo= $_REQUEST["comboTipo"];
      	$comboSeveridad= $_REQUEST["comboSeveridad"];


			$sqlTiempo = "SELECT tiempoReparacion,idDano FROM catiemposrepdanostbl where idDano = (select idDano  from cadanostbl where areaDano='".$comboArea."' 
and tipoDano = '".$comboTipo."'
and severidadDano ='".$comboSeveridad."' )";

			$rssqlTiempo= fn_ejecuta_query($sqlTiempo);

		echo json_encode($rssqlTiempo);

        }   


            function insertCatalogo(){

      	$comboArea= $_REQUEST["comboArea"];
      	$comboTipo= $_REQUEST["comboTipo"];
      	$comboSeveridad= $_REQUEST["comboSeveridad"];
      	$numMinutos= $_REQUEST["numMinutos"];



			$sqlIdDano = "SELECT idDano  from cadanostbl where areaDano='".$comboArea."' 
and tipoDano = '".$comboTipo."'
and severidadDano ='".$comboSeveridad."'  ";

			$rssqlIdDano= fn_ejecuta_query($sqlIdDano);

			$sqlInsertaTiempo = "INSERT INTO catiemposrepdanostbl (idDano,tiempoReparacion) VALUES ('".$rssqlIdDano['root'][0]['idDano']."','".$numMinutos."') ";

			$rssqlInsertaTiempo= fn_ejecuta_query($sqlInsertaTiempo);

			echo json_encode($rssqlInsertaTiempo);

        }   

     		function updateCatalogo(){

      	$idDano= $_REQUEST["idDano"];
      	$numMinutos= $_REQUEST["numMinutos"];

			$updTiempo = "UPDATE catiemposrepdanostbl set tiempoReparacion='".$numMinutos."' where idDano='".$idDano."' ";

			$rsupdTiempo= fn_ejecuta_query($updTiempo);

		echo json_encode($rssqlTiempo);

        }   


	?>