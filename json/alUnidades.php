<?php
    session_start();

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alLocalizacionPatios.php");
    require_once("alRetrabajos.php");
    require_once("catGenerales.php");
    require_once("alDestinosEspeciales.php");
    require_once("trViajesTractores.php");
    require_once("actividadesPmp.php");
    require("C:/Servidores/apache/htdocs/carbookbck/procesos/i830_CBK.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
    //echo $_REQUEST['alUnidadesActionHdn'];

    switch($_REQUEST['alUnidadesActionHdn']){
        case 'asignacionDeUnidades':
            asignacionDeUnidades();
            break;
        case 'validarRC':
            validarRC();
            break;
        case 'validaAsignacionUnidad':
            echo json_encode(validaAsignacionUnidad());
            break;
        case 'getUnidadesCabecero':
            getUnidadesCabecero();
            break;
        case 'getUnidades':
            echo json_encode(getUnidades());
            break;
        case 'getUnidadesModificacion':
            echo json_encode(getUnidadesModificacion());
            break;
        case 'getNoBloqueadas':
            getNoBloqueadas();
            break;
        case 'getBloqueadas':
            echo json_encode(getBloqueadas());
            break;
        case 'getDisponibles':
            echo json_encode(getDisponibles());
            break;
        case 'getUnidades660':
            echo json_encode(getUnidades660());
            break;
        case 'getUnidadesInstrucciones':
            echo json_encode(getUnidadesInstrucciones());
            break;
        case 'getHistoriaUnidad':
            echo json_encode(getHistoriaUnidad());
            break;
        case 'getHistoriaUnidadCambioD':
            echo json_encode(getHistoriaUnidadCambioD());
            break;
        case 'addUnidadMasivo':
            addUnidadMasivo();
            break;
        case 'addUnidad':
            echo json_encode(addUnidad($_REQUEST['alUnidadesVinHdn'],$_REQUEST['alUnidadesDistribuidorTxt'],
                                $_REQUEST['alUnidadesSimboloUnidadHdn'], $_REQUEST['alUnidadesColorHdn'],
                                $_REQUEST['alUnidadesCentroDistHdn'],$_REQUEST['alUnidadesClaveMovimientoHdn'],
                                $_REQUEST['alUnidadesTarifaTxt'],$_REQUEST['alUnidadesLocalizacionUnidadHdn'],
                                $_REQUEST['alUnidadesFolioRepuveTxt'],$_REQUEST['alUnidadesChoferHdn'],
                                $_REQUEST['alUnidadesObservacionesTxa'],$_REQUEST['alUnidadesDescripcionUnidadHdn']));
            break;
        case 'updUnidad':
            updUnidad();
            break;
        case 'addHistoricoUnidad':
            echo json_encode(addHistoricoUnidad($_REQUEST['alUnidadesCentroDistHdn'],$_REQUEST['alUnidadesVinHdn'],
                                                $_REQUEST['alUnidadesClaveMovimientoHdn'],$_REQUEST['alUnidadesDistribuidorTxt'],
                                                $_REQUEST['alUnidadesTarifaTxt'],$_REQUEST['alUnidadesLocalizacionUnidadHdn'],
                                                $_REQUEST['alUnidadesChoferHdn'],$_REQUEST['alUnidadesObservacionesTxa'],
                                                $_REQUEST['alUnidadesUsuarioHdn']));
            break;
        case 'addHistoricoUnidadMasivo':
            echo json_encode(addHistoricoUnidadMasivo($_REQUEST['alUnidadesCentroDistHdn'],$_REQUEST['alUnidadesVinHdn'],
                                                $_REQUEST['alUnidadesClaveMovimientoHdn'],$_REQUEST['alUnidadesDistribuidorTxt'],
                                                $_REQUEST['alUnidadesTarifaTxt'],$_REQUEST['alUnidadesLocalizacionUnidadHdn'],
                                                $_REQUEST['alUnidadesChoferHdn'],$_REQUEST['alUnidadesObservacionesTxa']));
            break;
        case 'updUltimoHistorico':
            echo json_encode(updUltimoHistorico($_REQUEST['alUnidadesVinHdn'],
                                                $_REQUEST['alUnidadesClaveMovimientoHdn'],
                                                $_REQUEST['alUnidadesDistribuidorHdn']));
            break;
        case 'updUltimoDetalle':
            echo json_encode(updUltimoDetalle($_REQUEST['alUnidadesVinHdn'],
                                                $_REQUEST['alUnidadesClaveMovimientoHdn'],
                                                $_REQUEST['alUnidadesDistribuidorHdn']));
            break;
        case 'liberarUnidad':
            echo json_encode(liberarUnidad($_REQUEST['alUnidadesVinHdn']));
            break;
        case 'liberarUnidadMasivo':
            echo json_encode(liberarUnidadMasivo($_REQUEST['alUnidadesVinHdn']));
            break;
        case 'bloquearUnidad':
            echo json_encode(bloquearUnidad($_REQUEST['alUnidadesVinHdn']));
            break;
        case 'getDetenidas':
            getDetenidas();
            break;
        case 'getNoDetenidas':
            getNoDetenidas();
            break;
        case 'detenerUnidad':
            echo json_encode(detenerUnidad($_REQUEST['alUnidadesVinHdn'], $_REQUEST['alUnidadesCentroDistHdn']));
            break;
        case 'detencionUnidades':
            detencionUnidades();
            break;
        case 'getUnidadesEspeciales':
            getUnidadesEspeciales();
            break;
        case 'liberarDetencionUnidades':
            liberarDetencionUnidades();
            break;
        case 'getHoldeadas':
            echo json_encode(getHoldeadas());
            break;
        case 'holdUnidades':
            echo json_encode(holdUnidades($_REQUEST['alUnidadesVinHdn'],
                                $_REQUEST['alUnidadesClaveMovimientoHdn'],
                                $_REQUEST['alUnidadesDistribuidorHdn']));
            break;
        case 'quitarHoldUnidades':
            quitarHoldUnidades();
            break;
        case 'quitarHoldUnitario':
            echo json_encode(quitarHoldUnitario($_REQUEST['alUnidadesAvanzadaHdn'], $_REQUEST['alUnidadesHoldHdn']));
            break;
        case 'insertarEntradaPatio':
            echo json_encode(insertarEntradaPatio($_REQUEST['alUnidadesCentroDistHdn'], $_REQUEST['alUnidadesConLocalizacionHdn'], $_REQUEST['alUnidadesFlotillaHdn'],
                                    $_REQUEST['alUnidadesVinHdn'], $_REQUEST['alUnidadesTarifaTxt'], $_REQUEST['alUnidadesDistribuidorTxt'],
                                    $_REQUEST['alUnidadesSimboloUnidadHdn'], $_REQUEST['alUnidadesRetrabajoHdn'],$_REQUEST['alUnidadesVirtualesHdn']));
            break;
        case 'getHistoricoUnidades':
            echo json_encode(getHistoricoUnidades());
            break;
        case 'updateObservacionesHistorico':
            updateObservacionesHistorico();
            break;
        case 'getRepuve':
            getRepuve();
            break;
        case 'getUltimosMovimientosUnidad':
            echo json_encode(getUltimosMovimientosUnidad($_REQUEST['alUnidadesVinHdn'],
                                        $_REQUEST['alUnidadesListaMovimientoHdn']//Claves de la forma E1|E1|E3|...etc del ultimo hacia atras
                                        ));
            break;
        case 'getUltimoDetalle':
            getUltimoDetalle();
            break;
        case 'addUltimoDetalle':
            echo json_encode(addUltimoDetalle($_REQUEST['alUnidadesVinHdn'], $_REQUEST['alUnidadesCentroDistHdn'],
                                $_REQUEST['alUnidadesClaveMovimientoHdn'], $_REQUEST['alUnidadesDistribuidorHdn'],
                                $_REQUEST['alUnidadesIdTarifaHdn'], $_REQUEST['alUnidadesLocalizacionUnidadHdn'],
                                $_REQUEST['alUnidadesChoferHdn'], $_REQUEST['alUnidadesObservacionesTxa']));
            break;
        case 'getUnidadesControlLlaves':
            getUnidadesControlLlaves();
            break;
        case 'getValidacionAltaUnidad':
            echo json_encode(getValidacionAltaUnidad());
            break;
        case 'addGastosEspeciales':
            echo json_encode(addGastosEspeciales($_REQUEST['alUnidadesDistribuidorHdn'], $_REQUEST['alUnidadesVinHdn'],
                                    $_REQUEST['alUnidadesConceptosGastosHdn'], $_REQUEST['alUnidadesImporteGastosHdn']));
            break;
        case 'getUnidadesEntradaPatio':
            echo json_encode(getUnidadesEntradaPatio());
            break;
        case 'isLiberadaDeHolds':
            isLiberadaDeHolds($_REQUEST['alUnidadesVinHdn']);
            break;
        case 'validaUnidadAsignacion':
            validaUnidadAsignacion();
            break;
        case 'getUnidadesEmbarcadas':
            getUnidadesEmbarcadas();
            break;
        case 'getReUnidades';
            getReUnidades();
            break;
        case 'addReUnidades';
            addReUnidades();
            break;
        case 'modificacionUnidades':
            modificacionUnidades();
            break;
        case 'eliminarHistorico':
                eliminarHistorico();
            break;
        case 'getValidaEntradaPatio':
            getValidaEntradaPatio();
            break;
         case 'validaVin':
            validaVin();
            break;
         case 'getBusca660':
            getBusca660();
            break;
        case 'getBuscaDY':
            getBuscaDY();
            break;
        case 'validarRC':
            validarRC();
            break;
        case 'validarBR':
            validarBR();
            break;
        case 'eliminarTLP':
            eliminarTLP();
            break;
        default:
            echo '';
    }

    function getUnidadesCabecero(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "u.vin", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "u.avanzada", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetUnidades =   "SELECT u.*, ".
                            "(SELECT hu.localizacionUnidad FROM alHistoricoUnidadesTbl hu WHERE hu.idHistorico = (".
                                "SELECT MAX(hu2.idHistorico) FROM alHistoricoUnidadesTbl hu2 WHERE hu2.vin = hu.vin) ".
                            "AND hu.vin = u.vin) AS localizacionUnidad ".
                            "FROM alUnidadesTbl u ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetUnidades);

        echo json_encode($rs);
    }

    function getUnidadesEmbarcadas(){
        $lsWhereStr =   "WHERE hu.fechaEvento =  (SELECT MAX(hu2.fechaEvento) FROM alHistoricoUnidadesTbl hu2 ".
                        "WHERE hu2.vin = ue.vin) ".
                        "AND hu.idTarifa = tt.idTarifa ".
                        "AND hu.vin = ue.vin";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesIdViajeTractorHdn'], "idViajeTractor", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesTipoTarifaHdn'], "tt.tipoTarifa", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesIdTarifaHdn'], "tt.idTarifa", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "hu.distribuidor", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetUnidadesEmbarcadas = "SELECT tt.idTarifa, tt.tarifa, tt.tipoTarifa, ue.* FROM alHistoricoUnidadesTbl hu, trUnidadesEmbarcadasTbl ue, caTarifasTbl tt ".
                                    $lsWhereStr." GROUP BY ue.vin";

        $rs = fn_ejecuta_query($sqlGetUnidadesEmbarcadas);
        echo json_encode($rs);
    }

    function getUnidades(){
        $lsWhereStr = " WHERE u.vin = h2.vin ".
                      " AND h2.idTarifa = t.idTarifa ".
                      //" AND h.centroDistribucion = h2.centroDistribucion".
                      //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                      " AND u.distribuidor = h2.distribuidor";

        if($_REQUEST['alUnidadesParaServEspHdn'] == 1){
            $lsWhereStr .= //" AND h.vin = h2.vin ".
                           //" AND h.idTarifa = h2.idTarifa ".
                           //" AND h.claveMovimiento = h2.claveMovimiento ".
                           //" AND h.centroDistribucion = h2.centroDistribucion".
                           //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                           " AND claveMovimiento NOT IN ( ".   //AND h.fechaEvento = h2.fechaEvento
                            "SELECT valor FROM cageneralestbl ".
                            "WHERE (tabla = 'alholdsunidadestbl' AND columna  = 'claveHold') ".
                            "OR (tabla = 'trHistoricoUnidadesTbl' AND columna = 'nvalidos') )";
                            //")";
                        //")";
        } else {
            //$lsWhereStr .= //" AND h.vin = h2.vin ".
                           //" AND h.claveMovimiento = h2.claveMovimiento ".
                            //" AND h.centroDistribucion = h2.centroDistribucion".
                            //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                            //" AND u.distribuidor = h2.distribuidor";
                           //"AND h.fechaEvento = h2.fechaEvento ";
        }


        $lsWhereStr .= " AND sm.simboloUnidad = u.simboloUnidad ".
                       " AND u.distribuidor = d2.distribuidorCentro ";
                       //" AND h.centroDistribucion = h2.centroDistribucion".
                        //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "u.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "u.vin", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "h.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesSimboloUnidadHdn'], "u.simboloUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesColorHdn'], "u.color", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            if ($_REQUEST['alUnidadesRepuveHdn'] == 'NOTNULL') {
                $lsWhereStr .= "AND u.folioRepuve IS NOT NULL ";
            } else if ($_REQUEST['alUnidadesRepuveHdn'] == 'NULL'){
                $lsWhereStr .= "AND u.folioRepuve IS NULL ";
            } else {
                $lsCondicionStr = fn_construct($_REQUEST['alUnidadesRepuveHdn'], "u.folioRepuve", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesCentroDistHdn'], "h2.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorTxt'], "h2.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesClaveMovimientoHdn'], "h2.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesMarcaHdn'], "sm.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesIdTarifaHdn'], "t.idTarifa", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesTipoDistribuidorHdn'], "d2.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        
        $select = "(SELECT 1 FROM alUnidadesDetenidasTbl de WHERE de.vin = u.vin AND de.claveMovimiento = 'UD') AS detenida, ";
        if($_SESSION['usuCompania'] == 'CDLZC' || $_SESSION['usuCompania'] == 'CDVER')
        {
                //$select = "0 AS detenida, ";
        }

        $sqlGetUnidadesStr = "SELECT u.vin, u.avanzada, u.distribuidor AS distribuidorInicial, ".
                             "u.simboloUnidad, u.color, u.folioRepuve, h2.centroDistribucion, h2.fechaEvento, h2.distribuidor, h2.idTarifa, ".
                             "h2.localizacionUnidad, h2.claveChofer, h2.observaciones, h2.usuario, h2.ip, h2.claveMovimiento AS cveMovHistorico, ".
                             "t.tarifa, t.descripcion AS descTarifa, t.tipoTarifa, sm.tieneRepuve AS simboloRepuve, ".
                             "d2.estatus AS estatusDistribuidor, d2.tipoDistribuidor, sm.marca, sm.descripcion AS nombreSimbolo, ".
                             "(SELECT IFNULL((SELECT ud.claveMovimiento FROM alUltimoDetalleTbl ud WHERE ud.vin = u.vin), "."(SELECT h2.claveMovimiento FROM alhistoricounidadestbl h WHERE h2.vin = u.vin AND h2.fechaEvento = (SELECT MAX(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.vin = h2.vin )))) AS claveMovimiento, ".                             
                             "(SELECT COUNT(*) FROM alhistoricounidadestbl a, catarifastbl b WHERE a.vin = u.vin AND a.claveMovimiento = 'OM' AND a.idTarifa = b.idTarifa AND b.tipoTarifa = 'N') AS movimientoOM, ".
                             "(SELECT IFNULL((SELECT 1 FROM caGeneralesTbl g2 WHERE g2.tabla = 'unidadAsignar' AND g2.columna = 'claveValida' AND g2.valor = (SELECT ud.claveMovimiento FROM alUltimoDetalleTbl ud WHERE ud.vin = u.vin)),0)) AS listaAsignar, ".
                             "(SELECT 1 FROM alUnidadesDetenidasTbl ud WHERE ud.vin = h2.vin AND ud.centroLibera IS NULL) AS detenida, ".
                             "(SELECT 1 FROM   alcotizacionesespecialestbl ud WHERE  ud.vin = h2.vin AND ud.estatus='CZ' limit 1) AS cotizacion, ".                             
                             "(SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 WHERE ud2.vin=ud.vin) limit 1) AS claveMovDetenidas, ".
                             "(SELECT g2.nombre FROM caGeneralesTbl g2 WHERE g2.tabla = 'alUnidadesDetenidasTbl' AND g2.columna = 'claveMovimiento' AND g2.valor = (SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 WHERE ud2.vin=ud.vin) limit 1 )) AS nombreClaveMovDetenidas, ".
                             "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h2.claveMovimiento AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreClaveMov, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE d.distribuidorCentro = u.distribuidor) AS nombreDistribuidor, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE d.distribuidorCentro = h2.centroDistribucion) AS descCentroDistribucion, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE d.distribuidorCentro = h2.localizacionUnidad) AS descLocalizacion,".
                             "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = sm.marca) as descMarca, ".
                             "(SELECT co.descripcion FROM caColorUnidadesTbl co WHERE co.marca = sm.marca AND co.color = u.color) AS nombreColor, ".
                             $select.
                             "(SELECT 1 FROM alUnidadesTmp tmp WHERE tmp.vin = u.vin) AS bloqueada, ".
                             "(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta WHERE un.vin =u.vin AND un.idTarifa = ta.idTarifa AND ta.tipoTarifa in('N','E')) AS tieneTarifaNormal, ".
                             //"(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta WHERE un.vin =u.vin AND un.idTarifa = ta.idTarifa AND ta.tipoTarifa = 'E') AS tieneTarifaEspecial, ".
                             "(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta WHERE un.vin =u.vin AND un.idTarifa = ta.idTarifa AND ta.tipoTarifa = 'C') AS tieneTarifaCD, ".
                             "(SELECT 1 FROM alDestinosEspecialesTbl de WHERE de.vin = u.vin LIMIT 1) AS destinoEspecial, ".
                             "(SELECT 1 FROM alRetrabajoUnidadesTbl ru WHERE ru.vin = u.vin) AS reTrabajo, ".
                             "(SELECT 1 FROM trunidadesembarcadastbl ue WHERE ue.vin = u.vin ) AS embarcada, ".
                             "(SELECT 1 FROM trunidadesembarcadastbl ue WHERE ue.vin = u.vin AND ue.claveMovimiento ='L' AND ue.folio!='') AS facturada, ".
                             "(SELECT dc.sucursalDe FROM caDistribuidoresCentrosTbl dc WHERE dc.distribuidorCentro = h2.localizacionUnidad) AS localizacionComoCentro, ".
                             "(SELECT de.distribuidorDestino FROM aldestinosespecialestbl de WHERE u.vin = de.vin AND idDestinoEspecial = (SELECT MAX(de1.idDestinoEspecial) FROM aldestinosespecialestbl de1 WHERE de1.vin = de.vin)) as distribuidorDestino ".
                             "FROM alUnidadesTbl u,alultimodetalletbl h2, caTarifasTbl t, caSimbolosUnidadesTbl sm, ".
                             "caDistribuidoresCentrosTbl d2 ".$lsWhereStr; 
                //echo "<br>$sqlGetUnidadesStr<br>";
        $rs = fn_ejecuta_query($sqlGetUnidadesStr);
      //echo json_encode($sqlGetUnidadesStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            //Para checar si tiene un servicio especial pendiente
            if($_REQUEST['alUnidadesListaMovimientoHdn'] != ""){
                $data = getUltimosMovimientosUnidad($rs['root'][$iInt]['vin'], $_REQUEST['alUnidadesListaMovimientoHdn']);
                $rs['root'][$iInt]['listaEstatus'] = $data['success'];
            }
            $rs['root'][$iInt]['descDist'] = $rs['root'][$iInt]['distribuidor']." - ".$rs['root'][$iInt]['nombreDistribuidor'];
            $rs['root'][$iInt]['descColor'] = $rs['root'][$iInt]['color']." - ".$rs['root'][$iInt]['nombreColor'];
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['descTarifa'];
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['nombreSimbolo'];
            $rs['root'][$iInt]['marcaDesc'] = $rs['root'][$iInt]['marca']." - ".$rs['root'][$iInt]['descMarca'];
            $rs['root'][$iInt]['detenida'] = $rs['root'][$iInt]['detenida'];
        }
        return $rs;
    }

    function getNoBloqueadas(){
        $lsWhereStr = "WHERE u.vin = h.vin ".
                      "AND h.idTarifa = t.idTarifa ".
                      //"AND h.fechaEvento=(".
                      //"SELECT MAX(h1.fechaEvento) ".
                      //"FROM alHistoricoUnidadesTbl h1 ".
                      //"WHERE h1.vin = u.vin) ".
                      "AND sm.simboloUnidad = u.simboloUnidad ".
                      "AND u.distribuidor = d2.distribuidorCentro ".
                      //"AND h.claveMovimiento IN(SELECT valor FROM caGeneralesTbl ge WHERE ge.tabla = 'alUltimoDetalleTbl' AND ge.columna = 'INVFIS' AND ge.valor = h.claveMovimiento) ".
                      "AND h.claveMovimiento NOT IN ('CA') ".
                      "AND u.vin NOT IN  (SELECT tmp.vin from alUnidadesTmp tmp)";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "u.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "u.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "h.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesSimboloUnidadHdn'], "u.simboloUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesColorHdn'], "u.color", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            if ($_REQUEST['alUnidadesRepuveHdn'] == 'NOTNULL') {
                $lsWhereStr .= "AND u.folioRepuve IS NOT NULL ";
            } else if ($_REQUEST['alUnidadesRepuveHdn'] == 'NULL'){
                $lsWhereStr .= "AND u.folioRepuve IS NULL ";
            } else {
                $lsCondicionStr = fn_construct($_REQUEST['alUnidadesRepuveHdn'], "u.folioRepuve", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesCentroDistHdn'], "h.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorTxt'], "h.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesClaveMovimientoHdn'], "h.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesMarcaHdn'], "sm.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesIdTarifaHdn'], "t.idTarifa", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesRecuperaVinHdn'], "u.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesRecuperaAvanzadaHdn'], "u.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetUnidadesStr = "SELECT u.vin, u.avanzada, u.distribuidor AS distribuidorInicial, ".
                             "u.simboloUnidad, u.color, u.folioRepuve, h.*, t.descripcion AS descTarifa, d2.idPlaza, ".
                             "d2.estatus AS estatusDistribuidor, d2.direccionEntrega, sm.marca, sm.descripcion AS nombreSimbolo, ".
                             "(SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin ".
                                "AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 ".
                                    "WHERE ud2.vin=ud.vin)) AS claveMovDetenidas, ".
                             "(SELECT g2.nombre FROM caGeneralesTbl g2 WHERE g2.tabla = 'alUnidadesDetenidasTbl' ".
                                "AND g2.columna = 'claveMovimiento' AND g2.valor = ".
                                    "(SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin ".
                                    "AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 ".
                                        "WHERE ud2.vin=ud.vin))".
                                ") AS nombreClaveMovDetenidas, ".
                             "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h.claveMovimiento ".
                             "AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreClaveMov, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d ".
                             "WHERE d.distribuidorCentro=h.distribuidor) AS nombreDistribuidor, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE ".
                             "d.distribuidorCentro=h.centroDistribucion) AS nombreCentroDist, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE ".
                             "d.distribuidorCentro=h.localizacionUnidad) AS descLocalizacion,".
                             "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = sm.marca) as descMarca, ".
                             "(SELECT co.descripcion FROM caColorUnidadesTbl co WHERE co.marca = sm.marca ".
                                "AND co.color = u.color) AS nombreColor, ".
                             "(SELECT 1 FROM alUnidadesDetenidasTbl de WHERE de.vin = u.vin AND de.claveMovimiento = 'UD') AS detenida ".                             
                             "FROM alUnidadesTbl u, alUltimoDetalleTbl h, caTarifasTbl t, caSimbolosUnidadesTbl sm, ".
                             "caDistribuidoresCentrosTbl d2 ".$lsWhereStr;
        //echo $sqlGetUnidadesStr;
        $rs = fn_ejecuta_query($sqlGetUnidadesStr);

        //echo json_encode($sqlGetUnidadesStr);
        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descDist'] = $rs['root'][$iInt]['distribuidor']." - ".$rs['root'][$iInt]['nombreDistribuidor'];
            $rs['root'][$iInt]['descColor'] = $rs['root'][$iInt]['color']." - ".$rs['root'][$iInt]['nombreColor'];
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['idTarifa']." - ".$rs['root'][$iInt]['descTarifa'];
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['nombreSimbolo'];
            $rs['root'][$iInt]['marcaDesc'] = $rs['root'][$iInt]['marca']." - ".$rs['root'][$iInt]['descMarca'];
            $rs['root'][$iInt]['descClaveMov'] = $rs['root'][$iInt]['claveMovimiento']." - ".$rs['root'][$iInt]['nombreClaveMov'];
            $rs['root'][$iInt]['descCentroDist'] = $rs['root'][$iInt]['centroDistribucion']." - ".$rs['root'][$iInt]['nombreCentroDist'];
            //Verifica si el último movimiento fue un Cambio de Distribuidor
            $sql = "SELECT COUNT(*) AS ultimoMovimiento FROM alHistoricoUnidadesTbl a".
                         " WHERE a.VIN = '".$rs['root'][$iInt]['vin']."'".
                         " AND a.claveMovimiento = 'CD'".
                         " AND a.fechaEvento = (SELECT MAX(b.fechaEvento) FROM alHistoricoUnidadesTbl b WHERE b.VIN = '".$rs['root'][$iInt]['vin']."')";
            //echo $sql;
                $rsUlt = fn_ejecuta_query($sql);                
            $rs['root'][$iInt]['ultimoMovimiento'] = $rsUlt['root'][$iInt]['ultimoMovimiento'];
        }
        echo json_encode($rs);
    }

    function getBloqueadas(){
        $lsWhereStr = "WHERE ut.idUsuario = us.idUsuario";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "ut.avanzada", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "ut.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetBloqueadasStr = "SELECT ut.*, us.usuario FROM alUnidadesTmp ut, segUsuariosTbl us , alultimodetalletbl tl ".
                                $lsWhereStr . " AND ut.vin=tl.vin ".
                                        " AND tl.centroDistribucion='".$_SESSION['usuCompania']."'";

        $rs = fn_ejecuta_query($sqlGetBloqueadasStr);

        return $rs;
    }

    function getDisponibles(){
        $success = true;

        $lsWhereStr = "WHERE un.simboloUnidad = su.simboloUnidad ". 
                        "AND ct.clasificacion = su.clasificacion ".
                        "AND ct.marca = su.marca ".
                        "AND ct.idTarifa = ud.idTarifa ".
                        "AND tf.idTarifa = ud.idTarifa ".
                        "AND un.vin = ud.vin ".
                        "AND un.vin = ud.vin ".
                        //"AND hu.idtarifa = ud.idTarifa ".
                        //"AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) FROM alhistoricounidadestbl hu2 ".
                        //    "WHERE hu2.vin  = un.vin ) ". 
                        //"AND ud.claveMovimiento in (SELECT valor FROM caGeneralesTbl WHERE tabla = 'i340') ".
                        //"AND hu.centroDistribucion = ud.centroDistribucion ".
                        "AND un.vin NOT IN  (SELECT tmp.vin from alUnidadesTmp tmp)";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "un.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "un.vin", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        /*if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesTarifaHdn'], "tf.tarifa", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }*/
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesTipoTarifaHdn'], "tf.tipoTarifa", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
       /* if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesClaveMovimientoHdn'], "ud.claveMovimiento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }*/

        //Para que el talon solo pueda tener unidades de su misma tarifa si son 13 o 00

        /*if($_REQUEST['alUnidadesTarifaTalonHdn'] != ""){
            if($_REQUEST['alUnidadesTarifaTalonHdn'] == "13"){
                $lsWhereStr .= " AND tf.tarifa = '00' ";
            } else if($_REQUEST['alUnidadesTarifaTalonHdn'] == "00"){
                $lsWhereStr .= " AND tf.tarifa = '00' ";
            } else {                
                $lsWhereStr .= " AND tf.tarifa != '13' AND tf.tarifa != '00'";
            }
        }*/

        $sqlGetDisponiblesStr = "SELECT un.vin, un.simboloUnidad, un.color, su.descripcion AS nombreSimbolo, ".
                                    "su.clasificacion, su.marca, ud.idTarifa, ud.claveMovimiento AS cveMovHistorico, ".
                                    "ud.distribuidor, tf.tarifa, tf.tipoTarifa, ud.claveMovimiento, ud.centroDistribucion, ".
                                    "(SELECT 1 FROM caGeneralesTbl g2 WHERE g2.tabla = 'unidadAsignar' ".
                                        "AND g2.columna = 'claveValida' AND g2.valor = ud.claveMovimiento) AS listaAsignar, ".
                                    "(SELECT co.descripcion FROM caColorUnidadesTbl co ".
                                        "WHERE co.color = un.color AND co.marca = su.marca) AS nombreColor, ".
                                    "(SELECT 1 FROM alUnidadesDetenidasTbl ud ".
                                        "WHERE ud.vin = un.vin AND ud.centroLibera IS NULL) AS detenida, ".
                                    "(SELECT 1 FROM alUnidadesDetenidasTbl ud ".
                                        "WHERE ud.vin = un.vin ".
                                        "AND numeroMovimiento =(SELECT MAX(numeroMovimiento) from alunidadesdetenidastbl where ud.vin=un.vin) ".
                                           "AND ud.centroLibera IS NOT NULL) AS detenidaServEsp, ".
                                "(SELECT distribuidorDestino FROM alDestinosEspecialesTbl de WHERE de.vin = un.vin AND de.idDestinoEspecial = ud.localizacionUnidad) as distribuidorDestino, ".
                                "(SELECT tipoDistribuidor FROM cadistribuidorescentrostbl di WHERE di.distribuidorCentro = '".$_REQUEST['alUnidadesDistribuidorHdn']."') as tipoDistribuidor ".
                                "FROM alunidadestbl un, casimbolosunidadestbl su, ".
                                "caclasificaciontarifastbl ct ,catarifastbl tf, alUltimoDetalleTbl ud ".$lsWhereStr;
//echo "$sqlGetDisponiblesStr<br>";
        $rs = fn_ejecuta_query($sqlGetDisponiblesStr);
                
                //var_dump($rs);
        //Cuando la avanzada tiene más de 1 VIN, sólo carga el store de VIN para su selección y después de seleccionarlo hacer las validaciones correspondientes
        if(isset($_REQUEST['idPantalla']) && $_REQUEST['idPantalla'] == "TRMODIFICACIONTALONESVIAJEWIN" && $rs['records'] > 1 &&
            (!isset($_REQUEST['alUnidadesVinHdn']) || $_REQUEST['alUnidadesVinHdn'] == '')){
                $_REQUEST['alUnidadesAvanzadaHdn'] = '';
                //echo "pass";
        }
        if($_REQUEST['alUnidadesVinHdn'] != "" || $_REQUEST['alUnidadesAvanzadaHdn'] != ""){

            if(sizeof($rs['root']) == 0){
                $rs['records'] = 1;
                $rs['root'][0]['errorMessage'] = "La Unidad no esta Disponible";
                $success = false;
            }            

            if($success && isset($_REQUEST['centroDistribucion']) && sizeof($rs['root']) > 0 &&
                 $_REQUEST['centroDistribucion'] != $rs['root'][0]['centroDistribucion']){
                $rs['records'] = 1;
                $rs['root'][0]['errorMessage'] = "Centro de Distribucion distinto al asignado.";
                $success = false;
            }            

            if($success && $rs['root'][0]['listaAsignar'] != "1"){
                $rs['records'] = 1;
                $rs['root'][0]['errorMessage'] = "Unidad no se encuentra Lista para Asignar";
                $success = false;
            }

            //Si ya se ingresó una unidad que alguna vez estuvo detenida O NO (SERVICIO ESPECIAL PARA DETENIDAS)
            if($success && $_REQUEST['alUnidadesDetenidasServEspHdn'] == '1'){
                $sqlCheckDetenidaDistribuidorStr = "SELECT 1 FROM alUnidadesDetenidasTbl ud ".
                                        "WHERE ud.vin = '".$rs['root'][0]['vin']."' AND ud.centroLibera IS NOT NULL";

                $rsTemp = fn_ejecuta_query($sqlCheckDetenidaDistribuidorStr);

                if(sizeof($rsTemp['root']) == 0){
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "Solo se pueden ingresar unidades detenidas para servicio especial.";
                    $success = false;
                }
            }

            //Si ya se ingresó una unidad detenida solo se pueden ingresar unidades detenidas
            if($success && $_REQUEST['alUnidadesDetenidasHdn'] == '1'){
                $sqlCheckDetenidaStr = "SELECT 1 FROM alUnidadesDetenidasTbl WHERE vin = '".$rs['root'][0]['vin']."' ".
                                            "AND centroLibera IS NULL";

                $rsTemp = fn_ejecuta_query($sqlCheckDetenidaStr);

                if(sizeof($rsTemp['root']) == 0){
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "Solo se pueden ingresar unidades detenidas en este talon";
                    $success = false;
                }
            }

            //Revisa que sea del mismo distribuidor

            //echo json_encode($rs['root'][0]['tipoDistribuidor']);
            //var_dump($_REQUEST['alUnidadesDistribuidorHdn']);
            //var_dump($rs['root'][0]['distribuidorDestino']);   
            if($success && $_REQUEST['alUnidadesDistribuidorHdn']){
                if($rs['root'][0]['tarifa'] == "00" && $rs['root'][0]['distribuidorDestino'] != $_REQUEST['alUnidadesDistribuidorHdn']){
                    $rs['records'] = 1;
                    $rs['root'][0]['errorMessage'] = "Unidad con cambio de Destino, el Destino no Corresponde al distribuidor del Talon";
                    $success = false;
                }else if($rs['root'][0]['tarifa'] == '13'){


                    //Consulto el Historico de la Unidad
                    $rsHistorico = getHistoricoUnidades($rs['root'][0]['vin']);
                    $rsHistorico = $rsHistorico['root'];

                    $claveMovimientoDS = $rsHistorico[ count($rsHistorico) - 2]['claveMovimiento'];
                    $claveMovimientoRC = $rsHistorico[ count($rsHistorico) - 1]['claveMovimiento'];

                    if($claveMovimientoDS == 'DS' && $claveMovimientoRC == 'RC'){

                        //Valida que corresponda el distribuidor en alDestinosEspecialesTbl
                        $_REQUEST['alDestinosEspecialesVinTxt'] = $rsUnidades[0]['vin'];
                        $rsDestino = getDestinosEspeciales();

                        //echo($rsDestino[0]['distribuidorDestino']);

                        if($rsUnidades[0]['distribuidorDestino'] != $rsUnidades[0]['distribuidor']){
                            $a['success'] = false;
                            $a['errorMsg'] = 'Unidad no Corresponde su Destino Especial';
                        }
                        else{
                            $a['success'] = true;
                        }
                    } else{
                        $rs['records'] = 1;
                        $rs['root'][0]['errorMessage'] = "La Unidad cuenta con un servicio especial";
                        $success = false;
                    }
                }elseif($rs['root'][0]['tipoTarifa']== 'N' && $rs['root'][0]['tipoDistribuidor'] =='DI' && $rs['root'][0]['tarifa'] != "00" && $rs['root'][0]['distribuidor'] != $_REQUEST['alUnidadesDistribuidorHdn']){

                    $rs['records'] = 1;
                    $rs['root'][0]['errorMessage'] = "Unidad no Corresponde al distribuidor del Talon";
                    $success = false;
                }elseif($rs['root'][0]['tipoDistribuidor'] =='CD'){
                    $a['success'] = true;
                }
            }

            //Revisa que la unidad no se encuentre en el mismo viaje
            if($success && $_REQUEST['alUnidadesViajeHdn'] != ''){
                $sqlCheckStr = "SELECT vin FROM alUnidadesDetallesTalonesTmp ".
                                "WHERE idViaje = ".$_REQUEST['alUnidadesViajeHdn']." ".
                                "AND vin = '".$rs['root'][0]['vin']."'";

                $rsTemp = fn_ejecuta_query($sqlCheckStr);

                if(sizeof($rsTemp['root']) > 0){
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "Unidad ya existente en este viaje";
                    $success = false;
                }
            }

            //Revisa que la unidad no se encuentre en retrabajo
            if($success){
                $sqlCheckStr = "SELECT vin FROM alRetrabajoUnidadesTbl WHERE vin = '".$rs['root'][0]['vin']."'";

                $rsTemp = fn_ejecuta_query($sqlCheckStr);

                if(sizeof($rsTemp['root'])){
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "Unidad en Retrabajo";
                    $success = false;
                }
            }

            //Revisa si es tarifa 13 que esté detenida
            if($success && $rs['root'][0]['idTarifa'] == '13'){
                $sqlCheckStr = "SELECT vin FROM alUnidadesDetenidasTbl WHERE vin = '".$rs['root'][0]['vin']."'";

                $rsTemp = fn_ejecuta_query($sqlCheckStr);

                if(sizeof($rsTemp['root'])){
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "Unidad con Tarifa 13 no se encuentra detenida";
                    $success = false;
                }
            }

            //Revisa que si es tarifa 13 que su antepenultimo movimiento debe ser Recoger Unidad,
            //Cambio de Tarifa o Detenida para Servicio Especial.
            /*
                Se busca el antepenúltimo movimiento de la unidad (excluyendo el estatus de “Talón Cancelado”)
                y si es diferente de “Recoger Unidad” y diferente de “Cambio de Tarifa” y diferente de
                “Detenida para Servicio Especial” entonces se le avisa al usuario que esta es una “Opción Invalida”
                en la captura. En caso contrario se busca la descripción del símbolo de la unidad para presentarlo
                en la pantalla como dato informativo.
            */
            if($success && $rs['root'][0]['idTarifa'] == '13'){
                $sqlCheckStr = "SELECT * FROM ( ".
                                    "SELECT claveMovimiento FROM alhistoricounidadestbl ".
                                        "WHERE vin = '".$rs['root'][0]['vin']."' ORDER BY idHistorico DESC LIMIT 3 ".
                                ") sub ORDER BY idHistorico ASC LIMIT 1 ";

                $rsTemp = fn_ejecuta_query($sqlCheckStr);

                if($rsTemp['root'][0]['claveMovimiento'] != 'RC' && $rsTemp['root'][0]['claveMovimiento'] != 'CE' && $rsTemp['root'][0]['claveMovimiento'] != 'DT'){
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "Opción inválida";
                    $success = false;
                }
            }

            //Revisa si es tarifa normal que tenga Entrada a Patio en su historia
            if($success && $rs['root'][0]['tipoTarifa'] == 'N'){
                $sqlCheckStr = "SELECT idHistorico FROM alHistoricoUnidadesTbl ".
                                            "WHERE vin = '".$rs['root'][0]['vin']."' ".
                                            "AND claveMovimiento IN ('EP','IC','AN'); ";

                $rsTemp = fn_ejecuta_query($sqlCheckStr);

                if(sizeof($rsTemp['root']) < 1){
                    $vin = $rs['root'][0]['vin'];
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "<b>La unidad '".$vin."' no cuenta con entrada a patio.</b>";
                    $success = false;
                }
            }

            //Revisa si la unidad no tiene holds
            if($success){
                /*$sqlCheckStr = "SELECT hu.claveHold, ".
                                            "(SELECT ge.nombre FROM caGeneralesTbl ge WHERE tabla = 'alHoldsUnidadesTbl' ".
                                                "AND ge.columna = 'claveHold' AND ge.valor = hu.claveHold) AS descHold ".
                                            "FROM alHoldsUnidadesTbl hu ".
                                            "WHERE hu.vin = '".$rs['root'][0]['vin']."'";

                $rsTemp = fn_ejecuta_query($sqlCheckStr);*/

                $rsClaveHold="SELECT * FROM alhistoricounidadestbl h
                                WHERE h.vin ='".$rs['root'][0]['vin']."'
                                and h.claveMovimiento in (SELECT valor FROM cageneralestbl
                                                            WHERE TABLA='alHoldsUnidadesTbl'
                                                            AND COLUMNA='claveHold'
                                                            AND ESTATUS='D')";
               $rsClaveHold=fn_ejecuta_query($rsClaveHold);                                             

                $sqlCheckStr = "SELECT hu.vin, ".
                                        "(SELECT count(h1.claveMovimiento) FROM alhistoricounidadestbl h1 WHERE h1.vin = ud.vin AND h1.centroDistribucion='".$rsClaveHold['root'][0]['centroDistribucion']."' AND h1.claveMovimiento = '".$rsClaveHold['root'][0]['claveMovimiento']."' LIMIT 1) as holdUnidad, ".
                                        "(SELECT count(h1.claveMovimiento) FROM alhistoricounidadestbl h1 WHERE h1.vin = ud.vin AND h1.centroDistribucion='".$rsClaveHold['root'][0]['centroDistribucion']."' AND h1.claveMovimiento = (SELECT ge.estatus FROM cageneralestbl ge WHERE ge.tabla = 'alHoldsUnidadesTbl' AND ge.columna = 'CDTOL' AND ge.valor = '".$rsClaveHold['root'][0]['claveMovimiento']."')) as libHold ".
                                        "FROM alhistoricounidadestbl hu, alultimodetalletbl ud ".
                                        "WHERE hu.vin = ud.vin ".
                                        "AND ud.claveMovimiento IN(SELECT valor FROM cageneralestbl WHERE TABLA = 'alultimodetalletbl' AND columna = 'INVFIS') ".
                                        "AND hu.vin = '".$rs['root'][0]['vin']."' ".
                                        "GROUP BY hu.vin;";

                $rsTemp = fn_ejecuta_query($sqlCheckStr);

                $sqlHold="SELECT VALOR as claveHold, nombre as descHold from caGeneralesTbl WHERE TABLA='alHoldsUnidadesTbl'
                                                AND COLUMNA='claveHold'
                                                AND VALOR='".$rsClaveHold['root'][0]['claveMovimiento']."'
                                                AND ESTATUS='D'";
                $rsHold=fn_ejecuta_query($sqlHold);

                if($rsTemp['root'][0]['holdUnidad']> $rsTemp['root'][0]['libHold']){
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "Unidad con hold: ".
                                        $rsHold['root'][0]['claveHold']." - ".
                                        $rsHold['root'][0]['descHold'];
                    $success = false;
                }
            }

            //Revisa si la unidad esta detenida este asignada al tractor 12
            if($success && $_REQUEST['alUnidadesNumTractorHdn'] != ''){
                $sqlCheckStr =  "SELECT dt.claveMovimiento FROM alUnidadesDetenidasTbl dt ".
                                "WHERE dt.vin = '".$rs['root'][0]['vin']."' ".
                                "AND dt.fechaInicial = (SELECT MAX(dt2.fechaInicial) FROM alUnidadesDetenidasTbl dt2 ".
                                    "WHERE dt2.vin = dt.vin) ".
                                "AND centroLibera IS NULL ".
                                "AND dt.claveMovimiento = 'DT'";

                $rsTemp = fn_ejecuta_query($sqlCheckStr);

                if(sizeof($rsTemp['root']) > 0 && $_REQUEST['alUnidadesNumTractorHdn'] != 12){
                    $rs = $rsTemp;
                    $rs['root'][0]['errorMessage'] = "Unidad no se encuentra Detenida para Servicio Especial.";
                    $success = false;
                } else {
                    $detenida = 1;
                }
            }

            //Revisa que si la tarifa es 13 solo pueda ir a destino final y no en tractor 12
            /*if($success){
                if($rs['root'][0]['tarifa'] == 13){
                    if($_REQUEST['alUnidadesNumTractorHdn'] == 12 ) {
                        $rs = array("success"=>true, "records"=>1);
                        $rs['root'][0]['errorMessage'] = "Unidad no puede ir asignada al tractor 12";
                        $success = false;
                    } else {
                        $sqlCheckDestinoFinalStr = "SELECT tipoDistribuidor FROM caDistribuidoresCentrosTbl ".
                                                    "WHERE distribuidorCentro = '".$_REQUEST['alUnidadesDistribuidorHdn']."'";

                        $rsTemp = fn_ejecuta_query($sqlCheckDestinoFinalStr);

                        if($rsTemp['root'][0]['tipoDistribuidor'] != 'DI' || $rsTemp['root'][0]['tipoDistribuidor'] != 'CD'){
                            $rs = $rsTemp;
                            $rs['root'][0]['errorMessage'] = "Unidad tiene que ir a destino final";
                            $success = false;
                        }
                    }
                }
            }*/
        }
        return $rs;
    }
    function getUnidades660(){

        $lsWhereStr =   "WHERE ss.vupdate = (SELECT MAX(a6.vUpDate) ".
                        "FROM al660tbl a6 ".
                        "WHERE a6.vin = ss.vin ".
                        "AND a6.vuptime = (SELECT MAX(al.vUpTime) ".
                            "FROM al660tbl al ".
                            "WHERE al.vin = a6.vin) ) ".
                        "AND ss.dealerID != '' ".
                        "AND ss.dealerID != 'COMO' ";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "ss.numAvanzada", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "ss.vin", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }


        $sqlGet660Str = "SELECT ss.*, (SELECT DISTINCT tt.idTarifa FROM caSimbolosUnidadesTbl su, caClasificacionMarcatbl cm, caClasificacionTarifasTbl ct, caTarifasTbl tt ".
                        "WHERE  su.simboloUnidad = ss.model ".
                        "AND su.clasificacion =  cm.clasificacion ".
                        "AND su.marca = cm.marca ".
                        "AND cm.clasificacion = ct.clasificacion ".
                        "AND cm.marca = ct.marca  ".
                        "AND su.clasificacion = ct.clasificacion ".
                        "AND su.marca = ct.marca ".
                        "AND tt.idTarifa <=7 ".
                        "AND tt.idTarifa = ct.idTarifa ".
                        "AND tt.tipoTarifa = 'N' ) AS idTarifa, ".
                        "(SELECT DISTINCT tt.descripcion FROM caSimbolosUnidadesTbl su, caClasificacionMarcatbl cm, caClasificacionTarifasTbl ct, caTarifasTbl tt ".
                        "WHERE  su.simboloUnidad = ss.model ".
                        "AND su.clasificacion =  cm.clasificacion ".
                        "AND su.marca = cm.marca ".
                        "AND cm.clasificacion = ct.clasificacion ".
                        "AND cm.marca = ct.marca ".
                        "AND su.clasificacion = ct.clasificacion ".
                        "AND su.marca = ct.marca ".
                        "AND tt.idTarifa <=7 ".
                        "AND tt.idTarifa = ct.idTarifa ".
                        "AND tt.tipoTarifa = 'N' ) AS nombreTarifa, ".
                        "(SELECT DISTINCT tt.tarifa FROM caSimbolosUnidadesTbl su, caClasificacionMarcatbl cm, caClasificacionTarifasTbl ct, caTarifasTbl tt ".
                        "WHERE  su.simboloUnidad = ss.model ".
                        "AND su.clasificacion =  cm.clasificacion ".
                        "AND su.marca = cm.marca ".
                        "AND cm.clasificacion = ct.clasificacion ".
                        "AND cm.marca = ct.marca ".
                        "AND tt.idTarifa <=7 ".
                        "AND su.clasificacion = ct.clasificacion ".
                        "AND su.marca = ct.marca ".
                        "AND tt.idTarifa = ct.idTarifa ".
                        "AND tt.tipoTarifa = 'N' ) AS tarifa, ".
                        "(SELECT su.marca FROM casimbolosunidadestbl su ".
                        "WHERE su.simboloUnidad = ss.model LIMIT 1) as marca ".
                        "FROM al660tbl ss ".$lsWhereStr." GROUP BY ss.vin";

        $rs = fn_ejecuta_query($sqlGet660Str);

        //echo json_encode($sqlGet660Str);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['nombreTarifa'];
        }

        return $rs;
    }

    function getHistoriaUnidad($vin = ''){

        if($vin == ''){
            $vin = $_REQUEST['trap276VinHdn'];
        }
        //SE COMENTARON LAS VALIDACIONES DE HORA EN LO QUE SE VE SI SE USARAN O NO
        $sqlGetHistoricoUnidadStr = "SELECT hu.*, t2.idTractor, t2.tractor, t2.folio, ".                                    
                                    "(SELECT lp.fila FROM alLocalizacionPatiosTbl lp WHERE lp.vin = hu.vin) AS fila, ".
                                    "(SELECT lp2.lugar FROM alLocalizacionPatiosTbl lp2 WHERE lp2.vin = hu.vin) AS lugar, ".
                                    "(SELECT tf.descripcion FROM caTarifasTbl tf WHERE idTarifa = hu.idTarifa) AS descTarifa, ".
                                    "(SELECT tf.tarifa FROM caTarifasTbl tf WHERE idTarifa = hu.idTarifa) AS claveTarifa, ".
                                    "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='alHistoricoUnidadesTbl' AND cg.columna='claveMovimiento' AND cg.valor=hu.claveMovimiento) AS claveMovimiento ".
                                    "FROM alhistoricounidadestbl hu ".
                                    "LEFT JOIN ( ".
                                        "SELECT ut2.vin, tv2.folio, tr.idTractor, tr.tractor, hu2.claveMovimiento ".
                                        "FROM trunidadesdetallestalonestbl ut2, trtalonesviajestbl tv2, trviajestractorestbl vt, ".
                                        "catractorestbl tr, alhistoricounidadestbl hu2 ".
                                        "WHERE hu2.vin = ut2.vin ".
                                        "AND tv2.idTalon = ut2.idTalon ".
                                        "AND vt.idViajeTractor = tv2.idViajeTractor ".
                                        "AND tr.idTractor = vt.idTractor ".
                                        "AND ut2.estatus = 'AM' ".
                                        "AND tv2.claveMovimiento != 'TX' ".
                                        "AND hu2.idHistorico = ( ".
                                            "SELECT MAX(hu3.idHistorico) FROM alhistoricounidadestbl hu3 ".
                                            "WHERE hu3.vin = hu2.vin AND hu3.claveMovimiento = hu2.claveMovimiento ".
                                        ") ".
                                        "HAVING hu2.claveMovimiento = 'AM' ".
                                    ") AS t2 ".
                                    "ON hu.vin = t2.vin ".
                                    "AND hu.claveMovimiento = t2.claveMovimiento ".
                                    "WHERE hu.vin = '".$vin."' ".
                                    "ORDER BY hu.fechaEvento ";

        $rs = fn_ejecuta_query($sqlGetHistoricoUnidadStr);
        //echo json_encode($sqlGetHistoricoUnidadStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descClaveMov'] = $rs['root'][$iInt]['claveMovimiento']." - ".$rs['root'][$iInt]['nombreClaveMov'];
            $rs['root'][$iInt]['localizacionCompleta'] = $rs['root'][$iInt]['localizacionUnidad'].", ".$rs['root'][$iInt]['fila']." - ".$rs['root'][$iInt]['lugar'];
        }

        if($_REQUEST['start'] != '' && $_REQUEST['limit'] != ''){
            $rs['root'] = array_slice($rs['root'], $_REQUEST['start'], $_REQUEST['start']+$_REQUEST['limit']);
        }

        return $rs;
    }

    function addUnidadMasivo(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $vinArr = explode('|', substr($_REQUEST['alUnidadesVinHdn'], 0, -1));
        if(in_array('', $vinArr)){
            $e[] = array('id'=>'alUnidadesVinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $distArr = explode('|', substr($_REQUEST['alUnidadesDistribuidorHdn'], 0, -1));
        if(in_array('', $distArr)){
            $e[] = array('id'=>'alUnidadesDistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $simboloArr = explode('|', substr($_REQUEST['alUnidadesSimboloUnidadHdn'], 0, -1));
        if(in_array('', $simboloArr)){
            $e[] = array('id'=>'alUnidadesSimboloUnidadHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $colorArr = explode('|', substr($_REQUEST['alUnidadesColorHdn'], 0, -1));
        if(in_array('', $colorArr)){
            $e[] = array('id'=>'alUnidadesColorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $centroDistArr = explode('|', substr($_REQUEST['alUnidadesCentroDistHdn'], 0, -1));
        if(in_array('', $centroDistArr)){
            $e[] = array('id'=>'alUnidadesCentroDistHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $tarifaArr = explode('|', substr($_REQUEST['alUnidadesTarifaHdn'], 0, -1));
        if(in_array('', $tarifaArr)){
            $e[] = array('id'=>'alUnidadesTarifaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $localizacionArr = explode('|', substr($_REQUEST['alUnidadesLocalizacionUnidadHdn'], 0, -1));
        if(in_array('', $localizacionArr)){
            $e[] = array('id'=>'alUnidadesLocalizacionUnidadHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            $arrTemp = explode('|', substr($_REQUEST['alUnidadesHoldHdn'], 0,-1));
            $holdArr = array();
            for ($nInt=0; $nInt < sizeof($arrTemp); $nInt++) {
                $arrTempVin = explode('-', $arrTemp[$nInt]);
                $holdArr[$arrTempVin[0]] = $arrTempVin[1];
            }

            $vinError = array();
            $repuveArr = explode('|', substr($_REQUEST['alUnidadesFolioRepuveTxt'], 0, -1));
            $choferArr = explode('|', substr($_REQUEST['alUnidadesChoferHdn'], 0, -1));
            $observacionesArr = explode('|', substr($_REQUEST['alUnidadesObservacionesTxa'], 0, -1));

            for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {

                validaSalidas($vinArr[$nInt]);

                $sqlCheckVinStr = "SELECT vin ".
                                  "FROM alHistoricoUnidadesTbl ".
                                  "WHERE vin = '".$vinArr[$nInt]."' ".
                                  "AND claveMovimiento != 'PR' ";

                $rs = fn_ejecuta_query($sqlCheckVinStr);

                if (sizeof($rs['root']) > 0 ) {
                    $data = addHistoricoUnidad($centroDistArr[$nInt],$vinArr[$nInt],'PR',$distArr[$nInt],$tarifaArr[$nInt],
                                            $localizacionArr[$nInt],$choferArr[$nInt],$observacionesArr[$nInt], $_SESSION['idUsuario']);

                    $sqlUpdUnidad = "UPDATE alUnidadesTbl ".
                                    "SET distribuidor = '".$distArr[$nInt]."', ".
                                    "simboloUnidad = '".$simboloArr[$nInt]."', ".
                                    "color = '".$colorArr[$nInt]."' ".
                                    "WHERE vin = '".$vinArr[$nInt]."';";

                    $data = fn_ejecuta_Upd($sqlUpdUnidad);
                } else {
                    $data = addUnidad($vinArr[$nInt],$distArr[$nInt],$simboloArr[$nInt],$colorArr[$nInt],$centroDistArr[$nInt],'PR',
                                        $tarifaArr[$nInt],$localizacionArr[$nInt],$repuve[$nInt],$choferArr[$nInt],$observacionesArr[$nInt], '',$nInt*2);
                }
                if ($data['success']) {
                    if (isset($holdArr[$vinArr[$nInt]])) {
                        $arrTemp = explode(',', $holdArr[$vinArr[$nInt]]);
                        for ($mInt=0; $mInt < sizeof($arrTemp); $mInt++) {
                            $data = addHistoricoUnidad($centroDistArr[$nInt],$vinArr[$nInt],$arrTemp[$mInt], $distArr[$nInt],$tarifaArr[$nInt],$localizacionArr[$nInt],'','',$_SESSION['idUsuario'],6);
                        }
                        $data = quitarHoldUnitario($vinArr[$nInt]);
                    }
                }
                if ($data['success'] == false) {
                    array_push($vinError, $vinArr[$nInt]);
                } else {
                    $success = true;
                }
            }

            if (sizeof($vinError) > 0) {
                if ($success == true) {
                    $a['successMessage'] = getInsertarUnidadMasivoMSg();;
                    foreach ($vinError as $vin) {
                        $a['successMessage'] .= "<br>".$vin.",";
                    }
                } else if ($success == false){
                    $a['errorMessage'] = getErrorInsertarUnidadMasivo();
                    foreach ($vinError as $vin) {
                        $a['errorMessage'] .= "<br>".$vin.",";
                    }
                }
            } else {
                $a['successMessage'] = getUnidadesMasivoSuccessMsg();
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function addUnidad($vin,$distribuidor,$simbolo,$color,$centroDist,$claveMovimiento,$tarifa,$localizacion,$repuve,$chofer,$observaciones,$descripcionUnidad,$time = 0, $idUsuario = ""){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == ""){
            $e[] = array('id'=>'%VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($distribuidor == ""){
            $e[] = array('id'=>'%DistribuidorTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($simbolo == ""){
            $e[] = array('id'=>'%SimboloUnidadHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($color == ""){
            $e[] = array('id'=>'%ColorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($centroDist == ""){
            $e[] = array('id'=>'%CentroDistHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($claveMovimiento == ""){
            $e[] = array('id'=>'%ClaveMovimientoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($tarifa == ""){
            $e[] = array('id'=>'%TarifaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($idUsuario == ""){
            $idUsuario =  $_SESSION['idUsuario'];
        }

        if($a['success'] == true){

            //$descripcionUnidad = 'cmDat_capturada';
            $descripcionUnidad = ' ';

            $sqlAddUnidadStr =  "INSERT INTO alUnidadesTbl ".
                                "(vin, avanzada, distribuidor, simboloUnidad, color, folioRepuve, descripcionUnidad) ".
                                "VALUES(".
                                "'".$vin."',".
                                "'".substr($vin, -8)."',".
                                "'".$distribuidor."',".
                                "'".$simbolo."',".
                                "'".$color."',".
                                "(SELECT folioRepuve FROM alRepuveTbl WHERE vin = '".$vin."'),".
                                replaceEmptyNull("'".$descripcionUnidad."'").")";

            $rs = fn_ejecuta_Add($sqlAddUnidadStr);
            //impresionEtiquetas($_REQUEST['alUnidadesVinHdn']);

            


            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlAddUnidadStr;
                $a['successMessage'] = getUnidadesSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUnidadStr;
                //echo $a['errorMessage'];
            }
        }

        if ($a['success'] == true) {
            $rsHist = addHistoricoUnidad($centroDist,$vin,$claveMovimiento,$distribuidor,$tarifa,$localizacion,$chofer,$observaciones,$idUsuario,$time);

            $rs = fn_ejecuta_Add($sqlAddCambioHistoricoStr);

            if($rsHist['success']){
                //BUSCO EN LA TABLA DE HOLDS

                $rs = getHoldeadas();

                if(count($rs['root']) > 0){ //Si existe la Avanzada
                    if($rs['root'][0]['claveHold'] != ''){ //Si tiene un Hold
                        //INSERTO EN HISTORICO ESE HOLD

                        //$rsHist = addHistoricoUnidad($centroDist,$vin,$rs['root'][0]['claveHold'],$distribuidor,$tarifa,$localizacion,$chofer,$observaciones,$idUsuario,$time);

                        //$rs = fn_ejecuta_Add($sqlAddHistoricoUnidadStr);

                        //BORRO LA AVANZADA DE LA TABLA DE HOLDS
                        $sqlDeleteHold = "DELETE FROM alHoldsUnidadesTbl WHERE vin = '".$vin."'";

                        $rs = fn_ejecuta_query($sqlDeleteHold);
                    }
                }

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlAddUnidadStr." ".$rsHist['sql'];
                    $a['successMessage'] = getUnidadesSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddHistoricoUnidadStr;
                }
            } else {
                $a = $rsHist;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function updUnidad(){
        $a = array();
        $e = array();
        $a['success'] = true;
        $updInt = 0;

        if($_REQUEST['alUnidadesVinHdn'] == ""){
            $e[] = array('id'=>'alUnidadesVinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success'] == true){

            $sqlUpdUnidadStr = "UPDATE alUnidadesTbl SET";

            if (isset($_REQUEST['alUnidadesDistribuidorTxt']) && $_REQUEST['alUnidadesDistribuidorTxt'] != '') {
                $sqlUpdUnidadStr .= " distribuidor ='".$_REQUEST['alUnidadesDistribuidorTxt']."'";
                $updInt++;
            }
            if (isset($_REQUEST['alUnidadesSimboloUnidadHdn']) && $_REQUEST['alUnidadesSimboloUnidadHdn'] != '') {
                if ($updInt > 0) {
                    $sqlUpdUnidadStr .= ",";
                }

                $sqlUpdUnidadStr .= " simboloUnidad ='".$_REQUEST['alUnidadesSimboloUnidadHdn']."'";
                $updInt++;
            }
            if (isset($_REQUEST['alUnidadesColorHdn']) && $_REQUEST['alUnidadesColorHdn'] != '') {
                if ($updInt > 0) {
                    $sqlUpdUnidadStr .= ",";
                }

                $sqlUpdUnidadStr .= " color ='".$_REQUEST['alUnidadesColorHdn']."'";
                $updInt++;
            }
            if (isset($_REQUEST['alUnidadesFolioRepuveTxt']) && $_REQUEST['alUnidadesFolioRepuveTxt'] != '') {
                if ($updInt > 0) {
                    $sqlUpdUnidadStr .= ",";
                }

                $sqlUpdUnidadStr .= " folioRepuve ='".$_REQUEST['alUnidadesFolioRepuveTxt']."'";
                $updInt++;
            }

            if ($updInt > 0) {
                $sqlUpdUnidadStr .= " WHERE vin='".$_REQUEST['alUnidadesVinHdn']."'";

                $rs = fn_ejecuta_Upd($sqlUpdUnidadStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['sql'] = $sqlUpdUnidadStr;
                    $a['successMessage'] = getUnidadesUpdMsg();
                    $a['id'] = $_REQUEST['alUnidadesVinHdn'];
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdUnidadStr;
                }

            } else {
                $sqlUpdUnidadStr = "";
                $a['successMessage'] = getUnidadesNotUpdMsg();
                $a['id'] = $_REQUEST['alUnidadesVinHdn'];
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        json_encode($a);
    }

    function addHistoricoUnidad($RQcentroDist,$RQvin,$RQclaveMovimiento,$RQdist,$RQtarifa,$RQlocalizacion,$RQchofer,$RQobservaciones,$RQusuario="",$idtalon){
        $idtalonAgrega=$idtalon;
                       

        $a = array();
        $e = array();
        $a['success'] = true;

        if($RQcentroDist == "") {
                $e[] = array('id'=>'CentroDistHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        if($RQvin == "") {
                $e[] = array('id'=>'VinHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        if($RQclaveMovimiento == "") {
                $e[] = array('id'=>'ClaveMovimientoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        if($RQtarifa == "") {
                $e[] = array('id'=>'TarifaTxt','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        if($RQdist == "") {
                $e[] = array('id'=>'DistribuidorTxt','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        if($RQlocalizacion == "") {
                $e[] = array('id'=>'LocalizacionUnidadHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }

        if($RQusuario == ""){
            $RQusuario = $_SESSION['idUsuario'];
        }

        if ($a['success'] == true) {
            $fechaEvento = date("Y-m-d H:i:s" );
            /* time()+$RQtime*/
            
            if($RQclaveMovimiento == 'CD'){

                ///este update se agrego para el cabecero de la consulta de unidades...
                $sqlUpdAlUnidad ="UPDATE alunidadestbl SET distribuidor='".$RQdist."' WHERE vin='".$RQvin."'";
                fn_ejecuta_query($sqlUpdAlUnidad);

                $selDistribuidor="SELECT idRegion FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro='".$RQdist."'";
                $rsDistribuidor=fn_ejecuta_query($selDistribuidor);

                $sqlUpdAlUnidad ="UPDATE alhistoricounidadestbl SET distribuidor='".$RQdist."' WHERE vin='".$RQvin."' AND claveMovimiento='EP'";
                fn_ejecuta_query($sqlUpdAlUnidad);

                $sqlUpdAlUnidad ="UPDATE alUltimoDetalleTbl SET distribuidor='".$RQdist."' WHERE vin='".$RQvin."'";
                fn_ejecuta_query($sqlUpdAlUnidad);

                if ($rsDistribuidor['root'][0]['idRegion'] =='8') {
                    $RQtarifa='10';
                }                
            }

            if($RQclaveMovimiento == 'HO'){
                
                $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                            "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                            "claveChofer, observaciones, usuario, ip) ".
                            "VALUES(".
                            "'".$RQcentroDist."',".
                            "'".$RQvin."',".
                            "NOW() - INTERVAL 2 SECOND,".
                            "'".$RQclaveMovimiento."',".
                            "'".$RQdist."',".
                            "'".$RQtarifa."',".
                            "'".$RQlocalizacion."',".
                            replaceEmptyNull($RQchofer).",".
                            "'".$RQobservaciones."',".
                            "'".$RQusuario."',".
                            "'".getClientIP()."')";

                $rs = fn_ejecuta_query($sqlAddCambioHistoricoStr);

            }else{
                
                $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                                            "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                            "claveChofer, observaciones, usuario, ip,idtalon) ".
                                            "VALUES(".
                                            "'".$RQcentroDist."',".
                                            "'".$RQvin."',".
                                            "NOW() + INTERVAL 1 SECOND,".
                                            "'".$RQclaveMovimiento."',".
                                            "'".$RQdist."',".
                                            "'".$RQtarifa."',".
                                            "'".$RQlocalizacion."',".
                                            replaceEmptyNull($RQchofer).",".
                                            "'".$RQobservaciones."',".
                                            "'".$RQusuario."',".
                                            "'".getClientIP()."',".
                                            replaceEmptyNull($idtalonAgrega).")";

                $rs = fn_ejecuta_query($sqlAddCambioHistoricoStr);
            }
            //echo json_encode($sqlAddCambioHistoricoStr);
            if($RQclaveMovimiento === 'SL'){
                insertActividadesPmp($RQvin,$RQusuario);
            }elseif($RQclaveMovimiento === 'EP'){

                $updPmp="UPDATE alactividadespmptbl SET claveEstatus='AP' WHERE centroDistribucion ='".$RQcentroDist."' and vin='".$RQvin."' AND fechaEvento <=NOW();";
                fn_ejecuta_query($updPmp);

                $delPmp = "DELETE FROM alactividadespmptbl WHERE centroDistribucion ='".$RQcentroDist."' and vin='".$RQvin."' and claveEstatus ='PE'";
                fn_ejecuta_query($delPmp);
            }


            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $sqlGetUltimoEstatusRealStr = "SELECT idHistorico, vin, claveMovimiento ".
                                               "FROM alHistoricoUnidadesTbl ".
                                                "WHERE vin = '".$RQvin."' ".
                                                "AND claveMovimiento NOT IN ( ".
                                                    "SELECT valor FROM caGeneralesTbl WHERE tabla = 'alHistoricoUnidadesTbl' ".
                                                    "AND columna = 'nvalidos'".
                                                ") ".
                                                "ORDER BY idHistorico DESC ".
                                                "LIMIT 1 ";

                $rs = fn_ejecuta_query($sqlGetUltimoEstatusRealStr);

                if(sizeof($rs) > 0){
                    $RQclaveMovimiento = $rs['root'][0]['claveMovimiento'];
                    $data = addUltimoDetalle($RQvin, $RQcentroDist, $fechaEvento, $RQclaveMovimiento, $RQdist, $RQtarifa, $RQlocalizacion, $RQchofer, $RQobservaciones, $RQusuario);

                    if($data['success'] == true){
                        $a['sql'] = $sqlAddCambioHistoricoStr;
                        $a['successMessage'] = getHistoricoUnidadMsg();
                    } else {
                        $a['sql'] = $data['sql'];
                        $a['errorMessage'] = $data['errorMessage'];
                    }

                } else {
                    $a['sql'] = $sqlAddCambioHistoricoStr;
                    $a['successMessage'] = getHistoricoUnidadMsg();
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddCambioHistoricoStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function addHistoricoUnidadMasivo($RQcentroDist,$RQvin,$RQclaveMovimiento,$RQdist,$RQtarifa,$RQlocalizacion,$RQchofer,$RQobservaciones)
    {
        $a = array();
        $e = array();
        $a['success'] = true;

        $centroDistArr = explode('|', substr($RQcentroDist,0,-1));
        if(in_array('', $centroDistArr)) {
                $e[] = array('id'=>'CentroDistHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $vinArr = explode('|', substr($RQvin,0,-1));
        if(in_array('', $vinArr)) {
                $e[] = array('id'=>'VinHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $claveMovArr = explode('|', substr($RQclaveMovimiento,0,-1));
        if(in_array('', $claveMovArr)) {
                $e[] = array('id'=>'ClaveMovimientoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $distArr = explode('|', substr($RQdist,0,1));
        if(in_array('', $distArr)) {
                $e[] = array('id'=>'DistribuidorTxt','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $tarifaArr = explode('|', substr($RQtarifa,0,1));
        if(in_array('', $tarifaArr)) {
                $e[] = array('id'=>'TarifaTxt','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $localizacionArr = explode('|', substr($RQlocalizacion,0,-1));
        if(in_array('', $localizacionArr)) {
                $e[] = array('id'=>'LocalizacionUnidadHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $choferArr = explode('|', substr($RQchofer,0,-1));
        if(in_array('', $choferArr)) {
                $e[] = array('id'=>'ChoferHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }

        $observacionesArr = explode('|', $RQobservaciones);

        if ($a['success'] == true) {
            for($nInt = 0; $nInt < sizeof($vinArr); $nInt++){
                $data = addHistoricoUnidad($centroDistArr[$nInt],$vinArr[$nInt],$claveMovArr[$nInt], $distArr[$nInt],$tarifaArr[$nInt],$localizacionArr[$nInt],$choferArr[$nInt],$observacionesArr[$nInt],$_SESSION['idUsuario'], $nInt*2);

                if ($data['success'] == false) {
                    $a['success'] = false;
                    $a['errorMessage'] = $data['errorMessage'];
                } else {
                    $a['successMessage'] = getHistoricoUnidadMasivoMsg();
                }
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function bloquearUnidad($vin){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == ""){
            $e[] = array('id'=>'VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if ($a['success'] == true) {
            $sqlBloquearUnidadStr = "INSERT INTO alUnidadesTmp ".
                                   "VALUES(".
                                   "'".$vin."',".
                                   "'".substr($vin, -8)."',".
                                   replaceEmptyNull("'".$_REQUEST['modulo']."'").",".
                                   "'".$_SESSION['idUsuario']."',".
                                   "'".$_SERVER['REMOTE_ADDR']."',".
                                   "'".date("Y-m-d H:i:s")."')";

            $rs = fn_ejecuta_Add($sqlBloquearUnidadStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlBloquearUnidadStr;
                $a['successMessage'] = getUnidadesBloquearMsg();
                $a['root'][0]['bloqueo'] = true;
            } else {
                $a['errorMsg'] = getErrorBloquearUnidad();
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlBloquearUnidadStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function updUltimoHistorico($vin, $claveMovimiento, $distribuidor){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == ""){
            $e[] = array('id'=>'VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($claveMovimiento == "" && $distribuidor == ""){
            $e[] = array('id'=>'ClaveMovimientoHdn|DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            $sqlGetUltimoHistoricoStr = "SELECT h.idHistorico FROM alHistoricoUnidadesTbl h ".
                                        "WHERE h.fechaEvento = (SELECT MAX(h2.fechaEvento) FROM alHistoricoUnidadesTbl h2 ".
                                            "WHERE h2.vin = h.vin) ".
                                        "AND vin = '".$vin."'";

            $rs = fn_ejecuta_query($sqlGetUltimoHistoricoStr);
            $idHistorico = $rs['root'][0]['idHistorico'];

            if($idHistorico != ''){
                $sqlUpdUltimoHistoricoStr = "UPDATE alHistoricoUnidadesTbl SET ";

                if($claveMovimiento != ''){
                    $sqlUpdUltimoHistoricoStr .= "claveMovimiento = '".$claveMovimiento."' ";
                    $successMessage = getHistClaveMovUpdMsg();
                    if($distribuidor != ''){
                        $sqlUpdUltimoHistoricoStr .= ", distribuidor = '".$distribuidor."' ";
                        $successMessage = getHistoricoUpdMsg();
                    }
                } else if($distribuidor != ''){
                    $sqlUpdUltimoHistoricoStr .= "distribuidor = '".$distribuidor."' ";
                    $successMessage = getHistDistribuidorUpdMsg();
                }

                $sqlUpdUltimoHistoricoStr .=    "WHERE idHistorico = ".$idHistorico;

                fn_ejecuta_Upd($sqlUpdUltimoHistoricoStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlUpdUltimoHistoricoStr;
                    $data = updUltimoDetalle($vin, $claveMovimiento, $distribuidor);
                    if($data['success']){
                        $a['successMessage'] = $successMessage;
                    } else {
                        $a = $data;
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdUltimoHistoricoStr;
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function updUltimoDetalle($vin, $claveMovimiento, $distribuidor){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == ""){
            $e[] = array('id'=>'VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($claveMovimiento == "" && $distribuidor == ""){
            $e[] = array('id'=>'ClaveMovimientoHdn|DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            $sqlUpdUltimoDetalleStr = "UPDATE alUltimoDetalleTbl SET ";

            if($claveMovimiento != ''){
                $sqlUpdUltimoDetalleStr .= "claveMovimiento = '".$claveMovimiento."' ";
                if($distribuidor != ''){
                    $sqlUpdUltimoDetalleStr .= ", distribuidor = '".$distribuidor."' ";
                }
            } else if($distribuidor != ''){
                $sqlUpdUltimoDetalleStr .= "distribuidor = '".$distribuidor."' ";
            }

            $sqlUpdUltimoDetalleStr .=    "WHERE vin = '".$vin."'";

            fn_ejecuta_Upd($sqlUpdUltimoDetalleStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlUpdUltimoDetalleStr;
                $a['successMessage'] = getHistoricoUpdMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdUltimoDetalleStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function liberarUnidad($vin){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == ""){
            $e[] = array('id'=>'VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if ($a['success'] == true) {
            $sqlLiberarUnidadStr = "DELETE FROM alUnidadesTmp ".
                                   "WHERE vin='".$vin."' ";

            $rs = fn_ejecuta_query($sqlLiberarUnidadStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlLiberarUnidadStr;
                $a['successMessage'] = getUnidadesLiberarMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlLiberarUnidadStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function liberarUnidadMasivo($vinLista){
        $a = array();
        $e = array();
        $a['success'] = true;

        $vinArr = explode('|', substr($vinLista, 0, -1));

        $sqlLiberarUnidadStr = "DELETE FROM alUnidadesTmp ".
                                       "WHERE vin IN (";

        $contDatos = 0;
        if ($a['success'] == true) {
            for($i = 0; $i < sizeof($vinArr); $i++){
                if($vinArr[$i] != ''){

                    if($i > 0 && $i != count($vinArr)){
                        $sqlLiberarUnidadStr .= ",";
                    }
                    $sqlLiberarUnidadStr .= "'".$vinArr[$i]."'";

                }
            }
            $sqlLiberarUnidadStr .= ");";

            $rs = fn_ejecuta_query($sqlLiberarUnidadStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlLiberarUnidadStr;
                $a['successMessage'] = getUnidadesLiberarMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlLiberarUnidadStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function getDetenidas(){
        $lsWhereStr = "WHERE u.vin = h.vin ".
                      "AND h.fechaEvento=(".
                      "SELECT MAX(h1.fechaEvento) ".
                      "FROM alHistoricoUnidadesTbl h1 ".
                      "WHERE h1.vin = u.vin) ".
                      "AND ud.vin = u.vin ".
                      "AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 ".
                       "WHERE ud2.vin = u.vin) ".
                       "AND ud.claveMovimiento ='UD' ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "h.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesSimboloUnidadHdn'], "u.simboloUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "u.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "u.avanzada", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetDetenidasStr = "SELECT u.*, h.*, ud.claveMovimiento AS claveDetenida,".
                              "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h.claveMovimiento ".
                              "AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreEstatus, ".
                              "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d ".
                              "WHERE d.distribuidorCentro=u.distribuidor) AS nombreDistribuidor, ".
                              "(SELECT c.descripcion FROM cacolorunidadestbl c, caSimbolosUnidadesTbl su WHERE c.color=u.color ".
                                "AND c.marca=su.marca AND su.simboloUnidad=u.simboloUnidad) AS nombreColor ".
                              "FROM alUnidadesTbl u, alUltimoDetalleTbl h, alUnidadesDetenidasTbl ud ".
                              $lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetDetenidasStr);

        echo json_encode($rs);
    }

    function getNoDetenidas(){
        $lsWhereStr = "WHERE u.vin = h.vin ".
                      /*"AND h.fechaEvento=(".
                      "SELECT MAX(h1.fechaEvento) ".
                      "FROM alHistoricoUnidadesTbl h1 ".
                      "WHERE h1.vin = u.vin) ".*/
                      "AND (ud.claveMovimiento IS NULL OR ud.claveMovimiento != 'UL') ".
                      "AND h.claveMovimiento IN('EP','RP','CT','RU','RA')" ;

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "h.distribuidor", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesSimboloUnidadHdn'], "u.simboloUnidad", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "u.vin", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "u.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesCentroDistHdn'], "h.centroDistribucion",2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetNoDetenidasStr = "SELECT u.*, h.*, 0 AS invalido, '' AS msgInvalido, ud.claveMovimiento AS claveDetencion, ".
                              "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h.claveMovimiento ".
                              "AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreEstatus, ".
                              "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d ".
                              "WHERE d.distribuidorCentro=u.distribuidor) AS nombreDistribuidor, ".
                              "(SELECT c.descripcion FROM cacolorunidadestbl c, caSimbolosUnidadesTbl su WHERE c.color=u.color ".
                              "AND c.marca=su.marca AND su.simboloUnidad=u.simboloUnidad) AS nombreColor ".
                              "FROM alUnidadesTbl u, alUltimoDetalleTbl h ".
                              "LEFT JOIN alUnidadesDetenidasTbl ud ON ud.vin = h.vin ".
                              "AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 ".
                              "WHERE ud2.vin = h.vin) ".$lsWhereStr." ORDER BY u.vin";
                //echo "$sqlGetNoDetenidasStr<br>";
        $rs = fn_ejecuta_query($sqlGetNoDetenidasStr);
        
        $i = 0;
        foreach ($rs['root'] as $idx => $row) { 
            //$rs['root'][$i][''] = $row[''];
            if($rs['root'][$i]['claveMovimiento'] == 'RP' && ($row['idTarifa'] == 5 || $row['idTarifa'] == 6))
            {
                    $rs['root'][$i]['invalido'] = 1;
                    $rs['root'][$i]['msgInvalido'] = 'Unidad con tarifa especial.';
            }
            if($rs['root'][$i]['invalido'] == 0)
            {
                    if(isset($_REQUEST['validaCentro']) && $_REQUEST['validaCentro'] == 1)
                    {
                            if($row['centroDistribucion'] != 'CDTOL' && $row['centroDistribucion'] != 'CDSAL' && 
                                 $row['centroDistribucion'] != 'CDAGS' && $row['centroDistribucion'] != 'CDANG')
                            {
                                    $rs['root'][$i]['invalido'] = 1;
                                    $rs['root'][$i]['msgInvalido'] = 'Centro de Distribucion '.$row['centroDistribucion'].' no valido.';
                            }
                    }
            }
            if($rs['root'][$i]['invalido'] == 0)
            {
                    if($row['claveDetencion'] == 'UD')
                    {
                            $rs['root'][$i]['invalido'] = 1;
                            $rs['root'][$i]['msgInvalido'] = 'La Unidad '.$rs['root'][$i]['vin'].' ya ha sido detenida anteriormente.';
                    }
            }            
            $i++;
        }

        echo json_encode($rs);
    }

    function detenerUnidad($vin, $centroDistribucion, $time = 0){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == "") {
            $e[] = array('id'=>'%Vin%','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            //Obtiene el proximo número de movimiento
            $sqlGetProxMovStr = "SELECT IFNULL(MAX(numeroMovimiento), 0)+1 AS numeroMovimiento FROM alUnidadesDetenidasTbl ".
                                    "WHERE vin = '".$vin."'";
            $rsMov = fn_ejecuta_query($sqlGetProxMovStr);
            $numeroMovimiento = $rsMov['root'][0]['numeroMovimiento'];

            $sqlDetenerUnidadStr = "INSERT INTO alUnidadesDetenidasTbl (vin, claveMovimiento, centroDetiene, fechaInicial, numeroMovimiento) ".
                                    "VALUES (".
                                        "'".$vin."',".
                                        "'UD',".
                                        "'".$centroDistribucion."',".
                                        //"'".date("Y-m-d H:i:s", time()+$time)."',".
                                        "now(),".                                        
                                        $numeroMovimiento.")";

            fn_ejecuta_Add($sqlDetenerUnidadStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlDetenerUnidadStr;
                $a['successMessage'] = getUnidadDetenidaSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDetenerUnidadStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function detencionUnidades(){
        $a = array();
        $a['success'] = true;
        $a['msjResponse'] = 'Unidades detenidas satisfactoriamente';

        $vinArr = json_decode($_POST['arrVIN'],true);                   //CHK
        //$vinArr = json_decode($_GET['arrVIN'],true);                      //CHK
                
        /*if($a['success']) {
            $sqlDetenerDistSimboloStr = "INSERT INTO alSimbolosDetenidasTbl (simboloUnidad, distribuidor, centroDistribucion) ".
                                        "VALUES (".
                                            replaceEmptyNull("'".$_REQUEST['alUnidadesSimboloUnidadHdn']."'").",".
                                            replaceEmptyNull("'".$_REQUEST['alUnidadesDistribuidorHdn']."'").",".
                                            "'".$_REQUEST['alUnidadesCentroDistHdn']."')";

            fn_ejecuta_Add($sqlDetenerDistSimboloStr);

            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success'] = false;
                $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $sqlLiberarDetencionUnidadStr;
            }
        }*/

        //Distribuidor 
        if($_REQUEST['tipo'] == '1' && $_REQUEST['restriccion'] == '1')
        {
                $sql = "DELETE FROM alDistCentrosDetenidosTbl".
                             " WHERE distribuidor = '".$_REQUEST['distribuidor']."'".
                             " AND centroDistribucion IN ('CDTOL', 'CDSAL', 'CDAGS', 'CDANG')";
                //echo "<br>$sql<br>";
                fn_ejecuta_query($sql);         //CHK
            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success'] = false;
                $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $sql;
            }                   
        }
        else
        {
                $i = 0;
                foreach($vinArr as $index => $row){
                //Obtiene el proximo número de movimiento
                $sql = "SELECT IFNULL(MAX(numeroMovimiento), 0)+1 AS numeroMovimiento FROM alUnidadesDetenidasTbl ".
                       "WHERE vin = '".$row['vin']."'";
                //echo "<br>$sql<br>";
                $rsMov = fn_ejecuta_query($sql);
                $numeroMovimiento = $rsMov['root'][0]['numeroMovimiento'];
                //Si no lo es se inserta su movimiento de detencion en alUnidadesDetenidasTbl                
                $sql = "INSERT INTO alUnidadesDetenidasTbl (vin, claveMovimiento, centroDetiene, fechaInicial, numeroMovimiento) VALUES (".
                          "'".$row['vin']."',".
                          "'UD',".
                          "'".$_REQUEST['alUnidadesCentroDistHdn']."',".
                          "now(),".
                          $numeroMovimiento.")";
                //echo "<br>$sql<br>";
                fn_ejecuta_query($sql);     //CHK

                if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $sql;
                    break;
                }
                }
             
                if ($a['success'] == true) {
                    if($_REQUEST['tipo'] == 1)
                    {
                            for($x=0;$x<=3;$x++)
                            {                           
                                            switch($x){
                                                case 0:
                                                    $centroDistribucion = 'CDTOL';
                                                    break;
                                                case 1:
                                                    $centroDistribucion = 'CDSAL';
                                                    break;
                                                case 2:
                                                    $centroDistribucion = 'CDAGS';
                                                    break;
                                                case 3:
                                                    $centroDistribucion = 'CDANG';
                                                    break;                                                                                      
                                                default:                                            
                                            }                           
                                    $sql = "INSERT INTO alDistCentrosDetenidosTbl (distribuidor, centroDistribucion) VALUES (".
                                                 "'".$_REQUEST['distribuidor']."', ".
                                                 "'".$centroDistribucion."')";
                                    fn_ejecuta_query($sql);         //CHK
                                if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success'] = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $sql;
                                }
                            }                    
                    }
                    
                    /*
                    if($_REQUEST['alUnidadesDistribuidorHdn'] != "" && $_REQUEST['alUnidadesSimboloUnidadHdn'] == ""){
                        $a['msjResponse'] = getUnidadesDetencionDistribuidor($_REQUEST['alUnidadesDistribuidorHdn']);
                    } elseif ($_REQUEST['alUnidadesDistribuidorHdn'] == "" && $_REQUEST['alUnidadesSimboloUnidadHdn'] != "") {
                        $a['msjResponse'] = getUnidadesDetencionSimbolo($_REQUEST['alUnidadesSimboloUnidadHdn']);
                    } elseif ($_REQUEST['alUnidadesDistribuidorHdn'] != "" && $_REQUEST['alUnidadesSimboloUnidadHdn'] != "") {
                        $a['msjResponse'] = getUnidadesDetencionDistSimbolo($_REQUEST['alUnidadesDistribuidorHdn'], $_REQUEST['alUnidadesSimboloUnidadHdn']);
                    } elseif ($_REQUEST['alUnidadesDistribuidorHdn'] == "" && $_REQUEST['alUnidadesSimboloUnidadHdn'] == ""){
                        $a['msjResponse'] = getUnidadesDetencionVin($vinArr[0]);
                    }
                    */
                }               
        }

        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function liberarDetencionUnidades(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['alUnidadesCentroDistHdn'] == "") {
            $e[] = array('id'=>'alUnidadesCentroDistHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $vinArr = explode('|', substr($_REQUEST['alUnidadesVinHdn'], 0, -1));

        if($a['success']) {
            $sqlLiberarDistSimboloStr = "DELETE FROM alSimbolosDetenidasTbl ".
                                        "WHERE centroDistribucion = '".$_REQUEST['alUnidadesCentroDistHdn']."' ";

            if($_REQUEST['alUnidadesDistribuidorHdn'] != ""){
                $sqlLiberarDistSimboloStr .= "AND distribuidor = '".$_REQUEST['alUnidadesDistribuidorHdn']."' ";
            }
            if($_REQUEST['alUnidadesSimboloUnidadHdn'] != ""){
                $sqlLiberarDistSimboloStr .= "AND simboloUnidad = '".$_REQUEST['alUnidadesSimboloUnidadHdn']."' ";
            }

            fn_ejecuta_query($sqlLiberarDistSimboloStr);

            if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlLiberarDistSimboloStr;
            }
        }

        if ($a['success'] == true) {
            for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                if($vinArr[$nInt] != ''){
                    //Obtiene el proximo número de movimiento
                    $sqlGetMovStr = "SELECT MAX(numeroMovimiento) AS numeroMovimiento FROM alUnidadesDetenidasTbl ".
                                    "WHERE vin = '".$vinArr[$nInt]."'";
                    $rsMov = fn_ejecuta_query($sqlGetMovStr);
                    $numeroMovimiento = $rsMov['root'][0]['numeroMovimiento'];

                    $sqlLiberarDetencionUnidadStr = "UPDATE alUnidadesDetenidasTbl ".
                                                    "SET centroLibera ='".$_REQUEST['alUnidadesCentroDistHdn']."',".
                                                    "claveMovimiento = 'UL',".
                                                    "fechaFinal = '".date("Y-m-d H:i:s", time()+$nInt*2)."' ".
                                                    "WHERE vin='".$vinArr[$nInt]."' ".
                                                    "AND numeroMovimiento = ".$numeroMovimiento;

                    $rs = fn_ejecuta_Upd($sqlLiberarDetencionUnidadStr);

                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $a['sql'] = $sqlLiberarDetencionUnidadStr;
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlLiberarDetencionUnidadStr;
                    }
                }
            }
        }

        if ($a['success'] == true) {
            if($_REQUEST['alUnidadesDistribuidorHdn'] != "" && $_REQUEST['alUnidadesSimboloUnidadHdn'] == ""){
                $a['successMessage'] = getUnidadesLiberarDetencionDistribuidor($_REQUEST['alUnidadesDistribuidorHdn']);
            } elseif ($_REQUEST['alUnidadesDistribuidorHdn'] == "" && $_REQUEST['alUnidadesSimboloUnidadHdn'] != "") {
                $a['successMessage'] = getUnidadesLiberarDetencionSimbolo($_REQUEST['alUnidadesSimboloUnidadHdn']);
            } elseif ($_REQUEST['alUnidadesDistribuidorHdn'] != "" && $_REQUEST['alUnidadesSimboloUnidadHdn'] != "") {
                $a['successMessage'] = getUnidadesLiberarDetencionDistSimbolo($_REQUEST['alUnidadesDistribuidorHdn'], $_REQUEST['alUnidadesSimboloUnidadHdn']);
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getHoldeadas(){

        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesClaveMovimientoHdn'], "claveHold", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "distribuidorCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesFechaEventoTxt'], "fechaEvento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetHoldeadasStr = "SELECT * FROM alHoldsUnidadesTbl ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetHoldeadasStr);

        return $rs;
    }

    function holdUnidades($listaVin, $listaClavesMovimiento, $listaDist){
        $a['success'] = true;
        $a['ok'] = 0;
        $a['fail'] = 0;

        $vinArr = explode("|", substr($listaVin, 0, -1));
        if (in_array('', $vinArr)) {
           $a['success'] = false;
           $e[] = array('id'=>'Lista de Vins','msg'=>getRequerido());
           $a['errorMessage'] = getErrorRequeridos();
        }
        $claveArr = explode("|", substr($listaClavesMovimiento, 0, -1));
        if (in_array('', $claveArr)) {
           $a['success'] = false;
           $e[] = array('id'=>'Lista de Claves de Movimientos','msg'=>getRequerido());
           $a['errorMessage'] = getErrorRequeridos();
        }

        if($a['success']){
            $distArr = explode("|", substr($listaDist, 0, -1));

            for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                if ($nInt != 0) {
                    $sqlHoldUnidadesStr .= ",";
                }

                $sqlHoldUnidadesStr = "INSERT INTO alHoldsUnidadesTbl ".
                                      "(vin, claveHold, distribuidorCentro, fechaEvento) ".
                                      "VALUES(".
                                        "'".$vinArr[$nInt]."', ".
                                        "'".$claveArr[$nInt]."', ".
                                        replaceEmptyNull("'".$distArr[$nInt]."'").", ".
                                        "'".date("Y-m-d H:i:s")."')";

                $rs = fn_ejecuta_Add($sqlHoldUnidadesStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $a['ok'] += 1;
                } else {
                       $a['fail'] += 1;
                }
            }

            if ($a['fail'] > 0) {
                $a['success'] = false;
                $a['errorMessage'] = getHoldUnidadesMessage($a['ok'], $a['fail']);
            } else {
                 $a['successMessage'] = getHoldUnidadesMessage($a['ok'], $a['fail']);
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function quitarHoldUnidades(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $vinArr = explode("|", substr($_REQUEST['alUnidadesVinHdn'], 0, -1));
        if (in_array('', $vinArr)) {
            $e[] = array('id'=>'alUnidadesVinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $sqlQuitarHoldUnidades = "DELETE FROM alHoldsUnidadesTbl ";

        if ($a['success'] == true) {
            for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                if ($nInt == 0) {
                    $sqlQuitarHoldUnidades .= "WHERE vin = '".$vinArr[$nInt]."' ";
                } else {
                    $sqlQuitarHoldUnidades .= "OR vin = '".$vinArr[$nInt]."' ";
                }
            }

            $rs = fn_ejecuta_query($sqlQuitarHoldUnidades);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlQuitarHoldUnidades;
                $a['successMessage'] = getUnidadesQuitarHold(sizeof($vinArr));
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlQuitarHoldUnidades;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function quitarHoldUnitario($vin, $hold){
        $a = array();
        $e = array();
        $a['success'] = true;

        if ($vin == "") {
            $e[] = array('id'=>'alUnidadesVinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            $sqlQuitarHoldUnidades = "DELETE FROM alHoldsUnidadesTbl ".
                                        "WHERE vin = '".$vin."' ".
                                        "AND claveHold = '".$hold."'";

            $rs = fn_ejecuta_query($sqlQuitarHoldUnidades);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlQuitarHoldUnidades;
                $a['successMessage'] = getUnidadesQuitarHold(sizeof($vinArr));
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlQuitarHoldUnidades;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function insertarEntradaPatio($centroDist, $conLocalizacion, $flotilla, $vin, $tarifa, $distribuidor, $simbolo, $retrabajo,$virtuales, $idUsuario = ""){
        $a = array();
        $e = array();
        $a['success'] = true;
        $unidadesError = array();
        $unidadesSinPatio = array();

       if($centroDist == ''){
                $e[] = array('id'=>'alUnidadesCentroDistHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        if($conLocalizacion == ''){
                $e[] = array('id'=>'alUnidadesConLocalizacionHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        if($flotilla == ''){
                $e[] = array('id'=>'alUnidadesFlotillaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $vinArr = explode('|', substr($vin, 0,-1));
        if(in_array('', $vinArr)){
                $e[] = array('id'=>'alUnidadesVinHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $tarifaArr = explode('|', substr($tarifa, 0,-1));
        if(in_array('', $tarifaArr)){
                $e[] = array('id'=>'alUnidadesTarifaTxt','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $distArr = explode('|', substr($distribuidor, 0,-1));
        if(in_array('', $distArr)){
                $e[] = array('id'=>'alUnidadesDistribuidorTxt','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }
        $simboloArr = explode('|', substr($simbolo, 0,-1));
        if(in_array('', $simboloArr)){
                $e[] = array('id'=>'alUnidadesSimboloUnidadHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
        }

        if($idUsuario == ""){
            $idUsuario = $_SESSION['idUsuario'];
        }

        //VALIDAR QUE LA PLAZA DEL DISTRIBUIDOR TENGA KILOMETROS ASIGNADOS EN EL CENTRO DE DISTRIBUCION
        //ASI COMO QUE EXISTA TARIFA DE TRANSPORTACION PARA ESTA UNIDAD
        if($a['success']){
            $distribuidoresSinKm = "";
            $simbolosSinTarifa = "";
            $kilometros = array();
            $tarifasTransportacion = array();

            foreach ($distArr as $distribuidor) {
                $sqlCheckKm = "SELECT kp.kilometros FROM caKilometrosPlazaTbl kp ".
                              "WHERE kp.idPlazaOrigen = (".
                              "SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                              "WHERE dc.distribuidorCentro = '".$centroDist."') ".
                              "AND kp.idPlazaDestino = (".
                              "SELECT dc.idPlaza FROM caDistribuidoresCentrosTbl dc ".
                              "WHERE dc.distribuidorCentro = '".$distribuidor."') ";

                $rs = fn_ejecuta_query($sqlCheckKm);

                if(sizeof($rs['root']) == 0){
                    $a['success'] = false;
                    $distribuidoresSinKm .= $distribuidor.",";
                } else {
                    array_push($kilometros, $rs['root'][0]['kilometros']);
                }
            }

            $distribuidoresSinKm = substr($distribuidoresSinKm, 0, -1);

            for($n=0; $n < sizeof($vinArr); $n++) {
                $sqlCheckTarifa = "SELECT (SELECT tt.importe FROM caTarifasTransportacionTbl tt ".
                                  "WHERE tt.centroDistribucion = '".$centroDist."' ".
                                  "AND tt.tipoCarga = (".
                                    "SELECT cm.tipoCarga FROM caClasificacionMarcaTbl cm ".
                                    "WHERE cm.clasificacion = (".
                                        "SELECT su.clasificacion FROM caSimbolosUnidadesTbl su ".
                                        "WHERE su.simboloUnidad = '".$simboloArr[$n]."')".
                                    "AND cm.marca = (".
                                        "SELECT su.marca FROM caSimbolosUnidadesTbl su ".
                                        "WHERE su.simboloUnidad = '".$simboloArr[$n]."'))".
                                  "AND tt.region = (".
                                    "SELECT dc.zonaGeografica FROM caDistribuidoresCentrosTbl dc ".
                                    "WHERE dc.distribuidorCentro = '".$distArr[$n]."') ".
                                  "AND tt.tipoTarifa = 'FI') AS tarifaFija, ".
                                  "(SELECT tt.importe FROM caTarifasTransportacionTbl tt ".
                                  "WHERE tt.centroDistribucion = '".$centroDist."' ".
                                  "AND tt.tipoCarga = (".
                                    "SELECT cm.tipoCarga FROM caClasificacionMarcaTbl cm ".
                                    "WHERE cm.clasificacion = (".
                                        "SELECT su.clasificacion FROM caSimbolosUnidadesTbl su ".
                                        "WHERE su.simboloUnidad = '".$simboloArr[$n]."')".
                                    "AND cm.marca = (".
                                        "SELECT su.marca FROM caSimbolosUnidadesTbl su ".
                                        "WHERE su.simboloUnidad = '".$simboloArr[$n]."'))".
                                  "AND tt.region = (".
                                    "SELECT dc.zonaGeografica FROM caDistribuidoresCentrosTbl dc ".
                                    "WHERE distribuidorCentro = '".$distArr[$n]."') ".
                                  "AND tt.tipoTarifa = 'VA') AS tarifaVariable ";

                $rs = fn_ejecuta_query($sqlCheckTarifa);

                if(sizeof($rs['root']) > 0){
                    if($rs['root'][0]['tarifaFija'] == "" || $rs['root'][0]['tarifaVariable'] == ""){
                        $a['success'] = false;
                        $simbolosSinTarifa .= $simboloArr[$n].",";
                    } else {
                        array_push($tarifasTransportacion, array($rs['root'][0]['tarifaFija'],$rs['root'][0]['tarifaVariable']));
                    }
                }
            }

            $simbolosSinTarifa = substr($simbolosSinTarifa, 0, -1);
        }
        $simbolosSinTarifa = "";

        //SE GENERA LA ENTRADA A PATIO
        if($a['success'] == true) {
            $retrabajoArr = explode('|', substr($retrabajo, 0,-1));
            $patio = '';

            if($conLocalizacion == 1){
                $data = addLocalizacionPatios($simbolo,$distribuidor, $vin);

                $a['success'] = $data['success'];
            }

            $time = 0;

            for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                if($conLocalizacion == 1){
                    $rsPatio = fn_ejecuta_query("SELECT patio FROM alLocalizacionPatiosTbl WHERE vin = '".$vinArr[$nInt]."'");
                    $patio = isset($rsPatio['root'][0]['patio']) ? $rsPatio['root'][0]['patio'] : '';

                    if($patio == ''){
                        $patio = $centroDist;
                        if(!in_array($vinArr[$nInt], $unidadesError))
                            array_push($unidadesSinPatio, $vinArr[$nInt]);
                    }

                } else {
                    $patio = $centroDist;
                }

                //FLOTILLA
                if($flotilla == 2){
                    $data = addHistoricoUnidad($centroDist,$vinArr[$nInt],'FL', $distArr[$nInt],$tarifaArr[$nInt],$patio,'','',$idUsuario);
                    $time += 2;
                    if(!$data['success']){
                        $a['success'] = false;
                        if(!in_array($vinArr[$nInt], $unidadesError))
                            array_push($unidadesError, $vinArr[$nInt]);
                    }
                }

                //HOMOLOGACION
                $sqlChomologa = "SELECT 1 ".
                                "FROM cadistribuidorescentrostbl ".
                                "WHERE homologacion = '1' ".
                                "AND tipoDistribuidor = 'CD' ".
                                "AND distribuidorCentro ='".$centroDist."'";

                $rsHomologa = fn_ejecuta_query($sqlChomologa);


                $sqlCheckSimboloStr = "SELECT 1 FROM caSimbolosUnidadesTbl ".
                                        "WHERE simboloUnidad = '".$simboloArr[$nInt]."' ".
                                        "AND tipoOrigenUnidad = 'I' ".
                                        "AND homologacion = '1' ".
                                        "AND marca not in ('HY','KI','GM')";

                $rsSimbolo = fn_ejecuta_query($sqlCheckSimboloStr);

                if(sizeof($rsSimbolo['root']) > 0 && substr($distArr[$nInt], 0,1) != 'H' && sizeof($rsHomologa['root']) > 0 ){
                    $rsDist = fn_ejecuta_query("SELECT tipoDistribuidor FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$distArr[$nInt]."'");
                    if($rsDist['root'][0]['tipoDistribuidor'] != 'DX'){
                        $data = addHistoricoUnidad($centroDist,$vinArr[$nInt],'HO', $distArr[$nInt],$tarifaArr[$nInt],$patio,'','',$idUsuario);
                        $time += 2;
                        if(!$data['success']){
                            $a['success'] = false;
                            if(!in_array($vinArr[$nInt], $unidadesError))
                                array_push($unidadesError, $vinArr[$nInt]);
                        }
                    }
                }
                
                    $sql = "SELECT * FROM alDistCentrosDetenidosTbl WHERE distribuidor = '".$distArr[$nInt]."' AND centroDistribucion = '".$_REQUEST['alUnidadesCentroDistHdn']."'";
                    //echo "<br>$sql<br>";
                    $rsCDist = fn_ejecuta_query($sql);
                    
                    /*
                    if($rsCDist['records'] > 0)
                    {
                        $sql = "SELECT * FROM alUnidadesDetenidasTbl WHERE vin = '".$vinArr[$nInt]."' AND claveMovimiento = 'UD'";
                        //echo "<br>$sql<br>";
                        $rsUnDet = fn_ejecuta_query($sql);
                        
                        if($rsUnDet['records'] == 0)
                        {
                                //Obtiene el proximo número de movimiento
                                $sql = "SELECT IFNULL(MAX(numeroMovimiento), 0)+1 AS numeroMovimiento FROM alUnidadesDetenidasTbl ".
                                       "WHERE vin = '".$vinArr[$nInt]."'";
                                //echo "<br>$sql<br>";
                                $rsMovAux = fn_ejecuta_query($sql);
                                $numeroMovimiento = $rsMovAux['root'][0]['numeroMovimiento'];
                                $sql = "INSERT INTO alUnidadesDetenidasTbl (vin, claveMovimiento, centroDetiene, fechaInicial, numeroMovimiento) VALUES (".
                                          "'".$vinArr[$nInt]."',".
                                          "'UD',".
                                          "'".$_REQUEST['alUnidadesCentroDistHdn']."',".
                                          "now(),".
                                          $numeroMovimiento.")";
                                //echo "<br>$sql<br>";
                                fn_ejecuta_Add($sql);
                                if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success'] = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $sql;
                                    break;
                                }                               
                        }                               
                    }
                    */

                //ENTRADA A PATIO
                $data = addHistoricoUnidad($centroDist,$vinArr[$nInt],'EP', $distArr[$nInt],$tarifaArr[$nInt],$patio,'','',$idUsuario);

                if ($virtuales=='1') {
                    $updHistorico="UPDATE alhistoricounidadestbl SET observaciones='UNIDAD VIRTUAL' WHERE vin='".$vinArr[$nInt]."' AND claveMovimiento='EP'";
                    fn_ejecuta_Upd($updHistorico);
                }
                
                $time += 2;
                if($data['success']){
                    $importeVariable = floatval($tarifasTransportacion[$nInt][1]) * floatval($kilometros[$nInt]);

                    //SE INSERTA EN LA TABLA DE FACTURACION DE TRANSPORTACION
                    $sqlCheckFacturacion =  "SELECT 1 FROM faFacturacionTransportacionTbl ".
                                            "WHERE vin = '".$vinArr[$nInt]."'";

                    $rs = fn_ejecuta_query($sqlCheckFacturacion);

                    if(sizeof($rs['root']) > 0){
                        $sqlAddUpdFacturacionEP =  "UPDATE faFacturacionTransportacionTbl SET ".
                                                "origen = '".$centroDist."',".
                                                "region = (SELECT zonaGeografica FROM caDistribuidoresCentrosTbl ".
                                                    "WHERE distribuidorCentro = '".$distArr[$nInt]."'), ".
                                                "tipoCarga = (SELECT cm.tipoCarga FROM caClasificacionMarcaTbl cm ".
                                                    "WHERE cm.clasificacion = (".
                                                        "SELECT su.clasificacion FROM caSimbolosUnidadesTbl su ".
                                                        "WHERE su.simboloUnidad = '".$simboloArr[$nInt]."')".
                                                    "AND cm.marca = (".
                                                        "SELECT su.marca FROM caSimbolosUnidadesTbl su ".
                                                        "WHERE su.simboloUnidad = '".$simboloArr[$nInt]."')),".
                                                "kilometros = ".$kilometros[$nInt].",".
                                                "importeFijo = ".$tarifasTransportacion[$nInt][0].",".
                                                "importeVariable = ".$importeVariable.",".
                                                "fechaEp = '".date("Y-m-d")."' ".
                                                "WHERE vin = '".$vinArr[$nInt]."'";

                        fn_ejecuta_Upd($sqlAddUpdFacturacionEP);
                    } else {
                        $sqlAddUpdFacturacionEP =   "INSERT INTO faFacturacionTransportacionTbl (vin, avanzada, origen,".
                                                    "region, tipoCarga, kilometros, importeFijo, importeVariable, fechaEp) VALUES (".
                                                    "'".$vinArr[$nInt]."',".
                                                    "'".substr($vinArr[$nInt], -8)."',".
                                                    "'".$centroDist."',".
                                                    "(SELECT zonaGeografica FROM caDistribuidoresCentrosTbl ".
                                                    "WHERE distribuidorCentro = '".$distArr[$nInt]."'),".
                                                    "(SELECT cm.tipoCarga FROM caClasificacionMarcaTbl cm ".
                                                    "WHERE cm.clasificacion = (".
                                                        "SELECT su.clasificacion FROM caSimbolosUnidadesTbl su ".
                                                        "WHERE su.simboloUnidad = '".$simboloArr[$nInt]."')".
                                                    "AND cm.marca = (".
                                                        "SELECT su.marca FROM caSimbolosUnidadesTbl su ".
                                                        "WHERE su.simboloUnidad = '".$simboloArr[$nInt]."')),".
                                                    $kilometros[$nInt].",".
                                                    $tarifasTransportacion[$nInt][0].",".
                                                    $importeVariable.",".
                                                    "'".date("Y-m-d")."')";

                        fn_ejecuta_Add($sqlAddUpdFacturacionEP);
                    }



                    if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success'] = false;
                        if(!in_array($vinArr[$nInt], $unidadesError))
                            array_push($unidadesError, $vinArr[$nInt]);
                        }

                } else {
                    $a['success'] = false;
                    if(!in_array($vinArr[$nInt], $unidadesError))
                        array_push($unidadesError, $vinArr[$nInt]);
                }


                $selStr = "SELECT ud.vin, ".
                        "ud.idtarifa, ". 
                        "ud.distribuidor, ".  
                        "ud.localizacionUnidad,".        
                        "dist.tipodistribuidor, ".
                        "CASE WHEN sim.marca IN ('AR','CD','DG','FI','JP','RA') THEN 'FCA' ".
                        "ELSE '' ".
                        "END as marca ".
                        "FROM alultimodetalletbl ud, ".
                        "cadistribuidorescentrostbl dist, ".        
                        "alunidadestbl un, ".
                        "casimbolosunidadestbl sim ".
                        "WHERE ud.centroDistribucion IN ('CDSLP','CMDAT','CDTOL','CDSAL','CDAGS','CDVER','CDSFE','CDLZC','CDANG','CDLCL') ".
                        "AND ud.vin = '".$vinArr[$nInt]."' ".
                        "AND ud.idtarifa != 5 ".
                        "AND dist.distribuidorcentro = ud.distribuidor ".
                        "AND un.vin = ud.vin ".
                        "AND sim.simboloUnidad = un.simboloUnidad ".
                        "AND ud.vin not in (select vin from alTransportacionEstandarTrackingObtTbl where vin ='".$vinArr[$nInt]."') ".
                        "AND ud.vin not in (select vin from alTransExportacionTrackingObtTbl where vin ='".$vinArr[$nInt]."') ".
                        "AND ud.distribuidor like 'M8%' ";

                $alTransExpRst = fn_ejecuta_query($selStr);
                if ($alTransExpRst['records'] > 0) {
                    if ($alTransExpRst['root'][0]['marca'] == 'FCA') {
                        $insStr = "INSERT INTO ";
                        if ($alTransExpRst['root'][0]['tipodistribuidor'] == 'DI') {
                            $insStr .= "alTransportacionEstandarTrackingObtTbl";
                        } else {
                            $insStr .= "alTransExportacionTrackingObtTbl";
                        }
                        $insStr .= " (vin, fechaom,fechaupload , estatusHistorico) VALUES ('".$vinArr[$nInt]."',NOW(),NOW(),'EP')";
                        //fn_ejecuta_query($insStr);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['errorMessage'] = $_SESSION['error_sql'];
                            $a['sql']         = $insStr;
                            break;
                        }
                    }
                }

                //DETENIDAS
                $detenida = false;

                $sqlConsultaDT = "SELECT COUNT(*) AS detensionGlobal, (SELECT 1  FROM  alsimbolosdetenidastbl sd WHERE sd.simboloUnidad = '".$simboloArr[$nInt]."' AND sd.centroDistribucion = '".$centroDist."') as simboloDetenido, ".
                                 " (SELECT 1  FROM  alsimbolosdetenidastbl sd WHERE sd.distribuidor = '".$distArr[$nInt]."' AND sd.centroDistribucion = '".$centroDist."') as distribuidorDtenido ".
                                 "FROM alDistCentrosDetenidosTbl di ".
                                 "WHERE distribuidor = '".$distArr[$nInt]."' ".
                                 "AND centroDistribucion = '".$_REQUEST['alUnidadesCentroDistHdn']."'";                                 

                $rsDT =  fn_ejecuta_query($sqlConsultaDT);

                foreach ($rsDT['root'] as $detencion_01) {
                    if($detencion_01['detensionGlobal'] == '1'){
                        //Si esta detenida por distribuidor en el Catalogo
                        $detenida = true;
                        break;
                    } else if($detencion_01['simboloDetenido'] !== NULL){
                        //Si esta detenida por simbolo/centroDistribucion
                        $detenida = true;
                        break;
                    } else if($detension_01['distribuidorDtenido'] !== NULL){
                        //Si esta detenida por simbolo o distribuidor/centroDistribucion
                        $detenida = true;
                    }
                }

                /*$sqlCheckDetenidasStr = "SELECT simboloUnidad, distribuidor FROM alSimbolosDetenidasTbl ".
                                        "WHERE centroDistribucion = '".$centroDist."' ".
                                        "AND (distribuidor = '".$distArr[$nInt]."' ".
                                            "OR simboloUnidad = '".$simboloArr[$nInt]."')";

                $rsDetenida = fn_ejecuta_query($sqlCheckDetenidasStr);

                foreach ($rsDetenida['root'] as $detencion) {
                    if($detencion['distribuidor'] == $distArr[$nInt]){
                        if($detencion['simboloUnidad']  == $simboloArr[$nInt] || $detencion['simboloUnidad']  == ''){
                            //Si esta detenida por distribuidor o distribuidor/simbolo
                            $detenida = true;
                            break;
                        }
                    } else if($detencion['simboloUnidad'] == $simboloArr[$nInt] && $detencion['distribuidor']  == ''){
                        //Si esta detenida por simbolo o distribuidor/simbolo
                        $detenida = true;
                        break;
                    }
                }*/

                //echo $detenida;

                if($detenida == true){
                    if(substr($distArr[$nInt], 0, 1) == 'H'){
                        //Si es Mercedes
                        $data = addHistoricoUnidad($centroDist,$vinArr[$nInt],'DT', $distArr[$nInt],$tarifaArr[$nInt],$patio,'','',$idUsuario, $time);
                        $time += 2;
                        if(!$data['success']){
                            $a['success'] = false;
                            if(!in_array($vinArr[$nInt], $unidadesError))
                                array_push($unidadesError, $vinArr[$nInt]);
                        }
                    } else {
                        //O no...
                        $data = detenerUnidad($vinArr[$nInt], $centroDist, $time);
                        $time += 2;
                        if(!$data['success']){
                            $a['success'] = false;
                            if(!in_array($vinArr[$nInt], $unidadesError))
                                array_push($unidadesError, $vinArr[$nInt]);
                        }
                    }
                    $time += 2;
                } else {
                    /*if($rsDist['root'][0]['tipoDistribuidor'] == 'DX' && $centroDist == 'CDTOL'){
                        $data = addHistoricoUnidad($centroDist, $vinArr[$nInt],'DX', $distArr[$nInt],$tarifaArr[$nInt],$patio,'','',$idUsuario, $time);
                        $time += 2;
                        if(!$data['success']){
                            $a['success'] = false;
                            if(!in_array($vinArr[$nInt], $unidadesError))
                                array_push($unidadesError, $vinArr[$nInt]);
                        }
                    }*/
                }

                //HOLDS
                $rsHolds = fn_ejecuta_query("SELECT * FROM alHoldsUnidadesTbl WHERE vin = '".$vinArr[$nInt]."'");

                foreach ($rsHolds['root'] as $hold) {
                    $data = addHistoricoUnidad($centroDist,$vinArr[$nInt],$hold['claveHold'], $distArr[$nInt],$tarifaArr[$nInt],$patio,'','',$idUsuario, $time);
                    $time += 2;
                    if(!$data['success'])
                        $a['success'] = false;
                    else
                        $data = quitarHoldUnitario($vinArr[$nInt], $hold['claveHold']);
                }

                //RETRABAJO
                if($retrabajoArr[$nInt] == '1'){
                    $data = addRetrabajo($vinArr[$nInt], $simboloArr[$nInt], 'SR');

                    if(!$data['success']){
                        $a['success'] = false;
                        if(!in_array($vinArr[$nInt], $unidadesError))
                            array_push($unidadesError, $vinArr[$nInt]);
                    }
                }
            }
        }

        if($a['success']){
                    $a['successMessage'] = getInsertarEntradaPatio();
        } else {
            $a['errorMessage'] = "";
            if($distribuidoresSinKm != ""){
                $a['errorMessage'] .= "Distribuidores sin Kilometros asignados a ".
                                        $centroDist.": ".$distribuidoresSinKm."<br>";
            }

            if($simbolosSinTarifa != ""){
                $a['errorMessage'] .= "Simbolos sin Tarifas Fija y Variable: ".
                                        $simbolosSinTarifa."<br>";
            }

            if(sizeof($unidadesSinPatio) > 0){
                $a['errorMessage'] .= "Unidades Procesadas: <br>";

                foreach ($unidadesSinPatio as $unidad) {
                    $a['errorMessage'] .= $unidad."<br>";
                }

                $a['errorMessage'] .= "<br>";
            }

            if(sizeof($unidadesError) > 0){
                $a['errorMessage'] .= "Unidades con Error en Entrada a Patio: <br>";

                foreach ($unidadesError as $unidad) {
                    $a['errorMessage'] .= $unidad."<br>";
                }
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();

        return $a;
    }

    function getHistoricoUnidades($vin = ''){
        if($vin == ""){
            $vin = $_REQUEST['alHistoricoUnidadesVinHdn'];
        }

        $lsWhereStr = "WHERE u.vin = h.vin ".
                      "AND h.idTarifa = t.idTarifa ".
                      "AND sm.simboloUnidad = u.simboloUnidad ".
                      " AND h.clavemovimiento!='CA' ".
                      "AND u.distribuidor = dc.distribuidorCentro ";
                      //"AND h.claveMovimiento in ('PR','SL') ";


        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alHistoricoUnidadesAvanzadaHdn'], "u.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($vin, "u.vin", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alHistoricoUnidadesCentroDistHdn'], "h.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alHistoricoUnidadesEstatusHdn'], "h.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alHistoricoUnidadesMarcaHdn'], "sm.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetUnidadesStr = "SELECT u.*, h.*, t.descripcion AS nombreTarifa, t.tipoTarifa, dc.descripcionCentro AS nombreDistribuidor, ".
                             "dc.estatus AS estatusDistribuidor, dc.tipoDistribuidor, sm.descripcion AS nombreSimbolo, ".
                             "(SELECT lp.fila FROM alLocalizacionPatiosTbl lp WHERE lp.vin = u.vin) AS fila,".
                             "(SELECT lp2.lugar FROM alLocalizacionPatiosTbl lp2 WHERE lp2.vin = u.vin) AS lugar,".
                             "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h.claveMovimiento ".
                                "AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreEstatus, ".
                             "(SELECT c.descripcion FROM caColorUnidadesTbl c WHERE u.color = c.color ".
                                "AND sm.marca = c.marca) AS nombreColor, ".
                             "(SELECT g2.nombre FROM caGeneralesTbl g2 WHERE g2.tabla = 'caDistribuidoresCentrosTbl' ".
                                "AND g2.columna='tipoDistribuidor' AND g2.valor = dc.tipoDistribuidor) AS nombreTipoDistribuidor ".
                             "FROM alUnidadesTbl u, alHistoricoUnidadesTbl h, caTarifasTbl t, caSimbolosUnidadesTbl sm, ".
                             "caDistribuidoresCentrosTbl dc ".$lsWhereStr." ORDER BY h.fechaEvento " ;

        $rs = fn_ejecuta_query($sqlGetUnidadesStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descDist'] = $rs['root'][$fechaInicialt]['distribuidor']." - ".$rs['root'][$iInt]['nombreDistribuidor'];
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['idTarifa']." - ".$rs['root'][$iInt]['nombreTarifa'];
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['nombreSimbolo'];
            $rs['root'][$iInt]['descColor'] = $rs['root'][$iInt]['color']." - ".$rs['root'][$iInt]['nombreColor'];
            $rs['root'][$iInt]['localizacionCompleta'] = $rs['root'][$iInt]['localizacionUnidad'].", ".$rs['root'][$iInt]['fila']." - ".$rs['root'][$iInt]['lugar'];
            $rs['root'][$iInt]['nombreEstatus'] = $rs['root'][$iInt]['claveMovimiento']." - ".$rs['root'][$iInt]['nombreEstatus'];
        }

     return $rs;
    }

    function updateObservacionesHistorico(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $idHistoricoArr = explode('|', substr($_REQUEST['alUnidadesIdHistoricoHdn'], 0, -1));
        if(in_array('', $idHistoricoArr)){
           $e[] = array('id'=>'alUnidadesIdHistoricoHdn','msg'=>getRequerido());
           $a['errorMessage'] = getErrorRequeridos();
           $a['success'] = false;
        }

        if ($a['success'] == true) {
            $observacionesArr = explode('|', substr($_REQUEST['alUnidadesObservacionesTxa'], 0, -1));

            for ($i=0; $i < sizeof($idHistoricoArr); $i++) {
                $sqlUpdObservacionesHistStr = "UPDATE alHistoricoUnidadesTbl ".
                                              "SET observaciones='".$observacionesArr[$i]."' ".
                                              "WHERE idHistorico=".$idHistoricoArr[$i];

                $rs = fn_ejecuta_Upd($sqlUpdObservacionesHistStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['successMessage'] = getUnidadesUpdObservaciones();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdObservacionesHistStr;
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getRepuve(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap135FolioRepuveHdn'], "folioRepuve", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap135MarcaHdn'], "marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap135CentroDistHdn'], "centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap135TidHdn'], "tid", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap135FechaTxt'], "fechaEvento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap135EstatusHdn'], "estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetRepuveStr = "SELECT * FROM alRepuveTbl ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetRepuveStr);

        echo json_encode($rs);
    }

    function getUltimosMovimientosUnidad($vin, $ultimosMovimientos){
        $success = true;

        $ultMovArr = explode('|', substr($ultimosMovimientos, 0, -1));

        $lsWhereStr = "WHERE vin = '".$vin."' ".
                        "ORDER BY fechaEvento DESC LIMIT ".sizeof($ultMovArr);

        $sqlGetUltimosMovStr = "SELECT * FROM alHistoricoUnidadesTbl ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetUltimosMovStr);

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            //echo $rs['root'][$nInt]['claveMovimiento'] ." - ". $ultMovArr[$nInt];
            if($rs['root'][$nInt]['claveMovimiento'] != $ultMovArr[$nInt]){
                $success = false;
                break;
            }
        }

        return array('success' => $success);;
    }

    function getUltimoDetalle(){
        $lsWhereStr = "WHERE ud.idTarifa = ct.idTarifa ".
                       "AND ud.vin = au.vin";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "ud.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "au.avanzada", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesClaveMovimientoHdn'], "ud.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesFechaEventoTxt'], "ud.fechaEvento", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "ud.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesTarifaHdn'], "ud.idTarifa", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesLocalizacionUnidadHdn'], "ud.localizacionUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesClaveChofer'], "ud.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        $sqlGetUltimoDetalleStr = "SELECT ud.*,au.avanzada,(SELECT cd.descripcionCentro FROM cadistribuidoresCentrostbl cd ".
                         "WHERE ud.CentroDistribucion = cd.distribuidorCentro) AS descCentroDistribucion, ".
                         "(SELECT cd.descripcionCentro FROM cadistribuidoresCentrostbl cd ".
                         "WHERE ud.distribuidor = cd.distribuidorCentro) AS descDistribuidor, ".
                         "(SELECT cd.descripcionCentro FROM cadistribuidoresCentrostbl cd ".
                         "WHERE ud.localizacionUnidad = cd.distribuidorCentro) AS descLocalizacion, ".
                         "ct.Descripcion AS descTarifa, ".
                         "(SELECT cc.apellidoPaterno FROM cachoferestbl cc ".
                         "WHERE cc.claveChofer = ud.claveChofer) AS apellidoPaterno, ".
                         "(SELECT cc.apellidoMaterno FROM cachoferestbl cc ".
                         "WHERE ud.claveChofer = cc.claveChofer) AS apellidoMaterno, ".
                         "(SELECT cc.nombre FROM cachoferestbl cc ".
                         "WHERE ud.claveChofer = cc.claveChofer) AS nombre, ".
                         "(SELECT au2.vin FROM alUnidadesTmp au2 WHERE ud.vin = au2.vin) AS temp, ".
                         "(SELECT ue.vin FROM trunidadesembarcadastbl ue WHERE ud.vin = ue.vin) AS embarcadas, ".
                         "(SELECT 1 FROM alUnidadesTmp ue WHERE ud.vin = ue.vin) AS bloqueada ".
                         "FROM alUltimoDetalleTbl ud, caTarifasTbl ct, alUnidadesTbl au ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetUltimoDetalleStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['idTarifa']." - ".$rs['root'][$iInt]['nombreTarifa'];
        }

        echo json_encode($rs);
    }

    function addUltimoDetalle($vin, $centroDist, $fechaEvento, $claveMovimiento, $distribuidor, $tarifa, $localizacion, $chofer, $observaciones, $idUsuario =""){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == ""){
            $e[] = array('id'=>'%VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($centroDist == ""){
            $e[] = array('id'=>'%CentroDistribucionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($fechaEvento == ""){
            $e[] = array('id'=>'%FechaEventoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($claveMovimiento == ""){
            $e[] = array('id'=>'%ClaveMovimientoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($distribuidor == ""){
            $e[] = array('id'=>'%DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($tarifa == ""){
            $e[] = array('id'=>'%IdTarifaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($localizacion == ""){
            $e[] = array('id'=>'%LocalizacionUnidadHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($idUsuario == ""){
            $idUsuario = $_SESSION['idUsuario'];
        }

        if ($a['success'] == true) {
            $sqlExistsUltimoDetalleStr = "SELECT 1 FROM alUltimoDetalleTbl ".
                                         "WHERE vin = '".$vin."'";

            $rs = fn_ejecuta_query($sqlExistsUltimoDetalleStr);

            if(sizeof($rs['root']) > 0) {
                $sqlAddUpdUltimoDetalleStr = "UPDATE alUltimoDetalleTbl SET ".
                                                "centroDistribucion = '".$centroDist."', ".
                                                //"fechaEvento = '".$fechaEvento."',".
                                                "fechaEvento = now(),".
                                                "claveMovimiento = '".$claveMovimiento."',".
                                                "distribuidor = '".$distribuidor."',".
                                                "idTarifa = ".$tarifa.",".
                                                "localizacionUnidad = '".$localizacion."',".
                                                "claveChofer = ".replaceEmptyNull($chofer).",".
                                                "observaciones = '".$observaciones."',".
                                                "usuario = ".$idUsuario.",".
                                                "ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                                "WHERE vin = '".$vin."'";
                $rs = fn_ejecuta_Upd($sqlAddUpdUltimoDetalleStr);

            } else {
                $sqlAddUpdUltimoDetalleStr = "INSERT INTO alUltimoDetalleTbl (vin, centroDistribucion, fechaEvento, ".
                                                "claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, ".
                                                "observaciones, usuario, ip) VALUES (".
                                             "'".$vin."',".
                                             "'".$centroDist."',".
                                             "'".date("Y-m-d H:i:s")."',".
                                             "'".$claveMovimiento."',".
                                             "'".$distribuidor."',".
                                             $tarifa.",".
                                             "'".$localizacion."',".
                                             replaceEmptyNull($chofer).",".
                                             "'".$observaciones."',".
                                             $idUsuario.",".
                                             "'".$_SERVER['REMOTE_ADDR']."')";

                $rs = fn_ejecuta_Add($sqlAddUpdUltimoDetalleStr);
            }
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlAddUpdUltimoDetalleStr;
                $a['successMessage'] = getUltimoDetalleMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUpdUltimoDetalleStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function getUnidadesControlLlaves(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesRecuperaclaveMovimientoHdn'], "h.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesRecuperaVinHdn'], "u.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesRecuperaAvanzadaHdn'], "u.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetUnidadesControlLlavesStr = "SELECT u.*, h.* ".
                             "FROM alunidadestbl u ".
                             "join alhistoricounidadestbl h on u.vin = h.vin ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetUnidadesControlLlavesStr);
        echo json_encode($rs);
    }

    //Funcion que se utiza para en el Txt del grid de altaUnidades(trap001) para validar unidades
    function getValidacionAltaUnidad(){

        $rsUnidades = getUnidades();
        //echo json_encode ($rsUnidades);

        if (sizeof($rsUnidades['root']) > 1) {//Si Hay más de 1 Avanzada

            $success = $rsUnidades;
            $success['root'][0]['success'] = true;
            $success['root'][0]['existUnidad'] = true;
            $success['root'][0]['es660'] = false;
            $success['root'][0]['successMsg'] = 'Avanzada Duplicada, elige un VIN';
            return $success;


        } else if (sizeof($rsUnidades['root']) == 1) {//Si la unidad ya está en el sistema

            $rsBloqueadas = getBloqueadas(); //Verifico que no este bloqueada...

            if (sizeof($rsBloqueadas['root']) > 0) {
                $success['root'][0] = array('success' => false, 'errorMsg' => 'Unidad Bloqueada int&eacute;ntelo m&aacute;s tarde');
                $success['root'][0]['success'] = false;
                $success['root'][0]['es660'] = false;
                return $success;
            }

            /*-----------------------------------------------------------------------------------------------
             * VERIFICA EN EL HISTÓRICO SI YA TIENE PRESENTADA PR -- ACTIVAR SI SE DESEA BUSCAR POR RECORRIDO
             *
             */
            $sqlHistorico =  "SELECT claveMovimiento FROM alHistoricoUnidadesTbl WHERE vin = '".$rsUnidades['root'][0]['vin']."'";
            $rsHistorico = fn_ejecuta_query($sqlHistorico);
            for($i = 0; $i < sizeof($rsHistorico['root']); $i++){
                $isPresentada = $rsHistorico['root'][$i]['claveMovimiento'] == 'PR';
                if ($isPresentada) {
                    break;

                }
            }

            $isSO = true;
            $isSN = true;
            $sqlCO =  "SELECT claveMovimiento FROM alHistoricoUnidadesTbl WHERE  vin = '".$rsUnidades['root'][0]['vin']."'";
            $rsCO = fn_ejecuta_query($sqlCO);
            for($i = 0; $i < sizeof($rsCO['root']); $i++){
                $isCO = $rsCO['root'][$i]['claveMovimiento'] == 'CO';
                if ($isCO == true) {
                    $sqlSO =  "SELECT claveMovimiento FROM alHistoricoUnidadesTbl WHERE vin = '".$rsUnidades['root'][0]['vin']."'";
                    $rsSO = fn_ejecuta_query($sqlSO);
                    for($i = 0; $i < sizeof($rsSO['root']); $i++){
                        $isSO = $rsSO['root'][$i]['claveMovimiento'] == 'SO';
                        if ($isSO == true) {
                            break;
                        }
                    }
                }
                else{
                    break;
                }
            }

            $sqlEO =  "SELECT claveMovimiento FROM alHistoricoUnidadesTbl WHERE  vin = '".$rsUnidades['root'][0]['vin']."'";
            $rsEO = fn_ejecuta_query($sqlEO);
            for($i = 0; $i < sizeof($rsEO['root']); $i++){
                $isEO = $rsEO['root'][$i]['claveMovimiento'] == 'EO';
                if ($isEO == true) {
                    $sqlSN =  "SELECT claveMovimiento FROM alHistoricoUnidadesTbl WHERE vin = '".$rsUnidades['root'][0]['vin']."'";
                    $rsSN = fn_ejecuta_query($sqlSN);
                    for($i = 0; $i < sizeof($rsSN['root']); $i++){
                        $isSN = $rsSN['root'][$i]['claveMovimiento'] == 'SN';
                        if ($isSN == true) {
                            break;
                        }
                    }
                }
                else{
                    break;
                }
            }

            if ($isPresentada == true) { //Verifico se clave de movimiento que sea valida para presentar
                $success['root'][0] = array('success'=> false, 'errorMsg' =>'Unidad Ya Presentada');
                return $success;
            }
            if ($isSO == false) { //Verifico se clave de movimiento que sea valida para presentar
                $success['root'][0] = array('success'=> false, 'errorMsg' =>'Unidad en contension');
                return $success;
            }
            if ($isSN == false) { //Verifico se clave de movimiento que sea valida para presentar
                $success['root'][0] = array('success'=> false, 'errorMsg' =>'Unidad en Campaña');
                return $success;
            }
            //------------------------------------------------------------------------------------------------

            //--------------QUITAR SI SE HABILITA EL PROCESO DE ARRIBA----------------------------------------
            //--------------AGREGAR SI SE DESHABILITA EL PROCESO DE ARRIBA------------------------------------
            /*if ($rsUnidades['root'][0]['claveMovimiento'] == 'PR') { //Verifico se clave de movimiento que sea valida para presentar
                $success['root'][0] = array('success'=> false, 'errorMsg' =>'Unidad Ya Presentada');
                $success['root'][0]['es660'] = false;
                return $success;
            }*/
            //-------------------------------------------------------------------------------------------------

            //-----------------DESCOMENTAR SI DEBEN EXISTIR CLAVES VALIDAS PARA EL ALTA
            /*$sql = "SELECT tabla, columna, valor, nombre, estatus, idioma " .
                           "FROM caGeneralesTbl WHERE tabla = 'trap001' ".
                           "AND columna ='validos' ".
                           "AND valor = '".$_SESSION['usuCompania']."' ".
                           "AND estatus = '".$rsUnidades['root'][0]['claveMovimiento']."'";*/

            //$rsValido = fn_ejecuta_query($sql);

            //print_r($rsValido);
            //if (sizeof($rsValido['root']) > 0) {
                $success = $rsUnidades;
                $success['root'][0]['success'] = true;
                $success['root'][0]['existUnidad'] = true;
                $success['root'][0]['es660'] = false;
                $success['root'][0]['trashData'] = true;
            /*} else {
                $success['root'][0] = array('success'=> false, 'errorMsg' =>'Unidad Ya Existe');
                $success['root'][0]['es660'] = false;
            }*/

        } else {

            $rs660 = getUnidades660();

            if (sizeof($rs660['root']) == 0) {//Si no existe en getUnidades660()...Regresa 0 registros y deja continuar!

                $success =  $rs660;
                $success['root'][0]['existUnidad'] = false;
                $success['root'][0]['success'] = true;
                $success['root'][0]['es660'] = true;

            } else if(sizeof($rs660['root']) == 1) {//Si existe 1 lo regresa para autocompletar

                $rsBloqueadas = getBloqueadas(); //Verifica que no este bloqueada
                if (sizeof($rsBloqueadas['root']) > 0) {
                    $success['root'][0] = array('success' => false, 'errorMsg' => 'Unidad Bloqueada intentalo m&aacute;s tarde');
                    $success['root'][0]['success'] = false;
                    $success['root'][0]['es660'] = true;
                    return $success;
                }

                //1.-Verificar el símbolo
                //1.1.-Verificar la Clasificación
                //2.-Verificar el color
                //3.-Verificar el distribuidor
                //*Hacerlo con subqueries DESPUÉS...


                //VERIFICAR EN SÍMBOLO---------------
                $sqlSimbolo = "SELECT * FROM caSimbolosUnidadesTbl WHERE simboloUnidad = '".$rs660['root'][0]['model']."'";
                $rsSim = fn_ejecuta_query($sqlSimbolo);

                if ($rsSim['root'][0]['clasificacion'] == 'XX') {
                    $errors = "El S&iacute;mbolo: ".$rs660['root'][0]['model']." no est&aacute; actualizado, favor de actualizarlo </br>";
                    $success['root'][0] = array('success' => false, 'errorMsg' => $errors);
                    $success['root'][0]['es660'] = true;
                    return $success;
                }
                //-----------------------------------

                //VERIFICAR LA CLASIFICACIÓN---------
                $sqlClasificacion = "SELECT su.* FROM caSimbolosUnidadestbl su, caClasificacionMarcatbl cm, caClasificacionTarifastbl ct, caTarifasTbl tt ".
                                    "WHERE su.clasificacion = cm.clasificacion ".
                                    "AND ct.clasificacion = cm.clasificacion ".
                                    "AND ct.idTarifa = tt.idTarifa ".
                                    "AND ct.clasificacion = su.clasificacion ".
                                    "AND tt.tipoTarifa = 'N' ".
                                    "AND simboloUnidad = '".$rs660['root'][0]['model']."'";

                $rsClas = fn_ejecuta_query($sqlClasificacion);

                if (count($rsClas['root']) < 1) {
                    $errors = "El S&iacute;mbolo: ".$rs660['root'][0]['model']." no tiene Tarifa;n</br>";
                    $success['root'][0] = array('success' => false, 'errorMsg' => $errors);
                    $success['root'][0]['es660'] = true;
                    return $success;
                }

                //-----------------------------------

                //VERIFICAR EN COLOR-----------------
                $sqlColor = "SELECT * FROM caColorUnidadesTbl WHERE color = '".$rs660['root'][0]['colorcode']."' ".
                            "AND marca = (SELECT marca FROM caSimbolosUnidadesTbl".
                                         " WHERE simboloUnidad = '".$rs660['root']['model']."')";
                $rsColor =  fn_ejecuta_query($sqlColor);

                if ($rsColor['root'][0]['descripcion'] == 'NVO') {
                    $errors = "El Color: ".$rs660['root'][0]['colorcode']." no est&aacute; actualizado, favor de actualizarlo </br>";
                    $success['root'][0] = array('success' => false, 'errorMsg' => $errors);
                    $success['root'][0]['es660'] = true;
                    return $success;
                }
                //------------------------------------

                //VERIFICAR EL DISTRIBUIDOR-----------
                $sqlDist = "SELECT * FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".
                           $rs660['root'][0]['dealerid']."'";

                $rsDist =  fn_ejecuta_query($sqlDist);
                if ($rsDist['root'][0]['estatus'] == '0') {
                    $errors = "El Distribuidor: ".$rs660['root'][0]['dealerid']." no est&aacute; actualizado, favor de actualizarlo </br>";
                    $success['root'][0] = array('success' => false, 'errorMsg' => $errors);
                    $success['root'][0]['es660'] = true;
                    return $success;
                }
                //------------------------------------

                $success = $rs660;
                $success['root'][0]['success'] = true;
                $success['root'][0]['existUnidad'] = true;
                $success['root'][0]['es660'] = true;

            } else {//Mas unidades con esa Avanzada
                $success = $rs660;
                $success['root'][0]['success'] = true;
                $success['root'][0]['successMsg'] = 'Avanzada Duplicada, elige un VIN';
                $success['root'][0]['existUnidad'] = true;
                $success['root'][0]['es660'] = true;
            }
        }
        return $success;
    }

    function addGastosEspeciales($distribuidor, $vin, $conceptosArr, $importeArr){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($distribuidor == ""){
            $e[] = array('id'=>'%DistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($vin == ""){
            $e[] = array('id'=>'%VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if(sizeof($conceptosArr) == 0){
            $a['success'] = false;
            $e[] = array('id'=>'%Conceptos','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();

        }

        if($a['success']){
            for ($nInt=0; $nInt < sizeof($conceptosArr); $nInt++) {
                $sqlAddGastosEspecialesStr = "INSERT INTO alGastosEspecialesTbl (distribuidorCentro, vin, secuencia, ".
                                                "claveGasto, importe) VALUES (".
                                                "'".$distribuidor."',".
                                                "'".$vin."',".
                                                ($nInt+1).",".
                                                $conceptosArr[$nInt].",".
                                                $importeArr[$nInt].")";

                fn_ejecuta_Add($sqlAddGastosEspecialesStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlAddGastosEspecialesStr;
                    $a['successMessage'] = getGastosEspecialesSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddGastosEspecialesStr;
                }

            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function getUnidadesEntradaPatio(){

        $response = array('success'=>true, 'records'=>0, 'root'=>array());

        $vinRec = getUnidades();

        foreach ($vinRec['root'] as $vin) {
            if($vin['bloqueada'] != 1){
                //$data = getHistoricoUnidades($vin['vin']);
                $presentada = false;
                $entradaPatio = false;
                $estatusNoValido = false;
                $noRetrabajada = false;
                $retrabajoSimbolo = false;
                $validaKia = false;

                foreach ($vinRec['root'] as $record) {

                    if($record['claveMovimiento'] == 'PR'){
                        $presentada = true;
                    }if($record['claveMovimiento'] == 'IC'){
                        $presentada = true;
                    }if($record['claveMovimiento'] == 'SL'){
                        $presentada = true;
                    }if($record['claveMovimiento'] == 'CO'){
                        $presentada = true;
                    }if($record['claveMovimiento'] == 'CD'){
                        $presentada = true;
                    }if($record['claveMovimiento'] == 'HO'){
                        $presentada = true;
                    }                    if($record['tieneTarifaNormal'] == '1'){
                        $tarifaNormal = true;
                    }if($record['tieneTarifaNormal'] == '1'){
                        $tarifaNormal = true;
                    }else if($record['claveMovimiento'] == 'EP'){
                        $presentada = true;
                        $entradaPatio = true;
                    }

                    if ($tarifaNormal) {
                        $presentada = true;
                        //$entradaPatio = true;
                    }else{
                        $vin['error'] = "La unidad no puede ingresar a patio, tiene tarifa especial";
                    }

                    if($presentada){
                        if(!$entradaPatio){
                            $sqlEP="SELECT * FROM alHistoricoUnidadesTbl WHERE vin ='".$vin['vin']."' AND claveMovimiento='EP'";
                            $rsEP=fn_ejecuta_query($sqlEP);
                            //echo json_encode($rsEP['root'][0]['claveMovimiento']);

                            if($rsEP['root'][0]['claveMovimiento'] === 'EP'){
                                $vin['error'] = "La unidad ya cuenta con Entrada a Patio:  ".$rsEP['root'][0]['centroDistribucion'];    
                            }else{
                                $sqlGetValido = "SELECT 1 as claveMovimiento FROM alultimodetalletbl ".
                                                "WHERE  VIN = '".$vin['vin']."' ".
                                                "AND claveMovimiento IN (SELECT valor FROM caGeneralesTbl ".
                                                                        "WHERE tabla = 'entradaPatioEscaner' ".
                                                                        "AND columna = 'epValidos');";
                                $rsGetValido = fn_ejecuta_query($sqlGetValido);

                                $sqlGetEstatus = "SELECT concat(ud.claveMovimiento,' - ' ,ge.nombre) as claveMovimiento ".
                                                "FROM alUltimoDetalleTbl ud, cageneralestbl ge ".
                                                "WHERE ge.valor = ud.claveMovimiento ".
                                                "AND ge.tabla = 'alhistoricounidadestbl' ".
                                                "AND ge.columna = 'claveMovimiento' ".
                                                "AND ud.vin = '".$vin['vin']."'";

                                $rsGetEstatus = fn_ejecuta_query($sqlGetEstatus);

                                if($rsGetValido['root'][0]['claveMovimiento'] === '1'){

                                    $estatusNoValido = true;

                                    $getValidaKia = "SELECT 1 AS unidadEntradaPatio ".
                                                    "FROM casimbolosunidadestbl su, alunidadestbl au, alinstruccionesmercedestbl im ".
                                                    "WHERE au.vin = im.vin ".
                                                    "AND au.simboloUnidad = su.simboloUnidad ".
                                                    "AND su.marca IN ('HY','KI') ".
                                                    "AND im.cveStatus IN ('PK','PH') ".
                                                    "AND au.vin = '".$vin['vin']."' ";

                                    $rsGetValidaKia = fn_ejecuta_query($getValidaKia);

                                    $sqlGetesUnidad = "SELECT 1 as unidadFH FROM alunidadestbl au1, casimbolosunidadestbl su1 ".
                                                      "WHERE au1.simboloUnidad = su1.simboloUnidad AND su1.marca IN ('HY' ,'KI') ".
                                                      "AND au1.vin = '".$vin['vin']."' ";

                                    $rsGetesUnidad = fn_ejecuta_query($sqlGetesUnidad);
                                    if($rsGetValidaKia['root'][0]['unidadEntradaPatio'] == '1' && $rsGetesUnidad['root'][0]['unidadFH'] == '1' ){
                                        $validaKia = true;
                                    }elseif($rsGetValidaKia['root'][0]['unidadEntradaPatio'] != '1' && $rsGetesUnidad['root'][0]['unidadFH'] != '1'){
                                        $validaKia = true;
                                    }
                                    // else{
                                    //     $vin['error'] = "La unidad no cuenta con un Estatus, PK o PH, no puede ingresar a Patio";
                                    // }
                                    $validaKia = true;
                                    break;
                                }else{
                                    $vin['error'] = "La unidad cuenta con estatus ".$rsGetEstatus['root'][0]['claveMovimiento'];
                                }
                                /*$rsNValidos = fn_ejecuta_query("SELECT valor FROM caGeneralesTbl WHERE tabla = 'entradaPatioEscaner' AND columna = 'epValidos'");

                                foreach ($rsNValidos['root'] as $estatus) {

                                    if($record['claveMovimiento'] == $estatus['valor']){
                                        $estatusNoValido = true;
                                        break;
                                    }
                                }*/

                                if(!$estatusNoValido){
                                    $rsCdValidos = fn_ejecuta_query("SELECT * FROM caGeneralesTbl WHERE tabla = 'trViajesTractoresTbl' AND columna = 'cdValidos'");
                                    foreach ($rsCdValidos['root'] as $centroDist) {
                                        if($vin['centroDistribucion'] == $centroDist['valor']){
                                            $sqlCheckRetrabajoStr = "SELECT * FROM alRetrabajoUnidadesTbl rt ".
                                                                    "WHERE rt.fechaEvento = (SELECT MAX(rt2.fechaEvento) FROM alRetrabajoUnidadesTbl rt2 ".
                                                                        "WHERE rt2.vin = rt.vin) ".
                                                                    "AND rt.vin = '".$vin['vin']."'";
                                            $rsCheckRetrabajo = fn_ejecuta_query($sqlCheckRetrabajoStr);

                                            if(sizeof($rsCheckRetrabajo['root']) > 0){
                                                if($rsCheckRetrabajo['root'][0]['estatusRegistro'] == 'NR'){
                                                    $noRetrabajada = true;
                                                    break;
                                                }
                                            } else {
                                                $sqlCheckRetrabajoStr = "SELECT * FROM alRetrabajoUnidadesTbl rt ".
                                                                        "WHERE rt.fechaEvento = (SELECT MAX(rt2.fechaEvento) FROM alRetrabajoUnidadesTbl rt2 ".
                                                                            "WHERE rt2.simboloUnidad = rt.simboloUnidad AND rt2.vin IS NULL) ".
                                                                        "AND rt.simboloUnidad = '".$vin['simboloUnidad']."'";
                                                $sqlCheckRetrabajoStr = fn_ejecuta_query($sqlCheckRetrabajoStr);

                                                if(sizeof($sqlCheckRetrabajoStr['root']) > 0){
                                                    $retrabajoSimbolo = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }if(!$noRetrabajada){
                                        $sqlGetCambioDestPendienteStr = "SELECT 1 FROM alDestinosEspecialesTbl WHERE vin = '".$vin['vin']."' ".
                                                                        "AND distribuidorOrigen IS NULL";
                                        $rsCambioDest = fn_ejecuta_query($sqlGetCambioDestPendienteStr);
                                        if(sizeof($rsCambioDest['root']) > 0){
                                            $vin['cambioDestino'] = 1;
                                        } else {
                                            $vin['cambioDestino'] = 0;
                                        }
                                    }
                                    if(!$validaKia){
                                        $getValidaKia = "SELECT 1 AS unidadEntradaPatio ".
                                        "FROM casimbolosunidadestbl su, alunidadestbl au, alinstruccionesmercedestbl im ".
                                        "WHERE au.vin = im.vin ".
                                        "AND au.simboloUnidad = su.simboloUnidad ".
                                        "AND su.marca IN ('HY','KI') ".
                                        "AND im.cveStatus IN ('PK','PH') ".
                                        "AND au.vin = '".$vin['vin']."' ";

                                        $rsGetValidaKia = fn_ejecuta_query($getValidaKia);

                                        $sqlGetesUnidad = "SELECT 1 as unidadFH FROM alunidadestbl au1, casimbolosunidadestbl su1 ".
                                                          "WHERE au1.simboloUnidad = su1.simboloUnidad AND su1.marca IN ('HY' ,'KI') ".
                                                          "AND au1.vin = '".$vin['vin']."' ";

                                        $rsGetesUnidad = fn_ejecuta_query($sqlGetesUnidad);

                                        if($rsGetValidaKia['root'][0]['unidadEntradaPatio'] == "1" && $rsGetesUnidad['root'][0]['unidadFH'] == "1" ){
                                            $validaKia = true;
                                        }elseif($rsGetValidaKia['root'][0]['unidadEntradaPatio'] != "1" && $rsGetesUnidad['root'][0]['unidadFH'] != "1"){
                                            $validaKia = true;
                                        }
                                        // else{
                                        //     $vin['error'] = "La unidad no cuenta con un Estatus, PK o PH, no puede ingresar a Patio";
                                        // }
                                        $validaKia = true;
                                    }
                                    if(!$noRetrabajada){
                                        //KIA Y HYNDAI NO CHECAN POR REPUVE
                                        if($vin['simboloRepuve'] == '1'){
                                            //REVISAR SI EL CENTRO DE DISTRIBUCION NECESITA REPUVE
                                            $sqlCheckRepuveDist =  "SELECT tieneRepuve FROM caDistribuidoresCentrosTbl ".
                                                                    "WHERE distribuidorCentro = '".$vin['centroDistribucion']."'";

                                            $rsRepuveDist = fn_ejecuta_query($sqlCheckRepuveDist);
                                            ////echo "GENERA ".$rsRepuveDist['root'][0]['tieneRepuve'];
                                            if($rsRepuveDist['root'][0]['tieneRepuve'] == "1" && substr($_REQUEST['alUnidadesAvanzadaHdn'], 0, 3) != 'DX3'){
                                                $sqlCheckRepuveStr = "SELECT folioRepuve, estatus FROM alRepuveTbl ".
                                                                    "WHERE vin = '".$vin['vin']."'";

                                                $rsRepuve = fn_ejecuta_query($sqlCheckRepuveStr);

                                                if(sizeof($rsRepuve['root']) > 0){
                                                    if(strtoupper($rsRepuve['root'][0]['estatus']) != 'OK'){
                                                        $vin['errorRepuve'] = 2;
                                                    }
                                                    if($vin['folioRepuve'] == ""){
                                                        $sqlUpdRepuveUnidadStr = "UPDATE alUnidadesTbl ".
                                                                                 "SET folioRepuve = ".$rsRepuve['root'][0]['folioRepuve']." ".
                                                                                 "WHERE vin = '".$vin['vin']."'";

                                                        fn_ejecuta_Upd($sqlUpdRepuveUnidadStr);
                                                    }
                                                } else {
                                                    $vin['errorRepuve'] = 1;
                                                }
                                            }
                                        }
                                    }
                                }else {
                                    $vin['error'] = "La unidad cuenta con estatus ".$record['claveMovimiento']." que no es válido para esta operación";
                                }
                            }
                        
                        } else {
                            $vin['error'] = "La unidad ya cuenta con Entrada a Patio";
                        }
                    } else {
                        $vin['error'] = "La unidad no se encuentra Presentada";
                    }
                }
            } else {
                $vin['error'] = "Unidad Bloqueada";
            }
            $vin['retrabajoSimbolo'] = (int)$retrabajoSimbolo;
            array_push($response['root'], $vin);
        }

        $response['records'] = sizeof($response['root']);

        return $response;

    }

    function validaAsignacionUnidad(){

        $a['success'] = true;
        $e['errors'] = array();
        $a['errorMsg'] = '';

        $rsUnidades = getUnidades();
        $rsUnidades = $rsUnidades['root'];

        //echo json_encode($rsUnidades);

        if($_REQUEST['alUnidadesAsignacionIdTalonHdn'] == ""){
            $a['success'] = false;
            $a['errorMsg'] = "Faltan Campos Requeridos";
            return $a;
        }
        if($_REQUEST['alUnidadesAsignacionIdViajeTractorHdn'] == ""){
            $a['success'] = false;
            $a['errorMsg'] = "Faltan Campos Requeridos";
            return $a;
        }

        //Valida que la unidad pertenesca al CD logueado *FRONT
        //echo json_encode($rsUnidades[0]['centroDistribucion']);
        //echo json_encode($_SESSION['usuCompania']);
        if($rsUnidades[0]['centroDistribucion'] !== $_SESSION['usuCompania']){
            $a['success'] = false;
            $a['errorMsg'] = "La Unidad no pertenece al Centro de Distribuci&oacute;n ".$_SESSION['usuCompania'];
            return $a;
        }
        //Si la última clave de movimiento no es Lista para Asignar no procede
        if($rsUnidades[0]['listaAsignar'] != '1'){
            $a['success'] = false;
            $a['errorMsg'] = "La Unidad no est&aacute; Lista para Asignar ".$_SESSION['usuCompania'];
            return $a;

        }

        //Si es un Centro de Distribución con EMBARQUE *Ahorita sólo CDTOL
        $_REQUEST['catGeneralesTablaTxt'] = 'trViajesTractoresTbl';
        $_REQUEST['catGeneralesColumnaTxt'] = 'cdValidos';
        $_REQUEST['catGeneralesColumnaTxt'] = $_SESSION['usuCompania'];
        $rsGenerales = getGenerales();
        //echo json_encode($rsGenerales);


         //echo count($rsGenerales['root']);
        // echo $rsUnidades[0]['embarcada'];

        // count($rsGenerales['root']) > 0 && VA A DENTRO DE LA COMPARACION DE EMBARCADAS

        if($rsUnidades[0]['facturada'] != 1){ //FRONT
            $a['success'] = false;
            $a['errorMsg'] = "La Unidad no ha sido Facturada En SICA-WEB ";
            return $a;

        }

        //Consulto el Historico de la Unidad
        $rsHistorico = getHistoricoUnidades($rsUnidades[0]['vin']);
        $rsHistorico = $rsHistorico['root'];


        //echo($rsUnidades[0]['tarifa']);
        //echo($rsUnidades[0]['distribuidorDestino']);
        //Verifica que no tenga un cambio de Destino Pendiente
        $sqlCheckCambioPendiente =  "SELECT hu.claveHold, ".
                                    "(SELECT ge.nombre FROM caGeneralesTbl ge WHERE tabla = 'alHoldsUnidadesTbl' ".
                                        "AND ge.columna = 'claveHold' AND ge.valor = hu.claveHold) AS descHold ".
                                    "FROM alHoldsUnidadesTbl hu ".
                                    "WHERE hu.vin = '".$rsUnidades[0]['vin']."'";

        $rsCambioPendiente = fn_ejecuta_query($sqlCheckCambioPendiente);

        if (count($rsCambioPendiente['root']) > 0){
            $a['success'] = false;
            $a['errorMsg'] = "La Unidad tiene un Cambio de Destino Pendiente";
            return $a;

        }
        //Verifica que se encuentre la Unidad Detenida, El Tractor != 12
        if($rsUnidades['detenida'] == '1' && $_REQUEST['alUnidadesAsignacionTractorCmb'] != '12' &&
            $rsUnidades[0]['tipoDistribuidor'] != 'CD' &&
            $rsHistorico[count($rsHistorico) - 1]['claveMovimiento'] != 'DT'){
                $a['success'] = false;
                $a['errorMsg'] = "Opci&oacute;n inv&aacute;lida para Unidad Detenida ";
                return $a;

        }
        //Verifica que se encuentre la Unidad Detenida, El Tractor == 12
        if($rsUnidades[0]['detenida'] == '1' && $_REQUEST['alUnidadesAsignacionTractorCmb'] == '12' &&
            $rsUnidades[0]['tipoDistribuidor'] == 'DI' &&
            $rsHistorico[count($rsHistorico) - 1]['claveMovimiento'] == 'DT'){
                $a['success'] = false;
                $a['errorMsg'] = "Opci&oacute;n inv&aacute;lida para Unidad Detenida ";
                return $a;


        }

        /*---------------------------------------------------------------------------------/
        /*                       VALIDA TARIFA ESPECIAL O NORMAL                           /
        /*                                                                                 /
        //Si Pasa esos Filtros Se Verifica su tipo de Tarifa------------------------------*/
        if($rsUnidades[0]['tarifa'] == '13'){ //Tarifa de Servicio Especial

            if ($_REQUEST['alUnidadesAsignacionTalonDistribuidorHdn'] == $rsUnidades[0]['distribuidorDestino']) {
                $espNorm = validaAsignacionTarifaEspecial($rsUnidades, $rsHistorico);

            }else if($_REQUEST['alUnidadesAsignacionTipoDistribuidorHdn'] =='CD'){

                $espNorm = validaAsignacionTarifaEspecial($rsUnidades, $rsHistorico);
            }else{
                $a['errorMsg'] = 'No coincide el Destino de la unidad con el Talon: '.$_REQUEST['alUnidadesAsignacionTalonDistribuidorHdn'].'Destino: '.$rsUnidades[0]['distribuidorDestino'];
            }
        }elseif($rsUnidades[0]['tarifa'] == '00'){
            if ($_REQUEST['alUnidadesAsignacionTalonDistribuidorHdn'] == $rsUnidades[0]['distribuidorDestino']){
                 //Agrega la Unidad a trUnidadesDetallesTalonesTmp
                if(!isset($_REQUEST['centroDistribucion']))
                {
                        $_REQUEST['centroDistribucion'] = ' ';
                }                  
                $sqlAgregaUnidadTmp =  "INSERT INTO trUnidadesDetallesTalonesTmp(numeroTalon, avanzada, vin, simbolo, color, centroDistribucion, idUsuario) ".
                                       "VALUES(".$_REQUEST['alUnidadesAsignacionIdTalonHdn'].", '".
                                       $rsUnidades[0]['avanzada']."', '".$rsUnidades[0]['vin']."', '".$rsUnidades[0]['simboloUnidad']."', '".$rsUnidades[0]['color']."',".
                                       "'".$_REQUEST['centroDistribucion']."',".
                                       $_SESSION['idUsuario'].")";

                fn_ejecuta_Add($sqlAgregaUnidadTmp);

                if(!isset($_SESSION['error_sql']) || isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == ""){
                    $a['success'] = true;
                    $a['successMsg'] = 'Se ha ingresado correctamente la Unidad';
                } else{
                    $a['success'] = false;
                    if(mysql_errno() == 1062){// Valida que no inserte otra unidad en otro talon
                        $a['errorMsg'] = 'La Unidad: '.$rsUnidades[0]['avanzada'].' ya est&aacute; Capturada';
                    } else{
                        $a['errorMsg'] = $_SESSION['error_sql'].$sqlAgregaUnidadTmp;
                    }
                }

                //SE BLOQUEA LA UNIDAD
                $_REQUEST['modulo'] =  'ASIGNACIONUNIDADES';
                bloquearUnidad($rsUnidades[0]['vin']);

                return $a;

            }else if ($_REQUEST['alUnidadesAsignacionTipoDistribuidorHdn'] =='CD'){
                //Agrega la Unidad a trUnidadesDetallesTalonesTmp
                if(!isset($_REQUEST['centroDistribucion']))
                {
                        $_REQUEST['centroDistribucion'] = ' ';
                }                 
                $sqlAgregaUnidadTmp =  "INSERT INTO trUnidadesDetallesTalonesTmp(numeroTalon, avanzada, vin, simbolo, color, centroDistribucion, idUsuario) ".
                                       "VALUES(".$_REQUEST['alUnidadesAsignacionIdTalonHdn'].", '".
                                       $rsUnidades[0]['avanzada']."', '".$rsUnidades[0]['vin']."', '".$rsUnidades[0]['simboloUnidad']."', '".$rsUnidades[0]['color']."',".
                                       "'".$_REQUEST['centroDistribucion']."',".
                                       $_SESSION['idUsuario'].")";

                fn_ejecuta_Add($sqlAgregaUnidadTmp);

                if(!isset($_SESSION['error_sql']) || isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == ""){
                    $a['success'] = true;
                    $a['successMsg'] = 'Se ha ingresado correctamente la Unidad';
                } else{
                    $a['success'] = false;
                    if(mysql_errno() == 1062){// Valida que no inserte otra unidad en otro talon
                        $a['errorMsg'] = 'La Unidad: '.$rsUnidades[0]['avanzada'].' ya est&aacute; Capturada';
                    } else{
                        $a['errorMsg'] = $_SESSION['error_sql'].$sqlAgregaUnidadTmp;
                    }
                }

                //SE BLOQUEA LA UNIDAD
                $_REQUEST['modulo'] =  'ASIGNACIONUNIDADES';
                bloquearUnidad($rsUnidades[0]['vin']);


                return $a;

            }else{
                 $a['success'] = false;
                $a['errorMsg'] = "Distribuidor Diferente";
                return $a;
            }

        }else {
            $espNorm = validaAsignacionTarifaNormal($rsUnidades, $rsHistorico);

        }
        //----------------------------------------------------------------------------------


        //$end = microtime(true); 
        //$timeElapsed = $end - $start;
        //echo 'El tiempo que ha transcurrido  despues de validacion tractor [12] : '.$timeElapsed;
        //return;


        if($espNorm['success'] == true){
            //Agrega la Unidad a trUnidadesDetallesTalonesTmp
            if(!isset($_REQUEST['centroDistribucion']))
            {
                    $_REQUEST['centroDistribucion'] = ' ';
            }             
            $sqlAgregaUnidadTmp =  "INSERT INTO trUnidadesDetallesTalonesTmp(numeroTalon, avanzada, vin, simbolo, color, centroDistribucion, idUsuario) ".
                                   "VALUES(".$_REQUEST['alUnidadesAsignacionIdTalonHdn'].", '".
                                   $rsUnidades[0]['avanzada']."', '".$rsUnidades[0]['vin']."', '".$rsUnidades[0]['simboloUnidad']."', '".$rsUnidades[0]['color']."',".
                                   "'".$_REQUEST['centroDistribucion']."',".
                                   $_SESSION['idUsuario'].")";

            fn_ejecuta_Add($sqlAgregaUnidadTmp);

            if(!isset($_SESSION['error_sql']) || isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == ""){
                $a['success'] = true;
                $a['successMsg'] = 'Se ha ingresado correctamente la Unidad';
            } else{
                $a['success'] = false;
                if(mysql_errno() == 1062){// Valida que no inserte otra unidad en otro talon
                   $a['errorMsg'] = 'La Unidad: '.$rsUnidades[0]['avanzada'].' ya est&aacute; Capturada';
                } else{
                    $a['errorMsg'] = $_SESSION['error_sql'].$sqlAgregaUnidadTmp;
                }
            }

            //SE BLOQUEA LA UNIDAD
            $_REQUEST['modulo'] =  'ASIGNACIONUNIDADES';
            bloquearUnidad($rsUnidades[0]['vin']);
        } else{
            $a['success'] = false;
            $a['errorMsg'] = $espNorm['errorMsg'];
        }
        return $a;

    }

    function validaAsignacionTarifaEspecial($rsUnidades, $rsHistorico){
        $a = array();
        $claveMovimientoDS = $rsHistorico[ count($rsHistorico) - 2]['claveMovimiento'];
        $claveMovimientoRC = $rsHistorico[ count($rsHistorico) - 1]['claveMovimiento'];

        if( $claveMovimientoRC == 'RC'|| $claveMovimientoRC == 'RP'){

            //Valida que corresponda el distribuidor en alDestinosEspecialesTbl
            $_REQUEST['alDestinosEspecialesVinTxt'] = $rsUnidades[0]['vin'];
            $rsDestino = getDestinosEspeciales();

            if($_REQUEST['alUnidadesAsignacionTalonDistribuidorHdn'] != $rsUnidades[0]['distribuidorDestino'] && $_REQUEST['alUnidadesAsignacionTipoDistribuidorHdn'] != 'CD' ){
                $a['success'] = false;
                $a['errorMsg'] = 'Unidad no Corresponde su Destino Especial';
            }
            else{
                $a['success'] = true;
            }
        } else{
            $a['success'] = false;
            $a['errorMsg'] = 'Unidad no valida';
        }
        return $a;
    }

    function validaAsignacionTarifaNormal($rsUnidades, $rsHistorico){
        //Valido que por cada hold tenga su liberación
        $a = isLiberadaDeHolds($rsHistorico);
        if($a['success'] == true){
            //Valida que tenga Entrada a Patio
            $isEP = false;
            foreach ($rsHistorico as $record){

                $sqlClaveValida="SELECT 1 as valor FROM cageneralestbl
                                WHERE tabla='unidadAsignar'
                                AND valor='".$record['claveMovimiento']."'";
                $rsCv=fn_ejecuta_query($sqlClaveValida);
                if ($rsCv['root'][0]['valor'] =='1') {
                    $isEP = true;
                    break;
                }
                /*if($record['claveMovimiento'] == 'EP' || $record['claveMovimiento'] == 'IC'){
                    $isEP = true;
                    break;
                }*/
            }
            if($isEP == false){ //si NO tiene entrada a Patio
                $a['success'] = false;
                $a['errorMsg'] = 'La Unidad no tiene Entrada a Patio';
                return $a;
            }
            //Valida que el Talón tenga el mismo distribuidor que la unidad
            $tipoDistribuidor = $_REQUEST['alUnidadesAsignacionTipoDistribuidorHdn'];
            //echo json_encode($tipoDistribuidor);
            if($tipoDistribuidor == 'DI' || $tipoDistribuidor == 'DX'){
                if($_REQUEST['alUnidadesAsignacionTalonDistribuidorHdn'] != $rsUnidades[0]['distribuidor']){
                    $a['success'] = false;
                    $a['errorMsg'] = "El Distribuidor de la Unidad no coincide con el del Tal&oacute;n";
                    return $a;
                }
            }
            //Valida que no exista en la tabla de retrabajos
            if($rsUnidades[0]['reTrabajo'] == '1'){
                $a['success'] = false;
                $a['errorMsg'] = "La Unidad no ha sido re-trabajada";
                return $a;
            }
        }
        return $a;
    }

    function isLiberadaDeHolds($historicoArr){

        $sqlHoldsPorCd = "SELECT * FROM caGeneralesTbl ge ".
                         "WHERE ge.tabla = 'alHoldsUnidadesTbl' ".
                         "AND ge.columna = '".$_SESSION['usuCompania']."'";


        $rs = fn_ejecuta_query($sqlHoldsPorCd);
        $holdLiberada = array();//Diccionario con VALOR hold-liberada

        for($i = 0; $i < count($rs['root']); $i++){
            $holdLiberada[$rs['root'][$i]['valor']] = $rs['root'][$i]['estatus'];
        }

        //Obtiene el Historico de la Unidad
        //$historicoArr = getHistoricoUnidades($vin);
        //$historicoArr = $historicoArr['root'];

        /*
        //PARA HACER PRUEBAS CON HOLDS DE HISTORICO--------------------------
        $historicoArr[0]['claveMovimiento'] = 'GC';
        $historicoArr[1]['claveMovimiento'] = 'GK';
        $historicoArr[2]['claveMovimiento'] = 'HH';
        $historicoArr[3]['claveMovimiento'] = 'LL';
        $historicoArr[4]['claveMovimiento'] = 'LH';
        $historicoArr[5]['claveMovimiento'] = 'LK';
        //$historicoArr[6]['claveMovimiento'] = 'HH';
        //--------------------------------------------------------------------
        */

        $a = array();
        $a['success'] = false;

        $tempArrLiberada = array();
        $x  = array();
        for($i = 0; $i < count($historicoArr); $i++){
            $holdKey = $historicoArr[$i]['claveMovimiento'];
            if(array_key_exists($holdKey, $holdLiberada)){
                $tempArrHold[$i] =  $holdKey;
                $x[] = array('hold' => $holdKey ,  'liberada' => $holdLiberada[$holdKey]);
            }
        }

        //Si no Existe Ningun Hold en el Historico de la Unidad, regresa liberado
        if(count($x) == 0){
            $a['success'] = true;
            $a['root'][0]['liberada'] = true;
            return $a;
        }

       //Se exactamente cuantos holds hay
       //Por cada hold del arreglo busco en el historico

        for($i = 0; $i < count($x); $i++ ){
            $j = 0;
            for($j = 0; $j < count($historicoArr); $j++){
                $isLib = false;
                if($x[$i]['liberada'] == $historicoArr[$j]['claveMovimiento']){
                    $historicoArr[$j]['claveMovimiento'] = '000';
                    $isLib = true;
                    break;
                }
            }
            if($isLib == false){
                $a['errorMsg'] = "La Unidad tiene un Hold ". $x[$i]['hold']. ", a&uacute;n no Liberado ".$x[$i]['liberada'];
                break;
            }
        }
        if($isLib == true){ // Se holdeo y Libero correctamente
            $a['success']  = true;
            $a['root'][0]['liberada'] = true;
        }
        return $a;
    }
    //Reacomoda el consecutivo de la Lista de Espera cuando se ha eliminado un tractor de la lista
    //Se usa en Asignacion de Unidades
    function updConsecutivoListaDeEspera(){
        $centroDistribucion = $_SESSION['usuCompania'];

        $sqlUpdListaEspera = "SELECT * FROM trEsperaChoferesTbl WHERE centroDistribucion = '".$centroDistribucion."'".
                             " ORDER BY consecutivo ASC;";

        $rs = fn_ejecuta_query($sqlUpdListaEspera);

        $i = 1;
        foreach($rs['root'] as $record){
            $sqlUpdChoferesEspera = "UPDATE trEsperaChoferesTbl SET consecutivo = ".$i++." ".
                                    "WHERE centroDistribucion = '".$centroDistribucion."' ".
                                    "AND idTractor = ".$record['idTractor'];

            fn_ejecuta_Upd($sqlUpdChoferesEspera);
        }
    }
    function dltListaEspera($idTractor, $centroDistribucion){

        $a['success'] = true;

        if($idTractor == ""){
            $a['success'] = false;
            $a['errors'] = array('id' => $idTractor, 'error'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }

        if($centroDistribucion == ""){
            $a['success'] = false;
            $a['errors'] = array('id' => $centroDistribucion, 'error'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }

        if($a['success']){
            $sqlDeleteEspera =  "DELETE FROM trEsperaChoferesTbl WHERE centroDistribucion = '".$centroDistribucion."' ".
                                "AND idTractor = ".$idTractor;

            fn_ejecuta_query($sqlDeleteEspera);
            if(!isset($_SESSION['error_sql']) || isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                $a['sql'] = $sqlDeleteEspera;
                $a['successMsg'] = 'Se ha Eliminado Correctamente de la Lista de Espera';
            } else{
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'].$sqlDeleteEspera;
            }

        }
        return $a;
    }

    function asignacionDeUnidades(){
        //Obtengo los idTalones de la Primer Pantalla
        //De la Tabla de trunidadesdetallestalonestmp obtengo los vin´s de los idTalones
        //Los borro de la misma
        //Los inserto en la tabla de trunidadesdetallestalonestbl
        //Los borro de la Tabla de Bloqueadas
        //Inserto en la tabla de historico cada unidad

        $a['success'] = true;
        $e['errors'] = array();

        if($_REQUEST['asignacionUnidadesCompaniaCmb'] == ""){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesCompaniaCmb', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['asignacionUnidadesTractorCmb'] == ""){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesTractorCmb', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['asignacionUnidadesNumeroRepartosDsp'] == ""){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesNumeroRepartosDsp', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['asignacionUnidadesIdViajeTractorHdn'] == ""){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesIdViajeTractorHdn', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['asignacionUnidadesKmTabuladosHdn'] == ""){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesKmTabuladosHdn', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['asignacionUnidadesChoferCmb'] == ""){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesChoferCmb', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }
        $isTempArr = explode('|', $_REQUEST['asignacionUnidadesTempArr']);
        if(in_array('', $idTalonArr)){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesIdTalones', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }
        $idTalonArr = explode('|', $_REQUEST['asignacionUnidadesIdTalonesArr']);
        if(in_array('', $idTalonArr)){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesIdTalones', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }

    $observacionesArr = explode('|', $_REQUEST['asignacionUnidadesIdObservacionesArr']);
        if(in_array('', $observacionesArr)){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesIdObservacionesArr', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }
        $tipoTalonArr = explode('|', $_REQUEST['asignacionUnidadesTipoTalonArr']);
        if(in_array('', $idTalonArr)){
            $a['success'] = false;
            $e['errors'] = array('id' => 'asignacionUnidadesTipoTalonArr', 'msg' => getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
        }

        $idDistribuidorArr = explode('|', $_REQUEST['asignacionUnidadesDistribuidorArr']);

        if($a['success'] == true){
            //OBTIENE EL FOLIO
            $sqlGetFolioStr = "SELECT folio FROM trFoliosTbl ".
                              "WHERE tipoDocumento = 'TR' ".
                              "AND centroDistribucion='".$_REQUEST['ciaSesVal']."' ".
                              "AND compania = '".$_REQUEST['asignacionUnidadesCompaniaCmb']."';";

            //$rsFolio = fn_ejecuta_query($sqlGetFolioStr);
            $folio = $rsFolio['root'][0]['folio'];
            //echo json_encode($sqlGetFolioStr);
            //----------------------------------------

            //OBTIENE LAS UNIDADES DE LOS TALONES
            $totalUnidadesTalon = array(); //El total real de unidades capturadas
            $totalUnidadesViaje = 0;
            $res = array();
            $folio = $folio + 1;            
            for($i = 0; $i < count($idTalonArr); $i++){
                if($isTempArr[$i] != "0"){
                    $sqlUnidadesTalonesTmp = "SELECT * FROM trUnidadesDetallesTalonesTmp ".
                                             "WHERE numeroTalon = ".$idTalonArr[$i].
                                             " AND idUsuario = ".$_SESSION['idUsuario'].
                                             " AND centroDistribucion = '".$_REQUEST['ciaSesVal']."'";

                    $rs = fn_ejecuta_query($sqlUnidadesTalonesTmp);
                    $totalUnidadesTalon[] = array('idTalon' => $idTalonArr[$i], 'totalUnidades' => count($rs['root']));
                    $totalUnidadesViaje += $totalUnidadesTalon[$i]['totalUnidades'];

                    if($i > 0){
                        $res['root'] = array_merge($res['root'], $rs['root']);//Merge Acumulativo
                    } else{
                        $res['root'] = $rs['root'];
                    }

                    //BORRA DE LA TEMPORAL
                    $sqlDeleteUnidades =  "DELETE FROM trUnidadesDetallesTalonesTmp ".
                                                                " WHERE numeroTalon = ".$idTalonArr[$i].
                                                                " AND idUsuario = ".$_SESSION['idUsuario'].
                                          " AND centroDistribucion = '".$_REQUEST['ciaSesVal']."'";

                    $rsDel = fn_ejecuta_query($sqlDeleteUnidades);

                    //INSERTA EN HISTORICO ETC

                    asignacionUnidadesAddUnidad($totalUnidadesTalon[$i]['idTalon'], $idDistribuidorArr[$i], $rs['root']);

                    //ACTUALIZA EL ESTATUS DEL TALON A TF, TIPO DOCUMENTO R Y NÚMERO DE UNIDADES REALES DEL TALÓN
                    //Si el Talon es epecial, observaciones pasa a...
                    $tipoTalon = "";
                    if ($tipoTalonArr[$i] == 'E') {
                        $tipoTalon = "ESP";
                    } else if ($tipoTalonArr[$i] == 'C') {
                        $tipoTalon = "CAMB DEST";
                    }
                    else {
                        $tipoTalon = "*Hora: ".$observacionesArr[$i];
                    }
                    $totUnidadesStr = "SELECT COUNT(*) AS totUnidades FROM trUnidadesDetallesTalonesTbl WHERE idTalon = ".$idTalonArr[$i]." AND estatus != 'C'";
                    $totUnidadesRst = fn_ejecuta_query($totUnidadesStr);
                    $totUnidades = floatval($totUnidadesRst['root'][0]['totUnidades']);
                    //INCREMENTO EL # DE FOLIO LO INSERTA EN EL TALÓN Y LO ACTUALIZA EN LA TABLA DE FOLIOS

                     /////////////////////// se agrega consulta para obtener datos de complemento carta porte         

                     

                    $sqlDltEmbarcada = "SELECT * FROM trunidadesembarcadastbl WHERE idViajeTractor='".$_REQUEST['asignacionUnidadesIdViajeTractorHdn']."' AND idTalon='".$idTalonArr[$i]."'";
                    $rs=fn_ejecuta_query($sqlDltEmbarcada);
                    /////////////////////


                    $sqlActualizarTalon = "UPDATE trTalonesViajesTbl SET claveMovimiento = 'TF', tipoDocumento = 'TF', fechaEvento = now(),tipoTalon = 'T".$tipoTalonArr[$i]."', numeroUnidades = ".$totUnidades.", observaciones = '".$tipoTalon."' ".", tipoDctoSica = '".$rs['root'][0]['tipoDocumento']."' ".", folioSica = '".$rs['root'][0]['folio']."' ".", subTotal = '".$rs['root'][0]['subtotal']."' ".", IVA = '".$rs['root'][0]['IVA']."' ".", retenciones = '".$rs['root'][0]['retenciones']."' ".", total = '".$rs['root'][0]['total']."' ".", totalIva = '".$rs['root'][0]['totalIVA']."' ".", totalRetenciones = '".$rs['root'][0]['totalRetenciones']."' ".", claveCliente = '".$rs['root'][0]['claveCliente']."' ".", nombre = '".$rs['root'][0]['nombre']."' ".", observacionesCP = '".$rs['root'][0]['observaciones']."' ".", referencia = '".$rs['root'][0]['referencia']."' ".", claveCsica = '".$rs['root'][0]['claveCompania']."' ".
                                          " WHERE idTalon = ".$idTalonArr[$i];

                    $rsUpdTalon = fn_ejecuta_Upd($sqlActualizarTalon);






                    if ((integer) $folio < 9) {
                        $folio = '0'.(string)((integer)$folio + 1 );
                    } else {
                        $folio = (string)((integer)$folio + 1);
                    }

                    $folioFinal = $folio - 1;

                    $sqlUpdFolio = "UPDATE trFoliosTbl SET folio = '".$folioFinal."'".
                                   " WHERE centroDistribucion = '".$_SESSION['usuCompania']."'".
                                   " AND compania = '".$_REQUEST['asignacionUnidadesCompaniaCmb']."'".
                                   " AND tipoDocumento = 'TR';";

                    //$rsUpdTalon = fn_ejecuta_Upd($sqlUpdFolio);
                } else {
                    //PARA LOS TALONES TEMPORALES QUE VIENEN DE SERVICIO ESPECIAL

                    //INCREMENTO EL # DE FOLIO LO INSERTA EN EL TALÓN Y LO ACTUALIZA EN LA TABLA DE FOLIOS
                    if ((integer) $folio < 9) {
                        $folio = '0'.(string)((integer)$folio+1);
                    } else {
                        $folio = (string)((integer)$folio+1);
                    }

                    /////////////////////// se agrega consulta para obtener datos de complemento carta porte                 

                    $sqlDltEmbarcada = "SELECT * FROM trunidadesembarcadastbl WHERE idViajeTractor='".$_REQUEST['asignacionUnidadesIdViajeTractorHdn']."' AND distribuidor='".$idDistribuidorArr[$i]."'";
                    $rs=fn_ejecuta_query($sqlDltEmbarcada);
                    /////////////////////

                    $sqlAddTalon =  "INSERT INTO trTalonesViajesTbl (distribuidor,folio,idViajeTractor,companiaRemitente,".
                                    "idPlazaOrigen,idPlazaDestino,direccionEntrega,centroDistribucion,tipoTalon,fechaEvento,".
                                    "observaciones,numeroUnidades,importe,seguro,tarifaCobrar,kilometrosCobrar,impuesto,retencion,".
                                    "claveMovimiento,tipoDocumento,tipoDctoSica,folioSica,UUID,subTotal,IVA,retenciones,total,totalIva,totalRetenciones,claveCliente,nombre.referencia) VALUES (".
                                    "'".$idDistribuidorArr[$i]."',".
                                    "'".$folio."',".
                                    $_REQUEST['asignacionUnidadesIdViajeTractorHdn'].",".
                                    "(SELECT companiaRemitente FROM trTalonesViajesTmp WHERE numeroTalon = ".$idTalonArr[$i]."),".
                                    "(SELECT idPlazaOrigen FROM alDestinosEspecialesTbl WHERE vin IN (".
                                        "SELECT vin FROM trUnidadesDetallesTalonesTmp ".
                                        "WHERE numeroTalon =".$idTalonArr[$i].") LIMIT 1),".
                                    "(SELECT idPlazaDestino FROM alDestinosEspecialesTbl WHERE vin IN (".
                                        "SELECT vin FROM trUnidadesDetallesTalonesTmp ".
                                        "WHERE numeroTalon =".$idTalonArr[$i].") LIMIT 1),".
                                    "(SELECT direccionEntrega FROM trTalonesViajesTmp WHERE numeroTalon = ".$idTalonArr[$i]."),".
                                    "(SELECT centroDistribucion FROM trTalonesViajesTmp WHERE numeroTalon = ".$idTalonArr[$i]."),".
                                    "'TE',".
                                    "'".date("Y-m-d")."',".
                                    "'ESP',".
                                    "(SELECT COUNT(*) FROM trTalonesViajesTmp WHERE numeroTalon = ".$idTalonArr[$i]."),".
                                    "0.00, 0.00, 0.00, 0, 0, 0.00,".
                                    "'TF',".
                                    "'TF',".
                                    "'".$rs['root'][0]['tipoDocumento']."'".
                                    "'".$rs['root'][0]['folio']."'".
                                    "'".$rs['root'][0]['UUID']."'".
                                    "'".$rs['root'][0]['subTotal']."'".
                                    "'".$rs['root'][0]['IVA']."'".
                                    "'".$rs['root'][0]['retenciones']."'".
                                    "'".$rs['root'][0]['total']."'".
                                    "'".$rs['root'][0]['totalIva']."'".
                                    "'".$rs['root'][0]['totalRetenciones']."'".
                                    "'".$rs['root'][0]['claveCliente']."'".
                                    "'".$rs['root'][0]['nombre']."'".
                                    "'".$rs['root'][0]['observaciones']."'".
                                    "'".$rs['root'][0]['referencia']."')";

                    fn_ejecuta_Add($sqlAddTalon);
                    //echo json_encode($sqlAddTalon);

                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $sqlGetIdTalon = "SELECT MAX(idTalon) AS idTalon FROM trTalonesViajesTbl WHERE centroDistribucion = ".
                                            "(SELECT centroDistribucion FROM trTalonesViajesTmp WHERE numeroTalon = ".$idTalonArr[$i].")";

                        $rs = fn_ejecuta_query($sqlGetIdTalon);
                        $idTalon = $rs['root'][0]['idTalon'];

                        //INGRESAR LAS UNIDADES DE TALON DE SERVICIO ESPECIAL
                        $sqlGetUnidadesEsp = "SELECT vin FROM trUnidadesDetallesTalonesTmp ".
                                                                 " WHERE numeroTalon = ".$idTalonArr[$i].
                                                                 " AND idUsuario = ".$_SESSION['idUsuario'].
                                             " AND centroDistribucion = '".$_REQUEST['ciaSesVal']."'";
                        $rs = fn_ejecuta_query($sqlGetUnidadesEsp);

                        //INSERTA EN HISTORICO ETC
                        asignacionUnidadesAddUnidad($idTalon, $idDistribuidorArr[$i], $rs['root']);




                        //BORRA DE LA TEMPORAL
                        $sqlDelete =  "DELETE FROM trUnidadesDetallesTalonesTmp ".
                                                    " WHERE numeroTalon = ".$idTalonArr[$i].
                                                          " AND idUsuario = ".$_SESSION['idUsuario'].
                                      " AND centroDistribucion = '".$_REQUEST['ciaSesVal']."'";                                                 
                        fn_ejecuta_query($sqlDelete);

                        $sqlDelete =  "DELETE FROM trTalonesViajesTmp WHERE numeroTalon = ".$idTalonArr[$i];
                        fn_ejecuta_query($sqlDelete);

                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddTalon;
                    }
                }
            }

            //ACTUALIZA trViajesTractoresTbl;
            $sqlUpdViajes =  "UPDATE trViajesTractoresTbl SET claveMovimiento = 'VF',".
                             " numeroUnidades = ".$totalUnidadesViaje.
                             ", numeroRepartos = ".$_REQUEST['asignacionUnidadesNumeroRepartosDsp'].
                             " WHERE idViajeTractor = ".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];

            fn_ejecuta_Upd($sqlUpdViajes);

            $sqlDltEmbarcada = "SELECT distinct folio FROM trunidadesembarcadastbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
            $rs=fn_ejecuta_query($sqlDltEmbarcada);

            $sqlDltEmbarcada1 = "SELECT * FROM trunidadesembarcadastbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
            $rsClave=fn_ejecuta_query($sqlDltEmbarcada1);

       // echo "numero talones".sizeof($rs['root']);
        //echo "cleinte".$rsClave['root'][0]['claveCliente'];

             /* if ($rsClave['root'][0]['claveCliente']=='406' && sizeof($rs['root']) > 1) {
                

               $sqlDistribuidor="SELECT distinct distribuidor FROM trunidadesembarcadastbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                $rsDist=fn_ejecuta_query($sqlDistribuidor);

                    $cadenaVin='';
            

                for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                    $cadenaVin=$cadenaVin.$rsDist['root'][$i]['distribuidor']."','";
                }


                $rsCotizacion['data']['VIN']=$cadenaVin;
                $distArr = explode("|", substr($cadenaVin, 0, -1));


                 $sqlKilCD="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$_SESSION['usuCompania']."') ";
                 $rsKilCD=fn_ejecuta_query($sqlKilCD);

                 $cadena= substr($cadenaVin,  0, -2);

                 $sqlKil="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$cadena.") ";
                 $rsKil=fn_ejecuta_query($sqlKil);

                    $cadenaVin='';
                

                for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                    $cadenaVin=$cadenaVin.$rsKil['root'][$i]['idPlaza'].",";
                }
                $plazas=substr($cadenaVin,0,-1);

                
                /*$sqlKmsPlaza="SELECT min(a.kilometros) as kilometros, b.distribuidorCentro as distribuidor ". 
                                            "FROM caKilometrosPlazaTbl a , cadistribuidorescentrostbl b ".
                                            "WHERE a.idPlazaOrigen=".$rsKilCD['root'][0]['idPlaza'].
                                            " AND a.idPlazaDestino in (".$plazas.") ".
                                            "and a.idPlazaDestino=b.idplaza ".
                                            "and b.distribuidorCentro in ('".$cadena.")";
                $rsKms=fn_ejecuta_query($sqlKmsPlaza);*/

           /*     $sqlKmsPlaza="SELECT  * FROM ".
                                    "caKilometrosPlazaTbl a, ".
                                    "cadistribuidorescentrostbl b WHERE ".                    
                                    "a.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                        " AND a.idPlazaDestino in (".$plazas.") ".
                                        "and a.idPlazaDestino = b.idplaza ".
                                        "and b.distribuidorCentro in ('".$cadena.") ".
                                        "AND a.kilometros in (SELECT min(c.kilometros) ".
                                                                "FROM cakilometrosplazatbl c ,cadistribuidorescentrostbl b ".
                                                                "WHERE c.idPlazaDestino in (".$plazas.") ".
                                                                "AND c.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                                                " and b.distribuidorCentro in ('".$cadena.") ".
                                                                "and c.idPlazaDestino = b.idplaza)";
                $rsKms=fn_ejecuta_query($sqlKmsPlaza);



                 $sqlTalon="SELECT * FROM trUnidadesEmbarcadasTbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn']." AND distribuidor !='".$rsKms['root'][0]['distribuidorCentro']."'";
                $rsTalon=fn_ejecuta_query($sqlTalon);

                for ($i=0; $i < sizeof($rsTalon['root']); $i++) { 
                    $updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][$i]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                    $upd=fn_ejecuta_query($updTalon);
                }

                /*$updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][0]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'] ."AND distribuidor != '".$rsKms['root'][0]['distribuidorCentro']."'";
                $upd=fn_ejecuta_query($updTalon);*/

        //}
       /*  if ($rsClave['root'][0]['claveCliente']=='589'  && sizeof($rs['root']) > 1) {
                

               $sqlDistribuidor="SELECT distinct distribuidor FROM trunidadesembarcadastbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                $rsDist=fn_ejecuta_query($sqlDistribuidor);

                    $cadenaVin='';
            

                for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                    $cadenaVin=$cadenaVin.$rsDist['root'][$i]['distribuidor']."','";
                }


                $rsCotizacion['data']['VIN']=$cadenaVin;
                $distArr = explode("|", substr($cadenaVin, 0, -1));


                 $sqlKilCD="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$_SESSION['usuCompania']."') ";
                 $rsKilCD=fn_ejecuta_query($sqlKilCD);

                 $cadena= substr($cadenaVin,  0, -2);

                 $sqlKil="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$cadena.") ";
                 $rsKil=fn_ejecuta_query($sqlKil);

                    $cadenaVin='';
                

                for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                    $cadenaVin=$cadenaVin.$rsKil['root'][$i]['idPlaza'].",";
                }
                $plazas=substr($cadenaVin,0,-1);

                
                /*$sqlKmsPlaza="SELECT min(a.kilometros) as kilometros, b.distribuidorCentro as distribuidor ". 
                                            "FROM caKilometrosPlazaTbl a , cadistribuidorescentrostbl b ".
                                            "WHERE a.idPlazaOrigen=".$rsKilCD['root'][0]['idPlaza'].
                                            " AND a.idPlazaDestino in (".$plazas.") ".
                                            "and a.idPlazaDestino=b.idplaza ".
                                            "and b.distribuidorCentro in ('".$cadena.")";
                $rsKms=fn_ejecuta_query($sqlKmsPlaza);*/

/*                $sqlKmsPlaza="SELECT  * FROM ".
                                    "caKilometrosPlazaTbl a, ".
                                    "cadistribuidorescentrostbl b WHERE ".                    
                                    "a.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                        " AND a.idPlazaDestino in (".$plazas.") ".
                                        "and a.idPlazaDestino = b.idplaza ".
                                        "and b.distribuidorCentro in ('".$cadena.") ".
                                        "AND a.kilometros in (SELECT min(c.kilometros) ".
                                                                "FROM cakilometrosplazatbl c ,cadistribuidorescentrostbl b ".
                                                                "WHERE c.idPlazaDestino in (".$plazas.") ".
                                                                "AND c.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                                                " and b.distribuidorCentro in ('".$cadena.") ".
                                                                "and c.idPlazaDestino = b.idplaza)";
                $rsKms=fn_ejecuta_query($sqlKmsPlaza);



                 $sqlTalon="SELECT * FROM trUnidadesEmbarcadasTbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn']." AND distribuidor !='".$rsKms['root'][0]['distribuidorCentro']."'";
                $rsTalon=fn_ejecuta_query($sqlTalon);

                for ($i=0; $i < sizeof($rsTalon['root']); $i++) { 
                    $updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][$i]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                    $upd=fn_ejecuta_query($updTalon);
                }

                $updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][0]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'] ."AND distribuidor != '".$rsKms['root'][0]['distribuidorCentro']."'";
                $upd=fn_ejecuta_query($updTalon);

        }*/

        ////////////////////// viajes Stellantis
        //////////////

        for ($i=0; $i < sizeof($rs['root']); $i++) { 
            $numeroTalones="SELECT * FROM trtalonesviajestbl where folioSica='".$rs['root'][$i]['folio']."';";
            $rsFolio=fn_ejecuta_query($numeroTalones);

            if ( sizeof($rsFolio['root']) > 1) {
            //echo "entra";
                    

                   $sqlDistribuidor="SELECT distinct distribuidor FROM trunidadesembarcadastbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                    $rsDist=fn_ejecuta_query($sqlDistribuidor);

                        $cadenaVin='';
                

                    for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                        $cadenaVin=$cadenaVin.$rsDist['root'][$i]['distribuidor']."','";
                    }


                    $rsCotizacion['data']['VIN']=$cadenaVin;
                    $distArr = explode("|", substr($cadenaVin, 0, -1));


                     $sqlKilCD="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$_SESSION['usuCompania']."') ";
                     $rsKilCD=fn_ejecuta_query($sqlKilCD);

                     $cadena= substr($cadenaVin,  0, -2);

                     $sqlKil="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$cadena.") ";
                     $rsKil=fn_ejecuta_query($sqlKil);

                        $cadenaVin='';
                    

                    for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                        $cadenaVin=$cadenaVin.$rsKil['root'][$i]['idPlaza'].",";
                    }
                    $plazas=substr($cadenaVin,0,-1);

                    
                    /*$sqlKmsPlaza="SELECT min(a.kilometros) as kilometros, b.distribuidorCentro as distribuidor ". 
                                                "FROM caKilometrosPlazaTbl a , cadistribuidorescentrostbl b ".
                                                "WHERE a.idPlazaOrigen=".$rsKilCD['root'][0]['idPlaza'].
                                                " AND a.idPlazaDestino in (".$plazas.") ".
                                                "and a.idPlazaDestino=b.idplaza ".
                                                "and b.distribuidorCentro in ('".$cadena.")";
                    $rsKms=fn_ejecuta_query($sqlKmsPlaza);*/

                    $sqlKmsPlaza="SELECT  * FROM ".
                                        "caKilometrosPlazaTbl a, ".
                                        "cadistribuidorescentrostbl b WHERE ".                    
                                        "a.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                            " AND a.idPlazaDestino in (".$plazas.") ".
                                            "and a.idPlazaDestino = b.idplaza ".
                                            "and b.distribuidorCentro in ('".$cadena.") ".
                                            "AND a.kilometros in (SELECT MAX(c.kilometros) ".
                                                                    "FROM cakilometrosplazatbl c ,cadistribuidorescentrostbl b ".
                                                                    "WHERE c.idPlazaDestino in (".$plazas.") ".
                                                                    "AND c.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                                                    " and b.distribuidorCentro in ('".$cadena.") ".
                                                                    "and c.idPlazaDestino = b.idplaza)";
                    $rsKms=fn_ejecuta_query($sqlKmsPlaza);



                    $sqlTalon="SELECT * FROM trUnidadesEmbarcadasTbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn']." AND distribuidor !='".$rsKms['root'][0]['distribuidorCentro']."'";
                    $rsTalon=fn_ejecuta_query($sqlTalon);

                   /* $sqlTalon="SELECT * FROM trUnidadesEmbarcadasTbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn']." AND subTotal ='0.00'";
                    $rsTalon=fn_ejecuta_query($sqlTalon);*/

                    //echo json_encode($rsTalon);

                    for ($i=0; $i < sizeof($rsTalon['root']); $i++) { 
                        $updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][$i]['idtalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                        $upd=fn_ejecuta_query($updTalon);
                    }

                    /*$updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][0]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'] ."AND distribuidor != '".$rsKms['root'][0]['distribuidorCentro']."'";
                    $upd=fn_ejecuta_query($updTalon);*/

            }

        }
        
        /*if ($rsClave['root'][0]['claveCliente']=='1'  && sizeof($rs['root']) > 1 && $rsClave['root'][0]['centroDistribucion']=='CDTUX') {
            

               $sqlDistribuidor="SELECT distinct distribuidor FROM trunidadesembarcadastbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                $rsDist=fn_ejecuta_query($sqlDistribuidor);

                    $cadenaVin='';
            

                for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                    $cadenaVin=$cadenaVin.$rsDist['root'][$i]['distribuidor']."','";
                }


                $rsCotizacion['data']['VIN']=$cadenaVin;
                $distArr = explode("|", substr($cadenaVin, 0, -1));


                 $sqlKilCD="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$_SESSION['usuCompania']."') ";
                 $rsKilCD=fn_ejecuta_query($sqlKilCD);

                 $cadena= substr($cadenaVin,  0, -2);

                 $sqlKil="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$cadena.") ";
                 $rsKil=fn_ejecuta_query($sqlKil);

                    $cadenaVin='';
                

                for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                    $cadenaVin=$cadenaVin.$rsKil['root'][$i]['idPlaza'].",";
                }
                $plazas=substr($cadenaVin,0,-1);

                
                /*$sqlKmsPlaza="SELECT min(a.kilometros) as kilometros, b.distribuidorCentro as distribuidor ". 
                                            "FROM caKilometrosPlazaTbl a , cadistribuidorescentrostbl b ".
                                            "WHERE a.idPlazaOrigen=".$rsKilCD['root'][0]['idPlaza'].
                                            " AND a.idPlazaDestino in (".$plazas.") ".
                                            "and a.idPlazaDestino=b.idplaza ".
                                            "and b.distribuidorCentro in ('".$cadena.")";
                $rsKms=fn_ejecuta_query($sqlKmsPlaza);*/

           /*     $sqlKmsPlaza="SELECT  * FROM ".
                                    "caKilometrosPlazaTbl a, ".
                                    "cadistribuidorescentrostbl b WHERE ".                    
                                    "a.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                        " AND a.idPlazaDestino in (".$plazas.") ".
                                        "and a.idPlazaDestino = b.idplaza ".
                                        "and b.distribuidorCentro in ('".$cadena.") ".
                                        "AND a.kilometros in (SELECT min(c.kilometros) ".
                                                                "FROM cakilometrosplazatbl c ,cadistribuidorescentrostbl b ".
                                                                "WHERE c.idPlazaDestino in (".$plazas.") ".
                                                                "AND c.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                                                " and b.distribuidorCentro in ('".$cadena.") ".
                                                                "and c.idPlazaDestino = b.idplaza)";
                $rsKms=fn_ejecuta_query($sqlKmsPlaza);



                 $sqlTalon="SELECT * FROM trUnidadesEmbarcadasTbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn']." AND distribuidor !='".$rsKms['root'][0]['distribuidorCentro']."'";
                $rsTalon=fn_ejecuta_query($sqlTalon);

                for ($i=0; $i < sizeof($rsTalon['root']); $i++) { 
                    $updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][$i]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                    $upd=fn_ejecuta_query($updTalon);
                }

                /*$updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][0]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'] ."AND distribuidor != '".$rsKms['root'][0]['distribuidorCentro']."'";
                $upd=fn_ejecuta_query($updTalon);*/

        /*}

        if ($rsClave['root'][0]['claveCliente']=='1'  && sizeof($rs['root']) > 1 && $rsClave['root'][0]['centroDistribucion']=='CDALT') {
            

               $sqlDistribuidor="SELECT distinct distribuidor FROM trunidadesembarcadastbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                $rsDist=fn_ejecuta_query($sqlDistribuidor);

                    $cadenaVin='';
            

                for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                    $cadenaVin=$cadenaVin.$rsDist['root'][$i]['distribuidor']."','";
                }


                $rsCotizacion['data']['VIN']=$cadenaVin;
                $distArr = explode("|", substr($cadenaVin, 0, -1));


                 $sqlKilCD="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$_SESSION['usuCompania']."') ";
                 $rsKilCD=fn_ejecuta_query($sqlKilCD);

                 $cadena= substr($cadenaVin,  0, -2);

                 $sqlKil="SELECT * FROM caDistribuidoresCentrosTbl where distribuidorcentro in ('".$cadena.") ";
                 $rsKil=fn_ejecuta_query($sqlKil);

                    $cadenaVin='';
                

                for ($i=0; $i <sizeof($rsDist['root']) ; $i++) {
                    $cadenaVin=$cadenaVin.$rsKil['root'][$i]['idPlaza'].",";
                }
                $plazas=substr($cadenaVin,0,-1);

                
                /*$sqlKmsPlaza="SELECT min(a.kilometros) as kilometros, b.distribuidorCentro as distribuidor ". 
                                            "FROM caKilometrosPlazaTbl a , cadistribuidorescentrostbl b ".
                                            "WHERE a.idPlazaOrigen=".$rsKilCD['root'][0]['idPlaza'].
                                            " AND a.idPlazaDestino in (".$plazas.") ".
                                            "and a.idPlazaDestino=b.idplaza ".
                                            "and b.distribuidorCentro in ('".$cadena.")";
                $rsKms=fn_ejecuta_query($sqlKmsPlaza);*/

             /*   $sqlKmsPlaza="SELECT  * FROM ".
                                    "caKilometrosPlazaTbl a, ".
                                    "cadistribuidorescentrostbl b WHERE ".                    
                                    "a.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                        " AND a.idPlazaDestino in (".$plazas.") ".
                                        "and a.idPlazaDestino = b.idplaza ".
                                        "and b.distribuidorCentro in ('".$cadena.") ".
                                        "AND a.kilometros in (SELECT min(c.kilometros) ".
                                                                "FROM cakilometrosplazatbl c ,cadistribuidorescentrostbl b ".
                                                                "WHERE c.idPlazaDestino in (".$plazas.") ".
                                                                "AND c.idPlazaOrigen = ".$rsKilCD['root'][0]['idPlaza'].
                                                                " and b.distribuidorCentro in ('".$cadena.") ".
                                                                "and c.idPlazaDestino = b.idplaza)";
                $rsKms=fn_ejecuta_query($sqlKmsPlaza);



                 $sqlTalon="SELECT * FROM trUnidadesEmbarcadasTbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn']." AND distribuidor !='".$rsKms['root'][0]['distribuidorCentro']."'";
                $rsTalon=fn_ejecuta_query($sqlTalon);

                for ($i=0; $i < sizeof($rsTalon['root']); $i++) { 
                    $updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][$i]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                    $upd=fn_ejecuta_query($updTalon);
                }

                /*$updTalon="UPDATE trtalonesviajestbl SET `subTotal`='0', `IVA`='0', `retenciones`='0', `total`='0', `totalIva`='0', `totalRetenciones`='0' WHERE `idTalon`='".$rsTalon['root'][0]['idTalon']."' AND idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'] ."AND distribuidor != '".$rsKms['root'][0]['distribuidorCentro']."'";
                $upd=fn_ejecuta_query($updTalon);

        }*/

///////////////////////////////////////////////////////////////////////            

            /* $sqlDistribuidor="SELECT * FROM trunidadesembarcadastbl WHERE idViajeTractor=".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'];
                $rsDist=fn_ejecuta_query($sqlDistribuidor);


                for ($i=0; $i < sizeof($rsDist['root']) ; $i++) { 
                    
                }*/




            //ELIMINA LAS UNIDADES DE LAS EMBARCADAS SI ES UN CDVALIDO
            $sqlDltEmbarcada = "DELETE FROM  trunidadesembarcadastbl WHERE idViajeTractor = ".$_REQUEST['asignacionUnidadesIdViajeTractorHdn'].
                               " AND '".$_SESSION['usuCompania']."' = (SELECT VALOR FROM cageneralestbl WHERE tabla = 'trviajestractorestbl' ".
                                                                      "AND columna = 'cdValidos' ".
                                                                      "AND valor = '".$_SESSION['usuCompania']."');";
            fn_ejecuta_query($sqlDltEmbarcada);

            //1.-Hace el Mantenimiento
            //1.1.-Hace El Log
            //2.-Elimina de la Lista de Espera
            //3.-Actualiza la Lista de Espera
            $a['successMessage'] = 'Unidades Asignadas Correctamente';
            /*$rsMantenimiento = checkProxMantenimiento($_REQUEST['asignacionUnidadesIdViajeTractorHdn'],
                                                      $_REQUEST['asignacionUnidadesKmTabuladosHdn'],
                                                      'ASIGNACIONUNIDADES');*/

            dltListaEspera($_REQUEST['asignacionUnidadesTractorCmb'], $_SESSION['usuCompania']);
            updConsecutivoListaDeEspera();
        }
        if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
            $a['successMessage'] = 'Unidades Asignadas Correctamente';
            if($rsMantenimiento['root'][0]['message']){
                $a['mantenimiento'] = $rsMantenimiento['root'][0]['message'];
            }
        } else{
            $a['errorMessage'] = 'No se pudo generar la asignaci&oacute;n';
            $e['errors'] =  $_SESSION['error_sql'];
        }
        $a['errors'] = $e;
        echo json_encode($a);
    }

    function asignacionUnidadesAddUnidad($idTalon, $idDistribuidor, $unidadArr,$idGuardaTalon){


            $sqlInsertUnidadesTalon = "INSERT INTO trUnidadesDetallesTalonesTbl(idTalon, vin, estatus,importeUnitario) VALUES";
            $sqlDltUnidadesTmp = "DELETE FROM alUnidadesTmp WHERE vin IN (";



            for($i = 0; $i < count($unidadArr); $i++){

                //INSERTA LAS UNIDADES EN LA NO TEMPORAL
                if($i > 0){
                    $sqlInsertUnidadesTalon .= ", ";
                }


                $sqlUnit="SELECT * FROM trUnidadesEmbarcadasTbl where vin='".$unidadArr[$i]['vin']."'";
                $rsUnit=fn_ejecuta_query($sqlUnit);
                $sqlInsertUnidadesTalon .= "(".$idTalon.", '".$unidadArr[$i]['vin']."', 'AM',".$rsUnit['root'][0]['importeUnitario'].")";
                if($i > 0){
                    $sqlDltUnidadesTmp .= ", ";
                }
                //ELIMINA DE LA TEMPORAL/LIBERA LA UNIDAD DE alUnidadesTmp
                $sqlDltUnidadesTmp .= "'".$unidadArr[$i]['vin']."'";

                //INSERTA LAS UNIDADES EN EL HISTORICO
                //Primero obtiene el idTarifa de las unidades a partir del vin
                /*$sqlGetHistUnidad = "SELECT hu1.*  FROM alHistoricoUnidadesTbl hu1 ".
                                  "WHERE hu1.fechaEvento =  (SELECT MAX(hu2.fechaEvento)  FROM alHistoricoUnidadesTbl hu2 ".
                                  "WHERE hu2.vin = '".$unidadArr[$i]['vin']."');";*/
                $sqlGetHistUnidad   = "SELECT * FROM alultimodetalletbl where vin = '".$unidadArr[$i]['vin']."' ";

                $rsHistUnidad = fn_ejecuta_query($sqlGetHistUnidad);
                $rsHistUnidad = $rsHistUnidad['root'];

                $sqlGetDiUns = "SELECT distribuidor ".
                                "FROM alunidadestbl ".
                                "WHERE vin = '".$unidadArr[$i]['vin']."';";

                $rsDiUnd = fn_ejecuta_Upd($sqlGetDiUns);

                $idDistribuidor = $rsDiUnd['root'][0]['distribuidor'];



                $rsAddHist = addHistoricoUnidad($_SESSION['usuCompania'],
                    $unidadArr[$i]['vin'],
                    'AM',
                    (substr($idDistribuidor, 0, 2) != 'CD' ? $idDistribuidor : $rsHistUnidad[0]['distribuidor']),
                    $rsHistUnidad[0]['idTarifa'],
                    $rsHistUnidad[0]['localizacionUnidad'],
                    $_REQUEST['asignacionUnidadesChoferCmb'],
                    '',
                    $_SESSION['idUsuario'], $idTalon);
                        //echo "idtalonADDHISTORICO".$idTalon;
                //parametros para transmision P01
                i830_manual($unidadArr[$i]['vin'],'AM');



                //BORRA LA UNIDAD DE LA LOCALIZACIÓN DEL PATIO SI Y SOLO SI NO ESTÁ EN EL TRACTOR 12 Y ESTE DETENIDA
                $sqlUpdLocalizacion = "UPDATE alLocalizacionPatiosTbl SET vin = NULL, estatus='DI' ".
                                      " WHERE vin = '".$unidadArr[$i]['vin']."' ";
                                      " AND (SELECT COUNT(ud.vin) FROM alUnidadesDetenidasTbl ud ".
                                            "WHERE ud.vin = '".$unidadArr[$i]['vin']."') > 0".
                                      " AND (SELECT vt.idTractor FROM trviajestractorestbl vt ".
                                            "WHERE vt.idViajeTractor = ".
                                                "(SELECT tv.idViajeTractor FROM trtalonesviajestbl tv ".
                                                "WHERE tv.idTalon = ".$idTalon.")) <> 12 ";

                fn_ejecuta_Upd($sqlUpdLocalizacion);

                $updPmp="UPDATE alactividadespmptbl SET claveEstatus='AP' WHERE centroDistribucion ='".$_SESSION['usuCompania']."' and vin='".$unidadArr[$i]['vin']."' AND fechaEvento <=NOW();";
                fn_ejecuta_query($updPmp);

                $deletePmp = "DELETE FROM alactividadespmptbl WHERE centroDistribucion ='".$_SESSION['usuCompania']."' and vin='".$unidadArr[$i]['vin']."' and claveEstatus ='PE'";
                fn_ejecuta_query($deletePmp);

            }
            $sqlDltUnidadesTmp .= ");";
            //Inserta
            fn_ejecuta_Add($sqlInsertUnidadesTalon);
            //Elimina/Libera Unidad
            fn_ejecuta_query($sqlDltUnidadesTmp);
    }

    function getReUnidades(){

        $getReUnidadesConsulta =  "SELECT uc.simboloUnidad, ud.vin, uc.avanzada, concat(su.simboloUnidad, '  ', su.descripcion) as simboloUnidades, concat(di.distribuidorCentro,'  ',di.descripcionCentro) as distribuidores, concat(cu.color,'  ',cu.descripcion)  as colorUnidades, ".
                                  "(SELECT COUNT(*) FROM alUltimoDetalleTbl hi WHERE hi.vin = ud.vin AND hi.claveMovimiento = 'EP') AS error, '' AS descrTarifa, '' AS idTarifa ".
                                  "FROM alunidadestbl uc, casimbolosunidadestbl su, cadistribuidorescentrostbl di, cacolorunidadestbl cu, alultimodetalletbl ud ".
                                  "WHERE ud.vin = uc.vin ".
                                  "AND uc.simboloUnidad = su.simboloUnidad ".
                                  "AND ud.claveMovimiento IN ('OM','OK','EO','SI','SN','SC','EN','RP','FS','FO') ".
                                  "AND ud.distribuidor = di.distribuidorCentro ".
                                  "AND uc.color = cu.color ".
                                  "AND su.marca = cu.marca ".
                                  "AND uc.avanzada = '".$_REQUEST['reUnidadesAvanzdaTxt']."' ";
                //echo "$getReUnidadesConsulta<br>";
        $rs = fn_ejecuta_query($getReUnidadesConsulta);
        
        $i = 0;
        foreach($rs['root'] AS $idx => $row)
        {               
                $sql = "SELECT c.idTarifa, CONCAT(c.tarifa,' - ',c.descripcion) AS descrTarifa FROM caSimbolosUnidadesTbl a, caClasificacionTarifasTbl b, caTarifasTbl c ".
                             " WHERE a.simboloUnidad = '".$row['simboloUnidad']."'".
                             " AND b.marca = a.marca".
                             " AND b.clasificacion = a.clasificacion".
                             " AND c.idTarifa = b.idTarifa".
                             " AND c.tipoTarifa = 'N'";
                //echo "$sql<br>";
                $rsTarifa = fn_ejecuta_query($sql);
                
                if($rsTarifa['records'] == 0)
                {
                        $sql = "SELECT hu1.idTarifa, CONCAT(c.tarifa,' - ',c.descripcion) AS descrTarifa ".
                                    " FROM alhistoricounidadestbl hu1, alUnidadesTbl au, caTarifasTbl c ".
                                    " WHERE idHistorico = (SELECT MAX(hu2. idHistorico) FROM alhistoricounidadestbl hu2 WHERE hu2.vin = hu1.vin ) ".
                                    " AND hu1.vin = au.vin ".
                                    " AND au.avanzada = '".$_REQUEST['reUnidadesAvanzdaTxt']."'".
                                    " AND c.idTarifa = hu1.idTarifa";
                            //echo "$sql<br>";
                        $rsTarifa = fn_ejecuta_query($sql);
                }
                $idTarifa = $rsTarifa['root'][0]['idTarifa'];               
                $descrTarifa = $rsTarifa['root'][0]['descrTarifa'];
                
                $rs['root'][$i]['idTarifa'] = $idTarifa;
                $rs['root'][$i]['descrTarifa'] = $descrTarifa;
                
                $i++;
        }

        echo json_encode($rs);
    }

    function addReUnidades(){
                $a = array();
                $a['success'] = true;
                $a['msjResponse'] = "Reactivaci&oacuten correcta.";
        /*
        $sql = "SELECT c.idTarifa FROM caSimbolosUnidadesTbl a, caClasificacionTarifasTbl b, caTarifasTbl c ".
                     " WHERE a.simboloUnidad = '".$_REQUEST['simboloUnidad']."'".
                     " AND b.marca = a.marca".
                     " AND b.clasificacion = a.clasificacion".
                     " AND c.idTarifa = b.idTarifa".
                     " AND c.tipoTarifa = 'N'";
        //echo "$sql<br>";
        $rsTarifa = fn_ejecuta_query($sql);
        
        if($rsTarifa['records'] == 0)
        {
                $sql = "SELECT hu1.idTarifa ".
                            " FROM alhistoricounidadestbl hu1, alUnidadesTbl au ".
                            " WHERE idHistorico = (SELECT MAX(hu2. idHistorico) FROM alhistoricounidadestbl hu2 WHERE hu2.vin = hu1.vin ) ".
                            " AND hu1.vin = au.vin ".
                            " AND au.avanzada = '".$_REQUEST['reUnidadesAvanzdaTxt']."'";
                    //echo "$sql<br>";
                $rsTarifa = fn_ejecuta_query($sql);
        }        
        $idTarifa = $rsTarifa['root'][0]['idTarifa'];
        */
        $idTarifa = $_REQUEST['idTarifa'];
        
        $sqlAddReUnidades = "INSERT INTO alhistoricounidadestbl ( centroDistribucion, vin, fechaEvento, claveMovimiento , distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                            " SELECT '".$_SESSION['usuCompania']."' as centroDistribucion, hu1.vin, NOW() as fechaEvento, 'RA' as claveMovimiento, hu1.distribuidor, ".$idTarifa.", hu1.localizacionUnidad, hu1.claveChofer, hu1.observaciones, hu1.usuario, hu1.ip ".
                            " FROM alhistoricounidadestbl hu1, alUnidadesTbl au ".
                            " WHERE idHistorico = (SELECT MAX(hu2. idHistorico) FROM alhistoricounidadestbl hu2 WHERE hu2.vin = hu1.vin ) ".
                            " AND hu1.vin = au.vin ".
                            " AND au.avanzada = '".$_REQUEST['reUnidadesAvanzdaTxt']."' ";
        //echo "$sqlAddReUnidades<br>";
        $rsAdd = fn_ejecuta_Add($sqlAddReUnidades);

                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                {
                        $a['success'] = false;
                        $a['sql'] = $sqlAddReUnidades;
                $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten del Hist&oacuterico.";
            }
            
            if($a['success'])
            {
                $sqlGetVin = "SELECT vin FROM alUnidadesTbl ".
                             " WHERE avanzada = '".$_REQUEST['reUnidadesAvanzdaTxt']."' ";
        
                $rsVin = fn_ejecuta_query($sqlGetVin);
        
                $reUnidadesVin = $rsVin['root'][0]['vin'];
        
        
                $sqlUpdReUnidades = " UPDATE alultimodetalletbl ".
                                    " SET fechaEvento = NOW(), ".
                                    " claveMovimiento = 'RA', ".
                                    " centroDistribucion = '".$_SESSION['usuCompania']."', ".                                    
                                    " idTarifa = ".$idTarifa.
                                    " WHERE vin = '".$reUnidadesVin."' ";
        
                $rsUpd = fn_ejecuta_Upd($sqlUpdReUnidades);        

                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                        {
                                $a['success'] = false;
                                $a['sql'] = $sqlUpdReUnidades;
                        $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten del Hist&oacuterico.";
                    }                       
            }

        echo json_encode($a);
    }

    function modificacionUnidades(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['modificacionUnidadesVinHdn'] == ""){
            $e[] = array('id'=>'modificacionUnidadesVinHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['modificacionUnidadesSimboloHdn'] == ""){
            $e[] = array('id'=>'modificacionUnidadesSimboloHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['modificacionUnidadesColorHdn'] == ""){
            $e[] = array('id'=>'modificacionUnidadesColorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($_REQUEST['modificacionUnidadesDistOriginalHdn'] == ""){
            $e[] = array('id'=>'modificacionUnidadesDistOriginalHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $idHistoricoArr = explode('|', substr($_REQUEST['modificacionUnidadesIdHistoricoHdn'], 0, -1));
        if(in_array('', $idHistoricoArr)){
            $e[] = array('id'=>'modificacionUnidadesIdHistoricoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $centroDistArr = explode('|', substr($_REQUEST['modificacionUnidadesCentroDistHdn'], 0, -1));
        if(in_array('', $centroDistArr)){
            $e[] = array('id'=>'modificacionUnidadesCentroDistHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $movimientoArr = explode('|', substr($_REQUEST['modificacionUnidadesMovimientoHdn'], 0, -1));
        if(in_array('', $vinArr)){
            $e[] = array('id'=>'modificacionUnidadesMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $fechaArr = explode('|', substr($_REQUEST['modificacionUnidadesFechaHdn'], 0, -1));
        if(in_array('', $fechaArr)){
            $e[] = array('id'=>'modificacionUnidadesFechaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $distArr = explode('|', substr($_REQUEST['modificacionUnidadesDistribuidorHdn'], 0, -1));
        if(in_array('', $distArr)){
            $e[] = array('id'=>'modificacionUnidadesDistribuidorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $tarifaArr = explode('|', substr($_REQUEST['modificacionUnidadesTarifaHdn'], 0, -1));
        if(in_array('', $tarifaArr)){
            $e[] = array('id'=>'modificacionUnidadesTarifaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $localizacionArr = explode('|', substr($_REQUEST['modificacionUnidadesLocalizacionHdn'], 0, -1));
        if(in_array('', $localizacionArr)){
            $e[] = array('id'=>'modificacionUnidadesLocalizacionHdn','msg'=>getRequerido());
            $a['success'] = false;
        }

        if($a['success']){
            $sqlUpdUnidad = "UPDATE alUnidadesTbl SET ".
                            "distribuidor = '".$_REQUEST['modificacionUnidadesDistOriginalHdn']."',".
                            "simboloUnidad = '".$_REQUEST['modificacionUnidadesSimboloHdn']."',".
                            "color = '".$_REQUEST['modificacionUnidadesColorHdn']."' ".
                            "WHERE vin = '".$_REQUEST['modificacionUnidadesVinHdn']."'";

            fn_ejecuta_Upd($sqlUpdUnidad);

            if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $sqlUpdHistorico = "";

                for ($i=0; $i < sizeof($idHistoricoArr); $i++) {
                    $sqlUpdHistorico =  "UPDATE alHistoricoUnidadesTbl SET ".
                                        "centroDistribucion = '".$centroDistArr[$i]."',".
                                        "claveMovimiento = '".$movimientoArr[$i]."',".
                                        "fechaEvento = '".$fechaArr[$i]."',".
                                        "distribuidor = '".$distArr[$i]."',".
                                        "idTarifa  = '".$tarifaArr[$i]."', ".
                                        "localizacionUnidad = '".$localizacionArr[$i]."' ".
                                        "WHERE idHistorico = ".$idHistoricoArr[$i];

                    fn_ejecuta_Upd($sqlUpdHistorico);

                    if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != ""){
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdHistorico;
                        break;
                    }
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdUnidad;
            }
        }

        if($a['success']){
            $a['successMessage'] = getModificacionUnidadSuccessMsg();
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }

    function impresionEtiquetas(){

    require_once("../funciones/fpdf/pdf_js.php");
    //require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/barcode.php");
    //require_once("../funciones/generales.php");
    //require_once("../funciones/construct.php");
    //require_once("../funciones/Barcode39.php");
    /*********************************************
    Sólo necesita alUnidadesVinHdn para funcionar
    *********************************************/


    class PDF_AutoPrint extends PDF_JavaScript
    {
        function AutoPrint($dialog=false,$server='10.1.2.232', $printer= 'B-EX4T1')
        {
            //Open the print dialog or start printing immediately on the standard printer
            $param=($dialog ? 'true' : 'false');
            $script="print($param);";
            $this->IncludeJS($script);
            $script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
            $script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
            $script .= "print(pp);";
            $this->IncludeJS($script);
        }

        function AutoPrintToPrinter($server='10.1.2.232', $printer= 'B-EX4T1', $dialog=false)
        {
            //Print on a shared printer (requires at least Acrobat 6)
            $script = "var pp = getPrintParams();";
            if($dialog){
                $script .= "pp.interactive = pp.constants.interactionLevel.full;";
            }
            else{
                $script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
                $script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
                $script .= "print(pp);";
                $this->IncludeJS($script);
            }
        }
    }



    $success = true;

    $vinArr = explode('|', substr($_REQUEST['alUnidadesVinHdn'], 0, -1));

    $pdf = new FPDF('P', 'mm', array(100, 156));

    for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
        if($vinArr[$nInt] != ""){
            $sqlGetDataStr = "SELECT h.vin, h.distribuidor, lp.fila, lp.lugar, h.claveMovimiento, dc.tipoDistribuidor, ".
                                     "dc.observaciones AS puerto, dc.descripcionCentro , p.pais, h.localizacionUnidad AS patio, h.fechaEvento, pl.plaza ".
                                     "FROM caDistribuidoresCentrosTbl dc, caDireccionesTbl di, caColoniasTbl co, ".
                                     "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p, caPlazasTbl pl, alHistoricoUnidadesTbl h ".
                                     "LEFT JOIN allocalizacionpatiostbl lp ON lp.vin = h.vin ".
                                     "WHERE dc.distribuidorCentro = h.distribuidor ".
                                     "AND pl.idPlaza = dc.idPlaza ".
                                     "AND di.direccion = dc.direccionEntrega ".
                                     "AND co.idColonia = di.idColonia ".
                                     "AND m.idMunicipio = co.idMunicipio ".
                                     "AND e.idEstado = m.idEstado ".
                                     "AND p.idPais = e.idPais ".
                                     "AND h.vin = '".$vinArr[$nInt]."' ".
                                     "AND fechaEvento = (SELECT MAX(h1.fechaEvento) ".
                                     "FROM alHistoricoUnidadesTbl h1 ".
                                     "WHERE h1.vin = h.vin AND (h1.claveMovimiento = 'EP' OR h1.claveMovimiento = 'PR')) ";

            $rs = fn_ejecuta_query($sqlGetDataStr);
            $data = $rs['root'][0];
            if (sizeof($data) > 0) {
                $pdf = generarPdf($data, $pdf, $nInt);
            }
        }
    }


    $pdf->AutoPrint(false);
    $pdf->Output('../../codeBar/archivo.pdf','I');
    $pdf->Output('../../codeBar/archivo.pdf','F');

    function generarPdf($data, $pdf, $n){
        //Solo para pruebas, por defecto 0
        $border = 0;
        if($data['tipoDistribuidor'] == 'DX') {
            $paisPlaza = substr($data['pais'],0,6);
            $puertoNombre = substr($data['puerto'], 0, 9);
        } else {
            $paisPlaza = substr($data['plaza'],0,6);
            $puertoNombre = substr($data['descripcionCentro'],0, 9);
        }

        if(sizeof($data) > 0){
            $pdf= new PDF_AutoPrint();
            $pdf->AddPage('');
            $pdf->AddFont('skyline', '', 'skyline-reg.php');
            //$pdf ->MakeFont('c:\\windows\\fonts\\comic.ttf','comic.afm','cp1252');
            //$pdf->SetMargins(0.2, 0.2, 0.2,0.2);
            /*if ($n > 0) {
                $pdf->SetY(10);
                $pdf->SetX(10);
            }*/

            //Código Distribuidor y País
            $pdf->SetY(6);
            $pdf->SetX(4);
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(20,7,$data['distribuidor'], 0,0, 'L');

            $pdf->SetY(15);
            $pdf->SetX(4);
            $pdf->SetFont('skyline','',40);
            $pdf->Cell(25,13,$paisPlaza, 0,0, 'L');

            //Patio, Fila, Cajon
            $pdf->SetFont('Arial','B',8);
            $pdf->SetY(6);
            $pdf->SetX(42);
            $pdf->Cell(33, 5, 'Patio: '.$data['patio'],0,1,'L');

            $pdf->SetY(13);
            $pdf->SetX(42);
            $pdf->Cell(33, 5, 'Fila: '.$data['fila'],0,1,'L');

            $pdf->SetY(20);
            $pdf->SetX(42);
            $pdf->Cell(33, 5, 'Cajon: '.$data['lugar'],0,1,'L');

            //Puerto
            $pdf->setY(35);
            $pdf->SetX(1);
            $pdf->SetFont('skyline','',75);
            $pdf->Cell(70,30,$puertoNombre, 0,1, 'C');

            //Fecha y Hora
            if (!isset($data['fechaEvento'])) {
                $data['fechaEvento'] = date("Y-m-d H:i:s");
            }

            $pdf->SetFont('Arial','B',13);
            $pdf->setY(70);
            $pdf->SetX(12);
            $pdf->Cell(50,5,$data['fechaEvento'], 0,1, 'C');

            //Avanzada
            $pdf->SetFont('skyline','',61);
            $pdf->setY(80);
            $pdf->SetX(19);
            $pdf->Cell(40,23,substr($data['vin'], -8), 0,1, 'C');

            $vin=$data['vin'];

            //****************
            //CODIGO DE BARRAS
            //****************

            $fontSize = 100; // GD1 in px ; GD2 in point
            $marge = 10; // between barcode and hri in pixel
            $x = 400;  // barcode center x
            $y = 30;  // barcode center y
            $height = 180;  // barcode height
            $width = 4;  // barcode width
            $angle = 0; // rotation in degrees
            $code = $data['vin']; // vin code for barcode
            $type = 'code128'; // barcode type
            $imageTitle = $code.'_barcodeTemp.gif';

            //Se crea la imagen para usar para el codigo de barras
            $im = imagecreatetruecolor(800, 120);
            $black = imagecolorallocate($im, 0x00, 0x00, 0x00);
            $white = imagecolorallocate($im,0xff,0xff,0xff);
            imagefilledrectangle($im, 0, 0, 800, 120, $white);
            //imagestring($im, 5, 335, 100, "*".$data['vin']."*", imagecolorallocate($im, 0, 0, 0));

            //Se genera el Codigo de barras
            $data = Barcode::gd($im, $black, $x, $y, $angle, $type,   array('code'=>$code), $width, $height);
            Header('Content-type: image/gif');
            imagegif($im, $imageTitle);

            //Código de Barras y vin

            $pdf->Image($imageTitle,10,110, 50);

            //VIN
            $pdf->SetFont('Arial','',10);
            $pdf->setY(122);
            $pdf->SetX(15);
            $pdf->Cell(40,5,"*".$vin."*", 0,1, 'C');

            if (file_exists($imageTitle)) {
                unlink($imageTitle);
            }
            return $pdf;

        } else {
            echo json_encode(array('success'=>false, 'errorMessage'=>$_SESSION['error_sql']." <br> ".$sqlGetDataStr));
        }
    }
    }

    function eliminarHistorico(){

        $claveMovimientoArr = explode('|', substr($_REQUEST['modificacionUnidadesMovimientoHdn'], 0, -1));

        for ($nInt=0; $nInt < sizeof($claveMovimientoArr); $nInt++) {
            if($claveMovimientoArr[$nInt] != ""){
                $sqlCveDlt = "DELETE FROM alHistoricoUnidadesTbl ".
                "WHERE vin = '".$_REQUEST['alUnidadesVinHdn']."' ".
                "AND claveMovimiento = '".$claveMovimientoArr[$nInt]."';";

                $rs = fn_ejecuta_query($sqlGetDataStr);
            }
        }
    }
    function getValidaEntradaPatio(){
       $sqlGetUnidad = "SELECT (SELECT 1 FROM alhistoricounidadestbl dt1 WHERE dt1.claveMovimiento = 'PR' AND dt1.vin = au.vin) AS unidadPresentada, ".
                       "(SELECT 1 FROM alhistoricounidadestbl dt2 WHERE dt2.claveMovimiento = 'EP' AND dt2.vin = au.vin) AS unidadEntradaPatio, ".
                       "(SELECT 1 FROM alultimodetalletbl ud, cageneralestbl ge WHERE ud.claveMovimiento = ge.valor AND ge.tabla =  'alhistoricounidadestbl' AND ge.columna = 'nValidos' AND ud.VIN = au.vin  ) as movimientoValido, ".
                       "(SELECT 1 FROM alUnidadesTmp ut WHERE ut.vin = au.vin) as unidadBloqueada, ".
                       "(SELECT 1 FROM alretrabajounidadestbl ru WHERE ru.vin = au.vin) AS unidadRetrabajo ".
                       "FROM alUnidadesTbl au ".
                       "WHERE au.vin = '".$_REQUEST['alUnidadesAvanzadaHdn']."' ";
    }

    function validaVin(){
        $sqlValidaVin ="SELECT * FROM alunidadestbl al, alhistoricounidadestbl hi, alultimodetalletbl ud ".
                        "WHERE al.vin='".$_REQUEST['alUnidadesVinHdn']."' ".
                        "AND al.vin = hi.vin ".
                        "AND hi.vin = ud.vin ".
                        "AND hi.claveMovimiento='PR'";

        $rstValidaVin = fn_ejecuta_query($sqlValidaVin);

        echo json_encode($rstValidaVin);
    }

    function getUnidadesInstrucciones(){

        /*$lsWhereStr =   "WHERE ss.vupdate = (SELECT MAX(a6.vUpDate) ".
                        "FROM al660tbl a6 ".
                        "WHERE a6.vin = ss.vin ".
                        "AND a6.vuptime = (SELECT MAX(al.vUpTime) ".
                            "FROM al660tbl al ".
                            "WHERE al.vin = a6.vin) ) ".
                        "AND ss.dealerID != '' ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "ss.numAvanzada", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "ss.vin", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }*/


        $sqlInstrucciones = "SELECT tt.cveDisFac as dealerid , tt.vin,'' AS idTarifa, '' AS nombreTarifa, '' AS tarifa, '' AS marca ".
                        "FROM cadistribuidorescentrostbl su,alinstruccionesmercedestbl tt ".
                        "WHERE  su.distribuidorCentro = tt.cveDisFac ".
                        "AND tt.cveStatus in ('PH','PK') ".
                        "AND tt.vin='".$_REQUEST['alUnidadesVinHdn']."'";

        $rs = fn_ejecuta_query($sqlInstrucciones);

        //echo json_encode($sqlInstrucciones);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['nombreTarifa'];
        }

        return $rs;
    }

    function getBusca660 (){

         $sqlBusca660 = "SELECT distinct tt.vin ".
                        "FROM al660tbl tt ".
                        "WHERE  tt.numavanzada ='".$_REQUEST['alUnidadesAvanzadaHdn']."' ".
                        "AND tt.scaccode != 'COMO' ".
                        "AND tt.vin not in (SELECT hu.vin FROM alhistoricounidadestbl hu WHERE hu.vin=tt.vin and hu.clavemovimiento ='PR') " ;

        $rs = fn_ejecuta_query($sqlBusca660);

        echo json_encode($rs);

        /*for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['nombreTarifa'];
        }*/

        //return $rs;

    }

     function getBuscaDY (){

         $sqlBusca660 = "SELECT distinct tt.vin ".
                        "FROM alinstruccionesmercedestbl tt ".
                        "WHERE  tt.natType ='".$_REQUEST['alUnidadesAvanzadaHdn']."'";

        $rs = fn_ejecuta_query($sqlBusca660);

        echo json_encode($rs);

        /*for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['nombreTarifa'];
        }*/

        //return $rs;

    }
      function getUnidadesModificacion(){
        $lsWhereStr = " WHERE u.vin = h2.vin ".
                      " AND h2.idTarifa = t.idTarifa ";
                      //" AND h.centroDistribucion = h2.centroDistribucion".
                      //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                     // " AND u.distribuidor = h2.distribuidor";

        if($_REQUEST['alUnidadesParaServEspHdn'] == 1){
            $lsWhereStr .= //" AND h.vin = h2.vin ".
                           //" AND h.idTarifa = h2.idTarifa ".
                           //" AND h.claveMovimiento = h2.claveMovimiento ".
                           //" AND h.centroDistribucion = h2.centroDistribucion".
                           //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                           //" AND u.distribuidor = h2.distribuidor".
                           " AND claveMovimiento NOT IN ( ".   //AND h.fechaEvento = h2.fechaEvento
                            "SELECT valor FROM cageneralestbl ".
                            "WHERE (tabla = 'alholdsunidadestbl' AND columna  = 'claveHold') ".
                            "OR (tabla = 'trHistoricoUnidadesTbl' AND columna = 'nvalidos') )";
                            //")";
                        //")";
        } else {
            $lsWhereStr .= //" AND h.vin = h2.vin ".
                           //" AND h.claveMovimiento = h2.claveMovimiento ".
                            //" AND h.centroDistribucion = h2.centroDistribucion".
                            //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                            " AND u.distribuidor = d2.distribuidorCentro";
                           //"AND h.fechaEvento = h2.fechaEvento ";
        }


        $lsWhereStr .= " AND sm.simboloUnidad = u.simboloUnidad ".
                       " AND u.distribuidor = d2.distribuidorCentro " ;
                       //" AND h.centroDistribucion = h2.centroDistribucion".
                        //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                        //" AND u.distribuidor = h2.distribuidor";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "u.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "u.vin", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "h.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesSimboloUnidadHdn'], "u.simboloUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesColorHdn'], "u.color", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            if ($_REQUEST['alUnidadesRepuveHdn'] == 'NOTNULL') {
                $lsWhereStr .= "AND u.folioRepuve IS NOT NULL ";
            } else if ($_REQUEST['alUnidadesRepuveHdn'] == 'NULL'){
                $lsWhereStr .= "AND u.folioRepuve IS NULL ";
            } else {
                $lsCondicionStr = fn_construct($_REQUEST['alUnidadesRepuveHdn'], "u.folioRepuve", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesCentroDistHdn'], "h2.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorTxt'], "h2.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesClaveMovimientoHdn'], "h2.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesMarcaHdn'], "sm.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesIdTarifaHdn'], "t.idTarifa", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesTipoDistribuidorHdn'], "d2.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetUnidadesStr = "SELECT u.vin, u.avanzada, u.distribuidor AS distribuidorInicial, ".
                             "u.simboloUnidad, u.color, u.folioRepuve, h2.centroDistribucion, h2.fechaEvento, h2.distribuidor, h2.idTarifa, ".
                             "h2.localizacionUnidad, h2.claveChofer, h2.observaciones, h2.usuario, h2.ip, h2.claveMovimiento AS cveMovHistorico, ".
                             "t.tarifa, t.descripcion AS descTarifa, t.tipoTarifa, sm.tieneRepuve AS simboloRepuve, ".
                             "d2.estatus AS estatusDistribuidor, d2.tipoDistribuidor, sm.marca, sm.descripcion AS nombreSimbolo, ".
                             "(SELECT IFNULL((SELECT ud.claveMovimiento FROM alUltimoDetalleTbl ud ".
                                "WHERE ud.vin = u.vin), "."(SELECT h2.claveMovimiento FROM alhistoricounidadestbl h ".
                                "WHERE h2.vin = u.vin AND h2.fechaEvento = (".
                                    "SELECT MAX(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.vin = h2.vin )))) AS claveMovimiento,".
                             "(SELECT 1 FROM caGeneralesTbl g2 WHERE g2.tabla = 'unidadAsignar' ".
                                "AND g2.columna = 'claveValida' AND g2.valor = (SELECT IFNULL((SELECT ud.claveMovimiento FROM alUltimoDetalleTbl ud ".
                                "WHERE ud.vin = u.vin), "."(SELECT h2.claveMovimiento FROM alhistoricounidadestbl h ".
                                "WHERE h2.vin = u.vin AND h2.fechaEvento = (".
                                    "SELECT MAX(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.vin = h2.vin ))))) AS listaAsignar, ".
                             "(SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin ".
                                "AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 ".
                                    "WHERE ud2.vin=ud.vin)) AS claveMovDetenidas, ".
                             "(SELECT g2.nombre FROM caGeneralesTbl g2 WHERE g2.tabla = 'alUnidadesDetenidasTbl' ".
                                "AND g2.columna = 'claveMovimiento' AND g2.valor = ".
                                    "(SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin ".
                                    "AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 ".
                                        "WHERE ud2.vin=ud.vin))".
                                ") AS nombreClaveMovDetenidas, ".
                             "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h2.claveMovimiento ".
                             "AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreClaveMov, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d ".
                             "WHERE d.distribuidorCentro = u.distribuidor) AS nombreDistribuidor, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE ".
                             "d.distribuidorCentro = h2.centroDistribucion) AS descCentroDistribucion, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE ".
                             "d.distribuidorCentro = h2.localizacionUnidad) AS descLocalizacion,".
                             "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = sm.marca) as descMarca, ".
                             "(SELECT co.descripcion FROM caColorUnidadesTbl co WHERE co.marca = sm.marca ".
                                "AND co.color = u.color) AS nombreColor, ".
                             "(SELECT 1 FROM alUnidadesDetenidasTbl de WHERE de.vin = u.vin AND de.claveMovimiento = 'UD') AS detenida, ".
                             "(SELECT 1 FROM alUnidadesTmp tmp WHERE tmp.vin = u.vin) AS bloqueada, ".
                             "(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta ".
                                 " WHERE un.vin =u.vin ".
                                " AND un.idTarifa = ta.idTarifa ".
                                " AND ta.tipoTarifa = 'N') AS tieneTarifaNormal, ".
                             "(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta ".
                                 " WHERE un.vin =u.vin ".
                                " AND un.idTarifa = ta.idTarifa ".
                                " AND ta.tipoTarifa = 'E') AS tieneTarifaEspecial, ".
                             "(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta ".
                                 " WHERE un.vin =u.vin ".
                                " AND un.idTarifa = ta.idTarifa ".
                                " AND ta.tipoTarifa = 'C') AS tieneTarifaCD, ".
                             "(SELECT 1 FROM alDestinosEspecialesTbl de WHERE de.vin = u.vin LIMIT 1) AS destinoEspecial, ".
                             "(SELECT 1 FROM alRetrabajoUnidadesTbl ru WHERE ru.vin = u.vin) AS reTrabajo, ".
                             "(SELECT 1 FROM trunidadesembarcadastbl ue WHERE ue.vin = u.vin AND ue.claveMovimiento = 'L') AS embarcada, ".
                             "(SELECT dc.sucursalDe FROM caDistribuidoresCentrosTbl dc ".
                                "WHERE dc.distribuidorCentro = h2.localizacionUnidad) AS localizacionComoCentro, ".
                             "(SELECT de.distribuidorDestino FROM aldestinosespecialestbl de WHERE u.vin = de.vin AND idDestinoEspecial = (SELECT MAX(de1.idDestinoEspecial) FROM aldestinosespecialestbl de1 WHERE de1.vin = de.vin)) as distribuidorDestino ".
                             "FROM alUnidadesTbl u,alultimodetalletbl h2, caTarifasTbl t, caSimbolosUnidadesTbl sm, ".
                             "caDistribuidoresCentrosTbl d2 ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetUnidadesStr);
    //  echo json_encode($sqlGetUnidadesStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            //Para checar si tiene un servicio especial pendiente
            if($_REQUEST['alUnidadesListaMovimientoHdn'] != ""){
                $data = getUltimosMovimientosUnidad($rs['root'][$iInt]['vin'], $_REQUEST['alUnidadesListaMovimientoHdn']);
                $rs['root'][$iInt]['listaEstatus'] = $data['success'];
            }
            $rs['root'][$iInt]['descDist'] = $rs['root'][$iInt]['distribuidor']." - ".$rs['root'][$iInt]['nombreDistribuidor'];
            $rs['root'][$iInt]['descColor'] = $rs['root'][$iInt]['color']." - ".$rs['root'][$iInt]['nombreColor'];
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['descTarifa'];
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['nombreSimbolo'];
            $rs['root'][$iInt]['marcaDesc'] = $rs['root'][$iInt]['marca']." - ".$rs['root'][$iInt]['descMarca'];
            $rs['root'][$iInt]['detenida'] = $rs['root'][$iInt]['detenida'];
        }
        return $rs;
    }

    function validarRC(){

        $sqlRC="SELECT claveMovimiento FROM alUltimoDetalleTbl WHERE VIN='".$_REQUEST['trap010VinCmb']."'";
        $rsSqlRC=fn_ejecuta_query($sqlRC);

        echo json_encode($rsSqlRC);
    }

    function validaSalidas($vin){
         $sqlCheckVinStr = "SELECT u.*, c.valor, c.estatus ".
                          "FROM alUltimoDetalleTbl u, caGeneralesTbl c ".
                          "WHERE u.vin = '".$vin."' ".
                          "AND c.columna='salidasValidas' ".
                          "AND u.claveMovimiento = c.valor ";

        $rs = fn_ejecuta_query($sqlCheckVinStr);
        //echo json_encode("capturando".$rs['root'][0]['estatus']);

        if (sizeof($rs['root']) > 0 && $rs['root'][0]['estatus'] != null) {
            
            $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                                        "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                        "claveChofer, observaciones, usuario, ip) ".
                                        "VALUES(".
                                        "'".$rs['root'][0]['centroDistribucion']."',".
                                        "'".$vin."',".
                                        "NOW() - INTERVAL 60 SECOND ,".
                                        "'".$rs['root'][0]['estatus']."',".
                                        "'".$rs['root'][0]['distribuidor']."',".
                                        "'".$rs['root'][0]['idTarifa']."',".
                                        "'".$rs['root'][0]['localizacionUnidad']."',".
                                        replaceEmptyNull($RQchofer).",".
                                        "'".$rs['root'][0]['observaciones']."',".
                                        "'".$_SESSION['idUsuario']."',".
                                        "'".getClientIP()."')";

            $rs = fn_ejecuta_query($sqlAddCambioHistoricoStr);
            
        } 
    }
    function validarBR(){

        $sqlBR="SELECT vin FROM alcargabluereporttbl WHERE VIN='".$_REQUEST['alUnidadesVinHdn']."'";
        $rsSqlBR=fn_ejecuta_query($sqlBR);

        echo json_encode($rsSqlBR);
    }

    function getUnidadesEspeciales(){
        $lsWhereStr = " WHERE u.vin = h2.vin ".
                      " AND h2.idTarifa = t.idTarifa ".
                      //" AND h.centroDistribucion = h2.centroDistribucion".
                      //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                      " AND u.distribuidor = h2.distribuidor";

        if($_REQUEST['alUnidadesParaServEspHdn'] == 1){
            $lsWhereStr .= //" AND h.vin = h2.vin ".
                           //" AND h.idTarifa = h2.idTarifa ".
                           //" AND h.claveMovimiento = h2.claveMovimiento ".
                           //" AND h.centroDistribucion = h2.centroDistribucion".
                           //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                           " AND claveMovimiento NOT IN ( ".   //AND h.fechaEvento = h2.fechaEvento
                            "SELECT valor FROM cageneralestbl ".
                            "WHERE (tabla = 'alholdsunidadestbl' AND columna  = 'claveHold') ".
                            "OR (tabla = 'trHistoricoUnidadesTbl' AND columna = 'nvalidos') )";
                            //")";
                        //")";
        } else {
            //$lsWhereStr .= //" AND h.vin = h2.vin ".
                           //" AND h.claveMovimiento = h2.claveMovimiento ".
                            //" AND h.centroDistribucion = h2.centroDistribucion".
                            //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".
                            //" AND u.distribuidor = h2.distribuidor";
                           //"AND h.fechaEvento = h2.fechaEvento ";
        }


        $lsWhereStr .= " AND sm.simboloUnidad = u.simboloUnidad ".
                       " AND u.distribuidor = d2.distribuidorCentro ";
                       //" AND h.centroDistribucion = h2.centroDistribucion".
                        //" AND h.fechaEvento=(select max(fechaEvento) from alhistoricounidadestbl e where e.vin = u.vin )".

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesAvanzadaHdn'], "u.avanzada", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesVinHdn'], "u.vin", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorHdn'], "h.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesSimboloUnidadHdn'], "u.simboloUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesColorHdn'], "u.color", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            if ($_REQUEST['alUnidadesRepuveHdn'] == 'NOTNULL') {
                $lsWhereStr .= "AND u.folioRepuve IS NOT NULL ";
            } else if ($_REQUEST['alUnidadesRepuveHdn'] == 'NULL'){
                $lsWhereStr .= "AND u.folioRepuve IS NULL ";
            } else {
                $lsCondicionStr = fn_construct($_REQUEST['alUnidadesRepuveHdn'], "u.folioRepuve", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesCentroDistHdn'], "h2.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesDistribuidorTxt'], "h2.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesClaveMovimientoHdn'], "h2.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesMarcaHdn'], "sm.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesIdTarifaHdn'], "t.idTarifa", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alUnidadesTipoDistribuidorHdn'], "d2.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        
        $select = "(SELECT 1 FROM alUnidadesDetenidasTbl de WHERE de.vin = u.vin AND de.claveMovimiento = 'UD') AS detenida, ";
        if($_SESSION['usuCompania'] == 'CDLZC' || $_SESSION['usuCompania'] == 'CDVER')
        {
                //$select = "0 AS detenida, ";
        }

        $sqlGetUnidadesStr = "SELECT u.vin, u.avanzada, u.distribuidor AS distribuidorInicial, ".
                             "u.simboloUnidad, u.color, u.folioRepuve, h2.centroDistribucion, h2.fechaEvento, h2.distribuidor, h2.idTarifa, ".
                             "h2.localizacionUnidad, h2.claveChofer, h2.observaciones, h2.usuario, h2.ip, h2.claveMovimiento AS cveMovHistorico, ".
                             "t.tarifa, t.descripcion AS descTarifa, t.tipoTarifa, sm.tieneRepuve AS simboloRepuve, ".
                             "d2.estatus AS estatusDistribuidor, d2.tipoDistribuidor, sm.marca, sm.descripcion AS nombreSimbolo, ".
                             "(SELECT IFNULL((SELECT ud.claveMovimiento FROM alUltimoDetalleTbl ud WHERE ud.vin = u.vin), "."(SELECT h2.claveMovimiento FROM alhistoricounidadestbl h WHERE h2.vin = u.vin AND h2.fechaEvento = (SELECT MAX(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.vin = h2.vin )))) AS claveMovimiento, ".                             
                             "(SELECT COUNT(*) FROM alhistoricounidadestbl a, catarifastbl b WHERE a.vin = u.vin AND a.claveMovimiento = 'OM' AND a.idTarifa = b.idTarifa AND b.tipoTarifa = 'N') AS movimientoOM, ".
                             "(SELECT IFNULL((SELECT 1 FROM caGeneralesTbl g2 WHERE g2.tabla = 'unidadAsignar' AND g2.columna = 'claveValida' AND g2.valor = (SELECT ud.claveMovimiento FROM alUltimoDetalleTbl ud WHERE ud.vin = u.vin)),0)) AS listaAsignar, ".
                             "(SELECT 1 FROM alUnidadesDetenidasTbl ud WHERE ud.vin = h2.vin AND ud.centroLibera IS NULL) AS detenida, ".
                             "(SELECT 1 FROM   alcotizacionesespecialestbl ud WHERE  ud.vin = h2.vin AND ud.estatus='CZ') AS cotizacion, ".                             
                             "(SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 WHERE ud2.vin=ud.vin)) AS claveMovDetenidas, ".
                             "(SELECT g2.nombre FROM caGeneralesTbl g2 WHERE g2.tabla = 'alUnidadesDetenidasTbl' AND g2.columna = 'claveMovimiento' AND g2.valor = (SELECT ud.claveMovimiento FROM alUnidadesDetenidasTbl ud WHERE ud.vin = u.vin AND ud.numeroMovimiento = (SELECT MAX(ud2.numeroMovimiento) FROM alUnidadesDetenidasTbl ud2 WHERE ud2.vin=ud.vin))) AS nombreClaveMovDetenidas, ".
                             "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.valor=h2.claveMovimiento AND g.tabla='alHistoricoUnidadesTbl' AND g.columna='claveMovimiento') AS nombreClaveMov, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE d.distribuidorCentro = u.distribuidor) AS nombreDistribuidor, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE d.distribuidorCentro = h2.centroDistribucion) AS descCentroDistribucion, ".
                             "(SELECT d.descripcionCentro FROM caDistribuidoresCentrosTbl d WHERE d.distribuidorCentro = h2.localizacionUnidad) AS descLocalizacion,".
                             "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = sm.marca) as descMarca, ".
                             "(SELECT co.descripcion FROM caColorUnidadesTbl co WHERE co.marca = sm.marca AND co.color = u.color) AS nombreColor, ".
                             $select.
                             "(SELECT 1 FROM alUnidadesTmp tmp WHERE tmp.vin = u.vin) AS bloqueada, ".
                             "(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta WHERE un.vin =u.vin AND un.idTarifa = ta.idTarifa AND ta.tipoTarifa in('N','E')) AS tieneTarifaNormal, ".
                             //"(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta WHERE un.vin =u.vin AND un.idTarifa = ta.idTarifa AND ta.tipoTarifa = 'E') AS tieneTarifaEspecial, ".
                             "(SELECT 1 FROM alultimodetalletbl un, catarifastbl ta WHERE un.vin =u.vin AND un.idTarifa = ta.idTarifa AND ta.tipoTarifa = 'C') AS tieneTarifaCD, ".
                             "(SELECT 1 FROM alDestinosEspecialesTbl de WHERE de.vin = u.vin LIMIT 1) AS destinoEspecial, ".
                             "(SELECT 1 FROM alRetrabajoUnidadesTbl ru WHERE ru.vin = u.vin) AS reTrabajo, ".
                             "(SELECT 1 FROM trunidadesembarcadastbl ue WHERE ue.vin = u.vin AND ue.claveMovimiento = 'L') AS embarcada, ".
                             "(SELECT dc.sucursalDe FROM caDistribuidoresCentrosTbl dc WHERE dc.distribuidorCentro = h2.localizacionUnidad) AS localizacionComoCentro, ".
                             "(SELECT de.distribuidorDestino FROM aldestinosespecialestbl de WHERE u.vin = de.vin AND idDestinoEspecial = (SELECT MAX(de1.idDestinoEspecial) FROM aldestinosespecialestbl de1 WHERE de1.vin = de.vin)) as distribuidorDestino ".
                             "FROM alUnidadesTbl u,alultimodetalletbl h2, caTarifasTbl t, caSimbolosUnidadesTbl sm, ".
                             "caDistribuidoresCentrosTbl d2 ".$lsWhereStr; 
                //echo "<br>$sqlGetUnidadesStr<br>";
        $rs = fn_ejecuta_query($sqlGetUnidadesStr);
      //    echo json_encode($rs);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            //Para checar si tiene un servicio especial pendiente
            if($_REQUEST['alUnidadesListaMovimientoHdn'] != ""){
                $data = getUltimosMovimientosUnidad($rs['root'][$iInt]['vin'], $_REQUEST['alUnidadesListaMovimientoHdn']);
                $rs['root'][$iInt]['listaEstatus'] = $data['success'];
            }
            $rs['root'][$iInt]['descDist'] = $rs['root'][$iInt]['distribuidor']." - ".$rs['root'][$iInt]['nombreDistribuidor'];
            $rs['root'][$iInt]['descColor'] = $rs['root'][$iInt]['color']." - ".$rs['root'][$iInt]['nombreColor'];
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['descTarifa'];
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['nombreSimbolo'];
            $rs['root'][$iInt]['marcaDesc'] = $rs['root'][$iInt]['marca']." - ".$rs['root'][$iInt]['descMarca'];
            $rs['root'][$iInt]['detenida'] = $rs['root'][$iInt]['detenida'];
        }
        echo json_encode($rs);
    }

    function eliminarTLP(){

        $dltTLP="DELETE FROM alcargatlptbl ".
                "WHERE tlp='".$_REQUEST['TLP']."'";
        fn_ejecuta_query($dltTLP);



    }

     function getHistoriaUnidadCambioD($vin = ''){

        if($vin == ''){
            $vin = $_REQUEST['trap276VinHdn'];
        }
        //SE COMENTARON LAS VALIDACIONES DE HORA EN LO QUE SE VE SI SE USARAN O NO
        $sqlGetHistoricoUnidadStr = "SELECT hu.*, /*t2.idTractor, t2.tractor, t2.folio,*/ ".                                    
                                    "(SELECT lp.fila FROM alLocalizacionPatiosTbl lp WHERE lp.vin = hu.vin) AS fila, ".
                                    "(SELECT lp2.lugar FROM alLocalizacionPatiosTbl lp2 WHERE lp2.vin = hu.vin) AS lugar, ".
                                    "(SELECT tf.descripcion FROM caTarifasTbl tf WHERE idTarifa = hu.idTarifa) AS descTarifa, ".
                                    "(SELECT tf.tarifa FROM caTarifasTbl tf WHERE idTarifa = hu.idTarifa) AS claveTarifa, ".
                                    "(SELECT cg.nombre FROM caGeneralesTbl cg WHERE cg.tabla='alHistoricoUnidadesTbl' AND cg.columna='claveMovimiento' AND cg.valor=hu.claveMovimiento) AS nombreClaveMov ".
                                    "FROM alhistoricounidadestbl hu ".
                                    /*"LEFT JOIN ( ".
                                        "SELECT ut2.vin, tv2.folio, tr.idTractor, tr.tractor, hu2.claveMovimiento ".
                                        "FROM trunidadesdetallestalonestbl ut2, trtalonesviajestbl tv2, trviajestractorestbl vt, ".
                                        "catractorestbl tr, alhistoricounidadestbl hu2 ".
                                        "WHERE hu2.vin = ut2.vin ".
                                        "AND tv2.idTalon = ut2.idTalon ".
                                        "AND vt.idViajeTractor = tv2.idViajeTractor ".
                                        "AND tr.idTractor = vt.idTractor ".
                                        "AND ut2.estatus = 'AM' ".
                                        "AND tv2.claveMovimiento != 'TX' ".
                                        "AND hu2.idHistorico = ( ".
                                            "SELECT MAX(hu3.idHistorico) FROM alhistoricounidadestbl hu3 ".
                                            "WHERE hu3.vin = hu2.vin AND hu3.claveMovimiento = hu2.claveMovimiento ".
                                        ") ".
                                        "HAVING hu2.claveMovimiento = 'AM' ".
                                    ") AS t2 ".
                                    "ON hu.vin = t2.vin ".
                                    "AND hu.claveMovimiento = t2.claveMovimiento ".*/
                                    "WHERE hu.vin = '".$vin."' ".
                                    "ORDER BY hu.fechaEvento ";

        $rs = fn_ejecuta_query($sqlGetHistoricoUnidadStr);
        //echo json_encode($sqlGetHistoricoUnidadStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descClaveMov'] = $rs['root'][$iInt]['claveMovimiento']." - ".$rs['root'][$iInt]['nombreClaveMov'];
            $rs['root'][$iInt]['localizacionCompleta'] = $rs['root'][$iInt]['localizacionUnidad'].", ".$rs['root'][$iInt]['fila']." - ".$rs['root'][$iInt]['lugar'];
        }

        if($_REQUEST['start'] != '' && $_REQUEST['limit'] != ''){
            $rs['root'] = array_slice($rs['root'], $_REQUEST['start'], $_REQUEST['start']+$_REQUEST['limit']);
        }

        return $rs;
    }


?>

