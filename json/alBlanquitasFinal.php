<?php
	session_start();
	$_SESSION['modulo'] = "catBancos";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    //$_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']) {
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['alBlanquitasHdn']) {
        case 'sqlGetUndBlq':
            sqlGetUndBlq();
            break;
        case 'addBlq':
        	addBlq();
            break;
        case 'addBlanquita':
            addBlanquita();
            break;
        case 'deleteTmp':
            deleteTmp();
        break;
        default:
            echo 'no';
    }

    function sqlGetUndBlq(){
		$sqlGetConteo = "SELECT COUNT(blq.vin) as vin, ".
		  							"(SELECT COUNT(b1.distribuidorCentro) FROM alblanquitastmp b1 WHERE b1.distribuidorCentro NOT IN (SELECT distribuidorCentro FROM cadistribuidorescentrostbl) ) as numDistribuidores, ".
		  							"(SELECT COUNT(b2.simboloUnidad) FROM alblanquitastmp b2 WHERE b2.simboloUnidad NOT IN (SELECT su.simboloUnidad FROM casimbolosunidadestbl su)) as numSimbolo, ".
		  							"(SELECT COUNT(b3.vin) FROM alblanquitastmp b3 WHERE b3.claveMovimiento = 'BJ' ) as undBj, ".
		  							"(SELECT COUNT(b4.vin) FROM alblanquitastmp b4 WHERE b4.claveMovimiento = 'CN' ) as undCn ".
		  							"FROM alblanquitastmp blq;";
		$rs = fn_ejecuta_query($sqlGetConteo);
		echo json_encode($rs);
    }

    function addBlanquita(){       

        $sqlGetBlqTmp = "SELECT * FROM alblanquitastmp;";

        $rsBlqTmp = fn_ejecuta_query($sqlGetBlqTmp);                       

        for ($i=0; $i < sizeof($rsBlqTmp['root']); $i++) {
            // FORMATo DE FECHA COMO VIENE EN LA CONSULTA 2019/07/25 DE LA TABLA alBlanquitasTmp
            $tmpMes = substr($rsBlqTmp['root'][$i]['fechaFacturacion'], 5,2);
            $tmpDia = substr($rsBlqTmp['root'][$i]['fechaFacturacion'], 8,2);
            $tmpDistribuidor = $rsBlqTmp['root'][$i]['distribuidorCentro'];

            $sqlGetBlqTbl = "SELECT vin,distribuidorCentro,SUBSTRING(fechaFacturacion,9,2) as numDia,SUBSTRING(fechaFacturacion,6,2) as numMes,claveMovimiento FROM alblanquitastbl ".
                            "WHERE vin = '".$rsBlqTmp['root'][$i]['vin']."';";

            $rsGetBlqTbl = fn_ejecuta_query($sqlGetBlqTbl);

            //MES,DIA, DISTRIBUIDOR Y CLAVEmOVIMIENTO DE LA TABLA alBlanquitasTbl
            $tblMes = $rsGetBlqTbl['root'][0]['numMes'];
            $tblDia = $rsGetBlqTbl['root'][0]['numDia'];
            $tblDistribuidor = $rsGetBlqTbl['root'][0]['distribuidorCentro'];
            $tblClaveMovimiento = $rsGetBlqTbl['root'][0]['claveMovimiento'];
            $tmpDistribuidorCentro = $rsBlqTmp['root'][$i]['distribuidorCentro'];                                 
            $tmpSimboloUnidad = $rsBlqTmp['root'][$i]['simboloUnidad']; 
            $tmpFechaFacturacion = $rsBlqTmp['root'][$i]['fechaFacturacion']; 
            $tmpImporteSeguro = $rsBlqTmp['root'][$i]['importeSeguro'];
            $tmpImporteFactura = $rsBlqTmp['root'][$i]['importeFactura'];
            $tmpImporteCuota = $rsBlqTmp['root'][$i]['importeCuota'];
            $tmpImporteCredito = $rsBlqTmp['root'][$i]['importeCredito']; 
            $tmpImporteGasto = $rsBlqTmp['root'][$i]['importeGasto'];
            $tmpImporteIva = $rsBlqTmp['root'][$i]['importeIva']; 
            $tmpFechaBaja = $rsBlqTmp['root'][$i]['fechaBaja']; 
            $tmpFechaCancelacion = $rsBlqTmp['root'][$i]['fechaCancelacion']; 
            $tmpClaveMovimiento = $rsBlqTmp['root'][$i]['claveMovimiento'];        

            // 01 - VALIDA SI EXISTE EN LA TABLA alBlanquitasTbl            
            if (sizeof($rsGetBlqTbl['root']) == '1') {
                //validar si la unidad esta duplicada o hay que darla de baja o cancelarla
                    /*echo "diaTemporal ".$tmpDia." ";
                    echo "diaTbl ".$tmpDia." ".PHP_EOL;
                    echo "mesTmp ".$tmpMes." ";
                    echo "mesTbl ".$tblMes." ".PHP_EOL;
                    echo "cMovimiento ".$tblClaveMovimiento." ";*/
                if ($tmpDia == $tblDia && $tmpMes == $tblMes) {
                    //echo "la unidad esta duplicada".$rsBlqTmp['root'][$i]['vin'].PHP_EOL;
                }else{
                    // VALIDAR SI HAY QUE DARLA DE BAJA O CANCELARLA
                    if($tmpDia !== $tblDia && $tmpMes == $tblMes && $tmpDistribuidor == $tblDistribuidor){
                        echo "esta unidad se da de baja:".$rsBlqTmp['root'][$i]['vin'].PHP_EOL;
                        $sqlUpdBlq = "UPDATE alBlanquitasTmp SET fechaBaja = '" .$rsBlqTmp['root'][$i]['fechaFacturacion']. "', claveMovimiento = 'BA' WHERE vin = '" .$rsBlqTmp['root'][$i]['vin']."';";
                        fn_ejecuta_query($sqlUpdBlq);
                    }elseif($tmpClaveMovimiento == 'B'){
                        echo "esta unidad se da de baja:".$rsBlqTmp['root'][$i]['vin'].PHP_EOL;
                        $sqlUpdBlq = "UPDATE alBlanquitasTmp SET fechaBaja = '" .$rsBlqTmp['root'][$i]['fechaFacturacion']. "', claveMovimiento = 'BA' WHERE vin = '" .$rsBlqTmp['root'][$i]['vin']."';";
                        fn_ejecuta_query($sqlUpdBlq);
                    }elseif($tmpMes !== $tblMes && $tblClaveMovimiento == 'DL' ){
                        echo "esta unidad se se cancela:".$rsBlqTmp['root'][$i]['vin'].PHP_EOL;
                        $sqlUpdBlq = "UPDATE alBlanquitasTmp SET fechaCancelacion = '" .$rsBlqTmp['root'][$i]['fechaFacturacion']. "', claveMovimiento = 'CN' WHERE vin = '" .$rsBlqTmp['root'][$i]['vin']."';";
                        fn_ejecuta_query($sqlUpdBlq);
                    }elseif ($tmpMes !== $tblMes && $tblClaveMovimiento == 'BA') {
                        // SE REACTIVA LA UNIDAD
                        echo "esta unidad se reactiva:".$rsBlqTmp['root'][$i]['vin'].PHP_EOL;
                        $sqlAddUndBlq = "INSERT INTO alBlanquitasTbl (compania, distribuidorCentro, vin, simboloUnidad, fechaFacturacion, importeSeguro, importeFactura, importeCuota, importeCredito, importeGasto, importeIva, fechaBaja, fechaCancelacion, claveMovimiento) VALUES(" . "'".$_SESSION['usuCompania']."', " . 
                                        "'".$rsBlqTmp['root'][$i]['distribuidorCentro']. "', " . 
                                        "'".$rsBlqTmp['root'][$i]['vin']. "', " . 
                                        "'".$rsBlqTmp['root'][$i]['simboloUnidad']."', " . 
                                        "'".$rsBlqTmp['root'][$i]['fechaFacturacion']."', " . 
                                        "'".$rsBlqTmp['root'][$i]['importeSeguro']."', " . 
                                        "'".$rsBlqTmp['root'][$i]['importeFactura']."', " . 
                                        "'".$rsBlqTmp['root'][$i]['importeCuota']."', " . 
                                        "'".$rsBlqTmp['root'][$i]['importeCredito']."', " . 
                                        "'".$rsBlqTmp['root'][$i]['importeGasto']."', " . 
                                        "'".$rsBlqTmp['root'][$i]['importeIva']."', " . 
                                        "null," . 
                                        "null," . 
                                        "'DL');";

                        fn_ejecuta_query($sqlAddUndBlq); 
                    }
                }                
            }else{
            	$clasDsitribuidor = substr($tmpDistribuidorCentro,0,2);
            	echo "distribuidor".$clasDsitribuidor;
            	if ($clasDsitribuidor == 'M8') {
            		echo "unidad de Chryler";
            	}else{
	                //NO EXISTE LA TABLA alBlanquitasTl SE INSERTA AUTOMATICAMENTE
	                echo "No existe esta unidad".$rsBlqTmp['root'][$i]['vin'].PHP_EOL;
	                $sqlAddUndBlq = "INSERT INTO alBlanquitasTbl (compania, distribuidorCentro, vin, simboloUnidad, fechaFacturacion, importeSeguro, importeFactura, importeCuota, importeCredito, importeGasto, importeIva, fechaBaja, fechaCancelacion, claveMovimiento) VALUES(" . "'".$_SESSION['usuCompania']."', " . 
	                                "'". $rsBlqTmp['root'][$i]['distribuidorCentro'] . "', " . 
	                                "'". $rsBlqTmp['root'][$i]['vin'] . "', " . 
	                                "'". $rsBlqTmp['root'][$i]['simboloUnidad'] ."', " . 
	                                "'". $rsBlqTmp['root'][$i]['fechaFacturacion'] ."', " . 
	                                "'". $rsBlqTmp['root'][$i]['importeSeguro'] ."', " . 
	                                "'". $rsBlqTmp['root'][$i]['importeFactura'] ."', " . 
	                                "'". $rsBlqTmp['root'][$i]['importeCuota'] ."', " . 
	                                "'". $rsBlqTmp['root'][$i]['importeCredito'] ."', " . 
	                                "'". $rsBlqTmp['root'][$i]['importeGasto'] ."', " . 
	                                "'". $rsBlqTmp['root'][$i]['importeIva'] ."', " . 
	                                "null," . 
	                                "null," . 
	                                "'DL');";

	                fn_ejecuta_query($sqlAddUndBlq);   
            	}
            }            
        }
        
        $dltBlqTmp = "DELETE FROM alBlanquitasTmp";

        $rsBlqTmp = fn_ejecuta_query($dltBlqTmp);        
        echo json_encode('Unidades Procesadas');        
    }

     function deleteTmp(){
        $delTmpBlanquitas = "DELETE FROM alblanquitastmp;";
        fn_ejecuta_query($delTmpBlanquitas);        
    }

?>
