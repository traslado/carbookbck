<?php
    session_start();
	
	//include 'iVerificacionRepuve.php';
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");

	
    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["archVerifRepuveArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["archVerifRepuveArchivoFld"]["error"];
			$a['errorMessage'] = "Falta escoger un archivo" ;

		} else {
			$temp_file_name = $_FILES['archVerifRepuveArchivoFld']['tmp_name']; 
			$original_file_name = $_FILES['archVerifRepuveArchivoFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/genArchRepuve/'. $file_name . $ext;

			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					//$a['root'] = genArchivoTxt($new_name);
					$a['root'] = genArchivoTxt();
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);



function genArchivoTxt(){

    date_default_timezone_set('America/Mexico_City');

	$ruta = "../../genArchRepuve/";
	$rutaRespal = "../../respaldogenArchRepuve/";

	$nombres = scandir($ruta,1);        
	$files2 = count($nombres);
	$files3 = $files2 -1;
	$files4 = $files2 -2;
	
	unset($nombres[$files3]);	
	unset($nombres[$files4]);
	
	//var_dump($nombres);
	
	for ($i=0; $i < sizeof($nombres); $i++){
		
		$lineas1 = $ruta.$nombres[$i];		
		$lineas = fopen($lineas1,"r");
		//var_dump($lineas1,$lineas);
		///*
		while(!feof($lineas)){
				$lin = fgets($lineas);
				$datos = explode("|",$lin);
				$vin = trim($datos[0]);
				$fRepuve = trim($datos[1]);
				$tid = trim($datos[2]);
				$date = $datos[3];
				$aux = explode('/',$date);			
				$date = substr($aux[2],0,2).'-'.$aux[1].'-'.$aux[0];				
				$hora = $datos[3];
				$aux = explode(':',$hora);
				//var_dump($aux);					
				$hora = substr($aux[0],9,2).':'.$aux[1].':'.substr($aux[2],0,2);	
				$date1 = $date." ".$hora;
				//$date1= date_format($date, 'Y-m-d H:i:s');
        $SqlFolioRepuve= "SELECT folioRepuve ".
				        				 "FROM alrepuvetbl ".
				        				 "WHERE folioRepuve='".$fRepuve."'";
        $rsSqlFolioRepuve= fn_ejecuta_query($SqlFolioRepuve);
	       // echo json_encode($SqlFolioRepuve);

		  if(strlen($vin) == 17) 
		  {   
					if(sizeof($rsSqlFolioRepuve['root']) != 0){
							//$errorMsg = 'El folio'.$fRepuve.' ya Existe!';
		        	$sql= "UPDATE alRepuveTbl ".
		        				" SET tid = '".$tid."', ".
		        				" fechaEvento = '".$date1."', ".
		        				" estatus = 'OK', ".
		        				" vin = '".$vin."' ".
						        "WHERE folioRepuve = '".$fRepuve."'";
			        //echo "$sql<br>";
			        fn_ejecuta_query($sql);				
					} 		
					/*					           		
					else{
					   $ciaSesVal=$_REQUEST['ceDis'];
		
					   $sqlAddRepuve= "INSERT INTO alrepuvetbl (folioRepuve,marca,centroDistribucion,vin,tid,fechaEvento,estatus,observacion) ".
			                        "VALUES ('".$fRepuve."','CD','".$ciaSesVal."','".$vin."','".$tid."','".$date1."','OK',NULL)";
		
						//echo json_encode($sqlAddRepuve);		
					   $rsSqlAddRepuve= fn_ejecuta_query($sqlAddRepuve);				
					   $sqlUpdRepuve= "UPDATE alunidadesTbl set folioRepuve='".$fRepuve."' ".
					   				  "WHERE vin ='".$vin."' ";
		
						//echo json_encode($sqlAddRepuve);		
					  $rsSqlUpdRepuve= fn_ejecuta_query($sqlUpdRepuve);
		
					     if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
							$errorMsg = 'Agregado Correctamente';
						 } else {
							$errorMsg = 'Registro No Agregado error en BD';
						       }
						}
					*/
					$root[]= array('vin'=>$datos[0],'folioRepuve'=>$datos[1], 'tid'=>$datos[2],'fechaVerificacion'=>$datos[3],'nose'=>$errorMsg);
			}	
	  }	
	  //*/	
		fclose($lineas);
		copy($lineas1, $rutaRespal.'verificacion'.str_replace(':', '-', str_replace(' ', '_', date("d-m-Y H:i:s"))).'.txt');
		unlink($lineas1);		
	}
		
	return $root;		
}
?>