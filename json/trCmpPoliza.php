<?php
	session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
	
    switch($_REQUEST['trCmpPolizaActionHdn']) {
        case 'getConceptos':
            getConceptos();
            break;
        default:
            echo '';
    }

    function getConceptos(){
        $sqlGetConcepto = "SELECT cc.concepto, cc.cuentaContable, co.nombre ".
                            "FROM caGeneralesTbl ge, caConceptosCentrosTbl cc, caConceptosTbl co ".
                              "WHERE ge.tabla = 'comprobacionPoliza' ".
                              "AND ge.columna = cc.centroDistribucion ".
                              "AND ge.valor = cc.concepto ".
                              "AND cc.concepto = co.concepto ". 
                              "AND ge.columna = 'CDTOL' ".
                              "ORDER BY ge.estatus ";

        $rsAddTmp = fn_ejecuta_query($sqlGetConcepto);
        echo json_encode($rsAddTmp);                               
    }

?>    