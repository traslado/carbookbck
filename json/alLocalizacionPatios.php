<?php
	session_start();
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
	
    $_REQUEST = trasformUppercase($_REQUEST);

	switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

	switch ($_REQUEST['alLocalizacionPatiosActionHdn']) {
		case 'getLocalizacionPatios':
			getLocalizacionPatios();
			break;
        case 'getFilasPatio':
            getFilasPatio();
            break;
        case 'getLugaresFila':
            getLugaresFila();
            break;
        case 'getGruposPatio':
            getGruposPatio();
            break;
        case 'getGruposPatioCombo':
            getGruposPatioCombo();
            break;
        case 'getDistribuidoresDisponiblesGrupo':
            getDistribuidoresDisponiblesGrupo();
            break;
        case 'getClasificacionDisponiblesGrupo':
            getClasificacionDisponiblesGrupo();
            break;
		case 'addLocalizacionPatios':
			echo json_encode(addLocalizacionPatios($_REQUEST['alLocalizacionPatiosSimboloHdn'],$_REQUEST['alLocalizacionPatiosDistribuidorHdn'],$_REQUEST['alLocalizacionPatiosVinTxt']));
			break;
        case 'updLocalizacionPatios':
            updLocalizacionPatios();
            break;
        case 'dltLocalizacionPatios':
            echo json_encode(dltLocalizacionPatios($_REQUEST['alLocalizacionPatiosVinTxt']));
            break;
        case 'addGrupo':
            addGrupo();
            break;
        case 'updGrupo':
            updGrupo();
            break;
        case 'desbloquearFilaLugar':
            desbloquearFilaLugar();
            break;
        case 'bloquearFilaLugar':
            bloquearFilaLugar();
            break;
		case 'ordenMinimo':
			ordenMinimo($_REQUEST['alLocalizacionPatiosPatioHdn'], $_REQUEST['alLocalizacionPatiosGrupoHdn']);
            break;
        case 'getGrupoPorSimbolo':
            getGrupoPorSimbolo($_REQUEST['alLocalizacionPatiosSimboloHdn']);
            break;
        case 'getGrupoPorDistribuidor':
            getGrupoPorDistribuidor($_REQUEST['alLocalizacionPatiosDistribuidorHdn']);
            break;
        default:
            echo '';
	}

	function getLocalizacionPatios(){
		$lsWhereStr = "WHERE dc.distribuidorCentro = lp.patio ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosIdLocalizacionHdn'], "lp.idLocalizacion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosPatioHdn'], "lp.patio", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosGrupoHdn'], "lp.grupo", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosGrupo1Hdn'], "lp.grupo1", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosGrupo2Hdn'], "lp.grupo2", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosFilaHdn'], "lp.fila", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosLugarHdn'], "lp.lugar", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosVinTxt'], "lp.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosOrdenHdn'], "lp.orden", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosOrden1Hdn'], "lp.orden1", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosOrden2Hdn'], "lp.orden2", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetLocalizacionPatiosStr = "SELECT lp.*, dc.descripcionCentro, ".
                                        "(SELECT ge.nombre FROM caGeneralesTbl ge WHERE ge.tabla = 'alLocalizacionPatiosTbl' ".
                                            "AND ge.columna = 'estatus' AND ge.valor = lp.estatus) AS nombreEstatus ".
                                       "FROM alLocalizacionPatiosTbl lp, caDistribuidoresCentrosTbl dc ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetLocalizacionPatiosStr);
            
        echo json_encode($rs);
	}

    function getFilasPatio(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosPatioHdn'], "lp.patio", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetFilasPatioStr = "SELECT lp.patio, lp.fila, ".
                                "(SELECT MAX(lp2.lugar) FROM alLocalizacionPatiosTbl lp2 ".
                                    "WHERE lp2.patio = lp.patio AND lp2.fila = lp.fila) AS ultimoLugar ".
                               "FROM alLocalizacionPatiosTbl lp ".$lsWhereStr.
                               "GROUP BY lp.fila";
        
        $rs = fn_ejecuta_query($sqlGetFilasPatioStr);
            
        echo json_encode($rs);
    }

    function getLugaresFila(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosPatioHdn'], "lp.patio", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosFilaHdn'], "lp.fila", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetLugaresFilaStr = "SELECT lp.patio, lp.fila, lp.lugar, ".
                                "(SELECT MAX(lp2.lugar) FROM alLocalizacionPatiosTbl lp2 ".
                                    "WHERE lp2.patio = lp.patio AND lp2.fila = lp.fila) AS ultimoLugar ".
                               "FROM alLocalizacionPatiosTbl lp ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetLugaresFilaStr);
            
        echo json_encode($rs);
    }

    function getGruposPatio(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosIdGrupoHdn'], "gp.idGrupo", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosGrupoHdn'], "gp.grupo", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosClasificacionHdn'], "gp.clasificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosMarcaClasificacionHdn'], "gp.marcaClasificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosDistribuidorHdn'], "gp.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosMarcaDistHdn'], "gp.marcaDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosCentroDistHdn'], "gp.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosColorHdn'], "gp.color", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetGruposPatioStr = "SELECT gp.*, ".
                                "(SELECT dc.descripcionCentro FROM caDistribuidoresCentrosTbl dc ".
                                    "WHERE dc.distribuidorCentro = gp.distribuidor) AS descripcionCentro, ".
                                "(SELECT cm.descripcion FROM caClasificacionMarcaTbl cm ".
                                    "WHERE cm.clasificacion = gp.clasificacion ".
                                    "AND cm.marca = gp.marcaClasificacion) AS descClasificacion, ".
                                "(SELECT cs.color FROM caColoresSistemaTbl cs ".
                                    "WHERE cs.claveColor = gp.color ) AS descColor, ".
                                "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu ".
                                    "WHERE mu.marca = gp.marcaClasificacion ) AS descMarca ".
                                "FROM alGruposPatioTbl gp ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetGruposPatioStr);

        for($i = 0; $i < count($rs['root']); $i++){
            if($rs['root'][$i]['marcaClasificacion'] != ""){
                $rs['root'][$i]['descClasificacion'] = $rs['root'][$i]['clasificacion']." - ".$rs['root'][$i]['descClasificacion'];
                $rs['root'][$i]['descMarca'] = $rs['root'][$i]['marcaClasificacion']." - ".$rs['root'][$i]['descMarca'];
            }
        } 
        echo json_encode($rs);
    }

    function getGruposPatioCombo(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosGrupoHdn'], "gp.grupo", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosClasificacionHdn'], "gp.clasificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosMarcaClasificacionHdn'], "gp.marcaClasificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosDistribuidorHdn'], "gp.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosMarcaDistHdn'], "gp.marcaDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosCentroDistHdn'], "gp.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosColorHdn'], "gp.color", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetGruposPatioStr = "SELECT gp.grupo ".
                                "FROM alGruposPatioTbl gp ".$lsWhereStr." GROUP BY gp.grupo";
        
        $rs = fn_ejecuta_query($sqlGetGruposPatioStr);
            
        echo json_encode($rs);
    }

    function getDistribuidoresDisponiblesGrupo(){
        $lsWhereStr = "WHERE (md.distribuidor, md.marca) NOT IN (".
                            "SELECT gp.distribuidor, gp.marcaDistribuidor ".
                            "FROM alGruposPatioTbl gp ".
                            "WHERE gp.distribuidor IS NOT NULL) ".
                      "AND dc.distribuidorCentro = md.distribuidor ".
                      "AND dc.tipoDistribuidor = 'DI'";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosDistribuidorHdn'], "md.distribuidorCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosMarcaHdn'], "md.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }  

        $sqlGetDistDispGrupo = "SELECT md.distribuidor, md.marca AS marcaDistribuidor, dc.descripcionCentro ".
                               "FROM caMarcasDistribuidoresCentrosTbl md, caDistribuidoresCentrosTbl dc ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetDistDispGrupo);
            
        echo json_encode($rs);
    }

    function getClasificacionDisponiblesGrupo(){
        $lsWhereStr = "WHERE (cm.clasificacion, cm.marca) NOT IN (".
                            "SELECT gp.clasificacion, gp.marcaClasificacion ".
                            "FROM alGruposPatioTbl gp ".
                            "WHERE gp.clasificacion IS NOT NULL)";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosClasificacionHdn'], "cm.clasificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alLocalizacionPatiosMarcaHdn'], "cm.marca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        } 

        $sqlGetDistDispGrupo = "SELECT cm.clasificacion, cm.marca AS marcaClasificacion, cm.descripcion AS descClasificacion ".
                               "FROM caClasificacionMarcaTbl cm ".$lsWhereStr;
        
        $rs = fn_ejecuta_query($sqlGetDistDispGrupo);
            
        echo json_encode($rs);
    }

	function addLocalizacionPatios($simbolo, $distribuidor, $vin){
		$a = array();
        $e = array();
        $a['success'] = true;

        $simboloArr = explode('|', substr($simbolo, 0, -1));
        if(in_array('', $simboloArr)){
            $e[] = array('id'=>'alLocalizacionPatiosSimboloHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $distArr = explode('|', substr($distribuidor, 0, -1));
        if(in_array('', $distArr)){
            $e[] = array('id'=>'alLocalizacionPatiosDistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $vinArr = explode('|', substr($vin, 0, -1));
        if(in_array('', $vinArr)){
            $e[] = array('id'=>'alLocalizacionPatiosVinTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if ($a['success'] == true) {
            $vinError = array();
            $success = false;

            for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {

                $grupo = getGrupoPorDistribuidor($distArr[$nInt]);

                if ($grupo == -1) {
                    $grupo = getGrupoPorSimbolo($simboloArr[$nInt]);
                }
                if ($grupo == -1) {
                        $a['success'] = false;  
                        array_push($vinError, $vinArr[$nInt]);
                } else {
                    $grupoCol = 'grupo';
                    $ordenCol = 'orden';

                    $dataArr = ordenMinimo($grupo, $grupoCol, $ordenCol);
                    if ($dataArr == null) {
                        $grupoCol = 'grupo1';
                        $ordenCol = 'orden1';

                        $dataArr = ordenMinimo($grupo, $grupoCol, $ordenCol);

                        if ($dataArr == null) {
                            $grupoCol = 'grupo2';
                            $ordenCol = 'orden2';

                            $dataArr = ordenMinimo($grupo, $grupoCol, $ordenCol);

                            if ($dataArr == null) {
                                $a['success'] = false;
                                array_push($vinError, $vinArr[$nInt]);
                            }
                        }
                    }
                        
                    if($a['success']) {
                        $sqlAddLocalizacionPatios = "UPDATE alLocalizacionPatiosTbl ".
                                                    "SET vin='".$vinArr[$nInt]."' ".
                                                    "WHERE patio = '".$dataArr['patio']."' ".
                                                    "AND ".$grupoCol." = ".$grupo." ".
                                                    "AND ".$ordenCol." = ".$dataArr['orden']." ".
                                                    "AND fila = '".$dataArr['fila']."'";

                        $rs = fn_ejecuta_Upd($sqlAddLocalizacionPatios);

                        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                            $sqlGetUltimoHistoricoStr = "SELECT MAX(idHistorico) FROM alHistoricoUnidadesTbl ".
                                                            "WHERE vin = '".$vinArr[$nInt]."'";

                            $data = fn_ejecuta_query($sqlGetUltimoHistoricoStr);

                            $sqlUpdateHistoricoStr = "UPDATE alHistoricoUnidadesTbl SET ".
                                                     "localizacionUnidad = '".$dataArr['patio']."' ".
                                                     "WHERE idHistorico = ".$data['root'][0]['idHistorico'];

                            $rs = fn_ejecuta_Upd($sqlUpdateHistoricoStr);

                            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                                $a['sql'] = $sqlAddLocalizacionPatios;
                                $success = true;
                            } else {
                                $a['success'] = false;
                                array_push($vinError, $vinArr[$nInt]);
                            }
                            
                        } else {
                            $a['success'] = false;
                            array_push($vinError, $vinArr[$nInt]);
                        }
                    }
                }
            }

            //Concatenate Errors
            if (sizeof($vinError) > 0) {
                if ($success == true) {
                    $a['errorMessage'] =  getLocalizacionSuccessMsg()."<br>".getLocalizacionFailedMsg();
                    foreach ($vinError as $vin) {
                        $a['errorMessage'] .= "<br>".$vin;
                    }
                } else if ($success == false){
                    $a['errorMessage'] = getLocalizacionFailedMsg();
                    foreach ($vinError as $vin) {
                        $a['errorMessage'] .= "<br>".$vin;
                    }
                }   
            } else if($success == true){
                $a['successMessage'] = getLocalizacionSuccessMsg();
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
	}

    function updLocalizacionPatios(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['alLocalizacionPatiosIdLocalizacionHdn'] == ""){
            $e[] = array('id'=>'alLocalizacionPatiosIdLocalizacionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['alLocalizacionPatiosGrupoHdn'] == ""){
            $e[] = array('id'=>'alLocalizacionPatiosGrupoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['alLocalizacionPatiosOrdenHdn'] == ""){
            $e[] = array('id'=>'alLocalizacionPatiosOrdenHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            $sqlUpdLocalizacionPatiosStr = "UPDATE alLocalizacionPatiosTbl SET ".
                                            "grupo = ".$_REQUEST['alLocalizacionPatiosGrupoHdn'].",".
                                            "grupo1 = ".replaceEmptyDec($_REQUEST['alLocalizacionPatiosGrupo1Hdn']).",".
                                            "grupo2 = ".replaceEmptyDec($_REQUEST['alLocalizacionPatiosGrupo2Hdn']).",".
                                            "orden = ".$_REQUEST['alLocalizacionPatiosOrdenHdn'].",".
                                            "orden1 = ".replaceEmptyDec($_REQUEST['alLocalizacionPatiosOrden1Hdn']).",".
                                            "orden2 = ".replaceEmptyDec($_REQUEST['alLocalizacionPatiosOrden2Hdn']).", ".
                                            "estatus = ".replaceEmptyNull("'".$_REQUEST['alLocalizacionPatiosEstatusHdn']."'")." ".
                                            "WHERE idLocalizacion = ".$_REQUEST['alLocalizacionPatiosIdLocalizacionHdn'];

            $rs = fn_ejecuta_Upd($sqlUpdLocalizacionPatiosStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] = getLocalizacionPatiosUpdMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlUpdLocalizacionPatiosStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function dltLocalizacionPatios($vin, $patio = ''){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == ""){
            $e[] = array('id'=>'VinTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            $sqlDltLocalizacionPatiosStr = "UPDATE alLocalizacionPatiosTbl ".
                                           "SET vin = NULL, estatus='DI' WHERE vin= '".$vin."' ";

            /*if($patio != ''){
                $sqlDltLocalizacionPatiosStr .= "AND patio = '".$patio."'";
            }*/

            $rs = fn_ejecuta_Upd($sqlDltLocalizacionPatiosStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlDltLocalizacionPatiosStr;
                $a['successMessage'] = getLocalizacionPatiosDeleteMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltLocalizacionPatiosStr;
            }   
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function addGrupo(){
        $a['success'] = true;
        $e = array();

        if($_REQUEST['catGruposCtoDistribucionCmb'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'catGruposCtoDistribucionCmb', 'msg' => getRequerido());
        }
        if($_REQUEST['catGruposGrupoTxt'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'catGruposGrupoTxt', 'msg' => getRequerido());
        }

        if($a['success'] == true){
            $sqlAddGrupo = "INSERT INTO alGruposPatioTbl(centroDistribucion, grupo, clasificacion, ".
                           "marcaClasificacion, distribuidor, color) ".
                           "VALUES('".$_REQUEST['catGruposCtoDistribucionCmb']."', ".
                           $_REQUEST['catGruposGrupoTxt'].", ".
                           replaceEmptyNull("'".$_REQUEST['catGruposClasificacionClasificacionCmb']."'").", ".
                           replaceEmptyNull("'".$_REQUEST['catGruposClasificacionMarcaCmb']."'").", ".
                           replaceEmptyNull("'".$_REQUEST['catGruposDistribuidorDistribuidorCmb']."'").", ".
                           replaceEmptyNull("'".$_REQUEST['catGruposColorCmb']."'").");";

            $rs = fn_ejecuta_Add($sqlAddGrupo);
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['success'] = true;
                $a['sql'] = $sqlAddGrupo;
                $a['successMessage'] = getGrupoSuccessMsg();
            } else{
                $a['success'] = false;
                $a['sql'] = $sqlAddGrupo;
                $a['errorMessage'] = $_SESSION['error_sql']." - ".$a['sql'];
                
            }
        }
        $a['errors'] = $e;
        echo json_encode($a);
    }

    function updGrupo(){
        $a['success'] = true;
        $e = array();

        if($_REQUEST['catGruposCtoDistribucionCmb'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'catGruposCtoDistribucionCmb', 'msg' => getRequerido());
        }
        if($_REQUEST['catGruposGrupoTxt'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'catGruposGrupoTxt', 'msg' => getRequerido());
        }
        if($_REQUEST['catGruposIdGrupoHdn'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'catGruposIdGrupoHdn', 'msg' => getRequerido());
        }

        if($a['success'] == true){
            $sqlAddGrupo = "UPDATE alGruposPatioTbl SET clasificacion = ".
                           replaceEmptyNull("'".$_REQUEST['catGruposClasificacionClasificacionCmb']."'").", ".
                           "marcaClasificacion = ".
                           replaceEmptyNull("'".$_REQUEST['catGruposClasificacionMarcaCmb']."'").", ".
                           "distribuidor = ".
                           replaceEmptyNull("'".$_REQUEST['catGruposDistribuidorDistribuidorCmb']."'").", ".
                           "color = ".
                           replaceEmptyNull("'".$_REQUEST['catGruposColorCmb']."'").
                           " WHERE idGrupo = ".$_REQUEST['catGruposIdGrupoHdn'].
                           " AND centroDistribucion = '".$_REQUEST['catGruposCtoDistribucionCmb'].
                           "' AND grupo = '".$_REQUEST['catGruposGrupoTxt']."';";
                          
            $rs = fn_ejecuta_Upd($sqlAddGrupo);
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['success'] = true;
                $a['sql'] = $sqlAddGrupo;
                $a['successMessage'] = getGrupoUpdateMsg();
            } else{
                $a['success'] = false;
                $a['sql'] = $sqlAddGrupo;
                $a['errorMessage'] = $_SESSION['error_sql']." - ".$a['sql'];
            }
        }
        $a['errors'] = $e;
        echo json_encode($a);
    }

    function desbloquearFilaLugar(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['alLocalizacionPatiosPatioHdn'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'alLocalizacionPatiosPatioHdn', 'msg' => getRequerido());
        }
        if($_REQUEST['alLocalizacionPatiosFilaHdn'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'alLocalizacionPatiosFilaHdn', 'msg' => getRequerido());
        }

        if($a['success']){
            $sqlDesbloquearFilaStr = "UPDATE alLocalizacionPatiosTbl SET estatus = NULL ".
                                    "WHERE patio = '".$_REQUEST['alLocalizacionPatiosPatioHdn']."' ".
                                    "AND fila = '".$_REQUEST['alLocalizacionPatiosFilaHdn']."' ";

            if($_REQUEST['alLocalizacionPatiosLugarHdn'] != ''){
                $sqlDesbloquearFilaStr .= "AND lugar = ".$_REQUEST['alLocalizacionPatiosLugarHdn'];
            }

            $rs = fn_ejecuta_Upd($sqlDesbloquearFilaStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] = getLocalizacionDesbloqueadaStr();
            } else{
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDesbloquearFilaStr;
                
            }
        }
        $a['errors'] = $e;
        echo json_encode($a);
    }

    function bloquearFilaLugar(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['alLocalizacionPatiosPatioHdn'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'alLocalizacionPatiosPatioHdn', 'msg' => getRequerido());
        }
        if($_REQUEST['alLocalizacionPatiosFilaHdn'] == ''){
            $a['success'] = false;
            $e[] = array('id' => 'alLocalizacionPatiosFilaHdn', 'msg' => getRequerido());
        }

        if($a['success']){
            $sqlBloquearFilaStr = "UPDATE alLocalizacionPatiosTbl SET estatus = 'BA' ".
                                    "WHERE patio = '".$_REQUEST['alLocalizacionPatiosPatioHdn']."' ".
                                    "AND fila = '".$_REQUEST['alLocalizacionPatiosFilaHdn']."' ";

            if($_REQUEST['alLocalizacionPatiosLugarHdn'] != ''){
                $sqlBloquearFilaStr .= "AND lugar = ".$_REQUEST['alLocalizacionPatiosLugarHdn'];
            }

            $rs = fn_ejecuta_Upd($sqlBloquearFilaStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] = getLocalizacionBloqueadaStr();
            } else{
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlBloquearFilaStr;
                
            }
        }
        $a['errors'] = $e;
        echo json_encode($a);
    }

	function ordenMinimo($grupo, $grupoCol, $ordenCol){
		$sqlGetPosiblesLugaresStr = "SELECT patio, ".$grupoCol.", vin, ".$ordenCol.", fila, lugar ".
									"FROM alLocalizacionPatiosTbl ".
									"WHERE ".$grupoCol." =".$grupo." ".
                                    "AND ".$ordenCol." != 0 ".
                                    "AND (estatus != 'B' || estatus IS NULL) ".
                                    "AND patio IN (".
                                        "SELECT dc.distribuidorCentro FROM caDistribuidoresCentrosTbl dc ".
                                            "WHERE dc.tipoDistribuidor = 'PA' ".
                                            "AND dc.sucursalDe = '".$_SESSION['usuCompania']."') ".
                                    "ORDER BY ".$ordenCol;

		$rs = fn_ejecuta_query($sqlGetPosiblesLugaresStr);

		if (sizeof($rs['root']) > 0) {
            for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) { 
                if ($rs['root'][$nInt]['vin'] == '') {
                    return array('patio'=>$rs['root'][$nInt]['patio'], 'grupo'=>$rs['root'][$nInt][$grupoCol], 'orden'=>$rs['root'][$nInt][$ordenCol],'fila'=>$rs['root'][$nInt]['fila'], 'lugar'=>$rs['root'][$nInt]['lugar']);
                }
            }
		}

		return null;
	}

    function getGrupoPorSimbolo($simbolo){
        $sqlGetGrupoPorSimboloStr = "SELECT gp.grupo ".
                                    "FROM caSimbolosUnidadesTbl su, alGruposPatioTbl gp ".
                                    "WHERE gp.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                    "AND gp.clasificacion = su.clasificacion ".
                                    "AND su.simboloUnidad = '".$simbolo."'";

        $rs = fn_ejecuta_query($sqlGetGrupoPorSimboloStr);
        
        if (sizeof($rs['root']) > 0) {
            return $rs['root'][0]['grupo'];
        }

        return -1;
    }

    function getGrupoPorDistribuidor($distribuidor){
        $sqlGetGrupoPorDistribuidorStr = "SELECT gp.grupo ".
                                         "FROM alGruposPatioTbl gp ".
                                         "WHERE gp.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                         "AND gp.distribuidor ='".$distribuidor."' ";

        $rs = fn_ejecuta_query($sqlGetGrupoPorDistribuidorStr);

        if (sizeof($rs['root']) > 0) {
            return $rs['root'][0]['grupo'];
        }

        return -1;
    }
?>      