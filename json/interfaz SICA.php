<?php
	ini_set('max_execution_time', 3000);
	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/generalesSICA.php");
    //require_once("../funciones/construct.php");
    //require_once("../funciones/utilidades.php");


    function generaInterfaseSICA($patio,$dia,$tipo,$numero,$tipoPoliza){
    	//echo "entra SICA  ".$tipo;
		$a = array('success' => true, 'msjResponse' => 'Interfase generada correctamente.', 'nombreArchivo'=>'', 'existeTmp'=>0);
		
		//$dir 	= "E:/carbook/i504/respArchivo/";
		//$arch = $_REQUEST['nombreArchivo'].'.nvo';
		
		$sql = "SELECT cuentaContable, tipoCuenta FROM caConceptosCentrosTbl".
					 " WHERE centroDistribucion = '".$patio."'".
					 " AND concepto in ('7001','9032') ";
		//echo "$sql<br>"; 
		$rsCtas = fn_ejecuta_query($sql);

		//var_dump($rsCtas);		
		
		$ind = 0;
		$totabo = 0;
		
		$sql1 = "SELECT * FROM trGastosViajeTractorTbl".
					 " WHERE centroDistribucion = '".$patio."'".
					 " AND  concepto in ('7001','9032') ".
					 " AND SUBSTRING(fechaEvento,1,10) = '".$dia."'".
					 " AND transmitido  IN ('GT')";    			# GX=Cancelado GT=Transmitido"
		//echo "$sql<br>";		
		$rsGastos = fn_ejecuta_query($sql1);


		//var_dump($rsGastos);		
		
		$idx = 0;
		$a['nombreArchivo'] = $dir.$arch;
		$fp = fopen($a['nombreArchivo'], 'w');
		foreach($rsGastos['root'] AS $index => $row)
		{
				$aux = explode('-',substr($row['fechaEvento'],0,10));
				$anio = $aux[0];				
				$mes = $aux[1];
				$dia = $aux[2];
				$fecha = $anio.'-'.$mes.'-'.$dia;
				
				$ctacon = rtrim($rsCtas['root'][0]['cuentaContable']);
				$cvecia = 4;
				$anio 	= substr($row['fechaEvento'],0,4);
				$mes 	= substr($row['fechaEvento'],5,2);
				$fecpol	= $fecha;

				$sql = "SELECT ch.cuentaContable FROM trViajesTractoresTbl vt, caChoferesTbl ch ".
						" WHERE vt.idViajeTractor = ".$row['idViajeTractor'].
						//" AND vt.centroDistribucion = '".$patio."'".
						" AND ch.claveChofer = vt.claveChofer";
				//echo "$sql<br>";
				$rsCtaChof = fn_ejecuta_query($sql);
				//var_dump($rsCtaChof);
				
				$ctacon = armaCuentaContable(rtrim($rsCtas['root'][0]['cuentaContable']), rtrim($rsCtaChof['root'][0]['cuentaContable']), '0');
				//var_dump($ctacon);
				$numpol = str_pad('1', 6, "0", STR_PAD_LEFT);
				$tippol = $tipo;
				$numche = 0;
				$cargo = '0.00';
				$abono = $row['importe'];
				if(rtrim($rsCtas['root'][0]['tipoCuenta']) == 'C')
				{
					$cargo = $row['importe'];
					$abono = '0.00';
				}
				$desmov = 'FOLIO DE RECIBO '.str_pad($row['folio'], 6, "0", STR_PAD_LEFT);
				$tippro = 0;
				$ind++;
				$orden = $ind;

				$selTipoPoliza="SELECT * FROM conTiposPolizasTbl WHERE claveCompania=4  AND tipoPoliza=".$tipo."";
				$rsTipoPoliza=fn_ejecuta_query_SICA($selTipoPoliza);

				// echo json_encode($selTipoPoliza);

				$sqlFolio="select * from conFoliosPolizasTbl ".
							"where anio='".$anio."' ".
							"AND claveCompania='4' ".
							"and mes='".$mes."' ".
							"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
				$rsFolio=fn_ejecuta_query_SICA($sqlFolio);


				if (sizeof($rsFolio['root'][0]['folio'])=='0') {
					$insFolio="INSERT INTO conFoliosPolizasTbl (claveCompania, anio, mes, idTipoPoliza, folio)  ".
							"VALUES ('4', '".$anio."', '".$mes."', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', '1')";
					fn_ejecuta_query_SICA($insFolio);
					/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
				      {
				         $a['success']		 = false;
				         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
				         $a['sql'] = $sql;
				      }_*/

					$folio='1';

					// echo json_encode($insFolio);

				}else{
					$folio=$rsFolio['root'][0]['folio'];

					/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
				      {
				         $a['success']		 = false;
				         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la folio.";
				         $a['sql'] = $sql;
				      }*/
					// echo json_encode($updFolio);
				}

				/*$insRegistro="INSERT INTO conPolizasTbl (claveCompania, idTipoPoliza, fecha, anio, mes, numero, nombre, divisa, paridad, cargosBase, abonosBase, cargos, abonos, estatus, usuario, fechaCaptura) ".
							"VALUES ('4', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', 
							'".$fecpol."', 
							'".$anio."', 
							'".$mes."', 
							'".$folio."', 
							'".$desmov."', 'MXN', '1.0000', '".$cargo."', 
							'".$abono."', '".$cargo."', '".$abono."', '01', '1', now())";
				fn_ejecuta_query_SICA($insRegistro);*/

				$sqlCuenta="SELECT * FROM conCatCuentasTbl where cuenta = '".$ctacon."';";
				$rsCuenta=fn_ejecuta_query_SICA($sqlCuenta);
				if (sizeof($rsCuenta['root']) == 0) {
					$existeCuenta = generaCuenta($ctacon);
					if ($existeCuenta['success']) {
						$rsCuenta['root'][0]['idCuentaContable'] = $existeCuenta['idCuentaContable'];
					}
					// $a['success'] = $existeCuenta['success'];
				}
				$idx++;
				$insDetalle="INSERT INTO conPolizasDetalleTbl (claveCompania, idTipoPoliza, fecha, anio, mes, numero, sec, idCuentaContable, referencia, descripcion, cargosBase, abonosBase, cargos, abonos, idTipoComprobante) ".
							"VALUES ('4', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', ".
							"'".$fecpol."', ".
							"'".$anio."', ".
							"'".$mes."', ".
							"'".$folio."', ".
							"'".$idx."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '".$desmov."', ".
							"'".$cargo."', ".
							"'".$abono."', ".
							"'".$cargo."', ".
							"'".$abono."', '0')";
				fn_ejecuta_query_SICA($insDetalle);



				//echo json_encode($insRegistro);


				//echo "numero folio".$folio;

				/*fputs($fp, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
				if($idx != $rsGastos['records'])
				{
					fputs($fp,"\n");
				}
				*/
				//fputs($fp,"\n");
				
				$totabo = $totabo + $row['importe'];				
		}

		/*$updFolio="UPDATE conFoliosPolizasTbl SET folio=".$folio ." where anio='".$anio."' ".
							"AND claveCompania='4' ".
							"and mes='".$mes."'";
		fn_ejecuta_query_SICA($updFolio);*/
				
		#### Inserta cuenta de Abono (BANCOS) ####		
		$sql = "SELECT cuentaContable, tipoCuenta FROM caConceptosCentrosTbl".
					 " WHERE centroDistribucion = '".$patio."'".
					 " AND concepto = '".$numero."'";
		//echo "$sql<br>";
		$rsCtas = fn_ejecuta_query($sql);
		//var_dump($rsCtas);
		
		$ind++;
		$orden = $ind;
		$ctacon = rtrim($rsCtas['root'][0]['cuentaContable']);
		$cargo = $totabo;
		$abono = $totabo;
		if(rtrim($rsCtas['root'][0]['tipoCuenta']) == 'C')
		{
			$cargo = $totabo;
			$abono = '0.00';
		}
		$desmov = 'ANTICIPOS DE VIAJE';
		/*fputs($fp, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');
		
		fclose($fp);*/

		//////////////TOAL DE ANTICIPOS//////
		
		$selTipoPoliza="SELECT * FROM conTiposPolizasTbl WHERE claveCompania=4  AND tipoPoliza=".$tipo."";
		$rsTipoPoliza=fn_ejecuta_query_SICA($selTipoPoliza);

		// echo json_encode($selTipoPoliza);

		$sqlFolio="select * from conFoliosPolizasTbl ".
					"where anio='".$anio."' ".
					"AND claveCompania='4' ".
					"and mes='".$mes."' ".
					"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
		$rsFolio=fn_ejecuta_query_SICA($sqlFolio);


		if (sizeof($rsFolio['root'])==0 || $rsFolio['root'][0]['folio'] == 0) {
			$insFolio="INSERT INTO conFoliosPolizasTbl (claveCompania, anio, mes, idTipoPoliza, folio)  ".
					"VALUES ('4', '".$anio."', '".$mes."', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', '1')";
			fn_ejecuta_query_SICA($insFolio);
			/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
			{
				$a['success']		= false;
				$a['msjResponse']	= $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
				$a['sql'] = $sql;
			}_*/

			$folio='1';

			// echo json_encode($insFolio);

		}else{
			$folio=$rsFolio['root'][0]['folio'];
			$folioupd=floatval($rsFolio['root'][0]['folio'])+1;

			$updFolio="UPDATE conFoliosPolizasTbl SET folio=".$folioupd ." where anio='".$anio."' ".
					"AND claveCompania='4' ".
					"AND idTipoPoliza='".$rsTipoPoliza['root'][0]['idTipoPoliza']."' ".
					"and mes='".$mes."'";
			fn_ejecuta_query_SICA($updFolio);
			/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
			{
				$a['success']		= false;
				$a['msjResponse']	= $_SESSION['error_sql']. " En la inserci&oacuten de la folio.";
				$a['sql'] = $sql;
			}*/
			// echo json_encode($updFolio);
		}

		$insRegistro="INSERT INTO conPolizasTbl (claveCompania, idTipoPoliza, fecha, anio, mes, numero, nombre, divisa, paridad, cargosBase, abonosBase, cargos, abonos, estatus, usuario, fechaCaptura) ".
					"VALUES ('4', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', 
					'".$fecpol."', 
					'".$anio."', 
					'".$mes."', 
					'".$folio."', 
					'".$desmov."', 'MXN', '1.0000', '".$cargo."', 
					'".$abono."', '".$cargo."', '".$abono."', '01', '1', now())";
			fn_ejecuta_query_SICA($insRegistro);

		$sqlCuenta="SELECT * FROM conCatCuentasTbl where cuenta = '".$ctacon."';";
		$rsCuenta=fn_ejecuta_query_SICA($sqlCuenta);
		if (sizeof($rsCuenta['root']) == 0) {
			$existeCuenta = generaCuenta($ctacon);
			if ($existeCuenta['success']) {
				$rsCuenta['root'][0]['idCuentaContable'] = $existeCuenta['idCuentaContable'];
			}
			// $a['success'] = $existeCuenta['success'];
		}

		$cargo='0.0';
		$idx++;
		$insDetalle="INSERT INTO conPolizasDetalleTbl (claveCompania, idTipoPoliza, fecha, anio, mes, numero, sec, idCuentaContable, referencia, descripcion, cargosBase, abonosBase, cargos, abonos, idTipoComprobante) ".
					"VALUES ('4', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', ".
					"'".$fecpol."', ".
					"'".$anio."', ".
					"'".$mes."', ".
					"'".$folio."', ".
					"'".$idx."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '".$desmov."', ".
					"'".$cargo."', ".
					"'".$abono."', ".
					"'".$cargo."', ".
					"'".$abono."', '0')";
		fn_ejecuta_query_SICA($insDetalle);
	}

	function generaCuenta($pscuenta) {
		$a = array('success' => false);

		$cuentaArr = explode('.', $pscuenta);

		if (sizeof($cuentaArr) > 0) {
			$claveChofer = end($cuentaArr);

			if (strlen($claveChofer) == 5) {
				$cuentaPrev = substr($pscuenta, 0,-6);
				$selCtaSicaPrev = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${cuentaPrev}';";
				$rsCuentaPrev = fn_ejecuta_query_SICA($selCtaSicaPrev);

				if (sizeof($rsCuentaPrev['root']) > 0) {
					$selCuentaSica = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${pscuenta}';";
					$rsCuenta = fn_ejecuta_query_SICA($selCuentaSica);

					if (sizeof($rsCuenta['root']) == 0) {
						$selNombre = "SELECT concat(apellidoMaterno,' ', apellidoPaterno,' ', nombre) as nombre FROM caChoferesTbl where claveChofer = ".$claveChofer;
						$rsNombre = fn_ejecuta_query($selNombre);

						if (sizeof($rsNombre['root']) > 0) {
							$insertCuenta = " INSERT INTO conCatCuentasTbl (claveCompania, cuenta, nombre, afectable, tipo, naturaleza, ubicacion, reportar, manejaSegmentos, numeroSegmentos, divisa, codigoAgrupador, RFC, estatus, idUsuarioAct, fechaAct, ipAct, macAddressAct)".
											" VALUES ('4', '${pscuenta}', ".
											"'".$rsNombre['root'][0]['nombre']."', ".
											"'1', '1', '1', ' ', '1', '0', '0', 'MXN', ' ', ' ', '1', '1', now(), '10.212.134.204', '10.212.134.204')";
							$rsCta = fn_ejecuta_query_SICA($insertCuenta);

							$selCuentaSica = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${pscuenta}';";
							$rsCuenta = fn_ejecuta_query_SICA($selCuentaSica);

							for ($j=1; $j <= 12; $j++) { 
								$rsConSaldos="INSERT INTO conSaldosTbl (claveCompania, tipo, calendario, periodo, idCuentaContable, idCentroCostos, saldoInicial, cargos, abonos, saldoFinal) ".
											"VALUES ('4', '1', year(now()), '".$j."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '0.00', '0.00', '0.00', '0.00')";
								$rsSal = fn_ejecuta_query_SICA($rsConSaldos);
							}
							$a['success'] = true;
							$a['idCuentaContable'] = $rsCuenta['root'][0]['idCuentaContable'];
						}
					}
				}
			}
		}

		return $a;
	}
?>