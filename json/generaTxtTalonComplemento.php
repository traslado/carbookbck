<?php

  session_start();
    ///setlocale(LC_TIME, 'es_MX.utf8');
    $codigoTalonEspecial = 'TE';

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    

  /*header("Content-type: application/pdf");
  header("Content-Disposition: inline; filename=documento.pdf");*/

    if(isset($_REQUEST['actionHdn']) && $_REQUEST['actionHdn'] == "buscaFolio"){
        buscaFolio();
    }
    else
    {
        if($_REQUEST['esTractor12Hdn'] != ""){
            imprimirTalon12($_REQUEST['trViajesTractoresIdViajeHdn']);
        } else{
            imprimirTalon();
        }       
    }

    function buscaFolio(){
        $sql = "SELECT tv.*, tr.tractor, vt.viaje, tv.numeroUnidades, tr.rendimiento, vt.numeroRepartos AS distribuidores, vt.kilometrosTabulados, ".
               "concat(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as operador, cast(tv.fechaEvento as date) as fechaEvento,".
               "(SELECT count(tl1.distribuidor) FROM trtalonesviajestbl tl1 WHERE tl1.idViajeTractor = vt.idViajeTractor) as talones, ".
               "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS plazaOrigen, ".
               "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaDestino) AS plazaDestino ".
               "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch ".
               "WHERE tv.idViajeTractor = vt.idViajeTractor ".
               "AND vt.idTractor = tr.idTractor ".
               "AND vt.claveChofer = ch.claveChofer ".
               "AND tr.compania = '".$_REQUEST['compania']."' ".
               "AND tv.folio = ".$_REQUEST['folio']." ".
               "AND tv.centroDistribucion in ('CDLZC','CDSAH','CDQRO','CDMTY','CDSAL','CDSFE','CDTOL','CDMAZ','CDALT','CDTUX','CDVER','CDAGS','CDSLP','CMDAT','CDSLO','CDLAR','CDKAN','CDRMS')".
               "AND tv.tipotalon = '".$_REQUEST['tipoTalon']."'";
         //echo "$sql<br>";
         $rsTalon = fn_ejecuta_query($sql);
         
         echo json_encode($rsTalon);
    }


    function imprimirTalon(){
        $a = array();
        $e = array();
        $a['success'] = true;
        $a['msjResponse'] = 'Reimpresi&oacuten generada correctamente.';
        global $codigoTalonEspecial;

        if($_REQUEST['trViajesTractoresIdViajeHdn'] != ""){

            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND tv.total!='0' ". 
                                     "AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                     "AND tv.claveMovimiento != 'TX' ".
                                     "AND tv.centroDistribucion in ('CDLZC','CDSAH','CDQRO','CDMTY','CDSAL','CDSFE','CDTOL','CDMAZ','CDALT','CDTUX','CDVER','CDAGS','CDSLP','CMDAT','CDSLO','CDLAR','CDKAN','CDRMS')".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].")";
        } else {
            if ($_REQUEST['trViajesTractoresFoliosHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresFoliosHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if ($_REQUEST['trViajesTractoresCompaniaHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresCompaniaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            $tipoTalon = '';
            if(isset($_REQUEST['tipoTalon']))
            {
                $tipoTalon = " AND tv.tipotalon = '".$_REQUEST['tipoTalon']."'";
            }

            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".       
                                     "AND tv.total!='0' ".                               
                                     "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].") ".
                                     "AND tv.centroDistribucion in ('CDLZC','CDSAH','CDQRO','CDMTY','CDSAL','CDSFE','CDTOL','CDMAZ','CDALT','CDTUX','CDVER','CDAGS','CDSLP','CMDAT','CDSLO','CDLAR','CDKAN','CDRMS')".
                                     $tipoTalon;

        }

        if ($a['success']) {

         // $pdf = new FPDF('L', 'mm','A4');


          //echo "$sqlGetTalonesViajeStr<br>";
          $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);
          //echo json_encode($sqlGetTalonesViajeStr);
          $varInc = 0;

          //echo json_encode($rsTalon);

          for ($nInt=1 ; $nInt <= sizeof($rsTalon['root']) ; $nInt++) {


            $sqlGetTalonStr = "SELECT tv.*, dc.descripcionCentro, tr.tractor, ".//co.descripcion AS nombreCiaRemitente, co.rfc AS rfcRemitente, co.direccion AS dirCompania, 
                              "vt.claveChofer,vt.kilometrosTabulados, ch.*, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, dc.contacto as contactoDist,dc.codigoCCP, dc.telefono as telefonoDist,".
                              "(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$varInc]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                              "(SELECT pl.plaza FROM caPlazasTbl pl ".
                              "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                              "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                              "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                              "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                              "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                              "(SELECT co2.compania FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor, ".
                              "  (SELECT 
                                    un.distribuidorOrigen
                                FROM
                                    trUnidadesDetallesTalonesTbl ut,
                                    aldestinosespecialestbl un,
                                    caSimbolosUnidadesTbl su
                                WHERE
                                    un.vin = ut.vin                
                                        AND ut.idTalon = ".$rsTalon['root'][$varInc]['idTalon']."
                                        AND ut.estatus != 'C' limit 1) as destinoEsp ".
                              "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caChoferesTbl ch, ".
                              "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                              "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".// caCompaniasTbl co,
                              "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                              "AND dc.distribuidorCentro = tv.distribuidor ".
                              // "AND co.compania = tv.companiaRemitente ".
                              "AND tr.idTractor = vt.idTractor ".
                              "AND ch.claveChofer = vt.claveChofer ".
                              "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                              "WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                              "AND col.idColonia = di.idColonia ".
                              "AND mu.idMunicipio = col.idMunicipio ".
                              "AND es.idEstado = mu.idEstado ".
                              "AND pa.idPais = es.idPais ".
                              "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                              "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                               "AND tv.centroDistribucion in ('CDLZC','CDSAH','CDQRO','CDMTY','CDSAL','CDSFE','CDTOL','CDMAZ','CDALT','CDTUX','CDVER','CDAGS','CDSLP','CMDAT','CDSLO','CDLAR','CDKAN','CDRMS')".
                              "AND tv.idTalon = ".$rsTalon['root'][$varInc]['idTalon']." ".
                              "AND tv.claveMovimiento != 'TX' ";

            $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

            //echo json_encode($sqlGetTalonStr);

            if(sizeof($dataTalon['root']) > 0){

              if ($dataTalon['root'][0]['destinoEsp']!=null) {

                
                /*$selStr = "SELECT * FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$dataTalon['root'][0]['destinoEsp']."'";
                $ciaRst = fn_ejecuta_query($selStr);
                $dataTalon['root'][0]['nombreCiaRemitente'] = $ciaRst['root'][0]['descripcionCentro'];
                $dataTalon['root'][0]['rfcRemitente'] = $ciaRst['root'][0]['rfc'];
                $dataTalon['root'][0]['dirCompania'] = $ciaRst['root'][0]['direccionEntrega'];*/
                $dataTalon['root'][0]['companiaRemitente']=$dataTalon['root'][0]['destinoEsp'];
              }



              $selStr = "SELECT * FROM caCompaniasTbl WHERE compania = '".$dataTalon['root'][0]['companiaRemitente']."'";
              $ciaRst = fn_ejecuta_query($selStr);
              if ($ciaRst['records'] > 0) {
                $dataTalon['root'][0]['nombreCiaRemitente'] = $ciaRst['root'][0]['descripcion'];
                $dataTalon['root'][0]['rfcRemitente'] = $ciaRst['root'][0]['rfc'];
                $dataTalon['root'][0]['dirCompania'] = $ciaRst['root'][0]['direccion'];
              } else {
                $selStr = "SELECT * FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$dataTalon['root'][0]['companiaRemitente']."'";
                $ciaRst = fn_ejecuta_query($selStr);
                $dataTalon['root'][0]['nombreCiaRemitente'] = $ciaRst['root'][0]['descripcionCentro'];
                $dataTalon['root'][0]['rfcRemitente'] = $ciaRst['root'][0]['rfc'];
                $dataTalon['root'][0]['dirCompania'] = $ciaRst['root'][0]['direccionEntrega'];
              }

              if($dataTalon['root'][0]['dirCompania'] != ''){

                $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                              "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                              "caEstadosTbl es, caPaisesTbl pa ".
                                              "WHERE pa.idPais = es.idPais ".
                                              "AND es.idEstado = mu.idEstado ".
                                              "AND mu.idMunicipio = co.idMunicipio ".
                                              "AND co.idColonia = dir.idColonia ".
                                              "AND dir.direccion = '".$dataTalon['root'][0]['dirCompania']."'";

                $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio']." ".$dirCompania['root'][0]['cp'];
                $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
              }

              

              
              $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ".
                                        "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                        "WHERE un.vin = ut.vin ".
                                        "AND su.simboloUnidad = un.simboloUnidad ".
                                        "AND ut.idTalon =".$rsTalon['root'][$varInc]['idTalon']." ".
                                        "AND ut.estatus != 'C' ";

              $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);


              if(sizeof($unidadesTalon['root']) > 0){

                $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                $dataTalon['root'][0]['apellidoPaterno']." ".
                $dataTalon['root'][0]['apellidoMaterno'];
                $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];

              if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                $sqlGetServEspStr = "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                    "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                    "WHERE dir.direccion = de.direccionDestino ".
                                    "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                    "AND dir.idColonia = co.idColonia ".
                                    "AND co.idMunicipio = mu.idMunicipio ".
                                    "AND mu.idEstado = es.idEstado ".
                                    "ANd es.idPais = pa.idPais ".
                                    "AND pl.idPlaza = dc.idPlaza ".
                                    "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                    "AND de.idDestinoEspecial =(SELECT MAX(idDestinoEspecial) FROM aldestinosespecialestbl where vin ='".$unidadesTalon['root'][0]['vin']."') ";

                $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);

                for ($nInt01=0; $nInt01 < sizeof($dataServEsp['root']); $nInt01++) {

                  $dataServEsp['root'][$nInt01]['descDist'] = $dataServEsp['root'][$nInt01]['distribuidor']." - ".
                  $dataServEsp['root'][$nInt01]['descripcionCentro'];

                  $dataServEsp['root'][$nInt01]['dirL1'] = $dataServEsp['root'][$nInt01]['calleNumero'];
                  $dataServEsp['root'][$nInt01]['dirL2'] = $dataServEsp['root'][$nInt01]['colonia'].", ".$dataServEsp['root'][$nInt01]['municipio']." ".$dataServEsp['root'][$nInt01]['cp'];
                  $dataServEsp['root'][$nInt01]['dirL3'] = $dataServEsp['root'][$nInt01]['estado'].", ".$dataServEsp['root'][$nInt01]['pais'];
                }
              }

              if ($dataTalon['root'][0]['companiaTractor']=='TR') {
                  $companiaTr='TRANSDRIZA';
              }
              else if ($dataTalon['root'][0]['companiaTractor']=='CR') {
                  $companiaTr='CHRYMEX';
              }



                  $nombre_fichero = "C:/cartaPorte/CONECTOR/".substr($dataTalon['root'][0]['centroDistribucion'],2,3).$dataTalon['root'][0]['folio'].$companiaTr.".txt";


                  if (file_exists($nombre_fichero)) {
                      echo "Archivo Generado";
                  } else {
                      generaArchivo($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);
                  }
                


            } else {
              $pdf->AddPage();
              $pdf->SetY(92+(4*$nInt)+0);
              $pdf->SetX(25+0);
              $pdf->Cell(17,3, 'TALON #'.$rsTalon['root'][$varInc]['idTalon']. ' SIN UNIDADES',0, 'C');
            }
          }
            $varInc ++ ;
        }
      }
        //Output
        //$pdf->Output('../../talon.pdf', I);
    }
    function imprimirTalon12($idViaje){
        $a = array();
        $e = array();
        $a['success'] = true;

        //SOLO PARA EL CASO DEL FONDO CON EL TALON ESCANEADO
        $fondoPath = "../img/impTalon.jpg";

        if ($idViaje == "") {
            $e[] = array('id'=>'idViaje','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success']) {
           
            $sqlGetTalonesViajeStr = "SELECT idTalon FROM trTalonesViajesTbl WHERE idViajeTractor = ".$idViaje." AND claveMovimiento='TF' AND folio='".$_REQUEST['trViajesTractoresFoliosHdn']."'" ;

            $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);

            for ($nInt=0; $nInt < sizeof($rsTalon['root']); $nInt++) {



               $sqlGetTalonStr = "SELECT tv.*, co.descripcion AS nombreCiaRemitente, co.rfc AS rfcRemitente, co.direccion AS dirCompania, dc.descripcionCentro,dc.codigoCCP, tr.tractor, ".
                                  "vt.claveChofer, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist, ".
                                  "(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                                  "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                        "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                                  "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                        "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                                  "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                                        "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                                  "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".
                                  "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caCompaniasTbl co, ".
                                    "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                                    "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
                                  "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                  "AND dc.distribuidorCentro = tv.distribuidor ".
                                  "AND co.compania = tr.compania  ".
                                  "AND tr.idTractor = vt.idTractor ".
                                  "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                                  "WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                                  "AND col.idColonia = di.idColonia ".
                                  "AND mu.idMunicipio = col.idMunicipio ".
                                  "AND es.idEstado = mu.idEstado ".
                                  "AND pa.idPais = es.idPais ".
                                  "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                                  "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                                  "AND idTalon = ".$rsTalon['root'][$nInt]['idTalon'];

                $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

                if(sizeof($dataTalon['root']) > 0){
                    if($dataTalon['root'][0]['dirCompania'] != ''){
                        $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                        "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                                        "caEstadosTbl es, caPaisesTbl pa ".
                                                        "WHERE pa.idPais = es.idPais ".
                                                        "AND es.idEstado = mu.idEstado ".
                                                        "AND mu.idMunicipio = co.idMunicipio ".
                                                        "AND co.idColonia = dir.idColonia ".
                                                        "AND dir.direccion = ".$dataTalon['root'][0]['dirCompania'];

                        $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                        $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                        $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio']." ".$dirCompania['root'][0]['cp'];
                        $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
                    }

                    $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ,(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal ".
                                                "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                                "WHERE un.vin = ut.vin ".
                                                "AND su.simboloUnidad = un.simboloUnidad ".
                                                "AND ut.idTalon =".$rsTalon['root'][$nInt]['idTalon']." ".
                                                "AND ut.estatus != 'C' ";

                    $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);

                    //echo $rSumaPeso[0]['totalPeso'];

                    if(sizeof($unidadesTalon['root']) > 0){
                        $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                        $dataTalon['root'][0]['codigoCCP'] = $dataTalon['root'][0]['codigoCCP'];
                        $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                        $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                                                                $dataTalon['root'][0]['apellidoPaterno']." ".
                                                                $dataTalon['root'][0]['apellidoMaterno'];
                        $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                        $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                        $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];

                        if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                            $sqlGetServEspStr = "SELECT de.distribuidorOrigen AS distribuidor, dc.descripcionCentro, de.direccionOrigen AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionOrigen ".
                                                "AND dc.distribuidorCentro = de.distribuidorOrigen ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                                "UNION ".
                                                "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionDestino ".
                                                "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."'";

                            $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);


                            for ($nInt=0; $nInt < sizeof($dataServEsp['root']); $nInt++) {
                                $dataServEsp['root'][$nInt]['descDist'] = $dataServEsp['root'][$nInt]['distribuidor']." - ".
                                                                            $dataServEsp['root'][$nInt]['descripcionCentro'];

                                $dataServEsp['root'][$nInt]['dirL1'] = $dataServEsp['root'][$nInt]['calleNumero'];
                                $dataServEsp['root'][$nInt]['dirL2'] = $dataServEsp['root'][$nInt]['colonia'].", ".$dataServEsp['root'][$nInt]['municipio']." ".$dataServEsp['root'][$nInt]['cp'];
                                $dataServEsp['root'][$nInt]['dirL3'] = $dataServEsp['root'][$nInt]['estado'].", ".$dataServEsp['root'][$nInt]['pais'];
                            }
                        }
                        //print_r($dirCompania['root'][0]);
                        echo $dataTalon['root'][0]['centroDistribucion'];

                        if ($dataTalon['root'][0]['companiaTractor']=='TR') {
                              $companiaTr='TRANSDRIZA';
                          }
                          else if ($dataTalon['root'][0]['companiaTractor']=='CR') {
                              $companiaTr='CHRYMEX';
                          }

                       $nombre_fichero = "C:/cartaPorte/CONECTOR/".substr($dataTalon['root'][0]['centroDistribucion'],2,3).$dataTalon['root'][0]['folio'].$companiaTr.".txt";


                        if (file_exists($nombre_fichero)) {
                            echo "El archivo ya existe";
                        } else {
                              $pdf = generaArchivo($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);
                        }

                   
                    } else {
                        $a['errorMessage'] = "ESTE TALON NO TIENE UNIDADES";
                        return $a;
                    }
                } else {
                    $a['errorMessage'] = "ERROR AL INTENTAR CARGAR EL TALON ".$rsTalon['root'][$nInt]['idTalon'];
                    return $a;
                }
            }

            echo json_encode($a);
        }
    }
  
  function generaArchivo($pdf, $fondo, $data, $dirCompania, $unidades, $especiales){  

    //echo json_encode($data);

      if ($data['companiaTractor']=='TR') {
              $companiaTr='TRANSDRIZA';
          }
          else if ($data['companiaTractor']=='CR') {
              $companiaTr='CHRYMEX';
          }
  
    $fileDir = "C:/cartaPorte/CONECTOR/".substr($data['centroDistribucion'],2,3).$data['folio'].$companiaTr.".txt";
    
    
    $flReporte660 = fopen($fileDir, "a") or die("No se pudo generar ,interfaz");

    //A) ENCABEZADO
                         
    fwrite($flReporte660,'tipoDeComprobante=0'.PHP_EOL);                         
    fwrite($flReporte660,'lugarExpedicion=11520'.PHP_EOL);

    if ($data['companiaTractor']=='CR') {
      $regimen='601';
    }
    if ($data['companiaTractor']=='TR') {
      $regimen='624';
    }

    fwrite($flReporte660,"regimen=".$regimen.PHP_EOL);

    if($data['tipoTalon'] == $codigoTalonEspecial){
            $plaza=$especiales[0]['plaza'];
        } else {
            $plaza=$data['descPlazaDestino'];
        }



        if($data['tipoTalon'] == $codigoTalonEspecial){
            $RFC=$especiales[0]['rfc'];
        } else {
            $RFC=$data['rfcDestinatario'];

        }if($RFC ==null){
            $RFC='XAXX010101000';
        } 

         if ($data['claveCliente'] =='10004') {
            $RFC='TRA8402225G0';
            $receptor='TRACOMEX';
            $plaza1='';
          }else if($data['claveCliente'] =='406'){
            $RFC='HGM140108B22';
            $receptor='HYUNDAI GLOVIS MEXICO';
            $plaza1='';

          }
          else if($data['claveCliente'] =='1'){
            $RFC='CME720930GM9';
            $receptor='STELLANTIS MEXICO';
            $plaza1='';

          }
          else if($data['claveCliente'] =='22'){
            $RFC='VME020208169';
            $receptor='VASCOR DE MÉXICO';
            $plaza1='';

          }
          else{
            $receptor=$data['companiaRemitente'].$data['dirL1Dest'];
            $plaza1=$plaza;
          }

        for ($nInt=0; $nInt < sizeof($unidades); $nInt++) {

             $sqlTLP="SELECT * FROM alcargatlptbl WHERE vin ='".$unidades[$nInt]['vin']."' and fechacarga=(SELECT max(fechacarga) from alcargatlptbl where vin ='".$unidades[$nInt]['vin']."')";
            $rsTLP=fn_ejecuta_query($sqlTLP);
      
                        
        }

   

    if ($rsTLP['root'][0]['vin']) {
      $customer=$rsTLP['root'][0]['customer'];
      

      $sqlRFCGlovis="SELECT * FROM cacompaniastbl WHERE compania='".$customer."'";
      $rsRFC=fn_ejecuta_query($sqlRFCGlovis);

      $customer=$rsTLP['root'][0]['customer'];
      $TLP=$rsTLP['root'][0]['tlp'];

      $RFCGlovis=$rsRFC['root'][0]['rfc'];
    }else{
      $customer='';
      $TLP='';
      $RFCGlovis='';
    }

    if ($data['claveCliente'] =='1') {
          $cliente='597';
          $pago='G03';
    }else{
      $cliente=$data['claveCliente'];
      $pago='G03';
    }

    if ($data['claveCliente'] =='406') {
         switch ($data['centroDistribucion']) {
            case 'CDVER':
                $codigo='TRV';
                break;
            case 'CDMTY':
                $codigo='TRM';
                break;
            case 'CDLZC':
                $codigo='TRL';
                break;
            case 'CDLAR':
                $codigo='TRX';
                break;
            case 'CDSFE':
                $codigo='TRS';
                break;
            default:
                $codigo=substr($data['centroDistribucion'],2,3);
                 break;
        }

    }else{
            $codigo=substr($data['centroDistribucion'],2,3);
    }

   
     
    fwrite($flReporte660,"receptor=".$RFC."|".$receptor." ".$plaza1."|".$cliente."||||||||||correo@tracomex.com.mx|RESIDENCIA_FISCAL|NumRegIdTrib|".$customer."|".$TLP."|".$RFCGlovis."|".PHP_EOL); 

    fwrite($flReporte660,"usoCFDI=".$pago."||||".PHP_EOL);
    fwrite($flReporte660,'serie='.substr($data['centroDistribucion'],2,3).PHP_EOL);
    //fwrite($flReporte660,'folio='.$data['folioSica']."-".substr($data['centroDistribucion'],2,3).$data['folio'].PHP_EOL);
    fwrite($flReporte660,'folio='.$codigo.$data['folio'].PHP_EOL);
    fwrite($flReporte660,'formaDePago=99'.PHP_EOL);
    fwrite($flReporte660,'condicionesDePago='.PHP_EOL);
    //fwrite($flReporte660,'metodoDePago=PPD'.PHP_EOL);
    fwrite($flReporte660,"metodoDePago=PPD|F|".$codigo.$data['folio']."||".PHP_EOL);
    fwrite($flReporte660,'confirmacion='.PHP_EOL);
    fwrite($flReporte660,'CFDIRelacionados='.PHP_EOL);

     if ($data['claveCliente'] !=='10004' && $data['companiaTractor']=='TR') {
        $leyenda="TRACOMEX SA DE CV en la calidad de coordinador cumple las obligaciones fiscales de Transdriza SA de CV conforme al artículo 72 fracción V de la ley del impuesto sobre la renta";
     }

    fwrite($flReporte660,"observaciones=".$leyenda."|".$data['tractor']."|".$data['observacionesCP']."|".$data['referencia'].PHP_EOL);

    fwrite($flReporte660,'concepto={78101803|ART-001|1|E48|SERVICIO|SERVICIO DE TRANSPORTACION');
    fwrite($flReporte660,"|".$data['subTotal']."|".$data['subTotal']."|||||Traslados[".$data['subTotal']."|002|Tasa|16.00000|".$data['totalIva']."|||||]Retenciones[|||||".$data['subTotal']."|002|Tasa|4.000|".$data['totalRetenciones']."]extradata[|||||||||]}".PHP_EOL);/////importes

    fwrite($flReporte660,"subTotal=".$data['subTotal'].PHP_EOL);
    fwrite($flReporte660,'descuento='.PHP_EOL);
    fwrite($flReporte660,'moneda=MXN|1.0'.PHP_EOL);
    fwrite($flReporte660,"totalImpuestosRetenidos=".$data['totalRetenciones'].PHP_EOL);
    fwrite($flReporte660,"totalImpuestosTrasladados=".$data['totalIva'].PHP_EOL);
    fwrite($flReporte660,"total=".$data['total'].PHP_EOL);

    $fecha = $data['fechaEvento'];
        $fechaEntrega = strtotime ( "+".$data['diasEntrega']." day" , strtotime ($fecha ));
        $fechaEntrega = date ( 'd/m/Y' , $fechaEntrega );

        


    

    /*fwrite($flReporte660,'CA=ORIGEN DE LA CARGA: '.$data['descPlazaOrigen']."  ".$data['nombreCiaRemitente']." ".$data['rfcRemitente']."  ".$dirCompania['dirL1Cia'].$dirCompania['dirL2Cia']."  ".$dirCompania['dirL3Cia']." |DESTINO DE LA CARGA: ".$data['descDistribuidor']." ".$data['rfcDestinatario']." ".$data['dirL1Dest']."  |EL MISMO|EL MISMO|".$fechaEntrega."||".$pesoTotal."|NO|$0,000.00|N/A|"."*HORA:\t".date("H:i:s")."|TRACTOR ".$data['tractor']."|".$data['claveChofer']." - ".$data['nombreChofer']."|SI||||   ".PHP_EOL);*/


    if ($data['claveCliente'] =='10004') {
          if ($data['companiaTractor']=='CR') {
            $companiaRFC='ATC900122NU0';
          }
          if ($data['companiaTractor']=='TR') {
            $companiaRFC='TRA891031SXA';
          }
    }else{
       $companiaRFC='TRA8402225G0';
    }


    
    //echo $data['companiaTractor'];

    fwrite($flReporte660,'emisor='.$companiaRFC.PHP_EOL.PHP_EOL); 



    fwrite($flReporte660,'// COMPLEMENTO DE CARTA PORTE. INICIA CON ESTA LEYENDA Y EL ORDEN DE LOS RENGLONES ES MUY IMPORTANTE RESPETARLO'.PHP_EOL);
    fwrite($flReporte660,'// esto es un comentario'.PHP_EOL);
    fwrite($flReporte660,'COMPLEMENTO_CARTA_PORTE'.PHP_EOL);
    fwrite($flReporte660,'//Header=Version|TranspInternac|EntradaSalidaMerc|ViaEntradaSalida|TotalDistRec'.PHP_EOL);

    fwrite($flReporte660,"Header=1.0|No|||".$data['kilometrosTabulados'].PHP_EOL.PHP_EOL);

    //////UBICACIONES 

    fwrite($flReporte660,'// INICIO DE NODO UBICACIONES. CADA NODO UBICACIÓN SE COMPONE DE 4 RENGLONES. PUEDEN SER 1 O MAS NODOS UBICACIÓN. EN PRINCIPIO AL MENOS 2, ORIGEN Y DESTINO'.PHP_EOL);
    fwrite($flReporte660,'// SE REPORTA VACÍO SI NO APLICA EL NODO'.PHP_EOL);
    fwrite($flReporte660,'//Ubicacion=TipoEstacion|DistanciaRecorrida'.PHP_EOL);
    fwrite($flReporte660,'//Origen=idOrigen|RFCRemitente|NombreRemitente|NumRegIdTrib|ResidenciaFiscal|NumEstacion|NombreEstacion|NavegacionTrafico|FechaHoraSalida'.PHP_EOL);
    fwrite($flReporte660,'//Destino=IDDestino|RFCDestinatario|NombreDestinatario|NumRegIdTrib|ResidenciaFiscal|NumEstacion|NombreEstacion|NavegacionTrafico|FechaHoraProgLlegada'.PHP_EOL);
    fwrite($flReporte660,'//Domicilio=calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP'.PHP_EOL.PHP_EOL);

      $sqlMun="SELECT  * from calocalidadcptbl where descripcion ='".$dirCompania['estado']."'";
    $rsMun=fn_ejecuta_query($sqlMun);
   
    fwrite($flReporte660,"Ubicacion=01|".$data['kilometrosTabulados'].PHP_EOL);
    fwrite($flReporte660,"Origen=|".$data['rfcRemitente']."|".$data['nombreCiaRemitente']."||||||".$data['fechaEvento'].PHP_EOL);
    fwrite($flReporte660,"Destino=".PHP_EOL);
    fwrite($flReporte660,"Domicilio=".$dirCompania['dirL1Cia']."|".substr($dirCompania['dirL1Cia'],-3)."||||||".$rsMun['root'][0]['c_Estado']."|MEX|".$dirCompania['cp'].PHP_EOL);
    fwrite($flReporte660,"Ubicacion=03|".$data['kilometrosTabulados'].PHP_EOL);
    fwrite($flReporte660,"Origen=".PHP_EOL);
    fwrite($flReporte660,"Destino=".$data['codigoCCP']."|".$data['rfcDestinatario']."|".$data['descDistribuidor']."||||||".$data['fechaEvento'].PHP_EOL);

    $sqlMun="SELECT  * from calocalidadcptbl where descripcion ='".$data['estado']."'";
    $rsMun=fn_ejecuta_query($sqlMun);
    fwrite($flReporte660,"Domicilio=".$data['dirL1Dest']."|".substr($data['dirL1Dest'],-5)."||||||".$rsMun['root'][0]['c_Estado']."|MEX|".$data['cp'].PHP_EOL.PHP_EOL);
    //fwrite($flReporte660,"Domicilio=INTERIOR RECINTO PORTUARIO|2995||2024|06||007|MIC|MEX|60950".PHP_EOL.PHP_EOL);

    /////MERCANCIAS

    if($data['claveCliente'] =='406'){        

        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);
                            
        }
        $unidades=$rsUnidades['root'];

    }
    else if($data['claveCliente'] =='589') {
        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);
                            
        }
        $unidades=$rsUnidades['root'];

    
    }
    else if($data['claveCliente'] =='9') {
        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);
                            
        }
        $unidades=$rsUnidades['root'];

    
    }
    else if($data['claveCliente'] =='620') {
        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);
                            
        }
        $unidades=$rsUnidades['root'];

    
    }
    else if($data['claveCliente'] =='1') {
        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);
                            
        }
        $unidades=$rsUnidades['root'];

    
    }
    else if($data['claveCliente'] =='519') {
        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);
                            
        }
        $unidades=$rsUnidades['root'];

    
    }
    else if($data['claveCliente'] =='1' && $data['claveCliente'] =='CDTUX') {
        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);
                            
        }
        $unidades=$rsUnidades['root'];

    
    }else if($data['claveCliente'] =='1' && $data['claveCliente'] =='CDALT') {
        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);
                            
        }
        $unidades=$rsUnidades['root'];

    
    }
    else{
        for ($nInt=0; $nInt < sizeof($unidades); $nInt++) {       
        
           if ($unidades[$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $unidades[$nInt]['pesoAprox'];
                  $peso1[] = $unidades[$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);

                            
        }
    }


     

    fwrite($flReporte660,'// Inicio de nodo Mercancias. Puede contener 1 o mas nodos mercancia. Cada nodo mercancia se compone de 3 renglones. Debe contener 1 nodo Autotransporte'.PHP_EOL);
    fwrite($flReporte660,'//Mercancias=PesoBrutoTotal|UnidadPeso|PesoNetoTotal|NumTotalMercancias|CargoPorTasacion'.PHP_EOL.PHP_EOL);
    fwrite($flReporte660,"Mercancias=".$pesoTotal."|Kgs||".sizeof($unidades)."|".PHP_EOL.PHP_EOL);

    fwrite($flReporte660,'//Mercancia=BienesTransp|ClaveSTCC|Descripcion|Cantidad|ClaveUnidad|Unidad|Dimensiones|MaterialPeligroso|CveMaterialPeligroso|Embalaje|DescripcionEmbalaje|PesoEnKg|ValorMercancia|Moneda|FraccionArancelaria|UUID'.PHP_EOL);
    fwrite($flReporte660,'//CantidadTransporta={Cantidad|IDOrigen|IDDestino|CvesTrasporte}, {},...{}'.PHP_EOL);
    fwrite($flReporte660,'//DetalleMercancia=UnidadPeso|PesoBruto|PesoNeto|PesoTara|NumPiezas'.PHP_EOL.PHP_EOL);

    
    if($data['claveCliente'] =='406'){

        

        $sqlDetalleUnidades="SELECT tu.vin, un.simboloUnidad, ca.pesoAproximado as pesoAprox, ca.descripcion as descSimbolo ".
                            "FROM trtalonesviajestbl tv, trunidadesdetallestalonestbl tu, alunidadestbl un, casimbolosunidadestbl ca ".
                            "WHERE tv.idViajeTractor=".$data['idViajeTractor'].
                            " AND tv.claveMovimiento='TF' ".
                            "AND tv.idtalon=tu.idtalon ".
                            "AND tv.folioSica='".$data['folioSica']."'". 
                            "AND ca.simboloUnidad=un.simboloUnidad ".
                            "AND un.vin=tu.vin";
        $rsUnidades=fn_ejecuta_query($sqlDetalleUnidades);


        for ($nInt=0; $nInt < sizeof($rsUnidades['root']); $nInt++) {     

           if ($rsUnidades['root'][$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $rsUnidades['root'][$nInt]['pesoAprox'];
                  $peso1[] = $rsUnidades['root'][$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);


            fwrite($flReporte660,"Mercancia=25101503||".$rsUnidades['root'][$nInt]['vin']."   ".$rsUnidades['root'][$nInt]['simboloUnidad']."   ".$rsUnidades['root'][$nInt]['descSimbolo']."|1|H87|UNIDAD||||||".$peso."|0|MXN||".PHP_EOL); 
                            
        }

    }else{
        for ($nInt=0; $nInt < sizeof($unidades); $nInt++) {

           if ($unidades[$nInt]['pesoAprox']== '0') {
                  $peso = '0999';
                  $peso1[] = '0999';
                }else{
                  $peso = $unidades[$nInt]['pesoAprox'];
                  $peso1[] = $unidades[$nInt]['pesoAprox'];
                }

                $pesoTotal= array_sum($peso1);


            fwrite($flReporte660,"Mercancia=25101503||".$unidades[$nInt]['vin']."   ".$unidades[$nInt]['simboloUnidad']."   ".$unidades[$nInt]['descSimbolo']."|1|H87|UNIDAD||||||".$peso."|0|MXN||".PHP_EOL); 
                            
        }
    }

    /////TRANSPORTE

    fwrite($flReporte660,'//AutoTrasporteFederal=PermSCT|NumPermisoSCT|NombreAseg|NumPolizaSeguro'.PHP_EOL);
    fwrite($flReporte660,'//IdentificacionVehicular=ConfigVehicular|PlacaVM|AnioModeloVM'.PHP_EOL);
    fwrite($flReporte660,'//Remolques={SubTipoRem|Placa}{SubTipoRem|Placa}'.PHP_EOL.PHP_EOL);

    $sqlTractor="SELECT * FROM caTractoresTbl where tractor=".$data['tractor']." AND compania='".substr($data['companiaTractor'], 0,2)."'";
    $rsTractor=fn_ejecuta_query($sqlTractor);

    fwrite($flReporte660,"AutoTrasporteFederal=TPAF01|00001178|".$rsTractor['root'][0]['aseguradora']."|".$rsTractor['root'][0]['polizaSeguro'].PHP_EOL);
    fwrite($flReporte660,"IdentificacionVehicular=".$rsTractor['root'][0]['tipoTractor']."|".$rsTractor['root'][0]['placas']."|".$rsTractor['root'][0]['modelo']."|46.5|".PHP_EOL);

    
    $sqlremolque="SELECT * FROM caremolquestbl where idremolque=".$rsTractor['root'][0]['remolque1'];
    $rsRemolque=fn_ejecuta_query($sqlremolque);

    $sqlTractor2="SELECT * FROM caremolquestbl where idremolque=".$rsTractor['root'][0]['remolque2'];
    $rsTractor2=fn_ejecuta_query($sqlTractor2);    

    if (sizeof($rsTractor2['root']) != 0) {
      $remolque2="{CTR012|".$rsTractor2['root'][0]['noPlaca1']."}";
    }
    if ($rsTractor['root'][0]['tipoTractor']=='C3') {
        fwrite($flReporte660,PHP_EOL.PHP_EOL);
    }else{
        fwrite($flReporte660,"Remolques={CTR014|".$rsRemolque['root'][0]['noPlaca1']."}".$remolque2.PHP_EOL.PHP_EOL);
    }   

    fwrite($flReporte660,'//NODO FIGURA TRANSPORTE, EN TEORÍA APLICA CUANDO EL PROPIETARIO DEL TRANSPORTE, ES DISTINTO AL EMISOR DEL CFDI CON COMPLEMENTO'.PHP_EOL);
    fwrite($flReporte660,'// PUEDEN EXISTIR VARIOS NODOS DE OPERADORES, PROPIETARIOS, ARRENDATARIOS Y NOTIFICADO'.PHP_EOL);
    fwrite($flReporte660,'//FiguraTransporte=CveTransporte'.PHP_EOL);
    fwrite($flReporte660,'//Operadores={RFCOperador|NumLicencia|NombreOperador|NumRegIdTribOperador|ResidenciaFiscalOperador|calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP}'.PHP_EOL);
    fwrite($flReporte660,'//Propietario={RFCPropietario|NombrePropietario|NumRegIdTribPropietario|ResidenciaFiscalPropietario|calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP}'.PHP_EOL);
    fwrite($flReporte660,'//Arrendatario={RFCArrendatario|NombreArrendatario|NumRegIdTribArrendatario|ResidenciaFiscalArrendatario|calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP}'.PHP_EOL);
    fwrite($flReporte660,'//Notificado={RFCNotificado|NombreNotificado|NumRegIdTribNotificado|ResidenciaFiscalNotificado|calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP}'.PHP_EOL.PHP_EOL);


    fwrite($flReporte660,'FiguraTransporte=01'.PHP_EOL);
    fwrite($flReporte660,'Operadores='.$data['rfc']."|".$data['licencia']."|".$data['claveChofer']." - ".$data['nombreChofer']."||||||||||||".PHP_EOL);
    fwrite($flReporte660,'Propietario='.PHP_EOL);
    fwrite($flReporte660,'Arrendatario='.PHP_EOL);
    fwrite($flReporte660,'Notificado='.$data['rfcDestinatario']."|".substr($data['descDistribuidor'],8)."|||".$data['descDistribuidor']."|1750||0145|03||048|HID|MEX|42083".PHP_EOL);
    //fwrite($flReporte660,'Notificado=INTERIOR RECINTO PORTUARIO|2995||2024|06||007|MIC|MEX|60950'.PHP_EOL);

    //$fileDir = "\\172.106.0.126\Import\Originales\TRANSDRIZA22".substr($data['centroDistribucion'],2,3).$data['folio'].$data['companiaTractor'].".txt";

    /*$remote_file_url = "C:/CFDI_Traslado/CONECTOR/".substr($data['centroDistribucion'],2,3).$data['folio'].$data['companiaTractor'].".txt";     //origen
     $local_file = "\\172.106.0.126\Import\Originales\TRANSDRIZA22";                         //destino
     $copy = copy($remote_file_url, $local_file);
     if ($copy) {
         echo "Archivo copiado correctamente!";
     } else {
         echo "Error! El archivo no se copió…";
     }*/

     
  
      //enviaArchivoFTP($nombreArchivo);

     //echo "FOLIO ASIGNADO ". $data['folio'];

     echo "Archivo Generado ";




    /////impresion PDF


    // dormir durante 7 segundos
    //sleep(15);


    setlocale(LC_TIME, "spanish");
    $dia=strftime("%d");
    $mes1=strftime("%B");
    $anio=strftime("%Y");

    $mes=substr($mes1, 0,3);

    

    //readfile("C:\MultiConectorCP\FACTURAS\abr2021\LZC\PDF\LZC1029777_XAXX010101000_06abr2021_134636.pdf");

    readfile("C:/CFDI_Traslado/FACTURAS/".$mes.$anio."/".substr($data['centroDistribucion'],2,3)."/PDF/".substr($data['centroDistribucion'],2,3).$data['folio']."_".$RFC."_".$dia.$mes.$anio.".pdf");

    //echo "C:/CFDI_Traslado/FACTURAS/".$mes.$anio."/".substr($data['centroDistribucion'],2,3)."/PDF/".substr($data['centroDistribucion'],2,3).$data['folio']."_".$RFC."_".$dia.$mes.$anio.".pdf";
    
  }

  function enviaArchivoFTP($nombreArchivo, $nombreArchivoCompleto, $IPServidorFTP='172.106.0.126', $usuarioFTP='COFIDI', $passwordFTP='Tracomex1', $directorioFTP='Import/Originales/TRANSDRIZA22', $proveedorCFDI=1) {
    
    $msg  = '';
    $error  = 0;

    //Aplica para Tracomex
    # nombreArchivo
    # nombreArchivoCompleto
      //'../../reportes/cuentasXcobrar/'.$nombreArchivo
    # directorioFTP::Tracomex/Originales
      //AUTOTRANSPORTESCR20
      //COMERCIALCR21
      //TRACOMEX19
      //TRANSDRIZA22
    $archivoDestino = $directorioFTP."/".$nombreArchivo;
    $archivoFuente  = $nombreArchivoCompleto;
    // echo $archivoDestino."--".$archivoFuente."<br>";

    // conexión
    $conexion = ftp_connect($IPServidorFTP);
    // loggeo
    $login = ftp_login($conexion, $usuario, $passwordFTP); 
    //echo "LOG ".$login;

    // conexión
    if ((!$conexion) || (!$login)) { 
      $error  = 1;
          $msg  = "Conexion fallida al sitio FTP!";
    } else {
      $msg = "Conectado a $IPServidorFTP, por el usuario $usuarioFTP";
    }

    if($error == 0) {
      // archivo a copiar/subir
      $descarga = ftp_put($conexion, $archivoDestino, $archivoFuente, FTP_BINARY);
       
      // Verifica que se haya descargado el archivo
      if (!$descarga) { 
        $error  = 1;
        $msg  = "Error al subir el archivo por FTP!";
        //echo "Error al subir el archivo!";
      } else {
        //$msg = "El Archivo $archivoFuente se ha subido exitosamente a la carpeta $IPServidorFTP"."/$archivoDestino";
        $msg = "El Archivo '".$nombreArchivo."' se ha generado exitosamente";
      }
    }
    // Se cierra la conexión
    ftp_close($conexion);

    return array($msg, $error);
  }


?>