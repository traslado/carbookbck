	<?php

		session_start();
		require_once("../funciones/generales.php");
		require_once("../funciones/construct.php");
		require_once("../funciones/utilidades.php");

		switch($_REQUEST['cancelacionRepuveHdn']){
			case 'buscaFolioRepuve':
				buscaFolioRepuve();
				break;
			case 'cancelacionRepuve':
				cancelacionRepuve();
				break;  
			case 'reposicionRepuveActa':
				reposicionRepuveActa();
				break;
			case 'reposicionRepuveChrysler':
				reposicionRepuveChrysler();
				break;  
		    case 'updateObservacion':
				updateObservacion();
				break;                 
		    default:
		        echo '';
		}


		function buscaFolioRepuve(){
      $lsWhereStr = "";
      
      $lsCondicionStr = fn_construct($_REQUEST['folioRepuve'], 'r.folioRepuve', 2);
      $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      
      $lsCondicionStr = fn_construct($_REQUEST['marca'], 'r.marca', 2);
      $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      
      $lsCondicionStr = fn_construct($_REQUEST['centroDistribucion'], 'r.centroDistribucion', 2);
      $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      
      $lsCondicionStr = fn_construct($_REQUEST['vin'], 'r.vin', 2);
      $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      
      $lsCondicionStr = fn_construct($_REQUEST['tid'], 'r.tid', 2);
      $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      
      $lsCondicionStr = fn_construct($_REQUEST['fechaEvento'], 'r.fechaEvento', 2);
      $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      
      $lsCondicionStr = fn_construct($_REQUEST['estatus'], 'r.estatus', 2);
      $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
      
      $lsCondicionStr = fn_construct($_REQUEST['observacion'], 'r.observacion', 2);
      $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);                                          

			$sql = "SELECT r.*, 'true' AS success, '' AS msjResponse, '' AS acta, '' AS fechaActa, (SELECT dc.descripcionCentro FROM caDistribuidoresCentrosTbl dc WHERE dc.distribuidorCentro = r.centroDistribucion) AS descripcionCentro, ".
						 " (SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = r.marca) AS descripcionMarca, ".
						 " (SELECT UPPER(ge.nombre) FROM caGeneralesTbl ge WHERE ge.tabla = 'alRepuveTbl' AND ge.columna = 'estatus' AND ge.valor = r.estatus) AS descripcionEstatus ".
						 " FROM alRepuveTbl r ".$lsWhereStr;
		  //echo "$sql<br>";
		  $rsRepuve = fn_ejecuta_query($sql);
		  
		  if(isset($_REQUEST['valida']) && $_REQUEST['valida'] == 1 && $rsRepuve['records'] > 0)
		  {
		  		$i = 0;
		  		foreach($rsRepuve['root'] as $idx => $row)
		  		{
					  	$fechaActa = '';
					  	switch($row['estatus']){
							    case 'CAN':
											$rsRepuve['root'][$i]['success'] = false;
						  				$rsRepuve['root'][$i]['msjResponse'] = '<b>Folio de REPUVE ya cancelado.</b>';
											break;
							    case 'REE':
											//$rsRepuve['root'][$i]['success'] = false;
						  				//$rsRepuve['root'][$i]['msjResponse'] = '<b>Folio de REPUVE con reposici&oacuten efectuada.</b>';
											$pos = strpos($row['observacion'], 'LEVANTADA EL DIA ');
											if($pos !== false)
											{
													$fechaActa = substr($row['observacion'],$pos+17,strlen($row['observacion']));
													$pos2 = strpos($fechaActa, '/');
													if($pos2 === false)
													{
															$fechaActa = '';
													}													
											}
											if($fechaActa != '')
											{
													$fechaActa = str_replace ("/", "-", $fechaActa);
													$rsRepuve['root'][$i]['fechaActa'] = $fechaActa;
													$pos2 = strpos($row['observacion'], 'CON FOLIO DEL ACTA ');															
													if($pos2 !== false)
													{
															$acta = substr($row['observacion'],$pos2+19,$pos-($pos2+19));
															$acta = trim($acta);
													}
													if(!empty($fechaActa))
													{
															$rsRepuve['root'][$i]['acta'] = $acta;
													}
											}
											break;  											
							    default:							        				  	
					  	}					  	
					  	$i++;  				
		  		}
		  }

			echo json_encode($rsRepuve);  						
		}

		function cancelacionRepuve(){
			$a = array();
			$a['success'] = true;
			$a['msjResponse'] = 'Cancelaci&oacuten efectuada.';
			
			$sql = "UPDATE alRepuveTbl ".
						"SET fechaEvento = NOW(), estatus = 'CAN', observacion = '".strtoupper($_REQUEST['observacion'])."' ". 
						"WHERE folioRepuve = '".$_REQUEST['folioRepuve']."' ";
						//"AND estatus = 'OK' ".
						//"AND vin='".$_REQUEST['vin']."' ".
						//"AND centroDistribucion='".$_REQUEST['centroDistribucion']."' ";		    
		  //echo "$sql<br>";
		  fn_ejecuta_query($sql);

      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
      {
         $a['success']		= false;
         $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten del Folio.";
         $a['sql'] = $sql;
      }  

			if(isset($_REQUEST['vin']) && $_REQUEST['vin'] != '' && $a['success'])
			{
					$sql = "UPDATE alUnidadesTbl ".
								"SET folioRepuve = NULL ". 
								"WHERE vin = '".$_REQUEST['vin']."'";		    
				  fn_ejecuta_query($sql);	

		      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
		      {
		         $a['success']		= false;
		         $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten de la unidad.";
		         $a['sql'] = $sql;
		      }
		      else
		      {
						  $sql = "DELETE FROM alTransaccionUnidadTbl ". 
										"WHERE vin = '".$_REQUEST['vin']."' ".
										"AND tipoTransaccion = 'REP'";		    
						  fn_ejecuta_query($sql);	
						  
				      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
				      {
				         $a['success']		= false;
				         $a['msjResponse'] = $_SESSION['error_sql']. " En el borrado de la transacci&oacuten.";
				         $a['sql'] = $sql;
				      }
		      }		
			}
			echo json_encode($a);					
		}	

		function reposicionRepuveActa(){
			$a = array();
			$a['success'] = true;
			$a['msjResponse'] = 'Reposi&oacuten efectuada.';
			//Reposición por Acta
			if($_REQUEST['panel'] == 0)
			{
					$tipo = 0;
					if(empty($_REQUEST['acta']) || empty($_REQUEST['fecha']))
					{
							$tipo++;
					}
					if($tipo > 0)
					{
							$sql = "UPDATE alRepuveTbl ".
										"SET folioDanado = '".strtoupper($_REQUEST['folioDanado'])."', ".
										" vin = '".strtoupper($_REQUEST['vin'])."', ".
										" fechaEvento = NOW(), estatus = 'REE', ".
										" observacion = 'REPOSICION DEL FOLIO ".strtoupper($_REQUEST['folioRepuve'])." EN EL VIN ".strtoupper($_REQUEST['vin'])." CON FOLIO DEL ACTA LEVANTADA EL DIA'". 
										" WHERE folioRepuve = '".$_REQUEST['folioRepuve']."' ";
										//"AND estatus = 'OK' ".
										//"AND vin='".$_REQUEST['vin']."' ".
										//"AND centroDistribucion='".$_REQUEST['centroDistribucion']."' ";		     							
					}
					else
					{
							$aux = explode('-',$_REQUEST['fecha']);
							//$_REQUEST['fecha'] = $aux[2].'/'.$aux[1].'/'.$aux[0];
							
							$sql = "UPDATE alRepuveTbl ".
										"SET fechaEvento = NOW(), estatus = 'REE', ".
										" observacion = 'REPOSICION DEL FOLIO ".strtoupper($_REQUEST['folioRepuve'])." EN EL VIN ".strtoupper($_REQUEST['vin'])." CON FOLIO DEL ACTA ".strtoupper($_REQUEST['acta'])." LEVANTADA EL DIA ".$_REQUEST['fecha']."'". 
										" WHERE folioRepuve = '".$_REQUEST['folioRepuve']."' ";
										//"AND estatus = 'OK' ".
										//"AND vin='".$_REQUEST['vin']."' ".
										//"AND centroDistribucion='".$_REQUEST['centroDistribucion']."' ";									
					}
				  //echo "$sql<br>";
				  fn_ejecuta_query($sql);
		
		      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
		      {
		         $a['success']		= false;
		         $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten del Folio.";
		         $a['sql'] = $sql;
		      }
		      
					if($a['success'] && $tipo > 0)
					{
							$sql = "UPDATE alUnidadesTbl ".
										"SET folioRepuve = NULL ". 
										"WHERE vin = '".$_REQUEST['vin']."' ".
										"AND folioRepuve = '".$_REQUEST['folioRepuve']."'";
						  fn_ejecuta_query($sql);	
		
				      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
				      {
				         $a['success']		= false;
				         $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten de la unidad.";
				         $a['sql'] = $sql;
				      }					
					}		      		      					
			}
			else
			{
					//Reposición por FCA
					$sql = "UPDATE alRepuveTbl ".
								"SET folioDanado = '".$_REQUEST['folioDanado']."', ".
								" vin = '".strtoupper($_REQUEST['vin'])."', ".
								" fechaEvento = NOW(), estatus = 'REE', ".
								" observacion = 'REPOSICION DEL FOLIO ".strtoupper($_REQUEST['folioRepuve'])." EN EL VIN ".strtoupper($_REQUEST['vin'])." PEDIDO EL DIA ".$_REQUEST['fecha']."'".
								" WHERE folioRepuve = '".$_REQUEST['folioRepuve']."' ";
								//"AND estatus = 'OK' ".
								//"AND vin='".$_REQUEST['vin']."' ".
								//"AND centroDistribucion='".$_REQUEST['centroDistribucion']."' ";		
				  //echo "$sql<br>";
				  fn_ejecuta_query($sql);	

		      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
		      {
		         $a['success']		= false;
		         $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten de la unidad.";
		         $a['sql'] = $sql;
		      }
					if($a['success'])
					{
							$sql = "UPDATE alUnidadesTbl ".
										"SET folioRepuve = NULL ". 
										"WHERE vin = '".$_REQUEST['vin']."' ".
										"AND folioRepuve = '".$_REQUEST['folioRepuve']."'";
						  //echo "$sql<br>";
						  fn_ejecuta_query($sql);	
		
				      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
				      {
				         $a['success']		= false;
				         $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten de la unidad.";
				         $a['sql'] = $sql;
				      }					
					}			      															
			}
			echo json_encode($a);	
		}	

		function reposicionRepuveChrysler(){

			$today =  date('Y-m-d H:i:s');	

			$fechaEmision= substr($_REQUEST['fechaEmision'],0,10) ;

			$sqlUpdfolioRepuve = "UPDATE alrepuveTbl ".
	  						"SET  fechaEvento='".$today."', estatus='".$_REQUEST['estatus']."', observacion='SE REPUSO EL FOLIO ".$_REQUEST['folioDanado']." POR EL FOLIO ".$_REQUEST['folioaReponer']."  REQUERIDO POR CHRYSLER CON FECHA DE ".$fechaEmision." ' ". 
	  						"WHERE folioRepuve=".$_REQUEST['folioDanado']." ".
	  						"AND estatus='OK' ".
	  						"AND vin='".$_REQUEST['vin']."' "; 

		     fn_ejecuta_query($sqlUpdfolioRepuve);


			$sqlUpdfolioRepuvealunidades = "UPDATE alunidadesTbl ".
	  						"SET folioRepuve= null ". 
	  						"WHERE  vin='".$_REQUEST['vin']."' ".
	  						"AND folioRepuve=".$_REQUEST['folioDanado']." ";		    
		     fn_ejecuta_query($sqlUpdfolioRepuvealunidades);
					
		}	

				function updateObservacion(){

			$today =  date('Y-m-d H:i:s');	

			$observacion = $_REQUEST['observaciones'] ;

			$observacion = strtoupper($observacion);

			$sqlUpdfolioRepuve = "UPDATE alrepuveTbl ".
	  						"SET  observacion='".$observacion."' ". 
	  						"WHERE folioRepuve=".$_REQUEST['folioRepuve']." ".
	  						"AND centroDistribucion='".$_REQUEST['centroDistribucion']."' ".
	  						"AND vin='".$_REQUEST['vin']."' "; 

		     fn_ejecuta_query($sqlUpdfolioRepuve);
					
		}
	?>