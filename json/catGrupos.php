<?php
	session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    //require_once("alLocalizacionPatios.php");
    require_once("alRetrabajos.php");

    $_REQUEST = trasformUppercase($_REQUEST);
	
	switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
	switch($_REQUEST['catGruposActionHdn']){
		case 'getGrupos':
			getGrupos();
			break;
		case 'addGrupo':
			addGrupo();
			break;
		case 'updGrupo':
			updGrupo();
			break;
		default:
            echo '';
	}

	function getGrupos(){
		$lsWhereStr = '';

		if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGruposCentrosDistribucionCmb'], "cm.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGruposGrupoTxt'], "cm.grupo", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGrupos = "SELECT * FROM alGruposPatioTbl";
        
        $rs = fn_ejecuta_query($sqlGrupos);

        echo json_encode($rs);
	}
	
	function addGrupo2(){

		$a['success'] = true;
		$e = array();

		if($_REQUEST['catGruposCtoDistribucionCmb'] == ''){
			$a['success'] = false;
			$e[] = array('id' => 'catGruposCtoDistribucionCmb', 'msg' => getRequerido());
		}
		if($_REQUEST['catGruposGrupoTxt'] == ''){
			$a['success'] = false;
			$e[] = array('id' => 'catGruposGrupoTxt', 'msg' => getRequerido());
		}

		if($a['success'] == true){
			$sqlAddGrupo = "INSERT INTO alGruposPatioTbl(centroDistribucion, grupo, clasificacion, ".
				           "marcaClasificacion, distribuidor, marcaDistribuidor, color) ".
						   "VALUES('".$_REQUEST['catGruposCtoDistribucionCmb']."', ".
						   $_REQUEST['catGruposGrupoTxt'].", ".
						   replaceEmptyNull("'".$_REQUEST['catGruposClasificacionClasificacionCmb']."'").", ".
						   replaceEmptyNull("'".$_REQUEST['catGruposClasificacionMarcaCmb']."'").", ".
						   replaceEmptyNull("'".$_REQUEST['catGruposDistribuidorDistribuidorCmb']."'").", ".
						   replaceEmptyNull("'".$_REQUEST['catGruposDistribuidorMarcaCmb']."'").", ".
						   replaceEmptyNull("'".$_REQUEST['catGruposColor']."'").");";

			$rs = fn_ejecuta_Add($sqlAddGrupo);
			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
				$a['success'] = true;
				$a['sql'] = $sqlAddGrupo;
				$a['successMessage'] = 'Se ha agregado Correctamente el Grupo';
			} else{
				$a['success'] = true;
				$a['sql'] = $sqlAddGrupo;
				$a['errorMessage'] = mysql_errno()." - ".$a['sql'];
				
			}
		}
		$a['errors'] = $e;
		echo json_encode($a);
	}
	function updGrupo(){
		$a['success'] = true;
		$e = array();

		if($_REQUEST['catGruposCtoDistribucionCmb'] == ''){
			$a['success'] = false;
			$e[] = array('id' => 'catGruposCtoDistribucionCmb', 'msg' => getRequerido());
		}
		if($_REQUEST['catGruposGrupoTxt'] == ''){
			$a['success'] = false;
			$e[] = array('id' => 'catGruposGrupoTxt', 'msg' => getRequerido());
		}
		if($_REQUEST['catGruposIdGrupoHdn'] == ''){
			$a['success'] = false;
			$e[] = array('id' => 'catGruposIdGrupoHdn', 'msg' => getRequerido());
		}

		if($a['success'] == true){
			$sqlAddGrupo = "UPDATE alGruposPatioTbl SET clasificacion, = ".
						   replaceEmptyNull("'".$_REQUEST['catGruposClasificacionClasificacionCmb']."'").", ".
				           "marcaClasificacion = ".
				           replaceEmptyNull("'".$_REQUEST['catGruposClasificacionMarcaCmb']."'").", ".
				           "distribuidor = ".
				           replaceEmptyNull("'".$_REQUEST['catGruposDistribuidorDistribuidorCmb']."'").", ".
				           "marcaDistribuidor = ".
				           replaceEmptyNull("'".$_REQUEST['catGruposDistribuidorMarcaCmb']."'").", ".
				           "color = ".
						   replaceEmptyNull("'".$_REQUEST['catGruposColor']."'").;
						   " WHERE idGrupo = ".$_REQUEST['catGruposIdGrupoHdn'].
						   " AND centroDistribucion = '".$_REQUEST['catGruposCtoDistribucionCmb'].
						   "' AND grupo = '".$_REQUEST['catGruposGrupoTxt']."';";
						   
						   
						  
			$rs = fn_ejecuta_Upd($sqlAddGrupo);
			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
				$a['success'] = true;
				$a['sql'] = $sqlAddGrupo;
				$a['successMessage'] = 'Se ha agregado Correctamente el Grupo';
			} else{
				$a['success'] = true;
				$a['sql'] = $sqlAddGrupo;
				$a['errorMessage'] = mysql_errno()." - ".$a['sql'];
				
			}
		}
		$a['errors'] = $e;
		echo json_encode($a);

	}

?>