<?php
require_once("../funciones/fpdf/fpdf.php");
require_once("../funciones/barcode.php");
require_once("../funciones/generales.php");
require_once("../funciones/construct.php");
require_once("../funciones/utilidades.php");

	$_SESSION['modulo'] = "pedimentosPdf";
	
	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
	
    switch($_REQUEST['alShipperActionHdn']){
        case 'getPedimento':
            getPedimento();
            break;
        case 'generaPedimento':
            generaPedimento();
            break;  
        default:
            echo '';
    }


function getPedimento (){
	$a = array();
    $e = array();
  	$success = true;

  	if($_REQUEST['shipperAvanzadaTxt'] == "numSerie"){
  	  $e[] = array('id'=>'shipperAvanzadaTxt','msg'=>getRequerido());
      $success = false;
  	}
  	if ($success) { 
  	 
    $sqlGetPedimentoStr = "SELECT * FROM alshipperspdftbl " .
    					  "WHERE numSerie = '".$_REQUEST['shipperAvanzadaTxt']."'";
	
	$rs = fn_ejecuta_query($sqlGetPedimentoStr);
		
	echo json_encode($rs);
	}else {
      	$a['errorMessage'] = getErrorRequeridos();
      	$a['errors'] = $e;
      	$a['successTitle'] = getMsgTitulo();
      echo json_encode($a);
    }
}


function generaPedimento (){
	$a = array();
    $e = array();
  	$success = true;

  	if($_REQUEST['shipperAvanzadaTxt'] == "numSerie"){
  	  $e[] = array('id'=>'shipperAvanzadaTxt','msg'=>getRequerido());
      $success = false;
  	}

  	if ($success){  

        $sqlGetDataStr = "SELECT * FROM alshipperspdftbl " .
        				 "WHERE numSerie = '".$_REQUEST['shipperAvanzadaTxt']."'";

      	$rs = fn_ejecuta_query($sqlGetDataStr);
		//echo json_encode($rs);

      	$data = $rs['root'][0];
      	//print_r($data);
		//$pdf = new FPDF('P', 'mm', array(210, 297));
		$cellh = 4;
		$cellw = 0;
		$offsetX= 0;
		$offsetY=0;    	    	

    	
    	$pdf=new FPDF('P','mm','Letter');
		$pdf->AddPage();
		//$pdf->SetMargins(6,1,3);
		$pdf->Image("../../carbookBck/img/platilla_pedimentos.jpg", 0, 0,217); 
		$pdf->SetXY(85,24);
		$pdf->SetFont('Helvetica','',6.5);

    	$border = 0;
    	if(sizeof($data) > 0){	      	
	      	$pdf->SetMargins(0.2, 0.2, 0.2, 0.2);

	      	//clave Pedimento  
	      	$pdf->setY(29+$offsetX);
	      	$pdf->SetX(100+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'CLAVE PEDIMENTO: '.$data['cvePedCont'], $border,1, 'L');

	      	//numero Pedimento  
	      	$pdf->setY(31+$offsetX);
	      	$pdf->SetX(100+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'NUMERO DE PEDIMENTO: '.$data['numpedCont'], $border,1, 'L');

	      	//acuse recibio
	      	$pdf->setY(34+$offsetX);
	      	$pdf->SetX(100+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'ACUSE DE RECIBIO: '.$data['acuseRboCont'], $border,1, 'L');      	

	        //route king
	      	$pdf->setY(37+$offsetX);
	      	$pdf->SetX(100+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'ROUTE KING: '.$data['routeKing'], $border,1, 'L');

	      	//numero Embarque
	      	$pdf->setY(31+$offsetX);
	      	$pdf->SetX(185+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['numEmb'], $border,1, 'L');

	    	switch($data['fmmEmb']){ 
				case 1: 
			 		$month = 'ENERO';
				break;
				case 2:
			 		$month = 'FEBRERO';
				break;
				case 3:
			 		$month = 'MARZO';
				break;
				case 4:
			 		$month = 'ABRIL';
				break;
				case 5:
			 		$month = 'MAYO';
				break;
				case 6:
			 		$month = 'JUNIO';
				break;
				case 7:
			 		$month = 'JULIO';
				break;
				case 8:
			 		$month = 'AGOSTO';
				break;
				case 9:
			 		$month = 'SEPTIEMBRE';
				break;
				case 10:
			 		$month = 'OCTUBRE';
				break;
				case 11:
			 		$month = 'NOVIEMBRE';
				break;
				case 12:
			 		$month = 'DICIEMBRE';
				break;	
			}

	      	//lugar y fecha
	      	$pdf->setY(45+$offsetX);
	      	$pdf->SetX(162+$offsetY);	 
	      	$pdf->SetFont('Arial','',06);	      	
	      	$pdf->Cell(93,10,'MEXICO, D.F. A '.$data['fddEmb'].'  '.$month.' DE '.$data['faaEmb'], $border,1, 'L');

	      	//numero de furgon	      	
	      	$pdf->setY(51+$offsetX);
	      	$pdf->SetX(124+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['numFurgon'], $border,1, 'L');

	      	//planta	      	
	      	$pdf->setY(66+$offsetX);
	      	$pdf->SetX(124+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['numSerie'].' CAR ASSY PLANT', $border,1, 'L');

	      	//destinatario	      	
	      	$pdf->setY(60+$offsetX);
	      	$pdf->SetX(161+$offsetY);	 
	      	$pdf->SetFont('Arial','',06);	      	
	      	$pdf->Cell(93,10,$data['destinatario'], $border,1, 'L');

	      	//destinatario	direccion      	
	      	$pdf->setY(64+$offsetX);
	      	$pdf->SetX(161+$offsetY);	 
	      	$pdf->SetFont('Arial','',06);	      	
	      	$pdf->Cell(93,10,$data['direccion'], $border,1, 'L');

	      	//localidad	      	
	      	$pdf->setY(67+$offsetX);
	      	$pdf->SetX(161+$offsetY);	 
	      	$pdf->SetFont('Arial','',06);	      	
	      	$pdf->Cell(93,10,$data['localidad'], $border,1, 'L');

	      	//patente	      	
	      	$pdf->setY(49+$offsetX);
	      	$pdf->SetX(45+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'PATENTE: '.$data['patenteCont'], $border,1, 'L');

	      	//R.F.C	      	
	      	$pdf->setY(52+$offsetX);
	      	$pdf->SetX(45+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'R.F.C: '.$data['rfcCont'], $border,1, 'L');

	      	//pais destino	      	
	      	$pdf->setY(55+$offsetX);
	      	$pdf->SetX(45+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'PAIS DESTINO: '.$data['paisDestCont'], $border,1, 'L');

	      	//numero de serie	      	
	      	$pdf->setY(105+$offsetX);
	      	$pdf->SetX(15+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['numSerie'], $border,1, 'L');	      	

	      	//vin	      	
	      	$pdf->setY(105+$offsetX);
	      	$pdf->SetX(28+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['vin'], $border,1, 'L');

	      	//simbolo	      	
	      	$pdf->setY(105+$offsetX);
	      	$pdf->SetX(55+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['simbolo'], $border,1, 'L');

	      	//descripcion simbolo	      	
	      	$pdf->setY(105+$offsetX);
	      	$pdf->SetX(68+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['descEspaniol'], $border,1, 'L');

	      	//von	      	
	      	$pdf->setY(110+$offsetX);
	      	$pdf->SetX(15+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['von'], $border,1, 'L');

	      	//distribuidor	      	
	      	$pdf->setY(110+$offsetX);
	      	$pdf->SetX(28+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['distribuidor'], $border,1, 'L');

	      	//opciones	
	      	$pdf->setY(110+$offsetX);
	      	$pdf->SetX(40+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['opciones'], $border,1, 'L');

	      	//folio factura	
	      	$pdf->setY(115+$offsetX);
	      	$pdf->SetX(40+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['folioFactura'], $border,1, 'L');

	      	//fecha	
	      	$pdf->setY(115+$offsetX);
	      	$pdf->SetX(80+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['faaFactura'].'-'.$data['fmmFactura'].'-'.$data['fddFactura'], $border,1, 'L');

	      	//tipo cambio	
	      	$pdf->setY(115+$offsetX);
	      	$pdf->SetX(120+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'$ '.$data['tipoCambio'], $border,1, 'L');

	      	//importe Unidad	
	      	$pdf->setY(105+$offsetX);
	      	$pdf->SetX(154+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impUndbas'],2,'.',','), $border,1, 'L');

	      	//importe moneda Nacional	
	      	$pdf->setY(105+$offsetX);
	      	$pdf->SetX(187+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impMonnal'],2,'.',','), $border,1, 'L');

	        //val 807 opcs	
	      	$pdf->setY(110+$offsetX);
	      	$pdf->SetX(155+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['val807Opcs'],2,'.',','), $border,1, 'L');

	      	//importe Seguro
	      	$pdf->setY(115+$offsetX);
	      	$pdf->SetX(160+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['tshpOpcs'],2,'.',','), $border,1, 'L');

	        //importe flete	
	      	$pdf->setY(110+$offsetX);
	      	$pdf->SetX(174+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impFleldo'],2,'.',','), $border,1, 'L');

	      	//importe gastos aduanal
	      	$pdf->setY(115+$offsetX);
	      	$pdf->SetX(177+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impGtosadu'],2,'.',','), $border,1, 'L');

	      	//importe gastos aduanal
	      	$pdf->setY(110+$offsetX);
	      	$pdf->SetX(188+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impUnidad'],2,'.',','), $border,1, 'L');

	      	//importe gastos aduanal
	      	$pdf->setY(115+$offsetX);
	      	$pdf->SetX(188+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impUnidad'],2,'.',','), $border,1, 'L');

	      	//Total Unidades
	      	$pdf->setY(148+$offsetX);
	      	$pdf->SetX(15+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'Total Unidades: 01', $border,1, 'L');

	      	//importe gastos aduanal
	      	$pdf->setY(157+$offsetX);
	      	$pdf->SetX(188+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impUnidad'],2,'.',','), $border,1, 'L');

	      	//importe gastos aduanal
	      	$pdf->setY(161+$offsetX);
	      	$pdf->SetX(188+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impUnidad'],2,'.',','), $border,1, 'L');

	      	//importe Seguro
	      	$pdf->setY(161+$offsetX);
	      	$pdf->SetX(160+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['tshpOpcs'],2,'.',','), $border,1, 'L');

	      	//importe Unidad	
	      	$pdf->setY(155+$offsetX);
	      	$pdf->SetX(154+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impUndbas'],2,'.',','), $border,1, 'L');
			//val 807 opcs	
	      	$pdf->setY(158+$offsetX);
	      	$pdf->SetX(155+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['val807Opcs'],2,'.',','), $border,1, 'L');	     

	      	//importe gastos aduanal
	      	$pdf->setY(158+$offsetX);
	      	$pdf->SetX(176+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impGtosadu'],2,'.',','), $border,1, 'L');

	      	//importe flete	
	      	$pdf->setY(155+$offsetX);
	      	$pdf->SetX(173+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,number_format($data['impFleldo'],2,'.',','), $border,1, 'L');

	      	//importe con Letra
	      	$pdf->setY(155+$offsetX);
	      	$pdf->SetX(15+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['tfacConletra'], $border,1, 'L');

	      	//importe en Pesos Mexicanos
	      	$pdf->setY(158+$offsetX);
	      	$pdf->SetX(15+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'IMPORTE EN PESOS MEXICANOS:            $'.number_format($data['impMonnal'],2,'.',','), $border,1, 'L');

	      	//PAIS DE ORIGEN: MEXICO
	      	$pdf->setY(158+$offsetX);
	      	$pdf->SetX(90+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'PAIS DE ORIGEN: MEXICO', $border,1, 'L');

	      	//COUNTRY OF ORIGIN: MEXICO
	      	$pdf->setY(162+$offsetX);
	      	$pdf->SetX(90+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,'COUNTRY OF ORIGIN: MEXICO', $border,1, 'L');

	      	//fecha
	      	$pdf->setY(162+$offsetX);
	      	$pdf->SetX(15+$offsetY);	 
	      	$pdf->SetFont('Arial','',07);	      	
	      	$pdf->Cell(93,10,$data['fddEmb'].' / '.$data['fmmEmb'].' / '.$data['faaEmb'], $border,1, 'L');
	      	$pdf->Output('PEDIMENTO '.$data['numEmb'].'.pdf', I);

    	} else {
      		echo json_encode(array('success'=>false, 'errorMessage'=>$_SESSION['error_sql']." <br> ".$sqlGetDataStr));
    	}	
  	} else {
      	$a['errorMessage'] = getErrorRequeridos();
      	$a['errors'] = $e;
      	$a['successTitle'] = getMsgTitulo();
      echo json_encode($a);
    }	
}
?>