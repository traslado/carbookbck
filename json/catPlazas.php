<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
	
	switch($_REQUEST['catPlazasActionHdn']){
		case 'getPlazas':
			getPlazas();
			break;
		case 'getKilometrosPlaza':
			getKilometrosPlaza();
			break;
		case 'getKilometrosPlazaOrigenes':
			getKilometrosPlazaOrigenes();
			break;
		case 'addPlaza':
			addPlaza();
			break;
		case 'updPlaza':
			updPlaza();
			break;
		case 'addKmPlaza':
			addKmPlaza();
			break;
		case 'updKmPlaza':
			updKmPlaza();
			break;
		default:
            echo '';	
	}
	
	function getPlazas(){
		$lsWhereStr = "";
	
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catPlazasIdPlazaHdn'], "p.idPlaza", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catPlazasPlazaTxt'], "p.plaza", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catPlazasEstatusHdn'], "p.estatus", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
    		if (isset($_REQUEST['catPlazasNoIdPlazaHdn']) && $_REQUEST['catPlazasNoIdPlazaHdn'] != "") {
    			$lsCondicionStr = fn_construct("!=".$_REQUEST['catPlazasNoIdPlazaHdn'], "p.idPlaza", 0);
		    	$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    		}
	    }
		
		$sqlGetStr = "SELECT p.idPlaza, p.plaza, p.estatus, ".
					 "(SELECT g.nombre FROM caGeneralesTbl g WHERE g.tabla='caPlazasTbl' AND g.columna='estatus' AND g.valor=p.estatus) AS 'nombreEstatus' ".
					 "FROM caPlazasTbl p ".$lsWhereStr." ORDER BY p.plaza";
	
		$rs = fn_ejecuta_query($sqlGetStr);
		
		echo json_encode($rs);
	}
	
	function getKilometrosPlaza(){
		$lsWhereStr = "WHERE dc.idPlaza = k.idPlazaOrigen ".
					  "AND dc.tipoDistribuidor = 'CD' ";
		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catPlazasIdPlazaHdn'], "k.idPlazaOrigen", 0);
    		$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaIdDestinoHdn'], "k.idPlazaDestino", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaKilometrosTxt'], "k.kilometros", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosTarifaCobradaTxt'], "k.tarifaCobradaPorKm", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPLazaTarifaIdaTxt'], "k.tarifaEspecialIda", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaTarifaRegresoTxt'], "k.tarifaEspecialRegreso", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaRetencionHdn'], "k.retencion", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaDiasEntregaHdn'], "k.diasEntrega", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catPlazasCentroDistHdn'], "dc.distribuidorCentro", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	
		$sqlGetKmPlazaStr = "SELECT k.idPlazaOrigen, k.idPlazaDestino, k.kilometros, k.tarifaCobradaPorKm, ".
							"k.tarifaEspecialIda, k.tarifaEspecialRegreso, k.retencion,  k.diasEntrega, ".
							"dc.distribuidorCentro AS centroDistPlaza, ".
							"(SELECT p1.plaza FROM caPlazasTbl p1 where p1.idPlaza = k.idPlazaOrigen) as nombreOrigen, ".
							"(SELECT p2.plaza FROM caPlazasTbl p2 where p2.idPlaza = k.idPlazaDestino) as nombreDestino ".
							"FROM caKilometrosPlazaTbl k, caDistribuidoresCentrosTbl dc ".$lsWhereStr.
							"ORDER BY nombreOrigen, nombreDestino ";
		
		$rs = fn_ejecuta_query($sqlGetKmPlazaStr);
		
		echo json_encode($rs);
	}

	function getKilometrosPlazaOrigenes(){
		$lsWhereStr = "";
		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catPlazasIdPlazaHdn'], "k.idPlazaOrigen", 0);
    		$lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaIdDestinoHdn'], "k.idPlazaDestino", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaKilometrosTxt'], "k.kilometros", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosTarifaCobradaTxt'], "k.tarifaCobradaPorKm", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPLazaTarifaIdaTxt'], "k.tarifaEspecialIda", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaTarifaRegresoTxt'], "k.tarifaEspecialRegreso", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }		
		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaRetencionHdn'], "k.retencion", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catKilometrosPlazaDiasEntregaHdn'], "k.diasEntrega", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	
		$sqlGetKmPlazaStr = "SELECT k.idPlazaOrigen, ".
							"(SELECT p1.plaza FROM caPlazasTbl p1 where p1.idPlaza = k.idPlazaOrigen) as nombreOrigen ".
							"FROM caKilometrosPlazaTbl k ".$lsWhereStr." GROUP BY idPlazaOrigen";
		
		$rs = fn_ejecuta_query($sqlGetKmPlazaStr);
		
		echo json_encode($rs);
	}
	
	function addPlaza(){
		$a = array();
        $e = array();
        $a['success'] = true;

		if($_REQUEST['catPlazasPlazaTxt'] == ""){
            $e[] = array('id'=>'catPlazasPlazaTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catPlazasEstatusHdn'] == ""){
            $e[] = array('id'=>'catPlazasEstatusHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
			
		if($a['success'] == true){
			$sqlAddPlazaStr = "INSERT INTO caPlazasTbl (plaza, estatus) VALUES(".
							  "'".$_REQUEST['catPlazasPlazaTxt']."',".
							  "'".$_REQUEST['catPlazasEstatusHdn']."')";


			$rs = fn_ejecuta_Add($sqlAddPlazaStr);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
				$a['sql'] = $sqlAddPlazaStr;
				$a['successMessage'] = getPlazaSuccessMsg();
			} else {
            	$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPlazaStr;
        	}
        }
		$a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
	}
	
	function updPlaza(){
		$a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catPlazasIdPlazaHdn'] == ""){
            $e[] = array('id'=>'catPlazasIdPlazaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catPlazasPlazaTxt'] == ""){
            $e[] = array('id'=>'catPlazasPlazaTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catPlazasEstatusHdn'] == ""){
            $e[] = array('id'=>'catPlazasEstatusHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

		if ($a['success'] == true) {
			$sqlUpdPlazaStr = "UPDATE caPlazasTbl ".
						    	"SET plaza= '".$_REQUEST['catPlazasPlazaTxt']."', ".
						    	"estatus ='".$_REQUEST['catPlazasEstatusHdn']."' ".
						    	"WHERE idPlaza=".$_REQUEST['catPlazasIdPlazaHdn'];
			
			$rs = fn_ejecuta_Upd($sqlUpdPlazaStr);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
				$a['sql'] = $sqlUpdPlazaStr;
				$a['successMessage'] = getPlazaUpdMsg();
			} else {	
        		$a['success'] = false;
            	$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdPlazaStr;
   	 		}
		}

		$a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();
	    echo json_encode($a);
	}

	function addKmPlaza(){
		$a = array();
        $e = array();
        $a['success'] = true;

		if($_REQUEST['catPlazasOrigenCmb'] == ""){
            $e[] = array('id'=>'catPlazasOrigenCmb','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catPlazasDestinoCmb'] == ""){
            $e[] = array('id'=>'catPlazasDestinoCmb','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catPlazasKilometrosTxt'] == ""){
            $e[] = array('id'=>'catPlazasKilometrosTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catPlazasDiasTxt'] == ""){
            $e[] = array('id'=>'catPlazasDiasTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

		if($a['success']){
			$sqlAddKmPlazaStr = "INSERT INTO caKilometrosPlazaTbl (idPlazaOrigen, idPlazaDestino, kilometros, tarifaCobradaPorKm,".
								"tarifaEspecialIda, tarifaEspecialRegreso, retencion, diasEntrega) VALUES (".
									"(SELECT idPlaza FROM caDistribuidoresCentrosTbl ".
										"WHERE distribuidorCentro = '".$_REQUEST['catPlazasOrigenCmb']."'),".
									$_REQUEST['catPlazasDestinoCmb'].",".
									$_REQUEST['catPlazasKilometrosTxt'].",".
									replaceEmptyNull($_REQUEST['catPlazasTarifaCobradaTxt']).",".
									replaceEmptyNull($_REQUEST['catPlazasTarifaEspecialIdaTxt']).",".
									replaceEmptyNull($_REQUEST['catPlazasTarifaEspecialRegTxt']).",".
									replaceEmptyNull($_REQUEST['catPlazasRetencionTxt']).",".
									$_REQUEST['catPlazasDiasTxt'].")";


			$rs = fn_ejecuta_Add($sqlAddKmPlazaStr);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
		    	$a['sql'] = $sqlAddKmPlazaStr;
	        	$a['successMessage'] = getKmPlazaSuccessMsg();
			} else {
	       		$a['success'] = false;
	        	$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddKmPlazaStr;
			}
		}
		$a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();
		echo json_encode($a);
	}

	function updKmPlaza(){
		$a = array();
        $e = array();
        $a['success'] = true;

		if($_REQUEST['catPlazasOrigenCmb'] == ""){
            $e[] = array('id'=>'catPlazasOrigenCmb','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catPlazasDestinoCmb'] == ""){
            $e[] = array('id'=>'catPlazasDestinoCmb','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catPlazasKilometrosTxt'] == ""){
            $e[] = array('id'=>'catPlazasKilometrosTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catPlazasDiasTxt'] == ""){
            $e[] = array('id'=>'catPlazasDiasTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

		if($a['success']){
			$sqlUpdKmPlazaStr = "UPDATE caKilometrosPlazaTbl SET ".
								"kilometros = ".$_REQUEST['catPlazasKilometrosTxt'].", ".
								"tarifaCobradaPorKm = ".replaceEmptyNull($_REQUEST['catPlazasTarifaCobradaTxt']).",".
								"tarifaEspecialIda = ".replaceEmptyNull($_REQUEST['catPlazasTarifaEspecialIdaTxt']).",".
								"tarifaEspecialRegreso = ".replaceEmptyNull($_REQUEST['catPlazasTarifaEspecialRegTxt']).",".
								"retencion = ".replaceEmptyNull($_REQUEST['catPlazasRetencionTxt']).",".
								"diasEntrega = ".$_REQUEST['catPlazasDiasTxt']." ".
								"WHERE idPlazaOrigen = (SELECT idPlaza FROM caDistribuidoresCentrosTbl ".
										"WHERE distribuidorCentro = '".$_REQUEST['catPlazasOrigenCmb']."') ".
								"AND idPlazaDestino = ".$_REQUEST['catPlazasDestinoCmb'];


			$rs = fn_ejecuta_Upd($sqlUpdKmPlazaStr);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
		    	$a['sql'] = $sqlUpdKmPlazaStr;
	        	$a['successMessage'] = getKmPlazaUpdMsg();
			} else {
	       		$a['success'] = false;
	        	$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdKmPlazaStr;
			}
		}
		$a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();
		echo json_encode($a);		
	}
?>
