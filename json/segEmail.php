<?php
	require_once("../funciones/mail/class.phpmailer.php");
	require_once("../funciones/generales.php");

	$a = array();
	$a['success'] 		= true;
	$a['msjResponse'] = 'Se le ha enviado un correo electr&oacutenico con su contraseña.';

	$pwd = "";

	$for = $_REQUEST['correoElectronico'];
	$msg = utf8_decode('Su contraseña es: ').'<br><br><b>';

	$sql = "SELECT password, estatus FROM segUsuariosTbl WHERE correoElectronico = '".$_REQUEST['correoElectronico']."'";
	//echo "$sql<br>";	
	$rs = fn_ejecuta_query($sql);

	if($rs['records'] > 0) {
			if($rs['root'][0]['estatus'] == '1') {
					//var_dump($rs['root'][0]['password']);					
					$sql = "SELECT password FROM segContraseniasTbl WHERE passwordMD5 = '".$rs['root'][0]['password']."'";
					//echo "$sql<br>";
					$rsMD5 = fn_ejecuta_query($sql);
					
					if($rsMD5['records'] > 0)
					{
							//var_dump($rsMD5['root'][0]['password']);
							$msg .= $rsMD5['root'][0]['password'].
											utf8_decode('</b><br><br>Favor de no responder este correo porque la cuenta no es administrada por algún usuario.').
											'<br>Saludos!<br><br>Grupo Tracomex<br>Lago Nargis #34 Piso 5<br>CDMX 11520<br>'; //'Tel: 55 5203 9022';
							
							//Se configura el SMTP:
							$mail = new PHPMailer();
							$mail->IsSMTP();
							$mail->SMTPAuth = true;
							
							//Se configura el destinatario, subject y el cuerpo del mensaje
							$mail->AddAddress($_REQUEST['correoElectronico']); 							// Dirección de envío
							$mail->IsHTML(true); 																						// El correo se envía como HTML
							$mail->Subject = utf8_decode('Recuperación de contraseña'); 		// Título del email
							$mail->Body = $msg; 																						// Cuerpo del Mensaje a enviar
							
							//Se envía el email
							$envia = $mail->Send(); 																				// Envía el correo
					
							if(!$envia){
									$a['success'] = false;
									$a['msjResponse'] = 'Error en el env&iacuteo de la contraseña al correo electr&oacutenico capturado.';
							}							
					}
					else
					{
							$a['success'] = false;
							$a['msjResponse'] = 'La contraseña no se encuentra.';
					}					
			}
			else
			{
					$a['success'] = false;
					$a['msjResponse'] = 'El usuario est&aacute inactivo.';
			}
	}
	else {
			$a['success'] = false;
			$a['msjResponse'] = 'No existe el correo.';
	}

	echo json_encode($a);
?>