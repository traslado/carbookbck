<?php
	session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php"); 

    $_REQUEST = trasformUppercase($_REQUEST);
	
	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
	
    switch($_REQUEST['alControlLlavesActionHdn']){
        case 'addControlLlaves':
            addControlLlaves();
            break;
        case 'getLlavesEntregadas':
            getLlavesEntregadas();
            break;
        case 'updControlLlavesRecepcion':
            updControlLlavesRecepcion();
            break;
        default:
            echo '';
    }

    function addControlLlaves(){
        $a = array();
        $e = array();
        $a['success'] = true;

        /*if($_REQUEST['alControlLlavesDistribuidorCentroHdn'] == ""){
            $e[] = array('id'=>'alControlLlavesDistribuidorCentroHdn:','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }*/
        
        if($_REQUEST['alControlLlavesVinHdn'] == ""){
            $e[] = array('id'=>'alControlLlavesVinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($_REQUEST['alControlLlavesUsuarioHdn'] == ""){
            $e[] = array('id'=>'alControlLlavesUsuarioTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }


        if ($a['success'] == true) {
            if ($_REQUEST['alControlLlavesVinHdn'] != "|") {
                $vinAsignados = explode('|', substr($_REQUEST['alControlLlavesVinHdn'], 0, -1));
                $sqlAsignarVinStr = "INSERT INTO alControlLlavesTbl (centroDistribucion,vin,fechaEntrega,idUsuarioEntrega,ipEntrega,claveMovimiento)VALUES";

                for ($i = 0; $i<sizeof($vinAsignados);$i++) {
                    if($i != 0){
                        $sqlAsignarVinStr .= ",";
                    }
                    
                    $sqlAsignarVinStr .= "('".$_SESSION['usuCompania']."','".$vinAsignados[$i]."','".date("Y-m-d H:i:s")."','".$_REQUEST['alControlLlavesUsuarioHdn']."', '".$_SERVER['REMOTE_ADDR']."','LE')";
                }

                $rs = fn_ejecuta_Add($sqlAsignarVinStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['sql'] = $sqlAsignarVinStr;
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAsignarVinStr;
                }
            } 
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }
    function updControlLlavesRecepcion(){
        $a = array();
        $e = array();
        $a['success'] = true;

        /*if($_REQUEST['alControlLlavesRecepcionDistribuidorCentroHdn'] == ""){
            $e[] = array('id'=>'alControlLlavesRecepcionDistribuidorCentroHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }*/

        if($_REQUEST['alControlLlavesVinHdn'] == ""){
            $e[] = array('id'=>'alControlLlavesVinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($_REQUEST['alControlLlavesUsuarioHdn'] == ""){
            $e[] = array('id'=>'alControlLlavesUsuarioHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

         if($a['success'] == true) {
            if ($_REQUEST['alControlLlavesVinHdn'] != "|") {
                $vinAsignados = explode('|', substr($_REQUEST['alControlLlavesVinHdn'], 0, -1));
                for($i = 0; $i<sizeof($vinAsignados);$i++) {
            
            $sqlUpdLlavesRecepcionStr = "UPDATE alControlLlavesTbl ".
                                "SET centroDistribucion= '".$_SESSION['usuCompania']."', ".
                                "idUsuarioRecepcion= '".$_REQUEST['alControlLlavesUsuarioHdn']."', ".
                                "fechaRecepcion= '".date("Y-m-d H:i:s")."', ".
                                "ipRecepcion= '".$_SERVER['REMOTE_ADDR']."', ".
                                "claveMovimiento= 'LR' ".
                                "WHERE vin='".$vinAsignados[$i]."'";
        
            $rs = fn_ejecuta_Upd($sqlUpdLlavesRecepcionStr);
        }

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['sql'] = $sqlUpdLlavesRecepcionStr;
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdLlavesRecepcionStr;
                }
            } 
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getLlavesEntregadas(){
        $lsWhereStr = '';
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesRecuperaVinHdn'], "vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesRecuperaAvanzadaHdn'], "avanzada", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }        
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesUsuarioEntregaHdn'], "idUsuarioEntrega", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['alControlLlavesEntregaFechaHdn'], "fechaEntrega", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlLlavesEntregadasStr = "SELECT * FROM alcontrolllavestbl ".$lsWhereStr;
        $rs = fn_ejecuta_query($sqlLlavesEntregadasStr);
            
        echo json_encode($rs);
    }

    

