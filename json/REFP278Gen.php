<?php
	// if ($_REQUEST['ActionHdn'] == 'generarEtiqueta') {
	// 	ini_set('display_errors', 1);
	// 	ini_set('display_startup_errors', 1);
	// 	error_reporting(E_ALL);
	// }

	require('../funciones/generalesSICA.php');
	require('../funciones/barcode.php');
	require('../funciones/construct.php');
	require('../funciones/fpdf/fpdf.php');
	ini_set('max_execution_time', 10000);
	session_start();
	date_default_timezone_set(date_default_timezone_get());

	$hoy = getdate();
	$fechaReport = date('Y-m-d H:i:s', $hoy[0]);

	switch($_REQUEST['ActionHdn']) {
		case 'generarEtiqueta':
			if ($_REQUEST['tipoArchHdn'] == 'PDF') {
				generarEtiqueta();
			}
			break;
	}

	function generarEtiqueta() {
		$ls_refacciones = $_REQUEST['psRef'];

		$ls_orden = "";
		switch ($_REQUEST['tipoOrden']) {
			case 'L':
				$ls_orden = " ORDER BY ex.localizacion ASC";
				break;
			case 'R':
				$ls_orden = " ORDER BY cr.claveRefaccion ASC";
				break;
			case 'D':
				$ls_orden = " ORDER BY cr.descripcionRefaccion ASC";
				break;
			case 'T':
				$ls_orden = " ORDER BY cr.tipoRefaccion ASC";
				break;
			default:
				// code...
				break;
		}

		$selStr = "SELECT cr.*, ex.localizacion".
				" FROM refCatRefaccionesTbl cr".
				" INNER JOIN refCatExistenciasTbl ex ON ex.claveCompania = cr.claveCompania AND ex.idRefaccion = cr.idRefaccion AND ex.claveAlmacen = ".$_REQUEST['claveAlmacen'].
				" WHERE cr.claveCompania = ".$_REQUEST['claveCompania'].
				" AND cr.idRefaccion IN (${ls_refacciones})".
				$ls_orden;
		$selRst = fn_ejecuta_query_SICAMLL($selStr);
		// $refaccionesArr[] = array('idRefaccion' => $selRst['root'][0]['idRefaccion'], 'claveRefaccion' => $selRst['root'][0]['claveRefaccion'], 'descripcionRefaccion' => $selRst['root'][0]['descripcionRefaccion'], 'localizacion' => $selRst['root'][0]['localizacion']);
		$refaccionesArr = $selRst['root'];

		$imprime = false;
		// var_dump($refaccionesArr);die();
		$a = array('msjResponse' => 'No existe informacion.', 'success' => false, 'root' => array());
		$li_reg = count($refaccionesArr);
		if ($li_reg > 0) {
			$pdf = new FPDF('P','mm','Letter');
    		$pdf->SetMargins(3.3, 10.4, 6);
			$pdf->SetAutoPageBreak(true, 5);
	        $iaux = 1;
	        $ipag = 0;
			for ($i=0; $i < $li_reg; $i++) { 
				$row = $refaccionesArr[$i];
				$claveRefaccion = $row['claveRefaccion'];
				if ($iaux == 1 || $ipag == 1) {
					$pdf->AddPage();
					$pdfX = 3.4;
					$pdfY = 10.5;
					$pdf->SetXY($pdfX,$pdfY);
					$ipag = 0;
				}

				# margen cuadro
				$pdf->SetFont('Arial', '', 10);
                $pdf->SetFillColor(224,224,224);
                $pdf->SetDrawColor(128,128,128);
				if ($_REQUEST['incluyeBordesExt'] == 'S') {
                	$pdf->Rect($pdfX, $pdfY, 66, 25);
                }
				$pdfX = $pdf->GetX();
				$li_recYAux = $pdf->GetY();
				$li_recY = 25.35;
				# 1
				if ($li_recYAux > 5 && $li_recYAux < 15) {
					$li_recY = 25.55;
				}
				# 2
				if ($li_recYAux > 20 && $li_recYAux < 45) {
					$li_recY = 26.50;
				}
				# 3
				if ($li_recYAux > 50 && $li_recYAux < 70) {
					$li_recY = 26.60;
				}
				# 4
				if ($li_recYAux > 80 && $li_recYAux < 95) {
					$li_recY = 26.90;
				}
				# 5
				if ($li_recYAux > 100 && $li_recYAux < 120) {
					$li_recY = 26.60;
				}
				# 6
				if ($li_recYAux > 130 && $li_recYAux < 145) {
					$li_recY = 26.10;
				}
				# 7
				if ($li_recYAux > 150 && $li_recYAux < 180) {
					$li_recY = 26.30;
				}
				# 8
				if ($li_recYAux > 185 && $li_recYAux < 200) {
					$li_recY = 26.30;
				}
				# 9
				if ($li_recYAux > 210 && $li_recYAux < 230) {
					$li_recY = 25.70;
				}
				$pdfY = $pdf->GetY() + $li_recY;
				# descripcion
				$pdf->SetFont('Arial','',8.5);
				$pdf->SetFillColor(235, 235, 235);
				$pdf->SetDrawColor(211,211,211);
				$pdf->SetLineWidth(0.07);
				$pdf->Ln(2);

				$pdf->SetX($pdfX);
				$border = 0;
				$pdf->Cell(64,3.5, $row['descripcionRefaccion'], $border, 0, 'C', false);
				$pdf->Ln(4);
				$pdf->SetX($pdfX);
				$pdf->Cell(64,3.5, $row['claveRefaccion'], $border, 0, 'C', false);
				$pdf->Ln(3.5);
				$pdf->SetX($pdfX);
				//****************
				//CODIGO DE BARRAS
				//****************
				// Configuraciones para código de barras
				$fontSize = 100; // GD1 in px ; GD2 in point
				$marge = 10; // between barcode and hri in pixel
				$x = 400;  // barcode center x
				$y = 60;  // barcode center y
				$height = 180;  // barcode height
				$width = 3;  // barcode width
				$angle = 0; // rotation in degrees 
				$code = $claveRefaccion; // vin code for barcode
				$type = 'code128'; // barcode type
				$imageTitle = $row['idRefaccion'].'_barcodeTemp.gif';

				// ASIGNAR RECURSOS DE GD
				$im    = imagecreatetruecolor(750, 120);
				$black = imagecolorallocate($im,0x00,0x00,0x00);
				$white = imagecolorallocate($im,0xff,0xff,0xff);
				$red   = imagecolorallocate($im,0xff,0x00,0x00);
  				$blue  = imagecolorallocate($im,0x00,0x00,0xff);
				imagefilledrectangle($im, 0, 0, 800, 120, $white);

				// Crear código de barras
				$data = Barcode::gd($im, $black, $x, $y, $angle, $type, array('code'=>$code), $width, $height);
				// Generar imagen con código de barras
				$pdfXAux = $pdf->GetX()+1;
				$pdfYAux = $pdf->GetY()+0.5;
				Header('Content-type: image/gif');
				imagegif($im, $imageTitle);
				$pdf->Image($imageTitle,$pdfXAux,$pdfYAux,63,9);

				imagedestroy($im);
				if (file_exists($imageTitle)) {
					unlink($imageTitle);
				}
				//****************
				//FIN CODIGO DE BARRAS
				//****************

				# localizacion
				$pdf->SetFont('Arial','',8.5);
				$pdf->Ln(11.5);
				$pdf->SetX($pdfX);
				$pdf->Cell(64,3.5, $row['localizacion'], 0, 0, 'C', false);

				# rect .0
				$pdf->SetXY($pdfX,$pdfY);

				if ($iaux % 30 == 0) {
					$pdfX = 3.4;
					$pdfY = 10.5;
					$pdf->SetXY($pdfX,$pdfY);
					$ipag = 1;
				}
				if ($iaux % 10 == 0) {
					$pdfX = $pdf->GetX() + 70;
					$pdfY = 10.5;
					$pdf->SetXY($pdfX,$pdfY);
				}
				// if ($iaux==6) {
				// 	break;
				// }
				$iaux++;
			}

			$nombreArchivo = 'REFP278_' . date('YmdHis', strtotime($fecha)) . '.pdf';
			$archivo = $nombreArchivo;
	        $pdf->Output($archivo,'I');
		}
	}
?>