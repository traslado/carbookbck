<?php
	session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
	
    switch($_REQUEST['faFacturacionTCOActionHdn']) {
        case 'getFacturacionTCO':
            getFacturacionTCO();
            break;
        case 'getCompaniasFacturacion':
            getCompaniasFacturacion();
            break;
        case 'addFacturacionTCO':
            addFacturacionTCO();
            break;
        case 'updFacturacionTCO':
            updFacturacionTCO();
            break;
        case 'bajaFacturacionTCO':
            bajaFacturacionTCO();
            break;
        case 'cancelarFacturacionTCO':
            cancelarFacturacionTCO();
            break;
        default:
            echo '';
    }

    function getFacturacionTCO(){
        $lsWhereStr = "WHERE dc.distribuidorCentro = ft.distribuidor ".
                        "AND co.compania = ft.compania ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturacionTCOCompaniaHdn'], "ft.compania", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturacionTCODistribuidorHdn'], "ft.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturacionTCOVinTxt'], "ft.vin", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturacionTCOSimboloHdn'], "ft.simboloUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturacionTCOFechaFacturacionTxt'], "ft.fechaFacturacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturacionTCOFechaHastaTxt'], "ft.fechaFacturacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['faFacturacionTCOEstatusChequeHdn'], "ft.estatusChequeBonificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        if($_REQUEST['faFacturacionTCOCanceladaChk'] == 'FALSE'){
            $sqlGetFacturacionTCO = "SELECT ft.compania, ft.distribuidor, ft.simboloUnidad, ft.vin, ft.fechaFacturacion, NULL AS fechaCancelacion, ".
                                    "ft.importeSeguro, ft.estatusChequeBonificacion, ft.estatusNotaBonificacion, ft.tarifaBonificacion, ".
                                    "ft.costoUnidad, ft.cuotaAsociacion, ft.cuotaTraslado, ft.chequeBonificacion, ft.notaBonificacion, ".
                                    "ft.flotilla, dc.descripcionCentro, co.descripcion AS descripcionCompania, ".
                                    "(SELECT 1 FROM faCargaTCOCanceladasTbl ca WHERE ca.compania = ft.compania AND ca.distribuidor = ft.distribuidor ".
                                        "AND ca.vin = ft.vin) AS cancelada ".
                                    "FROM faCargaTCOTbl ft, caDistribuidoresCentrosTbl dc, caCompaniasTbl co " . $lsWhereStr.
                                    " ORDER BY compania, distribuidor, vin, fechaCancelacion "; 
        } else {
            $sqlGetFacturacionTCO = "SELECT ft.*, dc.descripcionCentro, co.descripcion AS descripcionCompania, 1 AS cancelada ".
                                    "FROM faCargaTCOCanceladasTbl ft, caDistribuidoresCentrosTbl dc, caCompaniasTbl co " . $lsWhereStr.
                                    " ORDER BY compania, distribuidor, vin, fechaCancelacion ";     
        }

        $rs = fn_ejecuta_query($sqlGetFacturacionTCO);   

        for ($i=0; $i < sizeof($rs['root']); $i++) {
            $rs['root'][$i]['descCompania'] = $rs['root'][$i]['compania']." - ".$rs['root'][$i]['descripcionCompania'];
            $rs['root'][$i]['descDist'] = $rs['root'][$i]['distribuidor']." - ".$rs['root'][$i]['descripcionCentro'];
        }
        
        echo json_encode($rs);
    }

    function getCompaniasFacturacion(){
        $lsWhereStr = "WHERE tipoCompania = 'F' OR compania = 'TCO'";

        $sqlGetCompaniasFacturacion = "SELECT compania, descripcion FROM caCompaniasTbl " . $lsWhereStr;     

        $rs = fn_ejecuta_query($sqlGetCompaniasFacturacion);   

        for ($i=0; $i < sizeof($rs['root']); $i++) { 
            $rs['root'][$i]['descCompania'] = $rs['root'][$i]['compania']." - ".$rs['root'][$i]['descripcion'];
        }
            
        echo json_encode($rs);
    }

    function addFacturacionTCO(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturacionTCOCompaniaHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCODistribuidorHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCODistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOVinTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOVinTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOSimboloHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCOSimboloHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOFechaFacturacionTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOFechaFacturacionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOImporteSeguroTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOImporteSeguroTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOCostoTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCostoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOCuotaAsociacionTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCuotaAsociacionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOCuotaTrasladoTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCuotaTrasladoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOFlotillaChk'] == ""){
            $e[] = array('id'=>'faFacturacionTCOFlotillaChk','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if($a['success']){
            $sqlAddFacturacionTCO = "INSERT INTO faCargaTCOTbl (compania, distribuidor, simboloUnidad, vin, fechaFacturacion, importeSeguro,".
                                    "estatusChequeBonificacion, estatusNotaBonificacion, tarifaBonificacion, costoUnidad, cuotaAsociacion, ".
                                    "cuotaTraslado, chequeBonificacion, notaBonificacion, flotilla) VALUES (".
                                    "'".$_REQUEST['faFacturacionTCOCompaniaHdn']."',".
                                    "'".$_REQUEST['faFacturacionTCODistribuidorHdn']."',".
                                    "'".$_REQUEST['faFacturacionTCOSimboloHdn']."',".
                                    "'".$_REQUEST['faFacturacionTCOVinTxt']."',".
                                    "'".$_REQUEST['faFacturacionTCOFechaFacturacionTxt']."',".
                                    $_REQUEST['faFacturacionTCOImporteSeguroTxt'].",".
                                    "'D',".
                                    "'D',".
                                    "(SELECT importeBonificacion FROM caSimbolosUnidadesTbl ".
                                        "WHERE simboloUnidad ='".$_REQUEST['faFacturacionTCOSimboloHdn']."'),".
                                    $_REQUEST['faFacturacionTCOCostoTxt'].",".
                                    $_REQUEST['faFacturacionTCOCuotaAsociacionTxt'].",".
                                    $_REQUEST['faFacturacionTCOCuotaTrasladoTxt'].",".
                                    "NULL,".
                                    "NULL,".
                                    "'".$_REQUEST['faFacturacionTCOFlotillaChk']."')";

                fn_ejecuta_query($sqlAddFacturacionTCO);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = getFacturacionTCOSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddFacturacionTCO;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }

    function updFacturacionTCO(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturacionTCOCompaniaHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCODistribuidorHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCODistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOVinTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOVinTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOImporteSeguroTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOImporteSeguroTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if($a['success']){
            if($_REQUEST['faFacturacionTCOCanceladaChk'] == 'FALSE'){
                $tabla = 'faCargaTCOTbl';
            } else {
                $tabla = 'faCargaTCOCanceladasTbl';
            }

            $sqlUpdFacturacionTCO = "UPDATE ".$tabla." SET ".
                                    "importeSeguro = ".$_REQUEST['faFacturacionTCOImporteSeguroTxt']." ".
                                    "WHERE compania = '".$_REQUEST['faFacturacionTCOCompaniaHdn']."' ".
                                    "AND distribuidor = '".$_REQUEST['faFacturacionTCODistribuidorHdn']."' ".
                                    "AND vin = '".$_REQUEST['faFacturacionTCOVinTxt']."' ".
                                    "AND estatusChequeBonificacion = 'D'";

                fn_ejecuta_query($sqlUpdFacturacionTCO);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = getFacturacionTCOUpdMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlUpdFacturacionTCO;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }

    function bajaFacturacionTCO(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturacionTCOCompaniaHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCODistribuidorHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCODistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOVinTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOVinTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if($a['success']){
            $sqlUpdFacturacionTCO = "UPDATE faCargaTCOTbl SET ".
                                    "estatusChequeBonificacion = 'B', ".
                                    "estatusNotaBonificacion = 'B' ".
                                    "WHERE compania = '".$_REQUEST['faFacturacionTCOCompaniaHdn']."' ".
                                    "AND distribuidor = '".$_REQUEST['faFacturacionTCODistribuidorHdn']."' ".
                                    "AND vin = '".$_REQUEST['faFacturacionTCOVinTxt']."' ".
                                    "AND estatusChequeBonificacion = 'D'";

                fn_ejecuta_query($sqlUpdFacturacionTCO);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = getFacturacionTCOUpdMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlUpdFacturacionTCO;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }

    function cancelarFacturacionTCO(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturacionTCOCompaniaHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCODistribuidorHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCODistribuidorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOVinTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOVinTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOSimboloHdn'] == ""){
            $e[] = array('id'=>'faFacturacionTCOSimboloHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOFechaFacturacionTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOFechaFacturacionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOImporteSeguroTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOImporteSeguroTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOCostoTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCostoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOCuotaAsociacionTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCuotaAsociacionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOCuotaTrasladoTxt'] == ""){
            $e[] = array('id'=>'faFacturacionTCOCuotaTrasladoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionTCOFlotillaChk'] == ""){
            $e[] = array('id'=>'faFacturacionTCOFlotillaChk','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if($a['success']){
            $sqlCancelFacTCO = "INSERT INTO faCargaTCOCanceladasTbl (compania, distribuidor, simboloUnidad, vin, fechaFacturacion, fechaCancelacion, ".
                                    " importeSeguro, estatusChequeBonificacion, estatusNotaBonificacion, tarifaBonificacion, costoUnidad, ".
                                    "cuotaAsociacion, cuotaTraslado, chequeBonificacion, notaBonificacion, flotilla) VALUES (".
                                    "'".$_REQUEST['faFacturacionTCOCompaniaHdn']."',".
                                    "'".$_REQUEST['faFacturacionTCODistribuidorHdn']."',".
                                    "'".$_REQUEST['faFacturacionTCOSimboloHdn']."',".
                                    "'".$_REQUEST['faFacturacionTCOVinTxt']."',".
                                    "'".$_REQUEST['faFacturacionTCOFechaFacturacionTxt']."',".
                                    "'".date("Y-m-d")."',".
                                    $_REQUEST['faFacturacionTCOImporteSeguroTxt'].",".
                                    "'D',".
                                    "'D',".
                                    "(SELECT importeBonificacion FROM caSimbolosUnidadesTbl ".
                                        "WHERE simboloUnidad ='".$_REQUEST['faFacturacionTCOSimboloHdn']."'),".
                                    $_REQUEST['faFacturacionTCOCostoTxt'].",".
                                    $_REQUEST['faFacturacionTCOCuotaAsociacionTxt'].",".
                                    $_REQUEST['faFacturacionTCOCuotaTrasladoTxt'].",".
                                    "NULL,".
                                    "NULL,".
                                    "'".$_REQUEST['faFacturacionTCOFlotillaChk']."')";

                fn_ejecuta_query($sqlCancelFacTCO);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = getFacturacionTCOCancelMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlCancelFacTCO;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }


?>