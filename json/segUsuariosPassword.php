<?php
	session_start();

	$_SESSION['modulo'] = "segUsuariosPasswordTbl";

    require("../funciones/generales.php");
    require("../funciones/construct.php");

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
	
	switch($_REQUEST['segUsuariosPasswordActionHdn']){
        case 'obtenPassword':
            obtenPassword();
            break;
        case 'insertar':
            insertarPassword();
            break;
        default:
            echo '';
    }
	
	function obtenPassword(){
		$whereStr = "WHERE up.idUsuario = u.idUsuario";
		
		if ($gb_error_filtro == 0){
			$condicionStr = fn_construct($_REQUEST['segUsuariosPasswordIdUsuarioHdn'], "up.idUsuario", 1);
			$whereStr = fn_concatena_condicion($whereStr, $condicionStr);
		}
		if ($gb_error_filtro == 0){
			$condicionStr = fn_construct($_REQUEST['segUsuariosPasswordFechaHdn'], "up.fecha", 1);
			$whereStr = fn_concatena_condicion($whereStr, $condicionStr);
		}
		if ($gb_error_filtro == 0){
			$condicionStr = fn_construct($_REQUEST['segUsuariosPasswordPasswordTxt'], "up.password", 1);
			$whereStr = fn_concatena_condicion($whereStr, $condicionStr);
		}
		if ($gb_error_filtro == 0){
			$condicionStr = fn_construct($_REQUEST['segUsuariosPasswordObservacionesTxt'], "up.observaciones", 1);
			$whereStr = fn_concatena_condicion($whereStr, $condicionStr);
		}
		if ($gb_error_filtro == 0){
			$condicionStr = fn_construct($_REQUEST['segUsuariosPasswordIdUsuarioActHdn'], "up.idUsuarioAct", 1);
			$whereStr = fn_concatena_condicion($whereStr, $condicionStr);
		}
		if ($gb_error_filtro == 0){
			$condicionStr = fn_construct($_REQUEST['segUsuariosPasswordIpActHdn'], "up.ipAct", 1);
			$whereStr = fn_concatena_condicion($whereStr, $condicionStr);
		}	

    	$sqlGetPwd =	"SELECT up.*, u.nombre AS usuario ".
    					"FROM segUsuariosPasswordTbl up, segUsuariosTbl u " . $whereStr;

		$rs = fn_ejecuta_query($sqlGetPwd);
		
		
		echo json_encode($rs);
	}
	

	function insertarPassword(){
		$a = array();
    $a['success'] = true;

    $sqlAddPwd =  "INSERT INTO segUsuariosPasswordTbl ".
	     						"(idUsuario, fecha, password, observaciones, idUsuarioAct, ipAct) ".
	     						"VALUES (" .
     							$_REQUEST['segUsuariosPasswordIdUsuarioTxt'] . "," . 
     							"'".$_REQUEST['segUsuariosPasswordFechaTxt'] . "'," . 
     							"'".md5($_REQUEST['segUsuariosPasswordPasswordTxt']) . "'," . 
     							"'".$_REQUEST['segUsuariosPasswordObservacionesTxt'] . "'," . 
     							$_SESSION['idUsuario'] . "," . 
     							"'".$_SERVER['REMOTE_ADDR'] . "')";
      $rs = fn_ejecuta_query($sqlAddPwd);
 
	    if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
         	$sql = "SELECT password FROM segContraseniasTbl WHERE passwordMD5 = '".md5($_REQUEST['segUsuariosPasswordPasswordTxt'])."'";
					//echo "$sql<br>";
					$rsMD5 = fn_ejecuta_query($sql);
					
					if($rsMD5['records'] == 0)
					{
	           	$sql = "INSERT INTO segContraseniasTbl VALUES ('".md5($_REQUEST['segUsuariosPasswordPasswordTxt'])."','".$_REQUEST['segUsuariosPasswordPasswordTxt']."')";
							//echo "$sql<br>";
							fn_ejecuta_query($sql);
					}   

	        $sqlUpdUserPwd = 	"UPDATE segUsuariosTbl ".
					        					"SET password = '".md5($_REQUEST['segUsuariosPasswordPasswordTxt'])."' ".
					        					"WHERE idUsuario = ".$_REQUEST['segUsuariosPasswordIdUsuarioTxt'];
 
	       	fn_ejecuta_query($sqlUpdUserPwd);

	       	if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
		       	$a['sql'] = $sqlAddPwd;
		        $a['successMessage'] = getCambiarContraseniaSuccessMsg();
		    } else {
		    	$a['success'] = false;
	        	$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdUserPwd;	
		    }
	    } else {
	        $a['success'] = false;
	        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPwd;
	       
	    } 
      echo json_encode($a);
	}

?>