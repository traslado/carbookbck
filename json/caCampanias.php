<?php
	session_start();
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }
	
	switch($_REQUEST['caCampaniasActionHdn']){
        case 'getCampanias': 
            getCampanias();
            break;
        case 'addCampanias': 
            addCampanias();
            break;
        case 'updCampanias': 
            updCampanias();
            break;
        case 'delCampanias': 
            delCampanias();
            break;
        default:
            echo '';
    }
    function getCampanias(){
        $lsWhereStr = "WHERE tipoDistribuidor = 'CA' ";
        
        $lsCondicionStr = fn_construct($_REQUEST['caCampaniasCampaniaTxt'], 'distribuidorCentro', 2);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);

        $lsCondicionStr = fn_construct($_REQUEST['caCampaniasDescripcionTxt'], 'descripcionCentro', 2);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);

        $lsCondicionStr = fn_construct($_REQUEST['  '], 'sucursalDe', 2);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);

        $sqlCampaStr =  "SELECT * FROM caDistribuidoresCentrosTbl ".$lsWhereStr;
        echo json_encode(fn_ejecuta_query($sqlCampaStr));
    }
    function addCampanias(){
        $a = array('success'=>true);
        $insCampaStr =  "INSERT INTO caDistribuidoresCentrosTbl(distribuidorCentro,descripcionCentro,tipoDistribuidor,".
                        "idPlaza,sucursalDe,direccionFiscal,direccionEntrega,estatus,detieneUnidades)".
                        "VALUES(".
                        "'". $_REQUEST['caCampaniasCampaniaTxt'] ."',".
                        "'". $_REQUEST['caCampaniasDescripcionTxt'] ."',".
                        "'CA',".
                        "1,".
                        "'". $_REQUEST['caCampaniasOrigenCmb'] ."',".
                        "1,".
                        "1,".
                        "1,".
                        "0".
                        ")";
        fn_ejecuta_Add($insCampaStr);
        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = "La Campaña se agrego correctamente";
                $a['sql'] = $insCampaStr;
        }else{
            $a['success'] = false;
            $a['sql'] = $insCampaStr;
            $a['errorMessage'] = $_SESSION['error_sql'];
        }
        echo json_encode($a);
    }
    function updCampanias(){
        $a = array('success'=>true);
        $updCampaStr =  "UPDATE caDistribuidoresCentrosTbl ".
                        "SET descripcionCentro = '". $_REQUEST['caCampaniasDescripcionTxt'] ."', ".
                            "sucursalDe ='". $_REQUEST['caCampaniasOrigenCmb'] ."' ".
                        "WHERE tipoDistribuidor = 'CA' ".
                        "AND distribuidorCentro = '".  $_REQUEST['caCampaniasCampaniaTxt'] ."'";
        fn_ejecuta_Upd($updCampaStr);                
        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = "La Campaña se modifico correctamente";
                $a['sql'] = $updCampaStr;
        }else{
            $a['success'] = false;
            $a['sql'] = $updCampaStr;
            $a['errorMessage'] = $_SESSION['error_sql'];
        }
        echo json_encode($a);

    }
    function delCampanias(){
        $a = array('success'=>true);
        $updCampaStr =  "DELETE FROM caDistribuidoresCentrosTbl ".
                        "WHERE tipoDistribuidor = 'CA' ".
                        "AND distribuidorCentro = '".  $_REQUEST['caCampaniasCampaniaTxt'] ."'";
        fn_ejecuta_query($updCampaStr);                
        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['successMessage'] = "La Campaña se elimino correctamente";
                $a['sql'] = $updCampaStr;
        }else{
            $a['success'] = false;
            $a['sql'] = $updCampaStr;
            $a['errorMessage'] = $_SESSION['error_sql'];
        }
        echo json_encode($a);
    }
?>