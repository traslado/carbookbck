<?php
session_start();

require_once ("../funciones/generales.php");
require_once ("../funciones/construct.php");
require_once ("../funciones/utilidades.php");

$_REQUEST = trasformUppercase($_REQUEST);

switch ($_SESSION['idioma'])
{
    case 'ES':
        include ("../funciones/idiomas/mensajesES.php");
    break;
    case 'EN':
        include ("../funciones/idiomas/mensajesEN.php");
    break;
    default:
        include ("../funciones/idiomas/mensajesES.php");
}

switch ($_REQUEST['trPolizaGastosActionHdn'])
{
    case 'getDatosPoliza':
        getDatosPoliza();
    break;
     case 'obtenDiasGratificacion':
        obtenDiasGratificacion();
    break;
    case 'getAnticipos':
        getAnticipos();
    break;
    case 'getDatosComprobacion':
        getDatosComprobacion();
    break;
    case 'getFacturasPoliza':
        getFacturasPoliza();
    break;
    case 'getRfcPeajes':
        getRfcPeajes();
    break;
    case 'updCombustible':
        updCombustible();
    break;
    case 'addFacturaGastos':
        addFacturaGastos();
    break;
    case 'dltFacturasViaje':
        dltFacturasViaje();
    break;
    case 'dltFacturaGastos':
        dltFacturaGastos();
    break;
    case 'revisarLavadaMes':
        revisarLavadaMes();
    break;
    case 'getTarifasSueldos':
        getTarifasSueldos();
    break;
    case 'getDescuentosPersonales':
        getDescuentosPersonales();
    break;
    case 'addPoliza':
        addPoliza();
    break;
    case 'addPolizaComplementaria':
        addPolizaComplementaria();
    break;
    case 'siGeneraPoliza':
        siGeneraPoliza();
    break;
    case 'sumaFacturas':
        sumaFacturas();
    break;
    case 'addViajeTmp':
        addViajeTmp();
    break;
    case 'dltViajeTmp':
        dltViajeTmp();
    break;
    case 'getOdometro':
        getOdometro();
    break;
    case 'montoLavada':
        montoLavada();
    break;
    case 'polizaComplemento':
        polizaComplemento();
    break;
    case 'GeneraComplemento':
        GeneraComplemento();
    break;
    case 'getFolioParcial':
        getFolioParcial();
    break;
    case 'folioActual':
        folioActual();
    break;
    case 'getTicketCard':
        getTicketCard();
    break;
    case 'getDieselExtra':
        getDieselExtra();
    break;
    case 'getMontosDiesel':
        getMontosDiesel();
    break;
    case 'updConceptoTicket':
        updConceptoTicket();
    break;
    case 'sumImportes':
        sumImportes();
    break;
    case 'GeneraComplemento':
        GeneraComplemento();
    break;
    case 'addAnticipoPoliza':
        addAnticipoPoliza();
    break;
    case 'addRfcPeaje':
        addRfcPeaje();
    break;
    case 'getTalonesVeracruzMacheteros':
        getTalonesVeracruzMacheteros();
    break;
    default:
        echo 'no esta entrando a nada';
}

function addAnticipoPoliza()
{
    $addAnticipo = "INSERT INTO trgastosviajetractortbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, numeroTarjeta, litros, usuario, ip, subTotal, iva)
                        SELECT idViajeTractor, concepto, '" . $_REQUEST['polizaGastosCenDisHdn'] . "' as centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, '" . $_REQUEST['polizaGastosAnticipoNumAnticipo'] . "' AS importe, observaciones, claveMovimiento, numeroTarjeta, litros, usuario, ip, subTotal, iva
                        FROM trgastosviajetractortbl
                        WHERE idViajeTractor = '" . $_REQUEST['polizaGastosIdViajeHdn'] . "'
                        AND concepto = 7001
                        LIMIT 1";

    $rsSqlParcial = fn_ejecuta_query($addAnticipo);
    echo json_encode($rsSqlParcial);
}

function folioActual()
{
    $folioactual = "SELECT SUM(folio + 1) AS folio FROM trfoliostbl WHERE tipoDocumento = 'PZ' AND centroDistribucion = '" . $_SESSION['usuCompania'] . "';";

    $rsSqlParcial = fn_ejecuta_query($folioactual);
    echo json_encode($rsSqlParcial);

    $updFoliador = "UPDATE trfoliostbl " . "SET folio = '" . $rsSqlParcial['root'][0]['folio'] . "' " . "WHERE tipoDocumento = 'PZ' " . "AND centroDistribucion = '" . $_SESSION['usuCompania'] . "';";

    fn_ejecuta_query($updFoliador);
}

function sumImportes()
{

    $sumFolios = "SELECT (sum(g2.importe) - (SELECT g1.importe FROM trgastosviajetractortbl g1 WHERE g1.idViajeTractor = g2.idViajeTractor AND g1.concepto = '2222')) as importe " . "FROM trgastosviajetractortbl g2 " . "WHERE g2.idViajeTractor = '" . $_REQUEST['trViajesTractoresIdViajeHdn'] . "' " . "AND g2.concepto = '7001' " . "AND g2.claveMovimiento != 'GX';";

    $rsSumFolios = fn_ejecuta_query($sumFolios);
    echo json_encode($rsSumFolios);
}

function getDatosPoliza()
{

    $sqlGetDatosPoliza = "SELECT (SELECT cc.importe FROM caConceptosCentrosTbl cc " . "WHERE cc.centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND cc.concepto = " . $_REQUEST['trPolizaGastosConceptoHdn'] . ") AS importe";

    $rs = fn_ejecuta_query($sqlGetDatosPoliza);

    echo json_encode($rs);
}

function getFolioParcial()
{

    $sqlGetParcial = "SELECT sum(FOLIO +1) as folio, concat((SELECT LPAD((SELECT SUM(month(now())) - 1),2,0)),'0001') as anteriorFolio " . "FROM trgastosviajetractortbl " . "WHERE  claveMovimiento = 'GP' " . "AND FOLIO LIKE (SELECT LPAD((SELECT SUM(month(now())) - 1),2,0)) " . "AND YEAR(fechaEvento) = YEAR(NOW()) " . "LIMIT 1;";

    $rsSqlParcial = fn_ejecuta_query($sqlGetParcial);

    if ($rsSqlParcial['root'][0]['folio'] == '')
    {
        $updFolioParcial = "UPDATE trGastosViajeTractorTbl " . "SET folio = '" . $rsSqlParcial['root'][0]['anteriorFolio'] . "' " . "WHERE claveMovimiento = 'GP' " . "AND idViajeTractor = '" . $_REQUEST['trViajesTractoresIdViajeHdn'] . "'";

        fn_ejecuta_query($updFolioParcial);

        $updFobs = "UPDATE trObservacionesViajeTbl " . "SET folio = '" . $rsSqlParcial['root'][0]['anteriorFolio'] . "' " . "WHERE idViajeTractor = '" . $_REQUEST['trViajesTractoresIdViajeHdn'] . "'";
        fn_ejecuta_query($updFobs);

        echo json_encode($rsSqlParcial);
    }
    else
    {
        $FolioAnterior = $rsSqlParcial['root'][0]['folio'] + 1;
        $updFolioParcial = "UPDATE trGastosViajeTractorTbl " . "SET folio = '" . $FolioAnterio . "' " . "WHERE claveMovimiento = 'GP' " . "AND idViajeTractor = '" . $_REQUEST['trViajesTractoresIdViajeHdn'] . "'";

        fn_ejecuta_query($updFolioParcial);

        $updFobs = "UPDATE trObservacionesViajeTbl SET folio = '" . $FolioAnterio . "' " . "WHERE idViajeTractor = '" . $_REQUEST['trViajesTractoresIdViajeHdn'] . "'";
        fn_ejecuta_query($updFobs);

        echo json_encode($rsSqlParcial);
    }
}


function getAnticipos()
{
    $lsWhereStr = "WHERE gt.claveMovimiento != 'GX' ";

    if ($gb_error_filtro == 0)
    {
        $lsCondicionStr = fn_construct($_REQUEST['trPolizaGastosIdViajeHdn'], "gt.idViajeTractor", 0);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    }
    if ($gb_error_filtro == 0)
    {
        $lsCondicionStr = fn_construct($_REQUEST['trPolizaGastosCentroDistribucionHdn'], "gt.centroDistribucion", 1);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    }
    if ($gb_error_filtro == 0)
    {
        $lsCondicionStr = fn_construct($_REQUEST['trPolizaGastosConceptoHdn'], "gt.concepto", 1);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    }

    $sqlGetAnticiposStr = "SELECT gt.concepto, gt.centroDistribucion, gt.folio, gt.importe " . "FROM trGastosViajeTractorTbl  gt " . $lsWhereStr;

    $rs = fn_ejecuta_query($sqlGetAnticiposStr);

    echo json_encode($rs);
}

function getDatosComprobacion()
{
    $lsWhereStr = "WHERE ge.tabla = 'comprobacionPoliza' " . "AND ge.columna = cc.centroDistribucion " . "AND ge.valor = cc.concepto " . "AND cc.concepto = co.concepto ";

    if ($gb_error_filtro == 0)
    {
        $lsCondicionStr = fn_construct($_REQUEST['trPolizaGastosCentroDistribucionHdn'], "cc.centroDistribucion", 1);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    }
    if ($gb_error_filtro == 0)
    {
        $lsCondicionStr = fn_construct($_REQUEST['trPolizaGastosConceptoHdn'], "gt.concepto", 1);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    }

    /*$sqlGetDatosComprobacion = "SELECT cc.concepto, cc.cuentaContable, co.nombre ".
                                    "FROM caGeneralesTbl ge, caConceptosCentrosTbl cc, caConceptosTbl co ".$lsWhereStr.
                                    "ORDER BY ge.estatus";
    
        $rs = fn_ejecuta_query($sqlGetDatosComprobacion);*/

    $sqlGetDatosComprobacion = "SELECT cc.concepto,co.nombre, " . "concat(SUBSTRING(cc.cuentaContable, 1,13),'.',LPAD((SELECT ta.tractor " . "FROM catractorestbl ta " . "WHERE idTractor = (SELECT vt.idTractor " . "FROM trviajestractorestbl vt " . "WHERE vt.idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . ")),4,0),'.',LPAD((SELECT vt.claveChofer " . "FROM trviajestractorestbl vt " . "WHERE vt.idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . "),5,0)) as cuentaContable  " . "FROM caGeneralesTbl ge, caConceptosCentrosTbl cc, caConceptosTbl co " . $lsWhereStr . "ORDER BY ge.estatus;";

    $rs = fn_ejecuta_query($sqlGetDatosComprobacion);

    ///////////////////////busca macheteros autorizados
                $sqlSumEst="SELECT sum(cantidad) as totGrat
                    from autorizacionesespecialestbl
                    where idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']. "
                    and tipoAutorizacion='TAXIS'
                    and estatus='autorizado';";
                $rsSum=fn_ejecuta_query($sqlSumEst);

                $sqlSumEst1="SELECT sum(numeroUnidades) from trtalonesviajestbl
                            where idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']. "
                            and claveMovimiento='TE';";
                $rsSum1=fn_ejecuta_query($sqlSumEst1);

                


                $valTax=$rsSum['root'][0]['totGrat'] * 55;
                //echo $valMach;

                /////////////////////////////////////////

    for ($i = 0;$i < sizeof($rs['root']);$i++)
    {
        $rs['root'][$i]['subtotal'] = '0.00';
        $rs['root'][$i]['iva'] = '0.00';
        $rs['root'][$i]['total'] = '0.00';

        if ($rs['root'][$i]['nombre']=='TAXIS') {
            $rs['root'][$i]['subtotal'] = $valTax;
            $rs['root'][$i]['total'] = $valTax;
        }
    }


    $sqlGetAnticipos = "SELECT gv.concepto, gv.importe " . "FROM trGastosViajeTractorTbl gv " . "WHERE gv.idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND (gv.claveMovimiento = 'GP' OR gv.claveMovimiento = 'GM') ";

    $rsAnticipo = fn_ejecuta_query($sqlGetAnticipos);
    $anticiposArr = array();

    for ($i = 0;$i < sizeof($rsAnticipo['root']);$i++)
    {
        if (!isset($anticiposArr[$rsAnticipo['root'][$i]['concepto']]))
        {
            // /array_push($anticiposArr, $rsAnticipo[$i]['concepto']=>$rsAnticipo[$i]['importe']);
            $anticiposArr[$rsAnticipo['root'][$i]['concepto']] = $rsAnticipo['root'][$i]['importe'];
        }
        else
        {
            $anticiposArr[$rsAnticipo['root'][$i]['concepto']] += $rsAnticipo['root'][$i]['importe'];
        }
    }

    for ($i = 0;$i < sizeof($rs['root']);$i++)
    {
        if (isset($anticiposArr[$rs['root'][$i]['concepto']])) $rs['root'][$i]['anticipo'] = $anticiposArr[$rs['root'][$i]['concepto']];
        else $rs['root'][$i]['anticipo'] = '0.00';
    }

    echo json_encode($rs);
}

function getFacturasPoliza()
{
    $lsWhereStr = "";

    if ($gb_error_filtro == 0)
    {
        $lsCondicionStr = fn_construct($_REQUEST['trPolizaGastosFolioFiscalHdn'], "fp.folioFiscal", 2);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    }
    if (isset($_REQUEST['trPolizaGastosFolioRFCHdn']))
    {
        $lsCondicionStr = fn_construct($_REQUEST['trPolizaGastosFolioRFCHdn'], "fp.rfc", 1);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    }

    $sqlGetFacturasStr = "SELECT fp.* " . "FROM trPolizaFacturasTbl fp " . $lsWhereStr;

    $rs = fn_ejecuta_query($sqlGetFacturasStr);

    //echo json_encode(sizeof($rs['root'][0]['estatus']));
    

    if (sizeof($rs['root']) >= 1 && sizeof($rs['root'][0]['estatus']) ==0) {
        echo json_encode($rs);
    }else IF(sizeof($rs['root']) >= 1 && $rs['root'][0]['estatus'] =='AUTORIZADO') {
        $sqlValida="SELECT * FROM trPolizaFacturasTbl WHERE folioFiscal='".$_REQUEST['trPolizaGastosFolioFiscalHdn']."' AND estatus='PRECARGADA' ";
        $rs=fn_ejecuta_query($sqlValida);

        echo json_encode($rs);
    }else IF(sizeof($rs['root']) >= 1 && $rs['root'][0]['estatus'] =='PRECARGADA') {
        $sqlValida="SELECT  * from trpolizafacturastbl a, trviajestractorestbl b
                    where a.idViajeTractor=".$rs['root'][0]['idViajeTractor']."
                    AND a.idviajetractor=b.idviajetractor
                    AND a.folioFiscal='".$_REQUEST['trPolizaGastosFolioFiscalHdn']."'
                    AND b.clavemovimiento='VP';";        
        $rs=fn_ejecuta_query($sqlValida);
        echo json_encode($rs);
    }else{
        echo json_encode($rs);
    }

}

function getRfcPeajes()
{
    $lsWhereStr = "";

    if ($gb_error_filtro == 0)
    {
        $lsCondicionStr = fn_construct($_REQUEST['trPolizaGastosRfcHdn'], "rp.rfc", 1);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
    }

    $sqlGetRfc = "SELECT rp.* FROM caRfcPeajesTbl rp " . $lsWhereStr;
    $rs = fn_ejecuta_query($sqlGetRfc);

    echo json_encode($rs);
}

function updCombustible()
{
    $fecha = date("Y-m-d H:i:s");
    $a = array(
        'success' => true
    );
    $sqlDelTr = "DELETE FROM trGastosViajeTractorTbl " . "WHERE idViajeTractor = " . $_REQUEST['idViajeTractor'] . " " . "AND concepto = '" . $_REQUEST['concepto'] . "' " . "AND centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND claveMovimiento = 'GD'";
    fn_ejecuta_query($sqlDelTr);
    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
    {
        $a['success'] = false;
        $a['msjResponse'] = $_SESSION['error_sql'];
        $a['sql'] = $sqlDelTr;
    }
    if ($a['success'])
    {
        $insStr = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, litros, usuario, ip, subTotal, iva) VALUES (" . $_REQUEST['idViajeTractor'] . "," . "'" . $_REQUEST['concepto'] . "'," . "'" . $_SESSION['usuCompania'] . "'," . "'00000'," . //upd set folio: when ins
        "NOW()," . " (SELECT cuentaContable FROM caconceptoscentrostbl WHERE centroDistribucion = 'CDTOL' AND concepto = '" . $_REQUEST['concepto'] . "')," . "'" . $_REQUEST['mesAfectacion'] . "'," . "0," . "'" . $_REQUEST['idCombustible'] . "'," . "'GD'," . floatval($_REQUEST['litros']) . "," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . "0," . "0);";
        fn_ejecuta_query($insStr);
        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
        {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
        }
    }
    echo json_encode($a);
}

function addFacturaGastos()
{
    $rsLavada = array(
        'success' => true
    );
    if (!isset($_REQUEST['opcionHdn']))
    {
        $_REQUEST['opcionHdn'] = '3';
    }

    switch ($_REQUEST['opcionHdn'])
    {
        case '1': //obten numLavadas
            $sqlCheckLavadaMes = "SELECT COUNT(DISTINCT pf.concepto) AS numLavadas " . "FROM trpolizafacturastbl pf, trviajestractorestbl vt " . "WHERE pf.idViajeTractor = vt.idViajeTractor AND vt.idTractor = (SELECT idTractor FROM trviajesTractoresTbl WHERE idViajeTractor = " . $_REQUEST['idViajeTractorHdn'] . ") " . "AND SUBSTRING(pf.fechaEvento,1,7) = '" . substr($_REQUEST['fechaViajeHdn'], 0, 7) . "' " . "AND pf.lavada = '1'";
            $rsLavada = fn_ejecuta_query($sqlCheckLavadaMes);
            $rsLavada['numLavadas'] = floatval($rsLavada['root'][0]['numLavadas']);
            $rsLavada['lavadaMesViajeA'] = 0;
            if ($rsLavada['numLavadas'] == 0) {
                // substr($_REQUEST['fechaViajeHdn'], 0, 1)
                // if (condition) {
                //     # code...
                // }
            } elseif ($rsLavada['numLavadas'] == 1)
            { //obten inf. lavada
                $rsLavada['lavViajeDif'] = 0;
                $sqlCheckLavadaMes = "SELECT pf.idViajeTractor, pf.concepto " . "FROM trpolizafacturastbl pf, trviajestractorestbl vt " . "WHERE pf.idViajeTractor = vt.idViajeTractor AND vt.idTractor = (SELECT idTractor FROM trviajesTractoresTbl WHERE idViajeTractor = " . $_REQUEST['idViajeTractorHdn'] . ") " . "AND SUBSTRING(pf.fechaEvento,1,7) = '" . substr($_REQUEST['fechaViajeHdn'], 0, 7) . "' " . "AND pf.lavada = '1'";
                $rsViaje = fn_ejecuta_query($sqlCheckLavadaMes);
                if ($rsViaje['records'] > 0)
                {
                    if ($rsViaje['root'][0]['idViajeTractor'] != $_REQUEST['idViajeTractorHdn'])
                    {
                        $rsLavada['lavViajeDif'] = 1;
                        $lsWhereStr = "WHERE idViajeTractor = " . $rsViaje['root'][0]['idViajeTractor'] . " " . "AND claveMovimiento = 'GP'";
                        $rsInfLavada = fn_ejecuta_query("SELECT DISTINCT folio, centroDistribucion, fechaEvento FROM trgastosviajetractortbl " . $lsWhereStr);
                        $rsLavada['folio'] = $rsInfLavada['root'][0]['folio'];
                        $rsLavada['centroDistribucion'] = $rsInfLavada['root'][0]['centroDistribucion'];
                        $rsLavada['fechaEvento'] = $rsInfLavada['root'][0]['fechaEvento'];
                    }
                    else
                    {
                        switch ($rsViaje['root'][0]['concepto'])
                        {
                            case '2342':
                                $rsLavada['concepto'] = 'ALIMENTOS';
                            break;
                            case '6011':
                                $rsLavada['concepto'] = 'ACEITE';
                            break;
                            case '7004':
                                $rsLavada['concepto'] = 'GASOLINA';
                            break;
                        }
                    }
                }
            }
        break;
        case '2': //obten concepto
            if (isset($_REQUEST['conceptoHdn']) && $_REQUEST['conceptoHdn'] == '6002') {
                //MACHETEROS
                $rsLavada['root'] = array();
                $rsLavada['records'] = 0;
                $lsWhereStr = "WHERE idViajeTractor = ".$_REQUEST['idViajeTractorHdn']." ".
                              "AND concepto = '6002' ".
                              "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND folio = 'PM001'";
                $rsMacheteros = fn_ejecuta_query("SELECT * FROM trComplementosConceptoTbl " . $lsWhereStr);
///////////////////////busca macheteros autorizados
                $sqlSumEst="SELECT sum(cantidad) as totGrat
                    from autorizacionesespecialestbl
                    where idViajeTractor=".$_REQUEST['idViajeTractorHdn']. "
                    and tipoAutorizacion='MACHETEROS'
                    and estatus='autorizado';";
                $rsSum=fn_ejecuta_query($sqlSumEst);


                $valMach=$rsSum['root'][0]['totGrat'] * 45;
                //echo $valMach;

                /////////////////////////////////////////

                if ($rsMacheteros['records'] >= 0) {                                        

                    $arrMacheteros = $rsMacheteros['root'];
                    $totMacheteros = $rsMacheteros['records'] -1;
                    $sec = 0;

                    for ($idx=0; $idx < $totMacheteros; $idx++) { 

                        $row = $arrMacheteros[$idx];
                        $parametrosArr = explode('|', $row['parametros']);
                        $rowMacheteros = array('cantidad' => $parametrosArr[0], 'concepto' => $parametrosArr[1], 'importe' => $parametrosArr[2]);

                        if ($parametrosArr[1]=='CARGOS EXTRA AUTORIZADOS') {
                            $rowMacheteros = array('cantidad' => $parametrosArr[0], 'concepto' => $parametrosArr[1], 'importe' => $valMach);
                        }


                        $rsLavada['root'][] = $rowMacheteros;
                    }
                    $rsLavada['records'] = sizeof($rsLavada['root']);
                }
            } else {
                $lsWhereStr = "WHERE idViajeTractor = " . $_REQUEST['idViajeTractorHdn'] . " ";
                $lsCondicionStr = fn_construct($_REQUEST['conceptoHdn'], "concepto", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
                if (isset($_REQUEST['mesAfectacionHdn']) && !empty($_REQUEST['mesAfectacionHdn']))
                {
                    $lsCondicionStr = fn_construct($_REQUEST['mesAfectacionHdn'], "mesAfectacion", 1);
                    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
                }
                $rsLavada = fn_ejecuta_query("SELECT * FROM trpolizafacturastbl " . $lsWhereStr);
            }

        break;
        case '3': //insert concepto
            if (isset($_REQUEST['trPolizaGastosConceptoHdn']) && $_REQUEST['trPolizaGastosConceptoHdn'] == '6002') {
                //MACHETEROS
                $delStr = "DELETE FROM trComplementosConceptoTbl ".
                          "WHERE idViajeTractor = ".$_REQUEST['trPolizaGastosIdViajeHdn']." ".
                          "AND concepto = '6002' ".
                          "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                          "AND folio = 'PM001'";
                fn_ejecuta_query($delStr);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $rsLavada['success'] = false;
                    $rsLavada['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $delStr;
                } else {
                    if ($_REQUEST['total'] > 0) {
                        $arrMacheteros = json_decode($_POST['arrMacheteros'],true);
                        $totMacheteros = sizeof($arrMacheteros) +1;
                        $sec = 0;
                        for ($idx=0; $idx < $totMacheteros; $idx++) { 
                            if ($idx == ($totMacheteros -1)) {
                                $parametros = $_REQUEST['claveChofer']."|".$_REQUEST['nombreChofer']."|".$_REQUEST['unidad']."|".$_REQUEST['macheteros']."|".$_REQUEST['taxi']."|".$_REQUEST['total'];
                            } else {
                                $row = $arrMacheteros[$idx];
                                $parametros = $row['cantidad']."|".$row['concepto']."|".$row['importe'];
                            }
                            $sec++;
                            $insStr = "INSERT INTO trComplementosConceptoTbl (idViajeTractor, concepto, centroDistribucion, folio, secuencia, parametros, fecha, idUsuario) VALUES (" . 
                                      $_REQUEST['trPolizaGastosIdViajeHdn'].", ".
                                      "'6002', ".
                                      "'".$_SESSION['usuCompania']."', ".
                                      "'PM001', ".
                                      $sec.", ".
                                      "'${parametros}', ".
                                      "NOW(), ".
                                      $_SESSION['idUsuario'].")";
                            fn_ejecuta_query($insStr);
                            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                                $rsLavada['success'] = false;
                                $rsLavada['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                                break;
                            }
                        }
                    }
                }
            } else {
                if ($_REQUEST['trPolizaGastosFechaHdn'] == null)
                {
                    $_REQUEST['trPolizaGastosFechaHdn'] = date("Y-m-d H:i:s");
                }
                else
                {
                    if (!empty($_REQUEST['trPolizaGastosFechaHdn']))
                    {
                        $_REQUEST['trPolizaGastosFechaHdn'] = date('Y-m-d H:i:s', strtotime($_REQUEST['trPolizaGastosFechaHdn']));
                    }
                }
                if (!isset($_REQUEST['lavadaHdn']))
                {
                    $_REQUEST['lavadaHdn'] = '';
                }
                if (!empty($_REQUEST['trPolizaGastosObservacionesHdn']))
                {
                    $_REQUEST['trPolizaGastosObservacionesHdn'] = strtoupper($_REQUEST['trPolizaGastosObservacionesHdn']);
                }

                $sqlValidaFactura="SELECT * FROM trpolizafacturastbl where folioFiscal='".$_REQUEST['trPolizaGastosFolioFiscalHdn']."'";
                $rsFactura=fn_ejecuta_query($sqlValidaFactura);
                
                //echo sizeof($rsFactura['root'][0]['folioFiscal']);
                if (sizeof($rsFactura['root'][0]['folioFiscal'])>=1) {

                    $updFactura="UPDATE trPolizaFacturasTbl set concepto='".$_REQUEST['trPolizaGastosConceptoHdn']."', iva='".$_REQUEST['trPolizaGastosIvaHdn']."', subtotal='".$_REQUEST['trPolizaGastosSubtotalHdn']."', total='".$_REQUEST['trPolizaGastosTotalHdn']."', estatus='PRECARGADA' where folioFiscal='".$_REQUEST['trPolizaGastosFolioFiscalHdn']."' ";
                    fn_ejecuta_query($updFactura);

                     if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == ""))
                        {
                            $rsLavada['successMessage'] = getFacturaGastosSuccessMsg();
                        }
                        else
                        {
                            $rsLavada['success'] = false;
                            $rsLavada['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $updFactura;
                        }
                }else{
                    $sqlAddFacturaStr = "INSERT INTO trPolizaFacturasTbl (idViajeTractor, concepto, rfc, folio, folioFiscal, " . "subtotal, iva, total, voucher, mesAfectacion, fechaEvento, observaciones, diferencia, idUsuario, lavada) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'" . $_REQUEST['trPolizaGastosConceptoHdn'] . "'," . "'" . $_REQUEST['trPolizaGastosRFCHdn'] . "'," . "'" . $_REQUEST['trPolizaGastosFolioHdn'] . "'," . "'" . $_REQUEST['trPolizaGastosFolioFiscalHdn'] . "'," . $_REQUEST['trPolizaGastosSubtotalHdn'] . "," . $_REQUEST['trPolizaGastosIvaHdn'] . "," . $_REQUEST['trPolizaGastosTotalHdn'] . "," . replaceEmptyNull("'" . $_REQUEST['trPolizaGastosVoucherHdn'] . "'") . "," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . "'" . $_REQUEST['trPolizaGastosFechaHdn'] . "'," . replaceEmptyNull("'" . $_REQUEST['trPolizaGastosObservacionesHdn'] . "'") . "," . replaceEmptyNull("'" . $_REQUEST['trPolizaGastosDiferenciaHdn'] . "'") . "," . $_SESSION['idUsuario'] . "," . replaceEmptyNull("'" . $_REQUEST['lavadaHdn'] . "'") . ")";
                    fn_ejecuta_query($sqlAddFacturaStr);

                     if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == ""))
                    {
                        $rsLavada['successMessage'] = getFacturaGastosSuccessMsg();
                    }
                    else
                    {
                        $rsLavada['success'] = false;
                        $rsLavada['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddFacturaStr;
                    }
    
                }

               
            }
        break;
    }
    echo json_encode($rsLavada);
}

function dltFacturasViaje()
{
    $a = array();
    $e = array();
    $a['success'] = true;

    if ($a['success'])
    {
        $sqlValida = "UPDATE trPolizaFacturasTbl set estatus='AUTORIZADO' " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND estatus='PRECARGADA' AND mesAfectacion = '" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'";
        fn_ejecuta_query($sqlValida);

        

        $sqlDltFacturasStr = "DELETE FROM trPolizaFacturasTbl " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND estatus is null AND mesAfectacion = '" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'";

        fn_ejecuta_query($sqlDltFacturasStr);

        if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == ""))
        {
            $a['successMessage'] = getFacturaGastosDltMsg();
        }
        else
        {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltFacturasStr;
        }

        if ($a['success']) {
            $delStr = "DELETE FROM trComplementosConceptoTbl ".
                      "WHERE idViajeTractor = ".$_REQUEST['trPolizaGastosIdViajeHdn']." ".
                      "AND concepto = '6002' ".
                      "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                      "AND folio = 'PM001'";
            fn_ejecuta_query($delStr);
            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $delStr;
            }
        }
    }

    $a['errors'] = $e;
    echo json_encode($a);
}

function dltFacturaGastos()
{
    $a = array();
    $e = array();
    $a['success'] = true;

    if ($_REQUEST['trPolizaGastosFolioFiscalHdn'] == "")
    {
        $e[] = array(
            'id' => 'trPolizaGastosFolioFiscalHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }

    //VOUCHER NO REQUERIDO
    if ($a['success'])
    {
        $sqlDltFacturaStr = "DELETE FROM trPolizaFacturasTbl " . "WHERE folioFiscal = '" . $_REQUEST['trPolizaGastosFolioFiscalHdn'] . "'";
        //$sqlDltFacturaStr = "UPDATE  trPolizaFacturasTbl SET estatus='AUTORIZADO' " . "WHERE folioFiscal = '" . $_REQUEST['trPolizaGastosFolioFiscalHdn'] . "'";

        fn_ejecuta_query($sqlDltFacturaStr);

        if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == ""))
        {
            $a['successMessage'] = getFacturaGastosDltMsg();
        }
        else
        {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltFacturaStr;
        }
    }

    $a['errors'] = $e;
    echo json_encode($a);
}

function revisarLavadaMes()
{
    $sqlCheckLavadaMes = "SELECT 1 AS lavadaMes FROM trpolizafacturastbl pf, trviajestractorestbl vt " . "WHERE pf.idViajeTractor = vt.idViajeTractor AND vt.claveChofer = ( " . "SELECT vt2.claveChofer FROM trviajestractorestbl vt2 " . "WHERE vt2.idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . ") " . "AND pf.fechaEvento LIKE '" . date("Y-m") . "%' ";

    $rs = fn_ejecuta_query($sqlCheckLavadaMes);

    echo json_encode($rs);
}

function getTarifasSueldos()
{
    $fondoAhorro = 0.00;

    $where = "WHERE cc.concepto = co.concepto " . "AND cc.centroDistribucion = ( SELECT ch.centroDistribucionOrigen FROM caChoferesTbl ch " . "WHERE ch.claveChofer = " . $_REQUEST['polizaSueldosClaveChoferHdn'] . ") " . "AND co.tipoConcepto != 'X' " . "AND co.estatus = 1 ";

    if ($gb_error_filtro == 0)
    {
        $condicion = fn_construct($_REQUEST['polizaSueldosTipoHdn'], "co.tipoConcepto", 1);
        $where = fn_concatena_condicion($where, $condicion);
    }

    $sqlGetTarifas = " SELECT co.concepto, co.nombre, co.tipoConcepto, cc.cuentaContable, cc.importe, " . "(SELECT dc.sueldoGarantizado FROM caDistribuidoresCentrosTbl dc " . "WHERE dc.distribuidorCentro = ( SELECT ch.centroDistribucionOrigen FROM caChoferesTbl ch " . "WHERE ch.claveChofer = " . $_REQUEST['polizaSueldosClaveChoferHdn'] . ")) AS sueldoGarantizado, " . "(SELECT calculoIstp FROM caChoferesTbl WHERE claveChofer = " . $_REQUEST['polizaSueldosClaveChoferHdn'] . ") as calculoIstp " . "FROM caconceptoscentrostbl cc, caconceptostbl co " . $where;

    $rs = fn_ejecuta_query($sqlGetTarifas);

    $sqlGetSumaFondoAhorro = "SELECT sv.importe FROM roSueldosViajesChoferTbl sv, trViajesTractoresTbl vt " . "WHERE sv.idViajeTractor = vt.idViajeTractor " . "AND vt.claveChofer = " . $_REQUEST['polizaSueldosClaveChoferHdn'] . " " . "AND MONTH(sv.fechaEvento) = '" . date("m") . "'";

    $rsFondo = fn_ejecuta_query($sqlGetSumaFondoAhorro);

    for ($i = 0;$i < sizeof($rsFondo['root']);$i++)
    {
        $fondoAhorro += floatval($rsFondo['root'][$i]['importe']);
    }

    for ($i = 0;$i < sizeof($rs['root']);$i++)
    {
        $rs['root'][$i]['conceptoDesc'] = $rs['root'][$i]['concepto'] . " - " . $rs['root'][$i]['nombre'];
        $rs['root'][$i]['fondoAhorro'] = $fondoAhorro;
    }

    echo json_encode($rs);
}

function getDescuentosPersonales()
{
    date_default_timezone_set('America/Mexico_City');
    $a = array('success' => true, 'root' => array(), 'records' => 0);
    $ld_fechaActual = date('Y-m-d');
    $li_sueldoTotal = floatval($_REQUEST['sueldoTotal']);

    $where = "WHERE co.concepto = cc.concepto " . "AND dc.concepto = co.concepto " . "AND dc.centroDistribucion = cc.centroDistribucion " . "AND cc.centroDistribucion =  ( SELECT ch.centroDistribucionOrigen FROM caChoferesTbl ch " . "WHERE ch.claveChofer = " . $_REQUEST['polizaSueldosClaveChoferHdn'] . ") " . "AND dc.claveChofer = " . $_REQUEST['polizaSueldosClaveChoferHdn'] . " " . "AND dc.estatus = 'P'";

    $sqlGetDescuentos = "SELECT dc.idDescuento, co.concepto, co.nombre, cc.centroDistribucion, ".
        "CASE ".    
            "WHEN cc.concepto in (2230,2231,9032) THEN CONCAT(SUBSTR(cc.cuentaContable,1,14),(SELECT LPAD(ch1.cuentaContable,4,0) FROM cachoferestbl ch1 where ch1.claveChofer=" . $_REQUEST['polizaSueldosClaveChoferHdn'] . "))
                ELSE cc.cuentaContable ".
        "END  as cuentaContable, ".
        "dc.pagadoCobro, dc.tipoConcepto, dc.tipoImporte,dc.importe, dc.saldo, dc.estatus " . "FROM caConceptosTbl co, caConceptosCentrosTbl cc, roConceptosDescuentosChoferTbl dc " . $where;

    $rs = fn_ejecuta_query($sqlGetDescuentos);

    for ($i = 0;$i < $rs['records'];$i++)
    {
        $concValido = true;
        if ($rs['root'][$i]['concepto'] == '2227' && $rs['root'][$i]['tipoConcepto'] == 'A') {
            $concValido = false;
            $saldogeneraldeuda = 0;
            $adeudo = 0;
            if ($rs['root'][$i]['estatus'] == 'P') {
                $porcentajegeneral = floatval($rs['root'][$i]['tipoImporte']);

                $selStr = "SELECT * FROM roControlDescuentoPeriodoTbl ".
                          "WHERE idDescuento = ".$rs['root'][$i]['idDescuento']." ".
                          "AND '".date('Y-m-d H:i:s')."' BETWEEN fechaInicio AND fechaFinal";
                $ctrlTabRst = fn_ejecuta_query($selStr);
                $secuencia = intval($ctrlTabRst['root'][0]['secuencia']);

                $selStr = "SELECT * FROM roControlDescuentoPeriodoTbl ".
                          "WHERE idDescuento = ".$rs['root'][$i]['idDescuento']." ".
                          "AND secuencia <= ".$secuencia." ".
                          "ORDER BY secuencia ASC";
                $ctrlTabRst = fn_ejecuta_query($selStr);
                for ($j=0; $j < $ctrlTabRst['records']; $j++) { 
                    $saldogeneraldeuda += floatval($ctrlTabRst['root'][$j]['ctrlDescPeriodo']);
                } 

                if ($saldogeneraldeuda > 0) {
                    $concValido = true;
                    $adeudo = round($li_sueldoTotal * $porcentajegeneral / 100,2);
                    if ($adeudo > $saldogeneraldeuda) { 
                        $adeudo = $saldogeneraldeuda;
                    }
                }
            }
            $rs['root'][$i]['adeudo'] = $adeudo;
        }
        ///descuento gratificacion
        if ($rs['root'][$i]['concepto'] == '9032' && $rs['root'][$i]['tipoConcepto'] == 'P') {
            $concValido = false;
            $saldogeneraldeuda = 0;
            $adeudo = 0;
            if ($rs['root'][$i]['estatus'] == 'P') {
                $porcentajegeneral = floatval($rs['root'][$i]['tipoImporte']);

                $selStr = "SELECT * FROM roControlDescuentoPeriodoTbl ".
                          "WHERE idDescuento = ".$rs['root'][$i]['idDescuento']." ".
                          "AND '".date('Y-m-d H:i:s')."' BETWEEN fechaInicio AND fechaFinal";
                $ctrlTabRst = fn_ejecuta_query($selStr);
                $secuencia = intval($ctrlTabRst['root'][0]['secuencia']);

                $selStr = "SELECT * FROM roControlDescuentoPeriodoTbl ".
                          "WHERE idDescuento = ".$rs['root'][$i]['idDescuento']." ".
                          "AND secuencia <= ".$secuencia." ".
                          "ORDER BY secuencia ASC";
                $ctrlTabRst = fn_ejecuta_query($selStr);
                for ($j=0; $j < $ctrlTabRst['records']; $j++) { 
                    $saldogeneraldeuda += floatval($ctrlTabRst['root'][$j]['ctrlDescPeriodo']);
                } 

                if ($saldogeneraldeuda > 0) {
                    $concValido = true;
                    $adeudo = round($li_sueldoTotal * $porcentajegeneral / 100,2);
                    if ($adeudo > $saldogeneraldeuda) { 
                        $adeudo = $saldogeneraldeuda;
                    }
                }
            }
            $rs['root'][$i]['adeudo'] = $adeudo;
        }
        if ($rs['root'][$i]['concepto'] == '2228' && $rs['root'][$i]['tipoConcepto'] == 'A') {
            $concValido = false;
            $porcentajegeneral = 0;
            $saldogeneraldeuda = 0;
            $sumapagomensual = 0;
            $adeudo = 0;
            $params2228Str = '';

            $selStr = "SELECT a.* ".
                      "FROM roPrestamosPersonalesTbl a ".
                      "WHERE a.idDescuento = ".$rs['root'][$i]['idDescuento']." ".
                      "AND a.idEstatus = '1' ".
                      "ORDER BY a.secuencia ASC";
            $Rst = fn_ejecuta_query($selStr);
            if ($Rst['records'] > 0) {
                $porcentajegeneral = floatval($rs['root'][$i]['tipoImporte']);

                $selStr = "SELECT * FROM roControlDescuentoPeriodoTbl ".
                          "WHERE idDescuento = ".$rs['root'][$i]['idDescuento']." ".
                          "AND '".date('Y-m-d H:i:s')."' BETWEEN fechaInicio AND fechaFinal";
                $ctrlTabRst = fn_ejecuta_query($selStr);
                $secuencia = intval($ctrlTabRst['root'][0]['secuencia']);

                $selStr = "SELECT * FROM roControlDescuentoPeriodoTbl ".
                          "WHERE idDescuento = ".$rs['root'][$i]['idDescuento']." ".
                          "AND secuencia <= ".$secuencia." ".
                          "ORDER BY secuencia ASC";
                $ctrlTabRst = fn_ejecuta_query($selStr);
                for ($j=0; $j < $ctrlTabRst['records']; $j++) { 
                    $saldogeneraldeuda += floatval($ctrlTabRst['root'][$j]['ctrlDescPeriodo']);
                }

                if ($saldogeneraldeuda > 0) {
                    $concValido = true;
                    $arrPagoPeriodo = array_map(function ($rowPeriodo){return floatval($rowPeriodo['pagoPeriodo']);}, $Rst['root']);
                    $sumapagomensual = array_sum($arrPagoPeriodo);
                    $adeudoAux = round($li_sueldoTotal * $porcentajegeneral / 100,2);
                    if ($adeudoAux > $saldogeneraldeuda) {
                        $adeudoAux = $saldogeneraldeuda;
                    }

                    $sumaPorcentaje = 0;
                    $idxRec = 0;
                    foreach ((array) $Rst['root'] as $key => $row) {
                        $idxRec++;
                        $porcentaje = round(floatval($row['pagoPeriodo']) / $sumapagomensual,2);
                        if ($idxRec == $Rst['records']) {
                            $porcentaje = 1 - $sumaPorcentaje;
                        }
                        $adeudoPrestamo = round($adeudoAux * $porcentaje,2);

                        $selStr = "SELECT IFNULL(SUM(adeudo),0) AS sumpagosprestamo ".
                                  "FROM roPagosOperadorHistoricoTbl ".
                                  "WHERE idPrestamo = ".$row['idPrestamo']." ".
                                  "AND concepto = '2228' ".
                                  "AND claveMovimiento = 'PA'";
                        $histRst = fn_ejecuta_query($selStr);
                        $sumpagosprestamo = floatval($histRst['root'][0]['sumpagosprestamo']);
                        $deudaPrestamo = floatval($row['deudaTotal']) - $sumpagosprestamo;
                        if ($adeudoPrestamo > $deudaPrestamo) {
                            $adeudoPrestamo = $deudaPrestamo;
                        }
                        if ($params2228Str != '') {
                            $params2228Str .= ':';
                        }
                        $porcentajeAux = $porcentaje * 100;
                        $params2228Str .= $row['idPrestamo'].'|'.$porcentajeAux.'|'.$adeudoPrestamo;
                        $adeudo += $adeudoPrestamo;
                        $sumaPorcentaje += $porcentaje;
                    }
                }
            }
            $rs['root'][$i]['adeudo'] = $adeudo;
            $rs['root'][$i]['parametros'] = $params2228Str;
        }

        if ($concValido) {
            $rs['root'][$i]['conceptoDesc'] = $rs['root'][$i]['concepto'] . " - " . $rs['root'][$i]['nombre'];
            $a['root'][] = $rs['root'][$i];
        }
        $a['records'] = sizeof($a['root']);
    }

    echo json_encode($a);
}

function addPoliza()
{
    $a = array();
    $e = array();
    $a['success'] = true;

    $sqlAcompana="SELECT * FROM trViajesTractoresTbl WHERE idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn'];
    $rsAcompa=fn_ejecuta_query($sqlAcompana);    


    if ($rsAcompa['root'][0]['idViajePadre'] == null )
    {
        $_REQUEST['generaPolizaGtosHdn'] = 1;
    }else{
        $_REQUEST['generaPolizaGtosHdn'] = 0;
    }

    //CON ESTA CLAVE DE MOVIMIENTO SE GUARDARAN LOS DATOS EN LA TABLA
    $claveGastos = 'GP';
    $claveSueldos = 'GS';
    $claveDieselExtra = 'GD';



    /****************         POLIZA DE GASTOS         ******************/
    if ($_REQUEST['trPolizaGastosIdViajeHdn'] == "")
    {
        $e[] = array(
            'id' => 'trPolizaGastosIdViajeHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }
    if ($_SESSION['usuCompania'] == "")
    {
        $e[] = array(
            'id' => 'trPolizaGastosCentroDistHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }
    if ($_REQUEST['trPolizaGastosMesAfectacionHdn'] == "")
    {
        $e[] = array(
            'id' => 'trPolizaGastosMesAfectacionHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }
    if ($_REQUEST['trPolizaGastosParcialHdn'] == "")
    {
        $e[] = array(
            'id' => 'trPolizaGastosParcialHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }
    if ($_REQUEST['trPolizaGastosTaxiMacheterosHdn'] == "")
    {
        $e[] = array(
            'id' => 'trPolizaGastosTaxiMacheterosHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }

    $conceptoGastosArr = explode('|', substr($_REQUEST['trPolizaGastosConceptoHdn'], 0, -1));
    if (in_array('', $conceptoGastosArr))
    {
        $e[] = array(
            'id' => 'trPolizaGastosConceptoHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }
    $cuentaContArr = explode('|', substr($_REQUEST['trPolizaGastosCuentaContableHdn'], 0, -1));
    if (in_array('', $cuentaContArr))
    {
        $e[] = array(
            'id' => 'trPolizaGastosCuentaContableHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }

    $ticketCardArr = explode('|', substr($_REQUEST['trPolizaGastosTicketCardHdn'], 0, -1));

    $subtotalArr = explode('|', substr($_REQUEST['trPolizaGastosSubtotalHdn'], 0, -1));
    
    if (in_array('', $subtotalArr))
    {
        $e[] = array(
            'id' => 'trPolizaGastosSubtotalHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }
    $ivaArr = explode('|', substr($_REQUEST['trPolizaGastosIvaHdn'], 0, -1));
    if (in_array('', $ivaArr))
    {
        $e[] = array(
            'id' => 'trPolizaGastosIvaHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }
    $totalArr = explode('|', substr($_REQUEST['trPolizaGastosImporteHdn'], 0, -1));
    if (in_array('', $totalArr))
    {
        $e[] = array(
            'id' => 'trPolizaGastosImporteHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }
    if ($_REQUEST['trPolizaGastosKmPagadosHdn'] == "")
    {
        $e[] = array(
            'id' => 'trPolizaGastosKmPagadosHdn',
            'msg' => getRequerido()
        );
        $a['errorMessage'] = getErrorRequeridos();
        $a['success'] = false;
    }

    /*******************************************************************************/

    /*************************       POLIZA DE SUELDOS        **********************/
    if ($_REQUEST['generaPolizaGtosHdn'] == 1)
    {
        if ($_REQUEST['trPolizaSueldosCargosHdn'] == "")
        {
            $e[] = array(
                'id' => 'trPolizaSueldosCargosHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $descPersonalesArr = explode('|', substr($_REQUEST['trPolizaSueldosDescPersonalesHdn'], 0, -1));
        if (in_array('', $descPersonalesArr))
        {
            $e[] = array(
                'id' => 'trPolizaSueldosDescPersonalesHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $tipoConceptoArr = explode('|', substr($_REQUEST['trPolizaSueldosTipoConceptoHdn'], 0, -1));
        if (in_array('', $tipoConceptoArr))
        {
            $e[] = array(
                'id' => 'trPolizaSueldosTipoConceptoHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        // $tipoConceptoArr, $tipoImporteArr => tipoConcepto
        $importesPersonalesArr = explode('|', substr($_REQUEST['trPolizaSueldosImportesPersonalesHdn'], 0, -1));
        if (in_array('', $importesPersonalesArr))
        {
            $e[] = array(
                'id' => 'trPolizaSueldosImportesPersonalesHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $calculosPersonalesArr = explode('|', substr($_REQUEST['trPolizaSueldosCalculosPersonalesHdn'], 0, -1));
        if (in_array('', $calculosPersonalesArr))
        {
            $e[] = array(
                'id' => 'trPolizaSueldosCalculosPersonalesHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $saldosArr = explode('|', substr($_REQUEST['trPolizaSueldosSaldosHdn'], 0, -1));
        if (in_array('', $saldosArr))
        {
            $e[] = array(
                'id' => 'trPolizaSueldosSaldosHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $cuentaContSueldosArr = explode('|', substr($_REQUEST['trPolizaSueldosCuentaContableHdn'], 0, -1));
        if (in_array('', $cuentaContSueldosArr))
        {
            $e[] = array(
                'id' => 'trPolizaSueldosCuentaContableHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $tipoImporteArr = explode('|', substr($_REQUEST['trPolizaSueldostipoConceptosHdn'], 0, -1));
        if (in_array('', $tipoImporteArr))
        {
            $e[] = array(
                'id' => 'trPolizaSueldostipoConceptosHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        $sueldosPersonalesArr = explode('|', substr($_REQUEST['trPolizaSueldosPersonalHdn'], 0, -1));
        if (in_array('', $sueldosPersonalesArr))
        {
            $e[] = array(
                'id' => 'trPolizaSueldosPersonalHdn',
                'msg' => getRequerido()
            );
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
    }
    /********************************************************** ********************/

    $total = true;
    $folioLength = 4;

    $cuentaCargoOperador = "";
    $cuentaPagoEfectivo = "";

    /************* FOLIO ******************/
    $sqlGetNvoFolio = "SELECT  month(now()) as mes, folio " . "FROM trfoliostbl " . "WHERE tipoDocumento = 'PZ' " . "AND centroDistribucion = '" . $_SESSION['usuCompania'] . "';";

    $rsNvoFolio = fn_ejecuta_sql($sqlGetNvoFolio);

    $folioRellenado = sprintf('%06d', $rsNvoFolio['root'][0]['folio']);

    $mesFolio = substr($folioRellenado, 0, 2);

    // $valorSinCeros=str_replace("0", "", $rsNvoFolio['root'][0]['mesFolio']);
    

    if ($mesFolio == $rsNvoFolio['root'][0]['mes'])
    {
        $nvoFolio = $rsNvoFolio['root'][0]['folio'] + 1;
    }
    else
    {
        $nvoFolio = $rsNvoFolio['root'][0]['mes'] . "0001";
    }
    $nvoFolio= $_SESSION['usuCompania'].$nvoFolio;

    $sqlUpdF = "UPDATE trpolizafacturastbl " . "SET FOLIO = '" . $nvoFolio . "' " . "WHERE idViajeTractor ='" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "'  ";

    fn_ejecuta_sql($sqlUpdF);
    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
        $a['success'] = false;
        $a['errorMessage'] = $_SESSION['error_sql']. "<br>" . $sqlUpdF;
    } else {


   



        //macheteros
        $updStr = "UPDATE trComplementosConceptoTbl ".
                  "SET folio = '".$nvoFolio."' ".
                  "WHERE idViajeTractor = ".$_REQUEST['trPolizaGastosIdViajeHdn']." ".
                  "AND concepto = '6002' ".
                  "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                  "AND folio = 'PM001'";
        fn_ejecuta_sql($updStr);
        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $updStr;
        }
    }
    if ($a['success']) {
        $sqlUpdFolio = "UPDATE trfoliostbl " . "SET FOLIO = '" . substr($nvoFolio,5,20) . "' " . "WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND tipoDocumento = 'PZ'";

        fn_ejecuta_sql($sqlUpdFolio);
        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdFolio;
        }
    }

    $folio = $nvoFolio;
    /*********************************************************/

    //CARGO AL OPERADOR CHECK
    $sqlGetCuenta = "SELECT cuentaContable " . "FROM caConceptosCentrosTbl " . "WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND concepto = '7013'";

    $rsCuenta = fn_ejecuta_sql($sqlGetCuenta);

    if (sizeof($rsCuenta['root']) > 0)
    {
        $cuentaCargoOperador = $rsCuenta['root'][0]['cuentaContable'];
    }
    else
    {
        $a['success'] = false;
        $a['errorMessage'] = "No existe cuenta contable registrada " . "para el concepto de CARGO AL OPERADOR para " . $_SESSION['usuCompania'];
    }

    //PAGO EFECTIVO CHECK
    $sqlGetCuenta = "SELECT cuentaContable FROM caConceptosCentrosTbl " . "WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND concepto = '7014'";

    $rsCuenta = fn_ejecuta_sql($sqlGetCuenta);

    if (sizeof($rsCuenta['root']) > 0)
    {
        $cuentaPagoEfectivo = $rsCuenta['root'][0]['cuentaContable'];
    }
    else
    {
        $a['success'] = false;
        $a['errorMessage'] = "No existe cuenta contable registrada " . "para el concepto de PAGO EN EFECTIVO para " . $_SESSION['usuCompania'];
    }

    /***************************
        /   POLIZA DE GASTOS
    /***************************/
    if ($a['success'])
    {
        $fecha = date("Y-m-d H:i:s");
        $taxiExtra = 0.00;

        for ($i = 0;$i < sizeof($conceptoGastosArr);$i++)
        {

            /**
             ARREGLAR ESTO POR LO DE LOS CAMBIOS DE SUBTOTAL Y TOTAL
             *
             */

            //MACHETEROS QUITAR LOS TAXIS Y AGREGARLOS A TAXIS
            if ($conceptoGastosArr[$i] == '6002')
            {

                $sqlGetConceptoTaxi = "SELECT importe, " . "(SELECT importe FROM trGastosViajeTractorTbl WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND concepto = '6010') AS totalTaxi " . "FROM caConceptosCentrosTbl " . "WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND concepto = '" . $conceptoGastosArr[$i] . "'";

                $rsTaxi = fn_ejecuta_sql($sqlGetConceptoTaxi);

                if (sizeof($rsTaxi['root']) > 0)
                {
                    $taxiExtra = floatval($rsTaxi['root'][0]['totalTaxi']) + (floatval($rsTaxi['root'][0]['importe']) * intval($_REQUEST['trPolizaGastosTaxiMacheterosHdn']));

                    if ($taxiExtra > 0)
                    {
                        $sqlUpdTaxi = "UPDATE trGastosViajeTractorTbl SET importe = " . $taxiExtra . " " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND concepto = '6010'";

                        fn_ejecuta_sql($sqlUpdTaxi);
                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdTaxi;
                            break;
                        }
                    }
                }
            }
            //////////////////////////////////////////////////
            if (floatval($totalArr[$i]) != 0)
            {
                $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, subtotal, iva, importe, observaciones, claveMovimiento, usuario, ip) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'" . $conceptoGastosArr[$i] . "'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaContArr[$i] . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $subtotalArr[$i] . "," . $ivaArr[$i] . "," . $totalArr[$i] . ",''," . "'" . $claveGastos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "')";

                fn_ejecuta_sql($sqlAddPoliza);

                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                    break;
                }
            }
            if ($conceptoGastosArr[$i] == '2342') { //ALIMENTOS
                $actObservacion = floatval($_REQUEST['totDifAlimentos']);
                $insStr = "INSERT INTO tralimentospendientetbl (idViajeTractor, concepto, centroDistribucion, folio, facturadoDiferencia, claveMovimiento, idUsuario) VALUES (".
                          $_REQUEST['trPolizaGastosIdViajeHdn'].", ".
                          "'2342', ".
                          "'".$_SESSION['usuCompania']."', ".
                          "'".$folio . "', ".
                          $actObservacion.", ".
                          "'PA', ".
                          $_SESSION['idUsuario'].")";
                fn_ejecuta_sql($insStr);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                    break;
                }
            }

            if (floatval($totalArr[$i]) == 0 && $conceptoGastosArr[$i] == '6002') {
                //macheteros
                $updStr = "DELETE FROM trComplementosConceptoTbl ".
                          "WHERE idViajeTractor = ".$_REQUEST['trPolizaGastosIdViajeHdn']." ".
                          "AND concepto = '6002' ".
                          "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                          "AND folio = '".$folio."'";
                fn_ejecuta_sql($updStr);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $updStr;
                    break;
                }
            }
        }

        if ($a['success']) {
            for ($i = 0;$i < sizeof($ticketCardArr);$i++){
            
                $sqlUpdTicket ="UPDATE trpolizastckettbl set estatus='0', folio='". $folio ."', idViajeTractor='".$_REQUEST['trPolizaGastosIdViajeHdn']."' ".
                                "WHERE idTicket='".$ticketCardArr[$i]."' ";
                fn_ejecuta_sql($sqlUpdTicket);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdTicket;
                    break;
                }
            }
        }

        if ($a['success'] && isset($_REQUEST['trPolizaGastosDifCombustibleHdn']))
        {
            // COMBUSTIBLE: DESCUENTO EN BOMBAS Ó PAGO X POLIZA PEND
            if (floatval($_REQUEST['trPolizaGastosDifCombustibleHdn']) != 0)
            {
                if (floatval($_REQUEST['trPolizaGastosDifCombustibleHdn']) < 0)
                {
                    $_REQUEST['trPolizaGastosDifCombustibleHdn'] = str_replace('-', '', $_REQUEST['trPolizaGastosDifCombustibleHdn']);
                    $selStr = "SELECT * FROM trCombustiblePendienteTbl " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND claveChofer = " . $_REQUEST['trPolizaGastosChoferHdn'] . " " . "AND concepto = '9012'";
                    $combRs = fn_ejecuta_sql($selStr);
                    if ($combRs['records'] > 0)
                    {
                        $sqlUpdPdt = "UPDATE trCombustiblePendienteTbl SET litros = " . $_REQUEST['trPolizaGastosDifCombustibleHdn'] . ", claveMovimiento = 'CP' " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND claveChofer = " . $_REQUEST['trPolizaGastosChoferHdn'] . " " . "AND concepto = '9012'";
                        fn_ejecuta_sql($sqlUpdPdt);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'];
                            $a['sql'] = $sqlUpdPdt;
                        }
                    }
                    else
                    {
                        $insStr = "INSERT INTO trCombustiblePendienteTbl (idViajeTractor, claveChofer, litros, concepto, claveMovimiento) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . ", " . $_REQUEST['trPolizaGastosChoferHdn'] . ", " . $_REQUEST['trPolizaGastosDifCombustibleHdn'] . ", " . "'9012', " . "'CP')";
                        fn_ejecuta_sql($insStr);
                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                        }
                    }

                    if ($a['success'])
                    {
                        $insStr = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip, subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'9012'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaCargoOperador . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosDifCombustibleHdn']) . "," . "'DESCUENTO EN BOMBAS'," . "'GD'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . floatval($_REQUEST['trPolizaGastosDifCombustibleHdn']) . ")";
                        fn_ejecuta_sql($insStr);
                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                        }
                    }
                }
                else if (floatval($_REQUEST['trPolizaGastosDifCombustibleHdn']) > 0)
                {
                    $selStr = "SELECT * FROM trCombustiblePendienteTbl " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND claveChofer = " . $_REQUEST['trPolizaGastosChoferHdn'] . " " . "AND concepto = '9013'";
                    $combRs = fn_ejecuta_sql($selStr);
                    if ($combRs['records'] > 0)
                    {
                        $sqlUpdPdt = "UPDATE trCombustiblePendienteTbl SET litros = " . $_REQUEST['trPolizaGastosDifCombustibleHdn'] . ", claveMovimiento = 'CP' " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND claveChofer = " . $_REQUEST['trPolizaGastosChoferHdn'] . " " . "AND concepto = '9013'";
                        fn_ejecuta_sql($sqlUpdPdt);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'];
                            $a['sql'] = $sqlUpdPdt;
                        }
                    }
                    else
                    {
                        $insStr = "INSERT INTO trCombustiblePendienteTbl (idViajeTractor, claveChofer, litros, concepto, claveMovimiento) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . ", " . $_REQUEST['trPolizaGastosChoferHdn'] . ", " . $_REQUEST['trPolizaGastosDifCombustibleHdn'] . ", " . "'9013', " . "'CP')";
                        fn_ejecuta_sql($insStr);
                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                        }
                    }
                    if ($a['success'])
                    {
                        $insStr = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip, subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'9013'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaCargoOperador . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosDifCombustibleHdn']) . "," . "'PAGO X POLIZA PEND'," . "'GD'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . floatval($_REQUEST['trPolizaGastosDifCombustibleHdn']) . ")";
                        fn_ejecuta_sql($insStr);
                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $insStr;
                        }
                    }
                }
            }
        }

        if ($a['success']) {
            $ivaAcreditable = floatval($_REQUEST['ivaAcreditableHdn']);
            $sqlAddPolizaIa = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'7012'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'125.01401.001'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $ivaAcreditable . "," . "''," . "'" . $claveGastos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "', " . "'" . $ivaAcreditable . "')";
            fn_ejecuta_sql($sqlAddPolizaIa);
            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
            {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPolizaIa;
            } else {
                $conceptosAnticipos = "SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(cuentaContable,4,0) FROM cachoferestbl WHERE claveChofer = (SELECT claveChofer FROM trviajestractorestbl WHERE idViajeTractor =".$_REQUEST['trPolizaGastosIdViajeHdn']." ))) as cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.concepto ='7009' AND cc.centroDistribucion NOT IN('CDTSA','CDSFE','CDLCL','CDSLP') GROUP BY cc.concepto) as TolucaCCon, ".
                "(SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(cuentaContable,4,0) FROM cachoferestbl WHERE claveChofer = (SELECT claveChofer FROM trviajestractorestbl WHERE idViajeTractor =".$_REQUEST['trPolizaGastosIdViajeHdn']." ))) as cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.concepto ='7016' AND cc.centroDistribucion NOT IN('CDTSA','CDSFE','CDLCL','CDSLP') GROUP BY cc.concepto) as SaltilloCCon, ".
                "(SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(cuentaContable,4,0) FROM cachoferestbl WHERE claveChofer = (SELECT claveChofer FROM trviajestractorestbl WHERE idViajeTractor =".$_REQUEST['trPolizaGastosIdViajeHdn']." ))) as cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.concepto ='7015' AND cc.centroDistribucion NOT IN('CDTSA','CDSFE','CDLCL','CDSLP') GROUP BY cc.concepto) as AguascalientesCCon, ".
                "(SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(cuentaContable,4,0) FROM cachoferestbl WHERE claveChofer = (SELECT claveChofer FROM trviajestractorestbl WHERE idViajeTractor =".$_REQUEST['trPolizaGastosIdViajeHdn']." ))) as cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.concepto ='7023' AND cc.centroDistribucion NOT IN('CDTSA','CDSFE','CDLCL','CDSLP') GROUP BY cc.concepto) as SantaFeCCon, ".
                "(SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(cuentaContable,4,0) FROM cachoferestbl WHERE claveChofer = (SELECT claveChofer FROM trviajestractorestbl WHERE idViajeTractor =".$_REQUEST['trPolizaGastosIdViajeHdn']." ))) as cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.concepto ='7024' AND cc.centroDistribucion NOT IN('CDTSA','CDSFE','CDLCL','CDSLP') GROUP BY cc.concepto) as LazaroCCon ";

                $rsconceptosAnticipos= fn_ejecuta_sql($conceptosAnticipos);

                $cuentaTolAnt = $rsconceptosAnticipos['root'][0]['TolucaCCon'];
                $cuentaSalAnt = $rsconceptosAnticipos['root'][0]['SaltilloCCon'];
                $cuentaAgsAnt = $rsconceptosAnticipos['root'][0]['AguascalientesCCon'];
                $cuentaSfeAnt = $rsconceptosAnticipos['root'][0]['SantaFeCCon'];
                $cuentaLzcAnt = $rsconceptosAnticipos['root'][0]['LazaroCCon'];

                if ($_REQUEST['trPolizaGastosAnticiposTolucaHdn'] != '0.00' || $_REQUEST['trPolizaGastosAnticiposTolucaHdn'] != '0' || $_REQUEST['trPolizaGastosAnticiposTolucaHdn'] != 0.00 || $_REQUEST['trPolizaGastosAnticiposTolucaHdn'] != 0)
                {


                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'7009'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaTolAnt . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposTolucaHdn']) . "," . "'ANTICIPO GASTOS TOLUCA'," . "'GP'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposTolucaHdn']) . ")";

                    fn_ejecuta_sql($sqlAddPoliza);

                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                    {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                    }
                }

                if ($_REQUEST['trPolizaGastosAnticiposLazaroHdn'] != '0.00' || $_REQUEST['trPolizaGastosAnticiposLazaroHdn'] != '0' || $_REQUEST['trPolizaGastosAnticiposLazaroHdn'] != 0.00 || $_REQUEST['trPolizaGastosAnticiposLazaroHdn'] != 0)
                {


                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'7024'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaLzcAnt . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposLazaroHdn']) . "," . "'ANTICIPO GASTOS LAZARO'," . "'GP'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposLazaroHdn']) . ")";

                    fn_ejecuta_sql($sqlAddPoliza);

                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                    {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                    }
                }
                if ($_REQUEST['trPolizaGastosAnticiposVeracruzHdn'] != '0.00' || $_REQUEST['trPolizaGastosAnticiposVeracruzHdn'] != '0' || $_REQUEST['trPolizaGastosAnticiposVeracruzHdn'] != 0.00 || $_REQUEST['trPolizaGastosAnticiposVeracruzHdn'] != 0)
                {

                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'7023'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaSfeAnt . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposVeracruzHdn']) . "," . "'ANTICIPO GASTOS VERACRUZ'," . "'GP'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposVeracruzHdn']) . ")";

                    fn_ejecuta_sql($sqlAddPoliza);

                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                    {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                    }
                }
                if ($_REQUEST['trPolizaGastosAnticiposAguascalientesHdn'] != '0.00' || $_REQUEST['trPolizaGastosAnticiposAguascalientesHdn'] != '0' || $_REQUEST['trPolizaGastosAnticiposAguascalientesHdn'] != 0.00 || $_REQUEST['trPolizaGastosAnticiposAguascalientesHdn'] != 0)
                {   

                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'7015'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaAgsAnt . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposAguascalientesHdn']) . "," . "'ANTICIPO GASTOS AGUASCALIENTES'," . "'GP'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposAguascalientesHdn']) . ")";

                    fn_ejecuta_sql($sqlAddPoliza);

                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                    {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                    }
                }
                if ($_REQUEST['trPolizaGastosAnticiposSaltilloHdn'] != '0.00' || $_REQUEST['trPolizaGastosAnticiposSaltilloHdn'] != '0' || $_REQUEST['trPolizaGastosAnticiposSaltilloHdn'] != 0.00 || $_REQUEST['trPolizaGastosAnticiposSaltilloHdn'] != 0)
                {

                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'7016'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaSalAnt . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposSaltilloHdn']) . "," . "'ANTICIPO GASTOS SALTILLO'," . "'GP'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . floatval($_REQUEST['trPolizaGastosAnticiposSaltilloHdn']) . ")";

                    fn_ejecuta_sql($sqlAddPoliza);

                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                    {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                    }
                }
            }
        }

        if ($a['success'] && $_REQUEST['trPolizaGastosDiesel9012Hdn'] != '')
        {

            $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'9012'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaCargoOperador . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosDiesel9012Hdn']) . "," . "'DIESEL DESCUENTO'," . "'GP'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . floatval($_REQUEST['trPolizaGastosDiesel9012Hdn']) . ")";

            fn_ejecuta_sql($sqlAddPoliza);

            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
            {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
            }
        }

        //CARGO AL OPERADOR
        if ($a['success'] && $_REQUEST['trPolizaGastosCargoOperadorHdn'] != '' && $_REQUEST['trPolizaGastosCargoOperadorHdn'] != '0.00')
        {

            $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip, subtotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'7013'," . "'" . $_SESSION['usuCompania'] . "','" . $folio . "'," . "'" . $fecha . "', ". "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(ch.cuentaContable,4,0)) ".
                     " FROM trviajestractorestbl vt, cachoferestbl ch ".
                      "WHERE vt.idViajeTractor=" . $_REQUEST['trPolizaGastosIdViajeHdn'] . " ".
                      "AND vt.claveChofer=ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 7013)," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . "," . "''," . "'" . $claveGastos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "', " . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . ") ";

            fn_ejecuta_sql($sqlAddPoliza);

            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
            {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
            }
            else
            {
                if ($_REQUEST['generaPolizaGtosHdn'] == 1)
                {
                    // SUELDOS

                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subtotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'145'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," .
                        "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(ch.cuentaContable,4,0)) ".
                     " FROM trviajestractorestbl vt, cachoferestbl ch ".
                      "WHERE vt.idViajeTractor=" . $_REQUEST['trPolizaGastosIdViajeHdn'] . " ".
                      "AND vt.claveChofer=ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 145) ," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . "," . "''," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "', " . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . ")";

                    fn_ejecuta_sql($sqlAddPoliza);
                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                    }
                }
                else
                {
                    // SOLO GASTOS
                    $secStr = "SELECT secuencia FROM roconceptosdescuentoschofertbl " . "WHERE claveChofer = " . $_REQUEST['trPolizaGastosChoferHdn'] . " AND concepto = '2231'";
                    $sec1 = fn_ejecuta_sql($secStr);
                    $secuencia = 1;
                    if ($sec1['records'] == 0)
                    {
                        $descConcepto = "INSERT INTO roconceptosdescuentoschofertbl " . "(claveChofer, concepto, centroDistribucion, secuencia, fechaEvento, pagadoCobro, tipoConcepto, tipoImporte, importe, saldo, estatus) VALUES " . "('" . $_REQUEST['trPolizaGastosChoferHdn'] . "', '2231', 'CDTOL', '" . $secuencia . "', '" . $fecha . "', 'C', 'I', " . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . ", " . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . ", " . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . ", 'P')";//" . $_SESSION['usuCompania'] . "
                        fn_ejecuta_sql($descConcepto);
                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $descConcepto;
                        }
                    }
                    else
                    {
                        $secStr = "SELECT tipoConcepto, importe, IFNULL(saldo,0) AS saldo " . "FROM roconceptosdescuentoschofertbl " . "WHERE claveChofer = '" . $_REQUEST['trPolizaGastosChoferHdn'] . "' AND concepto = '2231' AND estatus = 'P'";
                        $sec2 = fn_ejecuta_sql($secStr);
                        if ($sec2['records'] == 0)
                        {
                            $secStr = "SELECT IFNULL(MAX(secuencia),0) AS secuencia " . 
                                    "FROM roconceptosdescuentoschofertbl " . 
                                    "WHERE claveChofer = '" . $_REQUEST['trPolizaGastosChoferHdn'] . "' ".
                                    "AND concepto = '2231' AND estatus = 'L'";
                            $sec2 = fn_ejecuta_sql($secStr);
                            $secuencia = floatval($sec2['root'][0]['secuencia']);

                            $secuencia++;
                            $descConcepto = "INSERT INTO roconceptosdescuentoschofertbl " . "(claveChofer, concepto, centroDistribucion, secuencia, fechaEvento, pagadoCobro, tipoConcepto, tipoImporte, importe, saldo, estatus) VALUES " . "('" . $_REQUEST['trPolizaGastosChoferHdn'] . "', '2231', 'CDTOL', '" . $secuencia . "', '" . $fecha . "', 'C', 'I', " . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . ", " . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . ", " . floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']) . ", 'P')";//" . $_SESSION['usuCompania'] . "
                            fn_ejecuta_sql($descConcepto);
                            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                                $a['success'] = false;
                                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $descConcepto;
                            }
                        }
                        else
                        {
                            $ls_upd = "";
                            $saldo = floatval($sec2['root'][0]['saldo']);
                            $saldo = $saldo + floatval($_REQUEST['trPolizaGastosCargoOperadorHdn']);
                            if ($sec2['root'][0]['tipoConcepto'] == 'I' && $saldo > floatval($sec2['root'][0]['importe'])) {
                                $ls_upd = "tipoImporte = ".$saldo.", importe = ".$saldo.", saldo = " . $saldo;
                            } else {
                                $ls_upd = "saldo = " . $saldo;
                            }

                            $actDesc = "UPDATE roconceptosdescuentoschofertbl " . "SET ${ls_upd} " . "WHERE claveChofer = '" . $_REQUEST['trPolizaGastosChoferHdn'] . "' AND concepto = '2231' AND estatus = 'P'";
                            fn_ejecuta_sql($actDesc);
                            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                                $a['success'] = false;
                                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $actDesc;
                            }
                        }
                    }
                }
            }
        }

        //PAGO EN EFECTIVO
        if ($a['success'] && $_REQUEST['trPolizaGastosPagoEfectivoHdn'] != '' && $_REQUEST['trPolizaGastosPagoEfectivoHdn'] != '0.00')
        {
            if ($_REQUEST['trPolizaGastosMesAfectacionHdn'] == date("m"))
            {
                $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'7014'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaPagoEfectivo . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . floatval($_REQUEST['trPolizaGastosPagoEfectivoHdn']) . "," . "''," . "'" . $claveGastos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . "'" . floatval($_REQUEST['trPolizaGastosPagoEfectivoHdn']) . "');";

                fn_ejecuta_sql($sqlAddPoliza);

                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                }
            }
        }

        //DIESEL EXTRA
        if ($a['success'] && $_REQUEST['trPolizaGastosDieselExtraHdn'] != "")
        {
            $dieselExtraArr = explode('|', substr($_REQUEST['trPolizaGastosDieselExtraHdn'], 0, -1));

            foreach ($dieselExtraArr as $diesel)
            {
                if ($diesel != "" && $a['success'])
                {
                    $temp = explode('-', $diesel);

                    if ($temp[0] != "" && $temp[1] != "")
                    {
                        $sqlAddDieselExtra = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'" . $temp[0] . "'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "(SELECT cuentaContable FROM caConceptosCentrosTbl " . "WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND concepto = '" . $temp[0] . "')," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $temp[1] . "," . "''," . "'" . $claveDieselExtra . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "')";

                        fn_ejecuta_sql($sqlAddDieselExtra);

                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddDieselExtra;
                            break;
                        }
                    }
                }
            }
        }
    }
    /***************************
        /   POLIZA DE SUELDOS
    /***************************/
    if ($_REQUEST['generaPolizaGtosHdn'] == 1)
    {
        if ($a['success'])
        {
            // descPersonalesArr => concepto, tipoImporteArr => tipoConcepto
            for ($i = 0;$i < sizeof($descPersonalesArr);$i++)
            {
                $extra = "";
                $nuevoSaldo = 0.00;

                if ($sueldosPersonalesArr[$i] == '1')//desc>> 0 generales, 1 personales
                {
                    if (($descPersonalesArr[$i] == '2227' || $descPersonalesArr[$i] == '2228') && $tipoImporteArr[$i] == 'A') {
                        if ($descPersonalesArr[$i] == '2227') {
                            $selSaldo = "SELECT * FROM roConceptosDescuentosChoferTbl " . 
                                        "WHERE claveChofer = ".$_REQUEST['trPolizaGastosChoferHdn']." ".
                                        "AND concepto = '2227' ".
                                        "AND tipoConcepto = 'A' ".
                                        "AND estatus = 'P'";
                            $infonavitRst = fn_ejecuta_sql($selSaldo);
                            $saldoGeneralDeuda = floatval($infonavitRst['root'][0]['saldo']);
                            $adeudoPrm = floatval($calculosPersonalesArr[$i]);
                            $saldoGeneralDeuda = $saldoGeneralDeuda - $adeudoPrm;

                            $insPagosHistorico = "INSERT INTO roPagosOperadorHistoricoTbl (folio, idViajeTractor, fechaPoliza, idPrestamo, concepto, sueldo, porcentaje, adeudo, saldoGeneralDeuda, claveMovimiento, parametros, fechaEvento, idUsuario) VALUES (".
                                    "'".$folio."', ".$_REQUEST['trPolizaGastosIdViajeHdn'].", '".$fecha."', ".$infonavitRst['root'][0]['idDescuento'].", '2227', ".$_REQUEST['trPolizaSueldosSueldoTotalHdn'].", 100, ".$adeudoPrm.", ".$saldoGeneralDeuda.", 'PA', NULL, '".$fecha."', ".$_SESSION['idUsuario'].")";
                            fn_ejecuta_sql($insPagosHistorico);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql']!="") {
                                $a['success']      = false;
                                $a['errorMessage'] = $_SESSION['error_sql'];
                                $a['sql_error']    = $insPagosHistorico;
                                break;
                            }
                        } else {
                            $prestamos2228Str = $_REQUEST['prestamos2228Hdn'];
                            $prestamos2228Arr = explode(':', $prestamos2228Str);
                            $totPrestamos2228 = sizeof($prestamos2228Arr);

                            $selSaldo = "SELECT * FROM roConceptosDescuentosChoferTbl " . 
                                        "WHERE claveChofer = ".$_REQUEST['trPolizaGastosChoferHdn']." ".
                                        "AND concepto = '2228' ".
                                        "AND tipoConcepto = 'A' ".
                                        "AND estatus = 'P'";
                            $fonacotRst = fn_ejecuta_sql($selSaldo);
                            $saldoGeneralDeuda = floatval($fonacotRst['root'][0]['saldo']);
                            for ($pp=0; $pp < $totPrestamos2228; $pp++) {
                                $prestamos2228Rec = $prestamos2228Arr[$pp];
                                list($idPrestamoPrm, $porcentajePrm, $adeudoPrm) = explode('|', $prestamos2228Rec);
                                $saldoGeneralDeuda = $saldoGeneralDeuda - floatval($adeudoPrm);
                                $insPagosHistorico = "INSERT INTO roPagosOperadorHistoricoTbl (folio, idViajeTractor, fechaPoliza, idPrestamo, concepto, sueldo, porcentaje, adeudo, saldoGeneralDeuda, claveMovimiento, parametros, fechaEvento, idUsuario) VALUES (".
                                    "'".$folio."', ".$_REQUEST['trPolizaGastosIdViajeHdn'].", '".$fecha."', ".$idPrestamoPrm.", '2228', ".$_REQUEST['trPolizaSueldosSueldoTotalHdn'].", ".$porcentajePrm.", ".$adeudoPrm.", ".$saldoGeneralDeuda.", 'PA', NULL, '".$fecha."', ".$_SESSION['idUsuario'].")";
                                fn_ejecuta_sql($insPagosHistorico);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql']!="") {
                                    $a['success']      = false;
                                    $a['errorMessage'] = $_SESSION['error_sql'];
                                    $a['sql_error']    = $insPagosHistorico;
                                    break;
                                }
                            }
                        }
                    } else {
                        if ($tipoImporteArr[$i] == 'P' || $tipoImporteArr[$i] == 'I')
                        {
                            // importesPersonalesArr ori, calculosPersonalesArr pos
                            $nuevoSaldo = floatval($saldosArr[$i]) - floatval($calculosPersonalesArr[$i]);
                            if ($nuevoSaldo <= 0)
                            {
                                $nuevoSaldo = 0;
                                $extra = ", estatus = 'L' ";
                            }
                        }

                        $sqlUpdSaldo = "UPDATE roConceptosDescuentosChoferTbl " . 
                                       "SET saldo = " . $nuevoSaldo . " " . $extra . 
                                       "WHERE claveChofer = " . $_REQUEST['trPolizaGastosChoferHdn'] . " " . 
                                       "AND concepto = '" . $descPersonalesArr[$i] . "' " . 
                                       "AND estatus = 'P' ";
                        fn_ejecuta_sql($sqlUpdSaldo);
                        if (!isset($_SESSION['error_sql']) || isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")
                        {
                            $selMaxSecuencia="SELECT IFNULL(max(secuencia)+1,1) as secuencia ".
                                             "FROM roHistoricoDescuentosChoferesTbl ".
                                             "WHERE claveChofer= '".$_REQUEST['trPolizaGastosChoferHdn']."' ";
                            $rsSelMaxSecuencia= fn_ejecuta_sql($selMaxSecuencia);

                            $secuencia=$rsSelMaxSecuencia['root'][0]['secuencia'];
                            // importesPersonalesArr ori, calculosPersonalesArr pos
                            $sqlAddHistorico = "INSERT INTO roHistoricoDescuentosChoferesTbl (centroDistribucion, claveChofer,secuencia,avanzada, " . "concepto, fechaEvento, viaje, importe) VALUES (" . "'" . $_SESSION['usuCompania'] . "'," . $_REQUEST['trPolizaGastosChoferHdn'] . ",'" .$secuencia."','". $folio . "'," . "'" . $descPersonalesArr[$i] . "'," . "'" . $fecha . "'," . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . $calculosPersonalesArr[$i] . ")";
                            fn_ejecuta_sql($sqlAddHistorico);
                            if (!isset($_SESSION['error_sql']) || isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")
                            {

                            }
                            else
                            {
                                $a['success'] = false;
                                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddHistorico;
                                break;
                            }
                        }
                        else
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdSaldo;
                            break;
                        }
                    }
                }
                //CONCEPTOS DE SUELDOS
                if ($a['success'] && floatval($calculosPersonalesArr[$i]) != 0)
                {
                    $sqlAddPoliza = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'" . $descPersonalesArr[$i] . "'," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "'" . $cuentaContSueldosArr[$i] . "'," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . strval(floatval($calculosPersonalesArr[$i]) * -1) . "," . "''," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . strval(floatval($calculosPersonalesArr[$i]) * -1) . ");";

                    fn_ejecuta_sql($sqlAddPoliza);

                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                    {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPoliza;
                        break;
                    }
                }
            }
        }

        if ($a['success'])
        {
            // SUELDOS

            if ($_REQUEST['trPolizaGastosChoferHdn']=='10904' || $_REQUEST['trPolizaGastosChoferHdn']=='10977' || $_REQUEST['trPolizaGastosChoferHdn']=='10979' || $_REQUEST['trPolizaGastosChoferHdn']=='30003' || $_REQUEST['trPolizaGastosChoferHdn']=='70042' || /*$_REQUEST['trPolizaGastosChoferHdn']=='70119'|| $_REQUEST['trPolizaGastosChoferHdn']=='10945' ||*/ $_REQUEST['trPolizaGastosChoferHdn']=='70043' || $_REQUEST['trPolizaGastosChoferHdn']=='70010' ||  $_REQUEST['trPolizaGastosChoferHdn']=='70013' || $_REQUEST['trPolizaGastosChoferHdn']=='30001' || $_REQUEST['trPolizaGastosChoferHdn']=='10978' || $_REQUEST['trPolizaGastosChoferHdn']=='30002' || $_REQUEST['trPolizaGastosChoferHdn']=='10984' || $_REQUEST['trPolizaGastosChoferHdn']=='70014') {
                
                
                //sueldo minimadrinas
                $sqlAddSueldo = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "160," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," .  "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(tt.tractor,4,0),'.',LPAD(ch.claveChofer,5,0)) ".
                        "FROM trviajestractorestbl vt, cachoferestbl ch , catractorestbl tt  ".
                        "WHERE vt.idViajeTractor='" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ".
                        "AND vt.idTractor=tt.idTractor ".
                        "AND vt.claveChofer= ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 160)," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $_REQUEST['trPolizaSueldosSueldoTotalHdn'] . "," . "'SUELDOS POR CUOTA'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $_REQUEST['trPolizaSueldosSueldoTotalHdn'] . ")";

                fn_ejecuta_sql($sqlAddSueldo);
            }else{
                ////sueldo cargado
                $sqlAddSueldo = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "143," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," .  "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(tt.tractor,4,0),'.',LPAD(ch.claveChofer,5,0)) ".
                        "FROM trviajestractorestbl vt, cachoferestbl ch , catractorestbl tt  ".
                        "WHERE vt.idViajeTractor='" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ".
                        "AND vt.idTractor=tt.idTractor ".
                        "AND vt.claveChofer= ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 143)," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $_REQUEST['polizaSueldosSueldoNormalDsp'] . "," . "'TOTAL PAGO NORMAL'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $_REQUEST['polizaSueldosSueldoNormalDsp'] . ")";

                fn_ejecuta_sql($sqlAddSueldo);
            }

                

            

            $sqlAddSueldo = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "149," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," .  "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(tt.tractor,4,0),'.',LPAD(ch.claveChofer,5,0)) ".
                    "FROM trviajestractorestbl vt, cachoferestbl ch , catractorestbl tt  ".
                    "WHERE vt.idViajeTractor='" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ".
                    "AND vt.idTractor=tt.idTractor ".
                    "AND vt.claveChofer= ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 149)," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $_REQUEST['polizaSueldosBonoCDMXTxt'] . "," . "'BONO LOCAL CDMX'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $_REQUEST['polizaSueldosBonoCDMXTxt'] . ")";

            fn_ejecuta_sql($sqlAddSueldo);

        


            $sqlAddSueldo = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "147," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," .  "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(tt.tractor,4,0),'.',LPAD(ch.claveChofer,5,0)) ".
                    "FROM trviajestractorestbl vt, cachoferestbl ch , catractorestbl tt  ".
                    "WHERE vt.idViajeTractor='" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ".
                    "AND vt.idTractor=tt.idTractor ".
                    "AND vt.claveChofer= ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 147)," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $_REQUEST['polizaSueldosSueldoVacioDsp'] . "," . "'TOTAL PAGO VACIO'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $_REQUEST['polizaSueldosSueldoVacioDsp'] . ")";

            fn_ejecuta_sql($sqlAddSueldo);

            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddSueldo;
            } else {
                // SUELDO TOTAL
                $sqlAddExtra = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "138," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," ." ".
                    "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(tt.tractor,4,0),'.',LPAD(ch.claveChofer,5,0)) ".
                    "FROM trviajestractorestbl vt, cachoferestbl ch , catractorestbl tt  ".
                    "WHERE vt.idViajeTractor='" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ".
                    "AND vt.idTractor=tt.idTractor ".
                    "AND vt.claveChofer= ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 138)," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $_REQUEST['trPolizaSueldosSueldoTotalHdn'] . "," . "'SUELDO TOTAL'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $_REQUEST['trPolizaSueldosSueldoTotalHdn'] . ")";

                fn_ejecuta_sql($sqlAddExtra);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddExtra;
                }
            }

            /////// inserta gratificaciones sueldo
        }

        if ($a['success'])
        {

            $pagoNeto = floatval($_REQUEST['pagoNetoHdn']);
            $gastosNoComprobado = floatval($_REQUEST['aGtosnocompHdn']);
            // echo json_encode("PN".$pagoNeto);
            
            // var_dump($pagoNeto);
            // var_dump($gastosNoComprobado);exit();
            if ($pagoNeto == 0)// && $gastosNoComprobado != 0
            {

                ///////////////////////gratificaciones
                  $sqlT2="SELECT * from autorizacionesespecialestbl
                        where concepto=136
                        and estatus='AUTORIZADO'
                        and idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']."
                        and descripcion ='T2'";
                $rsT2=fn_ejecuta_query($sqlT2);


                $sqlT2gen="SELECT * from cageneralestbl
                            where tabla='autorizacionesespecialestbl'
                            and valor='T2'";
                $rsGT2=fn_ejecuta_query($sqlT2gen);

                $importeT2=sizeof($rsT2['root'])*$rsGT2['root'][0]['estatus'];

                $sqlT3="SELECT * from autorizacionesespecialestbl
                        where concepto=136
                        and estatus='AUTORIZADO'
                        and idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']."
                        and descripcion ='T3';";
                $rsT3=fn_ejecuta_query($sqlT3);


                $sqlT3gen="SELECT * from cageneralestbl
                            where tabla='autorizacionesespecialestbl'
                            and valor='T3'";
                $rsGT3=fn_ejecuta_query($sqlT3gen);

                $importeT3=sizeof($rsT3['root'])*$rsGT3['root'][0]['estatus'];

                $importeGrat=$importeT2+$importeT3;



                $sqlAddPagoNeto = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "136," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . 
                    "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(tt.tractor,4,0),'.',LPAD(ch.claveChofer,5,0)) ".
                    "FROM trviajestractorestbl vt, cachoferestbl ch , catractorestbl tt  ".
                    "WHERE vt.idViajeTractor='" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ".
                    "AND vt.idTractor=tt.idTractor ".
                    "AND vt.claveChofer= ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 136), ". "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $importeGrat . "," . "'GRAT. EST.'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $importeGrat . ")";

                fn_ejecuta_sql($sqlAddPagoNeto);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPagoNeto;
                }

                $updGrat="UPDATE autorizacionesespecialestbl SET estatus='PAGADO A POLIZA', folioPoliza='".$folio."', fechaAplicacion=now()
                        where concepto=136
                        and estatus='AUTORIZADO'
                        and idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']."
                        and descripcion in('T2','T3');";
                fn_ejecuta_query($updGrat);



                ///////////////////////////

                $sqlAddPagoNeto = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "144," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . 
                    "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(ch.cuentaContable,4,0)) ".
                     " FROM trviajestractorestbl vt, cachoferestbl ch ".
                      "WHERE vt.idViajeTractor=" . $_REQUEST['trPolizaGastosIdViajeHdn'] . " ".
                      "AND vt.claveChofer=ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 144)," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $gastosNoComprobado . "," . "'GASTOS NO COMPROBADOS'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $gastosNoComprobado . ")";

                fn_ejecuta_sql($sqlAddPagoNeto);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPagoNeto;
                } else {
                    $sqlGetIdDescuentoAdeudo = "SELECT idDescuento,saldo,tipoConcepto, importe FROM roConceptosDescuentosChoferTbl " . "WHERE claveChofer = " . $_REQUEST['trPolizaGastosChoferHdn'] . " " . "AND concepto = '2231' AND estatus ='P' ";

                    $rs = fn_ejecuta_sql($sqlGetIdDescuentoAdeudo);

                    if (sizeof($rs['root']) > 0)
                    {
                        $ld_saldo = (floatval($rs['root'][0]['saldo']) + $gastosNoComprobado);
                        if ($rs['root'][0]['tipoConcepto'] == 'I' && $ld_saldo > floatval($rs['root'][0]['importe'])) {
                            $ls_importe = "tipoImporte = ".$ld_saldo.", importe = ".$ld_saldo.", saldo = " . $ld_saldo;
                        } else {
                            $ls_importe = "saldo = " . $ld_saldo;
                        }
                        $sqlUpdAdeudo = "UPDATE roConceptosDescuentosChoferTbl " . "SET ${ls_importe} ". "WHERE idDescuento = " . $rs['root'][0]['idDescuento'];

                        fn_ejecuta_sql($sqlUpdAdeudo);
                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdAdeudo;
                        }
                    }
                    else
                    {
                        $sqlGetSecuencia = "SELECT IFNULL(MAX(secuencia),0)+1 AS secuencia FROM roConceptosDescuentosChoferTbl " . "WHERE claveChofer = '" . $_REQUEST['trPolizaGastosChoferHdn'] . "' and concepto='2231' ";

                        $rsSec = fn_ejecuta_sql($sqlGetSecuencia);

                        $sqlAddAdeudo = "INSERT INTO roConceptosDescuentosChoferTbl (centroDistribucion, claveChofer, concepto, " . "secuencia, fechaEvento, pagadoCobro, tipoConcepto,tipoImporte, importe, saldo, estatus) VALUES (" . "'CDTOL'," . $_REQUEST['trPolizaGastosChoferHdn'] . "," . "'2231'," . $rsSec['root'][0]['secuencia'] . "," . "'" . $fecha . "'," . "'C'," . "'I'," . $gastosNoComprobado . ",". $gastosNoComprobado . "," .  $gastosNoComprobado ."," . "'P')";//" . $_SESSION['usuCompania'] . "

                        fn_ejecuta_sql($sqlAddAdeudo);

                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddAdeudo;
                        }
                    }
                }
            }
            else{

                $sqlT2="SELECT * from autorizacionesespecialestbl
                        where concepto=136
                        and idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']."
                        and descripcion ='T2'";
                $rsT2=fn_ejecuta_query($sqlT2);


                $sqlT2gen="SELECT * from cageneralestbl
                            where tabla='autorizacionesespecialestbl'
                            and valor='T2'";
                $rsGT2=fn_ejecuta_query($sqlT2gen);

                $importeT2=sizeof($rsT2['root'])*$rsGT2['root'][0]['estatus'];

                $sqlT3="SELECT * from autorizacionesespecialestbl
                        where concepto=136
                        and idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']."
                        and descripcion ='T3';";
                $rsT3=fn_ejecuta_query($sqlT3);


                $sqlT3gen="SELECT * from cageneralestbl
                            where tabla='autorizacionesespecialestbl'
                            and valor='T3'";
                $rsGT3=fn_ejecuta_query($sqlT3gen);

                $importeT3=sizeof($rsT3['root'])*$rsGT3['root'][0]['estatus'];

                $importeGrat=$importeT2+$importeT3;

                $pagoNeto =$pagoNeto + $importeGrat;

                //echo $pagoNeto;



                 $sqlAddPagoNeto = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "136," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . 
                    "(SELECT concat(SUBSTR(cuentaContable,1,14),(select CONCAT(LPAD(tt.tractor,4,0),'.',LPAD(ch.claveChofer,5,0)) ".
                    "FROM trviajestractorestbl vt, cachoferestbl ch , catractorestbl tt  ".
                    "WHERE vt.idViajeTractor='" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ".
                    "AND vt.idTractor=tt.idTractor ".
                    "AND vt.claveChofer= ch.claveChofer)) FROM caConceptosCentrosTbl WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' AND concepto = 136), ". "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $importeGrat . "," . "'GRAT. EST.'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $importeGrat . ")";

                fn_ejecuta_sql($sqlAddPagoNeto);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPagoNeto;
                }

                $sqlAddPagoNeto = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "141," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . 
                    "(SELECT cuentaContable FROM caConceptosCentrosTbl " . "WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND concepto = 141), ". "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $pagoNeto . "," . "'PAGO NETO'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $pagoNeto . ")";

                fn_ejecuta_sql($sqlAddPagoNeto);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddPagoNeto;
                }
            }
        }

        //OTROS y EXTRAS
        if ($a['success'])
        {

            if ($_REQUEST['trPolizaSueldosKmExtraHdn'] != "")
            {

                $sqlAddExtra = "INSERT INTO trGastosViajeTractorTbl (idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, " . "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip,subTotal) VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "140," . "'" . $_SESSION['usuCompania'] . "'," . "'" . $folio . "'," . "'" . $fecha . "'," . "(SELECT cuentaContable FROM caConceptosCentrosTbl " . "WHERE centroDistribucion = '" . $_SESSION['usuCompania'] . "' " . "AND concepto = 140)," . "'" . $_REQUEST['trPolizaGastosMesAfectacionHdn'] . "'," . $_REQUEST['trPolizaSueldosKmExtraHdn'] . "," . "'" . $_REQUEST['trPolizaSueldosAutosExtraHdn'] . "'," . "'" . $claveSueldos . "'," . $_SESSION['idUsuario'] . "," . "'" . $_SERVER['REMOTE_ADDR'] . "'," . $_REQUEST['polizaSueldosSueldoExtraDsp'] . ")";

                fn_ejecuta_sql($sqlAddExtra);

                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddExtra;
                }
            }

        }
        //

    }

    //OBSERVACIONES
    if ($a['success'])
    {
        if ($_REQUEST['trPolizaGastosObservacionesTxa'] != "")
        {
            //OBSERVACIONES GASTOS
            $sqlAddObservGastos = "INSERT INTO trObservacionesViajeTbl (idViajeTractor, folio, fechaEvento, tipo, observaciones )" . "VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'" . $folio . "'," . "'" . $fecha . "'," . "'G'," . "'" . $_REQUEST['trPolizaGastosObservacionesTxa'] . "')";

            fn_ejecuta_sql($sqlAddObservGastos);

            if (!isset($_SESSION['error_sql']) || isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")
            {
                if ($_REQUEST['generaPolizaGtosHdn'] == 1)
                {
                    if ($_REQUEST['trPolizaSueldosObservacionesTxa'] != "")
                    {
                        //OBSERVACIONES SUELDOS
                        $sqlAddObservGastos = "INSERT INTO trObservacionesViajeTbl (idViajeTractor, folio, fechaEvento, tipo, observaciones )" . "VALUES (" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "," . "'" . $folio . "'," . "'" . $fecha . "'," . "'S'," . "'" . $_REQUEST['trPolizaSueldosObservacionesTxa'] . "')";

                        fn_ejecuta_sql($sqlAddObservGastos);

                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                        {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddObservGastos;
                        }
                    }
                }
            }
            else
            {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddObservGastos;
            }
        }
    }

    if (!isset($_REQUEST['polizaSueldosOdInicialHdn']))
    {
        $_REQUEST['polizaSueldosOdInicialHdn'] = 0;
    }
    if (!isset($_REQUEST['polizaSueldosOdFinalHdn']))
    {
        $_REQUEST['polizaSueldosOdFinalHdn'] = 0;
    }

    

    //SE ACTUALIZAN LOS KILOMETROS COMPROBADOS
   // if ($a['success'])
    //{
        $sqlOper="SELECT * FROM trViajesTractoresTbl where idViajeTractor='".$_REQUEST['trPolizaGastosIdViajeHdn']."'";
        $rsOper=fn_ejecuta_query($sqlOper);

        $updOper="UPDATE autorizacionesespecialestbl SET estatus='PAGADO A POLIZA',folioPoliza='".$nvoFolio.
                "', fechaAplicacion=now() WHERE clavechofer='".$rsOper['root'][0]['claveChofer']."' AND tipoAutorizacion in ('COMPENSACION POR ESPERA DE CARGA','DIESEL EXTRA','EFECTIVO','ESTANCIAS','GRATIFICACIONES','REPARACIONES','PENSIONES','TAXIS EXTRAORDINARIOS','MANIOBRAS','TAXIS') AND estatus='autorizado'" ;
        fn_ejecuta_query($updOper);
        /*if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
        {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlOper;
        }*/
    //}

   //SE ACTUALIZAN LOS KILOMETROS COMPROBADOS
    if ($a['success'])
    {
        $sqlUpdViaje = "UPDATE trViajesTractoresTbl SET " . "kilometrosComprobados = " . $_REQUEST['trPolizaGastosKmPagadosHdn'] . ", " ."kilometrosSinUnidad = " . $_REQUEST['trPolizaGastosKmVacioHdn'] . ", " . "odInicial = '" . $_REQUEST['polizaSueldosOdInicialHdn'] . "', " . "odFinal = '" . $_REQUEST['polizaSueldosOdFinalHdn'] . "' " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'];
        fn_ejecuta_sql($sqlUpdViaje);
        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
        {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdViaje;
        }
    }

    //SE ACTUALIZA EL VIAJE SOLO SI NO ES VIAJE PARCIAL
    if ($a['success'] && !filter_var($_REQUEST['trPolizaGastosParcialHdn'], FILTER_VALIDATE_BOOLEAN))
    {
        $sqlUpdViaje = "UPDATE trViajesTractoresTbl SET claveMovimiento = 'VP' " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'];

        fn_ejecuta_sql($sqlUpdViaje);

        if (!isset($_SESSION['error_sql']) || isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")
        {
            $a['successMessage'] = getPolizaSuccessMsg();
        }
        else
        {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdViaje;
        }
    }
    else
    {
        $a['successMessage'] = getPolizaParcialSuccessMsg();
    }
    // ACTUALIZA CARGA EN BOMBAS
    if ($a['success'])
    {
        $sqlUpdFol = "UPDATE trgastosviajetractortbl " . "SET folio = '" . $folio . "' " . "WHERE idViajeTractor = " . $_REQUEST['trPolizaGastosIdViajeHdn'] . " " . "AND concepto IN (SELECT valor FROM caGeneralesTbl WHERE tabla = 'conceptos' AND columna = 9000 ) " . "AND centroDistribucion = '" . $_SESSION['usuCompania'] . "'";
        fn_ejecuta_sql($sqlUpdFol);
        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
        {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'];
            $a['sql'] = $sqlUpdFol;
        }
        else
        {
            $sqlUpdPdt = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CL' " . "WHERE idCombustible = " . $_REQUEST['idCombustibleHdn'] . " ";
            fn_ejecuta_sql($sqlUpdPdt);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
            {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'];
                $a['sql'] = $sqlUpdPdt;
            }
        }
    }
    if ($a['success']) {
        $lbvalidaPoliza = false;
        if(isset($_REQUEST['generaPolizaGtosHdn']) && $_REQUEST['generaPolizaGtosHdn'] != '' && $_REQUEST['generaPolizaGtosHdn'] != 2) {
            $validaPol = validaPoliza($folio);
            if ($validaPol['success'] == false) {
                $lbvalidaPoliza = true;
                $a['success'] = $validaPol['success'];
                $a['errorMessage'] = $validaPol['msjResponse'];
            }
        }
        if (isset($_REQUEST['vistaPreviaHdn']) && $_REQUEST['vistaPreviaHdn'] == 1) {
            $_REQUEST['trViajesTractoresIdViajeHdn'] = $_REQUEST['trPolizaGastosIdViajeHdn'];
            $_REQUEST['trViajesTractoresFolioPolizaHdn'] = $folio;
            $_REQUEST['trViajesTractoresMesAfectacionHdn'] = $_REQUEST['trPolizaGastosMesAfectacionHdn'];
            if (!isset($_REQUEST['generaPolizaGtosHdn']) || $_REQUEST['generaPolizaGtosHdn'] == '') {
                $_REQUEST['generaPolizaGtosHdn'] = 1;
            }
            require_once("impresionPolizaPdf.php");
            $a = generaPolizaPDF();
            $a['successMessage'] = '';
            if ($a['success'] && $lbvalidaPoliza) {
                $a['successMessage'] = $validaPol['msjResponse'];
            }
            sleep(1);
            fn_ejecuta_sql(false);
        } else {
            if ($a['success']) {
                $a['errors'] = $e;
                $a['folio'] = $folio;
                // Elimina archivos preview por usuario >:1:@aha
                $archivosArr = array('pdf','PDF');
                $ps_inputfile = "../documentos/";
                $files = array_diff(scandir($ps_inputfile), array('.', '..'));
                foreach ($files as $file) {
                    $data     = explode(".", $file);
                    $fileName = $data[0];
                    $extName  = end($data);
                    if(in_array($extName, $archivosArr)) {
                        $ps_filename = $ps_inputfile.$fileName.'.'.$extName;
                        if (substr($fileName, 0,strlen('polizagastos_'.$_SESSION['idUsuario'])) != 'polizagastos_'.$_SESSION['idUsuario']) {
                            continue;
                        }
                        if (file_exists($ps_filename)) {
                            unlink($ps_filename);
                        }
                    }
                }
                fn_ejecuta_sql($a['success']);
                if ($a['success']) {
                    //$exeproc = new programaexterno();
                    //$exeproc->runprogram("proDescuentosPersonales.php");
                }
            } else {
                fn_ejecuta_sql(false);
            }
        }
    }
    echo json_encode($a);
}

function validaPoliza($folio)
{
    $a = array('success' => true);

    $folioTMP = $folio;

    $sqlGetSueldos = "SELECT cc.concepto, ".
                    "CASE ".  
                    "WHEN cc.concepto in ('2230','2231','9032') THEN CONCAT(SUBSTR(cc.cuentaContable,1,14),(SELECT LPAD(cco.cuentaContable,4,'0') ".
                    "FROM cachoferestbl cco where cco.claveChofer = (SELECT vt.claveChofer from trviajestractorestbl vt where ".
                    "vt.idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn']." ))) ELSE cc.cuentaContable END  as cuentaContable, ".
                    "gv.iva, gv.subtotal, gv.importe, gv.observaciones, co.nombre, ge.columna, ".
                    "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE cc.concepto = gv.concepto ".
                    "AND cc.centroDistribucion = '".$_SESSION['usuCompania']."' ) AS importeConcepto, ".
                    "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                    "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                    "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                    "LEFT JOIN trGastosViajeTractorTbl gv ".
                    "ON gv.concepto = ge.valor ".
                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                    "AND gv.folio = '".$folioTMP."' ".
                    "AND gv.claveMovimiento = 'GS' ".
                    "WHERE ge.valor = co.concepto ".
                    "AND cc.concepto = co.concepto ".
                    "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".  
                    "WHERE gv2.folio = '".$folioTMP."'   AND gv2.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']. " LIMIT 1) ".
                    "AND ge.tabla = 'trPolizaGastosTbl' ".
                    "AND ge.columna = 'deducciones' ".
                    "ORDER BY ge.estatus ";
    $gastosSueldos1 = fn_ejecuta_query($sqlGetSueldos);

    $gastosSueldos = $gastosSueldos1['root'];

    $totalDeducciones = 0;
    foreach ($gastosSueldos as $gasto) {
        $totalDeducciones += floatval($gasto['importe']) * -1;
    }

    $sqlScargo = "SELECT subtotal ".
                  "FROM trgastosviajetractortbl  ".
                  "WHERE concepto='145' ".
                  "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                  "AND folio='".$folioTMP."' ".
                  "AND claveMovimiento='GS' ";
    $rsSqlScargo = fn_ejecuta_query($sqlScargo);

    if($rsSqlScargo['root'][0]['subtotal'] == null){
        $sCargo = 0.00;
    }
    else {
        $sCargo= $rsSqlScargo['root'][0]['subtotal'];
    }

    $sqlPagoNeto = "SELECT subtotal ".
                  "FROM trgastosviajetractortbl  ".
                  "WHERE concepto='141' ".
                  "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                  "AND folio='".$folioTMP."' ".
                  "AND claveMovimiento='GS' ";
    $rsSqlPagoNeto = fn_ejecuta_query($sqlPagoNeto);

    if($rsSqlPagoNeto['root'][0]['subtotal'] == null){
        $pagoNeto = 0.00;
    }
    else {
        $pagoNeto= $rsSqlPagoNeto['root'][0]['subtotal'];
    }

    $sqlAntGasNoComp = "SELECT subtotal ".
                      "FROM  trgastosviajetractortbl  ".
                      "WHERE concepto='144' ".
                      "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                      "AND folio='".$folioTMP."' ".
                      "AND claveMovimiento='GS' ";
    $rsSqlAntGasNoComp = fn_ejecuta_query($sqlAntGasNoComp);

    if($rsSqlAntGasNoComp['root'][0]['subtotal'] == null){
        $antGasNoComprobados = 0.00;
    }
    else{
        $antGasNoComprobados= $rsSqlAntGasNoComp['root'][0]['subtotal'];
    }
    $SqltotalSueldos = "SELECT subtotal ".
                      "FROM  trgastosviajetractortbl  ".
                      "WHERE concepto='138' ".
                      "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                      "AND folio='".$folioTMP."' ".
                      "AND claveMovimiento='GS' ";
    $rsSqltotalSueldos = fn_ejecuta_query($SqltotalSueldos);

    if($rsSqltotalSueldos['root'][0]['subtotal'] == null){
        $totalSueldos = 0.00;
    }
    else{
        $totalSueldos= $rsSqltotalSueldos['root'][0]['subtotal'];
    }
    // Totales
    $totalIngresos = $totalSueldos + $antGasNoComprobados;
    // $totalIngresos = 10;
    $totalEgresos = $sCargo + $pagoNeto + $totalDeducciones;
    // $totalEgresos = 2419.3;
    if ($totalIngresos != $totalEgresos) {
        $a['success'] = false;
        $a['msjResponse'] = "<b>La p&oacuteliza est&aacute descuadrada.</b><br>";
    }
    // echo json_encode($a);
    return $a;
}

function addPolizaComplementaria()
{

}
function siGeneraPoliza()
{

    $sqlGetValida = "SELECT 1 as generaPoliza, gt.centroDistribucion " . "FROM trviajestractorestbl vt, trgastosviajetractortbl gt " . "WHERE vt.idViajeTractor = gt.idViajeTractor " . "AND vt.viaje = (SELECT min(vi.viaje) FROM trViajesTractoresTbl vi WHERE vi.claveChofer = vt.claveChofer AND  vi.claveMovimiento  in ('VU','UV') and vi.idTractor=vt.idTractor) " . "AND gt.claveMovimiento = 'GU' " . "AND vt.claveChofer = '" . $_REQUEST['trViajesTractoresClaveChoferHdn'] . "' and vt.clavemovimiento='VU'";
    //"AND gt.centroDistribucion = '".$_REQUEST['trViajesTractoresCdistribucionHdn']."' ".
    //"AND gt.usuario = 83"; //$_SESSION['idUsuario']
    $rs = fn_ejecuta_query($sqlGetValida);
    echo json_encode($rs);

}

function sumaFacturas()
{
    $sqlGetSuma = "SELECT sum(total) as total,sum(iva) as iva,sum(subTotal ) as subTotal " . "FROM trpolizafacturastbl " . "WHERE idViajeTractor = '" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' " . "AND concepto = '" . $_REQUEST['trPolizaGastosConceptoHdn'] . "' ";

    $rsSuma = fn_ejecuta_query($sqlGetSuma);
    echo json_encode($rsSuma);
}

function addViajeTmp()
{
    $sqlAddViajeTmp = "INSERT INTO trviajestractorestmp (idViajeTractor, idTractor, claveChofer, centroDistribucion, modulo, idUsuario, ip, fechaEvento) VALUES( " . "'" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "', " . "'" . $_REQUEST['polizaGastosOInicialTxt'] . "', " . "'" . $_REQUEST['polizaGastosOfinalTxt'] . "', " . "'ODOMETRO', " . "'Generacion de Polizas', " . "'13', " . "'10.1.2.42', " . "now() )";

    $rsAddTmp = fn_ejecuta_query($sqlAddViajeTmp);
    echo json_encode($rsAddTmp);
}

function dltViajeTmp()
{
    $sqlDltViajeTmp = "DELETE FROM trviajestractorestmp " . "WHERE idViajeTractor = '" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ";

    $rsDltTmp = fn_ejecuta_query($sqlDltViajeTmp);
    echo json_encode($rsDltTmp);
}

function getOdometro()
{
    $sqlGetOdometro = "SELECT idTractor,claveChofer " . "FROM trviajestractorestmp " . "WHERE idViajeTractor = '" . $_REQUEST['trPolizaGastosIdViajeHdn'] . "' ";

    $rsGetTmp = fn_ejecuta_query($sqlGetOdometro);
    echo json_encode($rsGetTmp);
}

function montoLavada()
{
    $sqlGetMonto = "SELECT * FROM cageneralestbl " . "WHERE tabla = 'trPolizaGastos' " . "AND columna = '" . $_REQUEST['montoConcepto'] . "'";

    $rsGetMonto = fn_ejecuta_query($sqlGetMonto);
    echo json_encode($rsGetMonto);
}

function polizaComplemento()
{
    $sqlComplemento = "SELECT DISTINCT tb.claveChofer " . "FROM trgastosviajetractortbl gc, trviajestractorestbl tb " . "WHERE gc.idViajeTractor = tb.idViajeTractor " . "AND gc.folio='" . $_REQUEST['polizaGastosNumeroPolizaTxt'] . "' " . "AND gc.claveMovimiento in('GP')";
    $rsComplemento = fn_ejecuta_query($sqlComplemento);
    echo json_encode($rsComplemento);
}

function getTicketCard()
{

    $sqlGetTicket = "SELECT * FROM trPolizasTcketTbl WHERE estatus = '1' AND idTractor = " . $_REQUEST['idTractor'];
    //echo "$sqlGetTicket<br>";
    $rsTcket = fn_ejecuta_query($sqlGetTicket); 
    echo json_encode($rsTcket);
}
function getDieselExtra()
{
    $sqlGetDieselExtra = "SELECT de.idViajeTractor, concat(de.concepto,' - ',co.nombre) as concepto, de.litros, de.fechaEvento " . "FROM trpolizadieselextratbl de, caconceptostbl co " . "WHERE de.concepto = co.concepto " . "AND de.idViajeTractor = " . $_REQUEST['polizaGastosIdViajeHdn'];

    $rsDiesel = fn_ejecuta_query($sqlGetDieselExtra);
    echo json_encode($rsDiesel);
}
function getMontosDiesel()
{
    $sqlGetMontos = "SELECT IFNULL((SELECT sum( tp.litros ) FROM trgastosviajetractortbl tp WHERE tp.concepto IN (SELECT valor FROM caGeneralesTbl WHERE tabla = 'conceptos' AND columna = '9000' AND idioma = '+') AND tp.idViajeTractor = " . $_REQUEST['polizaGastosIdViajeHdn'] . " and (claveMovimiento !='XD' AND claveMovimiento !='XC')),0.00) AS numLitrosSuma, ".
    "IFNULL((SELECT sum( tp1.litros ) FROM trgastosviajetractortbl tp1 WHERE tp1.concepto IN (SELECT valor FROM caGeneralesTbl WHERE tabla = 'conceptos' AND columna = '9000' AND idioma = '-') AND tp1.idViajeTractor = " . $_REQUEST['polizaGastosIdViajeHdn'] . " and (claveMovimiento !='XD' AND claveMovimiento !='XC') ),0.00) AS numLitrosResta ";

    $rsMonto = fn_ejecuta_query($sqlGetMontos);
    echo json_encode($rsMonto);
}

function updConceptoTicket()
{
    $sqlUpdTicket = "UPDATE trPolizasTcketTbl " . "SET estatus = '0' " . "WHERE idViajeTractor = '" . $_REQUEST['polizaGastosIdViajeHdn'] . "' " . "AND noComprobante = '" . $_REQUEST['polizaGastosNumComprobanteHdn'] . "';";

    fn_ejecuta_query($sqlUpdTicket);
}

function GeneraComplemento()
{
    $sqlGetComplemento = "SELECT 1 as generaPoliza ,vt.centroDistribucion as centroDistribucionOrigen " . "FROM trviajestractorestbl vt, trgastosviajetractortbl gt " . "WHERE vt.idViajeTractor = gt.idViajeTractor " . "AND vt.viaje = (SELECT MAX(vt1.viaje) FROM trviajestractorestbl vt1 WHERE vt1.idViajeTractor = vt.idViajeTractor AND vt1.claveChofer = vt.claveChofer ) " . "AND gt.claveMovimiento = 'GU' " . "AND vt.claveChofer = '" . $_REQUEST['trViajesTractoresClaveChoferHdn'] . "';";

    $rs = fn_ejecuta_query($sqlGetComplemento);
    echo json_encode($rs);
}

function getTalonesVeracruzMacheteros()
{
    $a = array('success' => true, 'autoriza100' => 0, 'autoriza600' => 0, 'msjResponse' => '');

    $sqlConsultaTalones = " SELECT * FROM trtalonesViajesTbl WHERE idViajeTractor = ".$_REQUEST['trPolizaGastosIdViajeHdn'].
                          " AND distribuidor IN (SELECT valor FROM cageneralestbl WHERE tabla='macheteros' AND columna='100')".
                          " AND tipoTalon = 'TN'".
                          " AND claveMovimiento != 'TX'";
    $rsTalones100 = fn_ejecuta_query($sqlConsultaTalones);
    if ($rsTalones100['records'] > 0) {
        $a['autoriza100'] = 1;
        $a['nTalones100'] = sizeof($rsTalones100['root']);
    }

    $sqlConsultaTalones = " SELECT * FROM trtalonesViajesTbl".
                          " WHERE idViajeTractor = ".$_REQUEST['trPolizaGastosIdViajeHdn'].
                          " AND centroDistribucion IN ('CDVER','CDLZC')".
                          " AND tipoTalon = 'TE'".
                          " AND claveMovimiento != 'TX'";
    $rsTalones600 = fn_ejecuta_query($sqlConsultaTalones);

    if ($rsTalones600['records'] > 0) {
        $nTalonVER = 0;
        $nTalonLZC = 0;
        for ($i=0; $i < $rsTalones600['records']; $i++) { 
            $row = $rsTalones600['root'][$i];
            if ($row['centroDistribucion'] == 'CDVER') {
                $selStr = "SELECT * FROM cageneralestbl WHERE tabla='macheteros' AND columna='600' AND idioma = 'CDVER'";
                $rsCentroDist = fn_ejecuta_query($selStr." AND valor = '".$row['distribuidor']."'");
                if ($rsCentroDist['records'] > 0) {
                    $nTalonVER++;
                }
            }
            if ($row['centroDistribucion'] == 'CDLZC') {
                $selStr = "SELECT * FROM cageneralestbl WHERE tabla='macheteros' AND columna='600' AND idioma = 'CDLZC'";
                $rsCentroDist = fn_ejecuta_query($selStr." AND valor = '".$row['distribuidor']."'");
                if ($rsCentroDist['records'] > 0) {
                    $nTalonLZC++;
                }
            }
        }
        if ($nTalonVER > 0 || $nTalonLZC > 0) {
            $a['autoriza600'] = 1;
            $a['nTalones600VER'] = $nTalonVER;
            $a['nTalones600LZC'] = $nTalonLZC;
            if ($nTalonVER == 1) {
               $a['msjResponse']  = "<b>¿Es un viaje local de Veracruz?</b>";
            } elseif ($nTalonLZC == 1) {
               $a['msjResponse']  = "<b>¿Es un viaje local de L&aacutezaro C&aacuterdenas?</b>";
            }
        }
    }

    echo json_encode($a);
}

    function obtenDiasGratificacion(){
         ///////////////////////gratificaciones
                $sql="SELECT  distinct(SELECT count(a.descripcion) * b.estatus  from  autorizacionesespecialestbl a, cageneralestbl b
                                                        where a.concepto=136
                                                        and a.idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']."
                                                        and a.descripcion in ('T2')
                                                        and a.descripcion=b.valor ) as 'tarifa2',                            
                                    (SELECT count(a.descripcion) * b.estatus  from  autorizacionesespecialestbl a, cageneralestbl b
                                                        where a.concepto=136
                                                        and a.idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']."
                                                        and a.descripcion in ('T3')
                                                        and a.descripcion=b.valor ) as 'tarifa3'                    
                                from autorizacionesespecialestbl a, cageneralestbl b
                                where a.concepto=136
                                and a.idViajeTractor=".$_REQUEST['trPolizaGastosIdViajeHdn']."
                                and a.descripcion in ('T2','T3')
                                and a.descripcion=b.valor";
                $rs=fn_ejecuta_query($sql);


                echo json_encode($rs);

    }

?>
