<?php
    /************************************************************************
    * Autor: Carlos Sierra Mayoral
    * Fecha: 24-Agosto-2016
    *************************************************************************/
    session_start();
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

        $a = array();
    $e = array();
    $a['success'] = true;



    $_REQUEST = trasformUppercase($_REQUEST);
    
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
    
    switch($_REQUEST['trModPreViajeActionHdn']){
        case 'getTractoresModificacion':
            getTractoresModificacion();
            break;
        case 'getUnidadesEmbarcadas':
            getUnidadesEmbarcadas();
            break;
        case 'getInsertTmp':
            getInsertTmp();
            break;
        case 'getExists':
            getExists();
            break;
        case 'getInsertUnidadesEmbarcadas';
             getInsertUnidadesEmbarcadas();            
            break;
        case 'getUpdUnidades';
             getUpdUnidades();            
            break;
        case 'getDeleteTmp';
             getDeleteTmp();            
            break;
         case 'getDeleteUnidades';
             getDeleteUnidades();            
            break;
        case 'getDistribuidor';
             getDistribuidor();            
            break;
        case 'getInsertTractor';
             getInsertTractor();            
            break;
        case 'getUnidadEspecial';
             getUnidadEspecial();            
            break;
         case 'borrarCancel';
             borrarCancel();            
            break;
         case 'getTalones';
             getTalones();            
            break;
         case 'getInsertarTalon';
             getInsertarTalon();            
            break;
         case 'getDeleteTalones';
             getDeleteTalones();            
            break;
         case 'getNumeroUnidades';
             getNumeroUnidades();            
            break;
         case 'ValidaNumTalones';
             ValidaNumTalones();            
            break;
         case 'validaNumUnid';
             validaNumUnid();            
            break;
        default:
            echo '';
    }

    function getTalones(){

        $sqlTalonesViaje=" SELECT tl.numeroUnidades as numero , tl.companiaRemitente,concat(tl.distribuidor,' - ',dis.descripcionCentro) as distribuidor"
                        ." FROM trtalonesviajestbl tl, cadistribuidorescentrostbl dis"
                        ." WHERE idViajeTractor='".$_REQUEST['modPreviajeIdViajeTractor']."'"
                        ." AND tl.distribuidor= dis.distribuidorCentro";
        $rstTalonesViaje = fn_ejecuta_query($sqlTalonesViaje);

        echo json_encode($rstTalonesViaje);

    }

    function getTractoresModificacion(){
        
        if($_REQUEST['modPreviajeCompaniaCmb'] == ""){
            $e[] = array('id'=>'modPreviajeCompaniaCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }        
                
        $sqlGetTractor = "SELECT tr.idTractor, tr.tractor ".
                        "FROM caTractoresTbl tr, trviajestractorestbl vt, cachoferestbl ch, caplazastbl pl1, caplazastbl pl2 ".
                        "WHERE tr.idTractor = vt.idTractor ".
                        "AND vt.claveChofer = ch.claveChofer ".
                        "AND vt.idPlazaOrigen = pl1.idPlaza ".
                        "AND vt.idPlazaDestino = pl2.idPlaza ".
                        "AND vt.viaje = (SELECT max(viaje) FROM trviajestractorestbl vt2 WHERE vt2.idViajeTractor = vt.idViajeTractor AND vt2.idTractor = vt.idTractor) ".
                        "AND vt.claveMovimiento IN('VV','VF')  ".                        
                        "AND tr.compania = '".$_REQUEST['modPreviajeCompaniaCmb']."'".
                        " ORDER BY tr.tractor ASC ";
        
        $rs = fn_ejecuta_query($sqlGetTractor);
              
        echo json_encode($rs);                     
    } 

    function getUnidadesEmbarcadas(){
        $getEmbarcadas = "SELECT al.avanzada, ue.vin, ud.distribuidor FROM trunidadesembarcadastbl ue, alultimodetalletbl ud ,alunidadestbl al ".
                        "WHERE ue.vin = ud.vin ".
                        "AND al.vin=ue.vin ".
                        "AND ue.idViajeTractor = '".$_REQUEST['modPreviajeIdViajeTractor']."' ";
        
        $rsEmbarcadas = fn_ejecuta_query($getEmbarcadas);
              
        echo json_encode($rsEmbarcadas);                          
    }  

    function getInsertTmp(){
        $insUnidTmp="INSERT INTO alunidadestmp (vin, avanzada,modulo, idusuario,ip,fecha )"
                    ." SELECT vin, substr(vin,10,8) as avanzada, 'mod. Previaje', '13' as idUsuario, '".$_REQUEST['modPreviajeIdViajeTractor']."' as ip , now() as fecha" 
                    ." FROM trunidadesembarcadastbl"
                    ." WHERE idViajeTractor ='".$_REQUEST['modPreviajeIdViajeTractor']."'";
        $rstInsUniTmp=fn_ejecuta_query($insUnidTmp);
    }

    function getExists(){
        $selExists=" SELECT * from alunidadestmp where avanzada='".$_REQUEST['alUnidadesAvanzadaHdn']."'";
        $rstSelExists=fn_ejecuta_query($selExists);

        echo json_encode($rstSelExists);

    }

    function getInsertUnidadesEmbarcadas(){

        $sqlDelTalEx = "SELECT * FROM trtalonesviajestmp WHERE idViajeTractor=".$_REQUEST['modPreviajeIdViajeHdn']." AND bloqueado = 1";
        $rstDelTalEx = fn_ejecuta_query($sqlDelTalEx);

        for ($i=0; $i <sizeof($rstDelTalEx['root']) ; $i++) { 
            $deleteTalones = "DELETE FROM trtalonesviajestbl ".
                            " WHERE  idViajeTractor = '".$rstDelTalEx['root'][$i]['idViajeTractor']."'".
                            " AND distribuidor = '".$rstDelTalEx['root'][$i]['distribuidor']."'".
                            " AND companiaRemitente = '".$rstDelTalEx['root'][$i]['companiaRemitente']."'".
                            " AND centroDistribucion = '".$rstDelTalEx['root'][$i]['centroDistribucion']."'".
                            " AND numerounidades =0 ";
            $rstDelTal = fn_ejecuta_query($deleteTalones);
        }


        $sqlAddUnidades = "INSERT INTO trUnidadesEmbarcadasTbl (centroDistribucion, idViajeTractor, vin, fechaEmbarque, claveMovimiento)  ".
                            " SELECT '".$_REQUEST['mdViajeCdistrHdn'] ."' as centroDistribucion, ip as idViajeTractor, ut.vin, now() as fechaEmbarque, 'E' as claveMovimiento".
                            " FROM alUnidadesTmp ut".
                            " WHERE ut.ip = '".$_REQUEST['modPreviajeIdViajeHdn']."' ".
                            " AND ut.vin NOT IN (SELECT vin FROM trUnidadesEmbarcadasTbl ue WHERE ue.vin = ut.vin)".
                            " AND ut.idUsuario = '".$_SESSION['idUsuario']."'".
                            " AND ut.modulo = 'mod. Previaje'";

        fn_ejecuta_query($sqlAddUnidades);
        //echo json_encode($sqlAddUnidades);

        $sqlInsTalones = "INSERT INTO trtalonesviajestbl (distribuidor, folio, idViajeTractor, companiaRemitente, idPlazaOrigen, idPlazaDestino, direccionEntrega, centroDistribucion, tipoTalon, fechaEvento, observaciones, numeroUnidades, "           . "importe, seguro, tarifaCobrar, kilometrosCobrar, impuesto, retencion, claveMovimiento, tipoDocumento, firmaElectronica) ".
                        " SELECT tl.distribuidor,tl.numeroTalon as folio , tl.idViajeTractor,tl.companiaRemitente, ca.idPlaza as idPlazaOrigen, pl.idPlaza as idPlazaDestino, ca.direccionEntrega, '".$_REQUEST['mdViajeCdistrHdn']."' as centroDistribucion, tl.tipoTalon,now() as fechaEvento ,'', tl.numeroUnidades, '0.00' as importe, '0.00' as seguro, '0.00' as tarifaCobrar, '0.00' as kilometrosCobrar, '0.00' as impuesto, '0.00' as retencion, 'TV' as claveMovimiento, 'TF' as tipoDocumento, null as firmaElectronica ".
                        " FROM  trtalonesviajestmp tl, cadistribuidorescentrostbl ca, cadistribuidorescentrostbl pl ".
                        " WHERE tl.idViajeTractor= ".$_REQUEST['modPreviajeIdViajeHdn'].
                        " AND ca.distribuidorCentro = '".$_REQUEST['distribuidor']."'".
                        " AND pl.distribuidorCentro = '".$_REQUEST['mdViajeCdistrHdn']."'".
                        " AND tl.bloqueado = 0";
        fn_ejecuta_query($sqlInsTalones);
        echo json_encode($sqlInsTalones);

        $sqlDelTmpTalones = "DELETE FROM trtalonesviajestmp WHERE idViajeTractor =".$_REQUEST['modPreviajeIdViajeHdn']." AND distribuidor = '".$_REQUEST['distribuidor']."'";
        fn_ejecuta_query($sqlDelTmpTalones);
        

        $sqlDltTmp= " DELETE FROM  alunidadestmp ".
                    " WHERE ip = '".$_REQUEST['modPreviajeIdViajeHdn']."' ".
                    " AND idUsuario = '".$_SESSION['idUsuario']."'".
                    " AND modulo = 'mod. Previaje'";   

        fn_ejecuta_query($sqlDltTmp);   

        $sqlDetTmpTrac="DELETE FROM trviajestractorestmp WHERE idViajeTractor='".$_REQUEST['modPreviajeIdViajeHdn']."'"." AND idTractor='".$_REQUEST['idTractor']."'";
        fn_ejecuta_query($sqlDetTmpTrac);         

        }


    function getUpdUnidades(){

        $sqlUpdUnidades="INSERT INTO alunidadestmp (vin, avanzada, modulo, idUsuario, ip, fecha)".
                        "VALUES ('".$_REQUEST['modPreviajeVinDsp']."', '"
                            .$_REQUEST['modPreviajeAvanzadaTxt']."', 'mod. Previaje', ".$_SESSION['idUsuario'].", '"
                            .$_REQUEST['modPreviajeIdViajeTractor']."', now())";
        fn_ejecuta_query($sqlUpdUnidades);

        $selUnid ="SELECT numeroUnidades FROM trviajestractorestbl WHERE idViajeTractor='".$_REQUEST['modPreviajeIdViajeTractor']."'";
        $rst = fn_ejecuta_query($selUnid);
        $unidad = $rst['root'][0]['numeroUnidades'] + 1;
        if (isset($_REQUEST['numeroUnidades']) && $_REQUEST['numeroUnidades'] != '') {
            $unidad = $_REQUEST['numeroUnidades'];
        }

        $updUnidades = "UPDATE trviajestractorestbl SET numeroUnidades = '".$unidad
                    ."' WHERE idViajeTractor ='".$_REQUEST['modPreviajeIdViajeTractor']."'";
        fn_ejecuta_query($updUnidades);

        $updUnidades = "UPDATE trtalonesviajestbl SET numeroUnidades = '".$unidad
                    ."' WHERE idViajeTractor ='".$_REQUEST['modPreviajeIdViajeTractor']."'";
        fn_ejecuta_query($updUnidades);

    }

    function getDeleteTmp(){
        $sqlDeleteTmp="DELETE FROM alunidadestmp WHERE ip='".$_REQUEST['modPreviajeIdViajeTractor']."'";
        fn_ejecuta_query($sqlDeleteTmp);
        echo json_encode($sqlDeleteTmp);

        $sqlDetTmpTrac="DELETE FROM trviajestractorestmp WHERE idViajeTractor='".$_REQUEST['modPreviajeIdViajeTractor']."'";//." AND idTractor='".$_REQUEST['idTractor']."'";
        fn_ejecuta_query($sqlDetTmpTrac); 
        echo json_encode($sqlDetTmpTrac);   

        $sqlDelTmpTalones = "DELETE FROM trtalonesviajestmp WHERE idViajeTractor =".$_REQUEST['modPreviajeIdViajeTractor'];
        fn_ejecuta_query($sqlDelTmpTalones);     

    }

    function getDeleteUnidades(){
        $idViajeTractorStr = "";
        if (isset($_REQUEST['idViajeTractor']) && $_REQUEST['idViajeTractor'] != '') {
            $idViajeTractorStr = $_REQUEST['idViajeTractor'];
        } else {
            $SelIdTrac = "SELECT * FROM alUnidadesTmp where vin='".$_REQUEST['vin']."'";
            $rstIdTrac = fn_ejecuta_query($SelIdTrac);
            $idViajeTractorStr = $rstIdTrac['root'][0]['ip'];
        }

        $selUnid ="SELECT numeroUnidades FROM trviajestractorestbl WHERE idViajeTractor='".$idViajeTractorStr."'";
        $rst = fn_ejecuta_query($selUnid);

        $unidad = $rst['root'][0]['numeroUnidades'] - 1 ;

        $updUnidades = "UPDATE trviajestractorestbl SET numeroUnidades = '".$unidad
                    ."' WHERE idViajeTractor ='".$idViajeTractorStr."'";
        fn_ejecuta_query($updUnidades);

        $updUnidades = "UPDATE trtalonesviajestbl SET numeroUnidades = '".$unidad
                    ."' WHERE idViajeTractor ='".$idViajeTractorStr."'";
        fn_ejecuta_query($updUnidades);

        $sqlDelUnidades="DELETE FROM trUnidadesEmbarcadasTbl WHERE vin='".$_REQUEST['vin']."'";
        fn_ejecuta_query($sqlDelUnidades);

        $sqlDelUnidades="DELETE From alUnidadesTmp WHERE vin='".$_REQUEST['vin']."'";
        fn_ejecuta_query($sqlDelUnidades);
    }

    function getDistribuidor(){

        $sql ="SELECT * FROM trtalonesviajestmp WHERE idViajeTractor='".$_REQUEST['modPreviajeIdViajeTractor']."'";
        $rst = fn_ejecuta_query($sql);
        //echo json_encode($rst);

        if ($rst['records'] >= '1') {
            $sqlDistribuidor=" SELECT 1 as existentes FROM trtalonesviajestbl tl, trtalonesviajestmp tr  WHERE tl.idViajeTractor='".$_REQUEST['modPreviajeIdViajeTractor']
                                    ."' AND tr.idViajeTractor = tr.idViajeTractor AND tr.distribuidor='".$_REQUEST['distribuidor']."'";
            $rs=fn_ejecuta_query($sqlDistribuidor);
            echo json_encode($rs); 

        }
        else{
            $sqlDistribuidor=" SELECT 1 as existentes FROM trtalonesviajestbl   WHERE idViajeTractor='".$_REQUEST['modPreviajeIdViajeTractor']."'"
                                    ." AND distribuidor='".$_REQUEST['distribuidor']."'";
            $rs=fn_ejecuta_query($sqlDistribuidor);
             echo json_encode($rs); 

        }



       
    }

    function getInsertTractor(){


        $sqlInsTractor="INSERT INTO trviajestractorestmp (idViajeTractor, idTractor, claveChofer, centroDistribucion, modulo, idUsuario, ip, fechaEvento)"
                     ." VALUES ('".$_REQUEST['modPreviajeIdViajeTractor']."', '"
                        .$_REQUEST['idTractor']."', null, '"
                        .$_REQUEST['cenDistribucion']."', 'mod. PreViaje', '13', '"
                        .$_REQUEST['modPreviajeIdViajeTractor']."',"
                        ."NOW()  )";
      fn_ejecuta_query($sqlInsTractor);
    }

    function getUnidadEspecial(){
 
        $sqlCveMto = "SELECT hu.claveMovimiento, hu.distribuidor, au.vin ".
                        "FROM alunidadestbl au,alhistoricounidadestbl hu ".
                        "WHERE au.vin = hu.vin ".
                        "AND HU.claveMovimiento = 'DS' ".
                        "AND au.avanzada = '".$_REQUEST['modPreviajeAvanzadaTxt']."' ";
        
        $rstCveMto = fn_ejecuta_query($sqlCveMto);
        echo json_encode($rstCveMto);  

    }

    function borrarCancel(){

        $sqlVin = "SELECT max(vin) as vin ,max(ip) as ip FROM alUnidadesTmp WHERE modulo='mod. Previaje' ".
                "AND fecha=(SELECT max(fecha) FROM alunidadestmp WHERE  modulo='mod. Previaje')";
        $rsVin = fn_ejecuta_query($sqlVin);

        $selUnid ="SELECT numeroUnidades FROM trviajestractorestbl WHERE idViajeTractor='".$rsVin['root'][0]['ip']."'";
        $rst = fn_ejecuta_query($selUnid);

        $unidad = $rst['root'][0]['numeroUnidades'] - 1 ;

        $updUnidades = "UPDATE trviajestractorestbl SET numeroUnidades = '".$unidad
                    ."' WHERE idViajeTractor ='".$rsVin['root'][0]['ip']."'";
        fn_ejecuta_query($updUnidades);    

        $sqlDelUnidades="DELETE FROM trUnidadesEmbarcadasTbl WHERE vin='".$rsVin['root'][0]['vin']."'";
        fn_ejecuta_query($sqlDelUnidades);

        $sqlDelUnidades="DELETE From alUnidadesTmp WHERE vin='".$rsVin['root'][0]['vin']."'";
        fn_ejecuta_query($sqlDelUnidades);
    }


    function getInsertarTalon(){

        $sqlInsTalon="INSERT INTO trtalonesviajestmp (idViajeTractor,numeroTalon, distribuidor, numeroUnidades, direccionEntrega, companiaRemitente, centroDistribucion, observaciones, tipoTalon, tarifaUnidades, bloqueado) ".
                       "VALUES (".$_REQUEST['idVTractor'].",(SELECT ifnull(SUM(numeroTalon + 1),1) AS numeroTotal FROM trtalonesviajestmp tmp1 WHERE tmp1.idViajeTractor=".$_REQUEST['idVTractor']." AND numeroTalon = (SELECT max(numeroTalon) FROM trtalonesviajestmp tmp2 WHERE tmp2.idViajeTractor = ".$_REQUEST['idVTractor'].")), '".substr($_REQUEST['modPreviajeDistribuidorCmb'],0,5)."', '".$_REQUEST['modPreviajeNumeroUnidadesTxt']."', null, '".$_REQUEST['modPreviajeRemitenteCmb']."', '".$_REQUEST['ciaSesVal']."', null, 'NA', null, ".$_REQUEST['modPreviajeHdn'].")";
        fn_ejecuta_query($sqlInsTalon);

            //echo json_encode($sqlInsTalon);
    }

    function getDeleteTalones(){
        $sqlDelTalones="DELETE FROM trtalonesviajestbl WHERE distribuidor ='".substr($_REQUEST['distribuidor'],0,5)."' ".
                      " AND companiaRemitente = '".$_REQUEST['remitente']."'".
                      " AND idViajeTractor=".$_REQUEST['idVTractor'];
        fn_ejecuta_query($sqlDelTalones);
    }


    function getNumeroUnidades(){
        $sqlCountUnidades = "SELECT sum(tl.numeroUnidades) as tl   FROM trtalonesviajestbl tl WHERE tl.idViajeTractor = ".$_REQUEST['idVTractor'];
        $rstUnidades = fn_ejecuta_query($sqlCountUnidades);
        //echo json_encode($rstUnidades);

        $sqlCountUnidades1 = "SELECT sum(tm.numeroUnidades) as tm   FROM trtalonesviajestmp tm WHERE tm.idViajeTractor = ".$_REQUEST['idVTractor'];
        $rstUnidades1 = fn_ejecuta_query($sqlCountUnidades1);
        //echo json_encode($rstUnidades1);


        if ($rstUnidades1['root'][0]['tm'] == null) {
            $rstUnidades1['root'][0]['tm'] = 0;
            //echo json_encode($rstUnidades1);

        }

        $unidTal = $rstUnidades['root'][0]['tl'] + $rstUnidades1['root'][0]['tm'];
        //echo $unidTal;
////////////////////////////////////////////////////////////////////////////////////////////////talones
        $sqlNumUnid = "SELECT COUNT(*) as reg FROM trunidadesembarcadastbl WHERE idViajeTractor = ".$_REQUEST['idVTractor'];
        $rstNumUnid = fn_ejecuta_query($sqlNumUnid);
        //echo json_encode($rstNumUnid);

        $sqlNumUnid1 = "SELECT COUNT(*) as tmp FROM alunidadestmp WHERE ip = ".$_REQUEST['idVTractor'];
        $rstNumUnid1 = fn_ejecuta_query($sqlNumUnid1);
        //echo json_encode($rstNumUnid1);

        $unidUnid = $rstNumUnid['root'][0]['reg'] + ($rstNumUnid1['root'][0]['tmp'] - $rstNumUnid['root'][0]['reg']);
        //echo $unidUnid;


        if ($unidTal == $unidUnid) {
            $rstUnidades['records'] = 1;
            echo json_encode($rstUnidades);
        }else{
            $rstUnidades['records'] = null;
            echo json_encode($rstUnidades);
        }
    }

    function ValidaNumTalones(){

        $sqlNumTal = "SELECT count(*) as numTal, numerounidades FROM trtalonesviajestbl WHERE idViajeTractor = ".$_REQUEST['idVTractor'];
        $rstSqlTal = fn_ejecuta_query($sqlNumTal);

        echo json_encode($rstSqlTal);
    }

    function validaNumUnid(){
         $getEmbarcadas = "SELECT al.avanzada, ue.vin, ud.distribuidor FROM trunidadesembarcadastbl ue, alultimodetalletbl ud ,alunidadestbl al ".
                        "WHERE ue.vin = ud.vin ".
                        "AND al.vin=ue.vin ".
                        "AND ue.idViajeTractor = '".$_REQUEST['idVTractor']."' ";
        
        $rsEmbarcadas = fn_ejecuta_query($getEmbarcadas);
              
        echo json_encode($rsEmbarcadas);                          
    }


?>    