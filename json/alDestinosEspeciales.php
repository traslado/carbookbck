     <?php
        session_start();

        require_once("../funciones/generales.php");
        require_once("../funciones/construct.php");
        require_once("../funciones/utilidades.php");
        require_once("alUnidades.php");

        switch($_SESSION['idioma']){
            case 'ES':
                include_once("../funciones/idiomas/mensajesES.php");
                break;
            case 'EN':
                include_once("../funciones/idiomas/mensajesEN.php");
                break;
            default:
                include_once("../funciones/idiomas/mensajesES.php");
        }

        switch($_REQUEST['alDestinosEspecialesActionHdn']){
            case 'getDestinosEspeciales':
                echo json_encode(getDestinosEspeciales());
                break;
            case 'getUltimoDestinoEspecialUnidad':
                getUltimoDestinoEspecialUnidad();
                break;
            case 'getCambioDestino':
                getCambioDestino();
                break;
            case 'getUltDet':
                getUltDet();
                break;                
            case 'addDestinoEspecial':
                addDestinoEspecial();
                break;
            case 'updDestinoEspecial':
                updDestinoEspecial();
                break;
            case 'dltDestinoEspecial':
                dltDestinoEspecial();
                break;
            case 'cancelarServicioEspecial':
                cancelarServicioEspecial();
                break;
            case 'addCambioDestino':
                addCambioDestino();
                break;
            case 'dltCambioDestino':
                dltCambioDestino();
                break;
            case 'addCambioDestinoProgramado':
                addCambioDestinoProgramado();
                break;
            case 'dltCambioDestinoProgramado':
                dltCambioDestinoProgramado();
                break;
            case 'getDestinosEspecialesUnico':
                echo json_encode(getDestinosEspecialesUnico());
                break;
            case 'sqlGetCancelacion':
                sqlGetCancelacion();
                break;
            case 'addUndTmp':
                addUndTmp();
                break;
            case 'getUndTmp':
                getUndTmp();
                break;
            case 'DltCancTmp':
                DltCancTmp();
                break;
            case 'DltUndTmp':
                DltUndTmp();
                break;
            case 'cancservEsp':
                cancservEsp();
                break;
            case 'updCambioCentro':
                updCambioCentro();
                break;
            case 'addUndTmpCCD':
                addUndTmpCCD();
                break;
            case 'getUndTmpCCD':
                getUndTmpCCD();
                break;
            case 'DltUndTmpCCD':
                DltUndTmpCCD();
                break;
            case 'addCambioDeSE':
                addCambioDeSE();
                break;
            case 'validarST':
                validarST();
                break;
            default:
                echo '';
        }

        function getDestinosEspeciales(){
            $lsWhereStr = "WHERE de.vin = h.vin ".
                          /*"AND h.fechaEvento=(".
                          "SELECT MAX(h1.fechaEvento) ".
                          "FROM alHistoricoUnidadesTbl h1 ".
                          "WHERE h1.vin = de.vin) ".*/
                          "AND de.idDestinoEspecial =(select MAX(de1.idDestinoEspecial) from aldestinosespecialestbl de1 where de1.vin=u.vin)".
                          "AND h.vin = u.vin ".
                          "AND su.simboloUnidad = u.simboloUnidad ".
                          "AND tf.idTarifa = de.idTarifa ";

            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesIdDestinoHdn'], "de.idDestinoEspecial", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesVinTxt'], "de.vin", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDistOrigenHdn'], "de.distribuidorOrigen", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDirOrigenHdn'], "de.direccionOrigen", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesPlazaOrigenHdn'], "de.idPlazaOrigen", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDistDestinoHdn'], "de.distribuidorDestino", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDirDestinoHdn'], "de.direccionDestino", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesPlazaDestinoHdn'], "de.idPlazaDestino", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesImporteCobTxt'], "de.importeCob", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesSueldoTxt'], "de.sueldo", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesTarifaHdn'], "de.idTarifa", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesCentroDistHdn'], "h.centroDistribucion", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesAvanzadaHdn'], "u.avanzada", 2);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }

            $sqlGetDestinosEspStr = "SELECT de.*, h.centroDistribucion, u.avanzada, tf.tarifa, tf.descripcion AS nombreTarifa, ".
                                    "h.claveMovimiento, u.simboloUnidad, concat(u.simboloUnidad,' - ',su.descripcion) as descSimbolo,u.color, su.descripcion AS nombreSimbolo, ".
                                    "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = su.marca) AS nombreMarca, ".
                                    "(SELECT co.descripcion FROM caColorUnidadesTbl co ".
                                        "WHERE co.color = u.color AND co.marca = su.marca) AS nombreColor, ".
                                    "(SELECT dc.descripcionCentro FROM caDistribuidoresCentrosTbl dc ".
                                        "WHERE dc.distribuidorCentro = de.distribuidorOrigen) AS nombreDistOrigen, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = de.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                    "(SELECT dc1.descripcionCentro FROM caDistribuidoresCentrosTbl dc1 ".
                                        "WHERE dc1.distribuidorCentro = de.distribuidorDestino) AS nombreDistDestino, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = de.idPlazaDestino) AS nombrePlazaDestino, ".
                                    "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                        "WHERE dc2.distribuidorCentro = h.centroDistribucion) AS nombreCentroDist, ".
                                    "(SELECT 1 FROM caGeneralesTbl g2 WHERE g2.tabla = 'unidadAsignar' ".
                                            "AND g2.columna = 'claveValida' AND g2.valor = h.claveMovimiento) AS listaAsignar, ".
                                    "(select hu.claveMovimiento from alhistoricounidadestbl hu where hu.vin =u.vin and claveMovimiento != 'CA' order by fechaEvento desc limit 0,1) as ultimohistorico,".
                                    "(select hu.claveMovimiento from alhistoricounidadestbl hu where hu.vin =u.vin and claveMovimiento != 'CA' order by fechaEvento desc limit 1,1) as penultimohistorico ".
                                    "FROM alDestinosEspecialesTbl de, alUltimoDetalleTbl h, alUnidadesTbl u, ".
                                        "caTarifasTbl tf, caSimbolosUnidadesTbl su ".$lsWhereStr;

            $rs = fn_ejecuta_query($sqlGetDestinosEspStr);
            //echo json_encode($sqlGetDestinosEspStr)
          /*  if($rs['root'][0]['ultimohistorico'] == 'RC' && $rs['root'][0]['penultimoHistorico'] != 'DS' || $rs['root'][0]['ultimohistorico'] == 'RC' && $rs['root'][0]['penultimoHistorico'] == null )
            {*/

            for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
                $rs['root'][$nInt]['descDistOrigen'] = $rs['root'][$nInt]['distribuidorOrigen']." - ".$rs['root'][$nInt]['nombreDistOrigen'];
                $rs['root'][$nInt]['descPlazaOrigen'] = $rs['root'][$nInt]['idPlazaOrigen']." - ".$rs['root'][$nInt]['nombrePlazaOrigen'];
                $rs['root'][$nInt]['descDistDestino'] = $rs['root'][$nInt]['distribuidorDestino']." - ".$rs['root'][$nInt]['nombreDistDestino'];
                $rs['root'][$nInt]['descPlazaDestino'] = $rs['root'][$nInt]['idPlazaDestino']." - ".$rs['root'][$nInt]['nombrePlazaDestino'];
                $rs['root'][$nInt]['descTarifa'] = $rs['root'][$nInt]['tarifa']." - ".$rs['root'][$nInt]['nombreTarifa'];
                $rs['root'][$nInt]['descCentroDist'] = $rs['root'][$nInt]['centroDistribucion']." - ".$rs['root'][$nInt]['nombreCentroDist'];
                $rs['root'][$nInt]['descCentroDist'] = $rs['root'][$nInt]['centroDistribucion']." - ".$rs['root'][$nInt]['listaAsignar'];
            }

            return $rs;
        //}

    }

        function getUltimoDestinoEspecialUnidad(){
            $lsWhereStr = "WHERE de.idDestinoEspecial = (".
                          "SELECT MAX(de1.idDestinoEspecial) ".
                          "FROM alDestinosEspecialesTbl de1 ".
                          "WHERE de1.vin = de.vin) ".
                          "AND de.vin = h.vin ".
                          "AND h.fechaEvento=(".
                          "SELECT MAX(h1.fechaEvento) ".
                          "FROM alHistoricoUnidadesTbl h1 ".
                          "WHERE h1.vin = de.vin) ".
                          "AND h.vin = u.vin ".
                          "AND tf.idTarifa = de.idTarifa ";

            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesIdDestinoHdn'], "de.idDestinoEspecial", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesVinTxt'], "de.vin", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDistOrigenHdn'], "de.distribuidorOrigen", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDirOrigenHdn'], "de.direccionOrigen", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesPlazaOrigenHdn'], "de.idPlazaOrigen", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDistDestinoHdn'], "de.distribuidorDestino", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDirDestinoHdn'], "de.direccionDestino", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesPlazaDestinoHdn'], "de.idPlazaDestino", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesImporteCobTxt'], "de.importeCob", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesSueldoTxt'], "de.sueldo", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesTarifaHdn'], "de.idTarifa", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesCentroDistHdn'], "h.centroDistribucion", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesAvanzadaHdn'], "u.avanzada", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }

            $sqlGetDestinosEspStr = "SELECT de.*, h.centroDistribucion, u.avanzada, tf.tarifa, tf.descripcion AS nombreTarifa, ".
                                    "(SELECT dc.descripcionCentro FROM caDistribuidoresCentrosTbl dc ".
                                        "WHERE dc.distribuidorCentro = de.distribuidorOrigen) AS nombreDistOrigen, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = de.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                    "(SELECT dc1.descripcionCentro FROM caDistribuidoresCentrosTbl dc1 ".
                                        "WHERE dc1.distribuidorCentro = de.distribuidorDestino) AS nombreDistDestino, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = de.idPlazaDestino) AS nombrePlazaDestino, ".
                                    "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                        "WHERE dc2.distribuidorCentro = h.centroDistribucion) AS nombreCentroDist ".
                                    "FROM alDestinosEspecialesTbl de, alHistoricoUnidadesTbl h, alUnidadesTbl u, caTarifasTbl tf ".$lsWhereStr;

            $rs = fn_ejecuta_query($sqlGetDestinosEspStr);

            for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
                $rs['root'][$nInt]['descDistOrigen'] = $rs['root'][$nInt]['distribuidorOrigen']." - ".$rs['root'][$nInt]['nombreDistOrigen'];
                $rs['root'][$nInt]['descPlazaOrigen'] = $rs['root'][$nInt]['idPlazaOrigen']." - ".$rs['root'][$nInt]['nombrePlazaOrigen'];
                $rs['root'][$nInt]['descDistDestino'] = $rs['root'][$nInt]['distribuidorDestino']." - ".$rs['root'][$nInt]['nombreDistDestino'];
                $rs['root'][$nInt]['descPlazaDestino'] = $rs['root'][$nInt]['idPlazaDestino']." - ".$rs['root'][$nInt]['nombrePlazaDestino'];
                $rs['root'][$nInt]['descTarifa'] = $rs['root'][$nInt]['tarifa']." - ".$rs['root'][$nInt]['nombreTarifa'];
                $rs['root'][$nInt]['descCentroDist'] = $rs['root'][$nInt]['centroDistribucion']." - ".$rs['root'][$nInt]['nombreCentroDist'];
            }

            echo json_encode($rs);
        }

        function getUltDet(){
            $a = array('success' => true, 'msjResponse' => '');
            
            $sql = "SELECT DISTINCT a.claveMovimiento FROM alUltimoDetalleTbl a, alUnidadesTbl u".
                   " WHERE u.avanzada = '".$_REQUEST['avanzada']."'".
                   " AND a.vin = u.vin";
            //echo "$sql<br>";
            $rsUD = fn_ejecuta_query($sql);
            
            if($rsUD['records'] > 0)
            {
            		if($rsUD['root'][0]['claveMovimiento'] == 'CT')
            		{
            				$a['success'] = false;
            				$a['msjResponse'] = 'La unidad ya tiene Cambio de Destino.';
            		}
            }            
            echo json_encode($a);
        }

        function getCambioDestino(){
            $lsWhereStr = "";

            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesIdDestinoHdn'], "de.idDestinoEspecial", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesVinTxt'], "de.vin", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDistOrigenHdn'], "de.distribuidorOrigen", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDirOrigenHdn'], "de.direccionOrigen", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesPlazaOrigenHdn'], "de.idPlazaOrigen", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDistDestinoHdn'], "de.distribuidorDestino", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDirDestinoHdn'], "de.direccionDestino", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesPlazaDestinoHdn'], "de.idPlazaDestino", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesImporteCobTxt'], "de.importeCob", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesSueldoTxt'], "de.sueldo", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesTarifaHdn'], "de.idTarifa", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }

            $sqlGetCambioDestinoStr = "SELECT de.* FROM alDestinosEspecialesTbl de ".$lsWhereStr;

            $rs = fn_ejecuta_query($sqlGetCambioDestinoStr);

            echo json_encode($rs);
        }

        //SERVICIO ESPECIAL
        function addDestinoEspecial(){
            $a = array();
            $e = array();
            $a['success'] = true;

            $vinArr = explode('|', substr($_REQUEST['trap010VinTxt'], 0, -1));
            if(in_array('', $vinArr)){
                $e[] = array('id'=>'trap010VinTxt','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            $colorArr = explode('|', substr($_REQUEST['trap010ColorHdn'], 0, -1));
            if(in_array('', $colorArr)){
                $e[] = array('id'=>'trap010ColorHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            $simboloArr = explode('|', substr($_REQUEST['trap010SimboloHdn'], 0, -1));
            if(in_array('', $simboloArr)){
                $e[] = array('id'=>'trap010SimboloHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap010CentroDistHdn'] == ""){
                $e[] = array('id'=>'trap010CentroDistHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap010DistOrigenHdn'] == ""){
                $e[] = array('id'=>'trap010DistOrigenHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap010DistDestinoHdn'] == ""){
                $e[] = array('id'=>'trap010DistDestinoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            if ($a['success'] == true) {
                $errorArr = array();
                $sinTarifaArr = array();
                $success = false;

                $nuevaArr = explode('|', substr($_REQUEST['trap010UnidadNuevaHdn'], 0, -1));
                $detenidasArr = explode('|', substr($_REQUEST['trap010DetenidasHdn'], 0, -1));

                $conceptosArr = explode('|', substr($_REQUEST['trap010ConceptosHdn'], 0, -1));
                $importesArr = explode('|', substr($_REQUEST['trap010ImportesHdn'], 0, -1));

                for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {

                    $sqlGetTarifaStr = "SELECT tf.idTarifa ".
                                       "FROM caTarifasTbl tf, caClasificacionTarifasTbl ct, caSimbolosUnidadesTbl su ".
                                       "WHERE su.simboloUnidad = '".$simboloArr[$nInt]."' ".
                                       "AND ct.clasificacion = su.clasificacion ".
                                       "AND tf.idTarifa = ct.idTarifa ".
                                       "AND tf.tipoTarifa = 'E'";

                    $rs = fn_ejecuta_query($sqlGetTarifaStr);

                    if ($rs['records'] > 0) {
                        $tarifa = $rs['root'][0]['idTarifa'];
                    } else {
                        $a['success'] = false;
                        array_push($sinTarifaArr, $vinArr[$nInt]);
                    }

                    if($a['success']) {
                        if($nuevaArr[$nInt] == "1"){
                            //Si es nueva la unidad, ya con la tarifa se inserta en alUnidadesTbl
                            $data = addUnidad($vinArr[$nInt],$_REQUEST['trap010DistDitribuidorUndHdn'],$simboloArr[$nInt],
                                                $colorArr[$nInt],$_REQUEST['trap010CentroDistHdn'],'RC',$tarifa,
                                                'TEMP','','','');
                        }else{

                            $sqlGetDistribuidor = "SELECT distribuidor FROM alultimodetalletbl ".
                                                  "WHERE vin= '".$vinArr[$nInt]."' ";
                            $sqlRS = fn_ejecuta_query($sqlGetDistribuidor);

                            $data = addHistoricoUnidad($_REQUEST['trap010CentroDistHdn'],$vinArr[$nInt],'RC',
                                                        $sqlRS['root'][0]['distribuidor'],$tarifa,'TEMP',
                                                        '','',$_SESSION['idUsuario']);
                        }if($data['success']) {
                            //Se obtienen las direcciones y plazas del distribuidor Origen y Destino
                            $sqlGetDireccionesPlazasDistStr = "SELECT distribuidorCentro, idPlaza, direccionEntrega ".
                                                              "FROM caDistribuidoresCentrosTbl ".
                                                              "WHERE distribuidorCentro ='".$_REQUEST['trap010DistOrigenHdn']."' ".
                                                              "OR distribuidorCentro ='".$_REQUEST['trap010DistDestinoHdn']."'";

                            $rs = fn_ejecuta_query($sqlGetDireccionesPlazasDistStr);

                            for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
                                if ($rs['root'][$iInt]['distribuidorCentro'] == $_REQUEST['trap010DistOrigenHdn']) {
                                    $dirOrigen = $rs['root'][$iInt]['direccionEntrega'];
                                    $plazaOrigen = $rs['root'][$iInt]['idPlaza'];
                                }
                                if ($rs['root'][$iInt]['distribuidorCentro'] == $_REQUEST['trap010DistDestinoHdn']) {
                                    $dirDestino = $rs['root'][$iInt]['direccionEntrega'];
                                    $plazaDestino = $rs['root'][$iInt]['idPlaza'];
                                }
                            }
                            $sqlAddDestinoEspecialStr = "INSERT INTO alDestinosEspecialesTbl ".
                                                    "(idDestinoEspecial, vin, distribuidorOrigen, direccionOrigen, idPlazaOrigen, distribuidorDestino,".
                                                    "direccionDestino, idPlazaDestino, importeCob, sueldo, idTarifa,tipoServEsp,numeroST) VALUES(".
                                                    "(SELECT IFNULL(MAX(de2.idDestinoEspecial),0) + 1 FROM alDestinosEspecialesTbl de2 ".
                                                        "WHERE de2.vin = '".$vinArr[$nInt]."'),".
                                                    "'".$vinArr[$nInt]."','".$_REQUEST['trap010DistOrigenHdn']."',".
                                                    $dirOrigen.",".$plazaOrigen.",'".
                                                    $_REQUEST['trap010DistDestinoHdn']."',".$dirDestino.",".
                                                    $plazaDestino.",0.00,0.00,".$tarifa.",'".$_REQUEST['trap010TipoCmb']."',".replaceEmptyNull("'".$_REQUEST['trap010STTxt']."'").")";

                            fn_ejecuta_Add($sqlAddDestinoEspecialStr);

                            $updLocUnidad = "UPDATE alhistoricounidadestbl".
                                            "SET localizacionUnidad = (SELECT idDestinoEspecial ".
                                            "FROM aldestinosespecialestbl ".
                                            "WHERE VIN = '".$vinArr[$nInt]."') ".
                                            "WHERE claveMovimiento = 'RC' ".
                                            "AND VIN = '".$vinArr[$nInt]."' ".
                                            "AND localizacionUnidad = 'TEMP'";
                            fn_ejecuta_query($updLocUnidad);

                            $updUdLoc = "UPDATE alUltimoDetalleTbl".
                                            "SET localizacionUnidad = (SELECT idDestinoEspecial ".
                                            "FROM aldestinosespecialestbl ".
                                            "WHERE VIN = '".$vinArr[$nInt]."') ".
                                            "WHERE claveMovimiento = 'RC' ".
                                            "AND VIN = '".$vinArr[$nInt]."' ".
                                            "AND localizacionUnidad = 'TEMP'";

                            fn_ejecuta_query($updUdLoc);

                            $sqlUpdLibera = "UPDATE alUnidadesDetenidasTbl ".
                                            "SET claveMovimiento = 'UL', ".
                                            "centroLibera = '".$_REQUEST['trap010CentroDistHdn']."', ".
                                            "fechaFinal = NOW(), ".
                                            "numeroMovimiento = 2 ".
                                            "WHERE vin = '".$vinArr[$nInt]."'";

                            fn_ejecuta_Add($sqlUpdLibera);

                                $sqlGetUltimoID =   "SELECT ".
                                                    "(SELECT MAX(idDestinoEspecial) FROM alDestinosEspecialesTbl ".
                                                        "WHERE vin = '".$vinArr[$nInt]."') AS idDestinoEspecial,".
                                                    "(SELECT MAX(idHistorico) FROM alHistoricoUnidadesTbl ".
                                                        "WHERE vin = '".$vinArr[$nInt]."') AS idHistorico";

                                $rsId = fn_ejecuta_query($sqlGetUltimoID);

                                if(sizeof($rsId['root']) > 0){
                                    $updHistorico = "UPDATE alHistoricoUnidadesTbl ".
                                                    "SET localizacionUnidad = '".$rsId['root'][0]['idDestinoEspecial']."' ".
                                                    "WHERE idHistorico = ".$rsId['root'][0]['idHistorico'];

                                    fn_ejecuta_Upd($updHistorico);

                                    $updUD = "UPDATE alUltimoDetalleTbl ".
                                                    "SET localizacionUnidad = '".$rsId['root'][0]['idDestinoEspecial']."' ".
                                                    "WHERE vin = '".$vinArr[$nInt]."';";

                                    fn_ejecuta_Upd($updUD);
                                } else {
                                    $a['success'] = false;
                                    array_push($errorArr, $vinArr[$nInt]);
                                }


                            if($a['success']){
                                if($_REQUEST['trap010ConceptosHdn'] != "" && $_REQUEST['trap010ImportesHdn'] != ""){
                                    $data = addGastosEspeciales($_REQUEST['trap010DistOrigenHdn'], $vinArr[$nInt], $conceptosArr, $importesArr);
                                }

                                if ($detenidasArr[$nInt] == 1) {
                                    $sqlGetNumMovimiento = "SELECT MAX(numeroMovimiento) AS numeroMovimiento FROM alUnidadesDetenidasTbl ".
                                                           "WHERE vin = '".$vinArr[$nInt]."'";

                                    $rsLib = fn_ejecuta_query($sqlGetNumMovimiento);

                                    $sqlLiberarUnidadStr = "UPDATE alUnidadesDetenidasTbl SET ".
                                                           "claveMovimiento = 'UL', ".
                                                           "centroLibera = '".$_REQUEST['trap010CentroDistHdn']."', ".
                                                           "fechaFinal = '".date("Y-m-d H:i:s")."' ".
                                                           "WHERE vin = '".$vinArr[$nInt]."' ".
                                                           "AND numeroMovimiento = ".$rsLib['root'][0]['numeroMovimiento'];

                                    $rsLib = fn_ejecuta_Upd($sqlLiberarUnidadStr);
                                }
                            }


                        } else {
                            $a['success'] = false;
                            array_push($errorArr, $vinArr[$nInt]);
                        }
                    }
                }

                //Concatenate Errors
                if (sizeof($errorArr) > 0) {
                    if ($success == true) {
                        $a['errorMessage'] =  getDestinosEspecialesSuccessMsg()."<br>".getDestinosEspecialesFailedMsg();
                        foreach ($errorArr as $error) {
                            $a['errorMessage'] .= "<br>".$error;
                        }
                    } else if ($success == false){
                        $a['errorMessage'] = getDestinosEspecialesFailedMsg();
                        foreach ($errorArr as $error) {
                            $a['errorMessage'] .= "<br>".$error;
                        }
                    }
                } else {
                    if(sizeof($sinTarifaArr) > 0){
                        if($success == true){
                            $a['errorMessage'] = getDestinosEspecialesSuccessMsg()."<br>".getDestinosEspecialesSinTarifaEspecialMsg();
                            foreach ($sinTarifaArr as $unidad) {
                                $a['errorMessage'] .= "<br>".$unidad;
                            }
                        } else {
                            $a['errorMessage'] = getDestinosEspecialesSinTarifaEspecialMsg();
                            foreach ($sinTarifaArr as $unidad) {
                                $a['errorMessage'] .= "<br>".$unidad;
                            }
                        }
                    } else {
                        $a['successMessage'] = getDestinosEspecialesSuccessMsg();
                    }
                }
            }

            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
            echo json_encode($a);
        }

        function updDestinoEspecial(){
            $a = array();
            $e = array();
            $a['success'] = true;

            if($_REQUEST['trap045IdDestinoEspecialHdn'] == ""){
                $e[] = array('id'=>'trap045IdDestinoEspecialHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap045VinHdn'] == ""){
                $e[] = array('id'=>'trap045VinHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap045DistOrigenHdn'] == ""){
                $e[] = array('id'=>'trap045DistOrigenHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap045DirOrigenHdn'] == ""){
                $e[] = array('id'=>'trap045DirOrigenHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap045PlazaOrigenHdn'] == ""){
                $e[] = array('id'=>'trap045PlazaOrigenHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap045DistDestinoHdn'] == ""){
                $e[] = array('id'=>'trap045DistDestinoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap045DirDestinoHdn'] == ""){
                $e[] = array('id'=>'trap045DirDestinoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap045PlazaDestinoHdn'] == ""){
                $e[] = array('id'=>'trap045PlazaDestinoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap045IdTarifaHdn'] == ""){
                $e[] = array('id'=>'trap045IdTarifaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            if ($a['success'] == true) {
                $sqlUpdDestinoEspStr = "UPDATE alDestinosEspecialesTbl ".
                                       "SET vin = '".$_REQUEST['trap045VinHdn']."', ".
                                       "distribuidorOrigen = '".$_REQUEST['trap045DistOrigenHdn']."', ".
                                       "direccionOrigen = ".$_REQUEST['trap045DirOrigenHdn'].",".
                                       "idPlazaOrigen = ".$_REQUEST['trap045PlazaOrigenHdn'].",".
                                       "distribuidorDestino = '".$_REQUEST['trap045DistDestinoHdn']."',".
                                       "direccionDestino = ".$_REQUEST['trap045DirDestinoHdn'].",".
                                       "idPlazaDestino = ".$_REQUEST['trap045PlazaDestinoHdn'].",".
                                       "importeCob = ".replaceEmptyDec($_REQUEST['trap045ImporteCobHdn']).",".
                                       "sueldo = ".replaceEmptyDec($_REQUEST['trap045SueldoHdn']).",".
                                       "idTarifa = ".$_REQUEST['trap045IdTarifaHdn']." ".
                                       "WHERE idDestinoEspecial = ".$_REQUEST['trap045IdDestinoEspecialHdn'];

                $rs = fn_ejecuta_Upd($sqlUpdDestinoEspStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlUpdDestinoEspStr;
                    $a['successMessage'] = getDestinosEspecialesUpdMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdDestinoEspStr;
                }
            }
            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
            echo json_encode($a);
        }

        function dltDestinoEspecial(){
            $a = array();
            $e = array();
            $a['success'] = true;

            if($_REQUEST['trap733VinHdn'] == ""){
                $e[] = array('id'=>'trap733VinHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap733CentroDistHdn'] == ""){
                $e[] = array('id'=>'trap733CentroDistHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap733DistribuidorHdn'] == ""){
                $e[] = array('id'=>'trap733DistribuidorHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap733TarifaHdn'] == ""){
                $e[] = array('id'=>'trap733TarifaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['trap733LocalizacionHdn'] == ""){
                $e[] = array('id'=>'trap733LocalizacionHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            if($a['success'] == true){
                $sqlGetUltimoDestinoEspecialStr = "SELECT MAX(idDestinoEspecial) AS idDestinoEspecial FROM alDestinosEspecialesTbl ".
                                                  "WHERE vin = '".$_REQUEST['trap733VinHdn']."'";

                $rs = fn_ejecuta_query($sqlGetUltimoDestinoEspecialStr);

                $idDestino = $rs['root'][0]['idDestinoEspecial'];

                if($idDestino > 0){
                    $sqlDltDestinoEspecialStr = "DELETE FROM alDestinosEspecialesTbl ".
                                                "WHERE vin = '".$_REQUEST['trap733VinHdn']."' ".
                                                "AND idDestinoEspecial = ".$idDestino;

                    $rs = fn_ejecuta_query($sqlDltDestinoEspecialStr);

                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {

                        $sqlCheckDetenidaNuevaStr = "SELECT vin, MAX(numeroMovimiento) AS numeroMovimiento FROM alUnidadesDetenidasTbl ".
                                                    "WHERE vin = '".$_REQUEST['trap733VinHdn']."' ".
                                                    "AND numeroMovimiento = (SELECT max(numeroMovimiento) ".
                                                        "FROM alunidadesdetenidastbl WHERE vin = '".$_REQUEST['trap733VinHdn']."') ".
                                                    "AND centroLibera IS NOT null AND fechaFinal IS NOT NULL ";

                        $rs = fn_ejecuta_query($sqlCheckDetenidaNuevaStr);
                        $ultimaDetencion = $rs['root'][0]['numeroMovimiento'];

                        if ($rs['root'][0]['vin'] != "") {
                            $sqlLiberarUnidadStr = "UPDATE alUnidadesDetenidasTbl SET ".
                                                   "claveMovimiento = 'UD', ".
                                                   "centroLibera = NULL, ".
                                                   "fechaFinal = NULL ".
                                                   "WHERE vin = '".$_REQUEST['trap733VinHdn']."' ".
                                                    "AND numeroMovimiento = ".$ultimaDetencion;

                            $rs = fn_ejecuta_Upd($sqlLiberarUnidadStr);

                            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                            } else {
                                $a['success'] = false;
                                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlLiberarUnidadStr;
                            }
                        }
                    }
                }

                if($a['success'] == true){
                    $data = addHistoricoUnidad($_REQUEST['trap733CentroDistHdn'],$_REQUEST['trap733VinHdn'],'MC',$_REQUEST['trap733DistribuidorHdn'],$_REQUEST['trap733TarifaHdn'],$_REQUEST['trap733LocalizacionHdn'],'','',$_SESSION['idUsuario']);

                    if ($data['success'] == true) {
                        $a['successMessage'] = getDestinosEspecialesDltMsg();
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $data['errorMessage'];
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltDestinoEspecialStr;
                }
            }

            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
            echo json_encode($a);
        }

        function cancelarServicioEspecial(){


            //Eliminar el ultimo LA de Historico
            /*$sqlGetUltimoHistoricoStr = "SELECT  hu.idHistorico FROM alHistoricoUnidadesTbl hu ".
                                    "WHERE hu.vin = '".$_REQUEST['bajaServiciosEspecialesVinHdn']."' ".
                                    "ORDER BY fechaEvento DESC LIMIT 2";

            $rs = fn_ejecuta_query($sqlGetUltimoHistoricoStr);

            $sqlDltUltimoLAStr = "DELETE FROM alHistoricoUnidadesTbl WHERE idHistorico = ".$rs['root'][0]['idHistorico'];*/

            //fn_ejecuta_query($sqlDltUltimoLAStr);

            //Actualiar el RC con MC
            $sqlUpdCancelarStr = "UPDATE alHistoricoUnidadesTbl hu SET ".
                                 "hu.claveMovimiento = 'MC' ".
                                 "WHERE hu.vin = '".$_REQUEST['bajaServiciosEspecialesVinHdn']."' ".
                                 "AND hu.claveMovimiento = 'RC'";

            fn_ejecuta_Upd($sqlUpdCancelarStr);

                                //Quitar la unidad de destinos especiales
            /*$sqlDltDestinoEspecialStr = "DELETE FROM alDestinosEspecialesTbl ".
                                                "WHERE vin = '".$_REQUEST['bajaServiciosEspecialesVinHdn']."'";*/

            $sqlDltDestinoEspecialStr = "DELETE ".
                                        "FROM alDestinosEspecialesTbl  de ".
                                        "WHERE de.vin = '".$_REQUEST['bajaServiciosEspecialesVinHdn']."' ".
                                        "AND de.idDestinoEspecial = (SELECT max(d1.idDestinoEspecial)  FROM alDestinosEspecialesTbl d1 WHERE d1.VIN = de.vin);";

            $a = fn_ejecuta_query($sqlDltDestinoEspecialStr);

            echo json_encode($a);

            $sqlGetUD = "SELECT h1.claveMovimiento ".
                        "FROM alhistoricounidadestbl h1 ".
                        "WHERE h1.claveMovimiento NOT IN(SELECT valor FROM cageneralestbl WHERE columna = 'nValidos') ".
                        "AND h1.vin = '".$_REQUEST['bajaServiciosEspecialesVinHdn']."' ".
                        "AND h1.fechaEvento = (SELECT max(h2.fechaEvento) as fechaEvento FROM alhistoricounidadestbl h2 WHERE h1.vin = h2.vin  ) ;";

            $rsGetUD = fn_ejecuta_query($sqlGetUD);

            echo sizeof($rsGetUD);

            if(sizeof($rsGetUD) == '1'){
                $sqlUpdUD = "UPDATE alultimodetalletbl ".
                            "SET claveMovimiento = '".$rsGetUD['root'][0]['claveMovimiento']."', ".
                            "fechaEvento = now() ".
                            "WHERE vin = '".$_REQUEST['bajaServiciosEspecialesVinHdn']."';";

                fn_ejecuta_query($sqlUpdUD);
            }else{
                $sqlDltUD = "DELETE FROM alultimodetalletbl ".
                            "WHERE vin = '".$_REQUEST['bajaServiciosEspecialesVinHdn']."';";

                fn_ejecuta_query($sqlDltUD);
            }

        }

        function addCambioDestino(){
            $a = array();
            $e = array();
            $a['success'] = true;

            $vinArr = explode('|', substr($_REQUEST['cambioDestinoVinTxt'], 0, -1));
            if(in_array('', $vinArr)){
                $e[] = array('id'=>'cambioDestinoVinTxt','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            $colorArr = explode('|', substr($_REQUEST['cambioDestinoColorHdn'], 0, -1));
            if(in_array('', $colorArr)){
                $e[] = array('id'=>'cambioDestinoColorHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            $simboloArr = explode('|', substr($_REQUEST['cambioDestinoSimboloHdn'], 0, -1));
            if(in_array('', $simboloArr)){
                $e[] = array('id'=>'cambioDestinoSimboloHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            $distArr = explode('|', substr($_REQUEST['cambioDestinoDistribuidorHdn'], 0, -1));
            if(in_array('', $distArr)){
                $e[] = array('id'=>'cambioDestinoDistribuidorHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['cambioDestinoDistDestinoHdn'] == ""){
                $e[] = array('id'=>'cambioDestinoDistDestinoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            if($a['success']){
                //Se obtienen las direcciones y plazas del distribuidor Origen y Destino
                $sqlGetDireccionesPlazasDistStr = "SELECT idPlaza, direccionEntrega ".
                                                  "FROM caDistribuidoresCentrosTbl ".
                                                  "WHERE distribuidorCentro ='".$_REQUEST['cambioDestinoDistDestinoHdn']."' ".
                                                  "UNION ".
                                                  "SELECT idPlaza, direccionEntrega ".
                                                  "FROM caDistribuidoresCentrosTbl ".
                                                  "WHERE distribuidorCentro = '".$_SESSION['usuCompania']."'";

                $rs = fn_ejecuta_query($sqlGetDireccionesPlazasDistStr);

                if(sizeof($rs['root']) > 0){
                    $dirDestino = $rs['root'][0]['direccionEntrega'];
                    $plazaDestino = $rs['root'][0]['idPlaza'];
                    $dirOrigen = $rs['root'][1]['direccionEntrega'];
                    $plazaOrigen = $rs['root'][1]['idPlaza'];
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = "Error al obtener las direcciones y plazas del origen y destino";
                }
            }

            if($a['success']){
                $conceptosArr = explode('|', substr($_REQUEST['cambioDestinoConceptosHdn'], 0, -1));
                $importesArr = explode('|', substr($_REQUEST['cambioDestinoImportesHdn'], 0, -1));

                $sinFacturacion = array();

                for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) {
                    $sqlCheckFacturacion = "SELECT 1 FROM faFacturacionTransportacionTbl WHERE vin='".$vinArr[$nInt]."'";

                    $rs = fn_ejecuta_query($sqlCheckFacturacion);

                    if(sizeof($rs['root']) == 0){
                        array_push($sinFacturacion, $vinArr[$nInt]);
                    }

                    $sqlGetTarifaCambioDestinoStr = "SELECT tf.idTarifa ".
                                               "FROM caTarifasTbl tf, caClasificacionTarifasTbl ct, caSimbolosUnidadesTbl su ".
                                               "WHERE su.simboloUnidad = '".$simboloArr[$nInt]."' ".
                                               "AND ct.clasificacion = su.clasificacion ".
                                               "AND tf.idTarifa = ct.idTarifa ".
                                               "AND tf.tipoTarifa in ('N','E')";
                                               //"AND tf.tipoTarifa = 'C'";

                                               //C ES CAMBIO DE DESTINO
                    $rs = fn_ejecuta_query($sqlGetTarifaCambioDestinoStr);
                    //echo json_encode($sqlGetTarifaCambioDestinoStr);


                    if (sizeof($rs['root']) > 0) {
                        //$tarifa = $rs['root'][0]['idTarifa'];
                        $tarifa = "6";
                    } else {
                        $a['success'] = false;
                    }

                    if($a['success']){
                        $sqlAddCambioDestinoStr =  "INSERT INTO alDestinosEspecialesTbl ".
                                                "(idDestinoEspecial, vin, distribuidorOrigen, direccionOrigen, idPlazaOrigen, distribuidorDestino, ".
                                                    "direccionDestino, idPlazaDestino, ".
                                                    "importeCob, sueldo, idTarifa) VALUES(".
                                                "(SELECT IFNULL(MAX(de2.idDestinoEspecial),0)+1 FROM alDestinosEspecialesTbl de2 ".
                                                            "WHERE de2.vin = '".$vinArr[$nInt]."'),".
                                                "'".$vinArr[$nInt]."', ".
                                                "'".$_SESSION['usuCompania']."',".
                                                $dirOrigen.",".
                                                $plazaOrigen.",".
                                                "'".$_REQUEST['cambioDestinoDistDestinoHdn']."',".
                                                $dirDestino.",".
                                                $plazaDestino.
                                                ",0.00,0.00, ".
                                                $tarifa.")";

                        $rs = fn_ejecuta_Add($sqlAddCambioDestinoStr);

                        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                            $sqlGetLozalicacionStr = "SELECT idDestinoEspecial as localizacionUnidad FROM alDestinosEspecialesTbl h ".
                                                        "WHERE idDestinoEspecial = (SELECT MAX(h2.idDestinoEspecial) FROM alDestinosEspecialesTbl h2 ".
                                                                "WHERE h2.vin = h.vin) ".
                                                        "AND h.vin = '".$vinArr[$nInt]."'";

                            $rsLoc = fn_ejecuta_query($sqlGetLozalicacionStr);

                        //INSERTO EN HISTÓRICO Y ÚLTIMO DETALLE
                      $data = addHistoricoUnidad($_SESSION['usuCompania'], $vinArr[$nInt], 'CT', $distArr[$nInt],
                                $tarifa, $rsLoc['root'][0]['localizacionUnidad'],'', '', $_SESSION['idUsuario']);

                            if($data['success'] == true){
                                if($_REQUEST['cambioDestinoConceptosHdn'] != "" && $_REQUEST['cambioDestinoImportesHdnoImportesHdn'] != ""){
                                    $data = addGastosEspeciales($_REQUEST['cambioDestinoDistribuidorHdn'], $vinArr[$nInt], $conceptosArr, $importesArr);
                                }
                                $a['successMessage'] = getCambioDestinoSuccessMsg();

                                //ACTUALIZA LA INFORMACION PARA LA FACTURACION
                                if(!in_array($vinArr[$nInt], $sinFacturacion)){
                                    $importeVariable = 0.00;
                                    $kilometros = 0.00;

                                    $sqlGetTarifaVariable = "SELECT (SELECT tt.importe FROM caTarifasTransportacionTbl tt ".
                                                                "WHERE tt.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                                                "AND tt.tipoCarga = (".
                                                                    "SELECT cm.tipoCarga FROM caClasificacionMarcaTbl cm ".
                                                                    "WHERE cm.clasificacion = (".
                                                                        "SELECT su.clasificacion FROM caSimbolosUnidadesTbl su ".
                                                                        "WHERE su.simboloUnidad = '".$simboloArr[$nInt]."')".
                                                                    "AND cm.marca = (".
                                                                        "SELECT su.marca FROM caSimbolosUnidadesTbl su ".
                                                                        "WHERE su.simboloUnidad = '".$simboloArr[$nInt]."'))".
                                                                "AND tt.region = (".
                                                                    "SELECT dc.zonaGeografica FROM caDistribuidoresCentrosTbl dc ".
                                                                    "WHERE distribuidorCentro = '".$distArr[$nInt]."') ".
                                                                "AND tt.tipoTarifa = 'VA') AS tarifa, ".
                                                            "(SELECT kp.kilometros FROM caKilometrosPlazaTbl kp ".
                                                                "WHERE kp.idPlazaOrigen = ".$plazaOrigen." ".
                                                                "AND kp.idPlazaDestino = ".$plazaDestino.") AS kilometros";

                                    $rs = fn_ejecuta_query($sqlGetTarifaVariable);

                                    if(sizeof($rs['root']) > 0){
                                        if($rs['root'][0]['tarifa'] != "" && $rs['root'][0]['kilometros'] != ""){
                                            $kilometros = floatval($rs['root'][0]['kilometros']);
                                            $importeVariable =  floatval($rs['root'][0]['tarifa']) * $kilometros;
                                        }
                                    }

                                    $sqlUpdFacturacion =    "UPDATE faFacturacionTransportacionTbl SET ".
                                                            "plazaCambDes = (SELECT plaza FROM caPlazasTbl ".
                                                                "WHERE idPlaza = ".$plazaDestino."), ".
                                                            "regionCd = (SELECT zonaGeografica FROM caDistribuidoresCentrosTbl ".
                                                                "WHERE distribuidorCentro = '".$distArr[$nInt]."'), ".
                                                            "kilometrosCd = ".$kilometros.", ".
                                                            "importeVariableCd = ".$importeVariable." ".
                                                            "WHERE vin = '".$vinArr[$nInt]."'";

                                    fn_ejecuta_Upd($sqlUpdFacturacion);

                                    $updAlunidades ="UPDATE alunidadestbl SET tipoCambioDestino='".$_REQUEST['cambioDestinoTipoDestinoHdn']."' WHERE VIN='".$vinArr[$nInt]."'";
                                    fn_ejecuta_Upd($updAlunidades);
                            
                                }
                            } else{
                                $a['errors'] = $data['errors'];
                                $a['errorMessage'] = $data['errorMessage'];
                            }
                        } else {
                            $a['success'] = false;
                            $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddCambioDestinoStr;
                        }
                    } else {
                        $a['errorMessage'] .= getNoTarifaUnidadMsg($vinArr[$nInt], 'Cambio de Destino')."<br>";
                    }
                }
            }

            if(sizeof($sinFacturacion) > 0){
                $a['errorMessage'] .= "<br>Unidades sin Facturacion Actualizada: ";
                foreach ($sinFacturacion as $unidad) {
                    $a['errorMessage'] .= $unidad.",";
                }

                $a['errorMessage'] .= substr($a['errorMessage'], 0, -1);
            }

            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
            echo json_encode($a);
        }

        function dltCambioDestino(){
            $a = array();
            $e = array();
            $a['success'] = true;

            if($_REQUEST['alDestinosEspecialesVinHdn'] == ""){
                $e[] = array('id'=>'alDestinosEspecialesVinHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            if($a['success']){
                $sqlDltCambioDestinoStr = "DELETE FROM alDestinosEspecialesTbl ".
                                            "WHERE vin = '".$_REQUEST['alDestinosEspecialesVinHdn']."'";

                fn_ejecuta_query($sqlDltCambioDestinoStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['successMessage'] = getCambioDestinoDltMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltCambioDestinoStr;
                }
            }
            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
            echo json_encode($a);
        }

        function addCambioDestinoProgramado(){
            $a = array();
            $e = array();
            $a['success'] = true;

            if($_REQUEST['alDestinosEspecialesVinHdn'] == ""){
                $e[] = array('id'=>'alDestinosEspecialesVinHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['alDestinosEspecialesDistDestinoHdn'] == ""){
                $e[] = array('id'=>'alDestinosEspecialesDistDestinoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['alDestinosEspecialesDirDestinoHdn'] == ""){
                $e[] = array('id'=>'alDestinosEspecialesDirDestinoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if($_REQUEST['alDestinosEspecialesPlazaDestinoHdn'] == ""){
                $e[] = array('id'=>'alDestinosEspecialesPlazaDestinoHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            if($a['success']){
                $sqlAddCambioDestinoStr =  "INSERT INTO alDestinosEspecialesTbl ".
                                            "(vin, distribuidorOrigen, direccionOrigen, idPlazaOrigen, distribuidorDestino,".
                                            "direccionDestino, idPlazaDestino, importeCob, sueldo, idTarifa) VALUES('".
                                            $_REQUEST['alDestinosEspecialesVinHdn']."',".
                                            "NULL,".
                                            "NULL,".
                                            "NULL,".
                                            "'".$_REQUEST['alDestinosEspecialesDistDestinoHdn']."',".
                                            $_REQUEST['alDestinosEspecialesDirDestinoHdn'].",".
                                            $_REQUEST['alDestinosEspecialesPlazaDestinoHdn'].
                                            ",0.00,0.00, ".
                                            "NULL)";

                $rs = fn_ejecuta_Add($sqlAddCambioDestinoStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['successMessage'] = getCambioDestinoSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddCambioDestinoStr;
                }
            }
            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
            echo json_encode($a);
        }

        function dltCambioDestinoProgramado(){
            $a = array();
            $e = array();
            $a['success'] = true;

            if($_REQUEST['alDestinosEspecialesVinHdn'] == ""){
                $e[] = array('id'=>'alDestinosEspecialesVinHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            if($a['success']){
                $data = getHistoriaUnidad($_REQUEST['alDestinosEspecialesVinHdn']);

                if(sizeof($data['root']) > 0){
                    foreach ($data['root'] as $historico) {
                        if($historico['claveMovimiento'] == 'EP'){
                            $a['success'] = false;
                            $a['errorMessage'] = getCambioDestinoEntradaPatioDltMsg();
                            break;
                        }
                    }
                }
            }

            if($a['success']){
                $sqlDltCambioDestinoStr = "DELETE FROM alDestinosEspecialesTbl ".
                                            "WHERE vin = '".$_REQUEST['alDestinosEspecialesVinHdn']."'";

                fn_ejecuta_query($sqlDltCambioDestinoStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['successMessage'] = getCambioDestinoDltMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlDltCambioDestinoStr;
                }
            }
            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
        }

        function getDestinosEspecialesUnico(){
            $lsWhereStr = "WHERE de.vin = h.vin ".
                          "AND h.fechaEvento=(".
                          "SELECT MAX(h1.fechaEvento) ".
                          "FROM alHistoricoUnidadesTbl h1 ".
                          "WHERE h1.vin = de.vin) ".
                          "AND h.vin = u.vin ".
                          "AND su.simboloUnidad = u.simboloUnidad ".
                          "AND de.idDestinoEspecial = (SELECT max(de1.idDestinoEspecial) as ultimoDestino FROM alDestinosEspecialesTbl de1 WHERE de1.vin = de.vin )".
                          "AND tf.idTarifa = de.idTarifa ";

            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesIdDestinoHdn'], "de.idDestinoEspecial", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesVinTxt'], "de.vin", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDistOrigenHdn'], "de.distribuidorOrigen", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDirOrigenHdn'], "de.direccionOrigen", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesPlazaOrigenHdn'], "de.idPlazaOrigen", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDistDestinoHdn'], "de.distribuidorDestino", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesDirDestinoHdn'], "de.direccionDestino", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesPlazaDestinoHdn'], "de.idPlazaDestino", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesImporteCobTxt'], "de.importeCob", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesSueldoTxt'], "de.sueldo", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesTarifaHdn'], "de.idTarifa", 0);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesCentroDistHdn'], "h.centroDistribucion", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }
            if ($gb_error_filtro == 0){
                $lsCondicionStr = fn_construct($_REQUEST['alDestinosEspecialesAvanzadaHdn'], "u.avanzada", 1);
                $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
            }

            $sqlGetDestinosEspStr = "SELECT de.*, h.centroDistribucion, u.avanzada, tf.tarifa, tf.descripcion AS nombreTarifa, ".
                                    "h.claveMovimiento, u.simboloUnidad, u.color, su.descripcion AS nombreSimbolo, ".
                                    "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca = su.marca) AS nombreMarca, ".
                                    "(SELECT co.descripcion FROM caColorUnidadesTbl co ".
                                        "WHERE co.color = u.color AND co.marca = su.marca) AS nombreColor, ".
                                    "(SELECT dc.descripcionCentro FROM caDistribuidoresCentrosTbl dc ".
                                        "WHERE dc.distribuidorCentro = de.distribuidorOrigen) AS nombreDistOrigen, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = de.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                    "(SELECT dc1.descripcionCentro FROM caDistribuidoresCentrosTbl dc1 ".
                                        "WHERE dc1.distribuidorCentro = de.distribuidorDestino) AS nombreDistDestino, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = de.idPlazaDestino) AS nombrePlazaDestino, ".
                                    "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                        "WHERE dc2.distribuidorCentro = h.centroDistribucion) AS nombreCentroDist ".
                                    "FROM alDestinosEspecialesTbl de, alHistoricoUnidadesTbl h, alUnidadesTbl u, ".
                                        "caTarifasTbl tf, caSimbolosUnidadesTbl su ".$lsWhereStr;

            $rs = fn_ejecuta_query($sqlGetDestinosEspStr);

            for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
                $rs['root'][$nInt]['descDistOrigen'] = $rs['root'][$nInt]['distribuidorOrigen']." - ".$rs['root'][$nInt]['nombreDistOrigen'];
                $rs['root'][$nInt]['descPlazaOrigen'] = $rs['root'][$nInt]['idPlazaOrigen']." - ".$rs['root'][$nInt]['nombrePlazaOrigen'];
                $rs['root'][$nInt]['descDistDestino'] = $rs['root'][$nInt]['distribuidorDestino']." - ".$rs['root'][$nInt]['nombreDistDestino'];
                $rs['root'][$nInt]['descPlazaDestino'] = $rs['root'][$nInt]['idPlazaDestino']." - ".$rs['root'][$nInt]['nombrePlazaDestino'];
                $rs['root'][$nInt]['descTarifa'] = $rs['root'][$nInt]['tarifa']." - ".$rs['root'][$nInt]['nombreTarifa'];
                $rs['root'][$nInt]['descCentroDist'] = $rs['root'][$nInt]['centroDistribucion']." - ".$rs['root'][$nInt]['nombreCentroDist'];
            }

            return $rs;
        }

        function addUndTmp(){

            $addUnidad = "INSERT INTO alUnidadesTmp (vin, avanzada, modulo, idUsuario, ip, fecha) ".
                            "VALUES( '".$_REQUEST['canServVinDsp']."', ".
                            "'".$_REQUEST['canServAvanzadaTxt']."', ".
                            "'Cancelar Servicio Especial', ".
                            "'".$_SESSION['idUsuario']."', ".
                            "'".$_SERVER['REMOTE_ADDR']."', ".
                            "NOW()) ";

            $rsAddBloquea = fn_ejecuta_query($addUnidad);

            echo json_encode($rsAddBloquea);
        }

        function sqlGetCancelacion(){
            $sqlGetUnd = "SELECT hu.vin, concat(su.simboloUnidad,' - ',su.descripcion) as simboloUnidad, au.distribuidor as distribuidorUnidad,de.distribuidorOrigen, de.distribuidordestino ".
                            "FROM alDestinosEspecialesTbl  de, alultimodetalletbl hu, casimbolosunidadestbl su, alunidadestbl au ".
                            "WHERE de.vin = hu.vin ".
                            "AND de.vin = au.vin ".
                            "AND au.simboloUnidad = su.simboloUnidad ".
                            "AND de.vin LIKE '%".$_REQUEST['canServAvanzadaTxt']."' ".
                            "AND hu.claveMovimiento in ('RC','CA') ".
                            ///"AND hu.fechaEvento = (SELECT MAX(h2.fechaEvento) FROM alhistoricounidadestbl h2 WHERE h2.vin = hu.vin) ".
                            "AND hu.centroDistribucion = '".$_REQUEST['canServCentroCmb']."' ".
                            "AND hu.localizacionUnidad = (SELECT max(d1.idDestinoEspecial) FROM alDestinosEspecialesTbl d1 WHERE d1.VIN = de.vin);";

            $rsGetUnd = fn_ejecuta_query($sqlGetUnd);

            echo json_encode($rsGetUnd);
        }

        function getUndTmp(){
            $sqlGetUndTmp = "SELECT ut.avanzada,ut.vin, concat(su.simboloUnidad,' - ',su.descripcion) as simboloUnidad, au.distribuidor as distribuidorUnidad,de.distribuidorOrigen, de.distribuidordestino ".
                            "FROM alDestinosEspecialesTbl  de, alultimodetalletbl hu, casimbolosunidadestbl su, alunidadestbl au, alUnidadesTmp ut ".
                            "WHERE de.vin = hu.vin ".
                            "AND de.vin = au.vin ".
                            "AND au.vin = ut.vin ".
                            "AND ut.modulo = 'Cancelar Servicio Especial' ".
                            "AND au.simboloUnidad = su.simboloUnidad ".
                            // "AND de.vin LIKE '%".$_REQUEST['canServAvanzadaTxt']."' ".
                            "AND hu.claveMovimiento in ('RC','CA') ".
                            // "AND hu.centroDistribucion = '".$_REQUEST['canServCentroCmb']."' ".
                            "AND de.idDestinoEspecial = (SELECT max(d1.idDestinoEspecial)  FROM alDestinosEspecialesTbl d1 WHERE d1.VIN = de.vin);";

            $rsGetUnd = fn_ejecuta_query($sqlGetUndTmp);

            echo json_encode($rsGetUnd);
        }

        function DltCancTmp(){
            $sqlCancTmp = "DELETE ".
                            "FROM alunidadestmp ".
                            "WHERE idUsuario = '".$_SESSION['idUsuario']."' ".
                            "AND ip = '".$_SERVER['REMOTE_ADDR']."' ".
                            "AND modulo = 'Cancelar Servicio Especial'";

            $rsGetCancTmp = fn_ejecuta_query($sqlCancTmp);
        }

        function DltUndTmp(){
            $sqlCancTmp = "DELETE ".
                    "FROM alunidadestmp ".
                    "WHERE idUsuario = '".$_SESSION['idUsuario']."' ".
                    "AND vin LIKE '%".$_REQUEST['canServAvanzadaTxt']."' ".
                    "AND ip = '".$_SERVER['REMOTE_ADDR']."' ".
                    "AND modulo = 'Cancelar Servicio Especial'";

            $rsGetCancTmp = fn_ejecuta_query($sqlCancTmp);
        }

        function cancservEsp(){
            $getUnd = "SELECT vin ".
                        "FROM alunidadestmp ".
                        "WHERE idUsuario = '".$_SESSION['idUsuario']."' ".
                        "AND ip = '".$_SERVER['REMOTE_ADDR']."' ".
                        "AND modulo = 'Cancelar Servicio Especial';";

            $rsGetUnd = fn_ejecuta_query($getUnd);

            for ($i=0; $i < sizeof($rsGetUnd['root']) ; $i++) {
                $sqlGetBandera = "SELECT 1 AS bandera ".
                                    "FROM alhistoricounidadestbl hu, alunidadestmp tmp ".
                                    "WHERE hu.vin = tmp.vin ".
                                    "AND tmp.vin like '".$rsGetUnd['root'][$i]['vin']."' ".
                                    "AND hu.claveMovimiento NOT IN(SELECT valor FROM cageneralestbl WHERE tabla = 'alHistoricoUnidadesTbl' AND columna = 'nValidos') ".
                                    "AND hu.claveMovimiento != 'RC' ".
                                    "LIMIT 1;";
                                    // "AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) FROM alhistoricounidadestbl hu2 WHERE hu2.vin = hu.vin);";

                $rsCancUnd = fn_ejecuta_query($sqlGetBandera);

                if($rsCancUnd['root'][0]['bandera'] == '1'){
                    $sqlGetUnd = "SELECT hu.idHistorico,hu.vin, hu.localizacionUnidad ".
                                "FROM alhistoricounidadestbl hu, aldestinosespecialestbl de, alunidadestmp tmp ".
                                "WHERE hu.vin = de.vin ".
                                "AND de.vin = tmp.vin ".
                                "AND hu.claveMovimiento ='RC' ".
                                "AND de.idDestinoEspecial = hu.localizacionUnidad ".
                                "AND hu.fechaEvento = (SELECT MAX(hu1.fechaEvento) FROM alhistoricounidadestbl hu1 WHERE hu1.vin = hu.vin AND hu1.claveMovimiento ='RC' ) ".
                                "AND de.vin = '".$rsGetUnd['root'][$i]['vin']."'; ";

                                $rsSqlGetUnd = fn_ejecuta_query($sqlGetUnd);

                    $sqlDltUnd = "UPDATE alhistoricounidadestbl ".
                                "SET claveMovimiento = 'MC' ".
                                "WHERE claveMovimiento in ('RC','CA') ".
                                "AND vin = '".$rsGetUnd['root'][$i]['vin']."' ".
                                "AND idHistorico = '".$rsSqlGetUnd['root'][0]['idHistorico']."'; ";

                                fn_ejecuta_query($sqlDltUnd);

                    $sqlDltDe = "DELETE ".
                              "FROM aldestinosespecialestbl ".
                              "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ".
                              "AND idDestinoEspecial =  '".$rsSqlGetUnd['root'][0]['localizacionUnidad']."';";

                              fn_ejecuta_query($sqlDltDe);

                    $sqlGetMovimiento = "SELECT hu.claveMovimiento, hu.fechaEvento ".
                                        "FROM alhistoricounidadestbl hu ".
                                        "WHERE hu.claveMovimiento NOT IN (SELECT valor FROM cageneralestbl WHERE tabla = 'alHistoricoUnidadesTbl' AND columna = 'nValidos')  ".
                                        "AND hu.vin = '".$rsSqlGetUnd['root'][0]['vin']."' ".
                                        "ORDER BY hu.fechaEvento DESC ".
                                        "LIMIT 1;";

                                        $rsSqlGetMovimiento = fn_ejecuta_query($sqlGetMovimiento);

                    $sqlUpdUnd = "UPDATE alultimodetalletbl ".
                                "SET claveMovimiento = '".$rsSqlGetMovimiento['root'][0]['claveMovimiento']."', ".
                                "fechaEvento = '".$rsSqlGetMovimiento['root'][0]['fechaEvento']."' ".
                                "WHERE vin = '".$rsSqlGetUnd['root'][0]['vin']."';";

                                fn_ejecuta_query($sqlUpdUnd);
                    
                    $dlTmp = "DELETE FROM alunidadestmp WHERE vin = '".$rsSqlGetUnd['root'][0]['vin']."';";

                            fn_ejecuta_query($dlTmp);

                }else{
                    $sqlDltUd = "DELETE FROM alultimodetalletbl ".
                                    "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."'";

                    fn_ejecuta_query($sqlDltUd);

                    $sqlDltHu = "DELETE FROM alhistoricounidadestbl ".
                                    "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ";

                    fn_ejecuta_query($sqlDltHu);

                    $sqlDltEsp = "DELETE FROM alDestinosEspecialesTbl ".
                                    "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ";

                    fn_ejecuta_query($sqlDltEsp);

                    $sqlDltTmp = "DELETE FROM alunidadestmp ".
                                    "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ";

                    fn_ejecuta_query($sqlDltTmp);

                    $sqlDltAu = "DELETE FROM alunidadestbl ".
                                    "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ";

                    fn_ejecuta_query($sqlDltAu);
                }
            }
        }

        function addUndTmpCCD(){

            $addUnidad = "INSERT INTO alUnidadesTmp (vin, avanzada, modulo, idUsuario, ip, fecha) ".
                            "VALUES( '".$_REQUEST['canServVinDsp']."', ".
                            "'".$_REQUEST['canServAvanzadaTxt']."', ".
                            "'Cambio Centro Distribucion', ".
                            "'".$_SESSION['idUsuario']."', ".
                            "'".$_SERVER['REMOTE_ADDR']."', ".
                            "NOW()) ";

            $rsAddBloquea = fn_ejecuta_query($addUnidad);

            echo json_encode($rsAddBloquea);
        }

        function getUndTmpCCD(){
            $sqlGetUndTmp = "SELECT ut.avanzada,ut.vin, concat(su.simboloUnidad,' - ',su.descripcion) as simboloUnidad, au.distribuidor as distribuidorUnidad,de.distribuidorOrigen, de.distribuidordestino ".
                            "FROM alDestinosEspecialesTbl  de, alultimodetalletbl hu, casimbolosunidadestbl su, alunidadestbl au, alUnidadesTmp ut ".
                            "WHERE de.vin = hu.vin ".
                            "AND de.vin = au.vin ".
                            "AND au.vin = ut.vin ".
                            "AND ut.modulo = 'Cambio Centro Distribucion' ".
                            "AND au.simboloUnidad = su.simboloUnidad ".
                            // "AND de.vin LIKE '%".$_REQUEST['canServAvanzadaTxt']."' ".
                            "AND hu.claveMovimiento in ('RC','CA') ".
                            // "AND hu.centroDistribucion = '".$_REQUEST['canServCentroCmb']."' ".
                            "AND de.idDestinoEspecial = (SELECT max(d1.idDestinoEspecial)  FROM alDestinosEspecialesTbl d1 WHERE d1.VIN = de.vin);";

            $rsGetUnd = fn_ejecuta_query($sqlGetUndTmp);

            echo json_encode($rsGetUnd);
        }

        function updCambioCentro(){
            $getUnd = "SELECT vin ".
                        "FROM alunidadestmp ".
                        "WHERE idUsuario = '".$_SESSION['idUsuario']."' ".
                        "AND ip = '".$_SERVER['REMOTE_ADDR']."' ".
                        "AND modulo = 'Cambio Centro Distribucion';";

            $rsGetUnd = fn_ejecuta_query($getUnd);

            for ($i=0; $i < sizeof($rsGetUnd['root']) ; $i++) {
                $sqlGetBandera = "SELECT 1 AS bandera ".
                                    "FROM alhistoricounidadestbl hu, alunidadestmp tmp ".
                                    "WHERE hu.vin = tmp.vin ".
                                    "AND tmp.vin like '".$rsGetUnd['root'][$i]['vin']."' ".
                                    "AND hu.claveMovimiento NOT IN(SELECT valor FROM cageneralestbl WHERE tabla = 'alHistoricoUnidadesTbl' AND columna = 'nValidos') ".
                                    "AND hu.claveMovimiento != 'RC' ".
                                    "AND hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) FROM alhistoricounidadestbl hu2 WHERE hu2.vin = hu.vin);";

                $rsCancUnd = fn_ejecuta_query($sqlGetBandera);

                if($rsCancUnd['root'][0]['bandera'] == '1'){

                }else{
                    echo $_REQUEST['cambioCentroCmd'];
                    echo $_REQUEST['canServCentroCmb'];

                    $updUD = "UPDATE alultimodetalletbl ".
                                "SET centroDistribucion = '".$_REQUEST['cambioCentroCmd']."' ".
                                "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ".
                                "AND claveMovimiento in ('RC','CA');";

                    fn_ejecuta_query($updUD);

                    $sqlGetHistorico = "SELECT hu.idHistorico ".
                                        "FROM alhistoricounidadestbl hu ".
                                        "WHERE hu.claveMovimiento = 'RC' ".
                                        "AND hu.fechaEvento = (SELECT MAX(hu1.fechaEvento) FROM alhistoricounidadestbl hu1 WHERE hu1.vin = hu.vin) ".
                                        "AND hu.vin = '".$rsGetUnd['root'][$i]['vin']."';";

                    $rsCambio = fn_ejecuta_query($sqlGetHistorico);

                    $updUD = "UPDATE alHistoricoUnidadesTbl ".
                                "SET centroDistribucion = '".$_REQUEST['cambioCentroCmd']."' ".
                                "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ".
                                "AND claveMovimiento ='RC' ".
                                "AND idHistorico = '".$rsCambio['root'][0]['idHistorico']."';";

                    fn_ejecuta_query($updUD);  


                    $sqlgetFecha = "SELECT MAX(fechaEvento) AS fechaEvento FROM alhistoricounidadestbl ".
                                    "WHERE claveMovimiento = 'RC' ".
                                    "AND vin = '".$rsGetUnd['root'][$i]['vin']."';";

                    fn_ejecuta_query($sqlgetFecha);

                    /*$sqlGetCentro = "SELECT 1, ".
                                    "(SELECT direccionEntrega FROM cadistribuidorescentrostbl WHERE distribuidorCentro = '".$_REQUEST['cambioCentroCmd']."') AS direccionOrigen, ".
                                    "(SELECT idPlaza FROM cadistribuidorescentrostbl WHERE distribuidorCentro = '".$_REQUEST['cambioCentroCmd']."') AS plazaOrigen, ".
                                    "(SELECT max(idDestinoEspecial) FROM aldestinosespecialestbl d1 WHERE d1.vin = de.vin) AS ultimoservicio ".
                                    "FROM aldestinosespecialestbl de, cadistribuidorescentrostbl dc ".
                                    "WHERE de.distribuidorOrigen = dc.distribuidorCentro ".
                                    "AND dc.tipoDistribuidor = 'CD' ".
                                    "AND de.vin = '".$rsGetUnd['root'][$i]['vin']."';";

                    $rsGetCentro = fn_ejecuta_query($sqlGetCentro);

                    if(sizeof($rsGetCentro['root']) == '1'){
                        $updCD = "UPDATE alDestinosEspecialesTbl ".
                                    "SET distribuidorOrigen = '".$_REQUEST['cambioCentroCmd']."', ".
                                    "idPlazaOrigen = '".$rsGetCentro['root'][0]['plazaOrigen']."',".
                                    "direccionOrigen = '".$rsGetCentro['root'][0]['direccionOrigen']."' ".
                                    "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ".
                                    "AND idDestinoEspecial = '".$rsGetCentro['root'][0]['ultimoservicio']."' ";

                        fn_ejecuta_query($updCD);
                    }*/

                    /*$udpHU = "UPDATE alhistoricounidadestbl ".
                                "SET centroDistribucion = '".$_REQUEST['cambioCentroCmd']."' ".
                                "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ".
                                "AND centroDistribucion = '".$_REQUEST['canServCentroCmb']."' ".
                                "AND claveMovimiento ='RC' ".
                                "AND fechaEvento = '".$rsGetFecha['root'][$i]['fechaEvento']."';";

                    fn_ejecuta_query($udpHU);*/

                    $sqlDlt = "DELETE FROM alunidadestmp ".
                              "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."'";

                    fn_ejecuta_query($sqlDlt);
                }
            }
        }

        function DltUndTmpCCD(){
            $sqlCancTmp = "DELETE ".
                    "FROM alunidadestmp ".
                    "WHERE idUsuario = '".$_SESSION['idUsuario']."' ".
                    "AND vin LIKE '%".$_REQUEST['canServAvanzadaTxt']."' ".
                    "AND ip = '".$_SERVER['REMOTE_ADDR']."' ".
                    "AND modulo = 'Cambio Centro Distribucion'";

            $rsGetCancTmp = fn_ejecuta_query($sqlCancTmp);
        }

        function addCambioDeSE(){
          $sqlGetCambio = "SELECT distribuidorCentro, direccionEntrega, idPlaza ".
                          "FROM cadistribuidorescentrostbl ".
                          "WHERE distribuidorCentro = '".$_REQUEST['modDestinoDestinoCmb']."';";

          $rsGetCambio = fn_ejecuta_query($sqlGetCambio);

          $getUnd = "SELECT vin ".
                      "FROM alunidadestmp ".
                      "WHERE idUsuario = '".$_SESSION['idUsuario']."' ".
                      "AND ip = '".$_SERVER['REMOTE_ADDR']."' ".
                      "AND modulo = 'Cancelar Servicio Especial';";

          $rsGetUnd = fn_ejecuta_query($getUnd);

          for ($i=0; $i < sizeof($rsGetUnd['root']) ; $i++) {
            $sqlGetDestino = "SELECT max(idDestinoespecial) as numDestino ".
                              "FROM aldestinosespecialestbl WHERE vin = '".$rsGetUnd['root'][$i]['vin']."';";

            $rsGetDestino = fn_ejecuta_query($sqlGetDestino);

            $sqlGetMax = "SELECT max(idDestinoespecial) as maxDestino FROM aldestinosespecialestbl WHERE vin = '".$rsGetUnd['root'][$i]['vin']."'";

            $rsSqlGetMax = fn_ejecuta_query($sqlGetMax);

            $sqlUpdDestino = "UPDATE aldestinosespecialestbl ".
                              "SET distribuidorDestino = '".$rsGetCambio['root'][0]['distribuidorCentro']."', ".
                              "direccionDestino = '".$rsGetCambio['root'][0]['direccionEntrega']."', ".
                              "idPlazaDestino = '".$rsGetCambio['root'][0]['idPlaza']."' ".
                              "WHERE vin = '".$rsGetUnd['root'][$i]['vin']."' ".
                              "AND idDestinoEspecial = '".$rsSqlGetMax['root'][0]['maxDestino']."';";

            fn_ejecuta_query($sqlUpdDestino);

          }
          $sqlDlt = "DELETE FROM alunidadestmp ".
                    "WHERE modulo = 'Cancelar Servicio Especial'";

          fn_ejecuta_query($sqlDlt);
        }


    function validarST(){
        $sql="SELECT * FROM alcabeceroststbl ".
                "WHERE ServiceType='TR' ".
                "AND idSt=".$_REQUEST['trap010STTxt'];
        $rs=fn_ejecuta_query($sql);

        echo json_encode($rs);

    }
