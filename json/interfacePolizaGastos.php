<?php
session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
  require_once("../funciones/utilidades.php");

	switch($_REQUEST['intefacePolizaHdn'])
	{
		case 'sqlGetConsulta':
			sqlGetConsulta();
			break;
		case 'validaExistencia':
			validaExistencia();
			break;
		case 'validaCapturada':
			validaCapturada();
			break;
		case 'addPolizaCj':
			addPolizaCj();
			break;
		case 'updPolizaGS':
			updPolizaGS();
			break;
		default:
			echo '';
		break;
	}


	function validaExistencia(){

	$sqlValidaFolio = "SELECT idViajeTractor from trgastosviajetractortbl ". 
					  "WHERE centroDistribucion='".$_SESSION['usuCompania']."' ".
					  "AND folio='".$_REQUEST['trap485PolizaTxt']."' ".
					  "AND CAST(fechaEvento AS date)='".$_REQUEST['fechaPoilizaHdn']."' ".
					  "AND claveMovimiento='GP' ".
					  "limit 1" ;
	$rsSqlValidaFolio = fn_ejecuta_query($sqlValidaFolio);
	echo json_encode($rsSqlValidaFolio);	
	}

	function validaCapturada(){

	$sqlValidaCapturada = "SELECT idViajeTractor, claveMovimiento, SUBSTRING(fechaMovimiento,1,10) AS fechaMovimiento FROM trpolizainterfacetbl ".
					 	  "WHERE folio='".$_REQUEST['trap485PolizaTxt']."' ".
					 	  "AND idViajeTractor='".$_REQUEST['idViajeTractorHdn']."' ";
	$rsSqlValidaCapturada = fn_ejecuta_query($sqlValidaCapturada);
	echo json_encode($rsSqlValidaCapturada);	

	}
	function sqlGetConsulta(){


		$sqlGetFolio = "SELECT gt.fechaEvento as fechaPoliza, CONCAT(tr.compania ,' - ',(SELECT co.descripcion FROM cacompaniastbl co WHERE tr.compania = co.compania )) as compania, tr.tractor, vt.viaje, vt.idViajeTractor, ".
										"CONCAT(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as operador, vt.numeroRepartos as distribuidores, tr.rendimiento, vt.fechaEvento as fechaViaje, ".
										"(SELECT pl.plaza FROM caplazastbl pl WHERE pl.idPlaza = vt.idPlazaOrigen ) as plazaOrigen, ".
										"(SELECT pl.plaza FROM caplazastbl pl WHERE pl.idPlaza = vt.idPlazaDestino ) as plazaDestino, vt.kilometrosComprobados, vt.numeroUnidades, ".
										"ifNull((SELECT importe FROM trgastosviajetractortbl g2 WHERE g2.idViajeTractor = gt.idViajeTractor AND g2.folio=gt.folio AND g2.centroDistribucion=gt.centroDistribucion AND g2.claveMovimiento = 'GP' AND g2.concepto IN(7014)),0.00 ) as pagoEfectivo, ".
										"ifNull((SELECT importe FROM trgastosviajetractortbl g3 WHERE g3.idViajeTractor = gt.idViajeTractor AND g3.folio=gt.folio AND g3.centroDistribucion=gt.centroDistribucion AND g3.claveMovimiento = 'GS' AND g3.concepto IN(141)),0.00 ) as pagoNeto ".
										"FROM trgastosviajetractortbl gt, catractorestbl tr, caChoferesTbl ch, trviajestractorestbl vt ".
										"WHERE  gt.folio = '".$_REQUEST['trap485PolizaTxt']."' ".
										"AND cast(gt.fechaEvento as date) = '".$_REQUEST['fechaPolizaHdn']."' ".
										"AND gt.centroDistribucion = '".$_SESSION['usuCompania']."' ".
										"AND vt.idTractor = tr.idTractor ".
										"AND vt.claveChofer = ch.claveChofer ".
										"AND gt.claveMovimiento in ('GP','GS') ".
										"AND gt.idViajeTractor = vt.idViajeTractor ".	
										"LIMIT 1;" ;

    $rsGetFolio = fn_ejecuta_query($sqlGetFolio);
	echo json_encode($rsGetFolio);

	}

	function addPolizaCj(){

		$sqlAddPagoNeto = "INSERT INTO trpolizainterfacetbl (centroDistribucion, folio, idViajeTractor, fechaMovimiento, fechaPoliza, concepto, tipoDocumento, importe, observaciones, claveMovimiento) ".
						"VALUES ('".$_SESSION['usuCompania']."','".$_REQUEST['trap485PolizaTxt']."','".$_REQUEST['idViajeTractorHdn']."','".$_REQUEST['trfechaPagoPolizaHdn']."','".$_REQUEST['trfechaPolizaHdn']."','141','S','".$_REQUEST['trimportePagoNeto']."','','C')";

			$rsSqlAddPagoNeto = fn_ejecuta_query($sqlAddPagoNeto);


			$sqlAddPagoEfectivo = "INSERT INTO trpolizainterfacetbl (centroDistribucion, folio, idViajeTractor, fechaMovimiento, fechaPoliza, concepto, tipoDocumento, importe, observaciones, claveMovimiento) ".
						"VALUES ('".$_SESSION['usuCompania']."','".$_REQUEST['trap485PolizaTxt']."','".$_REQUEST['idViajeTractorHdn']."','".$_REQUEST['trfechaPagoPolizaHdn']."','".$_REQUEST['trfechaPolizaHdn']."','7014','G','".$_REQUEST['trimportePagoEfectivo']."','','C')";

			$rsSqlAddPagoEfectivo = fn_ejecuta_query($sqlAddPagoEfectivo);

		echo json_encode($rsSqlAddPagoEfectivo);	

	}

	function updPolizaGS() {
		$a = array('success' => true);

		$updStr = "UPDATE trpolizainterfacetbl ".
	              "SET fechaMovimiento = '".$_REQUEST['trfechaPagoPolizaHdn']."' ".
	              "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
	              "AND folio = '".$_REQUEST['trap485PolizaTxt']."' ".
	              "AND idViajeTractor = ".$_REQUEST['idViajeTractorHdn'];
	    fn_ejecuta_sql($updStr);
	    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
	        $a['success']     = false;
	        $a['msjResponse'] = $_SESSION['error_sql'];
	        $a['sql']         = $updStr;
	    }
	    fn_ejecuta_sql($a['success']);

	    echo json_encode($a);
	}
?>
