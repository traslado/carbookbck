<?php
    session_start();
	
	//include 'alCargaArchivoLzc.php';
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");
	
    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["alCinicialCargaFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["alCinicialCargaFld"]["error"];

		} else {
			$temp_file_name = $_FILES['alCinicialCargaFld']['tmp_name']; 
			$original_file_name = $_FILES['alCinicialCargaFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
		  
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLS($new_name);
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

    function leerXLS($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) <= 67 && $highestRow <= 3 ) {
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) < 68) {
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17) {
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN-Simbolo-Distribuidor-Ubicacion-TipoMercado-NombreBarco]', 'fail'=>'Y');
			return $root;
		}
		//------------------------------------------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
								'Simbolo'=>array('val'=>'','size'=>true,'exist'=>true),
								'Distribuidor' =>array('val'=>'','size'=>true,'exist'=>true)
							);
		
		for($row = 0; $row<sizeof($rowData); $row++){
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['Simbolo']['val'] = $rowData[$row][1];
			//$isValidArr['Simbolo']['size'] = strlen($rowData[$row][1]) > 0;
			$isValidArr['Distribuidor']['val'] = $rowData[$row][2];
			//$isValidArr['Distribuidor']['size'] = strlen($rowData[$row][2]) == 5;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {
				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}
				if($rowData[$row][1] == null) {
					$errorMsg .='EL '.$key.' Simbolo no existe';
					echo json_encode("Simbolo ".$rowData[$row][1]);
					$isTrue = false;
				}if($rowData[$row][2] == null) {
					$errorMsg .='El '.$key.' distribuidor no existe';
					echo json_encode(" Distribuidor ".$rowData[$row][2]);
					$isTrue = false;					
				}
				else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						if (!$isValidArr[$key]['exist']) {
							$siNo = ($key == 'VIN') ? ' ya ' : ' no ';
							$errorMsg .= 'El '.$key.$siNo.'Existe!';
							$isTrue = false; 
						}
					}
				}
			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
				$_SESSION['usuCompania'] = 'LZC02';
				
			
				$today = date("Y-m-d H:i:s");
				$fecha = substr($today,0,10);
				$hora=substr($today,11,8);

				$sqlGetSimbolo = "SELECT marca ".
								 "FROM casimbolosunidadestbl ".
								 "WHERE simboloUnidad = '".$rowData[$row][1]."' ";

				$rsGetSimbolo = fn_ejecuta_query($sqlGetSimbolo);

				$sqlGetDistribuidor = "SELECT tipoDistribuidor ".
								 "FROM caDistribuidoresCentrosTbl ".
								 "WHERE distribuidorCentro = '".$rowData[$row][2]."' ";

				$rsGetDistribuidor = fn_ejecuta_query($sqlGetDistribuidor);


				$numVin = $rowData[$row][0];
				$numAvanzada = substr($rowData[$row][0],9,17);
				$numSimbolo = $rowData[$row][1];
				$numCveDisFac = $rowData[$row][2];
				$numNomFac = $rowData[$row][3];
				$numDirEnt = $rowData[$row][4];
				$numFped = $rowData[$row][5];

				if($rsGetSimbolo['root'][0]['marca'] == 'KI' && $rsGetDistribuidor['root'][0]['tipoDistribuidor'] == 'DI' ){
					
					$tUnidad = 'DK';

				}
				elseif($rsGetSimbolo['root'][0]['marca'] == 'KI' && $rsGetDistribuidor['root'][0]['tipoDistribuidor'] == 'DX' ) {

					$tUnidad = 'RK';

				}

				elseif($rsGetSimbolo['root'][0]['marca'] == 'HY' && $rsGetDistribuidor['root'][0]['tipoDistribuidor'] == 'DX' ) {

					$tUnidad = 'RH';

				}
				elseif($rsGetSimbolo['root'][0]['marca'] == 'HY' && $rsGetDistribuidor['root'][0]['tipoDistribuidor'] == 'DI' ) {

					$tUnidad = 'DH';
				}

				$addHist = "INSERT INTO alInstruccionesMercedesTbl (cveDisFac,nomFac,cveDisEnt,dirEnt,cveLoc,fEvento,hEvento,fPed, vin, natType,modelDesc,especial,cveStatus) ".
					"VALUES('".$numCveDisFac."', ".
						"'".$numNomFac."', ".
						"'".$numCveDisFac."', ".
						"'".$numDirEnt."', 'LZC02', ".
						"'".$fecha."', ".
						"'".$hora."',".
						"'".$numFped."', ".
						"'".$numVin."', ".
						"'".$numAvanzada."', ".
			            "'".$numSimbolo."', ".
			            "'0', ".
			            "'".$tUnidad."') ";

					fn_ejecuta_query($addHist);	

				

				/*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCompania'],'PR',
						  $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCompania'], $repuve, $chofer, $observaciones,$rowData[$row][4]);*/

				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					$errorMsg = 'Agregado Correctamente';
				} else {
					$errorMsg = 'Registro No Agregado';
				}		
			}
			$root[]= array('vin'=>$rowData[$row][0],'simbolo'=>$rowData[$row][1], 'distribuidor'=>$rowData[$row][2],'ubicacion'=>$rowData[$row][3],'tipoMercado'=>$rowData[$row][4],'barco'=>$rowData[$row][5],'nose'=>$errorMsg);			

			//echo json_encode($rowData[$row]);
		}
		return $root;	
	}


	function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
		$tabla = 'tabla';
		$columna = 'columna';
		switch(strtoupper($field)) {
			case 'VIN':
				$tabla = 'alInstruccionesMercedesTbl'; $columna = 'vin'; break;
			case 'SIMBOLO':
				$tabla = 'caSimbolosUnidadesTbl'; $columna = 'simboloUnidad'; break;
			case 'DISTRIBUIDOR':
				$tabla = 'caDistribuidoresCentrosTbl'; $columna = 'distribuidorCentro'; break;


		}

	if ($tabla == 'alInstruccionesMercedesTbl') {
		
		$sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."' and cveLoc='LZC02'";
		$rs = fn_ejecuta_query($sqlExist);
	}
	else
	{

		$sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
		$rs = fn_ejecuta_query($sqlExist);
	}


		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
}
		




?>