<?php

/*********************************************
Sólo necesita reciboGastosIdViajeTractor para funcionar
*********************************************/
require_once("../funciones/fpdf/fpdf.php");
require_once("../funciones/generales.php");
require_once("../funciones/construct.php");
require_once("../funciones/utilidades.php");
require_once("../funciones/funcionesGlobales.php");
require_once("trGastosViajeTractor.php");

switch($_SESSION['idioma']){
    case 'ES':
        include_once("../funciones/idiomas/mensajesES.php");
        break;
    case 'EN':
        include_once("../funciones/idiomas/mensajesEN.php");
        break;
    default:
        include_once("../funciones/idiomas/mensajesES.php");
}
$a = array();
$e = array();

$a['success'] = true;
if($_REQUEST['pagoDieselIdViajeHdn1'] == "" && $_REQUEST['reciboGastosTractorHdn'] == ""){
    $e[] = array('id'=>'pagoDieselIdViajeHdn1 o reciboGastosTractorHdn','msg'=>getRequerido());
    $a['success'] = false;
}
$gFolio = $_REQUEST['folio'];
$pdf = new FPDF('P', 'mm', array(100, 150));

if ($a['success']) {
    $viaje = array();

    if($_REQUEST['pagoDieselIdViajeHdn1'] == ""){
        $sqlGetUltimoViajeTractor = "SELECT MAX(vt.viaje) AS viaje,vt.idViajeTractor, null AS idViajeAcomp ".
                                    "FROM trViajesTractoresTbl vt ".
                                    "WHERE vt.idTractor =".$_REQUEST['reciboGastosTractorHdn']." ".
                                    "AND vt.viaje = (select MAX(viaje) from trviajestractorestbl ".
                                    "where idTractor=".$_REQUEST['reciboGastosTractorHdn'].") ";
        $rs = fn_ejecuta_query($sqlGetUltimoViajeTractor);
        //echo json_encode($sqlGetUltimoViajeTractor);

        if($rs['records'] > 0){
            array_push($viaje,$rs['root'][0]['idViajeTractor']);

            if(isset($rs['root'][0]['idViajeAcomp']) && $rs['root'][0]['idViajeAcomp'] != "") {
                array_push($viaje, $rs['root'][0]['idViajeAcomp']);
            }
        } else {
            $a['errorMessage'] = "Error al obtener viaje del tractor ". $_REQUEST['reciboGastosTractorHdn'];
            $a['errors'] = $e;
            $a['success'] = false;
            echo json_encode($a);
        }
    } else {
        array_push($viaje, $_REQUEST['pagoDieselIdViajeHdn1']);
    }

    if($a['success']){
        foreach ($viaje as $idViaje) {
            $sqlGetDataStr = "SELECT CONCAT(ch.claveChofer,' - ',ch.nombre,'',ch.apellidoPaterno,'',ch.apellidoMaterno ) as nombreChofer, CONCAT(tr.compania,' - ', tr.tractor) as tractor, vt.viaje, ".
                            "(SELECT pl.plaza FROM caplazastbl pl WHERE pl.idPlaza = vt.idPlazaOrigen) as plazaOrigen, ".
                            "(SELECT p2.plaza FROM caplazastbl p2 WHERE p2.idPlaza = vt.idPlazaDestino) as plazaDestino, ".
                            "(SELECT gt.importe FROM trgastosviajetractortbl gt WHERE gt.idViajeTractor = vt.idViajeTractor AND gt.folio = ".$_REQUEST['folio']." AND gt.concepto = 2312 ) as numLitros, ".
                            "(SELECT gt.importe FROM trgastosviajetractortbl gt WHERE gt.idViajeTractor = vt.idViajeTractor AND gt.folio = ".$_REQUEST['folio']." AND gt.concepto = 2313 ) as montoLitros, ".
                            "(SELECT gt.importe FROM trgastosviajetractortbl gt WHERE gt.idViajeTractor = vt.idViajeTractor AND gt.folio = ".$_REQUEST['folio']." AND gt.concepto = 2314 ) as importeLitros, ".
                            "(SELECT LPAD(gt.folio,4,0) FROM trgastosviajetractortbl gt WHERE gt.idViajeTractor = vt.idViajeTractor AND gt.folio = ".$_REQUEST['folio']." AND gt.concepto = 2314 ) as  numFolio, ".
                            "(SELECT count(distinct(tl.distribuidor)) FROM trtalonesviajestbl tl WHERE tl.idViajeTractor = vt.idViajeTractor ) as numRepartos, ".
                            "(SELECT count(tl.distribuidor) FROM trtalonesviajestbl tl WHERE tl.idViajeTractor = vt.idViajeTractor ) as numtalones, ".
                            "vt.kilometrosTabulados, tr.rendimiento, vt.numeroUnidades ".
                            "FROM trviajestractorestbl vt, cachoferestbl ch, catractorestbl tr ".
                            "WHERE vt.claveChofer = ch.claveChofer ".
                            "AND tr.idTractor = vt.idTractor ".
                            "AND vt.idViajeTractor = '".$_REQUEST['pagoDieselIdViajeHdn1']."';";
            $rs = fn_ejecuta_query($sqlGetDataStr);

            $data = $rs['root'][0];
            if (sizeof($data) > 0) {
                $pdf = generarPdf($idViaje, $data, $pdf, $nInt);
            }
        }

        $pdf->Output('RECIBO DE '.$data['nombreChofer'].'.pdf', 'I');
    }
} else {
    $a['errorMessage'] = getErrorRequeridos();
    $a['errors'] = $e;
    echo json_encode($a);
}

function generarPdf($viaje, $data, $pdf, $n){
    //Solo para pruebas, por defecto 0
   /* $lsWhereStr = "";
    if($_REQUEST['CveMovHdn'] != "" && $_REQUEST['CveMovHdn'] == "GM"){
        $lsWhereStr = "AND tg.claveMovimiento = 'GM' AND tg.fechaEvento = ".
                      "(SELECT MAX(tg3.fechaEvento) FROM trGastosViajeTractorTbl tg3) AND tg.claveMovimiento = 'GM'";

        $lsWhereStr2 = "AND tg2.claveMovimiento = 'GM' AND tg2.fechaEvento = ".
                      "(SELECT MAX(tg3.fechaEvento) FROM trGastosViajeTractorTbl tg3) AND tg2.claveMovimiento = 'GM'";
    }

    $sqlGetCalculosGastosComplementosStr = "SELECT tg.concepto AS id, tg.observaciones, ".
                                        "(SELECT tg2.importe FROM trgastosviajetractortbl tg2 ".
                                            "WHERE tg2.idViajeTractor = tg.idViajeTractor AND tg2.concepto = tg.concepto AND tg2.claveMovimiento <> 'GX' $lsWhereStr2 ) AS cantidad, ".
                                          "(SELECT nombre FROM caconceptostbl cc WHERE cc.concepto = tg.concepto) AS DescConcepto ".
                                          "FROM trgastosviajetractortbl tg ".
                                          "WHERE tg.idViajeTractor = ".$viaje." ".$lsWhereStr.
                                          " GROUP BY tg.concepto;";


    $rsConcepto = fn_ejecuta_query($sqlGetCalculosGastosComplementosStr);
    */
    if($_REQUEST['CveMovHdn'] == "GM"){
        $selStr = "SELECT tg.concepto AS id, tg.observaciones,".
                "(SELECT tg2.importe FROM trgastosviajetractortbl tg2 WHERE tg2.idViajeTractor = tg.idViajeTractor AND tg2.concepto = tg.concepto AND tg2.claveMovimiento = 'GM' and tg2.folio = '".$_REQUEST['folio']."') AS cantidad,".
                "(SELECT nombre FROM caconceptostbl cc WHERE cc.concepto = tg.concepto) AS DescConcepto ".
                "FROM trgastosviajetractortbl tg ".
                "WHERE tg.idViajeTractor = ".$viaje." ".
                "AND tg.folio = '".$_REQUEST['folio']."' ".
                "GROUP BY tg.concepto";
        $rsConcepto = fn_ejecuta_query($selStr);
    }
    else
    {
        $selStr = "SELECT tg.concepto AS id, tg.observaciones, ".
                "(SELECT tg2.importe FROM trgastosviajetractortbl tg2 ".
                "WHERE tg2.idViajeTractor = tg.idViajeTractor AND tg2.concepto = tg.concepto AND tg2.claveMovimiento <> 'GX' $lsWhereStr2 AND tg2.folio = ".$_REQUEST['folio'].") AS cantidad, ".
                "(SELECT nombre FROM caconceptostbl cc WHERE cc.concepto = tg.concepto) AS DescConcepto ".
                "FROM trgastosviajetractortbl tg ".
                "WHERE tg.idViajeTractor = ".$viaje." ".
                "AND tg.folio = '".$_REQUEST['folio']."' ".
                "GROUP BY tg.concepto;";
        $rsConcepto = fn_ejecuta_query($selStr);
    }

    $border = 0;
    if(sizeof($data) > 0){
        $pdf->AddPage();
        $pdf->SetMargins(0.2, 0.2, 0.2, 0.2);
        if ($n > 0) {
            $pdf->SetY(10);
            $pdf->SetX(10);
        }
        //Etiqueta dependiendo de la clave Movimiento
        //echo print_r($data);
        //return;
        //Nombre de la empresa
        $pdf->setY(5);
        $pdf->SetX(0);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,10,'TRANSDRIZA, S.A DE C.V.', $border,0, 'C');
        //folio
        $pdf->setY(5);
        $pdf->SetX(0);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(157,10,'FOLIO: '.$_REQUEST['folio'], $border,0, 'C');//$data['numFolio']
        //titulo Recibo
        $pdf->setY(8);
        $pdf->SetX(0);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(0,10,'RECIBO DEPOSITO DIESEL', $border,0, 'C');
        //numeroTalon
        $pdf->setY(5);
        $pdf->SetX(0);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(93,10,$data['folio'], $border,0, 'R');
        //FechaOriginal
        $pdf->setY(8);
        $pdf->SetX(0);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(93,10,date("d/m/Y H:i:s"), $border,0, 'R');

        //OPERADOR
        $pdf->SetY(12);
        $pdf->SetX(5);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'OPERADOR: '.$data['nombreChofer'], $border,1, 'L');

        //TRACTOR
        $pdf->SetY(12);
        $pdf->SetX(60);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'TRACTOR: '.$data['tractor'], $border,1, 'L');

        //VIAJE
        $pdf->SetY(12);
        $pdf->SetX(84);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'VIAJE: '.$data['viaje'], $border,1, 'L');

        //ORIGEN
        $pdf->SetY(15);
        $pdf->SetX(5);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'ORIGEN: '.$data['plazaOrigen'], $border,1, 'L');

        //DESTINO
        $pdf->SetY(15);
        $pdf->SetX(40);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'DESTINO: '.$data['plazaDestino'], $border,1, 'L');

        //KILOMETROS TABULADOS
        $pdf->SetY(15);
        $pdf->SetX(81);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'KMS: '.$data['kilometrosTabulados'], $border,1, 'L');

        //KILOMETROS TABULADOS
        $pdf->SetY(18);
        $pdf->SetX(5);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'RENDIMIENTO: '.$data['rendimiento'], $border,1, 'L');

        //NUMERO DE REPARTOS
        $pdf->SetY(18);
        $pdf->SetX(30);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'NO. REPARTOS: '.$data['numRepartos'], $border,1, 'L');

        //NUMERO DE TALONES
        $pdf->SetY(18);
        $pdf->SetX(53);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'NO. TALONES: '.$data['numtalones'], $border,1, 'L');

        //NUMERO DE UNIDADES
        $pdf->SetY(18);
        $pdf->SetX(76);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'NO. UNIDADES: '.$data['numeroUnidades'], $border,1, 'L');

        //sumaDeLitros
        $pdf->SetY(23);
        $pdf->SetX(10);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'COMBUSTIBLE: '.number_format($data['numLitros'],2,'.',',').' Lts', $border,1, 'L');

        //montoCombustible
        $pdf->SetY(26);
        $pdf->SetX(10);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'COSTO:              $ '.number_format($data['montoLitros'],2,'.',','), $border,1, 'L');

        //TOTAL COMBUSTIBLE
        $pdf->SetY(29);
        $pdf->SetX(10);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'TOTAL:               $ '.number_format($data['importeLitros'],2,'.',','), $border,1, 'L');

        /**
         * CONCEPTOS
         */
        $y = 20;
        $x = 8;
        //for ($nInt=0; $nInt < sizeof($concepto); $nInt++) {
        $totalImporte = $data['importeLitros'];

        //TOTAL KILOMETROS
        $TotalKms = $data['kilometrosTabulados'] ;
        $TotalKms = number_format($TotalKms, 2, ".", ",");
        $pdf->SetY(34);
        $pdf->SetX(8);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'RECORRIDO TOTAL: '.number_format($TotalKms,2,'.',',').' Kms.', $border,1, 'L');

        //TOTAL GASTOS
        $totalImporte = ($totalImporte/100) * 100;

        //NUMERO EN LETRAS
        $pdf->SetY(37);
        $pdf->SetX(8);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,numtoletras($totalImporte), $border,1, 'L');

        $totalImporte = number_format($totalImporte, 2, ".", ",");

        //FIRMAS
        //CAPUTURADOR
        $pdf->SetY(60);
        $pdf->SetX(12);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'----------------------------------------------', $border,1, 'L');
        $pdf->SetY(62);
        $pdf->SetX(12);
        $pdf->Cell(40,10,$_SESSION['nombreUsr'] , $border,1, 'L');
        //OPERADOR
        $pdf->SetY(60);
        $pdf->SetX(60);
        $pdf->SetFont('Arial','',5);
        $pdf->Cell(40,10,'----------------------------------------------', $border,1, 'L');
        $pdf->SetY(62);
        $pdf->SetX(64);
        $pdf->Cell(40,10,'FIRMA DEL OPERADOR' , $border,1, 'L');
        //ISO
        $pdf->SetY(68);
        $pdf->SetX(62);
        $pdf->Cell(40,10,$data['ISO'] , $border,1, 'L');

        return $pdf;
    } else {
      echo json_encode(array('success'=>false, 'errorMessage'=>$_SESSION['error_sql']." <br> ".$sqlGetDataStr));
    }
}
?>
