<?php
	ini_set('max_execution_time', 3000);
	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/generalesSICA.php");
    //require_once("../funciones/construct.php");
    //require_once("../funciones/utilidades.php");


    function generaInterfaseSICA($patio,$dia,$tipo,$numero,$tipoPoliza){
    	$a = array('success' => true, 'msjResponse' => 'Interfase generada correctamente.', 'nombreArchivo'=>'', 'existeTmp'=>0);
		
		setlocale(LC_TIME, 'spanish');		
		$dir 		= "E:/carbook/i477/respArchivo/";
		
		
		##### GASTOS #####
				
		$arch 	= $_REQUEST['nombreArchivo'];
		$arch1 	= $_REQUEST['nombreArchivo'].'.nvo';

		$sql = "SELECT * FROM trPolizaInterfaceTbl".
				" WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
				" AND SUBSTRING(fechaMovimiento,1,10) = '".$_REQUEST['fechaInicio']."'".
				" AND tipoDocumento IN ('G')".
				" AND claveMovimiento = 'T'";
		//echo "$sql<br>";
		$rsGastos = fn_ejecuta_query($sql);
		$rsGastos1 = $rsGastos;
		//var_dump($rsGastos);		
		
		$idx = 0;
		$a['nombreArchivo'] = $dir.$arch;
		$a['nombreArchivo1'] = $dir.$arch1;
		$fp = fopen($a['nombreArchivo'], 'w');
		$fp1 = fopen($a['nombreArchivo1'], 'w');
		foreach($rsGastos['root'] AS $index => $row)
		{
			$sql = "SELECT a.*, b.tipoCuenta".
					" FROM trGastosViajeTractorTbl a, caConceptosCentrosTbl b".
					" WHERE a.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					" AND SUBSTRING(a.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
					" AND a.folio = '".$row['folio']."'".
					" AND a.concepto NOT IN (select valor from cageneralestbl where tabla='conceptos' and columna='9000')".
					" AND a.claveMovimiento != 'GX'".							#>>DIF. DE CANCELADO <<
					" AND (a.claveMovimiento = 'GC' OR ".						#>>COMPLEMENTO P/POLIZA CONTABLE <<
					" a.claveMovimiento in ('GP','AS'))".								#>>EXCEDENTE ALIMENTOS/DIESEL   <<
					" AND b.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					" AND b.concepto = a.concepto".
					" ORDER BY a.folio, b.tipoCuenta, a.concepto";
			//echo "$sql<br>";
			$rsCtas = fn_ejecuta_query($sql);

			//var_dump($rsCtas);
			
			$ind = 0;
			foreach($rsCtas['root'] AS $index2 => $row2)
			{
				$cvecia = 4;												
				$aux 	= explode('-',substr($row2['fechaEvento'],0,10));
				$anio	= $aux[0];				
				$mes 	= $aux[1];
				$mes1	= $aux[1];
				$dia 	= $aux[2];
				$fecha	= $anio.'-'.$mes.'-'.$dia;
										
				if((int) $mes == (int) $row2['mesAfectacion'])
				{
					$anio 	= substr($row2['fechaEvento'],0,4);
					$mes 	= substr($row2['fechaEvento'],5,2);
					$fecpol	= $fecha;
				}
				else
				{
					$mes = $row2['mesAfectacion'];								
					if((int) $mes1 < (int) $row2['mesAfectacion'])
					{
						$anio 	= substr($row2['fechaEvento'],0,4) - 1;										
						$dia 	= ultimoDiaMes($anio, $mes);
						$fecpol = $dia.'/'.str_pad($mes, 2, "0", STR_PAD_LEFT).'/'.$anio;										
					}
					else
					{
						$anio 	= substr($row2['fechaEvento'],0,4);
						$dia 	= ultimoDiaMes($anio, $mes);
						$fecpol = $dia.'/'.str_pad($mes, 2, "0", STR_PAD_LEFT).'/'.$anio;
					}
				}
				$numpol = str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
				$tippol = $_REQUEST['tipo'];
				$ctacon = rtrim($row2['cuentaContable']);
				//var_dump($ctacon);
				$numche = 0;
				$cargo = 0;
				$abono = str_pad($row2['subTotal'], 17, " ", STR_PAD_LEFT);
				if(rtrim($row2['tipoCuenta']) == 'C')
				{
					$cargo = $row2['subTotal'];
					$abono = str_pad('0.00', 17, " ", STR_PAD_LEFT);						
				}
				$desmov = 'LIQUIDACION DE GTOS(REM '.str_pad($_REQUEST['i477Remesa'], 2, " ", STR_PAD_LEFT).') '.str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
				if($row2['claveMovimiento'] == 'GC')			# Complemento de gastos
				{
					$desmov = 'COMP LIQUID DE GTOS(REM '.str_pad($_REQUEST['i477Remesa'], 2, " ", STR_PAD_LEFT).') '.str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
				}
				$tippro = 0;
				$ind++;
				$orden = $ind;

				$selTipoPoliza="SELECT * FROM conTiposPolizasTbl WHERE claveCompania=4  AND tipoPoliza=".$tipo."";
				$rsTipoPoliza=fn_ejecuta_query_SICA($selTipoPoliza);

				// echo json_encode($selTipoPoliza);

				$sqlFolio="select * from conFoliosPolizasTbl ".
							"where anio='".$anio."' ".
							"AND claveCompania='4' ".
							"and mes='".$mes."' ".
							"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
				$rsFolio=fn_ejecuta_query_SICA($sqlFolio);


				if (sizeof($rsFolio['root'][0]['folio'])=='0') {
					$insFolio="INSERT INTO conFoliosPolizasTbl (claveCompania, anio, mes, idTipoPoliza, folio)  ".
							"VALUES ('4', '".$anio."', '".$mes."', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', '1')";
					fn_ejecuta_query_SICA($insFolio);
					/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
				      {
				         $a['success']		 = false;
				         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
				         $a['sql'] = $sql;
				      }_*/

					$folio='1';

					// echo json_encode($insFolio);

				}else{
					$folio=$rsFolio['root'][0]['folio'];

					/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
				      {
				         $a['success']		 = false;
				         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la folio.";
				         $a['sql'] = $sql;
				      }*/
					// echo json_encode($updFolio);
				}

				$sqlCuenta = "SELECT * FROM conCatCuentasTbl where cuenta = '".$ctacon."';";
				$rsCuenta = fn_ejecuta_query_SICA($sqlCuenta);
				if (sizeof($rsCuenta['root']) == 0) {
					$existeCuenta = generaCuenta($ctacon);
					if ($existeCuenta['success']) {
						$rsCuenta['root'][0]['idCuentaContable'] = $existeCuenta['idCuentaContable'];
					}
					// $a['success'] = $existeCuenta['success'];
				}

				$insDetalle="INSERT INTO conPolizasDetalleTbl (claveCompania, idTipoPoliza, fecha, anio, mes, numero, sec, idCuentaContable, referencia, descripcion, cargosBase, abonosBase, cargos, abonos, idTipoComprobante) ".
						"VALUES ('4', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', ".
						"'".$_REQUEST['fechaInicio']."', ".
						"'".$anio."', ".
						"'".$mes."', ".
						"'".$folio."', ".
						"'".$orden."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '".$desmov."', ".
						"'".$cargo."', ".
						"'".$abono."', ".
						"'".$cargo."', ".
						"'".$abono."', '0')";
				fn_ejecuta_query_SICA($insDetalle);

				//echo json_encode($insRegistro);
				
			}

			$selTipoPoliza="SELECT * FROM conTiposPolizasTbl WHERE claveCompania=4  AND tipoPoliza=".$tipo."";
			$rsTipoPoliza=fn_ejecuta_query_SICA($selTipoPoliza);

			// echo json_encode($selTipoPoliza);

			$sqlFolio="select * from conFoliosPolizasTbl ".
						"where anio='".$anio."' ".
						"AND claveCompania='4' ".
						"and mes='".$mes."' ".
						"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
			$rsFolio=fn_ejecuta_query_SICA($sqlFolio);


			if (sizeof($rsFolio['root'][0]['folio'])=='0') {
				$insFolio="INSERT INTO conFoliosPolizasTbl (claveCompania, anio, mes, idTipoPoliza, folio)  ".
						"VALUES ('4', '".$anio."', '".$mes."', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', '1')";
				fn_ejecuta_query_SICA($insFolio);
				/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
			      {
			         $a['success']		 = false;
			         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
			         $a['sql'] = $sql;
			      }_*/

				$folio='1';

				// echo json_encode($insFolio);

			}else{
				$folio=$rsFolio['root'][0]['folio'];
				$folioupd=$rsFolio['root'][0]['folio']+1;

				$updFolio="UPDATE conFoliosPolizasTbl SET folio=".$folioupd ." where anio='".$anio."' ".
						"AND claveCompania='4' ".
						"AND idTipoPoliza='".$rsTipoPoliza['root'][0]['idTipoPoliza']."' ".
						"and mes='".$mes."'";
				fn_ejecuta_query_SICA($updFolio);
				/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
			      {
			         $a['success']		 = false;
			         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la folio.";
			         $a['sql'] = $sql;
			      }*/
				// echo json_encode($updFolio);
			}

			$sqlSum="SELECT sum(cargosBase) as sumaCargos, sum(abonosBase) as sumaAbonos from conPolizasDetalleTbl
						WHERE anio='".$anio."'
						and numero='".$folio."'
						AND idTipoPoliza='".$rsTipoPoliza['root'][0]['idTipoPoliza']."' 
						AND mes='".$mes."'; ";
			$rsSum=fn_ejecuta_query_SICA($sqlSum);

			$insRegistro="INSERT INTO conPolizasTbl (claveCompania, idTipoPoliza, fecha, anio, mes, numero, nombre, divisa, paridad, cargosBase, abonosBase, cargos, abonos, estatus, usuario, fechaCaptura) ".
						"VALUES ('4', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', 
						'".$_REQUEST['fechaInicio']."', 
						'".$anio."', 
						'".$mes."', 
						'".$folio."', 
						'".$desmov."', 'MXN', '1.0000', '".$rsSum['root'][0]['sumaCargos']."', 
						'".$rsSum['root'][0]['sumaAbonos']."', '".$rsSum['root'][0]['sumaCargos']."', '".$rsSum['root'][0]['sumaAbonos']."', '01', '1', now())";
			fn_ejecuta_query_SICA($insRegistro);

			$sqlCuenta="SELECT * FROM conCatCuentasTbl where cuenta = '".$ctacon."';";
			$rsCuenta=fn_ejecuta_query_SICA($sqlCuenta);
		}
		fclose($fp);
		fclose($fp1);

		//Verifica si existe el directorio tmp, sino lo crea
		if (!file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
			mkdir('../../carbookv1.2/modules/interfacesContables/tmp/', 0777);
		}
		if (file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
			copy($a['nombreArchivo1'], '../../carbookv1.2/modules/interfacesContables/tmp/'.$arch1);
			$a['existeTmp'] = 1;
		}	
		
		
		##### SUELDOS #####
				
		$arch2 	= $_REQUEST['nombreArchivo'].'.sdo';
		$arch22 = $arch2.'.nvo';

		$sql = "SELECT * FROM trPolizaInterfaceTbl".
				" WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
				" AND SUBSTRING(fechaMovimiento,1,10) = '".$_REQUEST['fechaInicio']."'".
				" AND tipoDocumento IN ('S')".
				// " AND importe!= '0.00'".
				" AND claveMovimiento = 'T'";
		//echo "$sql<br>";
		$rsGastos = fn_ejecuta_query($sql);
		$rsSueldos = $rsGastos;
		//var_dump($rsGastos);		
				
		if($rsGastos['records'] > 0)
		{
			$idx = 0;
			$a['nombreArchivo2'] = $dir.$arch2;
			$a['nombreArchivo22'] = $dir.$arch22;
			$fp2 	= fopen($a['nombreArchivo2'], 'w');
			$fp22 = fopen($a['nombreArchivo22'], 'w');				
			$folant = 0;
			foreach($rsGastos['root'] AS $index => $row)
			{						
				if($_REQUEST['tipo'] == '6')
				{
					$sql = "SELECT a.*, b.tipoCuenta".
						 " FROM trGastosViajeTractorTbl a, caConceptosCentrosTbl b".
						 " WHERE a.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
						 " AND SUBSTRING(a.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
						 " AND a.folio = '".$row['folio']."'".
						 " AND a.claveMovimiento = 'GS'".    					# >>> SUELDO PAGADO <<<
						 " AND a.concepto = '141'".
						 " AND SUBSTRING(a.cuentaContable, 4, 1) = '.'".											 
						 " AND b.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
						 " AND b.concepto = a.concepto".
						 " ORDER BY 4, 17, 2";			//ORDER BY folio, tipoCuenta, concepto
				}
				else
				{
					$sql = "SELECT a.*, b.tipoCuenta".
						" FROM trGastosViajeTractorTbl a, caConceptosCentrosTbl b".
						" WHERE a.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
						" AND SUBSTRING(a.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
						" AND a.folio = '".$row['folio']."'".
						" AND a.claveMovimiento = 'GS'".    					# >>> SUELDO PAGADO <<<
						" AND SUBSTRING(a.cuentaContable, 4, 1) = '.'".				
						" AND b.concepto NOT IN ('138') ".							 
						" AND b.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
						" AND b.concepto = a.concepto".
						" UNION ".
						" SELECT c.*, d.tipoCuenta".
						" FROM trGastosViajeTractorTbl c, caConceptosCentrosTbl d".
						" WHERE c.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
						" AND SUBSTRING(c.fechaEvento,1,10) = '".substr($row['fechaPoliza'], 0, 10)."'".
						" AND c.folio = '".$row['folio']."'".
						" AND c.claveMovimiento = 'GS'".    					# >>> SUELDO PAGADO <<<
						" AND c.concepto IN ('143', '137','136','147','149')".
						" AND SUBSTRING(c.cuentaContable, 4, 1) = '.'".											 
						" AND d.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
						" AND d.concepto = c.concepto".
						" ORDER BY 4, 17, 2";			//ORDER BY folio, tipoCuenta, concepto

				}
				//echo "$sql<br>";
				$rsCtas = fn_ejecuta_query($sql);
									
				//var_dump($rsCtas);
				
				$ind = 0;

				$conceptosDeducciones = array('8000','8001','8002','8003','8004','2331','2227','2228','2332','2233','2230','2231','2334');

				foreach($rsCtas['root'] AS $index2 => $row2)
				{
					if(in_array($row2['concepto'], $conceptosDeducciones))
					{
						$row2['subTotal']= $row2['subTotal'] * -1;
					}

					$cvecia = 4;												
					$aux = explode('-',substr($row['fechaMovimiento'],0,10));
					$anio = $aux[0];				
					$mes = $aux[1];
					$dia = $aux[2];
					$fecha = $anio.'-'.$mes.'-'.$dia;

					$anio 	= (int) substr($row['fechaMovimiento'],0,4);
					$mes 		= (int) substr($row['fechaMovimiento'],5,2);
					$fecpol	= $fecha;
					$numpol = str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
					$tippol = $_REQUEST['tipo'];
					$ctacon = rtrim($row2['cuentaContable']);
					//var_dump($ctacon);
					$numche = 0;
					$cargo = '0.00';
					$abono = $row2['subTotal'];
					if(rtrim($row2['tipoCuenta']) == 'C')
					{
						$cargo = $row2['subTotal'];
						$abono = '0.00';
					}
					$desmov = 'LIQUIDACION DE SDOS(REM '.str_pad($_REQUEST['i477Remesa'], 2, " ", STR_PAD_LEFT).') '.str_pad($row2['folio'], 6, "0", STR_PAD_LEFT);
					$tippro = 0;
					$ind++;
					$orden = $ind;
					
					/*fputs($fp2, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');*/

					switch ($tipo) {
						case '7':
							$tipo='13';
						break;
						case '8':
							$tipo='14';
						break;
						case '10':
							$tipo='15';
						break;
						case '11':
							$tipo='16';
						break;
						case '12':
							$tipo='17';
						break;
						default:
							# code...
							break;
					}

					$selTipoPoliza="SELECT * FROM conTiposPolizasTbl WHERE claveCompania=4  AND tipoPoliza=".$tipo."";
					$rsTipoPoliza=fn_ejecuta_query_SICA($selTipoPoliza);

					// echo json_encode($selTipoPoliza);

					$sqlFolio="select * from conFoliosPolizasTbl ".
								"where anio='".$anio."' ".
								"AND claveCompania='4' ".
								"and mes='".$mes."' ".
								"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
					$rsFolio=fn_ejecuta_query_SICA($sqlFolio);


					if (sizeof($rsFolio['root'][0]['folio'])=='0') {
						$insFolio="INSERT INTO conFoliosPolizasTbl (claveCompania, anio, mes, idTipoPoliza, folio)  ".
								"VALUES ('4', '".$anio."', '".$mes."', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', '1')";
						fn_ejecuta_query_SICA($insFolio);
						/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
					      {
					         $a['success']		 = false;
					         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
					         $a['sql'] = $sql;
					      }_*/

						$folio='1';

						// echo json_encode($insFolio);

					}else{
						$folio=$rsFolio['root'][0]['folio'];

						/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
					    {
					         $a['success']		 = false;
					         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la folio.";
					         $a['sql'] = $sql;
					      }*/
						// echo json_encode($updFolio);
					}

					$sqlCuenta="SELECT * FROM conCatCuentasTbl where cuenta = '".$ctacon."';";
					$rsCuenta = fn_ejecuta_query_SICA($sqlCuenta);
					if (sizeof($rsCuenta['root']) == 0) {
						$existeCuenta = generaCuenta($ctacon);
						if ($existeCuenta['success']) {
							$rsCuenta['root'][0]['idCuentaContable'] = $existeCuenta['idCuentaContable'];
						}
						// $a['success'] = $existeCuenta['success'];
					}

					$insDetalle="INSERT INTO conPolizasDetalleTbl (claveCompania, idTipoPoliza, fecha, anio, mes, numero, sec, idCuentaContable, referencia, descripcion, cargosBase, abonosBase, cargos, abonos, idTipoComprobante) ".
								"VALUES ('4', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', ".
								"'".$_REQUEST['fechaInicio']."', ".
								"'".$anio."', ".
								"'".$mes."', ".
								"'".$folio."', ".
								"'".$orden."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '".$desmov."', ".
								"'".$cargo."', ".
								"'".$abono."', ".
								"'".$cargo."', ".
								"'".$abono."', '0')";
					fn_ejecuta_query_SICA($insDetalle);

					$idx++;
					fputs($fp2,"\n");
			      	
					# Genera interface para nueva contabilidad armando cta con Numero de operador y/o num de tractor

					/*  	$sql = "SELECT cuentaContable FROM caConceptosCentrosTbl ".
					" WHERE centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					" AND concepto = '".$row2['concepto']."'";
					//echo "$sql<br>";
					$rsCtaConc = fn_ejecuta_query($sql);
					//var_dump($rsCtaConc);

					$ctacon = rtrim($rsCtaConc['root'][0]['cuentaContable']);

					$sql = "SELECT ch.cuentaContable FROM trViajesTractoresTbl vt, caChoferesTbl ch ".
					" WHERE vt.idViajeTractor = ".$row2['idViajeTractor'].
					" AND vt.centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
					" AND ch.claveChofer = vt.claveChofer";
					//echo "$sql<br>";
					$rsCtaChof = fn_ejecuta_query($sql);
					//var_dump($rsCtaChof);

					if(substr($ctacon, 0, 3) == '113' || substr($ctacon, 0, 3) == '116')
					{
						$ctacon = armaCuentaContable($ctacon, rtrim($rsCtaChof['root'][0]['cuentaContable']), '0');
					}
					else
					{
					$sql = "SELECT DISTINCT ca.tractor FROM trViajesTractoresTbl tr, caTractoresTbl ca".
							 " WHERE tr.idViajeTractor = ".$row2['idViajeTractor'].
							 " AND ca.idTractor = tr.idTractor";
					//echo "$sql<br>";		
					$rsTractor = fn_ejecuta_query($sql);
					//var_dump($rsObs);
					$ctacon = armaCuentaContable($ctacon, rtrim($rsTractor['root'][0]['tractor']), rtrim($rsCtaChof['root'][0]['cuentaContable']));
					}*/

					/*fputs($fp22, $cvecia. '|'. $anio. '|'. (int) $mes. '|'. $numpol. '|'. $fecpol. '|'. $tippol. '|'. $ctacon. '|'. $numche. '|'. $cargo. '|'. $abono. '|'. $desmov. '|'. $tippro. '|'. $orden. '|');*/														      	
				}

				if (sizeof($rsCtas['root']) > 0) {
					$selTipoPoliza="SELECT * FROM conTiposPolizasTbl WHERE claveCompania=4  AND tipoPoliza=".$tipo."";
					$rsTipoPoliza=fn_ejecuta_query_SICA($selTipoPoliza);

					// echo json_encode($selTipoPoliza);

					$sqlFolio="select * from conFoliosPolizasTbl ".
								"where anio='".$anio."' ".
								"AND claveCompania='4' ".
								"and mes='".$mes."' ".
								"and idTipoPoliza=".$rsTipoPoliza['root'][0]['idTipoPoliza']."";
					$rsFolio=fn_ejecuta_query_SICA($sqlFolio);


					if (sizeof($rsFolio['root'])==0 || $rsFolio['root'][0]['folio'] == 0) {
						$insFolio="INSERT INTO conFoliosPolizasTbl (claveCompania, anio, mes, idTipoPoliza, folio)  ".
								"VALUES ('4', '".$anio."', '".$mes."', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', '1')";
						fn_ejecuta_query_SICA($insFolio);
						/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
					      {
					         $a['success']		= false;
					         $a['msjResponse']	= $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
					         $a['sql'] = $sql;
					    }_*/

						$folio='1';
						// echo json_encode($insFolio);
					}else{
						$folio=$rsFolio['root'][0]['folio'];
						$folioupd=floatval($rsFolio['root'][0]['folio'])+1;

						$updFolio="UPDATE conFoliosPolizasTbl SET folio=".$folioupd ." where anio='".$anio."' ".
								"AND claveCompania='4' ".
								"AND idTipoPoliza='".$rsTipoPoliza['root'][0]['idTipoPoliza']."' ".
								"and mes='".$mes."'";
						fn_ejecuta_query_SICA($updFolio);
						/*if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
					    {
							$a['success']     = false;
							$a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la folio.";
							$a['sql'] = $sql;
					    }*/
						// echo json_encode($updFolio);
					}

					$sqlSum="SELECT sum(cargosBase) as sumaCargos, sum(abonosBase) as sumaAbonos from conPolizasDetalleTbl
								WHERE anio='".$anio."'
								AND idTipoPoliza='".$rsTipoPoliza['root'][0]['idTipoPoliza']."' 
								and numero='".$folio."'
								AND mes='".$mes."'; ";
					$rsSum=fn_ejecuta_query_SICA($sqlSum);

					$insRegistro="INSERT INTO conPolizasTbl (claveCompania, idTipoPoliza, fecha, anio, mes, numero, nombre, divisa, paridad, cargosBase, abonosBase, cargos, abonos, estatus, usuario, fechaCaptura) ".
								"VALUES ('4', '".$rsTipoPoliza['root'][0]['idTipoPoliza']."', 
								'".$_REQUEST['fechaInicio']."', 
								'".$anio."', 
								'".$mes."', 
								'".$folio."', 
								'".$desmov."', 'MXN', '1.0000', '".$rsSum['root'][0]['sumaCargos']."', 
								'".$rsSum['root'][0]['sumaAbonos']."', '".$rsSum['root'][0]['sumaCargos']."', '".$rsSum['root'][0]['sumaAbonos']."', '01', '1', now())";
						fn_ejecuta_query_SICA($insRegistro);

					$sqlCuenta="SELECT * FROM conCatCuentasTbl where cuenta = '".$ctacon."';";
					$rsCuenta=fn_ejecuta_query_SICA($sqlCuenta);
				}
			}
			fclose($fp2);
			fclose($fp22);
			
			//Verifica si existe el directorio tmp, sino lo crea
			if (!file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				mkdir('../../carbookv1.2/modules/interfacesContables/tmp/', 0777);
			}
			if (file_exists('../../carbookv1.2/modules/interfacesContables/tmp/')) {	
				copy($a['nombreArchivo22'], '../../carbookv1.2/modules/interfacesContables/tmp/'.$arch22);
				$a['existeTmp'] = 1;
			}				
		}
		else
		{
			$a['msjResponse'] = 'No existe informaci&oacuten a transmitir para sueldos entre las fechas.';
		}


		//Se realiza la actualización de la interfase contable		
		foreach($rsGastos1['root'] AS $index => $row)
		{
			$sql = "UPDATE trPolizaInterfaceTbl".
					" SET claveMovimiento = 'T'".					# Transmitido
					" WHERE centroDistribucion = '".$row['centroDistribucion']."'".
					" AND folio = '".$row['folio']."'".
					" AND SUBSTRING(fechaMovimiento,1,10) = '".substr($row['fechaMovimiento'],0,10)."'".
					" AND SUBSTRING(fechaPoliza,1,10) = '".substr($row['fechaPoliza'],0,10)."'".
					" AND concepto = '".$row['concepto']."'".
					" AND tipoDocumento = '".$row['tipoDocumento']."'".
					" AND importe = ".$row['importe'];
			//echo "$sql<br>";
			//fn_ejecuta_query($sql);	 		//CHK
			
			if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
			{
				$a['success']		= false;
				$a['msjResponse']	= $_SESSION['error_sql']. " En la actualizaci&oacuten de la Interfase.";
				$a['sql'] = $sql;
				break;
			}
		}
		
		foreach($rsSueldos['root'] AS $index => $row)
		{
			$sql = "UPDATE trPolizaInterfaceTbl".
					" SET claveMovimiento = 'T'".					# Transmitido
					" WHERE centroDistribucion = '".$row['centroDistribucion']."'".
					" AND folio = '".$row['folio']."'".
					" AND SUBSTRING(fechaMovimiento,1,10) = '".substr($row['fechaMovimiento'],0,10)."'".
					" AND SUBSTRING(fechaPoliza,1,10) = '".substr($row['fechaPoliza'],0,10)."'".
					" AND concepto = '".$row['concepto']."'".
					" AND tipoDocumento = '".$row['tipoDocumento']."'".							 
					" AND importe = ".$row['importe'];
			//echo "$sql<br>";
			//fn_ejecuta_query($sql);	 		//CHK
			
			if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
			{
				$a['success']		= false;
				$a['msjResponse']	= $_SESSION['error_sql']. " En la actualizaci&oacuten de la Interfase.";
				$a['sql'] = $sql;
				break;
			}
		}

		if($_REQUEST['insertaInterface'] == 1)
		{
			setlocale(LC_TIME, 'spanish');
			$horaActual = date('H:i:s');
			$sql = "INSERT INTO trInterfacesFechaTbl (centroDistribucion, tipoInterface, fechaInicio, fechaFinal, estatus) VALUES (".
					"'".$_REQUEST['centroDistribucion']."', ".
					"'C', ".
					//"NULL, ".
					"'".($_REQUEST['fechaInicio'].' '.$horaActual)."', ".
					"'".($_REQUEST['fechaInicio'].' '.$horaActual)."', ".
					"'G')";
			//echo "$sql<br>";
			//fn_ejecuta_query($sql);	 		//CHK
			if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
			{
				$a['success']		= false;
				$a['msjResponse']	= $_SESSION['error_sql']. " En la inserci&oacuten de la Interfase.";
				$a['sql'] = $sql;
			}
		}
	}

	function generaCuenta($pscuenta) {
		$a = array('success' => false);

		$cuentaArr = explode('.', $pscuenta);

		if (sizeof($cuentaArr) > 0) {
			$claveChofer = end($cuentaArr);

			if (strlen($claveChofer) == 5) {
				$cuentaPrev = substr($pscuenta, 0,-6);
				$selCtaSicaPrev = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${cuentaPrev}';";
				$rsCuentaPrev = fn_ejecuta_query_SICA($selCtaSicaPrev);

				if (sizeof($rsCuentaPrev['root']) > 0) {
					$selCuentaSica = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${pscuenta}';";
					$rsCuenta = fn_ejecuta_query_SICA($selCuentaSica);

					if (sizeof($rsCuenta['root']) == 0) {
						$selNombre = "SELECT concat(apellidoMaterno,' ', apellidoPaterno,' ', nombre) as nombre FROM caChoferesTbl where claveChofer = ".$claveChofer;
						$rsNombre = fn_ejecuta_query($selNombre);

						if (sizeof($rsNombre['root']) > 0) {
							$insertCuenta = " INSERT INTO conCatCuentasTbl (claveCompania, cuenta, nombre, afectable, tipo, naturaleza, ubicacion, reportar, manejaSegmentos, numeroSegmentos, divisa, codigoAgrupador, RFC, estatus, idUsuarioAct, fechaAct, ipAct, macAddressAct)".
											" VALUES ('4', '${pscuenta}', ".
											"'".$rsNombre['root'][0]['nombre']."', ".
											"'1', '1', '1', ' ', '1', '0', '0', 'MXN', ' ', ' ', '1', '1', now(), '10.212.134.204', '10.212.134.204')";
							$rsCta = fn_ejecuta_query_SICA($insertCuenta);

							$selCuentaSica = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${pscuenta}';";
							$rsCuenta = fn_ejecuta_query_SICA($selCuentaSica);

							for ($j=1; $j <= 12; $j++) { 
								$rsConSaldos="INSERT INTO conSaldosTbl (claveCompania, tipo, calendario, periodo, idCuentaContable, idCentroCostos, saldoInicial, cargos, abonos, saldoFinal) ".
											"VALUES ('4', '1', year(now()), '".$j."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '0.00', '0.00', '0.00', '0.00')";
								$rsSal = fn_ejecuta_query_SICA($rsConSaldos);
							}
							$a['success'] = true;
							$a['idCuentaContable'] = $rsCuenta['root'][0]['idCuentaContable'];
						}
					}
				} else {
					# 4to nivel tractor
					$cuentaPrev = substr($pscuenta, 0,-11);
					$selCtaSicaPrev = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${cuentaPrev}';";
					$rsCuentaPrev = fn_ejecuta_query_SICA($selCtaSicaPrev);
					$cuentaPrev = substr($pscuenta, 0,-6);
					if (sizeof($rsCuentaPrev['root']) > 0) {
						$ls_tractor = substr($pscuenta, 14,4);
						$selTractor = "SELECT tc.*, (SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca=tc.marca) AS nombreMarca ".
										"FROM caTractoresTbl tc ".
										"WHERE tc.tractor = ".$ls_tractor.";";
						$tractorRs = fn_ejecuta_query($selTractor);
						if (sizeof($tractorRs['root']) > 0) {
							$insertCuenta = " INSERT INTO conCatCuentasTbl (claveCompania, cuenta, nombre, afectable, tipo, naturaleza, ubicacion, reportar, manejaSegmentos, numeroSegmentos, divisa, codigoAgrupador, RFC, estatus, idUsuarioAct, fechaAct, ipAct, macAddressAct)".
											" VALUES ('4', '${cuentaPrev}', ".
											"'".$tractorRs['root'][0]['nombreMarca']."', ".
											"'0', '4', '1', ' ', '1', '0', '0', 'MXN', ' ', ' ', '1', '1', now(), '10.212.134.204', '10.212.134.204')";
							$rsCta = fn_ejecuta_query_SICA($insertCuenta);

							$selCuentaSica = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${cuentaPrev}';";
							$rsCuenta = fn_ejecuta_query_SICA($selCuentaSica);

							for ($j=1; $j <= 12; $j++) { 
								$rsConSaldos="INSERT INTO conSaldosTbl (claveCompania, tipo, calendario, periodo, idCuentaContable, idCentroCostos, saldoInicial, cargos, abonos, saldoFinal) ".
											"VALUES ('4', '1', year(now()), '".$j."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '0.00', '0.00', '0.00', '0.00')";
								$rsSal = fn_ejecuta_query_SICA($rsConSaldos);
							}
							$a['success'] = true;
							$a['idCuentaContable'] = $rsCuenta['root'][0]['idCuentaContable'];
						}
						# 5to nivel operador
						$selCtaSicaPrev = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${cuentaPrev}';";
						$rsCuentaPrev = fn_ejecuta_query_SICA($selCtaSicaPrev);

						if (sizeof($rsCuentaPrev['root']) > 0) {
							$selCuentaSica = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${pscuenta}';";
							$rsCuenta = fn_ejecuta_query_SICA($selCuentaSica);

							if (sizeof($rsCuenta['root']) == 0) {
								$selNombre = "SELECT concat(apellidoMaterno,' ', apellidoPaterno,' ', nombre) as nombre FROM caChoferesTbl where claveChofer = ".$claveChofer;
								$rsNombre = fn_ejecuta_query($selNombre);

								if (sizeof($rsNombre['root']) > 0) {
									$insertCuenta = " INSERT INTO conCatCuentasTbl (claveCompania, cuenta, nombre, afectable, tipo, naturaleza, ubicacion, reportar, manejaSegmentos, numeroSegmentos, divisa, codigoAgrupador, RFC, estatus, idUsuarioAct, fechaAct, ipAct, macAddressAct)".
													" VALUES ('4', '${pscuenta}', ".
													"'".$rsNombre['root'][0]['nombre']."', ".
													"'1', '1', '1', ' ', '1', '0', '0', 'MXN', ' ', ' ', '1', '1', now(), '10.212.134.204', '10.212.134.204')";
									$rsCta = fn_ejecuta_query_SICA($insertCuenta);

									$selCuentaSica = "SELECT * FROM conCatCuentasTbl WHERE claveCompania = 4 AND cuenta = '${pscuenta}';";
									$rsCuenta = fn_ejecuta_query_SICA($selCuentaSica);

									for ($j=1; $j <= 12; $j++) { 
										$rsConSaldos="INSERT INTO conSaldosTbl (claveCompania, tipo, calendario, periodo, idCuentaContable, idCentroCostos, saldoInicial, cargos, abonos, saldoFinal) ".
													"VALUES ('4', '1', year(now()), '".$j."', '".$rsCuenta['root'][0]['idCuentaContable']."', '0', '0.00', '0.00', '0.00', '0.00')";
										$rsSal = fn_ejecuta_query_SICA($rsConSaldos);
									}
									$a['success'] = true;
									$a['idCuentaContable'] = $rsCuenta['root'][0]['idCuentaContable'];
								}
							}
						}
					}
				}
			}
		}

		return $a;
	}
?>