<?php
	session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['trMantenimientoActionHdn']) {
        case 'getLogMantenimiento':
            getLogMantenimiento();
            break;
        case 'addLogMantenimiento':
            echo json_encode(addLogMantenimiento($_REQUEST['trMantenimientoIdTractorHdn'],
                                $_REQUEST['trMantenimientoIdViajeHdn'],
                                $_REQUEST['trMantenimientoPantallaTxt']));
            break;
        case 'getMantenimiento':
            getMantenimiento();
            break;
        case 'kilometrosRecorridos':
            kilometrosRecorridos();
            break;
        case 'getMantenimientoGroup':
            getMantenimientoGroup();
            break;
        case 'getUltimoMantenimiento':
            getUltimoMantenimiento();
            break;
        case 'getMantenimientoDetalle':
            getMantenimientoDetalle();
            break;
        case 'checkProxMantenimiento':
            echo json_encode(checkProxMantenimiento($_REQUEST['trMantenimientoIdTractorHdn'],
                                   $_REQUEST['trMantenimientoKmTabuladosHdn'],
                                   $_REQUEST['trMantenimientoIdViajeHdn'],
                                   $_REQUEST['trMantenimientoPantallaTxt']));
            break;
        case 'darMantenimiento':
            echo json_encode(darMantenimiento($_REQUEST['trMantenimientoIdTractorHdn'],
                $_REQUEST['trMantenimientoMovimientoHdn'],
                $_REQUEST['trControlMantenimientoOdometroTxt']));
            break;
        case 'addDetalleManto':
            echo json_encode(addDetalleManto($_REQUEST['trMantenimientoIdViajeHdn'],
                                $_REQUEST['trMantenimientoOdmHdn']));
            break;
		case 'getSqlAlerta':
			    getSqlAlerta();
			break;
		case 'addAlerta':
				echo json_encode(addAlerta($_REQUEST['manTractorAvisosTractorCmb'],
                                            $_REQUEST['manTractorAvisosKmsMantoTxt'],
                                            $_REQUEST['manTractorAvisosKmsAlertaTxt']));
			break;
        case 'getUpdAlerta':
                echo json_encode(getUpdAlerta($_REQUEST['manTractorAvisosTractorCmb'],
                                            $_REQUEST['manTractorAvisosKmsMantoTxt'],
                                            $_REQUEST['manTractorAvisosKmsAlertaTxt']));
            break;
        default:
            echo '';
    }

    function getLogMantenimiento(){
        $lsWhereStr = "WHERE ml.idTractor = tr.idTractor ".
                        "AND ml.idViajeTractor = vt.idViajeTractor ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoIdTractorHdn'], "ml.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoMovimientoHdn'], "ml.movimiento", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoIdViajeHdn'], "ml.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoPantallaTxt'], "ml.pantalla", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoUsuarioTxt'], "ml.idUsuario", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoIpTxt'], "ml.ip", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetMantenimiento = "SELECT ml.*, tr.tractor, ".
                                "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                    "WHERE pl.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, " .
                                "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                    "WHERE pl2.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, ".
                                "(SELECT us.usuario FROM segUsuariosTbl us WHERE us.idUsuario = ml.idUsuario) AS nombreUsuario ".
                                "FROM trLogMantenimientoTbl ml, caTractoresTbl tr, trViajesTractoresTbl vt ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetMantenimiento);

        echo json_encode($rs);
    }

    function addLogMantenimiento($idTractor, $idViaje, $pantalla){
    	$a = array();
        $e = array();
        $a['success'] = true;

        if($idTractor == ""){
            $e[] = array('id'=>'trMantenimientoIdTractorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($idViaje == ""){
            $e[] = array('id'=>'trMantenimientoIdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($pantalla == ""){
            $e[] = array('id'=>'trMantenimientoPantallaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
        	$sqlAddMantenimientoStr = "INSERT INTO trLogMantenimientoTbl ".
        							  "(idTractor, idViajeTractor, pantalla, idUsuario, ip, fechaEvento) ".
        							  "VALUES (".
        							  	$_REQUEST['trMantenimientoIdTractorHdn'].",".
        							  	$_REQUEST['trMantenimientoIdViajeHdn'].",".
        							  	"'".$_REQUEST['trMantenimientoPantallaTxt']."',".
        							  	$_SESSION['idUsuario'].",".
        							  	"'".$_SERVER['REMOTE_ADDR']."',".
                                        "'".date("Y-m-d")."')";

			$rs = fn_ejecuta_query($sqlAddMantenimientoStr);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlAddMantenimientoStr;
                $a['successMessage'] = getLogMantenimientoSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddMantenimientoStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        return $a;
    }

    function getMantenimiento(){


        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoIdTractorHdn'], "mt.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoMovimientoHdn'], "mt.movimiento", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoCentroDistHdn'], "mt.centroDistribucion", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoClaveMovimientoHdn'], "mt.claveMovimiento", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoFechaInicialTxt'], "mt.fechaEventoaInicial", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoCompaniaHdn'], "co.compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetMantenimientoStr = "SELECT mt.*,
(SELECT CONCAT(g.valor,'-',g.nombre) FROM caGeneralesTbl g WHERE g.tabla = 'trMantenimientoTractoresTbl' AND g.columna = 'claveMovimiento' AND g.valor = mt.claveMovimiento) AS nombreClaveMov ,
CASE
    WHEN mt.clavemovimiento='ME' THEN '0.00'
    WHEN mt.clavemovimiento='MP' AND mt.movimiento not in (select md.movimiento from caMantenimientoTractoresDetalleTbl md where mt.idTractor=md.idTractor) THEN '25000.00'
    WHEN mt.clavemovimiento='MP' AND mt.movimiento in (select md.movimiento from caMantenimientoTractoresDetalleTbl md where mt.idTractor=md.idTractor) THEN (SELECT (al.kilometrosMantenimiento-(md.odometro-mt.odometro))as kilometrosMantenimiento
FROM caMantenimientoTractoresTbl mt , camantenimientotractoresdetalletbl md, camantenimientoalertatbl al
WHERE mt.movimiento = (SELECT MAX(mt2.movimiento) FROM caMantenimientoTractoresTbl mt2 WHERE mt2.idTractor = mt.idTractor) 
AND mt.idTractor=md.idTractor
AND al.idTractor=md.idTractor
and mt.movimiento=md.movimiento
and md.odometro=(SELECT MAX(md1.odometro) from caMantenimientoTractoresDetalleTbl md1 where md1.idTractor=md.idTractor and md.movimiento=md1.movimiento)
AND mt.idTractor = '".$_REQUEST['trMantenimientoIdTractorHdn']."')
END  as kilometrosParaServicio
FROM caMantenimientoTractoresTbl mt ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetMantenimientoStr);

        echo json_encode($rs);
}

    function getMantenimientoGroup(){
        $sqlGetMtoNuevo = "SELECT ma.idTractor, tr.compania, tr.tractor, '0' as mantenimiento, tr.kilometrosservicio ".
                            "FROM camantenimientoalertatbl ma, catractorestbl tr ".
                            "WHERE ma.idTractor = tr.idTractor ".
                            //"AND ma.idTractor NOT IN(SELECT idTractor FROM camantenimientotractorestbl)".
                            "AND tr.compania = '".$_REQUEST['trMantenimientoCompaniaHdn']."';";

        $rs = fn_ejecuta_query($sqlGetMtoNuevo);
        echo json_encode($rs);
    }

    function getUltimoMantenimiento(){

														

        $sqlGetMantenimientoStr = "SELECT mt.*, ".
"(SELECT g.nombre FROM caGeneralesTbl g WHERE g.tabla = 'trMantenimientoTractoresTbl' AND g.columna = 'claveMovimiento' AND g.valor = mt.claveMovimiento) AS nombreClaveMov, ".
"(SELECT md.odometro from caMantenimientoTractoresDetalleTbl md where md.idTractor=mt.idTractor and mt.movimiento=md.movimiento and fechaEvento =(SELECT MAX(fechaEvento) from caMantenimientoTractoresDetalleTbl md where md.idTractor=mt.idTractor and mt.movimiento=md.movimiento)) as ultimoOdometroDetalle ".
"FROM caMantenimientoTractoresTbl mt ".
"WHERE mt.movimiento = (SELECT MAX(mt2.movimiento) FROM caMantenimientoTractoresTbl mt2 WHERE mt2.idTractor = mt.idTractor)  ".
"AND mt.idTractor ='".$_REQUEST['trMantenimientoIdTractorHdn']."' ";

    $rs = fn_ejecuta_query($sqlGetMantenimientoStr);


            $sqlValorMaximoCaptura = "SELECT ge.valor from cageneralestbl ge where ge.tabla='caMantenimiento' and ge.columna='capturaOdometro' ";

            $rssqlValorMaximoCaptura = fn_ejecuta_query($sqlValorMaximoCaptura);

            $rs['root'][0]['valorMaximo'] = $rssqlValorMaximoCaptura['root'][0]['valor'];

        
        echo json_encode($rs);
    }

    function getMantenimientoDetalle(){
        $lsWhereStr = "WHERE vt.idViajeTractor = md.idViajeTractor ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoIdTractorHdn'], "md.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trMantenimientoMovimientoHdn'], "md.movimiento", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        $sqlGetMantenimientoDetalle = "SELECT md.*, ".
                                        "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                            "WHERE pl.idPlaza = vt.idPlazaOrigen) AS descPlazaOrigen, " .
                                        "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                            "WHERE pl2.idPlaza = vt.idPlazaDestino) AS descPlazaDestino, vt.viaje ".
                                        "FROM caMantenimientoTractoresDetalleTbl md, trViajesTractoresTbl vt ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetMantenimientoDetalle);

        echo json_encode($rs);
    }

    function checkProxMantenimiento($idTractor, $kmTabulados, $idViaje, $pantalla){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idTractor == ""){
            $e[] = array('id'=>'idTractor','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($kmTabulados == ""){
            $e[] = array('id'=>'kmTabulados','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($idViaje == ""){
            $e[] = array('id'=>'idViajeTractor','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($pantalla == ""){
            $e[] = array('id'=>'pantalla','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            $sqlGetMaxKmTractorStr = "SELECT kilometrosServicio FROM caTractoresTbl ".
                                        "WHERE idTractor = ".$idTractor;

            $tractorData = fn_ejecuta_query($sqlGetMaxKmTractorStr);

            if(sizeof($tractorData['root']) > 0){
                $maxKm = $tractorData['root'][0]['kilometrosServicio'];

                //Sumar los registros del detalle del ultimo mantenimiento del tractor
                $sqlGetViajesMantenmientoStr = "SELECT md.odometro, ".
                                                "(SELECT dc.descripcionCentro FROM caDistribuidoresCentrosTbl dc ".
                                                    "WHERE dc.distribuidorCentro = mt.centroDistribucion) AS centroDistribucion ".
                                                "FROM caMantenimientoTractoresDetalleTbl md, caMantenimientoTractoresTbl mt ".
                                                "WHERE md.idTractor = mt.idTractor ".
                                                "AND md.movimiento = mt.movimiento ".
                                                "AND md.idTractor = ".$idTractor.
                                                " AND md.movimiento = (".
                                                    "SELECT MAX(mt2.movimiento) FROM caMantenimientoTractoresTbl mt2 ".
                                                    "WHERE mt2.idTractor = md.idTractor)";

                $rs = fn_ejecuta_query($sqlGetViajesMantenmientoStr);
                if(sizeof($rs['root']) >  0){
                    $kmTotales = 0;
                    foreach ($rs['root'] as $viaje) {
                        $kmTotales += intval($viaje['odometro']);
                    }
                    //Y se le suman los nuevos km del viaje
                    $kmTotales += intval($kmTabulados);

                    if($kmTotales >= $maxKm){
                        $a['root'] = array(array('message'=>getMantenimientoNecesario($rs['root'][0]['centroDistribucion'])));

                        addLogMantenimiento($idTractor, $idViaje, $pantalla);
                    }
                }
            }
        }
        $a['errors'] = $e;
        return $a;
    }

    function darMantenimiento($idTalon, $movimiento, $odometro){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idTalon == ""){
            $e[] = array('id'=>'trmantenimientoidtractorhdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($movimiento == ""){
            $e[] = array('id'=>'trMantenimientoMovimientoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($odometro == ""){
            $e[] = array('id'=>'trControlMantenimientoOdometroTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            if($movimiento != '0'){
                $sqlUpdMantenimientoStr = "UPDATE caMantenimientoTractoresTbl ".
                                            "SET claveMovimiento = 'ME' ".
                                            "WHERE idTractor = ".$idTalon." ".
                                            "AND movimiento = ".$movimiento;

                fn_ejecuta_query($sqlUpdMantenimientoStr);

            }
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $sqlAddNuevoMantenimientoStr = "INSERT INTO caMantenimientoTractoresTbl VALUES (".
                                                    $idTalon.",".
                                                    strval(intval($movimiento)+1).",".
                                                    //"'".$_SESSION['usuCompania']."',".
                                                    "'MP',".
                                                    "'".date("Y-m-d H:i:s")."',".
                                                    "'".$_REQUEST['trControlMantenimientoOdometroTxt']."')";

                fn_ejecuta_query($sqlAddNuevoMantenimientoStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['successMessage'] = getMantenimientoSuccessMsg();
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlAddNuevoMantenimientoStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlUpdMantenimientoStr;
            }
        }
        $a['errors'] = $e;
        return $a;
    }

    function addDetalleManto($idViaje,$odCapturado){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idViaje == ""){
            $e[] = array('id'=>'trMantenimientoIdViajeHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($odCapturado == ""){
            $e[] = array('id'=>'trMantenimientoOdmHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        //echo $a['success'];
        if($a['success']== true){


            if ($_SESSION['usuario']=='HARUMY') {


                $updOdometro="UPDATE trtalonesviajestbl set odometro=".$_REQUEST['trMantenimientoOdmHdn'].",semillero=".$_REQUEST['acompanante'].", maniobrasCarga='".$_REQUEST['manCarga']."-".$_REQUEST['manCargaAcom']."'
                , maniobrasDescarga='".$_REQUEST['manDescarga']."-".$_REQUEST['manDescargaAcom']."' where idViajeTractor=".$_REQUEST['trMantenimientoIdViajeHdn']." and odometro is null";
                fn_ejecuta_query($updOdometro);
            }else{
                $sqlAddDet = "INSERT INTO camantenimientotractoresdetalletbl (idTractor, movimiento, idViajeTractor, claveMovimiento, fechaEvento, odometro) ".
                "VALUES ((SELECT idTractor FROM trviajestractorestbl WHERE idViajeTractor = ".$_REQUEST['trMantenimientoIdViajeHdn']." ), ".
                "(SELECT ifnull(MAX(movimiento),1) as movimiento FROM camantenimientotractorestbl WHERE idTractor = (SELECT idTractor FROM trviajestractorestbl WHERE idViajeTractor = ".$_REQUEST['trMantenimientoIdViajeHdn'].")), ".
                "'".$_REQUEST['trMantenimientoIdViajeHdn']."', ".
                "'MP', ".
                "now(), ".
                "'".$_REQUEST['trMantenimientoOdmHdn']."');";

            fn_ejecuta_query($sqlAddDet);

            }

         /* $sqlAddLog = "INSERT INTO trlogmantenimientotbl (idViajeTractor, centroLibera, idTractor, movimiento,".
                         "pantalla, idUsuario, ip, fechaEvento, tipoDocumento) ".
                         "VALUES(".$_REQUEST['trMantenimientoIdViajeHdn'].",".
                         "'CDTOL',".
                         "(SELECT idTractor FROM trviajestractorestbl WHERE idViajeTractor = ".$_REQUEST['trMantenimientoIdViajeHdn']."),".
                         "(SELECT ifnull(MAX(movimiento),1) as movimiento FROM camantenimientotractorestbl WHERE idTractor = (SELECT idTractor FROM trviajestractorestbl WHERE idViajeTractor = ".$_REQUEST['trMantenimientoIdViajeHdn'].")),".
                         "'Liberacion de Viajes',".
                         "'3',".
                         "'10.1.2.122',".
                         "now() , ".
                         "'falta muy poco para el servicio')";

            fn_ejecuta_query($sqlAddLog); */         
        }
        $a['errors'] = $e;
        return $a;
    }
		function getSqlAlerta(){

			$lsWhereStr = "WHERE ma.idTractor = tr.idTractor ";
			if ($gb_error_filtro == 0){
					$ls_condicion = fn_construct($_REQUEST['manTractorAvisosCompaniaCmb'], "tr.compania", 1,1);
					$lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
				}
				if ($gb_error_filtro == 0){
					$ls_condicion = fn_construct($_REQUEST['manTractorAvisosTractorCmb'], "tr.idtractor", 0);
					$lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
				}


				$sqlAlerta = "SELECT concat(tr.compania,' - ',tr.Tractor) as tractor,ma.idTractor, ma.fechaEvento, ma.kilometrosMantenimiento, ma.kilometrosAvisos ".
											"FROM caMantenimientoAlertaTbl ma, catractorestbl tr ".$lsWhereStr;

				$rs = fn_ejecuta_query($sqlAlerta);

				echo json_encode($rs);

		}

		function addAlerta($numIdTractor,$kmsManto,$kmsAlerta){

			$a = array();
			$e = array();
			$a['success'] = true;

			if($numIdTractor == ""){
					$e[] = array('id'=>'manTractorAvisosTractorCmb','msg'=>getRequerido());
					$a['errorMessage'] = getErrorRequeridos();
					$a['success'] = false;
			}
			if($kmsManto == ""){
					$e[] = array('id'=>'manTractorAvisosKmsMantoTxt','msg'=>getRequerido());
					$a['errorMessage'] = getErrorRequeridos();
					$a['success'] = false;
			}
			if($kmsAlerta == ""){
					$e[] = array('id'=>'manTractorAvisosKmsAlertaTxt','msg'=>getRequerido());
					$a['errorMessage'] = getErrorRequeridos();
					$a['success'] = false;
			}
			if($a['success']== true){
				$sqlAddAlerta = "INSERT INTO camantenimientoalertatbl (idTractor,fechaEvento,kilometrosMantenimiento,kilometrosAvisos) ".
												"VALUES('".$_REQUEST['manTractorAvisosTractorCmb']."',NOW(),'".$_REQUEST['manTractorAvisosKmsMantoTxt']."','".$_REQUEST['manTractorAvisosKmsAlertaTxt']."');";

				fn_ejecuta_query($sqlAddAlerta);

			}
			$a['errors'] = $e;
			return $a;
		}

        function getUpdAlerta($numIdTractor,$kmsManto,$kmsAlerta){

            $a = array();
            $e = array();
            $a['success'] = true;

            if($numIdTractor == ""){
                    echo "1 actualiza";
                    $e[] = array('id'=>'manTractorAvisosTractorCmb','msg'=>getRequerido());
                    $a['errorMessage'] = getErrorRequeridos();
                    $a['success'] = false;
            }
            if($kmsManto == ""){
                    echo "2 actualiza";
                    $e[] = array('id'=>'manTractorAvisosKmsMantoTxt','msg'=>getRequerido());
                    $a['errorMessage'] = getErrorRequeridos();
                    $a['success'] = false;
            }
            if($kmsAlerta == ""){
                    echo "3 actualiza";
                    $e[] = array('id'=>'manTractorAvisosKmsAlertaTxt','msg'=>getRequerido());
                    $a['errorMessage'] = getErrorRequeridos();
                    $a['success'] = false;
            }
            if($a['success']== true){
                $sqlAddAlerta = "UPDATE  caMantenimientoAlertaTbl  ".
                                "SET kilometrosMantenimiento = '".$_REQUEST['manTractorAvisosKmsMantoTxt']."', ".
																"fechaEvento = NOW(), ".
                                "kilometrosAvisos = '".$_REQUEST['manTractorAvisosKmsAlertaTxt']."' ".
                                "WHERE idTractor = '".$_REQUEST['manTractorAvisosTractorCmb']."' ";

                fn_ejecuta_query($sqlAddAlerta);

            }
            $a['errors'] = $e;
            return $a;
        }

        function kilometrosRecorridos()
        {
               $sqlKilRec = "SELECT (md.odometro-mt.odometro)as kilometrosRecorridos, mt.movimiento
FROM caMantenimientoTractoresTbl mt , camantenimientotractoresdetalletbl md, camantenimientoalertatbl al
WHERE mt.idTractor=md.idTractor
AND al.idTractor=md.idTractor
and mt.movimiento=md.movimiento
and md.odometro=(SELECT MAX(md1.odometro) from caMantenimientoTractoresDetalleTbl md1 where md1.idTractor=md.idTractor and md.movimiento=md1.movimiento)
AND mt.idTractor =".$_REQUEST['trMantenimientoIdTractorHdn']."
and mt.movimiento=".$_REQUEST['trMantenimientoMovimientoHdn']." ";


           $rssqlKilRec=fn_ejecuta_query($sqlKilRec);

           echo json_encode($rssqlKilRec);

        }
?>
