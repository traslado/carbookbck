<?php
  
    require_once("../funciones/generales.php");
    //require_once("../funciones/etiquetasExportacion.php");


      switch($_REQUEST['getImpresionComodato']){
    	case 'unidadesGrd':
    		unidadesGrd();
    		break;
        case 'consultaVines':
            consultaVines();
            break;   
         case 'updateUnidades':
            updateUnidades();
            break;              
        default:
            echo '';
    }

    function unidadesGrd (){
    	$sqlUnidades = "SELECT  al.*,cd.distribuidorCentro, concat(cd.distribuidorCentro,' - ',cd.descripcionCentro) as distribuidor,".
                     " si.simboloUnidad, concat(si.simboloUnidad, ' - ', si.descripcion) as simbolo, al.descripcionUnidad as observaciones".
					 " FROM alunidadestbl al, cadistribuidorescentrostbl cd, casimbolosunidadestbl si,alultimodetalletbl ud".
					 " WHERE al.descripcionUnidad = 'cmDat_capturada'".
                     " AND ud.centroDistribucion = 'CMDAT'".   
                     " AND al.vin = ud.vin".                                       
					 " AND al.distribuidor = cd.distribuidorCentro".
					 " AND al.simbolounidad = si.simbolounidad";
		$rs =fn_ejecuta_query($sqlUnidades);
		echo json_encode($rs);
    }

    function consultaVines(){
        $sqlUnidades = "SELECT al.vin".
                     " FROM alunidadestbl al, cadistribuidorescentrostbl cd, casimbolosunidadestbl si,alultimodetalletbl ud".
                     " WHERE al.descripcionUnidad = 'cmDat_capturada'".
                     " AND ud.centroDistribucion = 'CMDAT'".   
                     " AND al.vin = ud.vin".                                       
                     " AND al.distribuidor = cd.distribuidorCentro".
                     " AND al.simbolounidad = si.simbolounidad";
        $rs =fn_ejecuta_query($sqlUnidades);
        //echo json_encode($rs);

        for ($i=0; $i <sizeof($rs['root']) ; $i++) { 
           $vines[$i] = $rs['root'][$i]['vin'];
        }
        //echo json_encode($vines);
        $vin1 =  implode( $vines);


        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");
        $vin=str_ireplace($buscar,$reemplazar,$vin1);

        $cadena = chunk_split($vin, 17,"|");
        echo json_encode($cadena);
    
    }

    function updateUnidades(){
        $updUnid = "UPDATE alunidadestbl SET descripcionUnidad='cmDat_impresa' WHERE descripcionUnidad = 'cmDat_capturada'";
        fn_ejecuta_query($updUnid);
    }
?>

    