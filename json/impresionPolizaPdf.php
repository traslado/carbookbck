<?php
	setlocale(LC_TIME, 'es_MX.utf8');
    date_default_timezone_set('America/Argentina/Buenos_Aires');
    session_start();

  	require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("trViajesTractores.php");
    //require_once("impresionPolizaComplemento.php");


    $folioTMP = $_REQUEST['trViajesTractoresFolioPolizaHdn'];
    if (!isset($_REQUEST['vistaPreviaHdn']) || $_REQUEST['vistaPreviaHdn'] != 1) {
        generaPolizaPDF();
    }

    function generaPolizaPDF() {
        $folioTMP = $_REQUEST['trViajesTractoresFolioPolizaHdn'];
        if($_REQUEST['trViajesTractoresIdViajeHdn'] != ''){

            $pdf = new FPDF('P', 'mm', array(216, 280));
            //DATOS GENERALES DEL VIAJE
            $data = getHistoricoViajesImpresion();

                    // ---------------------- CONTAR TALONES
                $sqlNumTalones = "SELECT COUNT(t1.folio) numfolio, ".
                                "CONCAT((SELECT t2.folio FROM trtalonesviajestbl t2 WHERE t2.idViajeTractor = t1.idViajeTractor AND t2.tipoTalon IN ('TN','TE') AND t2.claveMovimiento != 'TX' ORDER BY t2.folio ASC LIMIT 1),' AL ', ".
                                "(SELECT t3.folio FROM trtalonesviajestbl t3 WHERE t3.idViajeTractor = t1.idViajeTractor AND t3.tipoTalon IN ('TN','TE') AND t3.claveMovimiento != 'TX' ORDER BY t3.folio DESC LIMIT 1)) AS desCTalon ".
                                "FROM trtalonesviajestbl t1 ".
                                "WHERE t1.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                                "AND t1.claveMovimiento != 'TX';";                                         

                $rsNumTalones = fn_ejecuta_query($sqlNumTalones);

                if($rsNumTalones['root'][0]['numfolio'] <= '7'){
                    $numTalones = $data['root'][0]['foliosTalonesViaje'];
                }else{
                    $numTalones = $rsNumTalones['root'][0]['desCTalon'];
                }

            //DIESEL EXTRA
            $sqlGetDieselExtra = "SELECT gv.concepto, gv.importe, co.nombre ".
                                 "FROM trGastosViajeTractorTbl gv, caConceptosTbl co, caGeneralesTbl ge ".
                                 "WHERE gv.concepto = co.concepto ".
                                 "AND co.concepto=ge.valor ".
                                 "AND ge.tabla='conceptos' ".
                                 "AND ge.columna='9000' ". 
                                 "AND ge.idioma='+' ". 
                                 "AND gv.folio = '".$folioTMP."' ".
                                 "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                 "AND claveMovimiento !='XD' ";

            $dieselExtra = fn_ejecuta_query($sqlGetDieselExtra);
            
            $conceptoSueldo = '138';

            //DATOS EXTRA
            $sqlGetExtra = "SELECT gv.fechaEvento, ".
                            "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos, ".
                            "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7012' ) AS cuentaIva, ".
                            "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7013') AS cuentaCargoOperador,  ".
                             "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7025') AS cuentaOperadores,  ".
                            "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '7014') AS cuentaPagoEfectivo, ".
                            "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '2231') AS cuentaNoComprobados, ".
                            "(SELECT cc.cuentaContable FROM caConceptosCentrosTbl cc WHERE cc.centroDistribucion = gv.centroDistribucion AND cc.concepto = '141') AS cuentaPagoNeto, ".
                            "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$_SESSION['usuCompania']."' AND concepto = '".$conceptoSueldo."') AS tarifaSueldo, ".
                            "(SELECT concat(SUBSTRING(cc.cuentaContable, 1,13),'.', ".
                                           "LPAD((SELECT ta.tractor FROM catractorestbl ta ".
                                                 "WHERE idTractor = (SELECT vt.idTractor FROM trviajestractorestbl vt ".
                                                                    "WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn'].")),4,0),'.', ".
                                                                    "LPAD((SELECT vt.claveChofer FROM trviajestractorestbl vt ".
                                                                    " WHERE vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']."),5,0)) as cuentaContable ".
                            " FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$_SESSION['usuCompania']."' AND concepto = '".$conceptoSueldo."') AS cuentaSueldo, ".
                            "(SELECT (SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(ch.cuentaContable,4,0) FROM cachoferestbl ch WHERE ch.claveChofer = (SELECT vt1.claveChofer FROM trviajestractorestbl vt1 WHERE idViajeTractor = gv.idViajeTractor )))) FROM caConceptosCentrosTbl cc WHERE centroDistribucion = '".$data['root'][0]['centroDistribucionOrigen']."' AND concepto = '145') AS cuentaSCargo ".
                            "FROM trGastosViajeTractorTbl gv ".
                            "WHERE gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                            "AND gv.folio = '".$folioTMP."' ".
                            "AND gv.claveMovimiento = 'GP' LIMIT 1;";

            $extras = fn_ejecuta_query($sqlGetExtra);

            // VER SI TIENE POLIZA PUENTE EN CASO DE REIMPRESION

            $sqlCmpOperador = "SELECT concat(cc.cuentaContable,' ',co.nombre,'     ',gt.importe) as cmpOperador, gt.importe ".
                              "FROM caconceptoscentrostbl cc, caconceptostbl co, trgastosviajetractortbl gt ".
                              "WHERE co.concepto = gt.concepto ".
                              "AND cc.concepto = co.concepto ".
                              "AND cc.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND cc.concepto = '7025' ".
                              "AND gt.idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ";

            $rsCmpOperador = fn_ejecuta_query($sqlCmpOperador);

            if($_REQUEST['generaPolizaGtosHdn']  == 2)
            {
                if($rsCmpOperador['root'][0]['importe'] == null){

                  $pdf = generarPdfGastos($pdf, $data['root'][0], $dieselExtra['root'],$extras['root'][0],$folioTMP,$numTalones);
                }   else{
                    $pdf = generarPdfGastos($pdf, $data['root'][0], $dieselExtra['root'],$extras['root'][0],$folioTMP,$numTalones);

                  /* $pdf = generaPolizaPuente($pdf, $data['root'][0], $destinos['root'], $gastos['root'], $anticipos['root'], $dieselExtra['root'],$extras['root'][0],$folioTMP);*/
                }
                $pdf = generarPdfMacheteros($pdf, $_REQUEST['trViajesTractoresIdViajeHdn'], $folioTMP, $extras['root'][0]['fechaEvento']);
            }
            else{

                  if($rsCmpOperador['root'][0]['importe'] == null){

                  $pdf = generarPdfGastos($pdf, $data['root'][0], $dieselExtra['root'],$extras['root'][0],$folioTMP,$numTalones);

                  ////valida viaje acompañante

                   $sqlAcompana="SELECT * FROM trViajesTractoresTbl WHERE idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'];
                    $rsAcompa=fn_ejecuta_query($sqlAcompana);


                    if ($rsAcompa['root'][0]['idViajePadre'] == null )
                    {
                        $pdf = generarPdfSueldos($pdf, $data['root'][0], $dieselExtra['root'], $extras['root'][0],$folioTMP,$numTalones);
                    }

                } else{
                    $pdf = generarPdfGastos($pdf, $data['root'][0], $dieselExtra['root'],$extras['root'][0],$folioTMP,$numTalones);
                    

                    $sqlAcompana="SELECT * FROM trViajesTractoresTbl WHERE idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'];
                    $rsAcompa=fn_ejecuta_query($sqlAcompana);


                    if ($rsAcompa['root'][0]['idViajePadre'] == null)
                    {
                        $pdf = generarPdfSueldos($pdf, $data['root'][0], $dieselExtra['root'], $extras['root'][0],$folioTMP,$numTalones);
                    }
                    //$pdf = generarPdfSueldos($pdf, $data['root'][0], $dieselExtra['root'], $extras['root'][0],$folioTMP,$numTalones);
                  /*  $pdf = generaPolizaPuente($pdf, $data['root'][0], $destinos['root'], $gastos['root'], $anticipos['root'], $dieselExtra['root'],$extras['root'][0],$folioTMP);*/


                }
                $pdf = generarPdfMacheteros($pdf, $_REQUEST['trViajesTractoresIdViajeHdn'], $folioTMP, $extras['root'][0]['fechaEvento']);
            }

            if (isset($_REQUEST['vistaPreviaHdn']) && $_REQUEST['vistaPreviaHdn'] == 1) {
                $hoy = getdate();
                $fechaStr = date('YmdHis', $hoy[0]);
                $nombreArchivo = 'documentos/polizagastos_'.$_SESSION['idUsuario'].$fechaStr . '.pdf';
                $archivo = '../'.$nombreArchivo;
                $pdf->Output($archivo,'F');
                $pdf->Close();
                $a['success'] = (file_exists($archivo))?true:false;
                $a['archivo'] = "../carbookbck/".$nombreArchivo;
                return $a;
            } else {
                $pdf->Output("$folioTMP.pdf", 'I');
                $pdf->Close();
            }
        } else {
            echo "Error al obtener el ID del viaje";
        }
    }

    function generarPdfGastos($pdf, $data, $diesel, $extras, $folio,$numTal){
    	$border = 0;
    	$font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;
        $folioTMP= $folio;
        $numTalones=$numTal;

        //CONVERTIR EL MES AFECTACION A LETRA Y ESPAÑOL
        setlocale(LC_TIME, 'spanish');
        $mes = $_REQUEST['trViajesTractoresMesAfectacionHdn'];
        $mesAfecta = strtoupper(substr(strftime("%B",mktime(0, 0, 0, $mes)),0,3));

        //USUARIO QUIEN GENERO LA POLIZA
		$sqlGetElaboro = "SELECT DISTINCT us.idUsuario,us.nombre FROM trgastosviajetractortbl gt, segusuariostbl us ".
													"WHERE us.idUsuario = gt.usuario ".
													"AND gt.claveMovimiento = 'GP' ".
													"AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ";

	    $usuarioElaboracion = fn_ejecuta_query($sqlGetElaboro);


        //GASTOS DE POLIZA (GASTOS COMPROBACION)
        $sqlGetGastosPoliza = "SELECT cc.concepto, cc.cuentaContable, gv.iva, gv.subtotal, gv.importe, co.nombre, ".
                                "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                    "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                                "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioTMP."' ".
                                    "AND gv.claveMovimiento = 'GP' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".
                                "WHERE gv2.folio = '".$folioTMP."'   AND gv2.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']. " LIMIT 1) ".
                                "AND ge.tabla = 'comprobacionPoliza' ".
                                "GROUP BY ge.estatus ";

        $gastos1 = fn_ejecuta_query($sqlGetGastosPoliza);

        $gastos= $gastos1['root'];

        //ANTICIPOS
        $sqlGetAnticiposStr =   "SELECT concat(SUBSTR(cc.cuentaContable,1,14),(SELECT lpad(cuentaContable,4,0) FROM cachoferestbl WHERE claveChofer = (SELECT claveChofer FROM trviajestractorestbl WHERE idViajeTractor =".$_REQUEST['trViajesTractoresIdViajeHdn']." ))) as cuentaContable, ".
                                    "(SELECT nombre FROM caconceptostbl pl  WHERE  pl.concepto = cc.concepto) AS plaza, ".
                                     "(SELECT SUM(gv.importe) FROM trGastosViajeTractorTbl gv WHERE gv.concepto = cc.concepto AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." AND folio = '".$folioTMP."' AND gv.claveMovimiento != 'XP') AS importe ".
                                        /*"(SELECT GROUP_CONCAT(gv.folio SEPARATOR '-') FROM trGastosViajeTractorTbl gv ".
                                        "WHERE gv.claveMovimiento != 'GX' AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']."  and gv.concepto=7001 ".
                                        "GROUP BY gv.centroDistribucion) AS folio ".*/
                                        "FROM caConceptosCentrosTbl cc ".
                                        "WHERE cc.concepto IN ('7009','7016','7015','7023','7024') ".
                                        "AND cc.centroDistribucion NOT IN('CDTSA','CDSFE','CDLCL','CDSLP') ".
                                        "GROUP BY cc.concepto " ;

        $anticipos1 = fn_ejecuta_query($sqlGetAnticiposStr);

        $anticipos= $anticipos1['root'];

        // FOLIOS ANTICIPOS

        $foliosAnticiposGastos= "SELECT GROUP_CONCAT(gv.folio SEPARATOR '-') as folios FROM trGastosViajeTractorTbl gv ".
                                        "WHERE gv.claveMovimiento != 'GX' AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']."  and gv.concepto=7001 ";

        $rsfoliosAnticiposGastos = fn_ejecuta_query($foliosAnticiposGastos);
        //DESTINOS - TALONES
        $sqlGetDestinos = "SELECT tv.distribuidor, pl.plaza ".
                            "FROM trtalonesviajestbl tv, caPlazasTbl pl ".
                            "WHERE tv.idPlazaDestino = pl.idPlaza ".
                            "AND tv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                            "AND tv.claveMovimiento != 'TX' ";

        $destinos1 = fn_ejecuta_query($sqlGetDestinos);

        $destinos= $destinos1['root'];

        //CONCEPTO DE IVA ACREDITABLE

        $sqlIvaAcreditable = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='7012' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GP' ";

        $iva1 = fn_ejecuta_query($sqlIvaAcreditable);

        if($iva1['root'][0]['subtotal'] == null){
            $iva = 0.00;
        }
        else{

            $iva= $iva1['root'][0]['subtotal'];
        }

        $sqlCargoOperador = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='7013' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GP' ";

        $cargoOperador1 = fn_ejecuta_query($sqlCargoOperador);

        if($cargoOperador1['root'][0]['subtotal'] == null){
            $cargosOperador = 0.00;
        }
        else{
            $cargosOperador= $cargoOperador1['root'][0]['subtotal'];
        }
                $sqlpagoEfectivo = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='7014' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GP' ";

        $pagoEfectivo1 = fn_ejecuta_query($sqlpagoEfectivo);

        if($pagoEfectivo1['root'][0]['subtotal'] == null){
            $pagoEfectivo = 0.00;
        }
        else{
            $pagoEfectivo= $pagoEfectivo1['root'][0]['subtotal'];
        }


       $sqlOperadores = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='7025' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GP' ";

       $rssqlOperadores = fn_ejecuta_query($sqlOperadores);


        if($rssqlOperadores['root'][0]['subtotal'] == null){
            $operadores = 0.00;
        }
        else{
            $operadores= $rssqlOperadores['root'][0]['subtotal'];
        }

		$pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->AddPage();
        $pdf->SetFont($font,'',9);
        $pdf->SetAutoPageBreak(false);
        $fechaCompleta = date_create($extras['fechaEvento']);
        $fecha = date_format($fechaCompleta, "d/m/Y");
        $hora = date_format($fechaCompleta, "H:i:s");
        //HEADER
        $pdf->SetY(10+$offsetY);
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TRANSDRIZA S.A. DE C.V.", $border, 1, 'C');
        $pdf->SetX(73+$offsetX);
        $pdf->Cell(70,3,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TOLUCA EDO. DE MEXICO", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"R.F.C. TRA-891031-SXA", $border, 0, 'C');

        $pdf->SetX(175+$offsetX);
        if (!isset($_REQUEST['vistaPreviaHdn']) || $_REQUEST['vistaPreviaHdn'] != 1) {
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioTMP, $border, 1, 'L');
        } else {
            $pdf->SetTextColor(255,0,0);
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioTMP, $border, 1, 'L');
            $pdf->SetTextColor(0,0,0);
        }

        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") .$mesAfecta, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, $border, 1, 'L');

        $linea = "============================================================================================================";
        //DATOS
        $reimpresion = "";

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $reimpresion = " (REIMPRESION)";
        }

        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(78+$offsetX);
        $pdf->Cell(60,4,"POLIZA DE LIQUIDACION DE GASTOS".$reimpresion, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //DIST DESTINO HEADERS
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(130+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 0, 'C');
        $pdf->SetX(160+$offsetX);
        $pdf->Cell(20,4,"DIST.", $border, 0, 'C');
        $pdf->SetX(180+$offsetX);
        $pdf->Cell(30,4,"DESTINO", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

         $sqlTotalpagoVacio = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='147' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqlTotalpagoVacio = fn_ejecuta_query($sqlTotalpagoVacio);

        if($rsSqlTotalpagoVacio['root'][0]['subtotal'] == null){
            $pagoVacio = 0.00;
        }
        else{

            $pagoVacio= $rsSqlTotalpagoVacio['root'][0]['subtotal'];
        }

        $kmsVacios=$pagoVacio /3.15;
        $kmsTotales= $data['kilometrosComprobados'] + $kmsVacios;

        $sqlNumeroUnidades="SELECT sum(numeroUnidades) as numUnidades FROM trtalonesviajestbl
                            where claveMovimiento='TE'
                            and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'].";";
        $rsNumUnidades=fn_ejecuta_query($sqlNumeroUnidades);

        //DATOS GENERALES DEL VIAJE
		$pdf->SetX(5+$offsetX);        
        $pdf->Cell(100,4,"Talones  : ".$numTalones, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"No Autos :	".$rsNumUnidades['root'][0]['numUnidades'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(40,4,"Unidad   : ".$data['tractor']." (V-".str_pad($data['viaje'], 6, '0',STR_PAD_LEFT).")", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Rend.    :	".$data['rendimiento'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Cve. Ope.:	".$data['claveChofer'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Kms/Viaje:	".$kmsTotales, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);

        $litrosViaje1 = floatval(floatval($data['kilometrosSinUnidad']) + floatval($data['kilometrosComprobados'])) / floatval($data['rendimiento']);
        $litrosViaje = ceil($litrosViaje1);

        $pdf->Cell(30,4,"Lts/Apr  :	".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"Lts/Rec  :	".$litrosViaje, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30, 4,"Lts Menos --->	 0", $border, 1, 'L');

        //DIST DESTINO DATOS
        $maxTalones = 10;
        $xColumn = 110;

        $pdf->SetY(55+$offsetY);


        ////////////////////////////////////////////PENDIENTE DE REVISAR////////////////////////////////////

        for ($i=0; $i < $maxTalones; $i++) {
            $pdf->SetX($xColumn+$offsetX);
            if($i+1 <= sizeof($destinos)){
                $pdf->Cell(20,4,$destinos[$i]['distribuidor'], $border, 0, 'C');
                //EL ICONV SE USA PARA QUE IMPRIMA BIEN LOS ACENTOS
                $pdf->Cell(30,4,toUTF8($destinos[$i]['plaza']), $border, 1, 'C');
            } else {
                $pdf->Cell(20,4,"--------", $border, 0, 'C');
                $pdf->Cell(30,4,"--------------", $border, 1, 'C');
            }
            if(floatval($maxTalones) / floatval($i+1) == floatval(2)){
                $xColumn = 160;
                $pdf->SetY(55+$offsetY);
            }
        }
        ////////////////////////////////////////////PENDIENTE DE REVISAR////////////////////////////////////

        //CARGOS
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"C A R G O S", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"===========", $border, 1, 'L');

        $total = 0.00;

        foreach ($gastos as $gasto) {

        $cuentaContable = preg_replace("/(\*+)/",sprintf('%04d',$data['tractor']), $gasto['cuentaContable'], 1);
        $cuentaContable = preg_replace("/(\*+)/", sprintf('%05d',$data['claveChofer']), $cuentaContable, 1);

        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($cuentaContable,24," ")." ".
            substr($gasto['nombre'], 0, 11)." ".
            str_pad(number_format($gasto['subtotal'],2,'.',','), 9+(11-strlen(substr($gasto['nombre'], 0, 11))), " ", STR_PAD_LEFT),
            $border, 1, 'L');

        $total += floatval($gasto['subtotal']);

        }

        $pdf->SetX(75+$offsetX);
        $pdf->Cell(30,4,"==========", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(45,4,"Total de comp. gastos. ", $border, 0, 'L');
        $pdf->SetX(75.5+$offsetX);
        $pdf->Cell(29.5,4,str_pad(number_format($total, 2, '.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetY(185+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad($extras['cuentaIva'], 24, " ")." ".
            "IVA ACREDIT ".
            str_pad(number_format($iva,2,'.',','), 9, " ", STR_PAD_LEFT), $border, 1, 'L');


        //OBSERVACIONES
        $pdf->SetY(95+$offsetY);
        $pdf->SetX(140+$offsetX);
        $pdf->Cell(30,4,"OBSERVACIONES", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"==================================", $border, 1, 'C');

        $pdf->SetX(125+$offsetX);
        $pdf->MultiCell(55, 4, $extras['observacionGastos'], $border, 'L');

        //ANTICIPOS
        $pdf->SetY(170+$offsetY);
        $pdf->SetX(138+$offsetX);
        $pdf->Cell(30,4,"ABONOS", $border, 1, 'C');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(65,4,"================================", $border, 1, 'C');
        $pdf->SetX(132+$offsetX);
        $pdf->Cell(40,4,"Anticipo de Gastos", $border, 1, 'C');
        //LA SIGUIENTE CELDA VACIA ES SOLO PARA SALTAR EXACTAMENTE UNA LINEA
        $pdf->Cell(30,4,"", $border, 1, 'C');

        $totalAnticipo = 0.00;
        $importe = '0.00';
        $foliosAnticipos = "";

        // MONTO DE ANTICIPOS COMPROBADOS

            foreach ($anticipos as $anticipo) {
                if($anticipo['importe'] == ""){
                    $importe = '0.00';
                }else{
                    $importe = $anticipo['importe'];
                }

                $sqlSueldos="SELECT * FROM trgastosviajetractortbl
                                where concepto=139
                                and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'].";";
                $rsSueldos=fn_ejecuta_query($sqlSueldos);

                //echo "plaza".$anticipo['plaza'];

                if ($anticipo['plaza']=='TOLUCA ANT DE GASTOS') {
                    $importe=$importe + $rsSueldos['root'][0]['importe'];
                }

                $cuentaContable = preg_replace("/(\*+)/", $data['claveChofer'], $anticipo['cuentaContable'], 1);
                $cuentaContable = preg_replace("/(\*+)/", $data['tractor'], $cuentaContable, 1);

                $pdf->SetX(115+$offsetX);
                $pdf->Cell(80,4,
                str_pad($cuentaContable,18," ")." ".
                substr($anticipo['plaza'], 0, 10)." ".
                str_pad(number_format($importe,2,'.',','), 10+(10-strlen(substr($anticipo['plaza'], 0, 10))), " ", STR_PAD_LEFT),
                $border, 1, 'L');

                $totalAnticipo += floatval($importe);
                $foliosAnticipos = $anticipo['folio'];

            }
        
				$pdf->SetY(205+$offsetY);
				$pdf->SetX(05+$offsetX);
                $pdf->Cell(80,4,
                        str_pad($extras['cuentaOperadores'], 24, " ")." ".
            "OPERADORES ".
            str_pad(number_format($operadores, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

				$pdf->SetY(210+$offsetY);
				$pdf->SetX(115+$offsetX);

            $pdf->Cell(80,4,"TOTAL ANTIC. EN PATIOS =====> ".str_pad(number_format($totalAnticipo,2,'.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');
    

        //CONCEPTOS DE DIESEL EXTRA QUE NO SE SUMAN PARA LO DE DIESEL EXTRA
        $sumaDieselExtra = 0;

        foreach ($diesel as $d) {

                $sumaDieselExtra += floatval($d['importe']);
        }

        $pdf->SetY(80+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "DIESEL EXTRA: ".str_pad($sumaDieselExtra, 1, ' ', STR_PAD_LEFT), $border, 1, 'L');
	    $pdf->SetY(84+$offsetY);
        $pdf->SetX(110+$offsetX);
        $pdf->Cell(30, 4, "FOLIOS ANT:   ".$rsfoliosAnticiposGastos['root'][0]['folios'], $border, 1, 'L');

        //CARGO OPERADOR, PAGO EFECTIVO, COMPROBADO

        $pdf->SetY(215+$offsetY);
        $pdf->SetX(5+$offsetX);

        $sqlSueldos="SELECT * FROM trgastosviajetractortbl
                                where concepto=139
                                and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'].";";
                $rsSueldos=fn_ejecuta_query($sqlSueldos);

                //echo "plaza".$anticipo['plaza'];

                
                    $cargosOperador=$cargosOperador + $rsSueldos['root'][0]['importe'];
                
        $pdf->Cell(100,4,
            str_pad($extras['cuentaCargoOperador'], 24, " ")." ".
            "CARGO OPER ".
            str_pad(number_format($cargosOperador, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

				$pdf->SetY(215+$offsetY);
				$pdf->SetX(115+$offsetX);
        $pdf->Cell(80,4,
            str_pad($extras['cuentaPagoEfectivo'], 18, " ")." ".
            "PAGO EFECT ".
            str_pad(number_format($pagoEfectivo, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 1, 'L');

        $comprobado = $total + $iva;
				$pdf->SetY(219+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            str_pad("COMPROBADO", 25+10, " ", STR_PAD_LEFT)." ".
            str_pad(number_format($comprobado, 2, '.',','),10," ",STR_PAD_LEFT),
            $border, 0, 'L');

        //TOTALES
        $pdf->SetY(225+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,
            "TOTAL  CARGOS ".
            str_pad(number_format($comprobado + $cargosOperador, 2, '.',','),32," ",STR_PAD_LEFT),
            $border, 0, 'L');

                   
    	$pdf->SetY(225+$offsetY);
    	$pdf->SetX(115+$offsetX);
    	$pdf->Cell(80,4,
    			"TOTAL  ABONOS ".
                str_pad(number_format($totalAnticipo + $pagoEfectivo+$concOperadores, 2, '.',','),26," ",STR_PAD_LEFT),
    			$border, 1, 'L');


        //REIMPRESION
				$pdf->SetY(230+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $pdf->SetX(92+$offsetX);
            $pdf->Cell(40,4,"R E I M P R E S I O N", $border, 1, 'C');
        }


        //FIRMAS
        $pdf->SetY(245+$offsetY);
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 1, 'C');

        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"E L A B O R O",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"FIRMA DEL OPERADOR",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"R E V I S O",$border, 1, 'C');
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,$usuarioElaboracion['root'][0]['nombre'],$border, 0, 'C');
        $pdf->SetX(5+$offsetX);
        //$pdf->Cell(65,4,toUTF8($_SESSION['nombreUsr']),$border, 0, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,
            toUTF8($data['apellidoPaterno']." ".$data['apellidoMaterno']." ".$data['nombre']),
            $border, 1, 'C');
        $pdf->SetX(95+$offsetX);
        $pdf->Cell(65,4,"CORC-005                               P.D. No ".$folioTMP,$border, 0, 'L');


        return $pdf;
    }

    function generarPdfSueldos($pdf, $data, $diesel, $extras,$folio,$numTal){

        $folioTMP= $folio;
        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;
        $numTalones=$numTal;
        setlocale(LC_TIME, 'spanish');
        $mes = $_REQUEST['trViajesTractoresMesAfectacionHdn'];
        $mesAfecta = strtoupper(substr(strftime("%B",mktime(0, 0, 0, $mes)),0,3));

        $sqlGetElaboro = "SELECT DISTINCT us.idUsuario,us.nombre FROM trgastosviajetractortbl gt, segusuariostbl us ".
                                        "WHERE us.idUsuario = gt.usuario ".
                                        "AND gt.claveMovimiento = 'GS' ".
                                        "AND gt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ";

        $usuarioElaboracion = fn_ejecuta_query($sqlGetElaboro);

                //GASTOS DE SUELDOS
        $sqlGetSueldos = "SELECT cc.concepto, ".
                         "CASE ".  
                         "WHEN cc.concepto in ('2230','2231','9032') THEN CONCAT(SUBSTR(cc.cuentaContable,1,14),(SELECT LPAD(cco.cuentaContable,4,'0') ".
                         "FROM cachoferestbl cco where cco.claveChofer = (SELECT vt.claveChofer from trviajestractorestbl vt where ".
                         "vt.idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn']." ))) ELSE cc.cuentaContable END  as cuentaContable, ".
                         "gv.iva, gv.subtotal, gv.importe, gv.observaciones, co.nombre, ge.columna, ".
                            "(SELECT cc.importe FROM caConceptosCentrosTbl cc WHERE cc.concepto = gv.concepto ".
                                "AND cc.centroDistribucion = '".$_SESSION['usuCompania']."' ) AS importeConcepto, ".
                            "(SELECT ov.observaciones FROM trObservacionesViajeTbl ov WHERE ov.idViajeTractor = gv.idViajeTractor ".
                                "AND ov.tipo = 'G' AND ov.folio = gv.folio) AS observacionGastos ".
                            "FROM caConceptosTbl co, caConceptosCentrosTbl cc, caGeneralesTbl ge ".
                                "LEFT JOIN trGastosViajeTractorTbl gv ".
                                    "ON gv.concepto = ge.valor ".
                                    "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                    "AND gv.folio = '".$folioTMP."' ".
                                    "AND gv.claveMovimiento = 'GS' ".
                                "WHERE ge.valor = co.concepto ".
                                "AND cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = (SELECT gv2.centroDistribucion FROM trGastosViajeTractorTbl gv2 ".  
                                "WHERE gv2.folio = '".$folioTMP."'   AND gv2.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']. " LIMIT 1) ".
                                "AND ge.tabla = 'trPolizaGastosTbl' ".
                                "AND ge.columna = 'deducciones' ".
                                "ORDER BY ge.estatus ";

        $gastosSueldos1 = fn_ejecuta_query($sqlGetSueldos);

        $gastosSueldos = $gastosSueldos1['root'];

                        //DATOS DE DIESEL EXTRA
        $sqlGetDieselExtra = "SELECT gv.concepto, gv.importe, co.nombre ".
                             "FROM trGastosViajeTractorTbl gv, caConceptosTbl co ".
                             "WHERE gv.concepto = co.concepto ".
                             "AND gv.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                             "AND gv.concepto in (select valor from cageneralestbl ".
                                                "where tabla='conceptos' and columna='9000' ) ".
                            "AND gv.claveMovimiento = 'GD' ";

        $dieselExtra = fn_ejecuta_query($sqlGetDieselExtra);

         $sqlTotalPagoNormal = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='143' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqlTotalPagoNormal = fn_ejecuta_query($sqlTotalPagoNormal);

        if($rsSqlTotalPagoNormal['root'][0]['subtotal'] == null){
            $pagoNormal = 0.00;
        }
        else{

            $pagoNormal= $rsSqlTotalPagoNormal['root'][0]['subtotal'];
        }
        ///sueldoVacio
        $sqlTotalpagoVacio = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='147' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqlTotalpagoVacio = fn_ejecuta_query($sqlTotalpagoVacio);

        if($rsSqlTotalpagoVacio['root'][0]['subtotal'] == null){
            $pagoVacio = 0.00;
        }
        else{

            $pagoVacio= $rsSqlTotalpagoVacio['root'][0]['subtotal'];
        }
        /////
        $sqlScargo = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='145' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqlScargo = fn_ejecuta_query($sqlScargo);

        if($rsSqlScargo['root'][0]['subtotal'] == null){
            $sCargo = 0.00;
        }
        else{
            $sCargo= $rsSqlScargo['root'][0]['subtotal'];
        }

        $sqlPagoNeto = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='141' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqlPagoNeto = fn_ejecuta_query($sqlPagoNeto);

        if($rsSqlPagoNeto['root'][0]['subtotal'] == null){
            $pagoNeto = 0.00;
        }
        else{
            $pagoNeto= $rsSqlPagoNeto['root'][0]['subtotal'];
        }

        $sqlAntGasNoComprobados = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='144' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqlAntGasNoComprobados = fn_ejecuta_query($sqlAntGasNoComprobados);

        if($rsSqlAntGasNoComprobados['root'][0]['subtotal'] == null){
            $antGasNoComprobados = 0.00;
        }
        else{
            $antGasNoComprobados= $rsSqlAntGasNoComprobados['root'][0]['subtotal'];
        }

        $sqlTotalExtras = "SELECT  gv.subtotal, gv.observaciones, gv.importe as kilometros, cc.importe ".
                          "FROM  trgastosviajetractortbl gv, caconceptoscentrostbl cc  ".
                          "WHERE gv.concepto='140' ".
                          "AND gv.centroDistribucion=cc.centroDistribucion ".
                          "AND gv.concepto=cc.concepto ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqlTotalExtras = fn_ejecuta_query($sqlTotalExtras);

        if($rsSqlTotalExtras['root'][0]['subtotal'] == null){

            $totalExtra = 0.00;
        }
        else{

            $totalExtra= $rsSqlTotalExtras['root'][0]['subtotal'];
            $temp = explode(' ',$rsSqlTotalExtras['root'][0]['observaciones']);
            $autosExtra = $temp[0];
            $kmExtra= $rsSqlTotalExtras['root'][0]['kilometros'];
            $extraCosto= $rsSqlTotalExtras['root'][0]['importe'];

        }
         $SqltotalSueldos = "SELECT subtotal ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='138' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqltotalSueldos = fn_ejecuta_query($SqltotalSueldos);

        if($rsSqltotalSueldos['root'][0]['subtotal'] == null){
            $totalSueldos = 0.00;
        }
        else{
            $totalSueldos= $rsSqltotalSueldos['root'][0]['subtotal'];
        }

     


        $pdf->SetMargins(0.2, 0.2, 0.2,0.2);
        $pdf->AddPage();
        $pdf->SetFont($font,'',9);
        $pdf->SetAutoPageBreak(false);

        $fechaCompleta = date_create($extras['fechaEvento']);
        $fecha = date_format($fechaCompleta, "d/m/Y");
        $hora = date_format($fechaCompleta, "H:i:s");
       

        $linea = "============================================================================================================";

        //DEDUCCIONES
        //VAN AQUI POR LOGISTICA PARA MANEJAR LA LISTA DE DEDUCCIONES Y PERCEPCIONES AL MISMO TIEMPO
        $pdf->SetY(130+$offsetY);
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,6,"DEDUCCIONES", $border, 1, 'C');

        $totalDeducciones = 0;

        foreach ($gastosSueldos as $gasto) {
            //echo $gastosSueldos;

                $pdf->SetX(120+$offsetX);
                $pdf->Cell(90,4,
                    str_pad(substr($gasto['cuentaContable'], 0, 19),20," ")." ".
                    substr($gasto['nombre'], 0, 11)." ".
                    str_pad("$".number_format(floatval($gasto['importe'])*-1,2,'.',','), 11+(11-strlen(substr($gasto['nombre'], 0, 11))), " ", STR_PAD_LEFT),
                    $border, 1, 'L');

                $totalDeducciones += floatval($gasto['importe']) * -1;
            
        }

        $pdf->SetX(186+$offsetX);
        $pdf->Cell(30,4,"==========", $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(45,4,"TOTAL  DEDUCCIONES ", $border, 0, 'L');
        $pdf->SetX(185.5+$offsetX);
        $pdf->Cell(29.5,4,str_pad("$".number_format($totalDeducciones, 2, '.',','), 10, " ", STR_PAD_LEFT), $border, 1, 'L');

        //HEADER
        $pdf->SetY(10+$offsetY);
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TRANSDRIZA S.A. DE C.V.", $border, 1, 'C');
        $pdf->SetX(73+$offsetX);
        $pdf->Cell(70,3,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"TOLUCA EDO. DE MEXICO", $border, 1, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(45,3,"R.F.C. TRA-891031-SXA", $border, 0, 'C');
        $pdf->SetX(175+$offsetX);

        if (!isset($_REQUEST['vistaPreviaHdn']) || $_REQUEST['vistaPreviaHdn'] != 1) {
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") .$folioTMP, $border, 1, 'L');
        } else {
            $pdf->SetTextColor(255,0,0);
            $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") .$folioTMP, $border, 1, 'L');
            $pdf->SetTextColor(0,0,0);
        }
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") . $mesAfecta, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, $border, 1, 'L');
        $pdf->SetX(175+$offsetX);
        $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, $border, 1, 'L');

        //DATOS
        $reimpresion = "";

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $reimpresion = " (REIMPRESION)";
        }

        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,"POLIZA DE LIQUIDACION DE SUELDO DE OPERADORES".$reimpresion, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"TALONES-UNIDAD-OPERADOR".$reimpresion, $border, 0, 'L');
        $pdf->SetX(150+$offsetX);
        $pdf->Cell(30,4,"IMPORTE".$reimpresion, $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        $sqlNumeroUnidades="SELECT sum(numeroUnidades) as numUnidades FROM trtalonesviajestbl
                            where claveMovimiento='TE'
                            and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'].";";
        $rsNumUnidades=fn_ejecuta_query($sqlNumeroUnidades);

        //DATOS GENERALES DEL VIAJE
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"Talones  : ".$numTalones, $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(30,4,"No Autos : ".$rsNumUnidades['root'][0]['numUnidades'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"Unidad   : ".$data['tractor']." (V-".str_pad($data['viaje'], 6, '0',STR_PAD_LEFT).")", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(40,4,"Cve. Ope.: ".$data['claveChofer'], $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,toUTF8($data['apellidoPaterno']." ".$data['apellidoMaterno']." ".$data['nombre']), $border, 1, 'L');

        $pdf->SetY(75+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"====================================================", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,5,"DIESEL EXTRA", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"====================================================", $border, 1, 'L');


        if(sizeof($dieselExtra['root'])>=1){
            $numeroY = 90;
            $numeroVeces =  sizeof($dieselExtra['root']);

            for ($i=0; $i < $numeroVeces; $i++) {
                $pdf->SetY($numeroY+$offsetY);
                $pdf->SetX(5+$offsetX);
                $pdf->Cell(100,4,$dieselExtra['root'][$i]['concepto']." ".$dieselExtra['root'][$i]['nombre']." ".$dieselExtra['root'][$i]['importe']." lts.", $border, 1, 'L');
                $numeroY = $numeroY + 3;
            }
        }

        if ($data['claveChofer']=='10904' || $data['claveChofer']=='10977' || $data['claveChofer']=='10979' || $data['claveChofer']=='30003' || $data['claveChofer']=='70010' || $data['claveChofer']=='70042' || $data['claveChofer']=='70043' || $data['claveChofer']=='70013' || $data['claveChofer']=='30001' || $data['claveChofer']=='10978' || $data['claveChofer']=='30002' || $data['claveChofer']=='10984' || $data['claveChofer']=='70014') {
            
            // code...
        }
        else{
            //ADICIONALES
            $kmsRecorridos = floatval($data['kilometrosTabulados'] * 2);
            $kmAdicionales = floatval($data['kilometrosComprobados']) - floatval($kmsRecorridos);

            $pdf->SetY(55+$offsetY);
            $pdf->SetX(120+$offsetX);
            //$pdf->Cell(80,4,"Kms/Viaje      ".str_pad(number_format($kmsRecorridos,2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
            $pdf->Cell(80,4,"Kms/Viaje      ".str_pad(number_format("00.00",2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
            $pdf->SetX(120+$offsetX);
            //$pdf->Cell(80,4,"Kms/Adicionales".str_pad(number_format($kmAdicionales,2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
                    $pdf->Cell(80,4,"Kms/Adicionales".str_pad(number_format("00.00",2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
            $pdf->SetX(120+$offsetX);

            $pdf->Cell(80,4,"Kms/Totales    ".str_pad(number_format($data['kilometrosComprobados'],2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(80,4,"Pago x Km/viaje".str_pad("$".number_format($extras['tarifaSueldo'],2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');

            ////PAGO NORMAL
            $pdf->SetY(69+$offsetY);
            $pdf->SetX(120+$offsetX);        
            $pdf->Cell(80,4,"------------", $border, 1, 'R');
            $pdf->SetY(71+$offsetY);
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(80,4,"Total Pago Normal".str_pad("$".number_format($pagoNormal,2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
            $pdf->SetY(71+$offsetY);
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(80,4,"Total Pago Normal".str_pad("$".number_format($pagoNormal,2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
            $pdf->SetY(71+$offsetY);
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(80,4,"Total Pago Normal".str_pad("$".number_format($pagoNormal,2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
            

            ///PAGO VACIO

            $totalVacios= $pagoVacio/3.15;

            $pdf->SetY(76+$offsetY);
            $pdf->SetX(120+$offsetX);   
            $pdf->Cell(80,4,"Kms/Vacios    ".str_pad(number_format($totalVacios,2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');

            $pdf->SetY(80+$offsetY);
            $pdf->SetX(120+$offsetX);   
            $pdf->Cell(80,4,"Pago x Km/Vacio                    $3.15".str_pad(number_format("",2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');

            $pdf->SetY(82+$offsetY);
            $pdf->SetX(120+$offsetX);        
            $pdf->Cell(80,4,"------------", $border, 1, 'R');

            $pdf->SetY(84+$offsetY);
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(80,4,"Total Pago Vacio ".str_pad("$".number_format($pagoVacio,2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
            $pdf->SetY(84+$offsetY);
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(80,4,"Total Pago Vacio ".str_pad("$".number_format($pagoVacio,2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');

            $pdf->SetY(84+$offsetY);
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(80,4,"Total Pago Vacio ".str_pad("$".number_format($pagoVacio,2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');

            $sqlSueldosGrat="SELECT * FROM trgastosviajetractortbl
                            where concepto=136
                            and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'].";";
            $rsSueldosGrat=fn_ejecuta_query($sqlSueldosGrat);  

            $sueldoGrat=$rsSueldosGrat['root'][0]['importe'];

            if (sizeof($rsSueldosGrat['root'])!='0') {
                $totalSueldos1=$totalSueldos+$sueldoGrat;

            $pdf->SetY(115+$offsetY);
            $pdf->SetX(5+$offsetX);
            $pdf->Cell(100,4,"====================================================", $border, 0, 'L');
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(80,4,"Total Gratificaciones".str_pad("$".number_format($sueldoGrat, 2,'.',','), 20, ' ', STR_PAD_LEFT), $border, 1, 'L');  

            }else{
                $totalSueldos1=$totalSueldos;
            }


        }

        
        

        

        //OTROS
        /* $totalOtros = floatval($otrosKm) * floatval($otrosCosto);

        $pdf->SetY(85+$offsetY);
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Otros/Kms        ".str_pad(number_format($otrosKm, 2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Costo x Otros/Kms".str_pad("$".number_format($otrosCosto, 2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"============", $border, 1, 'R');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Total Otros      ".str_pad("$".number_format($totalOtros, 2,'.',','), 23, ' ', STR_PAD_LEFT), $border, 1, 'L');
        */
        //EXTRAS

       /* $pdf->SetY(91+$offsetY);
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Autos Extras   ".str_pad(number_format($autosExtra, 0,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Costo x Km     ".str_pad("$".number_format($extraCosto, 2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Kms Recs.      ".str_pad(number_format($kmExtra, 2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"------------", $border, 1, 'R');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Total Extras   ".str_pad("$".number_format($totalExtra, 2,'.',','), 25, ' ', STR_PAD_LEFT), $border, 1, 'L');*/

        //TOTAL DE SUELDOS

        

     
        $pdf->SetY(123+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"====================================================", $border, 0, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4,"Total de sueldos".str_pad("$".number_format($totalSueldos1, 2,'.',','), 24, ' ', STR_PAD_LEFT), $border, 1, 'L');


         $SqltotalSueldos2 = "SELECT subtotal, cuentaContable ".
                          "FROM  trgastosviajetractortbl  ".
                          "WHERE concepto='143' ".
                          "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                          "AND folio='".$folioTMP."' ".
                          "AND claveMovimiento='GS' ";

        $rsSqltotalSueldos2 = fn_ejecuta_query($SqltotalSueldos2);

        if($rsSqltotalSueldos2['root'][0]['subtotal'] == null){
            $totalSueldosLleno = 0.00;
        }
        else{
            $totalSueldosLleno= $rsSqltotalSueldos2['root'][0]['subtotal'];
        }

        //INGRESOS
        $pdf->SetY(125+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,6,"INGRESOS", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);


        if ($data['claveChofer']=='10904' || $data['claveChofer']=='10977' || $data['claveChofer']=='10979' || $data['claveChofer']=='30003' || $data['claveChofer']=='70010' || $data['claveChofer']=='70042' || $data['claveChofer']=='70043'|| $data['claveChofer']=='70013' || $data['claveChofer']=='30001' || $data['claveChofer']=='10978' || $data['claveChofer']=='30002' || $data['claveChofer']=='10984' || $data['claveChofer']=='70014') {

            $SqltotalSueldos1 = "SELECT subtotal, cuentaContable ".
                              "FROM  trgastosviajetractortbl  ".
                              "WHERE concepto='160' ".
                              "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                              "AND folio='".$folioTMP."' ".
                              "AND claveMovimiento='GS' ";

            $rsSqltotalSueldos1 = fn_ejecuta_query($SqltotalSueldos1);

            if($rsSqltotalSueldos1['root'][0]['subtotal'] == null){
                $totalSueldosVacio = 0.00;
            }
            else{
                $totalSueldosVacio= $rsSqltotalSueldos1['root'][0]['subtotal'];
            }

             //SUELDO VACIO
            $pdf->SetY(140+$offsetY);
            $pdf->SetX(5+$offsetX);
            $pdf->Cell(100,4, $rsSqltotalSueldos1['root'][0]['cuentaContable'], $border, 1, 'L');
            $pdf->SetY(140+$offsetY);
            $pdf->SetX(55+$offsetX);
            $pdf->Cell(100,4, "SUELDO POR CUOTA  $".number_format($totalSueldosVacio, 2,'.',','), $border, 1, 'L');

        }else{

            $pdf->Cell(100,4, $rsSqltotalSueldos2['root'][0]['cuentaContable'], $border, 1, 'L');
            $pdf->SetX(68+$offsetX);
            $pdf->Cell(100,4, "SUELDO LLENO  $".number_format($totalSueldosLleno, 2,'.',','), $border, 1, 'L');

               /////SUELDO VACIO

             $SqltotalSueldos1 = "SELECT subtotal, cuentaContable ".
                              "FROM  trgastosviajetractortbl  ".
                              "WHERE concepto='147' ".
                              "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                              "AND folio='".$folioTMP."' ".
                              "AND claveMovimiento='GS' ";

            $rsSqltotalSueldos1 = fn_ejecuta_query($SqltotalSueldos1);

            if($rsSqltotalSueldos1['root'][0]['subtotal'] == null){
                $totalSueldosVacio = 0.00;
            }
            else{
                $totalSueldosVacio= $rsSqltotalSueldos1['root'][0]['subtotal'];
            }

             //SUELDO VACIO
            $pdf->SetY(140+$offsetY);
            $pdf->SetX(5+$offsetX);
            $pdf->Cell(100,4, $rsSqltotalSueldos1['root'][0]['cuentaContable'], $border, 1, 'L');
            $pdf->SetX(68+$offsetX);
            $pdf->Cell(100,4, "SUELDO VACIO  $".number_format($totalSueldosVacio, 2,'.',','), $border, 1, 'L');


    ///////////////// APLICA SOLO PARA LOS TRACTORES 36 Y 37
            if ($data['tractor']=='36' || $data['tractor']=='37') {
                

                /////  /////BONO CDMX
                 $SqltotalSueldos1 = "SELECT subtotal, cuentaContable ".
                                  "FROM  trgastosviajetractortbl  ".
                                  "WHERE concepto='149' ".
                                  "AND idViajeTractor = '".$_REQUEST['trViajesTractoresIdViajeHdn']."' ".
                                  "AND folio='".$folioTMP."' ".
                                  "AND claveMovimiento='GS' ";

                $rsSqltotalSueldos2 = fn_ejecuta_query($SqltotalSueldos1);

                if($rsSqltotalSueldos2['root'][0]['subtotal'] == null){
                    $totalBono = 0.00;
                }
                else{
                    $totalBono= $rsSqltotalSueldos2['root'][0]['subtotal'];
                }

                $pdf->SetY(150+$offsetY);
                $pdf->SetX(5+$offsetX);
                $pdf->Cell(100,4, $rsSqltotalSueldos2['root'][0]['cuentaContable'], $border, 1, 'L');
                $pdf->SetX(68+$offsetX);
                $pdf->Cell(100,4, "BONO LOCAL CDMX $".number_format($totalBono, 2,'.',','), $border, 1, 'L');

            }


    //////////////////       

        }




        
        

        /////sueldo grat

       $sqlSueldosGrat="SELECT * FROM trgastosviajetractortbl
                            where concepto=136
                            and claveMovimiento='GS'
                            and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'].";";
            $rsSueldosGrat=fn_ejecuta_query($sqlSueldosGrat);  

        $sueldoGrat=$rsSueldosGrat['root'][0]['importe'];
        //echo json_encode($rsSueldosGrat)  ;

          $sqlT2="SELECT * from autorizacionesespecialestbl
                        where concepto=136
                        and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn']."
                        and descripcion ='T2'";
                $rsT2=fn_ejecuta_query($sqlT2);


                $sqlT2gen="SELECT * from cageneralestbl
                            where tabla='autorizacionesespecialestbl'
                            and valor='T2'";
                $rsGT2=fn_ejecuta_query($sqlT2gen);

                $importeT2=sizeof($rsT2['root'])*$rsGT2['root'][0]['estatus'];

                $sqlT3="SELECT * from autorizacionesespecialestbl
                        where concepto=136
                        and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn']."
                        and descripcion ='T3';";
                $rsT3=fn_ejecuta_query($sqlT3);


                $sqlT3gen="SELECT * from cageneralestbl
                            where tabla='autorizacionesespecialestbl'
                            and valor='T3'";
                $rsGT3=fn_ejecuta_query($sqlT3gen);

                $importeT3=sizeof($rsT3['root'])*$rsGT3['root'][0]['estatus'];

                $importeGrat=$importeT2+$importeT3;

                $cuentaGrat=$rsSueldosGrat['root'][0]['cuentaContable'];

        if (sizeof($rsT2['root']) != '0') {
                $pdf->SetX(5+$offsetX);
                $pdf->Cell(80,5, $cuentaGrat, $border, 1, 'L');

                $pdf->SetX(55+$offsetX);
                $pdf->Cell(80,5, "ESTANCIA 1ER DIA  $".number_format($importeT2, 2,'.',','), $border, 1, 'L');

                $pdf->SetX(55+$offsetX);
                $pdf->Cell(80,5, "ESTANCIA > DIA 1  $".number_format($importeT3, 2,'.',','), $border, 1, 'L');
        }

        
        




        //GARANTIA - ESTO ES FIJO PORQUE YA NO SE USA
       /* $pdf->SetY(150+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"Costo dias Garantia: $0.00", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"No, Dias              0", $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,"SUELDO DE ", $border, 1, 'L');*/

        //FECHAS
        $pdf->SetY(170+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,5,"FECHAS", $border, 1, 'C');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4,"====================================================", $border, 1, 'L');
        $pdf->SetY(190+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,5,"====================================================", $border, 1, 'L');

        $pdf->SetY(200+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(60,4,
            str_pad($extras['cuentaNoComprobados'], 19, ' ').
            " ANTIC. GASTOS NO COMP.".
            str_pad("$".number_format($antGasNoComprobados, 2,'.',','), 10, ' ', STR_PAD_LEFT), $border, 0, 'L');
        $pdf->SetX(120+$offsetX);

        $sqlSueldos="SELECT * FROM trgastosviajetractortbl
                                where concepto=136
                                and idViajeTractor=".$_REQUEST['trViajesTractoresIdViajeHdn'].";";
                $rsSueldos=fn_ejecuta_query($sqlSueldos);
                 

                //echo "plaza".$anticipo['plaza'];

                
                    //$sCargo=$sCargo + $rsSueldos['root'][0]['importe'];
        $pdf->Cell(60,4,
            str_pad($extras['cuentaSCargo'], 19, ' ').
            " S/CARGO. ".
            str_pad("$".number_format($sCargo, 2,'.',','), 15, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(120+$offsetX);
            $pagoNeto=$pagoNeto ;
        $pdf->Cell(60,4,
            str_pad($extras['cuentaPagoNeto'], 19, ' ').
            " PAGO NETO. ".
            str_pad("$".number_format($pagoNeto, 2,'.',','), 13, ' ', STR_PAD_LEFT), $border, 1, 'L');

        //TOTALES
        $totalIngresos = $totalSueldos + $antGasNoComprobados + $sueldoGrat;
        $totalEgresos = $sCargo + $pagoNeto + $totalDeducciones ;// + $antGasNoComprobados;      
        //echo $totalEgresos;

        $pdf->SetY(210+$offsetY);
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(100,4, str_pad("TOTAL DE INGRESOS ", 38, ' ', STR_PAD_LEFT).
            str_pad("$".number_format($totalIngresos, 2,'.',','), 14, ' ', STR_PAD_LEFT), $border, 0, 'L');
        $pdf->SetX(120+$offsetX);
        $pdf->Cell(80,4, "TOTAL DE EGRESOS ".
            str_pad("$".number_format($totalEgresos, 2,'.',','), 27, ' ', STR_PAD_LEFT), $border, 1, 'L');
        $pdf->SetX(5+$offsetX);
        $pdf->Cell(210,4,$linea, $border, 1, 'L');

        //TEXTO Y REIMPRESION
        $pdf->SetX(5+$offsetX);
        $pdf->MultiCell(200, 4,
            "EL SUELDO INCLUYE LA CUOTA POR KILOMETRO RECORRIDO MAS LA PARTE PROPORCIONAL DEL SEPTIMO DIA Y DE LOS DIAS FESTIVOS"
            , $border, 'L');

        if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){
            $pdf->SetX(92+$offsetX);
            $pdf->Cell(40,4,"R E I M P R E S I O N", $border, 1, 'C');
        }

        //FIRMAS
        $pdf->SetY(240+$offsetY);
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"===========================",$border, 1, 'C');

        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,"E L A B O R O",$border, 0, 'C');
        $pdf->SetX(85+$offsetX);
        $pdf->Cell(55,4,"FIRMA DEL OPERADOR",$border, 0, 'C');
        $pdf->SetX(155+$offsetX);
        $pdf->Cell(55,4,"R E V I S O",$border, 1, 'C');
        $pdf->SetX(10+$offsetX);
        $pdf->Cell(55,4,$usuarioElaboracion['root'][0]['nombre'],$border, 0, 'C');
        $pdf->SetX(5+$offsetX);
        //$pdf->Cell(65,4,toUTF8($_SESSION['nombreUsr']),$border, 0, 'C');
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(65,4,
            toUTF8($data['apellidoPaterno']." ".$data['apellidoMaterno']." ".$data['nombre']),
            $border, 1, 'C');
        $pdf->SetX(95+$offsetX);
        $pdf->Cell(65,4,"CORC-005                               P.D. No ".$folioTMP,$border, 0, 'L');


        return $pdf;
    }

    function generarPdfMacheteros($pdf, $idViajeTractorPrm, $folioPrm, $fechaEventoPrm){
        $selStr = "SELECT * FROM trComplementosConceptoTbl ".
                 " WHERE idViajeTractor = ${idViajeTractorPrm}".
                 " AND concepto = '6002'".
                 // " AND centroDistribucion = '".$_SESSION['usuCompania']."'".
                 " AND folio ='" .$folioPrm."'".
                 " ORDER BY secuencia";
        $Rst = fn_ejecuta_query($selStr);
        if($Rst['records'] > 0){
            $pdf->SetMargins(10, 15, 10);
            $pdf->SetAutoPageBreak(true, 15);
            $pdf->AddPage();

            $mes = $_REQUEST['trViajesTractoresMesAfectacionHdn'];//////////CHK
            setlocale(LC_TIME , 'es_MX.UTF-8');
            $mesAfecta = strtoupper(substr(strftime("%B",mktime(0, 0, 0, $mes)),0,3));
            $fecha = date('d/m/Y', strtotime($fechaEventoPrm));
            $hora = date('H:i:s', strtotime($fechaEventoPrm));

            $pdf->SetFont('Courier', '', 9);
            $pdf->Cell(29);
            $pdf->Cell(140,4,"TRANSDRIZA S.A. DE C.V.", 0, 0, 'C');
            $pdf->Ln(4);
            $pdf->SetFont('Courier', '', 9);
            $pdf->Cell(14);
            $pdf->Cell(170,4,"VIALIDAD TOLUCA-ATLACOMULCO No. 1850", 0, 0, 'C');
            $pdf->Ln(4);
            $pdf->SetFont('Courier', '', 9);
            $pdf->Cell(198,4,"TOLUCA EDO. DE MEXICO", 0, 0, 'C');
            $pdf->Ln(4);
            $pdf->Cell(198,4,"R.F.C. TRA-891031-SXA", 0, 0, 'C');
            $pdf->Ln(6);

            $pdf->Cell(163);
            if (!isset($_REQUEST['vistaPreviaHdn']) || $_REQUEST['vistaPreviaHdn'] != 1) {
                $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioPrm, 0, 0, 'L');
            } else {
                $pdf->SetTextColor(255,0,0);
                $pdf->Cell(35,4,str_pad("FOLIO", 7, " ") . $folioPrm, 0, 0, 'L');
                $pdf->SetTextColor(0,0,0);
            }
            $pdf->Ln(4);
            $pdf->Cell(163);
            $pdf->Cell(35,4,str_pad("AFECTA", 7, " ") .$mesAfecta, 0, 0, 'L');
            $pdf->Ln(4);
            $pdf->Cell(163);
            $pdf->Cell(35,4,str_pad("FECHA", 7, " ") . $fecha, 0, 0, 'L');
            $pdf->Ln(4);
            $pdf->Cell(163);
            $pdf->Cell(35,4,str_pad("HORA", 7, " ") . $hora, 0, 0, 'L');
            $pdf->Ln(8);

            // $linea = "======================================================================================================";
            // $pdf->Cell(198,4,$linea, 0, 0, 'L');
            // $pdf->Ln(8);

            $reimpresion = "";
            if($_REQUEST['trViajesTractoresReimpresionHdn'] == "1"){//////////CHK
                $reimpresion = " (REIMPRESION)";
            }

            $rowAux = array();
            $idxAux = $Rst['records']-1;
            $rowAux = explode('|', $Rst['root'][$idxAux]['parametros']);

            $pdf->SetFont('Courier', 'B', 10);
            $pdf->Cell(198,5,"NOTAS DE REMISION POR MANIOBRAS".$reimpresion,0,0,'C');
            $pdf->Ln(12);
            $pdf->SetFont('Courier','B',10);
            $pdf->Cell(5);$pdf->Cell(22,5,"No. NOMINA",0,0,'L');
            $pdf->SetFont('Courier','',10);
            $pdf->Cell(130,5,": ".$rowAux[0],0,0,'L');
            $pdf->Ln(5);
            $pdf->SetFont('Courier','B',10);
            $pdf->Cell(5);$pdf->Cell(22,5,"CHOFER",0,0,'L');
            $pdf->SetFont('Courier','',10);
            $pdf->Cell(130,5,": ".utf8_decode($rowAux[1]),0,0,'L');
            $pdf->Ln(5);
            $pdf->SetFont('Courier','B',10);
            $pdf->Cell(5);$pdf->Cell(22,5,"UNIDAD",0,0,'L');
            $pdf->SetFont('Courier','',10);
            $pdf->Cell(130,5,": ".$rowAux[2],0,0,'L');
            $pdf->Ln(10);

            $pdf->SetFont('Courier','B',9);
            $pdf->SetFillColor(224,224,222);
            $pdf->SetDrawColor(130,130,130);
            $pdf->SetLineWidth(0.3);
            $pdf->Cell(7);
            $pdf->Cell(30,6, "CANTIDAD" ,1,0,'C',true);
            $pdf->Cell(116,6,'CONCEPTO DE MANIOBRA',1,0,'C',true);
            $pdf->Cell(36,6, "IMPORTE" ,1,0,'C',true);
            $pdf->Ln(6.22);
            $pdf->SetFont('Courier','',9);
            $pdf->SetFillColor(222, 244, 222);
            $pdf->SetDrawColor(130,130,130);
            $pdf->SetLineWidth(0.02);

            $fill = false;
            foreach ($Rst['root'] as $i => $row) {
                $rowAux = explode('|', $row['parametros']);
                if ($i == $idxAux) {
                    break;
                }
                $border = ($pdf->GetY() <= 92)?0:'TB';
                $pdf->Cell(7,4);
                $pdf->Cell(30,4, $rowAux[0], $border, 0, 'C', $fill);
                $pdf->Cell(116,4, $rowAux[1], $border, 0, 'L', $fill);
                $pdf->Cell(36,4, number_format($rowAux[2],2,'.',','), $border, 0, 'R', $fill);
                $pdf->Ln(4);
                $fill = !$fill;
            }
            $selStr1 = "SELECT * FROM trgastosviajetractortbl ".
                 " WHERE idViajeTractor = ${idViajeTractorPrm}".
                 " AND concepto = '6010'".
                 // " AND centroDistribucion = '".$_SESSION['usuCompania']."'".
                 " AND folio ='" .$folioPrm."';";                 
            $Rst1 = fn_ejecuta_query($selStr1);
            if (sizeof($rowAux) > 0) {
                $pdf->Ln(2);
                $pdf->SetFont('Courier','B',8.5);
                $pdf->Cell(123);$pdf->Cell(30,4,"MACHETEROS:",0,0,'L');
                $pdf->Cell(36,4,"$ ".number_format($rowAux[3],2,'.',','),0,0,'R');
                $pdf->Ln(4);
                $pdf->Cell(123);$pdf->Cell(30,4,"TAXIS EXTRAORDINARIOS:",0,0,'L');
                $pdf->Cell(36,4,"$ ".number_format($Rst1['root'][0]['subTotal'],2,'.',','),0,0,'R');
                $pdf->Ln(4);
                $pdf->Cell(123);$pdf->Cell(30,4,"TOTAL:",0,0,'L');
                $pdf->Cell(36,4,"$ ".number_format($rowAux[3] + $Rst1['root'][0]['subTotal'],2,'.',','),0,0,'R');
                $pdf->SetFont('Courier','',8.5);
            }
        }

        return $pdf;
    }

?>
