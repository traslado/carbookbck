                <?php
	/************************************************************************
    * Autor: Alfonso César Martínez Fuertes
    * Fecha: 15-Enero-2014
    * Tablas afectadas: 
    * Descripción: Programa para dar mantenimiento a gastos de foraneos
    *************************************************************************/

    session_start();
	//$_SESSION['modulo'] = "trGastosViajeTractor";
    //SESION DE PRUEBA    
    //$_SESSION['usuario'] = 1;
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("catGenerales.php");

    $_REQUEST = trasformUppercase($_REQUEST);
	
	switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
	
    switch($_REQUEST['trGastosViajeTractorActionHdn']){
    	case 'getGastosViajeTractor':
    		getGastosViajeTractor();
    		break;
        case 'getGastosComplementos':
            getGastosComplementos();
            break;
        case 'getGastosViajePatio':
            getGastosViajePatio();
            break;
        case 'getCalculoGastos':
            getCalculoGastos();
            break;
        case 'getCalculoGastosComplementos':
            getCalculoGastosComplementos();
            break;   
        case 'getCalculoGastosViajeVacio':
            getCalculoGastosViajeVacio();
            break;
        case 'addGastosViajeTractor':
            echo json_encode(addGastosViajeTractor($_REQUEST['trGastosIdViajeTractorHdn'],
                                        $_REQUEST['trGastosViajeCompaniaHdn'],
                                        $_REQUEST['trGastosViajeConceptoHdn'],
                                        $_REQUEST['trGastosViajeTractorImporteTxt'],
                                        $_REQUEST['trGastosViajeTractorObservacionesTxa'],
                                        $_REQUEST['trGastosViajeTractorClaveMovimientoHdn'],
                                        $_REQUEST['trGastosViajeTipoDoctoHdn'], 
                                        $_REQUEST['trGastosViajeKmTabuladosHdn']));
            break;
        case 'cancelarGastos':
            cancelarGastos();
            break;
        case 'getNumFoliosPorViaje':
            getNumFoliosPorViaje();
            break;
        case 'addGastoViajeVacio': 
            echo json_encode(addGastoViajeVacio());
            break;
        case 'addComplementoViajeVacio':
            addComplementoViajeVacio();
            break;
        case 'getTotalComplementosViajeTractor':
            getTotalComplementosViajeTractor($_REQUEST['trGastosIdViajeTractorHdn']);
            break;
        case 'calculoAnticipoSueldo':
            calculoAnticipoSueldo();
            break;
        case 'insertaSueldo':
            insertaSueldo();
            break;
        default:
            echo '';
    }
    function getGastosViajeTractor(){
        $lsWhereStr = "WHERE gv.idViajeTractor = vt.idViajeTractor ".
                      "AND vt.idTractor = tr.idTractor ".
                      "AND ch.claveChofer = vt.claveChofer ".
                      "AND co.concepto = gv.concepto ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorIdViajeTractorHdn'], "gv.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorConceptoHdn'], "gv.concepto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorCentroDistHdn'], "gv.centroDistribucion", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorFolioTxt'], "gv.folio", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorFechaTxt'], "gv.fechaEvento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorCtaContableTxt'], "gv.cuentaContable", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorMesAfectacionTxt'], "gv.mesAfectacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorImporteTxt'], "gv.importe", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorObservacionesTxa'], "gv.observaciones", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorClaveMovimientoHdn'], "gv.claveMovimiento", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorViajeHdn'], "vt.viaje", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeIdTractorHdn'], "vt.idTractor", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetGastosViajesTractoresStr = "SELECT gv.*, gv.claveMovimiento AS claveMovGasto, vt.*, tr.tractor, co.nombre AS nombreConcepto, ".
                                          "tr.compania AS ciaTractor, ch.claveChofer, ch.nombre, ch.apellidoPaterno, ch.apellidoMaterno, ".
                                          "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS descCiaTractor, ".
                                          "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = vt.idPlazaOrigen) AS nombrePlazaOrigen, ".
                                          "(SELECT pl2.plaza FROM caPlazasTbl pl2 WHERE pl2.idPlaza = vt.idPlazaDestino) AS nombrePlazaDestino ".
                                          "FROM trGastosViajeTractorTbl gv, trViajesTractoresTbl vt, ".
                                          "caTractoresTbl tr, caChoferesTbl ch, caConceptosTbl co ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetGastosViajesTractoresStr);

        //echo json_encode($sqlGetGastosViajesTractoresStr);

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) { 
            $rs['root'][$nInt]['descDistribuidor'] = $rs['root'][$nInt]['distribuidor']." - ".$rs['root'][$nInt]['descripcionCentro'];
            $rs['root'][$nInt]['descCompania'] = $rs['root'][$nInt]['companiaRemitente']." - ".$rs['root'][$nInt]['descripcionCompania'];
            $rs['root'][$nInt]['descTractorCia'] = $rs['root'][$nInt]['ciaTractor']." - ".$rs['root'][$nInt]['descCiaTractor'];
            $rs['root'][$nInt]['nombreChofer'] = $rs['root'][$nInt]['claveChofer']." - ".
                                                 $rs['root'][$nInt]['nombre']." ".
                                                 $rs['root'][$nInt]['apellidoPaterno']." ".
                                                 $rs['root'][$nInt]['apellidoMaterno'];
            $rs['root'][$nInt]['fechaEvento'] = date_format(date_create($rs['root'][$nInt]['fechaEvento']), "Y-m-d");

            $rs['root'][$nInt]['descConcepto'] = $rs['root'][$nInt]['concepto']." - ".$rs['root'][$nInt]['nombreConcepto'];
        }

        echo json_encode($rs);	
    }

    function getGastosViajePatio(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorIdViajeTractorHdn'], "gv.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetGastosViajePatioStr = "SELECT gv.*, ".
                                     "(SELECT dc.descripcionCentro FROM caDistribuidoresCentrosTbl dc ".
                                        "WHERE dc.distribuidorCentro = gv.centroDistribucion) AS nombreCd ".
                                     "FROM trGastosViajeTractorTbl gv ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetGastosViajePatioStr);
        
        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) { 
            if (!isset($patios[$rs['root'][$nInt]['centroDistribucion']])) {
                $patios[$rs['root'][$nInt]['centroDistribucion']] = 0;
            }

            $patios[$rs['root'][$nInt]['centroDistribucion']] += $rs['root'][$nInt]['importe'];
            $nombres[$rs['root'][$nInt]['centroDistribucion']] = $rs['root'][$nInt]['nombreCd'];
        }

        $data = array();
        $data['success'] = true;
        $data['records'] = sizeof($patios);

        for ($nInt=0; $nInt < sizeof($patios); $nInt++) { 
            $data['root'][$nInt] = array('patio' => key($patios), 'nombre' => $nombres[key($patios)], 'importe'=>$patios[key($patios)]);   
        }

        echo json_encode($data);
    }
    function getCalculoGastosComplementos(){
        $a = array();
        $e = array();
        $a['success'] = true;
        //$lsWhereStr = "";
/*
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorIdViajeTractorHdn'], "tg.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorConceptoHdn'], "tg.concepto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorClaveMovimientoHdn'], "tg.claveMovimiento", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }*/
        /*if($_SESSION['usuCompania'] == 'CDTOL'){
            $base = 'BaseCDTOL';
        }else{
            $base = 'BaseCDSAL';
        }*/
        $base = 'BaseCDTOL';
        $sqlGetCalculosGastosComplementosStr = "SELECT valor as id, (SELECT nombre FROM caconceptostbl cc ". 
                                               "WHERE cc.concepto = cg.valor) AS concepto, ".
                                               "(SELECT sum(tg.importe) FROM trgastosviajetractortbl tg ".
                                               "WHERE tg.concepto = cg.valor AND tg.idviajeTractor = ".
                                               $_REQUEST['trGastosViajeTractorIdViajeTractorHdn'].
                                               " AND tg.claveMovimiento != 'GX' GROUP BY tg.concepto) AS cantidad, ".
                                               "(SELECT tg.observaciones FROM trgastosviajetractortbl tg ".
                                               "WHERE tg.concepto = cg.valor AND tg.idviajeTractor = ".
                                               $_REQUEST['trGastosViajeTractorIdViajeTractorHdn'].
                                               " AND tg.claveMovimiento != 'GX' GROUP BY tg.concepto) AS observaciones, ".
                                               "(SELECT tv.claveMovimiento FROM trViajesTractoresTbl tv WHERE tv.idViajeTractor = ".
                                               $_REQUEST['trGastosViajeTractorIdViajeTractorHdn'].") AS claveMovViaje ".
                                               "FROM cageneralestbl cg where cg.tabla = 'trGastosViajetractortbl' ". 
                                               "AND cg.columna = '".$base."';";

        $rs = fn_ejecuta_query($sqlGetCalculosGastosComplementosStr);

        $conceptoArr = array();
        $cantidadArr = array();
        $nombreArr = array();
        $observacionesArr = array();
        $claveMovViajeArr = array();

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $conceptoArr[$rs['root'][$iInt]['id']] = $rs['root'][$iInt]['id'];
            $cantidadArr[$rs['root'][$iInt]['id']] = $rs['root'][$iInt]['cantidad'];
            $nombreArr[$rs['root'][$iInt]['id']] = $rs['root'][$iInt]['concepto'];
            $observacionesArr[$rs['root'][$iInt]['id']] = $rs['root'][$iInt]['observaciones'];
            $claveMovViajeArr[$rs['root'][$iInt]['id']] = $rs['root'][$iInt]['claveMovViaje'];
        }
        if($base == 'BaseCDTOL'){
            $conceptoTaxi = '6010';
            $conceptoAlimentos = '2342';
            $conceptoMacheteros ='6002';
            ///
            $taxisextra ='9021';
            $gratificaciones ='9022';
            $guias ='9023';
            $reparaciones ='9024';
            $diesextra ='9025';
            $efectivo ='9026';
            $maniobras ='9027';
            $desvioruta ='9028';
        }else{
            $conceptoTaxi = '6004';
            $conceptoAlimentos = '2343';
            $conceptoMacheteros ='6003';
        }
        if($claveMovViajeArr[$conceptoAlimentos] != 'VC' && $claveMovViajeArr[$conceptoAlimentos] != 'VA'){
             $a['root'] = array(array('id'=>'2315','concepto'=>$nombreArr['2315'],'cantidad' => $cantidadArr['2315'],'cantidadOriginal' => $cantidadArr['2315'],'observaciones'=>$observacionesArr['2315'],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                    array('id'=>$conceptoTaxi,'concepto'=>$nombreArr[$conceptoTaxi],'cantidad' => $cantidadArr[$conceptoTaxi],'cantidadOriginal' => $cantidadArr[$conceptoTaxi],'observaciones'=>$observacionesArr[$conceptoTaxi],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                    array('id'=>$conceptoAlimentos,'concepto'=>$nombreArr[$conceptoAlimentos],'cantidad' => $cantidadArr[$conceptoAlimentos],'cantidadOriginal' => $cantidadArr[$conceptoAlimentos],'observaciones'=>$observacionesArr[$conceptoAlimentos],'observaciones'=>$observacionesArr[$conceptoAlimentos],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                    /////
                    array('id'=>$taxisextra,'concepto'=>$nombreArr[$taxisextra],'cantidad' => $cantidadArr[$taxisextra],'cantidadOriginal' => $cantidadArr[$taxisextra],'observaciones'=>$observacionesArr[$taxisextra],'observaciones'=>$observacionesArr[$taxisextra],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                     array('id'=>$gratificaciones,'concepto'=>$nombreArr[$gratificaciones],'cantidad' => $cantidadArr[$gratificaciones],'cantidadOriginal' => $cantidadArr[$gratificaciones],'observaciones'=>$observacionesArr[$gratificaciones],'observaciones'=>$observacionesArr[$gratificaciones],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                      array('id'=>$guias,'concepto'=>$nombreArr[$guias],'cantidad' => $cantidadArr[$guias],'cantidadOriginal' => $cantidadArr[$guias],'observaciones'=>$observacionesArr[$guias],'observaciones'=>$observacionesArr[$guias],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                       array('id'=>$reparaciones,'concepto'=>$nombreArr[$reparaciones],'cantidad' => $cantidadArr[$reparaciones],'cantidadOriginal' => $cantidadArr[$reparaciones],'observaciones'=>$observacionesArr[$reparaciones],'observaciones'=>$observacionesArr[$reparaciones],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                        array('id'=>$diesextra,'concepto'=>$nombreArr[$diesextra],'cantidad' => $cantidadArr[$diesextra],'cantidadOriginal' => $cantidadArr[$diesextra],'observaciones'=>$observacionesArr[$diesextra],'observaciones'=>$observacionesArr[$diesextra],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                         array('id'=>$efectivo,'concepto'=>$nombreArr[$efectivo],'cantidad' => $cantidadArr[$efectivo],'cantidadOriginal' => $cantidadArr[$efectivo],'observaciones'=>$observacionesArr[$efectivo],'observaciones'=>$observacionesArr[$efectivo],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                          array('id'=>$maniobras,'concepto'=>$nombreArr[$maniobras],'cantidad' => $cantidadArr[$maniobras],'cantidadOriginal' => $cantidadArr[$maniobras],'observaciones'=>$observacionesArr[$maniobras],'observaciones'=>$observacionesArr[$maniobras],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                           array('id'=>$desvioruta,'concepto'=>$nombreArr[$desvioruta],'cantidad' => $cantidadArr[$desvioruta],'cantidadOriginal' => $cantidadArr[$desvioruta],'observaciones'=>$observacionesArr[$desvioruta],'observaciones'=>$observacionesArr[$desvioruta],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                   /// 
                    array('id'=>'2333','concepto'=>$nombreArr['2333'],'cantidad' =>$cantidadArr['2333'],'cantidadOriginal' => $cantidadArr['2333'],'observaciones'=>$observacionesArr['2333'],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                    array('id'=>$conceptoMacheteros,'concepto'=>$nombreArr[$conceptoMacheteros],'cantidad' => $cantidadArr[$conceptoMacheteros],'cantidadOriginal' => $cantidadArr[$conceptoMacheteros],'observaciones'=>$observacionesArr[$conceptoMacheteros],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                    array('id'=>'7006','concepto'=>$nombreArr['7006'], 'cantidad'=>$cantidadArr['7006'],'cantidadOriginal'=>$cantidadArr['7006'],'observaciones'=>$observacionesArr['7006'],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']));
        } else{    
        switch ($claveMovViajeArr[$conceptoAlimentos]) {
            case 'VA':
                $a['root'] = array(array('id'=>$conceptoAlimentos,'concepto'=>$nombreArr[$conceptoAlimentos],'cantidad' => $cantidadArr[$conceptoAlimentos],'cantidadOriginal' => $cantidadArr[$conceptoAlimentos],'observaciones'=>$observacionesArr[$conceptoAlimentos],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                array('id'=>'7006','concepto'=>$nombreArr['7006'], 'cantidad'=>$cantidadArr['7006'],'cantidadOriginal'=>$cantidadArr['7006'],'observaciones'=>$observacionesArr['7006'],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']));
                break;
            case 'VC':
                $a['root'] = array(array('id'=>'2315','concepto'=>$nombreArr['2315'],'cantidad' => $cantidadArr['2315'],'cantidadOriginal' => $cantidadArr['2315'],'observaciones'=>$observacionesArr['2315'],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                array('id'=>$conceptoAlimentos,'concepto'=>$nombreArr[$conceptoAlimentos],'cantidad' => $cantidadArr[$conceptoAlimentos],'cantidadOriginal' => $cantidadArr[$conceptoAlimentos],'observaciones'=>$observacionesArr[$conceptoAlimentos],'observaciones'=>$observacionesArr[$conceptoAlimentos],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                array('id'=>'2333','concepto'=>$nombreArr['2333'],'cantidad' =>$cantidadArr['2333'],'cantidadOriginal' => $cantidadArr['2333'],'observaciones'=>$observacionesArr['2333'],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                array('id'=>'2333','concepto'=>$nombreArr['2333'],'cantidad' =>$cantidadArr['2333'],'cantidadOriginal' => $cantidadArr['2333'],'observaciones'=>$observacionesArr['2333'],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']),
                array('id'=>'7006','concepto'=>$nombreArr['7006'], 'cantidad'=>$cantidadArr['7006'],'cantidadOriginal'=>$cantidadArr['7006'],'observaciones'=>$observacionesArr['7006'],'idViajeTractor'=>$_REQUEST['trGastosViajeTractorIdViajeTractorHdn']));
                break;
        }
    }

        /*foreach ($a['root'] as $key => $value) {
            if ($value['cantidad']==null) {
                $a['root'][$key]['cantidad']=0;
                # code...
            }
        
        }*/

        echo json_encode($a);
    }

    function getGastosComplementos(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap484IdViajeTractorHdn'] == ""){
            $e[] = array('id'=>'trap484IdViajeTractorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $esAcompanante =  isset($_REQUEST['trap484esAcompananteHdn']) && $_REQUEST['trap484esAcompananteHdn'] != "";

        if($_SESSION['usuCompania'] == 'CDTOL'){
            $base = 'BaseCDTOL';
            $filter = array('2315', '2342', '2333', '7006');
        } else{
            $base = 'BaseCDSAL';
            $filter = array('2315', '2343', '2333', '7006');
        }

        $sqlGetGastos = "SELECT gv.idViajeTractor, cg.valor as id, ".
                       "(SELECT nombre c FROM caconceptostbl c where c.concepto = cg.valor) as concepto, ".
                       "SUM(gv.importe) as cantidad ".
                        "FROM caGeneralesTbl cg ".
                        "LEFT JOIN trGastosViajeTractorTbl gv ".
                        "ON cg.valor = gv.concepto ".
                        "AND gv.idViajeTractor = ".$_REQUEST['trap484IdViajeTractorHdn'].
                        " AND gv.claveMovimiento !='GX' WHERE tabla = 'trGastosViajeTractorTbl' ".
                        "AND columna = '".$base."' ".
                        "GROUP BY id, concepto;";

        $rs = fn_ejecuta_query($sqlGetGastos);
        /*Conceptos Viaje Vacio
                * 2315 - Combustible
                * 2343/2342 - Alimentos *Acompañante
                * 2333 - Peaje
                * 7006 - Otros *Acompañante
                * 7001 - Total *Acompañante
        */
        $rsTmp = array();
        $suma = 0;
        for ($i = 0; $i < count($rs['root']) ; $i++) {
            if(!$esAcompanante){
                if(in_array($rs['root'][$i]['id'], $filter)){
                    $rsTmp['root'][] = $rs['root'][$i];
                    $suma += floatval($rs['root'][$i]['cantidad']);
                } 
            } else{ //ACOMPAÑANTE
                if(in_array($rs['root'][$i]['id'], $filter) && $rs['root'][$i]['id'] != '2315' && 
                $rs['root'][$i]['id'] != '2333') {
                    $rsTmp['root'][] = $rs['root'][$i];
                    $suma += floatval($rs['root'][$i]['cantidad']);
                } 
            }
        }
        //Le Asigna el Total Tanto a cantidad como a complemento para que el FRONT lo "Agarre" Bien
        $rsTmp['root'][] = array('id' => '7001', 'concepto' => 'TOTAL', 'cantidad' => ceil($suma/100)*100, 'complemento' => ceil($suma/100)*100);
        $rsTmp['records'] = count($rsTmp['root']);
        echo json_encode($rsTmp);
    }

    function getCalculoGastos(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $lsWhereStr = "WHERE tr.idTractor = vt.idTractor";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trap484IdViajeHdn'], "vt.idViajeTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $getDatosViajeStr = "SELECT vt.kilometrosTabulados,vt.numeroUnidades,vt.numeroRepartos, tr.rendimiento, ".
                            "(SELECT SUM(tv.numeroUnidades) FROM trtalonesviajestbl tv WHERE tv.idViajeTractor = ".$_REQUEST['trap484IdViajeHdn'].
                            " AND tv.distribuidor NOT LIKE 'CD%') AS numUnidades ".
                            "FROM trViajesTractoresTbl vt, caTractoresTbl tr ".$lsWhereStr;

        $rs = fn_ejecuta_query($getDatosViajeStr);

        //Si cambian el dato de kilometraje a mano
        if (isset($_REQUEST['trap484KilometrosTxt']) && $_REQUEST['trap484KilometrosTxt'] >= 0) {
            $kmTabulados = $_REQUEST['trap484KilometrosTxt'];
        } else {

            $kmTabulados = $rs['root'][0]['kilometrosTabulados'];
        }

        $rendimiento = $rs['root'][0]['rendimiento'];
        $numUnidades = $rs['root'][0]['numeroUnidades'];
        $repartos = $rs['root'][0]['numeroRepartos'];
        $macheteros = 0;
        

       $sqlGetConceptosStr = "SELECT cc.concepto, co.nombre, cc.importe, cc.cuentaContable ".
                                "FROM caConceptosCentrosTbl cc, caconceptostbl co ".
                                "WHERE cc.concepto = co.concepto ".
                                "AND cc.centroDistribucion = '".$_REQUEST['trap484CentroDistribucionChoferHdn']."'";

        $rs = fn_ejecuta_query($sqlGetConceptosStr);      
        $conceptosArr = array();
        $nombreArr = array();
        $cuentaContableArr = array();

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $conceptosArr[$rs['root'][$iInt]['concepto']] = $rs['root'][$iInt]['importe'];
            $nombreArr[$rs['root'][$iInt]['concepto']] = $rs['root'][$iInt]['nombre'];
            $cuentaContableArr[$rs['root'][$iInt]['concepto']] = $rs['root'][$iInt]['cuentaContable'];
        }
        if ($a['success'] == true) {
            //CALCULO DE LITROS
            $kmDf = "SELECT idEstado FROM trtalonesviajestbl tt, cadireccionestbl cd, cacoloniastbl co, ".
                    "camunicipiostbl cm, trviajestractorestbl vt ". 
                    "WHERE tt.direccionEntrega = cd.direccion ".
                    "AND cd.idColonia = co.idColonia ".
                    "AND co.idMunicipio = cm.idMunicipio ".
                    "AND tt.idViajeTractor = vt.idViajeTractor ".
                    "AND vt.centroDistribucion = 'CDTOL' ".
                    "AND cm.idEstado = (select valor from cageneralestbl where tabla = 'caEstadosTbl' ".
                    "AND columna = 'idEstado') ".
                    "AND tt.idViajeTractor = ".$_REQUEST['trap484IdViajeHdn'].";";

            $rs_consulta = fn_ejecuta_query($kmDf);

            if($_REQUEST['trap484ViajeVacioHdn'] == ''){
                if(sizeof($rs_consulta['root']) == 0){
                    $litros = ($kmTabulados * 2) / $rendimiento;
                }else{
                    $litros = (($kmTabulados * 2) / $rendimiento) + 30;
                }
            }else{
                $litros = $kmTabulados / $rendimiento;
            }

            $litros = number_format($litros,2, ".", "");

            // CALCULO DE COMBUSTIBLE 
            $combustible = $litros *  $conceptosArr['2315'];


            
            //CALCULO MACHETEROS
            if($_REQUEST['trap484CentroDistribucionChoferHdn']== 'CDSAL' || $_REQUEST['trap484CentroDistribucionChoferHdn']== 'CDTSA'){
                $macheteros  = $conceptosArr['6003'] * $numUnidades;
                $conceptoMacheteros = '6003';
            } else {
                $macheteros  = $conceptosArr['6002'] * $numUnidades;
                $conceptoMacheteros = '6002';
                
            }
            //CALCULO DE TAXIS (DE ACUERDO A BASE DEL CHOFER)
            if($_REQUEST['trap484CentroDistribucionChoferHdn']== 'CDSAL' || $_REQUEST['trap484CentroDistribucionChoferHdn']== 'CDTSA'){
                $taxi = $conceptosArr['6004'] * $numUnidades; //Base Saltillo y TAMSA
                 $conceptoTaxi = '6004';
            }else{
                $taxi = $conceptosArr['6010'] * $numUnidades; //Base Toluca, todos las bases diferentes a CDSAL y CDTSA
                $conceptoTaxi = '6010';                
            }

            //CALCULO DE ALIMENTOS (DE ACUERDO A BASE DEL CHOFER)
        if($_REQUEST['trap484ViajeVacioHdn'] == ''){
            if($_REQUEST['trap484CentroDistribucionChoferHdn']== 'CDSAL' || $_REQUEST['trap484CentroDistribucionChoferHdn']== 'CDTSA'){
                $alimentos = $conceptosArr['2343'] * $kmTabulados; //Base Saltillo y TAMSA
                $totalAlimentos = $alimentos * 2;
                $conceptoAlimentos = '2343';
            }else{
                $alimentos = $conceptosArr['2342'] * $kmTabulados; //Base Toluca, todos las bases diferentes a CDSAL y CDTSA
                $totalAlimentos = $alimentos * 2;
                $conceptoAlimentos = '2342';
            }
        }else{
            if($_REQUEST['trap484CentroDistribucionChoferHdn']== 'CDSAL' || $_REQUEST['trap484CentroDistribucionChoferHdn']== 'CDTSA'){
                $totalAlimentos = $conceptosArr['2343'] * $kmTabulados; //Base Saltillo y TAMSA
                $conceptoAlimentos = '2343';
            }else{
                $totalAlimentos = $conceptosArr['2342'] * $kmTabulados; //Base Toluca, todos las bases diferentes a CDSAL y CDTSA
                $conceptoAlimentos = '2342';
            }
        }


            $combustible = number_format($combustible, 2, ".", "");
            $macheteros = number_format($macheteros, 2, ".", "");
            $taxi = number_format($taxi, 2, ".", "");
            $totalAlimentos = number_format($totalAlimentos, 2, ".", "");
            $peajes = number_format(00.00, 2, ".", "");
            $otros = number_format(00.00, 2, ".", "");
            //ARREGLO CON LOS CALCULOS
            if($_REQUEST['trap484AcompananteHdn'] == '' && $_REQUEST['trap484ViajeVacioHdn'] == ''){
                $a['root'] = array(array('id'=>'2315','concepto'=>$nombreArr['2315'],'cantidad' => $combustible,'cantidadOriginal' => $combustible,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>'litros','concepto'=>'LITROS','cantidad' => $litros,'cantidadOriginal' => $litros,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>$conceptoTaxi,'concepto'=>$nombreArr[$conceptoTaxi],'cantidad' => $taxi,'cantidadOriginal' => $taxi,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>$conceptoAlimentos,'concepto'=>$nombreArr[$conceptoAlimentos],'cantidad' => $totalAlimentos,'cantidadOriginal' => $totalAlimentos,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>'2333','concepto'=>$nombreArr['2333'],'cantidad' => $peajes,'cantidadOriginal' => $peajes,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>$conceptoMacheteros,'concepto'=>$nombreArr[$conceptoMacheteros],'cantidad' => $macheteros,'cantidadOriginal' => $macheteros,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>'7006','concepto'=>$nombreArr['7006'], 'cantidad'=>$otros,'cantidadOriginal'=>$otros,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']));
            }else{
                if($_REQUEST['trap484ViajeVacioHdn'] === 'ACOMPANANTE'){
                    $a['root'] = array(array('id'=>$conceptoAlimentos,'concepto'=>$nombreArr[$conceptoAlimentos],'cantidad' => $totalAlimentos,'cantidadOriginal' => $totalAlimentos,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                        array('id'=>'7006','concepto'=>$nombreArr['7006'], 'cantidad'=>$otros,'cantidadOriginal'=>$otros,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']));
                }else{
                    $a['root'] = array(array('id'=>'2315','concepto'=>$nombreArr['2315'],'cantidad' => $combustible,'cantidadOriginal' => $combustible,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>'litros','concepto'=>'LITROS','cantidad' => $litros,'cantidadOriginal' => $litros,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>$conceptoAlimentos,'concepto'=>$nombreArr[$conceptoAlimentos],'cantidad' => $totalAlimentos,'cantidadOriginal' => $totalAlimentos,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>'2333','concepto'=>$nombreArr['2333'],'cantidad' => $peajes,'cantidadOriginal' => $peajes,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']),
                    array('id'=>'7006','concepto'=>$nombreArr['7006'], 'cantidad'=>$otros,'cantidadOriginal'=>$otros,'idViajeTractor'=>$_REQUEST['trap484IdViajeHdn']));

                }
            }
        }            
        
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getCalculoGastosViajeVacio(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap484KmTabuladosTxt'] == ""){
            $e[] = array('id'=>'trap484KmTabuladosTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['trap484RendimientoTxt'] == ""){
            $e[] = array('id'=>'trap484RendimientoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        
        if($a['success'] == true){
            $kmTabulados = $_REQUEST['trap484KmTabuladosTxt'];
            $rendimiento = $_REQUEST['trap484RendimientoTxt'];

            $sqlGetConceptosStr = "SELECT cc.concepto, co.nombre, cc.importe, cc.cuentaContable ".
                                  "FROM caConceptosCentrosTbl cc, caconceptostbl co ".
                                  "WHERE cc.concepto = co.concepto ".
                                  "AND cc.centroDistribucion = '".$_SESSION['usuCompania']."'";

            $rs = fn_ejecuta_query($sqlGetConceptosStr);        
            
            $conceptosArr = array();

            for ($i = 0; $i < sizeof($rs['root']); $i++) { 
                $conceptosArr[$rs['root'][$i]['concepto']]['nombre'] = $rs['root'][$i]['nombre'];
                $conceptosArr[$rs['root'][$i]['concepto']]['importe'] = $rs['root'][$i]['importe'];
            }

            $choferCentroOrigen = $_REQUEST['trap484CentroDistribucionChoferHdn'];

            //Dependiendo del Centro de Distribución del Chofer, los alimentos pertenecen a otro Concepto
            if($choferCentroOrigen == 'CDSAL' || $choferCentroOrigen == 'CDTSA'){
                //Base Saltillo y TAMSA
                $conceptoAlimentos = '2343';
            } else{
                //Base Toluca, todos las bases diferentes a CDSAL y CDTSA
                $conceptoAlimentos = '2342';
            }

            //Si los conceptos no existen, los cálculos de gastos no se realizan
            if (!isset($conceptosArr['2315'])){
                $a['id'] = '2315';
                $a['success'] = false;
                $a['errorMessage'] = getConceptosNoExist();
            }
            if (!isset($conceptosArr[$conceptoAlimentos])){
                $a['id'] = $conceptoAlimentos;
                $a['success'] = false;
                $a['errorMessage'] = getConceptosNoExist();
            }

            if ($a['success'] == true){
                //CALCULO DE LITROS
                $litros = $kmTabulados / $rendimiento;
                $litros = number_format($litros,2, ".", "");

                // CALCULO DE COMBUSTIBLE 
                $combustible = $litros *  $conceptosArr['2315']['importe'];

                //CALCULO DE ALIMENTOS
                $totalAlimentos = $conceptosArr[$conceptoAlimentos]['importe'] * $kmTabulados; //Base Saltillo y TAMSA

                //TOTAL
                $total = ceil(($combustible + $totalAlimentos)/100)*100;

                //Formatea las Cantidades
                $combustible = number_format($combustible, 2, ".", "");
                $totalAlimentos = number_format($totalAlimentos, 2, ".", "");
                $peajes = $otros = '0'; 

                /*Conceptos Viaje Vacio
                * 2315 - Combustible
                * 2343/2342 - Alimentos *Acompañante
                * 2333 - Peaje
                * 7006 - Otros *Acompañante
                * 7001 - Total *Acompañante
                */
                
                if(!$esViajeAcompanate){  //SI ES UN VIAJE VACÍO NORMAL
                    $conceptos[0] = array('id' => '2315', 'concepto' => $conceptosArr['2315']['nombre'], 'cantidad' => $combustible);
                    $conceptos[1] = array('id' => $conceptoAlimentos, 'concepto' => $conceptosArr[$conceptoAlimentos]['nombre'], 'cantidad' => $totalAlimentos);
                    $conceptos[2] = array('id' => '2333', 'concepto' => $conceptosArr['2333']['nombre'], 'cantidad' => $peajes);
                    $conceptos[3] = array('id' => '7006', 'concepto' => $conceptosArr['7006']['nombre'], 'cantidad' => $otros); 
                    $conceptos[4] = array('id' => '7001', 'concepto' => $conceptosArr['7001']['nombre'], 'cantidad' => $total, 'litros' => $litros);

                } else { //VIAJE VACÍO DE ACOMPAÑANTE
                    $total = ceil($totalAlimentos/100)*100;
                    $conceptos[0] = array('id' => $conceptoAlimentos, 'concepto' => $conceptosArr[$conceptoAlimentos]['nombre'], 'cantidad' => $totalAlimentos);
                    $conceptos[1] = array('id' => '7006', 'concepto' => $conceptosArr['7006']['nombre'], 'cantidad' => $otros); 
                    $conceptos[2] = array('id' => '7001', 'concepto' => $conceptosArr['7001']['nombre'], 'cantidad' => $total);
                }
                $a['records'] = count($conceptos);
                $a['root'] = $conceptos;           
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function addGastosViajeTractor($idViaje, $compania, $concepto, $importe, $observaciones, $claveMov, $tipoDocumento, $kmTabulados){
        $e = array();
        $a = array('success' => true, 'folio' => '');

        if($idViaje == ""){
            $e[] = array('id'=>'%IdViajeTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($compania == ""){
            $e[] = array('id'=>'%CompaniaHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        if($tipoDocumento == ""){
            $e[] = array('id'=>'%TipoDoctoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        /*if($kmTabulados == ""){
            $e[] = array('id'=>'%KmTabuladosHdn','msg'=>getRequerido());
            $a['success'] = false;
        }*/
        if($claveMov == ""){
            $e[] = array('id'=>'%ClaveMovimientoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $conceptoArr = explode('|', substr($concepto, 0, -1));
        if(in_array('', $valorArr)){
            $e[] = array('id'=>'%ConceptoHdn','msg'=>getRequerido());
            $a['success'] = false;
        }
        $importeArr = explode('|', substr($importe, 0, -1));
        if(in_array('', $valorArr)){
            $e[] = array('id'=>'%ImporteTxt','msg'=>getRequerido());
            $a['success'] = false;
        }
        $observacionesArr = explode('|', substr($observaciones, 0, -1));
        if(in_array('', $valorArr)){
            $e[] = array('id'=>'%ObservacionesTxa','msg'=>getRequerido());
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            //Obtener el folio

            $sqlGetFolioStr = "SELECT folio FROM trFoliosTbl ".
                              "WHERE tipoDocumento='".$tipoDocumento."' ".
                              "AND centroDistribucion='".$_SESSION['usuCompania']."' ".
                              "AND compania = 'TR'";

            $rs = fn_ejecuta_query($sqlGetFolioStr);
            $folio = $rs['root'][0]['folio'];

            if ((integer) $folio < 9) {
                $folio = '0'.(string)((integer)$folio+1);
            } else {
                $folio = (string)((integer)$folio+1);
            }
            $a['folio'] = $folio;

            $fecha = date("Y-m-d H:i:s");
            $importeTotal = 0;

            for ($iInt=0; $iInt < sizeof($conceptoArr); $iInt++) { 
                if ($conceptoArr[$iInt] != "OTROS") {

                   //echo json_encode($conceptoArr[$iInt]);
                    $importe = $importeArr[$iInt];
                    $importeTotal = $importeTotal + floatval($importe);

                    
                
                    $sqlAddGastosViajeTractorStr = "INSERT INTO trGastosViajeTractorTbl ".
                                                   "(idViajeTractor, concepto,centroDistribucion,folio,fechaEvento,cuentaContable,".
                                                    "mesAfectacion,importe,observaciones,claveMovimiento,usuario,ip) ".
                                                   "VALUES (".
                                                    $idViaje.",".
                                                    "'".$conceptoArr[$iInt]."',".
                                                    "'".$_SESSION['usuCompania']."',".
                                                    "'".$folio."',".
                                                    "'".$fecha."',".
                                                    "(SELECT cuentaContable FROM caConceptosCentrosTbl ".
                                                        "WHERE concepto = '".$conceptoArr[$iInt]."' ".
                                                        "AND centroDistribucion = '".$_SESSION['usuCompania']."'),".
                                                    "0,".
                                                    replaceEmptyDec($importe).",".
                                                    replaceEmptyNull($observacionesArr[$iInt]).",".
                                                    "'".$claveMov."','".
                                                    $_SESSION['idUsuario']."',".
                                                    "'".$_SERVER['REMOTE_ADDR']."'".
                                                    ")";

                    $rs = fn_ejecuta_query($sqlAddGastosViajeTractorStr);

                    if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $a['successMessage'] = getGastosViajeTractorSuccessMsg();
                        $a['sql'] = $sqlAddGastosViajeTractorStr;    
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddGastosViajeTractorStr;
                    }
                }
            }

            $nombreConcepto = "7001";
            $importeTotal = ceil($importeTotal/100)*100;

            //Inserta Conceptos de TOTAL Gastos/Complementos
            $sqlInsertGasto =  "INSERT INTO trGastosViajeTractorTbl(idViajeTractor, concepto, centroDistribucion, ".
                               "folio, fechaEvento, cuentaContable, mesAfectacion, importe, observaciones, ".
                               "claveMovimiento, usuario, ip) VALUES(".$idViaje.", '".$nombreConcepto."', '".
                               $_SESSION['usuCompania']."', '".$folio."', '".$fecha."', ".
                               "(SELECT cuentaContable FROM caConceptosCentrosTbl ".
                                                        "WHERE concepto = '".$conceptoNombre."' ".
                                                        "AND centroDistribucion = '".$_SESSION['usuCompania']."'), ".
                                "0,".
                                replaceEmptyDec($importeTotal).",".
                                replaceEmptyNull($observacionesArr[$iInt]).",".
                                "'".$claveMov."','".
                                $_SESSION['idUsuario']."',".
                                "'".$_SERVER['REMOTE_ADDR']."');";

            fn_ejecuta_query($sqlInsertGasto);      

            //Actualiza los kilometrosTabulados, si y solo sí son modificados
            $sqlUpdKmTabulados = "UPDATE trviajestractorestbl set kilometrosTabulados = '".$kmTabulados."' ".
                                 " WHERE idViajeTractor = ".$idViaje.
                                 " AND (SELECT tmp.kilometrosTabulados FROM (SELECT tv2.kilometrosTabulados ".
                                 " FROM trviajestractorestbl tv2 ".
                                 " WHERE tv2.idViajeTractor = ".$idViaje." ) as tmp) <> ".$kmTabulados.";";

            fn_ejecuta_query($sqlUpdKmTabulados);

            //Verifico Lista Espera
            /*if($_REQUEST['trap484ClaveMovimientoHdn'] != 'VA'){ //Si es Valido, es decir, si tiene lista de espera
                //ACTUALIZO LA LISTA DE ESPERA si es un Cto de Distribución Válido con Patio
                $sqlListaEspera =  "UPDATE trEsperaChoferesTbl SET claveMovimiento = 'VG' ".
                                   "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                   "AND idTractor = ".$_REQUEST['trap484TractorCmb'].
                                   " AND '".$_SESSION['usuCompania']."' = (SELECT valor FROM caGeneralesTbl ".
                                                                         "WHERE tabla = 'trViajesTractoresTbl' ".
                                                                         "AND columna = 'cdValidos' ".
                                                                         "AND valor = '".$_SESSION['usuCompania']."')";    
                $rs = fn_ejecuta_query($sqlListaEspera);
                if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['successMessage'] = getGastosViajeTractorSuccessMsg();
                    $a['sql'] = $sqlListaEspera;
                } else{
                    $a['success'] = false;
                    $a['errorMessage'] =  'No se pudo actualizar la Lista de Espera </br>'.$_SESSION['error_sql']."</br>".$sqlListaEspera;
                }
            }*/

            $sqlUpdFolioStr = "UPDATE trFoliosTbl ".
                              "SET folio = '".$folio."' ".
                              "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND compania = 'TR' ".
                              "AND tipoDocumento = '".$tipoDocumento."'";

            $rs = fn_ejecuta_query($sqlUpdFolioStr);

            $sqlSelectClaveMovimiento = "SELECT claveMovimiento FROM trviajestractorestbl ".
                                      "WHERE idViajeTractor = ".$_REQUEST['trGastosIdViajeTractorHdn'];

            $rs1 = fn_ejecuta_query($sqlSelectClaveMovimiento);

                if ($rs1['root'][0]['claveMovimiento']== 'VV') {
                    
                    $sqlUpdViajeStr = "UPDATE trviajestractorestbl SET claveMovimiento = 'VG', ".
                                      "fechaEvento = '".date("Y-m-d H:i:s")."' ".
                                      "WHERE idViajeTractor = ".$_REQUEST['trGastosIdViajeTractorHdn'];
                    $rs = fn_ejecuta_query($sqlUpdViajeStr);
                } 

            
        } else {
            $a['errorMessage'] = getErrorRequeridos();
        }
        
        if($_REQUEST['msgLog'] != ''){
        		$sql = "INSERT INTO trLogMantenimientoTbl (idViajeTractor, centroLibera, idTractor, movimiento, pantalla, idUsuario, ip, fechaEvento, tipoDocumento) VALUES(".
        				   $idViaje.", '".
        				   $_SESSION['usuCompania']."', ".
        				   $_REQUEST['trap484TractorCmb'].", ".
        				   $_REQUEST['movAux'].", '".
        				   $_REQUEST['titleWin']."', ".
        				   $_SESSION['idUsuario'].", '".
        				   $_SERVER['REMOTE_ADDR']."', ".
        				   "now(), '".
        				   $_REQUEST['msgLog']."')";
            fn_ejecuta_query($sql);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){                
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sql;
            }        				   
        }        

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        $a['importe'] = $importeTotal;
        return $a;
    }

    function cancelarGastos(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['trap486FolioTxt'] == ""){
            $e[] = array('id'=>'trGastosViajeTractorClaveMovimientoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($a['success'] == true){
            $sqlCancelarGastosStr = "UPDATE trGastosViajeTractorTbl SET claveMovimiento = 'GX' ".
                                    "WHERE claveMovimiento = '".$_REQUEST['trGastosViajeTractorClaveMovimientoHdn']."' ".
                                    "AND idViajeTractor = '".$_REQUEST['trGastosIdViajeTractorHdn']."'";
                                    

            $rs = fn_ejecuta_query($sqlCancelarGastosStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlCancelarGastosStr;
                $a['successMessage'] = getGastosCanceladosSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlCancelarGastosStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getNumFoliosPorViaje($idViajeTractor){
        $lsWhereStr = '';
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorIdViajeTractorHdn'], "idViajeTractor", 0, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorClaveMovimientoHdn'], "claveMovimiento", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        $lsWhere2Str = '';
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeIdTractorHdn'], "vt.idTractor", 0, 1);
            $lsWhere2Str = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trGastosViajeTractorViajeHdn'], "vt.viaje", 0, 1);
            $lsWhere2Str = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetNumFoliosStr = "SELECT idViajeTractor, COUNT(idViajeTractor) +1 AS numFolios FROM ".
                              "(SELECT gv.idViajeTractor, gv.folio, gv.claveMovimiento, COUNT(gv.folio) AS conceptosXFolio FROM trGastosViajeTractorTbl gv  ".$lsWhereStr. " ".
                              "GROUP BY gv.idViajeTractor, gv.folio) AS trGastosViajeTractorTmp ".
                              $lsWhereStr. " GROUP BY idViajeTractor";

        $rs = fn_ejecuta_query($sqlGetNumFoliosStr);

        echo json_encode($rs);
    }

    function addGastoViajeVacio(){
        $e = array();
        $a = array('success' => true, 'folio' => '');

        if($_REQUEST['gastosViajeVacioIdTractorHdn'] == ""){
            $e[] = array('id'=>'gastosViajeVacioIdTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['gastosViajeVacioClaveChoferHdn'] == ""){
            $e[] = array('id'=>'gastosViajeVacioClaveChoferHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if ($_REQUEST['gastosViajeVacioCompaniaHdn'] == "") {
            $e[] = array('id'=>'gastosViajeVacioCompaniaHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if ($_REQUEST['gastosViajeVacioIdPlazaOrigenHdn'] == "") {
            $e[] = array('id'=>'gastosViajeVacioIdPlazaOrigenHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if ($_REQUEST['gastosViajeVacioIdPlazaDestinoHdn'] == "") {
            $e[] = array('id'=>'gastosViajeVacioIdPlazaDestinoHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if ($_REQUEST['gastosViajeVacioKmTabuladosHdn'] == "") {
            $e[] = array('id'=>'gastosViajeVacioKmTabuladosHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if ($_REQUEST['gastosViajeVacioTotalHdn'] == "") {
            $e[] = array('id'=>'gastosViajeVacioTotalHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if ($_REQUEST['gastosViajeVacioLitrosDsp'] == "") {
            $e[] = array('id'=>'gastosViajeVacioLitrosDsp','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        //Valida que el Total no sea 0 o menos, como lo recibe con el formato $#,###.## Lo formatea a válido
        $total7001 = str_replace(" ", "", $_REQUEST['gastosViajeVacioTotalHdn']);
        $total7001 = str_replace("$", "", $total7001);
        $total7001 = str_replace(",", "", $total7001);



        if($total7001 < 1){
            $a['success'] = false;
            $e[] = array('id'=>'gastosViajeVacioTotalHdn','msg'=>'Cantidad igual o menor a 0');
            $a['errorMessage'] = 'El Total de Gastos no puede ser $0.00. </br><center>Cancele y Capture de nuevo</center>';
        }
        
        $conceptoArr = $_REQUEST['gastosViajeVacioConceptoHdn'];
        $importeArr = $_REQUEST['gastosViajeVacioImporteHdn'];

        //echo ("Concepto".$conceptoArr); 
        //echo ("importe".$importeArr);         

        $conceptoArr = explode('|', $conceptoArr);

        if(in_array('', $conceptoArr)){
            $e[] = array('id'=>'%ConceptoHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        $importeArr = explode('|',$importeArr);
        if(in_array('', $importeArr)){
            $e[] = array('id'=>'%ImporteTxt','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }

        if($a['success'] == true){
            //Si es un Viaje Normal vacio INSERTA el Viaje
            $sqlAddViajeTractor = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                                             "centroDistribucion,viaje,fechaEvento,kilometrosTabulados, kilometrosComprobados, ".
                                             "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                                             "VALUES(";


            if($_REQUEST['gastosViajeVacioIdViajePadreHdn'] != ""){ 
                //Es Viaje de Acompañante VA
                $cveMovimiento = "VA";
                $idViajePadre = $_REQUEST['gastosViajeVacioIdViajePadreHdn'];
                $cveMovimientoGasto = "GO";
                $claveChofer = $_REQUEST['gastosViajeVacioAcompananteHdn'];
            } else{ // Es Viaje Vacio Normal VC
                $cveMovimiento = "VC";
                $idViajePadre = "";
                $cveMovimientoGasto = "GV";
                $claveChofer = $_REQUEST['gastosViajeVacioClaveChoferHdn'];
            }                                   
            $sqlAddViajeTractor .= $_REQUEST['gastosViajeVacioIdTractorHdn'].", ".$claveChofer.", ".
                                   $_REQUEST['gastosViajeVacioIdPlazaOrigenHdn'].", ".$_REQUEST['gastosViajeVacioIdPlazaDestinoHdn'].", '".
                                   $_SESSION['usuCompania']."', (SELECT IFNULL((max(tmp.viaje)) + 1, 1) FROM (SELECT * FROM trViajesTractoresTbl) as tmp WHERE tmp.idTractor = ".
                                   $_REQUEST['gastosViajeVacioIdTractorHdn']."), '".date('Y-m-d H:i:s')."', ".
                                   $_REQUEST['gastosViajeVacioKmTabuladosHdn'].", NULL, ".$_REQUEST['gastosViajeVacioKmTabuladosHdn'].", 0, 0, ".
                                   replaceEmptyNull($idViajePadre).", '".$cveMovimiento."', '".$_SESSION['idUsuario']."', '".$_SERVER['REMOTE_ADDR']."');";

            
            $rs = fn_ejecuta_query($sqlAddViajeTractor);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlAddViajeTractor;
                $a['successMessage'] .= 'Se Agrego el Viaje Correctamente|';
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddViajeTractor;
                return $a;
            }

            $idViajeTractor = mysql_insert_id();
            $folio = getFolioGastos();

            //Agrega los Gastos...
            $sqlAddGastoViajeVacio = "INSERT INTO trGastosViajeTractorTbl ".
                                     "(idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable,".
                                     "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip) VALUES ";
            
            for ($i = 0; $i < count($conceptoArr); $i++) {
                if(isset($conceptoArr) && $conceptosArr[$i] != '0'){
                    $observaciones = '';
                    $importe = $importeArr[$i];
                    if($i > 0){
                        $sqlAddGastoViajeVacio .= ", ";
                    }
                    //Si es el importe 7001  le asigna el TOTAL de los Gastos
                    if($conceptoArr[$i] == '7001'){
                        $importe = $total7001;
                    }
                    if($conceptoArr[$i] == '2315'){
                        $observaciones = $_REQUEST['gastosViajeVacioLitrosDsp'];
                    }
                    $sqlCtaContable = "(SELECT cuentaContable FROM caConceptosCentrosTbl WHERE concepto = '".$conceptoArr[$i].
                                      "' AND centroDistribucion = '".$_SESSION['usuCompania']."')";

                    $sqlAddGastoViajeVacio .= "(".$idViajeTractor.", '".$conceptoArr[$i]."', '".$_SESSION['usuCompania']."', '".$folio.
                                              "', '".date('Y-m-d H:i:s', time() + $i)."', ".$sqlCtaContable.", 0, ".$importe.
                                              ", '".$observaciones."', '".$cveMovimientoGasto."', '".$_SESSION['idUsuario']."', '".$_SERVER['REMOTE_ADDR']."')";
                }                                 
            }

            $rs = fn_ejecuta_query($sqlAddGastoViajeVacio);

            updFolioGasto($folio);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlAddGastoViajeVacio;
                $a['successMessage'] = 'Se Agregaron los Gastos Correctamente';
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddGastoViajeVacio;
            }
            $a['folio'] = $folio;
        }
        if($a['success'] && $_REQUEST['msgLog'] != ''){
        		$sql = "INSERT INTO trLogMantenimientoTbl (idViajeTractor, centroLibera, idTractor, movimiento, pantalla, idUsuario, ip, fechaEvento, tipoDocumento) VALUES(".
        				   $idViajeTractor.", '".
        				   $_SESSION['usuCompania']."', ".
        				   $_REQUEST['gastosViajeVacioIdTractorHdn'].", ".
        				   $_REQUEST['movAux'].", '".
        				   $_REQUEST['titleWin']."', ".
        				   $_SESSION['idUsuario'].", '".
        				   $_SERVER['REMOTE_ADDR']."', ".
        				   "now(), '".
        				   $_REQUEST['msgLog']."')";
            fn_ejecuta_query($sql);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sql;
                $a['successMessage'] .= 'Se Agrego el log Correctamente';
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sql;
                return $a;
            }        				   
        }
        $a['errors'] = $e;

        return $a;

        //Si ya es un Viaje Normal Vacio y se agrega Acompañante Actualiza el Viaje
        //"UPDATE trViajesTractoresTbl SET";
    }
    function addComplementoViajeVacio(){
        $e = array();
        $a = array('success' => true, 'folio' => '');


        if($_REQUEST['gastosViajeVacioIdViajeTractorHdn'] == ""){
            $e[] = array('id'=>'gastosViajeVacioIdViajeTractorHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        if($_REQUEST['gastosViajeVacioTotalHdn'] == ""){
            $e[] = array('id'=>'gastosViajeVacioTotalHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
                                                                
        if($_REQUEST['gastosViajeVacioIdViajePadreHdn'] != ""){ 
            $idViajeTractor = $_REQUEST['gastosViajeVacioIdViajePadreHdn'];
        } else{
            $idViajeTractor = $_REQUEST['gastosViajeVacioIdViajeTractorHdn'];
        }
        
        $conceptoArr = $_REQUEST['gastosViajeVacioConceptoHdn'];
        $importeArr = $_REQUEST['gastosViajeVacioImporteHdn'];

        $conceptoArr = explode('|', $conceptoArr);
        if(in_array('', $conceptoArr)){
            $e[] = array('id'=>'%ConceptoHdn','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos();
        }
        $importeArr = explode('|',$importeArr);
        if(in_array('', $importeArr) || count($importeArr) < 2){
            $e[] = array('id'=>'%ImporteTxt','msg'=>getRequerido());
            $a['success'] = false;
            $a['errorMessage'] = getErrorRequeridos()."</br> O no hay complementos capturados.";
        }
        //Valida que el Total no sea 0 o menos, como lo recibe con el formato $#,###.## Lo formatea a válido

        $total7001 = str_replace(" ", "", $_REQUEST['gastosViajeVacioTotalComplementosHdn']);
        $total7001 = str_replace("$", "", $total7001);
        $total7001 = str_replace(",", "", $total7001);
        $total7001 = floatval($total7001);

        if($total7001 < 1){
            $a['success'] = false;
            $e[] = array('id'=>'gastosViajeVacioTotalHdn','msg'=>'Cantidad igual o menor a 0');
            $a['errorMessage'] = 'El Total de Gastos no puede ser $0.00. </br><center>Cancele y Capture de nuevo</center>';
        }
        $total7001 = ceil($total7001/100)*100;
				//var_dump($total7001);

        if($a['success'] == true){
            $folio = getFolioGastos();

            //Agrega los Gastos...
            $sqlAddGastoViajeVacio = "INSERT INTO trGastosViajeTractorTbl ".
                                         "(idViajeTractor, concepto, centroDistribucion, folio, fechaEvento, cuentaContable, ".
                                         "mesAfectacion, importe, observaciones, claveMovimiento, usuario, ip) VALUES ";
                
            for ($i = 0; $i < count($importeArr); $i++) {
                if(isset($conceptoArr[$i]) && ($conceptoArr[$i] != '0' || $conceptoArr[$i] == "")){
                    $importe = $importeArr[$i];
                    if($i > 0){
                        $sqlAddGastoViajeVacio .= ", ";
                    }
                    if($conceptoArr[$i] == '7001'){

                        $importe = $total7001;
                    }
                    $sqlCtaContable = "(SELECT cuentaContable FROM caConceptosCentrosTbl WHERE concepto = '".$conceptoArr[$i].
                                      "' AND centroDistribucion = '".$_SESSION['usuCompania']."')";

                    $sqlAddGastoViajeVacio .= "(".$idViajeTractor.", '".$conceptoArr[$i]."', '".$_SESSION['usuCompania']."', '".$folio.
                                              "', '".date('Y-m-d H:i:s', time() + $i)."', ".$sqlCtaContable.", 0, ".$importe.
                                              ", '', 'GM', '".$_SESSION['idUsuario']."', '".$_SERVER['REMOTE_ADDR']."')";
                }                                 
            }

            $rs = fn_ejecuta_query($sqlAddGastoViajeVacio);

            updFolioGasto($folio);
            $a['folio'] = $folio;

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['sql'] = $sqlAddGastoViajeVacio;
                $a['successMessage'] = 'Se Agregaron los Complementos Correctamente';
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddGastoViajeVacio;
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);

    }
    function getFolioGastos(){
        //Obtener el folio
        $sqlGetFolioStr = "SELECT folio FROM trFoliosTbl ".
                          "WHERE tipoDocumento = 'TG' ".
                          "AND centroDistribucion='".$_SESSION['usuCompania']."' ".
                          "AND compania = 'TR'";

        $rs = fn_ejecuta_query($sqlGetFolioStr);
        $folio = $rs['root'][0]['folio'];

        if ((integer) $folio < 9) {
            $folio = '0'.(string)((integer)$folio+1);
        } else {
            $folio = (string)((integer)$folio+1);
        }
        return $folio;
    }
    function updFolioGasto($folio){
        
        $sqlUpdFolio =  "UPDATE trFoliosTbl SET folio = '".$folio."' ".
                        "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                        "AND compania = 'TR' ".
                        "AND tipoDocumento = 'TG';";

        fn_ejecuta_query($sqlUpdFolio);
    }

    function getTotalComplementosViajeTractor($idViajeTractor){

        $conceptoComplementoTotal = "7001";

        $sqlGetTotal = "SELECT ";

        $sqlGetTotal .= "(SELECT SUM(gt.importe) FROM trGastosViajetractortbl gt ".
                        "WHERE gt.idViajeTractor = ".$idViajeTractor.
                        " AND gt.concepto = '".$conceptoComplementoTotal."' ".
                        "AND gt.claveMovimiento = 'GG' ".
                        "GROUP BY gt.concepto) AS importeTotal, ";

        $sqlGetTotal .= "(SELECT SUM(gt.importe) FROM trGastosViajetractortbl gt ".
                        "WHERE gt.idViajeTractor = ".$idViajeTractor.
                        " AND gt.concepto = '".$conceptoComplementoTotal."' ".
                        "AND gt.claveMovimiento IN ('GM', 'GO') ".
                        "GROUP BY gt.concepto) AS complementosTotal";

        $rsImporteTotal = fn_ejecuta_query($sqlGetTotal); 

        echo json_encode($rsImporteTotal);
    }

    function calculoAnticipoSueldo(){
        $calculoSueldo="SELECT (importe*".$_REQUEST['trgastosKilometrosTxt'].") as totalSueldo, (importe*".$_REQUEST['trgastosKilometrosTxt'].")*35/100 as totalAnticipo
                        from caconceptoscentrostbl
                        where concepto=139
                        AND centroDistribucion='CDTOL'";

        $rs=fn_ejecuta_query($calculoSueldo);

        echo json_encode($rs);
    }

    function insertaSueldo(){

         $sqlGetFolioStr = "SELECT folio FROM trFoliosTbl ".
                              "WHERE tipoDocumento='AS' ".
                              "AND centroDistribucion='".$_SESSION['usuCompania']."' ".
                              "AND compania = 'TR'";

            $rs = fn_ejecuta_query($sqlGetFolioStr);
            $folio = $rs['root'][0]['folio'];

            if ((integer) $folio < 9) {
                $folio = '0'.(string)((integer)$folio+1);
            } else {
                $folio = (string)((integer)$folio+1);
            }
            $a['folio'] = $folio;

            $fecha = date("Y-m-d H:i:s");
            $importeTotal = 0;


                    $sqlAddGastosViajeTractorStr = "INSERT INTO trGastosViajeTractorTbl ".
                                                   "(idViajeTractor, concepto,centroDistribucion,folio,fechaEvento,cuentaContable,".
                                                    "mesAfectacion,importe,observaciones,claveMovimiento,usuario,ip) ".
                                                   "VALUES (".
                                                    $_REQUEST['idviajeTractor'].",".
                                                    "'139',".
                                                    "'".$_SESSION['usuCompania']."',".
                                                    "'".$folio."',".
                                                    "'".$fecha."',".
                                                    "(SELECT cuentaContable FROM caConceptosCentrosTbl ".
                                                        "WHERE concepto = '139' ".
                                                        "AND centroDistribucion = '".$_SESSION['usuCompania']."'),".
                                                    "0,".
                                                    replaceEmptyDec($_REQUEST['anticipoSueldo']).",".
                                                    replaceEmptyNull($observacionesArr[$iInt]).",".
                                                    "'AS','".
                                                    $_SESSION['idUsuario']."',".
                                                    "'".$_SERVER['REMOTE_ADDR']."'".
                                                    ")";

                    $rs = fn_ejecuta_query($sqlAddGastosViajeTractorStr);

                    if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $a['successMessage'] = getGastosViajeTractorSuccessMsg();
                        $a['sql'] = $sqlAddGastosViajeTractorStr;    
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddGastosViajeTractorStr;
                    }
                
            

            

            $sqlUpdFolioStr = "UPDATE trFoliosTbl ".
                              "SET folio = '".$folio."' ".
                              "WHERE centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND compania = 'TR' ".
                              "AND tipoDocumento = '".$tipoDocumento."'";

            $rs = fn_ejecuta_query($sqlUpdFolioStr);


            $sql="SELECT * FROM trGastosViajetractortbl where idViajeTractor=".$_REQUEST['idviajeTractor']." AND concepto=7001";
            $rs=fn_ejecuta_query($sql);

            echo json_encode($rs);



    }
?>