<?php
    setlocale(LC_TIME, "");
	setlocale(LC_TIME, 'es_MX.utf8');
	session_start();

    $codigoTalonEspecial = 'TE';
    $codigoTalonCambioDestino = 'TX';

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    generarTalones();

	function generarTalones(){
		$a = array();
        $e = array();
        $a['success'] = true;

        global $codigoTalonEspecial;
        global $codigoTalonCambioDestino;

        if($_REQUEST['trViajesTractoresIdViajeHdn'] != ""){
            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                     "AND tv.claveMovimiento != 'TX'";
        } else {

            if ($_REQUEST['trViajesTractoresFoliosHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresFoliosHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if ($_REQUEST['trViajesTractoresCompaniaHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresCompaniaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].") ".
                                     "AND tv.claveMovimiento != 'TX'";
        }

        if ($a['success']) {

            $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);  
            echo json_encode($rsTalon);
            echo json_encode(sizeof($rsTalon['root']));

            for ($nInt=0; $nInt < sizeof($rsTalon['root']) ; $nInt++) {                 
                $sqlGetTalonStr =   "SELECT tv.*, dc.descripcionCentro AS nombreCiaRemitente, dc.rfc AS rfcRemitente, dc.direccionFiscal AS dirCompania, dc.descripcionCentro, tr.tractor, ".
                                    "vt.claveChofer, ch.*, di.direccion, di.calleNumero, di.idColonia, di.distribuidor AS distDireccion, di.tipoDireccion, ".
                                    "col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, ".
                                    "(SELECT dc2.descripcionCentro FROM caDistribuidoresCentrosTbl dc2 ".
                                        "WHERE dc2.distribuidorCentro = tv.distribuidor) AS descripcionCentro,".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                        "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                                    "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                        "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                                    "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                                        "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                                    "(SELECT co.descripcion FROM caCompaniasTbl co WHERE co.compania = tr.compania) AS companiaTractor ".  
                                        "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caChoferesTbl ch, ".
                                        "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                                        "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
                                    "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                    "AND dc.distribuidorCentro = tv.companiaRemitente ".
                                    "AND tr.idTractor = vt.idTractor ".
                                    "AND ch.claveChofer = vt.claveChofer ".
                                    "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                                            "WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                                    "AND col.idColonia = di.idColonia ".
                                    "AND mu.idMunicipio = col.idMunicipio ".
                                    "AND es.idEstado = mu.idEstado ".
                                    "AND pa.idPais = es.idPais ".
                                    "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                                    "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                                    "AND tv.idTalon = ".$rsTalon['root'][$nInt]['idTalon'];

                $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

                if(sizeof($dataTalon['root']) > 0){
                	$dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                    $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                    $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                                                            $dataTalon['root'][0]['apellidoPaterno']." ".
                                                            $dataTalon['root'][0]['apellidoMaterno'];

                    //DIRECCION DESTINo
                    $tempDireccion = $dataTalon['root'][0]['calleNumero']." ".$dataTalon['root'][0]['colonia'].", ".
                    				 $dataTalon['root'][0]['estado']." ".$dataTalon['root'][0]['cp'];

                    $dataTalon['root'][0]['dirL1Dest'] = substr($tempDireccion, 0, 30);
                    $dataTalon['root'][0]['dirL2Dest'] = ltrim(substr($tempDireccion, 30, 30));
                    $dataTalon['root'][0]['dirL3Dest'] = ltrim(substr($tempDireccion, 60, 30));

                    if($dataTalon['root'][0]['dirCompania'] != ''){
                        $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                        "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                                        "caEstadosTbl es, caPaisesTbl pa ".
                                                        "WHERE pa.idPais = es.idPais ".
                                                        "AND es.idEstado = mu.idEstado ".
                                                        "AND mu.idMunicipio = co.idMunicipio ".
                                                        "AND co.idColonia = dir.idColonia ".
                                                        "AND dir.direccion = ".$dataTalon['root'][0]['dirCompania'];

                        $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                        if(sizeof($dirCompania['root']) > 0){
                        	//DIRECCION REMITENTE
		                    $tempDireccion = $dirCompania['root'][0]['calleNumero']." ".$dirCompania['root'][0]['colonia'].", ".
		                    				 $dirCompania['root'][0]['estado']." ".$dirCompania['root'][0]['cp'];

		                    $dirCompania['root'][0]['dirL1Cia'] = substr($tempDireccion, 0, 30);
		                    $dirCompania['root'][0]['dirL2Cia'] = ltrim(substr($tempDireccion, 30, 30));
		                    $dirCompania['root'][0]['dirL3Cia'] = ltrim(substr($tempDireccion, 60, 30));
                        }
                    }

                    $sqlGetUnidadesTalonStr = "SELECT ut.*, un.avanzada, un.simboloUnidad, su.descripcion AS descSimbolo ".
                                                "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                                "WHERE un.vin = ut.vin ".
                                                "AND su.simboloUnidad = un.simboloUnidad ".
                                                "AND ut.idTalon =".$rsTalon['root'][$nInt]['idTalon']." ".
                                                "AND ut.estatus != 'C' ";

                    $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);
                

                    if(sizeof($unidadesTalon['root']) > 0){
                        if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial || $dataTalon['root'][0]['tipoTalon'] == $codigoTalonCambioDestino){
                            $sqlGetServEspStr = "SELECT de.distribuidorOrigen AS distribuidor, dc.descripcionCentro, de.direccionOrigen AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionOrigen ".
                                                "AND dc.distribuidorCentro = de.distribuidorOrigen ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                                "UNION ".
                                                "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionDestino ".
                                                "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                                "AND de.idDestinoEspecial = (SELECT MAX(de2.idDestinoEspecial) ".
                                                    "FROM alDestinosEspecialesTbl de2 WHERE de2.vin = de.vin)";

                            $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);
                            //echo json_encode($sqlGetServEspStr);

                            for ($nInt=0; $nInt < sizeof($dataServEsp['root']); $nInt++) { 
                                $dataServEsp['root'][$nInt]['descDist'] = $dataServEsp['root'][$nInt]['distribuidor']." - ".
                                                                            $dataServEsp['root'][$nInt]['descripcionCentro'];


                                $tempDireccion = $dataServEsp['root'][$nInt]['calleNumero']." ".$dataServEsp['root'][$nInt]['colonia'].", ".
                                				 $dataServEsp['root'][$nInt]['estado']." ".$dataServEsp['root'][$nInt]['cp'];

                                $dataServEsp['root'][$nInt]['dirL1'] = substr($tempDireccion, 0, 30);
                                $dataServEsp['root'][$nInt]['dirL2'] = substr($tempDireccion, 30, 30);
                                $dataServEsp['root'][$nInt]['dirL3'] = substr($tempDireccion, 60, 30);
                            }
                        } else {
                        	$dataServEsp = array('root'=>array());
                        }
                        
                        $archivo = generarArchivoTalon($dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);

                        if($archivo != '')
                            mostrarTalon($dataTalon['root'][0]['folio'], $dataTalon['root'][0]['distribuidor'], $archivo);
                        else {
                            echo "Error al generar el archivo, verificar que la direccion ".
                                    "/archivos/talones/".$_SESSION['usuCompania']."/ exista";                            
                            return;
                        }
                    } else {
                    	$a['success'] = false;
                    	$a['errorMessage'] = "Talon(es) sin unidades";
                    }
                }
            }
        } else {
        	$a['errors'] = $e;
        	echo json_encode($a);
        }
	}

	function generarArchivoTalon($data, $dirCompania, $unidades, $especiales){
		global $codigoTalonEspecial;
        global $codigoTalonCambioDestino;
		
		$path = "\\archivos\\talones\\".$_SESSION['usuCompania']."\\".$_SESSION['usuCompania']."-".$data['folio'].".txt";
		$filePath = dirname(__FILE__)."\\..\\..".$path;

		if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
		    $filePath = str_replace("\\", "/", $filePath);
		}

		$talon = fopen($filePath, "w");
		if($talon){
			//AQUI SE ESCRIBE EL ARCHIVO
			$space = " ";
			$modifier = 0;

			//1
			fwrite($talon, PHP_EOL);
			//2
			fwrite($talon, PHP_EOL);
			//3
			fwrite($talon, PHP_EOL);
			//4
			fwrite($talon, PHP_EOL);

			//5 - Centro Distribucion
			fwrite($talon, str_repeat($space, 60).$data['centroDistribucion']." ");
			fwrite($talon, str_pad($data['folio'], 7, ' ', STR_PAD_LEFT)." ");
			fwrite($talon, PHP_EOL);

			//6
			fwrite($talon, PHP_EOL);

			//7 - Fecha Talon
			$fecha =  strftime("%d-%B-%Y",strtotime($data['fechaEvento']));
			$fechaArr = explode('-', $fecha);            

			fwrite($talon, str_repeat($space, 43).$fechaArr[0]);
			fwrite($talon, str_repeat($space, 6).str_pad(strtoupper($fechaArr[1]),10,' '));
			fwrite($talon, str_repeat($space, 7).strtoupper($fechaArr[2]));
			fwrite($talon, PHP_EOL);

			//8 - Plaza Origen y Destino
			fwrite($talon, str_repeat($space, 7).str_pad($data['descPlazaOrigen'], 29, ' '));
			
			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				fwrite($talon, str_repeat($space, 12).str_pad($especiales[1]['plaza'], 29, ' '));
			} else {
				fwrite($talon, str_repeat($space, 12).str_pad($data['descPlazaDestino'], 29, ' '));
			}
			fwrite($talon, PHP_EOL);

			//9 - Nombre Origen, Destino
			fwrite($talon, str_repeat($space, 7).str_pad(substr($data['nombreCiaRemitente'], 0, 30), 29, ' '));
			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				fwrite($talon, str_repeat($space, 12).str_pad(substr($especiales[1]['descDist'], 0, 24), 24, ' '));
			} else {
				fwrite($talon, str_repeat($space, 12).str_pad(substr($data['descDistribuidor'], 0, 24), 24, ' '));
			}
			fwrite($talon, PHP_EOL);

			//10 - Segunda linea del nombre del REMITENTE (Solo remitente, lo pregunté o_ô)
			fwrite($talon, str_repeat($space, 7).str_pad(substr($data['nombreCiaRemitente'], 30, 30), 29, ' '));
			fwrite($talon, PHP_EOL);

			//11 - RFC
			fwrite($talon, str_repeat($space, 10).str_pad($data['rfcRemitente'], 16, ' '));

			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				fwrite($talon, str_repeat($space, 22).str_pad($especiales[1]['rfc'], 16, ' '));
			} else {
				fwrite($talon, str_repeat($space, 22).str_pad($data['rfcDestinatario'], 16, ' '));
			}
			fwrite($talon, PHP_EOL);

			//12 - Direccion Linea 1
			fwrite($talon, str_repeat($space, 10).str_pad($dirCompania['dirL1Cia'], 30, ' '));

			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				fwrite($talon, str_repeat($space, 8).str_pad($especiales[1]['dirL1'], 30, ' '));
			} else {
				fwrite($talon, str_repeat($space, 8).str_pad($data['dirL1Dest'], 30, ' '));
			}
			fwrite($talon, PHP_EOL);

			//13 - Direccion Linea 2
			fwrite($talon, str_repeat($space, 10).str_pad($dirCompania['dirL2Cia'], 30, ' '));

			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				fwrite($talon, str_repeat($space, 8).str_pad($especiales[1]['dirL2'], 30, ' '));
			} else {
				fwrite($talon, str_repeat($space, 8).str_pad($data['dirL2Dest'], 30, ' '));
			}
			fwrite($talon, PHP_EOL);

			//14 - Direccion Linea 3 (SI ES NORMAL SON ESPACIOS, SO NADA)
			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				fwrite($talon, str_repeat($space, 10).str_pad($dirCompania['dirL3Cia'], 30, ' '));
				fwrite($talon, str_repeat($space, 8).str_pad(substr($especiales[1]['descDist'],0,30), 30, ' '));
			}
			fwrite($talon, PHP_EOL);

			//15
			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				fwrite($talon, str_repeat($space, 8).str_pad(substr($especiales[1]['descDist'],30,30), 30, ' '));
			}
			fwrite($talon, PHP_EOL);

			//16 - Direccion Recoger y Entrega
			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				//DIRECCION RECOGER
				//DIRECCION ENTREGA
			} else {
				fwrite($talon, str_repeat($space, 10).str_pad("EL MISMO", 35, ' '));
				fwrite($talon, str_repeat($space, 3)."EL MISMO");
			}
			fwrite($talon, PHP_EOL);

			//17
			fwrite($talon, str_repeat($space, 10)."0.00");
			fwrite($talon, str_repeat($space, 23)."N/D");
			fwrite($talon, str_repeat($space, 27)."CREDITO");
			fwrite($talon, PHP_EOL);

			//18
			fwrite($talon, PHP_EOL);
			//19
			fwrite($talon, PHP_EOL);
			//20
			fwrite($talon, PHP_EOL);
			//21
			fwrite($talon, PHP_EOL);

			//22,23,24,25,26,27,28,29,30,31,32,33,34,35,36
			$espaciosExtra = 0;
			for($i = 0; $i < 15; $i++) {
				if($i < sizeof($unidades)){
					fwrite($talon, str_repeat($space, 7).str_pad($unidades[$i]['simboloUnidad'], 6, ' '));
					fwrite($talon, str_repeat($space, 2).str_pad(substr($unidades[$i]['vin'],-8), 8, ' '));
					fwrite($talon, str_repeat($space, 3).str_pad($unidades[$i]['descSimbolo'], 20, ' '));
				} else {
					$espaciosExtra = 46;
				}

				if($i+1 == 1 || $i+1 == 3 || $i+1 == 10){
					fwrite($talon, str_repeat($space, $espaciosExtra+29)."$.00");
				}
				fwrite($talon, PHP_EOL);
			}

			//37
			fwrite($talon, str_repeat($space, 7).str_pad($data['claveChofer'], 5, ' '));
			fwrite($talon, str_repeat($space, 2).str_pad($data['nombreChofer'], 5, ' '));
			fwrite($talon, str_repeat($space, 34-strlen($data['nombreChofer'])).str_pad($data['tractor'], 5, ' '));
			fwrite($talon, PHP_EOL);

			//38
			fwrite($talon, PHP_EOL);

			//39
			fwrite($talon, str_repeat($space, 4)."PESOS 00 M.N.");
			fwrite($talon, PHP_EOL);

			//40
			fwrite($talon, PHP_EOL);
			//41
			fwrite($talon, PHP_EOL);
			//42
			fwrite($talon, PHP_EOL);

			//43
			fwrite($talon, str_repeat($space, 25)."FECHA ESTIMADA DE ENTREGA:  ".date("d/m/Y", strtotime(date("m/d/Y").' + '.$data['diasEntrega'].' days')));
			fwrite($talon, PHP_EOL);

			//44
			fwrite($talon, PHP_EOL);

			//45
			if($data['tipoTalon'] == $codigoTalonEspecial || $data['tipoTalon'] == $codigoTalonCambioDestino){
				fwrite($talon, str_repeat($space, 26).$data['centroDistribucion']." - ESP:\t"."*HORA:\t".date("H:i:s"));
	        } else {
	        	fwrite($talon, str_repeat($space, 26)."*HORA:\t".date("H:i:s"));
	        }
	        fwrite($talon, PHP_EOL);

	        //46
	        fwrite($talon, str_repeat($space, 26).$data['companiaTractor']);
	        fwrite($talon, PHP_EOL);
	        
		} else {
            return '';
        }

		return $path;
	}

	function mostrarTalon($folio, $distribuidor, $archivoTalon){
        echo "TALON ".$folio."<br>";
        echo '<iframe id="textfile'.$folio.'" src="'.$archivoTalon.'" onload="this.width=screen.width*.66;this.height=screen.height*.50;"></iframe><br> ';
        echo '<button onclick="print()">Imprimir</button> ';
        echo '<script type="text/javascript"> ';
        echo 'function print() { ';
        echo "var iframe = document.getElementById('textfile".$folio."');";
        echo 'iframe.contentWindow.print(); ';
        echo '} ';
        echo '</script> ';
		echo '<br><br>';	
	}

?>