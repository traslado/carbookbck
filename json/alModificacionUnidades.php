<?php
    session_start();
    $_SESSION['tiempo'] = time();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("actividadesPmp.php");


    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['alModificacionUndHdn']){
        case 'addHistTmp':
            addHistTmp();
            break;
        case 'modificacionUnidadesModif':
            modificacionUnidadesModif();
            break;
        case 'modifGrid':
            modifGrid();
            break;
        case 'delTmp':
            delTmp();
            break;
        case 'dltHistUnd':
            dltHistUnd();
            break;
        case 'UpdAlUnid':
            UpdAlUnid();
            break;
        default:
            echo 'no entra en ningun lado';
    }

function addHistTmp(){

        $addHistTmp = "INSERT INTO  alhistoricounidadestmp (idHistorico, centroDistribucion, vin, fechaEvento, ".
                        "claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                        "SELECT idHistorico, centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, 'DELETE' as observaciones, usuario, ip ".
                        "FROM alhistoricounidadestbl ".
                        "WHERE idHistorico = '".$_REQUEST['idHistoricoUnd']."';";

        $rs = fn_ejecuta_query($addHistTmp);
    }

function dltHistUnd($vin){

        $dqlHist = "DELETE FROM alhistoricounidadestbl ".
                    "WHERE vin = '".$vin."' ".
                    "AND idHistorico IN (SELECT tmp.idHistorico FROM alhistoricounidadestmp tmp ".
                                        "WHERE tmp.vin = '".$vin."' ".
                                          "AND tmp.observaciones = 'DELETE' );";

        fn_ejecuta_query($dqlHist);
        

        updateUD($vin);

        $dltUndTmp = "DELETE FROM alhistoricounidadestmp ".
                    "WHERE observaciones = 'DELETE' ".
                    "AND vin = '".$vin."';";

        fn_ejecuta_query($dltUndTmp);

    }

function UpdAlUnid(){

                $dltUndTmp = "UPDATE  alunidadesTbl SET distribuidor='".$_REQUEST['modUnidadDistribuidorCmd']."' , ".
                            "simboloUnidad= '".$_REQUEST['modUnidadSimboloCmd']."' , ".
                            "color= '".$_REQUEST['modUnidadColorCmd']."' ".
                            "WHERE vin = '".$_REQUEST['modUnidadVinCmd']."' ";

        fn_ejecuta_query($dltUndTmp);


        dltHistUnd($_REQUEST['modUnidadVinCmd']);
        acomodaPMP($_REQUEST['modUnidadVinCmd']);

    }

  function delTmp(){

        $dltUndTmp = "DELETE from alhistoricounidadestmp WHERE vin = '".$_REQUEST['modUnidadVinCmd']."' ";

        fn_ejecuta_query($dltUndTmp);
  }


  function modifGrid(){

        $json = json_decode($_REQUEST["changes"],true);


        for ($i=0; $i < sizeof($json); $i++) { 

         if ($json[$i]["claveChofer"] == null)
        {

            $claveChofer= "null";
        }
        else 
        {
            $claveChofer=$json[$i]["claveChofer"];

        }

            $localizacionUnidad = strtoupper($json[$i]["localizacionUnidad"]);


        if ($json[$i]["claveMovimiento"] == 'AM') {

                $updlocalizacionUnidad= "UPDATE allocalizacionpatiostbl set vin=null,estatus='DI' where vin='".$_REQUEST['modUnidadVinCmd']."' ";

                fn_ejecuta_query($updlocalizacionUnidad);

								$sql = "SELECT DISTINCT distribuidor FROM alUltimoDetalleTbl ".
								       " WHERE vin = '".$_REQUEST['modUnidadVinCmd']."'".
								       " AND centroDistribucion = '".$_SESSION['usuCompania']."'";
								//echo "$sql<br>";
								$udRst = fn_ejecuta_query($sql);
								
								$json[$i]["distribuidor"] = ($udRst['records'] == 0)?$json[$i]["distribuidor"]:$udRst['root'][0]['distribuidor'];
            }



        if ($json[$i]["idHistorico"] == '') {

               $InsertHistUnidades= "INSERT INTO alhistoricounidadestbl (centroDistribucion,vin,fechaEvento,claveMovimiento,distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
               "VALUES ('".$json[$i]["centroDistribucion"]."','".$_REQUEST['modUnidadVinCmd']."','".$json[$i]["fechaEvento"]."','".$json[$i]["claveMovimiento"]."', ".
               "'".$json[$i]["distribuidor"]."','".$json[$i]["idTarifa"]."','".$localizacionUnidad."',".$claveChofer.",'INSERTADO POR TOTO','".$_SESSION['idUsuario']."','".getClientIP()."') ";
                fn_ejecuta_query($InsertHistUnidades); 

            
                
            }

            else

            {

                $updHisUnidades= "UPDATE alhistoricounidadestbl set centroDistribucion='".$json[$i]["centroDistribucion"]."', ".
                                 "fechaEvento='".$json[$i]["fechaEvento"]."', ".
                                 "claveMovimiento= '".$json[$i]["claveMovimiento"]."', ".
                                 "distribuidor= '".$json[$i]["distribuidor"]."', ".
                                 "idTarifa= '".$json[$i]["idTarifa"]."', ".
                                 "localizacionUnidad= '".$localizacionUnidad."', ".
                                 "observaciones = 'MODIFICADO POR TOTO', ".
                                 "claveChofer= ".$claveChofer.", ".
                                 "usuario= '".$_SESSION['idUsuario']."' , ".
                                 "ip= '".getClientIP()."' ". 
                                 "WHERE idHistorico = '".$json[$i]['idHistorico']."' ";


                fn_ejecuta_query($updHisUnidades);


            
            }
      
        }

     updateUD($_REQUEST['modUnidadVinCmd']);
     acomodaPMP($_REQUEST['modUnidadVinCmd']);
        
    }

function updateUD ($vin) {



           $sqlSelUd ="SELECT vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
              "claveChofer, observaciones, usuario, ip ".
              "FROM alhistoricounidadestbl h1 ".
              "WHERE h1.vin = '".$vin."' ".
              "AND h1.fechaEvento = (SELECT MAX(hu2.fechaEvento) ".
                                    "FROM alhistoricounidadestbl hu2 ".
                                    "WHERE hu2.vin = h1.vin ".
                                    "AND hu2.claveMovimiento not in (SELECT ge.valor ". 
                                                                    "FROM cageneralestbl ge ". 
                                                                    "WHERE ge.tabla = 'alHistoricoUnidadesTbl' ".
                                                                    "AND ge.columna = 'nvalidos')) ";

                  $rssqlSelUd= fn_ejecuta_query($sqlSelUd);                                                            


                  $dltUdUnd = "SELECT *  FROM alultimodetalletbl ".
                              "WHERE vin = '".$vin."';";

                  $rsVin = fn_ejecuta_query($dltUdUnd);


                  if (sizeof($rsVin['root']) == 1 ) {

                            if ($rssqlSelUd['root'][0]['claveChofer'] == null)
                             {

                                 $claveChofer= "null";
                              }
                            else 
                              {
                                 $claveChofer=$rssqlSelUd['root'][0]['claveChofer'];

                              }


                  $sqlUpdUnd = "UPDATE alultimodetalletbl ".
                        "SET centroDistribucion= '".$rssqlSelUd['root'][0]['centroDistribucion']."', ".
                        "fechaEvento = '".$rssqlSelUd['root'][0]['fechaEvento']."', ".
                        "claveMovimiento = '".$rssqlSelUd['root'][0]['claveMovimiento']."', ".
                        "distribuidor = '".$rssqlSelUd['root'][0]['distribuidor']."', ".
                        "idTarifa = '".$rssqlSelUd['root'][0]['idTarifa']."', ".
                        "localizacionUnidad = '".$rssqlSelUd['root'][0]['localizacionUnidad']."', ".
                        "claveChofer =".$claveChofer.",".
                        "usuario = '".$_SESSION['idUsuario']."', ".
                        "observaciones = 'MODIFICADO POR TOTO', ".
                        "ip = '".getClientIP()."' ".
                        "WHERE vin = '".$rssqlSelUd['root'][0]['vin']."'";

                    fn_ejecuta_query($sqlUpdUnd);

                  }

                  else

                  {

                  $addUdUnd = "INSERT INTO alultimodetalletbl(vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, ".
                              "localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                              "SELECT vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                              "claveChofer, observaciones, usuario, ip ".
                              "FROM alhistoricounidadestbl h1 ".
                              "WHERE h1.vin = '".$vin."' ".
                              "AND h1.fechaEvento = (SELECT MAX(hu2.fechaEvento) ".
                                                    "FROM alhistoricounidadestbl hu2 ".
                                                    "WHERE hu2.vin = h1.vin ".
                                                    "AND hu2.claveMovimiento not in (SELECT ge.valor ". 
                                                                                    "FROM cageneralestbl ge ". 
                                                                                    "WHERE ge.tabla = 'alHistoricoUnidadesTbl' ".
                                                                                    "AND ge.columna = 'nvalidos')) ";

                    fn_ejecuta_query($addUdUnd);

                  }
    }

    function acomodaPMP ($vin) {



        $selectUdUnid = "SELECT * from alultimodetalletbl where vin='".$vin."' and claveMovimiento in ('SL','CO','EO','IC','CA') ";
         $rsselectUdUnid= fn_ejecuta_query($selectUdUnid);

         // SI SU UD ES IGUAL A LOS ESTATUS ('SL','CO','IC') VA A ENTRAR A VER LOS PMPS DEL CENTRO DE DISTRIBUCION DEL UD

          if ( sizeof($rsselectUdUnid['root']) > 0) {

                $sqlSelPmp1 = "SELECT * FROM alactividadespmptbl  where vin ='".$vin."' ".
                            "AND centroDistribucion='".$rsselectUdUnid['root'][0]['centroDistribucion']."' ";

                 $rssqlSelPmp1 = fn_ejecuta_query($sqlSelPmp1);


                 // SI ENTRA EN ESTA PARTE REGENERA LOS PMPS POR QUE YA TENIA APLICADOS EN ESTE PATIO Y SI NO TENIA NINGUN REGISTRO SE VA AL ULTIMO ELSE Y LOS GENERA TODOS COMO SI FUERA UNA UNIDAD NUEVA.
                 
                 if (sizeof($rssqlSelPmp1['root']) > 0) {

                   $sqlSelPmp = "SELECT DISTINCT actividad FROM alactividadespmptbl  where vin ='".$vin."' ".
                                "AND centroDistribucion='".$rsselectUdUnid['root'][0]['centroDistribucion']."' ";

                 $rssqlSelPmp = fn_ejecuta_query($sqlSelPmp);

                for ($i=0; $i <sizeof($rssqlSelPmp['root']) ; $i++) { 

                  $actividadesExistentes[] = $rssqlSelPmp['root'][$i]['actividad'];

                        $sqlSel = "SELECT * FROM caactividadespmptbl ca, alactividadespmptbl al ".
                                  "WHERE al.vin ='".$vin."' ".
                                  "AND ca.actividad = al.actividad ".
                                  "AND al.centroDistribucion='".$rsselectUdUnid['root'][0]['centroDistribucion']."' ".
                                  "AND al.actividad ='".$rssqlSelPmp['root'][$i]['actividad']."' ".
                                  "AND al.fechaEvento =(select max(fechaEvento) from alactividadespmptbl where vin='".$vin."' AND centroDistribucion='".$rsselectUdUnid['root'][0]['centroDistribucion']."' AND ACTIVIDAD='".$rssqlSelPmp['root'][$i]['actividad']."') ";

                            $rstSel = fn_ejecuta_query($sqlSel);

                            //////////////////////////////////////////////////////////

                            if ($rstSel['root'][0]['actividad'] == 14) {
                                  
                                }
                                else if ($rstSel['root'][0]['actividad'] == 16) {
                                    $sqlCountAct = "SELECT count(*) as conteo FROM caactividadespmptbl ca, alactividadespmptbl al ".
                                          "WHERE al.vin ='".$rstSel['root'][0]['vin']."' " .
                                          "AND ca.actividad = al.actividad ".
                                          "AND al.centroDistribucion='".$rsselectUdUnid['root'][0]['centroDistribucion']."' ".
                                          "AND al.actividad ='".$rstSel['root'][0]['actividad']."'";
                                    $rsCountAct = fn_ejecuta_query($sqlCountAct);
                                    echo json_encode($rsCountAct);

                                    if ($rsCountAct['root'][0]['conteo'] < '2') {
                                      $sqlCatAct = "SELECT * FROM caactividadespmptbl ".
                                              "WHERE actividad = '".$rstSel['root'][0]['actividad']."'";
                                    $rsCatAct = fn_ejecuta_query($sqlCatAct);
                                    //echo json_encode($rsCatAct);

                                    $fecha = strtotime("+".$rsCatAct['root'][0]['frecuencia']." days", strtotime( $rstSel['root'][0]['fechaEvento'] ) );
                                    $nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );    


                                    $insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
                                             " VALUES ('".$rstSel['root'][0]['centroDistribucion'].
                                               "', '".$rstSel['root'][0]['vin'].
                                               "', '".$rstSel['root'][0]['actividad'].
                                               "', '".$rstSel['root'][0]['frecuencia'].
                                               "', '".$nuevafecha.
                                                "', 'PE".
                                               "', '".$rstSel['root'][0]['importe'].
                                               "', '".$rstSel['root'][0]['usuario']."')";

                                    $rstInsert = fn_ejecuta_query($insActividadesPmp);  
                                    }

                                                
                                }else{
                                  switch ($rstSel['root'][0]['tipo']) {
                                  
                                    case '1':
                                      $fecha = strtotime("+".$rstSel['root'][0]['frecuencia']." days", strtotime( $rstSel['root'][0]['fechaEvento'] ) );
                                      $nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );          


                                      $insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
                                               " VALUES ('".$rstSel['root'][0]['centroDistribucion'].
                                                 "', '".$rstSel['root'][0]['vin'].
                                                 "', '".$rstSel['root'][0]['actividad'].
                                                 "', '".$rstSel['root'][0]['frecuencia'].
                                                 "', '".$nuevafecha.
                                                  "', 'PE".
                                                 "', '".$rstSel['root'][0]['importe'].
                                                 "', '".$rstSel['root'][0]['usuario']."')";

                                      $rstInsert = fn_ejecuta_query($insActividadesPmp);          
                                    break;

                                    case '2':     
                                      $sqlCount = "SELECT count(*) as conteo FROM caactividadespmptbl ca, alactividadespmptbl al ".
                                            "WHERE al.vin ='".$rstSel['root'][0]['vin']."' " .
                                            "AND ca.actividad = al.actividad ".
                                            "AND al.centroDistribucion='".$rsselectUdUnid['root'][0]['centroDistribucion']."' ".
                                            "AND al.actividad ='".$rstSel['root'][0]['actividad']."'";
                                      $rsCount = fn_ejecuta_query($sqlCount);
                                      //echo json_encode($rsCount['root'][0]['conteo']);

                                      if ($rsCount['root'][0]['conteo'] == '1') {

                                        $sqlGen = "SELECT * FROM cageneralestbl ".
                                              "WHERE tabla ='alactividadespmptbl' ".
                                              "AND valor ='".$rstSel['root'][0]['actividad']."'";
                                        $rsGen = fn_ejecuta_query($sqlGen);
                                          //echo json_encode($rsGen); 

                                        $sqlCatAct = "SELECT * FROM caactividadespmptbl ".
                                                "WHERE actividad = '".$rstSel['root'][0]['actividad']."'";
                                        $rsCatAct = fn_ejecuta_query($sqlCatAct);
                                        //echo json_encode($rsCatAct);        

                                        $fecha = strtotime("+".$rsCatAct['root'][0]['frecuencia']." days", strtotime( $rstSel['root'][0]['fechaEvento'] ) );
                                        $nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );  

                                        $fecha1 = strtotime("-".$rsGen['root'][0]['nombre']." days", strtotime( $nuevafecha ) );
                                        $nuevafecha1 = date ( 'Y-m-d H:i:s' , $fecha1 );


                                        $insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
                                                 " VALUES ('".$rstSel['root'][0]['centroDistribucion'].
                                                   "', '".$rstSel['root'][0]['vin'].
                                                   "', '".$rstSel['root'][0]['actividad'].
                                                   "', '".$rsCatAct['root'][0]['frecuencia'].
                                                   "', '".$nuevafecha1.
                                                    "', 'PE".
                                                   "', '".$rstSel['root'][0]['importe'].
                                                   "', '".$rstSel['root'][0]['usuario']."')";               
                                        $rstInsert = fn_ejecuta_query($insActividadesPmp);    
                                      }
                                      else if ($rsCount['root'][0]['conteo'] > '1') {           

                                        $sqlCatAct = "SELECT * FROM caactividadespmptbl ".
                                                "WHERE actividad = '".$rstSel['root'][0]['actividad']."'";
                                        $rsCatAct = fn_ejecuta_query($sqlCatAct);
                                        //echo json_encode($rsCatAct);        

                                        $fecha = strtotime("+".$rsCatAct['root'][0]['frecuencia']." days", strtotime( $rstSel['root'][0]['fechaEvento'] ) );
                                        $nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );            


                                        $insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
                                                 " VALUES ('".$rstSel['root'][0]['centroDistribucion'].
                                                   "', '".$rstSel['root'][0]['vin'].
                                                   "', '".$rstSel['root'][0]['actividad'].
                                                   "', '".$rsCatAct['root'][0]['frecuencia'].
                                                   "', '".$nuevafecha.
                                                    "', 'PE".
                                                   "', '".$rstSel['root'][0]['importe'].
                                                   "', '".$rstSel['root'][0]['usuario']."')";               
                                        $rstInsert = fn_ejecuta_query($insActividadesPmp);    
                                      }         
                                    break;
                                    default:
                                        echo 'No entro';
                                  }
                                }                 

                            //////////////////////////////////////////////////////////////
                
                }

                $usuario= $_SESSION['idUsuario'];

                $actividadesPorComa = implode(",", $actividadesExistentes);

                             $SqlRegistros=" SELECT ud.centroDistribucion, ud.vin, ca.actividad, ge.nombre as frecuencia, ud.fechaEvento, 'PE' as estatus, ca.importe, '".$usuario."' as usuario ".
                              "FROM alHistoricoUnidadesTbl ud, cageneralestbl ge, caactividadespmptbl ca ".
                              "WHERE ud.vin='".$vin."' ".
                              "AND  ud.claveMovimiento in ('SL','CO','EO','IC') ".
                              "AND ud.centroDistribucion =ca.centroDistribucion ".
                              "AND ud.fechaEvento in(SELECT max(h1.fechaEvento) FROM alHistoricoUnidadesTbl h1 WHERE h1.vin=ud.vin and h1.claveMovimiento in ('SL','CO','EO','IC')) ".
                              "AND ge.valor = ca.actividad ".
                              "AND ca.actividad not in (".$actividadesPorComa.") ".
                              "AND ge.tabla = 'alactividadespmptbl'";

                        $rstRegistros= fn_ejecuta_query($SqlRegistros);
                    
                           
                       for ($i=0; $i<sizeof($rstRegistros['root']); $i++) 
                           { 

                          $fecha = strtotime("+".$rstRegistros['root'][$i]['frecuencia']." days", strtotime( $rstRegistros['root'][$i]['fechaEvento'] ) );
                          $nuevafecha = date ( 'Y-m-d H:i:s' , $fecha );        


                          $insActividadesPmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, fechaEvento, claveEstatus, importe, usuario) ".
                                     " VALUES ('".$rstRegistros['root'][$i]['centroDistribucion'].
                                     "', '".$rstRegistros['root'][$i]['vin'].
                                     "', '".$rstRegistros['root'][$i]['actividad'].
                                     "', '".$rstRegistros['root'][$i]['frecuencia'].
                                     "', '".$nuevafecha.
                                      "', '".$rstRegistros['root'][$i]['estatus'].
                                     "', '".$rstRegistros['root'][$i]['importe'].
                                     "', '".$rstRegistros['root'][$i]['usuario']."')";

                          $rstInsert = fn_ejecuta_query($insActividadesPmp);        

                           }


                      $SqlRegistros1=" SELECT ud.centroDistribucion, ud.vin, ca.actividad, ca.frecuencia, ud.fechaEvento, 'PE' as estatus, ca.importe, '".$usuario."' as usuario ".
                              "FROM alHistoricoUnidadesTbl ud, caactividadespmptbl ca ".
                              "WHERE ud.vin='".$vin."' ".
                              "AND  ud.claveMovimiento in ('SL','CO','EO','IC') ".
                              "AND ud.centroDistribucion =ca.centroDistribucion ".
                              "AND ud.fechaEvento in(SELECT max(h1.fechaEvento) FROM alHistoricoUnidadesTbl h1 WHERE h1.vin=ud.vin and h1.claveMovimiento in ('SL','CO','EO','IC')) ".
                              "AND  ca.frecuencia is not null ".
                              "AND ca.actividad != '18' ".
                              "AND ca.actividad not in (".$actividadesPorComa.") ".
                              "AND ca.actividad not in (select valor from cageneralestbl where tabla='alactividadespmptbl')";

                      $rstRegistros1= fn_ejecuta_query($SqlRegistros1);

                      for ($i=0; $i<sizeof($rstRegistros1['root']) ; $i++) 
                      { 

                        $fecha1 = strtotime("+".$rstRegistros1['root'][$i]['frecuencia']." days", strtotime( $rstRegistros1['root'][$i]['fechaEvento'] ) );
                        $nuevafecha1 = date ( 'Y-m-d H:i:s' , $fecha1 );

                        $insActividades1Pmp = "INSERT INTO alactividadespmptbl (centroDistribucion, vin, actividad, frecuencia, "."fechaEvento,claveEstatus, importe, usuario) ".
                                  "VALUES ('".$rstRegistros1['root'][$i]['centroDistribucion'].
                                   "', '".$rstRegistros1['root'][$i]['vin'].
                                   "', '".$rstRegistros1['root'][$i]['actividad'].
                                   "', '".$rstRegistros1['root'][$i]['frecuencia'].
                                   "', '".$nuevafecha1.
                                   "', '".$rstRegistros1['root'][$i]['estatus'].
                                   "', '".$rstRegistros1['root'][$i]['importe'].
                                   "', '".$rstRegistros1['root'][$i]['usuario']."') ";

                        fn_ejecuta_query($insActividades1Pmp);

                        }
                  }

    // EN ESTE ELSE LOS GENERA SI ES UNA UNIDAD NUEVA
     else {

          $usuario = $_SESSION['idUsuario'];
          
         insertActividadesPmp($_REQUEST['modUnidadVinCmd'],$usuario);

        }
      }

      // ENTRA EN ESTE ELSE SI LA UNIDAD NO CUENTA CON ('SL','CO','IC') Y SI CUENTA CON LOS QUE ELIMINAN LOS PMPS LO HACE PARA ESOS ESTATUS DE UD QUE SERIAN EN COMODATO TODAS LAS SALIDAS MAS EL AM Y LAS EP PARA LOS DEMAS PATIOS.
      else {

                $selectUdUnid = "SELECT * from alultimodetalletbl where vin='".$vin."' and claveMovimiento in (SELECT valor 
                                FROM cageneralestbl 
                                WHERE tabla = 'alHistoricoUnidadesTbl' 
                                AND COLUMNA='CLAVEMOVIMIENTO'
                                AND IDIOMA = 'CMDAT' 
                                UNION
                                select DISTINCT valor
                                from cageneralestbl
                                where valor in ('EP','AM'))"; 

               $rsselectUdUnid= fn_ejecuta_query($selectUdUnid);


               if ( sizeof($rsselectUdUnid['root']) > 0)


               {

                $updPmp="UPDATE alactividadespmptbl SET claveEstatus='AP' WHERE centroDistribucion ='".$rsselectUdUnid['root'][0]['centroDistribucion']."' and vin='".$_REQUEST['modUnidadVinCmd']."' AND fechaEvento <= '".$rsselectUdUnid[$i]["fechaEvento"]."' ";
                fn_ejecuta_query($updPmp);

                $delPmp = "DELETE FROM alactividadespmptbl WHERE centroDistribucion ='".$rsselectUdUnid['root'][0]['centroDistribucion']."' and vin='".$_REQUEST['modUnidadVinCmd']."' AND claveEstatus ='PE'";
                fn_ejecuta_query($delPmp);

               }

      }

    }

?>