<?php
    session_start();
	$_SESSION['modulo'] = "catCompanias";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['cargaTicketHdn']){
        case 'getTractor':
                getTractor();
            break;
        case 'getCargaTicket':
            getCargaTicket();
            break;
        case 'getViajeTractor':
            getViajeTractor();
            break;
        default:
            echo 'no esta entrando a ningun proceso';
    }

    //$_REQUEST = trasformUppercase($_REQUEST);

    function getTractor(){

        $sqlGetTractores = "SELECT ch.tractor, vt.viaje, ch.idTractor ".
                            "FROM catractorestbl ch,trviajestractorestbl vt ".
                            "WHERE ch.idTractor = vt.idTractor ".
                            "AND vt.claveMovimiento = 'VU' ".
                            "AND ch.compania = '".$_REQUEST['cargaTicketCompaniaCmb']."';";

        $rsTractores = fn_ejecuta_query($sqlGetTractores);

        echo json_encode($rsTractores);
    }

    function getViajeTractor(){
        $sqlGetViaje = "SELECT ch.tractor, vt.viaje, ch.idTractor, vt.idViajeTractor ".
                        "FROM catractorestbl ch,trviajestractorestbl vt ".
                        "WHERE ch.idTractor = vt.idTractor ".
                        "AND vt.claveMovimiento = 'VU' ".
                        "AND ch.idTractor = '".$_REQUEST['cargaTicketractorCmb']."';";

        $rsViajes = fn_ejecuta_query($sqlGetViaje);

        echo json_encode($rsViajes);
    }


    function getCargaTicket(){
        $a = array();
        $a['successTitle'] = "Manejador de Archivos";
        $a['successMessage'] = "Carga efectuada.";

        if(isset($_FILES)) {
            if($_FILES["cargaTicketArchivoFld"]["error"] > 0){
                $a['success'] = false;
                $a['message'] = $_FILES["cargaTicketArchivoFld"]["error"];

            } else {
                $temp_file_name = $_FILES['cargaTicketArchivoFld']['tmp_name'];
                $original_file_name = $_FILES['cargaTicketArchivoFld']['name'];

                // Find file extention
                $ext = explode ('.', $original_file_name);
                $ext = $ext [count ($ext) - 1];

                // Remove the extention from the original file name
                $file_name = str_replace ($ext, '', $original_file_name);

                $new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
                
                //$temp_file_name = '../../tmp/TicketCard24-02-20.xlsx';											//Sólo para probar por fuera
                //$new_name = '../../respaldos/TicketCard24-02-20.xlsx';											//Sólo para probar por fuera

                if (move_uploaded_file($temp_file_name, $new_name)){

                    if (!file_exists($new_name)){
                        $a['success'] = false;
                        $a['errorMessage'] = "Error al procesar el archivo " . $new_name;
                    } else {
                        $a['success'] = true;
                        $a['archivo'] = $file_name . $ext;
                        $a['root'] = leerXLS($new_name);
                    }
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
                }
            }
        } else {
            $a['success'] = false;
            $a['errorMessage'] = "Error FILES NOT SET";
        }

        echo json_encode($a);
    }

function leerXLS($inputFileName) {
        /** Error reporting */
        error_reporting(E_ERROR);		//CHK

        date_default_timezone_set ("America/Mexico_City");

        //  Include PHPExcel_IOFactory
        include '../funciones/Classes/PHPExcel/IOFactory.php';

        //$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
        //echo $inputFileName;
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader =
             PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
        $rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

        //-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

        if (ord(strtoupper($highestColumn)) <= 67 && $highestRow <= 10 ) {
            $root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
            return $root;
        }if (ord(strtoupper($highestColumn)) < 2) {
            $root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
            return $root;
        }if (strlen($rowData[0][0]) < 1) {
            $root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [No.Comprobante-fechaEvento-litros-importe,placas]', 'fail'=>'Y');
            return $root;
        }
        //------------------------------------------------------------------------------------------------


        $isValidArr = array(    'compania' =>array('val'=>'','size'=>true,'exist'=>true),
                                'tractor' =>array('val'=>'','size'=>true,'exist'=>true),
                                'noComprobante' =>array('val'=>'','size'=>true,'exist'=>true),
                                'fechaEvento'=>array('val'=>'','size'=>true,'exist'=>true),
                                'litros' =>array('val'=>'','size'=>true,'exist'=>true),
                                'importe' =>array('val'=>'','size'=>true,'exist'=>true),
                                'placas' =>array('val'=>'','size'=>true,'exist'=>true),
                                'aprobado' =>array('val'=>'','size'=>true,'exist'=>true));

        for($row = 0; $row<sizeof($rowData); $row++){
            //Validar si las unidades estan bien o no
            //Verifico si la longitud de loas campos sea la correcta...
            $isValidArr['compania']['val'] = $rowData[$row][0];
            $isValidArr['compania']['size'] = strlen($rowData[$row][0]) == 2;
            $isValidArr['tractor']['val'] = $rowData[$row][1];
            $isValidArr['noComprobante']['val'] = $rowData[$row][2];
            //$isValidArr['noComprobante']['size'] = strlen($rowData[$row][0]) == 17;
            $isValidArr['fechaEvento']['val'] = $rowData[$row][3];
            //$isValidArr['fechaEvento']['size'] = strlen($rowData[$row][1]) > 0;
            $isValidArr['litros']['val'] = $rowData[$row][4];
            $isValidArr['importe']['val'] = $rowData[$row][5];
            $isValidArr['placas']['val'] = $rowData[$row][6];
            $isValidArr['aprobado']['val'] = $rowData[$row][7];
            //cargaTicketIdViajeHdn
            //$isValidArr['litros']['size'] = strlen($rowData[$row][2]) == 5;

				    $sql = "SELECT idTractor FROM caTractoresTbl WHERE compania = '".$rowData[$row][0]."' AND tractor = '".$rowData[$row][1]."'";
				    //echo "$sql<br>";
				    $rsTractorAux = fn_ejecuta_query($sql);            
				      
				    $idTractor = ($rsTractorAux['records']==0)?0:$rsTractorAux['root'][0]['idTractor'];

            $errorMsg = '';
            $isTrue = true;



            foreach ($isValidArr as $key => $value) {
                /*if ($_REQUEST['cargaTicketIdViajeHdn']== NULL) { //Si no es del tamaño correcto
                    $errorMsg .='El '.$key.' no existe el viaje del tractor correcto|';
                    $isTrue = false;
                }*/if($isValidArr['compania']['size'] != 2) {
                    $errorMsg .='La Compania Tractor no tiene la Misma Longitud';
                    $isTrue = false;
                }/*if($rowData[$row][4] == null) {
                    $errorMsg .='El '.$key.' Tipo de Mercado no existe para esta Unidad';
                    echo json_encode(" mercado ".$rowData[$row][4]);
                    $isTrue = false;
                }if($rowData[$row][5] == null) {
                    $errorMsg .='El '.$key.' Nombre del Barco no existe para esta Unidad';
                    echo json_encode(" brco ".$rowData[$row][5]);
                    $isTrue = false;
                }*/
            }

            if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
                //Busco el idTarifa

                //Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
                //$_SESSION['usuCompania'] = 'LZC02';

                $sqlGetViaje = "SELECT ch.tractor, vt.viaje, ch.idTractor, vt.idViajeTractor ".
                                "FROM catractorestbl ch,trviajestractorestbl vt ".
                                "WHERE ch.idTractor = vt.idTractor ".
                                "AND vt.claveMovimiento = 'VU' ".
                                "AND ch.compania = '".$rowData[$row][0]."' ".
                                "AND ch.tractor = '".$rowData[$row][1]."' ";
								//echo "$sqlGetViaje<br>";
                #Se desactiva la búsqueda
                //$rsViaje = fn_ejecuta_query($sqlGetViaje);
                
                $today = date("Y-m-d H:i:s");
                $fecha = substr($today,0,10);
                $hora=substr($today,11,8);

                $numComprobante = $rowData[$row][2];
                $numfechaEvento = $rowData[$row][3];
                $numlitros = $rowData[$row][4];
                $numImporte = $rowData[$row][5];
                $numPlacas = $rowData[$row][6];

                $sql = "SELECT COUNT(*) AS totalReg FROM trPolizasTcketTbl".
                		   " WHERE noComprobante = ".$numComprobante.
                		   " AND fechaEvento = '".$numfechaEvento."'".
                		   " AND litros = ".$numlitros.
                		   " AND importe = ".$numImporte.
                		   " AND placas = '".$numPlacas."'";
                //echo "$sql<br>";
                #Se desactiva la búsqueda
                $rsTicket = fn_ejecuta_query($sql);
                $existeReg = (empty($rsTicket['root'][0]['totalReg']))?0:$rsTicket['root'][0]['totalReg'];

                /*$numNomFac = $rowData[$row][3];
                $numDirEnt = $rowData[$row][4];
                $numFped = $rowData[$row][5];*/                                
                
                //if(sizeof($rsViaje) > 1){
                //Sólo inserta los registros que no existan
                if(!$existeReg){
                    if($rowData[$row][7] ==='APROBADA'){                        
                        $addTicketCard = "INSERT INTO trPolizasTcketTbl (idTractor,noComprobante,fechaevento,litros,importe,placas,estatus) VALUES(".
			                            				$idTractor.", ".
					                                $numComprobante.", ".
					                                "'".$numfechaEvento."', ".
					                                $numlitros.", ".
					                                $numImporte.", ".
					                                "'".$numPlacas."', ".
					                                "'1') ";
                        fn_ejecuta_query($addTicketCard);

                        if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                            $errorMsg = 'Agregado Correctamente';
                        }else {
                            $errorMsg = 'Registro No Agregado';
                        }
                    }else{
                        $errorMsg = 'Registro No Agregado, no esta APROBADO el Ticket '.$numComprobante;
                    }                                    
                }else{
                    $errorMsg = 'Registro ya existente.';
                }

                /*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCompania'],'PR',
                          $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCompania'], $repuve, $chofer, $observaciones,$rowData[$row][4]);*/
            }
            $root[]= array('compania'=>$rowData[$row][0],'tractor'=>$rowData[$row][1],'noComprobante'=>$rowData[$row][2],'fechaEvento'=>$rowData[$row][3], 'litros'=>$rowData[$row][4],'importe'=>$rowData[$row][5],'placas'=>$rowData[$row][6],'nose'=>$errorMsg);

            //echo json_encode($rowData[$row]);
        }
        return $root;
    }


    function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
        $tabla = 'tabla';
        $columna = 'columna';
        switch(strtoupper($field)) {
            case 'noComprobante':
                $tabla = 'alInstruccionesMercedesTbl'; $columna = 'noComprobante'; break;
            case 'fechaEvento':
                $tabla = 'cafechaEventosUnidadesTbl'; $columna = 'fechaEventoUnidad'; break;
            case 'litros':
                $tabla = 'calitrosesCentrosTbl'; $columna = 'litrosCentro'; break;
        }
        $sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
        $rs = fn_ejecuta_query($sqlExist);
        return strtoupper($field) == 'noComprobante' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
    }
?>
