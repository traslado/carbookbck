<?php
	require_once("../funciones/fpdf/pdf_js.php");
	//require_once("../funciones/fpdf/fpdf.php");
	require_once("../funciones/barcode.php");
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	//require_once("../funciones/Barcode39.php");
	


	/*********************************************
	Sólo necesita alUnidadesVinHdn para funcionar
	*********************************************/


	class PDF_AutoPrint extends PDF_JavaScript
	{
		function AutoPrint($dialog=false,$server='10.1.2.232', $printer= 'B-EX4T1')
		{
			//Open the print dialog or start printing immediately on the standard printer
			$param=($dialog ? 'true' : 'false');
			$script="print($param);";
			$this->IncludeJS($script);
			$script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
			$script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
			$script .= "print(pp);";
			$this->IncludeJS($script);
		}

		function AutoPrintToPrinter($server='10.1.2.232', $printer= 'B-EX4T1', $dialog=false)
		{
			//Print on a shared printer (requires at least Acrobat 6)
			$script = "var pp = getPrintParams();";
			if($dialog){
				$script .= "pp.interactive = pp.constants.interactionLevel.full;";	
			}				
			else{
				$script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
				$script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
				$script .= "print(pp);";
				$this->IncludeJS($script);	
			}
		}
	}



	$success = true;

	$vinArr = explode('|', substr($_REQUEST['alUnidadesVinHdn'], 0, -1));

	$pdf = new FPDF('P', 'mm', array(100, 156));

	for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) { 
		if($vinArr[$nInt] != ""){
			$sqlGetDataStr = "SELECT h.vin, h.distribuidor, lp.fila, lp.lugar, h.claveMovimiento, dc.tipoDistribuidor, ".
									 "dc.observaciones AS puerto, dc.descripcionCentro , p.pais, h.localizacionUnidad AS patio, h.fechaEvento, pl.plaza ".
									 "FROM caDistribuidoresCentrosTbl dc, caDireccionesTbl di, caColoniasTbl co, ".
									 "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p, caPlazasTbl pl, alHistoricoUnidadesTbl h ".
									 "LEFT JOIN allocalizacionpatiostbl lp ON lp.vin = h.vin ".
									 "WHERE dc.distribuidorCentro = h.distribuidor ".
									 "AND pl.idPlaza = dc.idPlaza ".
									 "AND di.direccion = dc.direccionEntrega ".
									 "AND co.idColonia = di.idColonia ".
									 "AND m.idMunicipio = co.idMunicipio ".
									 "AND e.idEstado = m.idEstado ".
									 "AND p.idPais = e.idPais ".
									 "AND h.vin = '".$vinArr[$nInt]."' ".
									 "AND fechaEvento = (SELECT MAX(h1.fechaEvento) ".
									 "FROM alHistoricoUnidadesTbl h1 ". 
									 "WHERE h1.vin = h.vin AND (h1.claveMovimiento = 'EP' OR h1.claveMovimiento = 'PR')) ";

			$rs = fn_ejecuta_query($sqlGetDataStr);
			$data = $rs['root'][0];
			if (sizeof($data) > 0) {
				$pdf = generarPdf($data, $pdf, $nInt); 
			}
		}
	}

	
	$pdf->AutoPrint(false);
	$pdf->Output('../../codeBar/archivo.pdf','I');
	$pdf->Output('../../codeBar/archivo.pdf','F');

	function generarPdf($data, $pdf, $n){
		//Solo para pruebas, por defecto 0
		$border = 0;
		if($data['tipoDistribuidor'] == 'DX') {
			$paisPlaza = substr($data['pais'],0,6);
			$puertoNombre = substr($data['puerto'], 0, 9);
		} else {
			$paisPlaza = substr($data['plaza'],0,6);
			$puertoNombre = substr($data['descripcionCentro'],0, 9);
		}

		if(sizeof($data) > 0){
			$pdf= new PDF_AutoPrint();
			$pdf->AddPage('');
			$pdf->AddFont('skyline', '', 'skyline-reg.php');
			//$pdf ->MakeFont('c:\\windows\\fonts\\comic.ttf','comic.afm','cp1252');
			//$pdf->SetMargins(0.2, 0.2, 0.2,0.2);
			/*if ($n > 0) {
				$pdf->SetY(10);
				$pdf->SetX(10);
			}*/

			//Código Distribuidor y País
			$pdf->SetY(6);
			$pdf->SetX(4);
			$pdf->SetFont('Arial','B',15);
			$pdf->Cell(20,7,$data['distribuidor'], 0,0, 'L');  
			
			$pdf->SetY(15);
			$pdf->SetX(4);
			$pdf->SetFont('skyline','',40);
			$pdf->Cell(25,13,$paisPlaza, 0,0, 'L');
			
			//Patio, Fila, Cajon
			$pdf->SetFont('Arial','B',8);
			$pdf->SetY(6);		
			$pdf->SetX(42);
			$pdf->Cell(33, 5, 'Patio: '.$data['patio'],0,1,'L');

			$pdf->SetY(13);
			$pdf->SetX(42);
			$pdf->Cell(33, 5, 'Fila: '.$data['fila'],0,1,'L');

			$pdf->SetY(20);
			$pdf->SetX(42);
			$pdf->Cell(33, 5, 'Cajon: '.$data['lugar'],0,1,'L');

			//Puerto
			$pdf->setY(35);
			$pdf->SetX(1);
			$pdf->SetFont('skyline','',75);
			$pdf->Cell(70,30,$puertoNombre, 0,1, 'C');

			//Fecha y Hora
			if (!isset($data['fechaEvento'])) {
				$data['fechaEvento'] = date("Y-m-d H:i:s");
			}

			$pdf->SetFont('Arial','B',13);
			$pdf->setY(70);
			$pdf->SetX(12);
			$pdf->Cell(50,5,$data['fechaEvento'], 0,1, 'C'); 

			//Avanzada
			$pdf->SetFont('skyline','',61);
			$pdf->setY(80);
			$pdf->SetX(19);
			$pdf->Cell(40,23,substr($data['vin'], -8), 0,1, 'C'); 

			$vin=$data['vin'];

			//****************
			//CODIGO DE BARRAS
			//****************

			$fontSize = 100; // GD1 in px ; GD2 in point
			$marge = 10; // between barcode and hri in pixel
			$x = 400;  // barcode center x
			$y = 30;  // barcode center y
			$height = 180;  // barcode height
			$width = 4;  // barcode width
			$angle = 0; // rotation in degrees 
			$code = $data['vin']; // vin code for barcode
			$type = 'code128'; // barcode type
			$imageTitle = $code.'_barcodeTemp.gif';

			//Se crea la imagen para usar para el codigo de barras
			$im = imagecreatetruecolor(800, 120);
			$black = imagecolorallocate($im, 0x00, 0x00, 0x00);
			$white = imagecolorallocate($im,0xff,0xff,0xff);
			imagefilledrectangle($im, 0, 0, 800, 120, $white);
			//imagestring($im, 5, 335, 100, "*".$data['vin']."*", imagecolorallocate($im, 0, 0, 0));

			//Se genera el Codigo de barras
			$data = Barcode::gd($im, $black, $x, $y, $angle, $type,   array('code'=>$code), $width, $height);
			Header('Content-type: image/gif');
			imagegif($im, $imageTitle);

			//Código de Barras y vin

			$pdf->Image($imageTitle,10,110, 50);

			//VIN
			$pdf->SetFont('Arial','',10);
			$pdf->setY(122);
			$pdf->SetX(15);
			$pdf->Cell(40,5,"*".$vin."*", 0,1, 'C'); 			
			
			return $pdf;		
			if (file_exists($imageTitle)) {
				unlink($imageTitle);
			}
			
		} else {
			echo json_encode(array('success'=>false, 'errorMessage'=>$_SESSION['error_sql']." <br> ".$sqlGetDataStr));
		}		
	}
?>