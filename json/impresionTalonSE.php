<?php
    setlocale(LC_TIME, 'es_MX.utf8');
    $codigoTalonEspecial = 'TE';

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    
    if($_REQUEST['esTractor12Hdn'] != ""){
        imprimirTalon12($_REQUEST['trViajesTractoresIdViajeHdn']);
    } else{
        imprimirTalon();
    }
    

    function imprimirTalon(){
        $a = array();
        $e = array();
        $a['success'] = true;
        global $codigoTalonEspecial;

        if($_REQUEST['trViajesTractoresIdViajeHdn'] != ""){
          
            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                     "AND tv.claveMovimiento != 'TX' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].")"; 
        } else {          
            if ($_REQUEST['trViajesTractoresFoliosHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresFoliosHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }if ($_REQUEST['trViajesTractoresCompaniaHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresCompaniaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].") ";
                                     
        }

        if ($a['success']) {

          $pdf = new FPDF('L', 'mm','A4'); 


          $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);
          
        //  echo json_encode($sqlGetTalonesViajeStr);
          
          $varInc = 0;     

          //echo json_encode($rsTalon);
          
          for ($nInt=0 ; $nInt < sizeof($rsTalon['root']) ; $nInt++) {   

                $sqlGetTalonStr =  "SELECT   tv.*, tv.companiaRemitente AS nombreCiaRemitente, ".
                                    "dc.direccionFiscal AS dirCompania, dc.descripcionCentro, tr.tractor, ".
                                    "vt.claveChofer, ch.*, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, ".
                                    "kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist,(SELECT sum(su.pesoAproximado) ".
                                    "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                    "WHERE un.vin = ut.vin AND su.simboloUnidad = ".
                                    "un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                                    "(SELECT pl2.plaza FROM ".
                                    "caPlazasTbl pl2 WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".   
                                    "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                                    "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".
                                    "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caChoferesTbl ch, caDistribuidoresCentrosTbl dc, ".
                                    "caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, caMunicipiosTbl mu, ".
                                    "caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
                                    "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                    "AND dc.distribuidorCentro = tv.distribuidor  ".
                                    //"AND dc.distribuidorCentro = tv.companiaRemitente ".
                                    "AND tr.idTractor = vt.idTractor ".
                                    "AND ch.claveChofer = vt.claveChofer ".
                                    "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                                    "AND col.idColonia = di.idColonia ".
                                    "AND mu.idMunicipio = col.idMunicipio ". 
                                    "AND es.idEstado = mu.idEstado ".
                                    "AND pa.idPais = es.idPais ".
                                    "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                                    "AND kp.idPlazaDestino = tv.idPlazaDestino ". 
                                    "AND tv.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." ".
                                    "AND tv.claveMovimiento != 'TX' limit 1 " ;



            $dataTalon = fn_ejecuta_query($sqlGetTalonStr);
                //echo json_encode($sqlGetTalonStr);

            $selectCompaniaRemit="SELECT direccionEntrega as dirCompania, descripcionCentro as descripcion, rfc as rfcRemitente from caDistribuidoresCentrosTbl where distribuidorCentro='".$dataTalon['root'][0]['nombreCiaRemitente']."' ";

            $dataCompania = fn_ejecuta_query($selectCompaniaRemit);

            if(sizeof($dataTalon['root']) > 0){

              if($dataCompania['root'][0]['dirCompania'] != ''){

                $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                              "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                              "caEstadosTbl es, caPaisesTbl pa ".
                                              "WHERE pa.idPais = es.idPais ".
                                              "AND es.idEstado = mu.idEstado ".
                                              "AND mu.idMunicipio = co.idMunicipio ".
                                              "AND co.idColonia = dir.idColonia ".
                                              "AND dir.direccion = ".$dataCompania['root'][0]['dirCompania'];

                $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio'];
                $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['cp']." ".$dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
              }

              $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ".
                                        "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                        "WHERE un.vin = ut.vin ".
                                        "AND su.simboloUnidad = un.simboloUnidad ".
                                        "AND ut.idTalon =".$rsTalon['root'][$varInc]['idTalon']." ".
                                        "AND ut.estatus != 'C' ";

              $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);


              if(sizeof($unidadesTalon['root']) > 0){
                
                $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                $dataTalon['root'][0]['apellidoPaterno']." ".
                $dataTalon['root'][0]['apellidoMaterno'];     
                $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];
                $dataTalon['root'][0]['descPlazaOrigen'];  


              if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                $sqlGetServEspStr = "SELECT de.distribuidorOrigen AS distribuidor, dc.descripcionCentro, de.direccionOrigen AS direccion, ".
                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                    "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                    "WHERE dir.direccion = de.direccionOrigen ".
                                    "AND dc.distribuidorCentro = de.distribuidorOrigen ".
                                    "AND dir.idColonia = co.idColonia ".
                                    "AND co.idMunicipio = mu.idMunicipio ".
                                    "AND mu.idEstado = es.idEstado ".
                                    "ANd es.idPais = pa.idPais ".
                                    "AND pl.idPlaza = dc.idPlaza ".
                                    "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                    "AND de.idDestinoEspecial=(SELECT max(es.idDestinoEspecial) from aldestinosespecialestbl es where de.vin=es.vin) ".
                                    "UNION ".
                                    "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                    "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                    "WHERE dir.direccion = de.direccionDestino ".
                                    "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                    "AND dir.idColonia = co.idColonia ".
                                    "AND co.idMunicipio = mu.idMunicipio ".
                                    "AND mu.idEstado = es.idEstado ".
                                    "ANd es.idPais = pa.idPais ".
                                    "AND pl.idPlaza = dc.idPlaza ".
                                    "AND de.idDestinoEspecial=(SELECT max(es.idDestinoEspecial) from aldestinosespecialestbl es where de.vin=es.vin) ".
                                    "AND de.vin = '".$unidadesTalon['root'][0]['vin']."'";
                
                $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);
                //echo json_encode($sqlGetServEspStr);

                for ($nInt01=0; $nInt01 < sizeof($dataServEsp['root']); $nInt01++) {
                  
                  $dataServEsp['root'][$nInt01]['descDist'] = $dataServEsp['root'][$nInt01]['distribuidor']." - ".
                  $dataServEsp['root'][$nInt01]['descripcionCentro'];

                  $dataServEsp['root'][$nInt01]['dirL1'] = $dataServEsp['root'][$nInt01]['calleNumero'];
                  $dataServEsp['root'][$nInt01]['dirL2'] = $dataServEsp['root'][$nInt01]['colonia'].", ".$dataServEsp['root'][$nInt01]['municipio'];
                  $dataServEsp['root'][$nInt01]['dirL3'] = $dataServEsp['root'][$nInt01]['cp']." ".$dataServEsp['root'][$nInt01]['estado'].", ".$dataServEsp['root'][$nInt01]['pais'];
                }
              }        
                generarPdf($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0],$dataCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']); 

              
            } else {
              $pdf->AddPage();
              $pdf->SetY(92+(4*$nInt)+0);
              $pdf->SetX(25+0);
              $pdf->Cell(17,3, 'TALON #'.$rsTalon['root'][$varInc]['idTalon']. ' SIN UNIDADES',0, 'C');
            }
          }                       
            $varInc ++ ;           
        }
      }
        //Output
        $pdf->Output('../../talon.pdf', I);
    }
    
    function generarPdf($pdf, $fondo, $data, $dirCompania,$dataCompania, $unidades, $especiales){

        global $codigoTalonEspecial;
        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;

        $pdf->AddPage();
        $pdf->SetFont($font,'B',10);
        $pdf->SetAutoPageBreak(false);

        //FOLIO DEL TALON
        $pdf->SetY(27+$offsetY);
        $pdf->SetX(180+$offsetX);
        $pdf->Cell(40,5, $data['centroDistribucion']." ".$data['folio'], $border,0, 'L'); 

        ///////Fecha
        date_default_timezone_set('America/Mexico_City');        
        $fecha1=substr($data['fechaEvento'],0,10);
        //$fecha = setlocale(LC_TIME,"spanish");
        $fecha = explode('-',$fecha1);        

        setlocale(LC_TIME, 'spanish');  
        $nombre=strftime("%B",mktime(0, 0, 0, $fecha[1], 1, 2000)); 
        //echo $nombre;       

        $pdf->SetY(35+$offsetY);
        //Dia
        $pdf->SetX(119+$offsetX);
        $pdf->Cell(6,5, $fecha[2], $border,0, 'C'); 
        //Mes
        $pdf->SetX(140+$offsetX);
        $pdf->Cell(20,5, strtoupper($nombre), $border,0, 'C');
        //Año
        $pdf->SetX(187+$offsetX);
        $pdf->Cell(10,5, $fecha[0], $border,0, 'C');
        ////////////

        /////////Origen
        //Cod y Nombre Origen
        $pdf->SetY(42+$offsetY);
        $pdf->SetX(31+$offsetX);
        $pdf->Cell(87,3, $data['descPlazaOrigen'], $border,0, 'L');
        //Remitente
        $pdf->SetY(47+$offsetY);
        $pdf->SetX(31+$offsetX);
        $pdf->Cell(83,3, $data['nombreCiaRemitente']." - ".$dataCompania['descripcion'], $border,0, 'L');
        //RFC
        $pdf->SetY(50+$offsetY);
        $pdf->SetX(31+$offsetX);
        $pdf->Cell(30,3, $dataCompania['rfcRemitente'], $border,0, 'L');
        //DireccionL1
        $pdf->SetY(53+$offsetY); 
        $pdf->SetX(31+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL1Cia'], $border,0, 'L');
        //DireccionL2
        $pdf->SetY(56+$offsetY);
        $pdf->SetX(31+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL2Cia'], $border,0, 'L');
        //DireccionL3
        $pdf->SetY(59+$offsetY);
        $pdf->SetX(31+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL3Cia'], $border,0, 'L');

        ////////Destino
        //Cod y Nombre Destino
        $pdf->SetY(62+$offsetY);
        $pdf->SetX(131+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[1]['plaza'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['descPlazaDestino'], $border,0, 'L');
        }
        //Destinatario
        $pdf->SetY(47+$offsetY);
        $pdf->SetX(131+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(79,3, substr($especiales[1]['descDist'],0,30), $border,0, 'L');
            //$pdf->Cell(79,3, substr($especiales[1]['descDist'],30,30), $border,0, 'L');
        } else {
            $pdf->Cell(79,3, $data['descDistribuidor'], $border,0, 'L');
        }
        //RFC
        $pdf->SetY(50+$offsetY);
        $pdf->SetX(131+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(30,3, $especiales[1]['rfc'], $border,0, 'L');
        } else {
            $pdf->Cell(30,3, $data['rfcDestinatario'], $border,0, 'L');
        }
        //DireccionL1
        $pdf->SetY(53+$offsetY);
        $pdf->SetX(131+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[1]['dirL1'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['dirL1Dest'], $border,0, 'L');
        }
        $pdf->SetY(56+$offsetY);
        $pdf->SetX(131+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[1]['dirL2'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['dirL2Dest'], $border,0, 'L');
        }
        //DireccionL3
        $pdf->SetY(59+$offsetY);
        $pdf->SetX(131+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[1]['dirL3'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['dirL3Dest'], $border,0, 'L');
        }

        //Se recogera
        //checar este debe
        $pdf->SetY(50+$offsetY);
        $pdf->SetX(5+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            //$pdf->Cell(93,3, $especiales[0]['descDist'], $border, 0, 'C');
            $pdf->SetY(70+$offsetY);
            $pdf->SetX(17+$offsetX);
            $pdf->Cell(93,3, "REC. EN:".$especiales[0]['dirL1'], $border, 0, 'C');
            $pdf->SetY(73+$offsetY);
            $pdf->SetX(27+$offsetX);
            $pdf->Cell(93,3, $especiales[0]['dirL2'], $border,0, 'C');
            $pdf->SetY(76+$offsetY);
            $pdf->SetX(9+$offsetX);
            $pdf->Cell(93,3, $especiales[0]['dirL3'], $border,0, 'C');

        } else {
            $pdf->Cell(93,3,"EL MISMO", $border, 0, 'C');
        }
        //Se entregara
        $pdf->SetY(70+$offsetY);
        $pdf->SetX(117+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){

            $pdf->Cell(93,3, $especiales[1]['dirL1'], $border, 0, 'C');
        } else {
            $pdf->Cell(93,3,"EL MISMO", $border, 0, 'C');
        }

        //Valor Unitario
        $pdf->SetY(83+$offsetY);
        $pdf->SetX(18+$offsetX);
        $pdf->Cell(45,3,"0.00", $border, 0, 'C');
        if($data['tipoTalon'] !== $codigoTalonEspecial){
            //$pdf->Cell(45,3,"0.00", $border, 0, 'C');
        }
        //Valor Declarado
        $pdf->SetY(80+$offsetY);
        $pdf->SetX(88+$offsetX);
        $pdf->Cell(45,3,"N/D", $border, 0, 'C');
        if($data['tipoTalon'] !== $codigoTalonEspecial){
            //$pdf->Cell(45,3,"N/D", $border, 0, 'C');
        }
        //Condiciones de Pago
        $pdf->SetY(80+$offsetY);
        $pdf->SetX(165+$offsetX);
        $pdf->Cell(45,3,"CREDITO", $border, 0, 'C');
        if($data['tipoTalon'] !== $codigoTalonEspecial){
            //$pdf->Cell(45,3,"CREDITO", $border, 0, 'C');
        }

        ///////Unidades
        for ($nInt=0; $nInt < sizeof($unidades); $nInt++) { 
            //Simbolo
            $pdf->SetY(95+(3*$nInt)+$offsetY);
            $pdf->SetX(33+$offsetX);
            $pdf->Cell(17,3, $unidades[$nInt]['simboloUnidad'], $border,0, 'C');
            //vin
            $pdf->SetY(95+(3*$nInt)+$offsetY);
            $pdf->SetX(57+$offsetX);
            $pdf->Cell(22,3, $unidades[$nInt]['vin'], $border,0, 'C');
            //Descripcion Simbolo
            $pdf->SetY(95+(3*$nInt)+$offsetY);
            $pdf->SetX(90+$offsetX);
            $pdf->Cell(72,3, $unidades[$nInt]['descSimbolo'], $border,0, 'L');

            //Peso Aproximado
            /*$pdf->SetY(101+(3*$nInt)+$offsetY);
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(72,3, $unidades[$nInt]['pesoAprox'], $border,0, 'L'); */        

            

            if ($unidades[$nInt]['pesoAprox']== '0') {
              $peso = '0999';              
            }else{
              $peso = $unidades[$nInt]['pesoAprox'];                           
              $peso1[] = $unidades[$nInt]['pesoAprox'];                         
            }        

            $pesoTotal= array_sum($peso1);             
 

            $pdf->SetY(95+(3*$nInt)+$offsetY);
            $pdf->SetX(145+$offsetX);
            $pdf->Cell(72,3, $peso, $border,0, 'L'); 
        }

        //PESO APROXIMADO 
        /*$pdf->SetY(156+(4*$nInt)+$offsetY);
        $pdf->SetX(107+$offsetX);
        $pdf->Cell(72,3,"PESO APROXIMADO: ... ",$border,0, 'L'); */

        $pdf->SetY(156+$offsetY);
        $pdf->SetX(107+$offsetX);
        $pdf->Cell(72,3,"PESO APROXIMADO: ... ",$border,0, 'L'); 

        ////////Importes
        //Flete
        $pdf->SetY(105+$offsetY);
        $pdf->SetX(174+$offsetX);
        $pdf->Cell(26,3,"$.00", $border, 0, 'R');
        //Cargo por Seguro
        $pdf->SetY(120+$offsetY);
        $pdf->SetX(174+$offsetX);
        $pdf->Cell(26,3,"$.00", $border, 0, 'R');
        //SubTotal
        $pdf->SetY(129+$offsetY);
        $pdf->SetX(174+$offsetX);
        $pdf->Cell(26,3,"$.00", $border, 0, 'R');
        //Retencion IVA
        $pdf->SetY(143+$offsetY);
        $pdf->SetX(174+$offsetX);
        $pdf->Cell(26,3,"$.00", $border, 0, 'R');

        /////////Chofer
        //Clave
        $pdf->SetY(164+$offsetY);
        $pdf->SetX(28+$offsetX);
        $pdf->Cell(10,3,$data['claveChofer'], $border, 0, 'L');
        //Nombre
        $pdf->SetY(164+$offsetY);
        $pdf->SetX(45+$offsetX);
        $pdf->Cell(95,3,$data['nombreChofer'], $border, 0, 'C');
        //Tractor
        $pdf->SetY(164+$offsetY);
        $pdf->SetX(124+$offsetX);
        $pdf->Cell(20,3,$data['tractor'], $border, 0, 'C');

        //Importe Total en Letra
        $pdf->SetY(172+$offsetY);
        $pdf->SetX(38+$offsetX);
        $pdf->Cell(100,3,"CERO PESOS  00  M.N.", $border, 0, 'L');

        //Fecha Estimada Entrega

        $fecha = $data['fechaEvento'];
        $fechaEntrega = strtotime ( "+".$data['diasEntrega']." day" , strtotime ($fecha ));
        $fechaEntrega = date ( 'd/m/Y' , $fechaEntrega );    


        $pdf->SetY(184+$offsetY);
        $pdf->SetX(78+$offsetX);
        $pdf->Cell(100,3,"FECHA ESTIMADA DE ENTREGA:  ".$fechaEntrega, $border, 0, 'L');          

        ///////Observaciones
        //Hora
        $pdf->SetY(187+$offsetY);
        $pdf->SetX(78+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            //FALTA AGREGAR LO DE PRIMER SERVICIO O LO DE CONTINUIDAD
            $pdf->Cell(130,3,$data['centroDistribucion']." - ESP:\t"."*HORA:\t".date("H:i:s"), $border, 0, 'L');
        } else {
            $pdf->Cell(145,3,"*HORA:\t".date("H:i:s"), $border, 0, 'L');
        }
        //Compania
        $pdf->SetY(198+$offsetY);
        $pdf->SetX(80+$offsetX);
        $pdf->Cell(130,3,$data['companiaTractor'], $border, 0, 'L');

        //Peso Total
        $pdf->SetY(198+$offsetY);
        $pdf->SetX(130+$offsetX);
        $pdf->Cell(130,3,"PESO NETO: ".$pesoTotal, $border, 0, 'L');

        //CONTACTO
        $pdf->SetY(202+$offsetY);
        $pdf->SetX(65+$offsetX);
        $pdf->Cell(130,3,"CONTACTO: ".$data['contactoDist'], $border, 0, 'L');

        //telefono
        $pdf->SetY(202+$offsetY);
        $pdf->SetX(145+$offsetX);
        $pdf->Cell(130,3,"Telefono: ".$data['telefonoDist'], $border, 0, 'L');   
    }
?>