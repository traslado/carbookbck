<?php
    /************************************************************************
    * Autor: Carlos Sierra Mayoral
    * Fecha: 18-Agosto-2016
    * Descripción: Programa para dar mantenimiento a foraneos
    *************************************************************************/
    session_start();
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");
    require_once("alLocalizacionPatios.php");
    require_once("catGenerales.php");

    $_REQUEST = trasformUppercase($_REQUEST);
    
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
    
    switch($_REQUEST['trViajesEspecialesActionHdn']){
        case 'getDatosViaje':
            getDatosViaje();
            break;
        case 'getTalonesViajes':
            getTalonesViajes();
            break;
        case 'addBloqueoUnidad':
            addBloqueoUnidad();
            break;
        case 'addViajeBloqueo';
            addViajeBloqueo();
            break;
        case 'getTalonViaje';
            getTalonViaje();
            break;
        case 'addTalonTmp';
             addTalonTmp();
            break;
        case 'liberaViaje';
            liberaViaje();
            break;
        case 'addTalonViaje';
            addTalonViaje();
            break;
        case 'getNumeroUnidades';
            getNumeroUnidades();
            break;
        case 'eliminaTlTmp';
            eliminaTlTmp();                    
            break;
        case 'eliminaUnidadTmp';
            eliminaUnidadTmp();
            break;
        case 'getNumeroTalones';
            getNumeroTalones();
            break;
        case 'impresioTalones';
            impresioTalones();                
            break;
        case 'foliosTalones_01';
            foliosTalones_01();                
            break;
        case 'getNumeroUnidadestmp';
            getNumeroUnidadestmp();                
            break;
        case 'getTractoresEspeciales':
            getTractoresEspeciales();
            break;
        default:
            echo '';
    }

    function getDatosViaje(){

        $sqlGetViaje = "SELECT CONCAT(ch.claveChofer,' - ', ch.nombre, ' ',ch.apellidoPaterno,' ',apellidoMaterno) as desChofer, plO.plaza as plazaOrigen, plD.plaza as plazaDestino, vt.viaje, idViajeTractor, ch.claveChofer, vt.claveMovimiento ".
                        "FROM caTractoresTbl tr, trviajestractorestbl vt,  cachoferestbl ch, caplazastbl plO, caplazastbl plD ".
                        "WHERE tr.idTractor = vt.idTractor ".
                        "AND vt.claveChofer = ch.claveChofer ".
                        "AND vt.idPlazaOrigen = plO.idPlaza ".
                        "AND vt.idPlazaDestino = plD.idPlaza ".
                        "AND vt.idTractor = '".$_REQUEST['asigEspecialesTractorCmb']."'  ".
                        "AND vt.viaje = (SELECT max(viaje) FROM trviajestractorestbl vt2 WHERE vt2.idViajeTractor = vt.idViajeTractor AND vt2.idTractor = vt.idTractor) ".
                        "AND vt.claveMovimiento IN('VG','VF','VE','VC') ".
                        "AND vt.idTractor NOT IN (SELECT idTractor FROM trViajesTractoresTmp tmp WHERE tmp.idTractor = vt.idTractor) ORDER BY vt.viaje DESC ";
        
        $rsGetViaje = fn_ejecuta_query($sqlGetViaje);
        $rsGetViaje['bloqueado'] = 0;
        if ($rsGetViaje['records'] == 0) {
            $bloqRst = fn_ejecuta_query("SELECT idTractor FROM trViajesTractoresTmp tmp WHERE tmp.idTractor = ".$_REQUEST['asigEspecialesTractorCmb']);
            if ($bloqRst['records'] > 0) {
                $rsGetViaje['bloqueado'] = 1;
            }
        }
              
        echo json_encode($rsGetViaje);
    }

    function getTalonesViajes(){

        $sqlGetTalones = "SELECT folio, distribuidor, numeroUnidades, pl.plaza plazaDestino, '1' as bloqueado, tl.companiaRemitente ".
                        "FROM trtalonesviajestbl tl, caplazastbl pl ".
                        "WHERE tl.idPlazaDestino = pl.idPlaza ".
                        "AND tl.claveMovimiento != 'TX' ".
                        "AND tl.idViajeTractor =  '".$_REQUEST['asigEspecialesIdViajeHdn']."' ";     
        
        $rsGetTalon = fn_ejecuta_query($sqlGetTalones);

        echo json_encode($rsGetTalon);
    }

    function addViajeBloqueo (){

        $selCentrDist="SELECT * FROM trviajestractorestbl WHERE idViajeTractor='".$_REQUEST['asigEspecialesIdViajeHdn']."' ";
        $rsCenDist=fn_ejecuta_query($selCentrDist);

        $addViajeBlq = "INSERT INTO trViajesTractoresTmp (idViajeTractor,idTractor,claveChofer,centroDistribucion,modulo,idUsuario,ip,fechaEvento) ".
                        "VALUES ( '".$_REQUEST['asigEspecialesIdViajeHdn']."', ".
                        "'".$_REQUEST['asigEspecialesTractorCmb']."', ".
                        "'".$_REQUEST['asigEspecialesCveChofer']."', ".
                        "'".$rsCenDist['root'][0]['centroDistribucion']."', ".
                        "'Asignacion Especiales', ".
                        "'".$_SESSION['idUsuario']."', ".
                        "'".$_SERVER['REMOTE_ADDR']."', ".
                        "NOW())";

        $rsAddBlq = fn_ejecuta_query($addViajeBlq);
              
        echo json_encode($rsAddBlq);  
    }

    function getTalonViaje(){

        if($_REQUEST['asigEspecialesFolioHdn'] != ''){
            $sqlGetUnidadesTalonStr = "SELECT au.avanzada, au.vin, au.simboloUnidad, ud.distribuidor, au.simboloUnidad AS descSimbolo, ud.distribuidor AS descDistribuidor ". 
                                        "FROM alunidadestbl au, alultimodetalletbl ud, trviajestractorestbl vt, trtalonesviajestbl tl, trunidadesdetallestalonestbl ts ".
                                        "WHERE au.vin = ud.vin ".
                                        "AND ud.vin = ts.vin ".
                                        "AND vt.idViajeTractor = tl.idViajeTractor ".
                                        "AND tl.idTalon = ts.idTalon ".
                                        "AND tl.idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ".
                                        "AND tl.folio = '".$_REQUEST['asigEspecialesFolioHdn']."' ";
        } else {
            $sqlGetUnidadesTalonStr =  "SELECT un.avanzada, un.vin, su.simboloUnidad, ud.distribuidor, su.simboloUnidad AS descSimbolo, ud.distribuidor AS descDistribuidor ".
                                        "FROM alunidadestmp ut, alunidadestbl un, alultimodetalletbl ud, casimbolosunidadestbl su, cadistribuidorescentrostbl di, aldestinosespecialestbl de ".
                                        "WHERE ut.vin = un.vin ".
                                        "AND ud.vin = de.vin ".
                                        "AND ud.distribuidor = di.distribuidorCentro ".
                                        "AND ut.vin = ud.vin ".
                                        "AND un.simboloUnidad = su.simboloUnidad ".
                                        "AND ut.modulo = 'Asignacion Especiales'".
                                        "AND ut.idUsuario = '".$_SESSION['idUsuario']."' ".
                                        "AND ut.ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                        "AND de.distribuidorDestino = '".$_REQUEST['asigEspecialesDistribuidorHdn']."'  ";
        }

        $rs = fn_ejecuta_query($sqlGetUnidadesTalonStr);
        
        echo json_encode($rs);
    }

    function addBloqueoUnidad(){
        $noTalon = floatval($_REQUEST['noTalon']);
        $addUnidad = "INSERT INTO alUnidadesTmp (vin, avanzada, modulo, idUsuario, ip, fecha) VALUES( ".
                     "'".$_REQUEST['asigEspecialesVin']."', ".
                     "'".$_REQUEST['asigEspecialesAvanzada']."', ".
                     "'Asignacion Especiales', ".
                     "'".$_SESSION['idUsuario']."', ".
                     "'".$_SERVER['REMOTE_ADDR']."', ".
                     "NOW())";
        $rsAddBloquea = fn_ejecuta_query($addUnidad);

        $selectDetalle = "SELECT * FROM aldestinosespecialestbl ".
                         "WHERE VIN='".$_REQUEST['asigEspecialesVin']."'".
                         "AND idDestinoEspecial=(SELECT MAX(idDestinoEspecial) from aldestinosespecialestbl where VIN='".$_REQUEST['asigEspecialesVin']."') ";
        $rsDetalle = fn_ejecuta_query($selectDetalle);

        $addUnidad = "INSERT INTO trunidadesdetallestalonestmp (numeroTalon, avanzada, vin, simbolo, color, centroDistribucion, idUsuario) ".
                    "VALUES ('".$noTalon."', ".
                    "'".$_REQUEST['asigEspecialesAvanzada']."', ".
                    "'".$_REQUEST['asigEspecialesVin']."', ".
                    "'".$rsDetalle['root'][0]['simboloUnidad']."', ".
                    "'".$rsDetalle['root'][0]['color']."', ".
                    "'".$_REQUEST['asigEspecialesCiaSesVal']."', ".
                    "'".$_SESSION['idUsuario']."')";
        $rsAddBloquea = fn_ejecuta_query($addUnidad);
        
        if(isset($_REQUEST['pantalla']) && isset($_REQUEST['idViajeTractor']) && $_REQUEST['pantalla'] == '1ASE')
        {
        		$sql = "UPDATE trTalonesViajesTmp SET centroDistribucion = '".$_REQUEST['centroDistribucion']."'".
        				   " WHERE idViajeTractor = ".$_REQUEST['idViajeTractor'].
        				   " AND numeroTalon = ".$_REQUEST['noTalon'].
        				   " AND idUsuario = ".$_SESSION['idUsuario'].
        				   " AND observaciones = 'talon Servicio Especial'".
        				   " AND tipoTalon = 'TE'";
        		//echo "$sql<br>";
        		fn_ejecuta_query($sql);
        }

        echo json_encode($rsAddBloquea);
    }

    function addTalonTmp(){

        $selCentrDist = "SELECT * FROM trviajestractorestbl WHERE idViajeTractor='".$_REQUEST['asigEspecialesIdViajeHdn']."' ";
        $rsCenDist=fn_ejecuta_query($selCentrDist);

        $selStr = "SELECT ifnull(max(numeroTalon +1),1) AS numeroTalon FROM trtalonesviajestmp tmp WHERE tmp.idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' AND tmp.observaciones = 'talon Servicio Especial'";
        $Rst = fn_ejecuta_query($selStr);

        $addTalonesTmp = "INSERT INTO trtalonesviajestmp (idViajeTractor, numeroTalon, distribuidor, numeroUnidades, direccionEntrega, companiaRemitente, centroDistribucion, observaciones, tipoTalon, tarifaUnidades, bloqueado, idUsuario) ".
                            "VALUES ('".$_REQUEST['asigEspecialesIdViajeHdn']."', ".
                            "".$Rst['root'][0]['numeroTalon'].", ".
                            "'".$_REQUEST['asigEspecialesDistribuidorCmb']."', ".
                            "'".$_REQUEST['asigEspecialesNumUnidadesTxt']."', ".
                            "(SELECT direccionEntrega FROM cadistribuidorescentrostbl WHERE distribuidorCentro = '".$_REQUEST['asigEspecialesDistribuidorCmb']."' ), ".
                            "'".$_REQUEST['asigespecialesRemitenteCmb']."', ".
                            "'".$rsCenDist['root'][0]['centroDistribucion']."', ".
                            "'talon Servicio Especial', ".
                            "'TE', ".
                            "'5', ".
                            "'0', '".$_SESSION['idUsuario']."') ";
        $rsAddTalonTmp = fn_ejecuta_query($addTalonesTmp);
        $rsAddTalonTmp['numeroTalon'] = floatval($Rst['root'][0]['numeroTalon']);

        echo json_encode($rsAddTalonTmp);
    }

    function liberaViaje(){

        $dltViajeTmp =  "DELETE FROM trViajesTractoresTmp ".
                        "WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ".
                        "AND modulo = 'Asignacion Especiales' ".
                        "AND idUsuario = ".$_SESSION['idUsuario'];
        
        $rsDltTmp = fn_ejecuta_query($dltViajeTmp);              
        // echo json_encode($rsDltTmp);               

        $selectTalon="SELECT * FROM trtalonesviajestmp ".
                     "WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ".
                     "AND observaciones = 'talon Servicio Especial' ".
                     "AND idUsuario = ".$_SESSION['idUsuario'];
        $rsTalon= fn_ejecuta_query($selectTalon);     

        for ($i=0; $i < sizeof($rsTalon['root']); $i++) { 
            $deleteTmp = "DELETE FROM trunidadesdetallestalonestmp WHERE numeroTalon='".$rsTalon['root'][$i]['numeroTalon']."' AND idUsuario = ".$_SESSION['idUsuario'];
            fn_ejecuta_query($deleteTmp);
        }
        
        $dltTalonTmp =  "DELETE FROM trtalonesviajestmp ".
                        "WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ".
                        "AND observaciones = 'talon Servicio Especial' ".
                        "AND idUsuario = ".$_SESSION['idUsuario'];
        fn_ejecuta_query($dltTalonTmp);

        $dltUnidadTmp = "DELETE FROM alUnidadesTmp ".
                        "WHERE idUsuario = '".$_SESSION['idUsuario']."' ".
                        "AND modulo = 'Asignacion Especiales'";
                        // "AND ip = '".$_SERVER['REMOTE_ADDR']."' ";
        fn_ejecuta_query($dltUnidadTmp);
        echo json_encode($rsUnidadDlt);                        
    }

    function addTalonViaje(){
        $a = array('success' => true);

        $getNumFolio = "SELECT fo.folio  as folioOriginal, tr.compania, tl.numeroTalon ".
                        "FROM trtalonesviajestmp tl, trviajestractorestbl vt, catractorestbl tr, trfoliostbl fo ".
                        "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                        //"AND tl.centroDistribucion = fo.centroDistribucion ".
                        "AND tr.compania = fo.compania ".
                        "AND fo.tipoDocumento = 'ER' ".
                        "AND vt.idTractor = tr.idTractor ".
                        "AND fo.centroDistribucion = 'CDTOL' ".
                        "AND vt.idViajeTractor = ".$_REQUEST['asigEspecialesIdViajeHdn']." ";
        $rsGetNumFolio = fn_ejecuta_query($getNumFolio);

        $folOriginal = $rsGetNumFolio['root'][0]['folioOriginal'];
        $trCompania = $rsGetNumFolio['root'][0]['compania'];

        $folioSumado = 0;
        
        
        if ($a['success']) {
            $sql = "UPDATE trviajestractorestbl ".
                  "SET claveMovimiento = 'VF' ".
                  "WHERE idViajeTractor = ".$_REQUEST['asigEspecialesIdViajeHdn'];
            //echo "$sql<br>";
            fn_ejecuta_query($sql);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success']     = false;
                $a['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten del estatus del viaje.";
                $a['sql']         = $sql;
            }
        }        
        
        if ($a['success']) {
            $sqlGetObtenFolio= "SELECT folio as folioFinal ".
                                "FROM trfoliostbl ".
                                "WHERE tipoDocumento = 'ER' ".
                                "AND compania = '".$trCompania."' ".
                                "AND centroDistribucion = 'CDTOL'" ;
            $rsUpdTalonViajeTmp = fn_ejecuta_query($sqlGetObtenFolio);

            $folioSumado = floatval($rsUpdTalonViajeTmp['root'][0]['folioFinal']) + 1;

            $updFolioTbl = "UPDATE trfoliostbl ".
                            "SET folio = '".$folioSumado."' ".
                            "WHERE tipoDocumento = 'ER' ".
                            "AND compania = '".$trCompania."' ".
                            "AND centroDistribucion = 'CDTOL'";
            $rsupdFolioTbl = fn_ejecuta_query($updFolioTbl);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success']     = false;
                $a['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten del Folio.";
                $a['sql']         = $updFolioTbl;
            } else {
                $selStr = "SELECT DISTINCT numeroTalon, distribuidor, ${folioSumado} AS folio, companiaRemitente, ".
                        "(SELECT idPlaza FROM cadistribuidorescentrostbl WHERE distribuidorCentro IN (SELECT centroDistribucion FROM trtalonesviajestmp WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' )  LIMIT 1) as idPlazaOrigen, ".
                        "(SELECT idPlazaDestino FROM aldestinosespecialestbl de, alunidadestmp au, trtalonesviajestmp tl WHERE de.vin = au.vin AND de.distribuidorDestino = tl.distribuidor LIMIT 1) as idPlazaDestino, ".
                        "(SELECT direccionDestino FROM aldestinosespecialestbl de, alunidadestmp au, trtalonesviajestmp tl WHERE de.vin = au.vin AND de.distribuidorDestino = tl.distribuidor LIMIT 1) as direccionEntrega, ".
                        "centroDistribucion, ".
                        "concat('PRIMER MOVIMIENTO S.E. ' , now()) as observaciones, ".
                        "numeroUnidades as numeroUnidades, ".
                        "'1.00' as importe ".
                        "FROM  trtalonesviajestmp  ".
                        " WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ".
                        " AND observaciones = 'talon Servicio Especial'".
                        " AND idUsuario = ".$_SESSION['idUsuario'];
                //echo "$selStr<br>";
                $talonesRst = fn_ejecuta_query($selStr);
                for ($i=0; $i < $talonesRst['records']; $i++) { 
                    $row = $talonesRst['root'][$i];
                    
                    $sql = "SELECT * FROM trtalonesviajestbl ".
                    			 " WHERE distribuidor = '".$row['distribuidor']."'".
													 " AND folio = ".$row['folio'].
													 " AND idViajeTractor = ".$_REQUEST['asigEspecialesIdViajeHdn'].
													 " AND centroDistribucion = '".$row['centroDistribucion']."'".
													 " AND tipoDocumento = 'TF'";
                    $rsAux1 = fn_ejecuta_query($sql);  
				            if($rsAux1['records'] == 0)
				            {
		                    $addTalonVStr = "INSERT INTO trtalonesviajestbl (distribuidor, folio, idViajeTractor, companiaRemitente, idPlazaOrigen, idPlazaDestino, direccionEntrega, centroDistribucion, tipoTalon, fechaEvento, observaciones, numeroUnidades, importe, seguro, tarifaCobrar, kilometrosCobrar, impuesto, retencion, claveMovimiento, tipoDocumento, firmaElectronica) VALUES (".
		                                    "'".$row['distribuidor']."', ".
		                                    "".$row['folio'].", ".
		                                    "'".$_REQUEST['asigEspecialesIdViajeHdn']."', ".
		                                    "'".$row['companiaRemitente']."', ".
		                                    "".$row['idPlazaOrigen'].", ".
		                                    "".$row['idPlazaDestino'].", ".
		                                    "".$row['direccionEntrega'].", ".
		                                    "'".$row['centroDistribucion']."', ".
		                                    "'TE', NOW(), ".
		                                    "'".$row['observaciones']."', ".
		                                    "".$row['numeroUnidades'].", ".
		                                    "".$row['importe'].", ".
		                                    "'0.00', ".
		                                    "'0.00', ".
		                                    "'0.00', ".
		                                    "'0.00', ".
		                                    "'0.00', ".
		                                    "'TF', ".
		                                    "'TF', NULL)";
		                    //echo "$addTalonVStr<br>";
		                    $rsTalonViaje = fn_ejecuta_query($addTalonVStr);
		                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
		                        $a['success']     = false;
		                        $a['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del cabecero del Tal&oacuten.";
		                        $a['sql']         = $addTalonVStr;
		                        break;
		                    }
		                    else
		                    {
		                    		$idDet = mysql_insert_id();
		                    }
				            }
                    if($a['success']) {
                        
                        $addUndTalStr = "SELECT tmp.* FROM trunidadesdetallestalonestmp tmp ".
                                        " WHERE tmp.numeroTalon = ".$row['numeroTalon'].
                                        " AND idUsuario = ".$_SESSION['idUsuario'].
                                        " AND centroDistribucion = '".$_SESSION['usuCompania']."'"; 
                        //echo "$addUndTalStr<br>";
                        $rsAddUndTalon = fn_ejecuta_query($addUndTalStr);
                        // print_r($rsAddUndTalon);echo "<br>";
                        for ($j=0; $j < sizeof($rsAddUndTalon['root']); $j++) { 
                            $rowDet = $rsAddUndTalon['root'][$j];
                            // print_r($rowDet);echo "<br>";                            
				                    $sql = "SELECT * FROM trUnidadesDetallesTalonesTbl".
				                           " WHERE idTalon = ".$idDet.
				                           " AND vin = '".$rowDet['vin']."'".
				                           " AND estatus = '1'";
				                    //echo "$sql<br>";
				                    $rsAux = fn_ejecuta_query($sql);  
				                    if($rsAux['records'] == 0)
				                    {
		                            $addDetTalStr = "INSERT INTO trUnidadesDetallesTalonesTbl (idTalon,vin,estatus) VALUES (".
		                                            $idDet.",".
		                                            "'".$rowDet['vin']."',".
		                                            "'1')";
		                            //echo "$addDetTalStr<br>";
		                            fn_ejecuta_query($addDetTalStr);

                                    $sqlDltLocalizacionPatiosStr = "UPDATE alLocalizacionPatiosTbl ".
                                           "SET vin = NULL, estatus='DI' WHERE vin= '".$rowDet['vin']."' ";

                                    /*if($patio != ''){
                                        $sqlDltLocalizacionPatiosStr .= "AND patio = '".$patio."'";
                                    }*/

                                    $rs = fn_ejecuta_Upd($sqlDltLocalizacionPatiosStr);

		                           
		                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
		                                $a['success']     = false;
		                                $a['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del detalle del Tal&oacuten.";
		                                $a['sql']         = $addDetTalStr;
		                                // echo "err>".$j."<br>";
		                                break;
		                            }				                    		
				                    }
                        }
                    }				            		
                }

                if ($a['success']) {
                    $addUndHistStr = "INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                                    "SELECT DISTINCT hu.centroDistribucion, tmp.vin, NOW(), 'AM' as claveMovimiento, hu.distribuidor, '5' AS idTarifa, hu.centroDistribucion, vt.claveChofer, concat('PRIMER MOVIMIENTO S.E.' , now()) as observaciones, ".$_SESSION['idUsuario']." AS usuario, '".$_SERVER['REMOTE_ADDR']."' AS ip ".
                                    "FROM alultimodetalletbl hu, alunidadestmp tmp, trviajestractorestbl vt, trTalonesViajesTbl tl, trUnidadesDetallesTalonesTbl ts ".
                                    "WHERE vt.idViajeTractor = tl.idViajeTractor ".
                                    "AND tl.idTalon = ts.idTalon ".
                                    "AND ts.vin = hu.vin ".
                                    "AND hu.vin = tmp.vin ".
                                    "AND tmp.idUsuario = '".$_SESSION['idUsuario']."' ".
                                    "AND tmp.ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                    "AND vt.idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ";
                    $rsAddUndHistorico = fn_ejecuta_query($addUndHistStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql']." En la inserci&oacuten del Hist&oacuterico.";
                        $a['sql']         = $addUndHistStr;
                    } else {
                        $updUndUd = "UPDATE alultimodetalletbl ".
                                    "SET claveMovimiento = 'AM', ".
                                    "fechaEvento = NOW() ".
                                    "WHERE vin IN (SELECT tmp.VIN ".
                                                    "FROM alunidadestmp tmp ".
                                                    "WHERE tmp.ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                                    "AND tmp.idUsuario = '".$_SESSION['idUsuario']."')";
                        $rsAddUndHistorico = fn_ejecuta_query($updUndUd);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten del &uacuteltimo detalle.";
                            $a['sql']         = $updUndUd;
                        } else {
                            $sumaVinStr = "SELECT sum(numeroUnidades + (SELECT COUNT(VIN) as numeroUnidades FROM alunidadestmp ".
                                        "WHERE MODULO = 'Asignacion Especiales'  ".
                                        "AND idUsuario = '".$_SESSION['idUsuario']."' ".
                                        "AND IP = '".$_SERVER['REMOTE_ADDR']."')) AS numeroUnidades ".
                                        "FROM trviajestractorestbl ".                           
                                        "WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ";
                            //echo "$sumaVinStr<br>";
                            $rsNumeroUnidades = fn_ejecuta_query($sumaVinStr);

                            $numUnidades = $rsNumeroUnidades['root'][0]['numeroUnidades'];  

                            $getNumeroRepartStr = "SELECT count(distinct distribuidor) as numeroRepartos ".
                                                "FROM trtalonesviajestbl ".
                                                "WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."'";

                            $rsNumeroRepartos = fn_ejecuta_query($getNumeroRepartStr);              
                            // echo json_encode($rsNumeroUnidades);

                            $numRepartos = $rsNumeroRepartos['root'][0]['numeroRepartos'];
                            if($numRepartos== '0'){
                                $numRepartos = 1;
                            }

                            if ($a['success']) {
                                $updViaje = "UPDATE trviajestractorestbl ".
                                            "SET numeroRepartos =  '".$numRepartos."', ".
                                            "numeroUnidades = '".$numUnidades."' ".
                                            "WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."'";
                                //echo "$updViaje<br>";
                                $rsUpdViaje = fn_ejecuta_query($updViaje);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success']     = false;
                                    $a['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten del viaje.";
                                    $a['sql']         = $updViaje;
                                }
                            }
                        }
                    }
                }
            }
        }
        $a['folio'] = $folioSumado;
        //fn_ejecuta_query("rollback");		/////// COMENTAR LINEA SOLO PARA PRUEBAS

        echo json_encode($a);
    }

    function getNumeroUnidades(){

        /*$sqlGetNumeroUnidades =  "SELECT COUNT(un.vin) as sumaVin ".
                            "FROM alunidadestmp ut, alunidadestbl un, alultimodetalletbl ud, casimbolosunidadestbl su, cadistribuidorescentrostbl di, aldestinosespecialestbl de ".
                            "WHERE ut.vin = un.vin ".
                            "AND ud.vin = de.vin ".
                            "AND ud.distribuidor = di.distribuidorCentro ".
                            "AND ut.vin = ud.vin ".
                            "AND un.simboloUnidad = su.simboloUnidad ".
                            "AND ut.modulo = 'Asignacion Especiales'".
                            "AND de.idDestinoEspecial = (SELECT max(de1.idDestinoEspecial) FROM aldestinosespecialestbl de1 WHERE de1.vin = ud.vin) ".                            
                            "AND ut.idUsuario = '".$_SESSION['idUsuario']."' ".
                            "AND ut.ip = '".$_SERVER['REMOTE_ADDR']."' ".
                            "AND de.distribuidorDestino = '".$_REQUEST['asigEspecialesDistribuidorHdn']."'  ";*/
        $sqlGetNumeroUnidades =  "SELECT COUNT(ut.vin) as sumaVin ".
                                "FROM trunidadesdetallestalonestmp ut, trtalonesviajestmp tm, alunidadestmp un ".
                                "WHERE ut.vin = un.vin AND ut.numeroTalon = tm.numeroTalon ".
                                "AND ut.idUsuario = tm.idUsuario ".
                                "AND tm.idViajeTractor = ".$_REQUEST['idViajeTractor']." ".
                                "AND tm.idUsuario = ".$_SESSION['idUsuario']." ".
                                "AND tm.numeroTalon = ".$_REQUEST['noTalon']." ".
                                "AND ut.idUsuario = un.idUsuario";

        $rsGetNumeroUnidades = fn_ejecuta_query($sqlGetNumeroUnidades);
        echo json_encode($rsGetNumeroUnidades);        
    }

    function getNumeroUnidadestmp(){

        $sqlGetNumeroUnidades =  "SELECT COUNT(un.vin) as sumaVin ".
                            "FROM trunidadesdetallestalonestmp ut, alunidadestbl un, alultimodetalletbl ud, casimbolosunidadestbl su, cadistribuidorescentrostbl di, aldestinosespecialestbl de ,trtalonesviajestmp tm ".
                            "WHERE ut.vin = un.vin ".
                            "AND ud.vin = de.vin ".
                            //"AND tm.centroDistribucion='".$_SESSION['usuCompania']."' ".
                            "AND tm.numeroTalon = ut.numeroTalon ".
                            "AND tm.numeroTalon= ".$_REQUEST['noTalon']." ".
                            "AND ud.distribuidor = di.distribuidorCentro ".
                            "AND ut.vin = ud.vin ".
                            "AND tm.centroDistribucion=ut.centroDistribucion ".
                            "AND un.simboloUnidad = su.simboloUnidad ".
                            // "AND ut.modulo = 'Asignacion Especiales'".
                            "AND de.idDestinoEspecial = (SELECT max(de1.idDestinoEspecial) FROM aldestinosespecialestbl de1 WHERE de1.vin = ud.vin) ".                            
                            "AND ut.idUsuario = '".$_SESSION['idUsuario']."' ".
                            ///"AND ut.ip = '".$_SERVER['REMOTE_ADDR']."' ".
                            "AND de.distribuidorDestino = '".$_REQUEST['asigEspecialesDistribuidorHdn']."'  ";

        $rsGetNumeroUnidades = fn_ejecuta_query($sqlGetNumeroUnidades);

        echo json_encode($rsGetNumeroUnidades);
    }

    function eliminaTlTmp(){

        $selectTalon="SELECT * FROM trtalonesviajestmp ".
                     "WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ".
                     "AND numeroTalon = ".$_REQUEST['noTalon']." ".
                     "AND observaciones = 'talon Servicio Especial' ".
                     "AND idUsuario = ".$_SESSION['idUsuario'];
        $rsTalon= fn_ejecuta_query($selectTalon);     

        for ($i=0; $i < sizeof($rsTalon['root']); $i++) { 
            $dltUnidadTmp = "DELETE FROM alUnidadesTmp ".
                            "WHERE idUsuario = '".$_SESSION['idUsuario']."' ".
                            "AND vin IN (SELECT VIN FROM trunidadesdetallestalonestmp WHERE idUsuario = ".$_SESSION['idUsuario']." AND numeroTalon = ".$_REQUEST['noTalon'].") ".
                            "AND modulo = 'Asignacion Especiales'";
            fn_ejecuta_query($dltUnidadTmp);

            $deleteTmp = "DELETE FROM trunidadesdetallestalonestmp WHERE numeroTalon='".$rsTalon['root'][$i]['numeroTalon']."' AND idUsuario = ".$_SESSION['idUsuario'];
            fn_ejecuta_query($deleteTmp);
        }
        
        $dltTalonTmp =  "DELETE FROM trtalonesviajestmp ".
                        "WHERE idViajeTractor = '".$_REQUEST['asigEspecialesIdViajeHdn']."' ".
                        "AND numeroTalon = ".$_REQUEST['noTalon']." ".
                        "AND observaciones = 'talon Servicio Especial' ".
                        "AND idUsuario = ".$_SESSION['idUsuario'];
        fn_ejecuta_query($dltTalonTmp);
    }

    function eliminaUnidadTmp(){
        $dtlUnTmp = "DELETE FROM alUnidadesTmp ".
                    "WHERE avanzada = '".$_REQUEST['asigEspecialesAvanzadaHdn']."' ".
                    "AND idUsuario = '".$_SESSION['idUsuario']."' ".
                    "AND modulo = 'Asignacion Especiales'";
        fn_ejecuta_query($dtlUnTmp);

        $dtlUnTmp = "DELETE FROM trunidadesdetallestalonestmp ".
                    "WHERE avanzada = '".$_REQUEST['asigEspecialesAvanzadaHdn']."' ".
                    "AND numeroTalon = ".$_REQUEST['noTalon']." ".
                    "AND idUsuario = '".$_SESSION['idUsuario']."' ";
        fn_ejecuta_query($dtlUnTmp);
    }

    function getNumeroTalones(){
        $sqlGetNumeroTalon = "SELECT COUNT(numeroTalon) AS counTalon ".
                                "FROM trtalonesviajestmp ".
                                "WHERE idViajeTractor = ".$_REQUEST['asigEspecialesIdViajeHdn']." ";

        $reGetTalones = fn_ejecuta_query($sqlGetNumeroTalon);
                                        
        echo json_encode($reGetTalones);
    } 

    function impresioTalones(){
        $sqlGetImpresion = "SELECT (SELECT COUNT(tl2.numeroTalon) FROM trtalonesviajestmp tl2 WHERE tl2.idViajeTractor = ".$_REQUEST['asigEspecialesIdViajeHdn'].") as numeroTalones, tr.compania, tl.idViajeTractor, tl3.folio ".
                            "FROM trtalonesviajestmp tl, catractoresTbl tr, trviajestractorestbl vt, trtalonesviajestbl tl3 ".
                            "WHERE vt.idViajeTractor = tl.idViajeTractor ".
                            "AND vt.idViajeTractor = tl3.idViajeTractor ".
                            "AND tl.distribuidor = tl3.distribuidor ".
                            "AND vt.idTractor = tr.idTractor ".
                            "AND tl.idViajeTractor = ".$_REQUEST['asigEspecialesIdViajeHdn']." ".
                            "AND tl3.claveMovimiento != 'TX' ".
                            "ORDER BY TL3.fechaEvento DESC ";
        
        $reGetImpresio = fn_ejecuta_query($sqlGetImpresion);
                                        
        echo json_encode($reGetImpresio);
    }    

    function foliosTalones_01(){

        $sqlFoliosTalones = "SELECT * FROM trtalonesviajestbl WHERE idviajeTractor=".$_REQUEST['asigEspecialesIdViajeHdn'].
                            " AND tipoTalon='TE' ".
                            "AND fechaEvento=(SELECT MAX(fechaEvento) ".
                            "FROM trtalonesviajestbl) ";

        $rstTalon=  fn_ejecuta_query($sqlFoliosTalones);

        for ($i=0; $i <sizeof($rstTalon['root']) ; $i++) { 
            $folios[] = $rstTalon['root'][$i]['folio'];
        }

        echo json_encode($folios);

        /*$getNumFolio = "SELECT fo.folio  as folioOriginal, tr.compania, tl.numeroTalon ".
                        "FROM trtalonesviajestmp tl, trviajestractorestbl vt, catractorestbl tr, trfoliostbl fo ".
                        "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                        ///"AND tl.centroDistribucion = fo.centroDistribucion ".
                        "AND tr.compania = fo.compania ".
                        "AND fo.tipoDocumento = 'ER' ".
                        "AND vt.idTractor = tr.idTractor ".
                        "AND fo.centroDistribucion = '".$_SESSION['usuCompania']."'".
                        "AND vt.idViajeTractor = ".$_REQUEST['asigEspecialesIdViajeHdn']." ";

        $rsGetNumFolio = fn_ejecuta_query($getNumFolio);
        

        $folOriginal = $rsGetNumFolio['root'][0]['folioOriginal'];
        $trCompania = $rsGetNumFolio['root'][0]['compania'];     


        $sqlGetObtenFolio = "SELECT sum(fo.folio + (SELECT count(idViajeTractor) FROM trtalonesviajestmp WHERE centroDistribucion = '".$_SESSION['usuCompania']."' AND idViajeTractor = ".$_REQUEST['asigEspecialesIdViajeHdn'].")) as folioFinal ".
                            "FROM trfoliostbl fo ".
                            "WHERE fo.tipoDocumento = 'ER' ".
                            "AND fo.compania = '".$trCompania."' ".
                            "AND fo.centroDistribucion = '".$_SESSION['usuCompania']."' ";
        
        $rsUpdTalonViajeTmp = fn_ejecuta_query($sqlGetObtenFolio);

        $folioSumado =  $rsUpdTalonViajeTmp['root'][0]['folioFinal'];


            $updFolioTbl = "UPDATE trfoliostbl ".
                            "SET folio = '".$folioSumado."' ".
                            "WHERE tipoDocumento = 'ER' ".
                            "AND compania = '".$trCompania."' ".
                            "AND centroDistribucion = '".$_SESSION['usuCompania']."'" ;

            $rsupdFolioTbl = fn_ejecuta_query($updFolioTbl);            

        echo json_encode($folioSumado);  */  
    }

    function getTractoresEspeciales(){

        $sqlGetTractor = "SELECT tr.idTractor, tr.tractor ".
                         "FROM caTractoresTbl tr, trviajestractorestbl vt ".
                         "WHERE tr.idTractor = vt.idTractor ".
                         "AND tr.compania = '".$_REQUEST['asigEspecialesCompaniaCmb']."' ".
                         "AND vt.viaje = (SELECT max(viaje) FROM trviajestractorestbl vt2 WHERE vt2.idViajeTractor = vt.idViajeTractor AND vt2.idTractor = vt.idTractor) ".
                         "AND vt.claveMovimiento IN('VG','VF','VC') ".
                         "AND vt.idTractor NOT IN (SELECT idTractor FROM trViajesTractoresTmp tmp WHERE tmp.idTractor = vt.idTractor) ".
                         "ORDER BY tr.tractor ASC ";

        $rs = fn_ejecuta_query($sqlGetTractor);
              
        echo json_encode($rs);
    }
?>