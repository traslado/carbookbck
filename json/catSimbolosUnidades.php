<?php
    session_start();
	$_SESSION['modulo'] = "catSimboloUnidades";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

	switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['catSimbolosUnidadesActionHdn']){
        case 'getSimbolosUnidades':
            getSimbolosUnidades();
            break;
        case 'getSimbolosDetenidos':
            getSimbolosDetenidos();
            break;
        case 'getSimbolosPorTarifa':
            getSimbolosPorTarifa();
            break;
        case 'addSimboloUnidades':
            addSimboloUnidades();
            break;
        case 'updSimbolosUnidades':
            updSimbolosUnidades();
            break;
        case 'dltSimboloUnidades':
            dltSimboloUnidades();
        default:
            echo '';
    }


    function getSimbolosUnidades(){
      	$lsWhereStr = "";

        if ($_REQUEST['catSimbolosUnidadesSimboloTxt'] != ''){
            $lsCondicionStr = fn_construct("'".$_REQUEST['catSimbolosUnidadesSimboloTxt']."'", "su.simboloUnidad", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesDescripcionTxt'], "su.descripcion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesTipoOrigenUnidadHdn'], "su.tipoOrigenUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesClasificacionHdn'], "su.clasificacion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesImporteBonificacionTxt'], "su.importeBonificacion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesRepuveTxt'], "su.tieneRepuve", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesHomologacionTxt'], "su.homologacion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesMarcaHdn'], "su.marca", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesTipoUnidadHdn'], "su.tipoUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }/*
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesTarifaHdn'], "tf.tarifa", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        //TIPO TARIFA
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesTipoTarifaHdn'], "tf.tipoTarifa", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }*/
        //SE USA PARA TRAER SIMBOLOS DETENIDOS, NO DETENIDOS O TODOS
        //0 PARA NO DETENIDOS, 1 PARA DETENIDOS, VACIO PARA TODOS
        if ($_REQUEST['catSimbolosUnidadesDetenidosHdn'] == '0'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "su.simboloUnidad NOT IN (SELECT IFNULL(de2.simboloUnidad, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                        "WHERE de2.distribuidor IS NULL)");
        } else if ($_REQUEST['catSimbolosUnidadesDetenidosHdn'] == '1'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "su.simboloUnidad IN (SELECT IFNULL(de2.simboloUnidad, 1) FROM alSimbolosDetenidasTbl de2)");
        }

		$sqlGetSimbolosUnidadesStr = "SELECT su.*, ".
                                     "(SELECT descripcion FROM caMarcasUnidadesTbl mu ".
                                        "WHERE mu.marca = su.marca AND mu.tipoMarca='A') AS nombreMarca, ".
                                     "(SELECT cg.nombre FROM caGeneralesTbl cg ".
                                        "WHERE cg.valor = su.tipoOrigenUnidad AND tabla = 'caSimbolosUnidadesTbl' ".
                                        "AND columna = 'tipoOrigenUnidad') AS nombreTipoOrigen, ".
                                     "(SELECT cg2.nombre FROM caGeneralesTbl cg2 ".
                                        "WHERE cg2.valor = su.tipoUnidad AND tabla = 'caSimbolosUnidadesTbl' ".
                                        "AND columna = 'tipoUnidad') AS nombreTipoUnidad,".
                                     "(SELECT cm.descripcion FROM caClasificacionMarcaTbl cm ".
                                        "WHERE cm.marca = su.marca AND cm.clasificacion = su.clasificacion LIMIT 1) as descClasificacion, ".
                                     "(SELECT 1 FROM alSimbolosDetenidasTbl de ".
                                        "WHERE de.simboloUnidad = su.simboloUnidad AND de.distribuidor IS NULL) AS simboloDetenido ";

        //SE USAR PARA SABER SI EL SIMBOLO DE CIERTO DISTRIBUIDOR ESTA DETENIDO
        if($_REQUEST['catSimbolosUnidadesDistribuidorHdn'] != ''){
            $sqlGetSimbolosUnidadesStr .= ", (SELECT de.distribuidor FROM alSimbolosDetenidasTbl de ".
                                        "WHERE de.simboloUnidad = su.simboloUnidad AND de.distribuidor = '".$_REQUEST['catSimboloUnidadesDistribuidorHdn']."' ".
                                            ") AS simboloDistDetenido ";
        }

        $sqlGetSimbolosUnidadesStr .= "FROM caSimbolosUnidadesTbl su " . $lsWhereStr;

		$rs = fn_ejecuta_query($sqlGetSimbolosUnidadesStr);


        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['descripcion'];
        }

		echo json_encode($rs);
    }

    function getSimbolosDetenidos(){
        $lsWhereStr = "WHERE sd.simboloUnidad = su.simboloUnidad";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesCentroDistHdn'], "sd.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesDistribuidorHdn'], "sd.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesSimboloUnidadHdn'], "sd.simboloUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetDistribuidoresDetenidosStr = "SELECT sd.simboloUnidad, sd.distribuidor AS simboloDistDetenido, ".
                                            "sd.centroDistribucion, su.descripcion, ".
                                            "(SELECT 1 FROM alSimbolosDetenidasTbl de WHERE de.simboloUnidad = su.simboloUnidad ".
                                                "AND de.distribuidor IS NULL) AS simboloDetenido ".
                                            "FROM alSimbolosDetenidasTbl sd, caSimbolosUnidadesTbl su ".
                                            $lsWhereStr." GROUP BY sd.simboloUnidad";

        $rs = fn_ejecuta_query($sqlGetDistribuidoresDetenidosStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['descripcion'];
        }

        echo json_encode($rs);

    }

    function getSimbolosPorTarifa(){
        $lsWhereStr = "WHERE ct.clasificacion = su.clasificacion ".
                        "AND su.marca=ct.marca ".
                        "AND tf.idTarifa = ct.idTarifa ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesSimboloTxt'], "su.simboloUnidad", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesTarifaHdn'], "tf.tarifa", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catSimbolosUnidadesTipoTarifaHdn'], "tf.tipoTarifa", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetSimboloPorTarifaStr ="SELECT su.*, ct.idTarifa, tf.tarifa, tf.descripcion AS descripcionTarifa, tf.tipoTarifa, ".
                                    "(SELECT descripcion FROM caMarcasUnidadesTbl mu ".
                                        "WHERE mu.marca = su.marca AND mu.tipoMarca='A') AS nombreMarca, ".
                                    "(SELECT cg.nombre FROM caGeneralesTbl cg ".
                                        "WHERE cg.valor = su.tipoOrigenUnidad AND tabla = 'caSimbolosUnidadesTbl' ".
                                            "AND columna = 'tipoOrigenUnidad') AS nombreTipoOrigen, ".
                                    "(SELECT cg2.nombre FROM caGeneralesTbl cg2 ".
                                        "WHERE cg2.valor = su.tipoUnidad AND tabla = 'caSimbolosUnidadesTbl' ".
                                            "AND columna = 'tipoUnidad') AS nombreTipoUnidad,".
                                    "(SELECT cm.descripcion FROM caClasificacionMarcaTbl cm ".
                                        "WHERE cm.marca = su.marca AND cm.clasificacion = su.clasificacion LIMIT 1) as descClasificacion ".
                                    "FROM caSimbolosUnidadesTbl su, caClasificacionTarifasTbl ct, caTarifasTbl tf ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetSimboloPorTarifaStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) {
            $rs['root'][$iInt]['descSimbolo'] = $rs['root'][$iInt]['simboloUnidad']." - ".$rs['root'][$iInt]['descripcion'];
            $rs['root'][$iInt]['descTarifa'] = $rs['root'][$iInt]['tarifa']." - ".$rs['root'][$iInt]['descripcionTarifa'];
        }

        echo json_encode($rs);

    }

    function addSimboloUnidades(){
        $a = array();
        $e = array();
        $a['success'] = true;

		if($_REQUEST['catSimbolosUnidadesSimboloTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesSimboloTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
                if($_REQUEST['catSimbolosUnidadesPesoAproximadoTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesPesoAproximadoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catSimbolosUnidadesDescripcionTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesDescripcionTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catSimbolosUnidadesTipoOrigenUnidadHdn'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesTipoOrigenUnidadHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesClasificacionMarcaHdn'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesClasificacionMarcaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesImporteBonificacionTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesImporteBonificacionTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesRepuveTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesRepuveTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesMarcaHdn'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesMarcaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesTipoUnidadHdn'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesTipoUnidadHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
         if ($a['success'] == true) {
            $sqlAddSimbolUnidadStr = "INSERT INTO caSimbolosUnidadesTbl (simboloUnidad, descripcion, tipoOrigenUnidad, ".
                                     "clasificacion, importeBonificacion, tieneRepuve, marca, tipoUnidad,homologacion, pesoAproximado) " .
                                     "VALUES (" .
                                        "'".$_REQUEST['catSimbolosUnidadesSimboloTxt'] . "', ".
                                        "'".$_REQUEST['catSimbolosUnidadesDescripcionTxt']."', " .
                                        "'" . $_REQUEST['catSimbolosUnidadesTipoOrigenUnidadHdn'] . "', " .
                                        "'".$_REQUEST['catSimbolosUnidadesClasificacionMarcaHdn'] . "', " .
                                        "'" . $_REQUEST['catSimbolosUnidadesImporteBonificacionTxt'] . "', " .
                                        $_REQUEST['catSimbolosUnidadesRepuveTxt'] . ", " .
                                        "'" . $_REQUEST['catSimbolosUnidadesMarcaHdn'] . "', " .
                                        "'".$_REQUEST['catSimbolosUnidadesTipoUnidadHdn'] . "', ".  
                                        "".$_REQUEST['catSimbolosUnidadesHomologacionTxt'] . ", ".
                                        "'".$_REQUEST['catSimbolosUnidadesPesoAproximadoTxt'] . "')";

            $rs = fn_ejecuta_Add($sqlAddSimbolUnidadStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == ""))
            {
                $a['sql'] = $sqlAddSimbolUnidadStr;
                $a['successMessage'] = getSimbolosUnidadesSuccessMsg();
                $a['id'] = $_REQUEST['catSimbolosUnidadesSimboloTxt'];
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddSimbolUnidadStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
	}

    function updSimbolosUnidades(){
        $a = array();
        $e = array();
        $a['success'] = true;

		if($_REQUEST['catSimbolosUnidadesDescripcionTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesDescripcionTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesPesoAproximadoTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesPesoAproximadoTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catSimbolosUnidadesTipoOrigenUnidadHdn'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesTipoOrigenUnidadHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesClasificacionMarcaHdn'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesClasificacionMarcaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesImporteBonificacionTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesImporteBonificacionTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesRepuveTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesRepuveTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesMarcaHdn'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesMarcaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catSimbolosUnidadesTipoUnidadHdn'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesTipoUnidadHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success']){
            $sqlUpdSimboloUnidadStr = "UPDATE caSimbolosUnidadesTbl ".
                                      "SET descripcion = '" . $_REQUEST['catSimbolosUnidadesDescripcionTxt'] . "', " .
                                      "tipoOrigenUnidad = '" . $_REQUEST['catSimbolosUnidadesTipoOrigenUnidadHdn'] . "', " .
                                      "clasificacion = '" . $_REQUEST['catSimbolosUnidadesClasificacionMarcaHdn'] . "', " .
                                      "importeBonificacion = '" . $_REQUEST['catSimbolosUnidadesImporteBonificacionTxt'] . "', " .
                                      "tieneRepuve = " . $_REQUEST['catSimbolosUnidadesRepuveTxt'] . ", " .
                                      "homologacion = " . $_REQUEST['catSimbolosUnidadesHomologacionTxt'] . ", " .
                                      "marca = '" . $_REQUEST['catSimbolosUnidadesMarcaHdn'] . "', " .
                                      "pesoAproximado = '" . $_REQUEST['catSimbolosUnidadesPesoAproximadoTxt'] . "', " .
                                      "tipoUnidad = '" . $_REQUEST['catSimbolosUnidadesTipoUnidadHdn'] . "' " .
                                      "WHERE simboloUnidad = '" . $_REQUEST['catSimbolosUnidadesSimboloTxt'] . "' ";


			$rs = fn_ejecuta_Upd($sqlUpdSimboloUnidadStr);

			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
			    $a['sql'] = $sqlUpdSimboloUnidadStr;
                $a['successMessage'] = getSimbolosUnidadesUpdateMsg();
                $a['id'] = $_REQUEST['catSimbolosUnidadesSimboloTxt'];
			} else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdSimboloUnidadStr;
			}
        }
        $a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
	}

    function dltSimboloUnidades(){
        $a = array();
        $e = array();
        $a['success'] = true;

		if($_REQUEST['catSimbolosUnidadesSimboloTxt'] == ""){
            $e[] = array('id'=>'catSimbolosUnidadesSimboloTxt','msg'=>'Falto Seleccionar el Simbolo');
			$a['errorMessage'] = 'Los campos marcados con "*" son Requeridos';
            $a['success'] = false;
        }

        if ($a['success']) {
            $sqlDltSimboloUnidadStr = "DELETE FROM caSimbolosUnidadesTbl " .
			                          "WHERE simboloUnidad = '" . $_REQUEST['catSimbolosUnidadesSimboloTxt'] . "' ";

			$rs = fn_ejecuta_query($sqlDltSimboloUnidadStr);

			if($_SESSION['error_sql'] == ""){
			    $a['sql'] = $sqlDltSimboloUnidadStr;
                $a['successmessage'] = getSimbolosUnidadesDeleteMsg();
                $a['id'] = $_REQUEST['catSimbolosUnidadesSimboloTxt'];
			} else {
                $a['success'] = false;
                $a['errormessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDltSimboloUnidadStr;
			}
        }
        $a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
	}
?>
