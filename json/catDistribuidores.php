<?php
    /************************************************************************
    * Autor: Alfonso César Martínez Fuertes
    * Fecha: 09-Enero-2014
    * Tablas afectadas: caDistribuidoresCentrosTbl
    * Descripción: Programa para dar mantenimiento a los Distribuidores
    *************************************************************************/
    session_start();
    $_SESSION['modulo'] = "catDistribuidores";
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("catDirecciones.php");

    $_REQUEST = trasformUppercase($_REQUEST);
    
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    } 

    switch($_REQUEST['catDistCentroActionHdn']){
        case 'getDistribuidores':
            getDistribuidores();
            break;
        case 'getDistribuidoresCambio':
            getDistribuidoresCambio();
            break;
        case 'getDistribuidores1':
            getDistribuidores1();
            break;
        case 'getDistribuidoresDetenidos':
            getDistribuidoresDetenidos();
            break;
        case 'getDistribuidoresKilometros':
            getDistribuidoresKilometros();
            break;
        case 'getDistribuidoresKilometrosFromDistribuidor':
            getDistribuidoresKilometrosFromDistribuidor();
            break;
        case 'getSucursalCombo':
            getSucursalCombo();
            break;
        case 'getDireccionDistViajes':
            echo json_encode(getDireccionDistViajes());
            break;
        case 'addDistribuidor':
            addDistribuidor();
            break;
        case 'updDistribuidor':
            updDistribuidor();                                                   
            break;
        case 'addDistribuidorEspecial':
            addDistribuidorEspecial();
            break;
        case 'updDistribuidorEspecial':
            updDistribuidorEspecial();                                                   
            break;
        case 'getDistribuidoresPuerto':
            getDistribuidoresPuerto();                                                   
            break;    
        default:
            echo '';
    }

        function getDistribuidoresCambio(){
       /* $lsWhereStr = "WHERE d.idColonia = c.idColonia ".
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ".
                      "AND pl.idPlaza = dc.idPlaza ".
                      "AND d.direccion = dc.direccionEntrega "; 

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTipoHdn'], "dc.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDistribuidorTxt'], "dc.distribuidorCentro", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDescripcionTxt'], "dc.descripcionCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPlazaHdn'], "dc.idPlaza", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroObservacionesTxa'], "dc.observaciones", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTelefonoTxt'], "dc.telefono", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroFaxTxt'], "dc.fax", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroContactoTxt'], "dc.contacto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEmailTxt'], "dc.eMail", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRutaDestinoTxt'], "dc.rutaDestino", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSucursalDeHdn'], "dc.sucursalDe", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }    
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSueldoTxt'], "dc.sueldoGarantizado", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroIdRegionHdn'], "dc.idRegion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEstatusHdn'], "dc.estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRepuveHdn'], "dc.tieneRepuve", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }        
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRFCHdn'], "dc.rfc", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPuertoHdn'], "dc.puerto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }                 
        if ($_REQUEST['catDistCentroDetenidoHdn'] == '0'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "dc.distribuidorCentro NOT IN (SELECT IFNULL(de2.distribuidor, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                        "WHERE de2.simboloUnidad IS NULL)");
        } else if ($_REQUEST['catDistCentroDetenidoHdn'] == '1'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "dc.distribuidorCentro IN (SELECT IFNULL(de2.distribuidor, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                            "WHERE de2.simboloUnidad IS NULL)");
        }

       /* $sqlGetDistribuidoresStr = "SELECT dc.*, pl.idPlaza, pl.plaza, p.pais, e.estado, m.municipio, c.colonia, c.cp, ".
                                    "d.calleNumero, d.direccion, ".
                                    "(SELECT 1 FROM alSimbolosDetenidasTbl sd WHERE sd.distribuidor = dc.distribuidorCentro ".
                                        "AND sd.simboloUnidad IS NULL) AS distribuidorDetenido, dc.zonaGeografica, ".
                                        "(SELECT concat(re.nombre,' - ',re.color) as color FROM caregionestbl re WHERE re.idRegion = dc.idRegion) as descRegion ".
                                    "FROM caDistribuidoresCentrosTbl dc, caPlazasTbl pl, caDireccionesTbl d, caColoniasTbl c, ".
                                        "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p ".$lsWhereStr;*/

                                  $sqlGetDistribuidoresStr="select  distribuidorCentro as distribuidorCentro , descripcionCentro  from cadistribuidorescentrostbl
                where estatus=1
                and tipoDistribuidor in('DI','DX')";

        $rs = fn_ejecuta_query($sqlGetDistribuidoresStr);
        //echo json_encode($rs);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['distDesc'] = $rs['root'][$iInt]['distribuidorCentro']." - ".$rs['root'][$iInt]['descripcionCentro'];
           
            /*$rs['root'][$iInt]['dirEntregaCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];*/
        }
            
        echo json_encode($rs);
    }

    function getDistribuidores(){
        $lsWhereStr = "WHERE d.idColonia = c.idColonia ".
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ".
                      "AND pl.idPlaza = dc.idPlaza ".
                      "AND d.direccion = dc.direccionEntrega "; 

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTipoHdn'], "dc.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDistribuidorTxt'], "dc.distribuidorCentro", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDescripcionTxt'], "dc.descripcionCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPlazaHdn'], "dc.idPlaza", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroObservacionesTxa'], "dc.observaciones", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTelefonoTxt'], "dc.telefono", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroFaxTxt'], "dc.fax", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroContactoTxt'], "dc.contacto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEmailTxt'], "dc.eMail", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRutaDestinoTxt'], "dc.rutaDestino", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSucursalDeHdn'], "dc.sucursalDe", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }    
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSueldoTxt'], "dc.sueldoGarantizado", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroIdRegionHdn'], "dc.idRegion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEstatusHdn'], "dc.estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRepuveHdn'], "dc.tieneRepuve", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }        
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRFCHdn'], "dc.rfc", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPuertoHdn'], "dc.puerto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }                 
        if ($_REQUEST['catDistCentroDetenidoHdn'] == '0'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "dc.distribuidorCentro NOT IN (SELECT IFNULL(de2.distribuidor, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                        "WHERE de2.simboloUnidad IS NULL)");
        } else if ($_REQUEST['catDistCentroDetenidoHdn'] == '1'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "dc.distribuidorCentro IN (SELECT IFNULL(de2.distribuidor, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                            "WHERE de2.simboloUnidad IS NULL)");
        }

        $sqlGetDistribuidoresStr = "SELECT dc.*, pl.idPlaza, pl.plaza, p.pais, e.estado, m.municipio, c.colonia, c.cp, ".
                                    "d.calleNumero, d.direccion, ".
                                    "(SELECT 1 FROM alSimbolosDetenidasTbl sd WHERE sd.distribuidor = dc.distribuidorCentro ".
                                        "AND sd.simboloUnidad IS NULL) AS distribuidorDetenido, dc.zonaGeografica, ".
                                        "(SELECT concat(re.nombre,' - ',re.color) as color FROM caregionestbl re WHERE re.idRegion = dc.idRegion) as descRegion ".
                                    "FROM caDistribuidoresCentrosTbl dc, caPlazasTbl pl, caDireccionesTbl d, caColoniasTbl c, ".
                                        "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetDistribuidoresStr);
        //echo json_encode($rs);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['distDesc'] = $rs['root'][$iInt]['distribuidorCentro']." - ".$rs['root'][$iInt]['descripcionCentro'];
            $rs['root'][$iInt]['dirEntregaCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];
        }
            
        echo json_encode($rs);
    }

    function getDistribuidores1(){
        $lsWhereStr = "WHERE d.idColonia = c.idColonia ".
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ".
                      "AND pl.idPlaza = dc.idPlaza ".
                      "AND d.direccion = dc.direccionEntrega "; 

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTipoHdn'], "dc.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDistribuidorTxt'], "dc.distribuidorCentro", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDescripcionTxt'], "dc.descripcionCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPlazaHdn'], "dc.idPlaza", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroObservacionesTxa'], "dc.observaciones", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTelefonoTxt'], "dc.telefono", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroFaxTxt'], "dc.fax", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroContactoTxt'], "dc.contacto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEmailTxt'], "dc.eMail", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRutaDestinoTxt'], "dc.rutaDestino", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSucursalDeHdn'], "dc.sucursalDe", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }    
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSueldoTxt'], "dc.sueldoGarantizado", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroIdRegionHdn'], "dc.idRegion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEstatusHdn'], "dc.estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRepuveHdn'], "dc.tieneRepuve", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }        
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRFCHdn'], "dc.rfc", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPuertoHdn'], "dc.puerto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }                 
        if ($_REQUEST['catDistCentroDetenidoHdn'] == '0'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "dc.distribuidorCentro NOT IN (SELECT IFNULL(de2.distribuidor, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                        "WHERE de2.simboloUnidad IS NULL)");
        } else if ($_REQUEST['catDistCentroDetenidoHdn'] == '1'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "dc.distribuidorCentro IN (SELECT IFNULL(de2.distribuidor, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                            "WHERE de2.simboloUnidad IS NULL)");
        }

        $sqlGetDistribuidoresStr = "SELECT dc.*, pl.idPlaza, pl.plaza, p.pais, e.estado, m.municipio, c.colonia, c.cp, ".
                                    "d.calleNumero, d.direccion, ".
                                    "(SELECT 1 FROM alSimbolosDetenidasTbl sd WHERE sd.distribuidor = dc.distribuidorCentro ".
                                        "AND sd.simboloUnidad IS NULL) AS distribuidorDetenido, dc.zonaGeografica, ".
                                        "(SELECT concat(re.nombre,' - ',re.color) as color FROM caregionestbl re WHERE re.idRegion = dc.idRegion) as descRegion ".
                                    "FROM caDistribuidoresCentrosTbl dc, caPlazasTbl pl, caDireccionesTbl d, caColoniasTbl c, ".
                                        "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p ".$lsWhereStr." LIMIT 1";

        $rs = fn_ejecuta_query($sqlGetDistribuidoresStr);
        //echo json_encode($rs);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['distDesc'] = $rs['root'][$iInt]['distribuidorCentro']." - ".$rs['root'][$iInt]['descripcionCentro'];
            $rs['root'][$iInt]['dirEntregaCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];
        }
            
        echo json_encode($rs);
    }

    function getDistribuidoresDetenidos(){
        $lsWhereStr = "WHERE sd.distribuidor = dc.distribuidorCentro";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroCentroDistHdn'], "sd.centroDistribucion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDistribuidorHdn'], "sd.distribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSimboloUnidadHdn'], "sd.simboloUnidad", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetDistribuidoresDetenidosStr = "SELECT sd.simboloUnidad, sd.distribuidor AS distribuidorCentro, ".
                                            "sd.centroDistribucion, dc.descripcionCentro, ".
                                            "(SELECT 1 FROM alSimbolosDetenidasTbl sd2 WHERE sd2.distribuidor = dc.distribuidorCentro ".
                                                "AND sd2.simboloUnidad IS NULL) AS distribuidorDetenido ".
                                            "FROM alSimbolosDetenidasTbl sd, caDistribuidoresCentrosTbl dc ".
                                            $lsWhereStr." GROUP BY sd.distribuidor";

        $rs = fn_ejecuta_query($sqlGetDistribuidoresDetenidosStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['distDesc'] = $rs['root'][$iInt]['distribuidorCentro']." - ".$rs['root'][$iInt]['descripcionCentro'];
        }

        echo json_encode($rs);

    }

    //OBTIENE LOS DISTRIBUIDORES QUE PUEDES HACER VIAJE DESDE EL CENTRO DE DISTRIBUCION AL QUE ESTAS LOGUEADO
    function getDistribuidoresKilometros(){

        

        $lsWhereStr = "WHERE (SELECT dc2.idPlaza FROM caDistribuidoresCentrosTbl dc2 ".
                      "WHERE distribuidorCentro='".$_SESSION['usuCompania']."') = kp.idPlazaOrigen ".
                      "AND dc.idPlaza = kp.idPlazaDestino ".
                      "AND d.idColonia = c.idColonia ".
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ".
                      "AND d.direccion = dc.direccionEntrega ".
                      "AND pl.idPlaza = dc.idPlaza "; 


        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTipoHdn'], "dc.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDistribuidorTxt'], "dc.distribuidorCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDescripcionTxt'], "dc.descripcionCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPlazaHdn'], "dc.idPlaza", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroObservacionesTxa'], "dc.observaciones", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTelefonoTxt'], "dc.telefono", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroFaxTxt'], "dc.fax", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroContactoTxt'], "dc.contacto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEmailTxt'], "dc.eMail", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRutaDestinoTxt'], "dc.rutaDestino", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSucursalDeHdn'], "dc.sucursalDe", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }    
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSueldoTxt'], "dc.sueldoGarantizado", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroIdRegionHdn'], "dc.idRegion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEstatusHdn'], "dc.estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }


        $sqlGetDistKmStr = "SELECT dc.distribuidorCentro, dc.descripcionCentro, dc.tipoDistribuidor,kp.idPlazaOrigen, dc.idPlaza as idPlazaDestino, dc.direccionEntrega, ".
                           "d.calleNumero, c.colonia, c.cp, m.municipio, e.estado, p.pais, kp.kilometros, ".
                           "(SELECT plaza FROM caplazastbl pl2 WHERE kp.idPlazaOrigen = pl2.idPlaza) as plazaOrigen, ".
                           "pl.plaza as plazaDestino FROM caDistribuidoresCentrosTbl dc, caKilometrosPlazaTbl kp, caDireccionesTbl d, caColoniasTbl c,".
                           "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p, caPlazasTbl pl ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetDistKmStr);
       //echo json_encode($sqlGetDistKmStr);

        
        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['direccionCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];

            $rs['root'][$iInt]['distDesc'] = $rs['root'][$iInt]['distribuidorCentro']." - ".$rs['root'][$iInt]['descripcionCentro'];
        }
            
        echo json_encode($rs);
    }

    //OBTIENE LOS DISTRIBUIDORES QUE TIENEN KILOMETROS ASIGNADOS DESDE OTRO DISTRIBUIDOR
    function getDistribuidoresKilometrosFromDistribuidor(){
        $lsWhereStr = "WHERE (SELECT dc2.idPlaza FROM caDistribuidoresCentrosTbl dc2 ".
                      "WHERE distribuidorCentro='".$_REQUEST['catDistCentroDistribuidorTxt']."') = kp.idPlazaOrigen ".
                      "AND dc.idPlaza = kp.idPlazaDestino ".
                      "AND d.idColonia = c.idColonia ".
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ".
                      "AND d.direccion = dc.direccionEntrega ".
                      "AND pl.idPlaza = dc.idPlaza ".
                      "AND (dc.tipoDistribuidor = 'DI' OR dc.tipoDistribuidor = 'DE' OR dc.tipoDistribuidor = 'CD')"; 

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTipoHdn'], "dc.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDescripcionTxt'], "dc.descripcionCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPlazaHdn'], "dc.idPlaza", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroObservacionesTxa'], "dc.observaciones", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTelefonoTxt'], "dc.telefono", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroFaxTxt'], "dc.fax", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroContactoTxt'], "dc.contacto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEmailTxt'], "dc.eMail", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRutaDestinoTxt'], "dc.rutaDestino", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSucursalDeHdn'], "dc.sucursalDe", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }    
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSueldoTxt'], "dc.sueldoGarantizado", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroIdRegionHdn'], "dc.idRegion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEstatusHdn'], "dc.estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetDistKmStr = "SELECT dc.distribuidorCentro, dc.descripcionCentro, kp.idPlazaOrigen, dc.idPlaza as idPlazaDestino, dc.direccionEntrega, ".
                           "d.calleNumero, c.colonia, c.cp, m.municipio, e.estado, p.pais, kp.kilometros, ".
                           "(SELECT plaza FROM caplazastbl pl2 WHERE kp.idPlazaOrigen = pl2.idPlaza) as plazaOrigen, ".
                           "pl.plaza as plazaDestino FROM caDistribuidoresCentrosTbl dc, caKilometrosPlazaTbl kp, caDireccionesTbl d, caColoniasTbl c,".
                           "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p, caPlazasTbl pl ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetDistKmStr);
        
        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['direccionCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];

            $rs['root'][$iInt]['distDesc'] = $rs['root'][$iInt]['distribuidorCentro']." - ".$rs['root'][$iInt]['descripcionCentro'];
        }
            
        echo json_encode($rs);
    }

    function getSucursalCombo(){

        $sqlGetSucursalComboStr = "SELECT distribuidorCentro, descripcionCentro " .
                                  "FROM caDistribuidoresCentrosTbl  ".
                                  "WHERE sucursalDe is NULL ".
                                  "AND tipoDistribuidor = 'DI' ".
                                  "AND distribuidorCentro != '".$_REQUEST['catDistribuidoresDistribuidorTxt']."'";       
        
        $rs = fn_ejecuta_query($sqlGetSucursalComboStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['descDistribuidor'] = $rs['root'][$iInt]['distribuidorCentro']." - ".$rs['root'][$iInt]['descripcionCentro'];
        }
        

        echo json_encode($rs);      
    }

    function getDireccionDistViajes(){
        $lsWhereStr = "WHERE d.idColonia = c.idColonia ".
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ".
                      "AND pl.idPlaza = dc.idPlaza ".
                      "AND d.direccion = dc.direccionEntrega "; 

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['trViajesTractorDistribuidorHdn'], "dc.distribuidorCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetDireccionDistViajesStr = "SELECT dc.distribuidorCentro, dc.descripcionCentro, dc.direccionEntrega, dc.idPlaza, ".
                                        "d.calleNumero, c.colonia, c.cp, m.municipio, e.estado, p.pais, pl.plaza ".
                                        "FROM caDistribuidoresCentrosTbl dc, caDireccionesTbl d, caColoniasTbl c, ".
                                        "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p, caPlazasTbl pl ".$lsWhereStr;
             
        $rs = fn_ejecuta_query($sqlGetDireccionDistViajesStr);
        
        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['direccionCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];
        }
            
        return $rs;
    }

    function addDistribuidor(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catDistribuidoresDistribuidorTxt'] == ""){
            $e[] = array('id'=>'catDistribuidoresDistribuidorTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        /*if($_REQUEST['catDistribuidoresRfcTxt'] == ""){
            $e[] = array('id'=>'catDistribuidoresRfcTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }*/
        if($_REQUEST['catDistribuidoresDescripcionTxt'] == ""){
            $e[] = array('id'=>'catDistribuidoresDescripcionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistCentroTipoHdn'] == ""){
            $e[] = array('id'=>'catDistCentroTipoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresPlazaHdn'] == ""){
            $e[] = array('id'=>'catDistribuidoresPlazaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresDirFiscalHdn'] == ""){
            $e[] = array('id'=>'catDistribuidoresDirFiscalHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresDirEntregaHdn'] == ""){
            $e[] = array('id'=>'catDistribuidoresDirEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresEstatusHdn'] == ""){
            $e[] = array('id'=>'catDistribuidoresEstatusHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresAonaCmb'] == ""){
            $e[] = array('id'=>'catDistribuidoresAonaCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true){
            $sqlAddDistribuidorStr = "INSERT INTO caDistribuidoresCentrosTbl ".
                                    "(distribuidorCentro, descripcionCentro, tipoDistribuidor, ".
                                    "rfc, idPlaza, observaciones, telefono, fax, contacto, ".
                                    "eMail, rutaDestino, sucursalDe, direccionFiscal, direccionEntrega, ".
                                    "sueldoGarantizado, idRegion, estatus, tieneRepuve,puerto, ".
                                    "detieneUnidades, homologacion, zonaGeografica) ".
                                  "VALUES (".
                                  "'".$_REQUEST['catDistribuidoresDistribuidorTxt']."', ".
                                  "'".$_REQUEST['catDistribuidoresDescripcionTxt']."', ".
                                  "'".$_REQUEST['catDistCentroTipoHdn']."', ".
                                  replaceEmptyNull("'".$_REQUEST['catDistribuidoresRfcTxt']."'").", ".
                                  $_REQUEST['catDistribuidoresPlazaHdn'].", ".
                                  "'".$_REQUEST['catDistribuidoresObservacionesTxa']."', ".
                                  "'".$_REQUEST['catDistribuidoresTelefonoTxt']."', ".
                                  "'".$_REQUEST['catDistribuidoresFaxTxt']."', ".
                                  "'".$_REQUEST['catDistribuidoresContactoTxt']."', ".
                                  "'".$_REQUEST['catDistribuidoresEmailTxt']."', ".
                                  "'".$_REQUEST['catDistribuidoresRutaDestinoTxt']."', ".
                                  replaceEmptyNull("'".$_REQUEST['catDistribuidoresSucursalDeHdn']."'"). ", ".
                                  $_REQUEST['catDistribuidoresDirFiscalHdn'].", ".
                                  $_REQUEST['catDistribuidoresDirEntregaHdn'].", ".
                                  replaceEmptyNull($_REQUEST['catDistribuidoresSueldoTxt']).", ".
                                  replaceEmptyNull($_REQUEST['catDistribuidoresIdRegionHdn']).", ".
                                  "'".$_REQUEST['catDistribuidoresEstatusHdn']."', ".
                                  "NULL,".
                                  replaceEmptyNull("'".$_REQUEST['catDistrinuidoresPuertoHdn']."'").", ".
                                  $_REQUEST['catDistribuidoresDetieneUnidadesHdn'].",".
                                  "0, '".
                                  $_REQUEST['catDistribuidoresAonaCmb']."')";

            $rs = fn_ejecuta_Add($sqlAddDistribuidorStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlAddDistribuidorStr;
                $a['successMessage'] = getDistribuidoresSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddDistribuidorStr;
            }
        }
        //Si se inserta correctamente, se agrega el id a las direcciones correspondientes
        if ($a['success'] == true) {

             //UPDATE a campos NULL de las direcciones por el distribuidor ya creado
            //ACTUALIZO LAS DIRECCIONES NULAS ASIGNANDOLES EL DISTRIBUIDOR
            $sqlUpdDireccionesNULLStr = "UPDATE cadireccionestbl ".
                                                "SET distribuidor= '".$_REQUEST['catDistribuidoresDistribuidorTxt']."' ".
                                                "WHERE distribuidor IS NULL".
                                                " AND tipoDireccion = '".$_REQUEST['catDistCentroTipoHdn']."'"; 
                
                $rs = fn_ejecuta_Upd($sqlUpdDireccionesNULLStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlUpdDireccionesNULLStr;
                } else {
                    $a['success'] = false;
                }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function updDistribuidor() {
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catDistribuidoresDistribuidorTxt'] == ""){
            $e[] = array('id'=>'catDistribuidoresDistribuidorTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresDescripcionTxt'] == ""){
            $e[] = array('id'=>'catDistribuidoresDescripcionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistCentroTipoHdn'] == ""){
            $e[] = array('id'=>'catDistCentroTipoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        /*if($_REQUEST['catDistribuidoresRfcTxt'] == ""){
            $e[] = array('id'=>'catDistribuidoresRfcTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }*/
        if($_REQUEST['catDistribuidoresPlazaHdn'] == ""){
            $e[] = array('id'=>'catDistribuidoresPlazaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresDirFiscalHdn'] == ""){
            $e[] = array('id'=>'catDistribuidoresDirFiscalHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresDirEntregaHdn'] == ""){
            $e[] = array('id'=>'catDistribuidoresDirEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresEstatusHdn'] == ""){
            $e[] = array('id'=>'catDistribuidoresEstatusHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catDistribuidoresAonaCmb'] == ""){
            $e[] = array('id'=>'catDistribuidoresAonaCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            $sqlUpdDistribuidoresStr = "UPDATE caDistribuidoresCentrosTbl ".
                                       "SET descripcionCentro = '".$_REQUEST['catDistribuidoresDescripcionTxt']."', ".
                                       "tipoDistribuidor = '".$_REQUEST['catDistCentroTipoHdn']."', ".
                                       "rfc =".replaceEmptyNull("'".$_REQUEST['catDistribuidoresRfcTxt']."'").", ".
                                       "idPlaza =".$_REQUEST['catDistribuidoresPlazaHdn'].", ".
                                       "observaciones = '".$_REQUEST['catDistribuidoresObservacionesTxa']."', ".
                                       "telefono = '".$_REQUEST['catDistribuidoresTelefonoTxt']."', ".
                                       "fax = '".$_REQUEST['catDistribuidoresFaxTxt']."', ".
                                       "contacto = '".$_REQUEST['catDistribuidoresContactoTxt']."', ".
                                       "email = '".$_REQUEST['catDistribuidoresEmailTxt']."', ".
                                       "rutaDestino = '".$_REQUEST['catDistribuidoresRutaDestinoTxt']."', ".
                                       "sucursalDe =".replaceEmptyNull("'".$_REQUEST['catDistribuidoresSucursalDeHdn']."'").", ".
                                       "direccionFiscal =".$_REQUEST['catDistribuidoresDirFiscalHdn'].", ".
                                       "direccionEntrega =".$_REQUEST['catDistribuidoresDirEntregaHdn'].", ".
                                       "sueldoGarantizado = ".replaceEmptyDec($_REQUEST['catDistribuidoresSueldoTxt']).", ".
                                       "idRegion=".replaceEmptyNull($_REQUEST['catDistribuidoresIdRegionHdn']).", ".
                                       "estatus = '".$_REQUEST['catDistribuidoresEstatusHdn']."', ".
                                       "tieneRepuve = 0, ".
                                       "detieneUnidades = ".$_REQUEST['catDistribuidoresDetieneUnidadesHdn'].", ".
                                       "puerto = '".$_REQUEST['catDistrinuidoresPuertoHdn']."', ".
                                       "zonaGeografica = '".$_REQUEST['catDistribuidoresAonaCmb']."' ".
                                       "WHERE distribuidorCentro= '".$_REQUEST['catDistribuidoresDistribuidorTxt']."'";
            
            $rs = fn_ejecuta_Upd($sqlUpdDistribuidoresStr);




            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlUpdDistribuidoresStr;
                $a['successMessage'] = getDistribuidoresUpdateMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdDistribuidoresStr;
            }
        }

        if ($a['success'] == true) {

            //ACTUALIZO LAS DIRECCIONES NULAS ASIGNANDOLES EL DISTRIBUIDOR
            $sqlUpdDireccionesNULLStr = "UPDATE cadireccionestbl ".
                                                "SET distribuidor= '".$_REQUEST['catDistribuidoresDistribuidorTxt']."' ".
                                                "WHERE distribuidor IS NULL".
                                                " AND tipoDireccion = '".$_REQUEST['catDistCentroTipoHdn']."'"; 

            $rs = fn_ejecuta_Upd($sqlUpdDireccionesNULLStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlUpdDireccionesNULLStr;
            } else {
                $a['successMessage'] =getDistribuidoresUpdateNoDirectionUpdMsg();
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    //DESTINOS ESPECIALES, CENTROS DE DISTRIBUCION y PATIOS
    function addDistribuidorEspecial(){
        
        switch($_REQUEST['catDistCentroTipoHdn']){
            case 'DE':
                $RQdist = $_REQUEST['catDestinosEspecialesDistribuidorTxt'];
                $RQdesc = $_REQUEST['catDestinosEspecialesDescripcionTxt'];
                $RQtipo = $_REQUEST['catDistCentroTipoHdn'];
                $RQplaza = $_REQUEST['catDestinosEspecialesPlazaHdn'];
                $RQobserv = $_REQUEST['catDestinosEspecialesObservacionesTxa'];
                $RQtel = $_REQUEST['catDestinosEspecialesTelefonoTxt'];
                $RQfax = $_REQUEST['catDestinosEspecialesFaxTxt'];
                $RQcontacto = $_REQUEST['catDestinosEspecialesContactoTxt'];
                $RQemail = $_REQUEST['catDestinosEspecialesEmailTxt'];
                $RQdestino = $_REQUEST['catDestinosEspecialesRutaDestinoTxt'];
                $RQsueldo = $_REQUEST['catDestinosEspecialesSueldoTxt'];
                $RQregion = $_REQUEST['catDestinosEspecialesIdRegionHdn'];
                $RQestatus = $_REQUEST['catDestinosEspecialesEstatusHdn'];
                $RQrepuve = "0";
                $RQdetiene = $_REQUEST['catDestinosEspecialesDetieneUnidadesHdn'];
                /*$RQcalleNum = $_REQUEST['catDestinosEspecialesCalleNumeroTxt'];
                $RQcolonia = $_REQUEST['catDestinosEspecialesIdColoniaHdn'];
                $RQtipoDir = $_REQUEST['catDistCentroTipoDireccionHdn'];*/
                $RQdireccionFis = $_REQUEST['catDestinosEspecialesDirFiscalHdn'];
                $RQdireccionDes = $_REQUEST['catDestinosEspecialesDirEntregaHdn'];
                $RQcalleNumFis = $_REQUEST['catDestinosEspecialesCalleNumeroFiscalTxt'];
                $RQcalleNumDes = $_REQUEST['catDestinosEspecialesCalleNumeroEntregaTxt'];
                $RQcoloniaFis = $_REQUEST['catDestinosEspecialesIdColoniaFiscalHdn'];
                $RQcoloniaDes = $_REQUEST['catDestinosEspecialesIdColoniaEntregaHdn'];
                $RQtipoDir = $_REQUEST['catDistCentroTipoDireccionHdn'];
                $RQrfc = $_REQUEST['catDestinosEspecialesRfcTxt'];
                break;
            case 'CD':
                $RQdist = $_REQUEST['catCentrosDistribucionDistribuidorTxt'];
                $RQdesc = $_REQUEST['catCentrosDistribucionDescripcionTxt'];
                $RQtipo = $_REQUEST['catDistCentroTipoHdn'];
                $RQplaza = $_REQUEST['catCentrosDistribucionPlazaHdn'];
                $RQobserv = $_REQUEST['catCentrosDistribucionObservacionesTxa'];
                $RQtel = $_REQUEST['catCentrosDistribucionTelefonoTxt'];
                $RQfax = $_REQUEST['catCentrosDistribucionFaxTxt'];
                $RQcontacto = $_REQUEST['catCentrosDistribucionContactoTxt'];
                $RQemail = $_REQUEST['catCentrosDistribucionEmailTxt'];
                $RQdestino = $_REQUEST['catCentrosDistribucionRutaDestinoTxt'];
                $RQsueldo = $_REQUEST['catCentrosDistribucionSueldoTxt'];
                $RQregion = $_REQUEST['catCentrosDistribucionIdRegionHdn'];
                $RQestatus = $_REQUEST['catCentrosDistribucionEstatusHdn'];
                $RQrepuve = $_REQUEST['catCentrosDistribucionRepuveHdn'];
                $RQdetiene = $_REQUEST['catCentrosDistribucionDetieneUnidadesHdn'];
                /*$RQcalleNum = $_REQUEST['catCentrosDistribucionCalleNumeroTxt'];
                $RQcolonia = $_REQUEST['catCentrosDistribucionIdColoniaHdn'];
                $RQtipoDir = $_REQUEST['catDistCentroTipoDireccionHdn'];*/
                $RQdireccionFis = $_REQUEST['catCentrosDistribucionDirFiscalHdn'];
                $RQdireccionDes = $_REQUEST['catCentrosDistribucionDirEntregaHdn'];
                $RQcalleNumFis = $_REQUEST['catCentrosDistribucionCalleNumeroFiscalTxt'];
                $RQcalleNumDes = $_REQUEST['catCentrosDistribucionCalleNumeroEntregaTxt'];
                $RQcoloniaFis = $_REQUEST['catCentrosDistribucionIdColoniaFiscalHdn'];
                $RQcoloniaDes = $_REQUEST['catCentrosDistribucionIdColoniaEntregaHdn'];
                $RQtipoDir = $_REQUEST['catDistCentroTipoDireccionHdn'];
                break;
            case 'PA':
                $RQdist = $_REQUEST['catPatiosDistribuidorTxt'];
                $RQdesc = $_REQUEST['catPatiosDescripcionTxt'];
                $RQtipo = $_REQUEST['catDistCentroTipoHdn'];
                $RQplaza = $_REQUEST['catPatiosPlazaHdn'];
                $RQobserv = $_REQUEST['catPatiosObservacionesTxa'];
                $RQtel = $_REQUEST['catPatiosTelefonoTxt'];
                $RQfax = $_REQUEST['catPatiosFaxTxt'];
                $RQcontacto = $_REQUEST['catPatiosContactoTxt'];
                $RQemail = $_REQUEST['catPatiosEmailTxt'];
                $RQdestino = $_REQUEST['catPatiosRutaDestinoTxt'];
                $RQsueldo = $_REQUEST['catPatiosSueldoTxt'];
                $RQregion = $_REQUEST['catPatiosIdRegionHdn'];
                $RQestatus = $_REQUEST['catPatiosEstatusHdn'];
                $RQrepuve = "0";
                $RQdetiene = $_REQUEST['catPatiosDetieneUnidadesHdn'];
                $RQcalleNum = $_REQUEST['catPatiosCalleNumeroTxt'];
                $RQcolonia = $_REQUEST['catPatiosIdColoniaHdn'];
                $RQtipoDir = $_REQUEST['catDistCentroTipoDireccionHdn'];
                break;
        }

        $a = array();
        $e = array();
        $a['success'] = true;

        if($RQdist == ""){
            $e[] = array('id'=>'DistribuidorTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQdesc == ""){
            $e[] = array('id'=>'DescripcionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQtipo == ""){
            $e[] = array('id'=>'TipoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQplaza == ""){
            $e[] = array('id'=>'PlazaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQestatus == ""){
            $e[] = array('id'=>'EstatusHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQrepuve == ""){
            $e[] = array('id'=>'RepuveHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        /*if($RQcalleNum == ""){
            $e[] = array('id'=>'CalleNumeroTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQcolonia == ""){
            $e[] = array('id'=>'IdColoniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }*/
        
        if($RQdireccionFis == ""){
            $e[] = array('id'=>'DireccionFiscal','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQdireccionDes == ""){
            $e[] = array('id'=>'DireccionEntrega','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }       
        if($RQcalleNumFis == ""){
            $e[] = array('id'=>'CalleNumeroFiscal','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQcalleNumDes == ""){
            $e[] = array('id'=>'CalleNumeroEntrega','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQcoloniaFis == ""){
            $e[] = array('id'=>'IdColoniaFiscalHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQcoloniaDes == ""){
            $e[] = array('id'=>'IdColoniaEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQtipoDir == ""){
            $e[] = array('id'=>'TipoDireccionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }       

          

        if ($a['success'] == true){
            $sqlAddDistribuidorEspecialStr = "INSERT INTO caDistribuidoresCentrosTbl ".
                                    "(distribuidorCentro, descripcionCentro, tipoDistribuidor, rfc, idPlaza, ".
                                    "observaciones, telefono, fax, contacto, eMail, rutaDestino, sucursalDe, ".
                                    "direccionFiscal, direccionEntrega, sueldoGarantizado, idRegion, estatus, ".
                                    "tieneRepuve, detieneUnidades, homologacion) ".
                                        "VALUES (".
                                        "'".$RQdist."', ".
                                        "'".$RQdesc."', ".
                                        "'".$RQtipo."', ".
                                        replaceEmptyNull("'".$RQrfc."'").", ".
                                        $RQplaza.", ".
                                        "'".$RQobserv."', ".
                                        "'".$RQtel."', ".
                                        "'".$RQfax."', ".
                                        "'".$RQcontacto."', ".
                                        "'".$RQemail."', ".
                                        "'".$RQdestino."', ".
                                        "NULL, ". //No tiene sucursal
                                        $RQdireccionFis.", ".
                                        $RQdireccionDes.", ".
                                        replaceEmptyDec($RQsueldo).", ".
                                        replaceEmptyNull($RQregion).", ".
                                        "'".$RQestatus."', ".
                                        $RQrepuve.", ".
                                        $RQdetiene.", 0)";//0 en homoloagación
            
            $rs = fn_ejecuta_Add($sqlAddDistribuidorEspecialStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql1'] = $sqlAddDistribuidorEspecialStr;
                

                //Finalmente se agrega el distribuidor a la dirección
                //updDirecciones($RQdireccionFis, $RQcalleNumFis, $RQcoloniaFis, $RQdist."|", $RQtipoDir);
                $sqlUpdateNullDir = "UPDATE caDireccionesTbl SET distribuidor = '". $RQdist. "'".
                      "WHERE distribuidor IS NULL AND tipoDireccion = '".$RQtipoDir."';";

                $rs = fn_ejecuta_Upd($sqlUpdateNullDir);
                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['successMessage'] = 'Se ha agregado correctamente';
                }else{
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdateNullDir;
                }    

            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddDistribuidorEspecialStr;
                
                //Si el distribuidor no se crea, se borra la dirección asociada
                dltDireccion($idDireccion);
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function updDistribuidorEspecial(){
        $a = array();
        $e = array();
        $a['success'] = true;

        switch($_REQUEST['catDistCentroTipoHdn']){
            case 'DE':
                $RQdist = $_REQUEST['catDestinosEspecialesDistribuidorTxt'];
                $RQdesc = $_REQUEST['catDestinosEspecialesDescripcionTxt'];
                $RQtipo = $_REQUEST['catDistCentroTipoHdn'];
                $RQplaza = $_REQUEST['catDestinosEspecialesPlazaHdn'];
                $RQobserv = $_REQUEST['catDestinosEspecialesObservacionesTxa'];
                $RQtel = $_REQUEST['catDestinosEspecialesTelefonoTxt'];
                $RQfax = $_REQUEST['catDestinosEspecialesFaxTxt'];
                $RQcontacto = $_REQUEST['catDestinosEspecialesContactoTxt'];
                $RQemail = $_REQUEST['catDestinosEspecialesEmailTxt'];
                $RQdestino = $_REQUEST['catDestinosEspecialesRutaDestinoTxt'];
                $RQsueldo = $_REQUEST['catDestinosEspecialesSueldoTxt'];
                $RQregion = $_REQUEST['catDestinosEspecialesIdRegionHdn'];
                $RQestatus = $_REQUEST['catDestinosEspecialesEstatusHdn'];
                $RQrepuve = "0";
                $RQdetiene = $_REQUEST['catDestinosEspecialesDetieneUnidadesHdn'];
                $RQdireccionFis = $_REQUEST['catDestinosEspecialesDirFiscalHdn'];
                $RQdireccionDes = $_REQUEST['catDestinosEspecialesDirEntregaHdn'];
                $RQcalleNumFis = $_REQUEST['catDestinosEspecialesCalleNumeroFiscalTxt'];
                $RQcalleNumDes = $_REQUEST['catDestinosEspecialesCalleNumeroEntregaTxt'];
                $RQcoloniaFis = $_REQUEST['catDestinosEspecialesIdColoniaFiscalHdn'];
                $RQcoloniaDes = $_REQUEST['catDestinosEspecialesIdColoniaEntregaHdn'];
                $RQtipoDir = $_REQUEST['catDistCentroTipoDireccionHdn'];
                $RQrfc = $_REQUEST['catDestinosEspecialesRfcTxt'];
                break;
            case 'CD':
                $RQdist = $_REQUEST['catCentrosDistribucionDistribuidorTxt'];
                $RQdesc = $_REQUEST['catCentrosDistribucionDescripcionTxt'];
                $RQtipo = $_REQUEST['catDistCentroTipoHdn'];
                $RQplaza = $_REQUEST['catCentrosDistribucionPlazaHdn'];
                $RQobserv = $_REQUEST['catCentrosDistribucionObservacionesTxa'];
                $RQtel = $_REQUEST['catCentrosDistribucionTelefonoTxt'];
                $RQfax = $_REQUEST['catCentrosDistribucionFaxTxt'];
                $RQcontacto = $_REQUEST['catCentrosDistribucionContactoTxt'];
                $RQemail = $_REQUEST['catCentrosDistribucionEmailTxt'];
                $RQdestino = $_REQUEST['catCentrosDistribucionRutaDestinoTxt'];
                $RQsueldo = $_REQUEST['catCentrosDistribucionSueldoTxt'];
                $RQregion = $_REQUEST['catCentrosDistribucionIdRegionHdn'];
                $RQestatus = $_REQUEST['catCentrosDistribucionEstatusHdn'];
                $RQrepuve = $_REQUEST['catCentrosDistribucionRepuveHdn'];
                $RQdetiene = $_REQUEST['catCentrosDistribucionDetieneUnidadesHdn'];
                $RQdireccionFis = $_REQUEST['catCentrosDistribucionDirFiscalHdn'];
                $RQdireccionDes = $_REQUEST['catCentrosDistribucionDirEntregaHdn'];
                $RQcalleNumFis = $_REQUEST['catCentrosDistribucionCalleNumeroFiscalTxt'];
                $RQcalleNumDes = $_REQUEST['catCentrosDistribucionCalleNumeroEntregaTxt'];
                $RQcoloniaFis = $_REQUEST['catCentrosDistribucionIdColoniaFiscalHdn'];
                $RQcoloniaDes = $_REQUEST['catCentrosDistribucionIdColoniaEntregaHdn'];
                $RQtipoDir = $_REQUEST['catDistCentroTipoDireccionHdn'];
                break;
            case 'PA':
                $RQdist = $_REQUEST['catPatiosDistribuidorTxt'];
                $RQdesc = $_REQUEST['catPatiosDescripcionTxt'];
                $RQtipo = $_REQUEST['catDistCentroTipoHdn'];
                $RQplaza = $_REQUEST['catPatiosPlazaHdn'];
                $RQobserv = $_REQUEST['catPatiosObservacionesTxa'];
                $RQtel = $_REQUEST['catPatiosTelefonoTxt'];
                $RQfax = $_REQUEST['catPatiosFaxTxt'];
                $RQcontacto = $_REQUEST['catPatiosContactoTxt'];
                $RQemail = $_REQUEST['catPatiosEmailTxt'];
                $RQdestino = $_REQUEST['catPatiosRutaDestinoTxt'];
                $RQsueldo = $_REQUEST['catPatiosSueldoTxt'];
                $RQregion = $_REQUEST['catPatiosIdRegionHdn'];
                $RQestatus = $_REQUEST['catPatiosEstatusHdn'];
                $RQrepuve = "0";
                $RQdetiene = $_REQUEST['catPatiosDetieneUnidadesHdn'];
                $RQdireccion = $_REQUEST['catDistCentroDireccionHdn'];
                $RQcalleNum = $_REQUEST['catPatiosCalleNumeroTxt'];
                $RQcolonia = $_REQUEST['catPatiosIdColoniaHdn'];
                $RQtipoDir = $_REQUEST['catDistCentroTipoDireccionHdn'];
                break;
        }

        if($RQdireccionFis == ""){
            $e[] = array('id'=>'DireccionFiscalHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQdireccionDes == ""){
            $e[] = array('id'=>'DireccionEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQdist == ""){
            $e[] = array('id'=>'DistribuidorTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQdesc == ""){
            $e[] = array('id'=>'DescripcionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQtipo == ""){
            $e[] = array('id'=>'TipoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQplaza == ""){
            $e[] = array('id'=>'PlazaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQestatus == ""){
            $e[] = array('id'=>'EstatusHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQrepuve == ""){
            $e[] = array('id'=>'RepuveHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQcalleNumFis == ""){
            $e[] = array('id'=>'CalleNumeroFiscal','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQcalleNumDes == ""){
            $e[] = array('id'=>'CalleNumeroEntrega','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQcoloniaFis == ""){
            $e[] = array('id'=>'IdColoniaFiscalHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQcoloniaDes == ""){
            $e[] = array('id'=>'IdColoniaEntregaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($RQtipoDir == ""){
            $e[] = array('id'=>'TipoDireccionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }   

        if ($a['success'] == true){
            $sqlUpdDestinoEspecialStr = "UPDATE caDistribuidoresCentrosTbl ".
                                        "SET descripcionCentro='".$RQdesc."',".
                                        "tipoDistribuidor='".$RQtipo."', ".
                                        "idPlaza=".$RQplaza.", ".
                                        "observaciones='".$RQobserv."', ".
                                        "RFC='".$RQrfc."', ".                                        
                                        "telefono='".$RQtel."', ".
                                        "fax='".$RQfax."', ".
                                        "contacto='".$RQcontacto."', ".
                                        "eMail='".$RQemail."', ".
                                        "rutaDestino='".$RQdestino."', ".
                                        //"NULL, ". //No tiene sucursal
                                        "direccionFiscal=".$RQdireccionFis.", ".
                                        "direccionEntrega=".$RQdireccionDes.", ".
                                        "sueldoGarantizado=".replaceEmptyDec($RQsueldo).", ".
                                        "idRegion=".replaceEmptyNull("'".$RQregion."'").",".
                                        "estatus='".$RQestatus."', ".
                                        "tieneRepuve=".$RQrepuve.", ".
                                        "detieneUnidades= ".$RQdetiene." ".
                                        "WHERE distribuidorCentro='".$RQdist."'";

            $rs = fn_ejecuta_Upd($sqlUpdDestinoEspecialStr);
            
            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlUpdDestinoEspecialStr;
                $a['successMessage'] = getDistEspecialUpdMsg($RQtipo);
                //UPDATE DE LA DIRECCION
                //$dataFis = updDirecciones($RQdireccionFis, $RQcalleNumFis, $RQcoloniaFis, $RQdist."|", $RQtipoDir);
                //$dataDes = updDirecciones($RQdireccionDes, $RQcalleNumDes, $RQcoloniaDes, $RQdist."|", $RQtipoDir);

                //Finalmente se agrega el distribuidor a la dirección
                //updDirecciones($RQdireccionFis, $RQcalleNumFis, $RQcoloniaFis, $RQdist."|", $RQtipoDir);
                $sqlUpdateNullDir = "UPDATE caDireccionesTbl SET distribuidor = '". $RQdist. "'".
                                    "WHERE distribuidor IS NULL AND tipoDireccion = '".$RQtipo."';";

                $rs = fn_ejecuta_query($sqlUpdateNullDir);
                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlUpdDestinoEspecialStr;
                    $a['successMessage'] = getDistEspecialUpdMsg($RQtipo);
                }

            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdDestinoEspecialStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

     function getDistribuidoresPuerto(){

        
        $lsWhereStr = "WHERE d.idColonia = c.idColonia ".
                      "AND c.idMunicipio = m.idMunicipio ".
                      "AND m.idEstado = e.idEstado ".
                      "AND e.idPais = p.idPais ".
                      "AND pl.idPlaza = dc.idPlaza ".
                      "AND d.direccion = dc.direccionEntrega "; 

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTipoHdn'], "dc.tipoDistribuidor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDistribuidorTxt'], "dc.distribuidorCentro", 2);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroDescripcionTxt'], "dc.descripcionCentro", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPlazaHdn'], "dc.idPlaza", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroObservacionesTxa'], "dc.observaciones", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroTelefonoTxt'], "dc.telefono", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroFaxTxt'], "dc.fax", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroContactoTxt'], "dc.contacto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEmailTxt'], "dc.eMail", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRutaDestinoTxt'], "dc.rutaDestino", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSucursalDeHdn'], "dc.sucursalDe", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }    
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroSueldoTxt'], "dc.sueldoGarantizado", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroIdRegionHdn'], "dc.idRegion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroEstatusHdn'], "dc.estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRepuveHdn'], "dc.tieneRepuve", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }        
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroRFCHdn'], "dc.rfc", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catDistCentroPuertoHdn'], "dc.puerto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }                 
        if ($_REQUEST['catDistCentroDetenidoHdn'] == '0'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "dc.distribuidorCentro NOT IN (SELECT IFNULL(de2.distribuidor, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                        "WHERE de2.simboloUnidad IS NULL)");
        } else if ($_REQUEST['catDistCentroDetenidoHdn'] == '1'){
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, "dc.distribuidorCentro IN (SELECT IFNULL(de2.distribuidor, 1) FROM alSimbolosDetenidasTbl de2 ".
                                                            "WHERE de2.simboloUnidad IS NULL)");
        }

        $sqlGetDistribuidoresStr = "SELECT dc.*, pl.idPlaza, pl.plaza, p.pais, e.estado, m.municipio, c.colonia, c.cp, ".
                                    "d.calleNumero, d.direccion, ".
                                    "(SELECT 1 FROM alSimbolosDetenidasTbl sd WHERE sd.distribuidor = dc.distribuidorCentro ".
                                        "AND sd.simboloUnidad IS NULL) AS distribuidorDetenido, dc.zonaGeografica, ".
                                        "(SELECT concat(re.nombre,' - ',re.color) as color FROM caregionestbl re WHERE re.idRegion = dc.idRegion) as descRegion ".
                                    "FROM caDistribuidoresCentrosTbl dc, caPlazasTbl pl, caDireccionesTbl d, caColoniasTbl c, ".
                                        "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetDistribuidoresStr);
        //echo json_encode($rs);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['distDesc'] = $rs['root'][$iInt]['distribuidorCentro']." - ".$rs['root'][$iInt]['descripcionCentro'];
            $rs['root'][$iInt]['dirEntregaCompleta'] = $rs['root'][$iInt]['calleNumero'].", ".
                                                      $rs['root'][$iInt]['colonia'].", ".
                                                      $rs['root'][$iInt]['municipio'].", ".
                                                      $rs['root'][$iInt]['estado'].", ".
                                                      $rs['root'][$iInt]['pais'].", ".
                                                      $rs['root'][$iInt]['cp'];
        }
            
        echo json_encode($rs);
    }
?>