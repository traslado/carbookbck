<?php
    session_start();
	$_SESSION['modulo'] = "segUsuarios";

    require("../funciones/generales.php");
    require("../funciones/construct.php");
	require("../funciones/mysqlErrorCatch.php");
	$_REQUEST = trasformUppercase($_REQUEST);
	
    switch($_REQUEST['segUsuariosActionHdn']){
        case 'getUsuarios':
            getUsuarios();
            break;
        case 'addUsuario':
            addUsuario();
            break;
        case 'updUsuario':
            updUsuario();
            break;
        case 'getCompaniasUsuario':
            getCompaniasUsuario();
            break;
        case 'getCompaniasDisponibles':
            getCompaniasDisponibles();
            break;
        case 'getIPUsuario':
            getIPUsuario();
            break;
        case 'getHorariosUsuario':
            getHorariosUsuario();
            break;
        case 'getUsuarioConfig':
            getUsuarioConfig();
            break;
        case 'generaPassword':
            generaPassword();
            break;
        default:
            echo '';
    }

    function getUsuarios(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['segUsuariosIdUsuarioHdn'], "a.idUsuario", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['segUsuariosUsuarioTxt'], "a.usuario", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['segUsuariosNombreTxt'], "a.nombre", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['segUsuariosCorreoElectronicoTxt'], "a.correoElectronico", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['segUsuariosEstatusHdn'], "a.estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetUsuariosStr = "SELECT a.*, " . 
                             "(SELECT b.descripcion " .
                                "FROM genCatGeneralesTbl b " . 
                                "WHERE b.tabla = 'segUsuariosTbl' " . 
                                "AND b.columna = 'estatus' " . 
                                "AND b.valor = a.estatus) AS nombreEstatus " . 
                             "FROM segUsuariosTbl a " . $lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetUsuariosStr);

        $i = 0;
        $response->success = true;
        $response->records = mysql_num_rows($rs);

        while ($row = mysql_fetch_assoc($rs)) {
            $response->root[$i] = $row;
            $i++;
        }

        echo json_encode($response);
    }

    function addUsuario(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['segUsuariosUsuarioTxt'] == ""){
            $e[] = array('id'=>'segUsuariosUsuarioTxt','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "La clave del Usuario es Requerida";
            $a['success'] = false;
        }
        if($_REQUEST['segUsuariosNombreTxt'] == ""){
            $e[] = array('id'=>'segUsuariosNombreTxt','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "El Nombre del Usuario es Requerido";
            $a['success'] = false;
        }
        if($_REQUEST['segUsuariosCorreoElectronicoTxt'] == ""){
            $e[] = array('id'=>'segUsuariosCorreoElectronicoTxt','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "El Correo Electrónico es Requerido";
            $a['success'] = false;
        }
        if($_REQUEST['segUsuariosEstatusHdn'] == ""){
            $e[] = array('id'=>'segUsuariosEstatusHdn','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "El Estatus es Requerido";
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            //Checar next increment value
            $sqlCheckNextValStr = "SHOW TABLE STATUS WHERE name = 'segUsuariosTbl'";

            $rs = fn_ejecuta_query($sqlCheckNextValStr);

            $row = mysql_fetch_array($rs);
            $idUsuario = $row['Auto_increment'];
            $pwd = generaPassword();
            $a['idUsuario'] = $idUsuario;
            $a['password'] = $pwd;
            $ipRest = $_REQUEST['segUsuariosIpHdn'] == "" ? 0 : 1;
            $horarioRest = $_REQUEST['segUsuariosHorariosDiasHdn'] == "" ? 0 : 1;

            $sqlAddUsuarioStr = "INSERT INTO segUsuariosTbl (usuario, password, nombre, correoElectronico, restriccionPorIP, ".
                                "restriccionPorHorario, estatus, wallpaper, theme,tipoUsuario)".
                                "VALUES(".
                                "'".$_REQUEST['segUsuariosUsuarioTxt']."',".
                                "'".md5($pwd)."',".
                                "'".$_REQUEST['segUsuariosNombreTxt']."',".
                                "'".$_REQUEST['segUsuariosCorreoElectronicoTxt']."',".
                                $ipRest.",". 
                                $horarioRest.",".
                                "'".$_REQUEST['segUsuariosEstatusHdn']."',".
                                "'wallpapers/SICA-Inicial.jpg',".
                                "'classic',".
                                $_REQUEST['segUsuariosTipoUsuarioCmb'].")";

            $rs = fn_ejecuta_query($sqlAddUsuarioStr);

            //IP
            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                if ($ipRest == 1) {
                    $ipArr = explode('|', substr($_REQUEST['segUsuariosIpHdn'], 0, -1));
                    $ipFechaArr = explode('|', substr($_REQUEST['segUsuariosFechaIpHdn'], 0, -1));
                    
                    $sqlAddIpRestStr = "INSERT INTO segUsuariosIpTbl (idUsuario, fecha, ip, idUsuarioAct, ipAct) VALUES";

                    for ($iInt=0; $iInt < sizeof($ipArr); $iInt++) { 
                        if ($ipArr[$iInt] != "") {
                            if ($iInt != 0) {
                                $sqlAddIpRestStr .= ",";
                            }

                            $sqlAddIpRestStr .= "(".$idUsuario.",".
                                                "'".$ipFechaArr[$iInt]."',".
                                                "'".$ipArr[$iInt]."',".
                                                $_SESSION['idUsuario'].",".
                                                "'".$_SERVER['REMOTE_ADDR']."')";
                        }
                    }
                    
                    $rs = fn_ejecuta_query($sqlAddIpRestStr);

                    $a['sql'] = $sqlAddIpRestStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $a['sql'];
            }
            
            //Horarios
            /*if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                if ($horarioRest == 1) {
                    $diasArr = explode("|", substr($_REQUEST['segUsuariosHorariosDiasHdn'], 0, -1));
                    $entradasArr = explode("|", substr($_REQUEST['segUsuariosHorariosEntradasHdn'], 0, -1));
                    $salidasArr = explode("|", substr($_REQUEST['segUsuariosHorariosSalidasHdn'], 0, -1));
                    $diaCompArr = explode("|", substr($_REQUEST['segUsuariosHorariosDiaCompletoHdn'], 0, -1));

                    $sqlAddHorarioRestStr = "INSERT INTO segUsuariosHorariosTbl ".
                                            "(idUsuario, dia, entrada, salida, diaCompleto) VALUES";

                    for ($iInt=0; $iInt < sizeof($diasArr); $iInt++) { 
                        if ($diasArr[$iInt] != "") {
                            if ($iInt != 0) {
                                $sqlAddHorarioRestStr .= ",";
                            }

                            $sqlAddHorarioRestStr .= "(".$idUsuario.",".
                                                    $diasArr[$iInt].",".
                                                    "'".$entradasArr[$iInt]."',".
                                                    "'".$salidasArr[$iInt]."',".
                                                    $diaCompArr[$iInt].")";
                        }
                    }

                    $rs = fn_ejecuta_query($sqlAddHorarioRestStr);

                    $a['sql'] = $sqlAddHorarioRestStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $a['sql'];
            }*/

            //Compañías
            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                if ($_REQUEST['segUsuariosCentroDistHdn'] != "") {

                    $centroDistArr = explode("|", substr($_REQUEST['segUsuariosCentroDistHdn'], 0, -1));

                    $sqlAddCentroDistStr = "INSERT INTO genAsignacionUsuariosCompaniasTbl (claveCompania, idUsuario, cambiarFecha) VALUES ";

                    for ($iInt=0; $iInt < sizeof($centroDistArr); $iInt++) { 
                        if ($centroDistArr[$iInt] != "") {
                            if ($iInt != 0) {
                                $sqlAddCentroDistStr .= ",";
                            }

                            $sqlAddCentroDistStr .= "(" . $centroDistArr[$iInt] . ", " . $idUsuario . ", 'N')";
                        }
                    }

                    $rs = fn_ejecuta_query($sqlAddCentroDistStr);

                    $a['sql'] = $sqlAddCentroDistStr;

                    if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $a['successMessage'] = "Usuario Ingresado Correctamente";
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddCentroDistStr;
                    }
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $a['sql'];
            }  

            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] =  "Usuario Ingresado Correctamente";
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = "Mantenimiento a Usuarios";
        echo json_encode($a);
    }

    function updUsuario(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['segUsuariosIdUsuarioHdn'] == ""){
            $e[] = array('id'=>'segUsuariosIdUsuarioHdn','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "El id del Usuario es Requerido";
            $a['success'] = false;
        }
        if($_REQUEST['segUsuariosUsuarioTxt'] == ""){
            $e[] = array('id'=>'segUsuariosUsuarioTxt','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "La Clave del Usuario es Requerido";
            $a['success'] = false;
        }
        if($_REQUEST['segUsuariosNombreTxt'] == ""){
            $e[] = array('id'=>'segUsuariosNombreTxt','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "El Nombre del Usuario es Requerido";
            $a['success'] = false;
        }
        if($_REQUEST['segUsuariosCorreoElectronicoTxt'] == ""){
            $e[] = array('id'=>'segUsuariosCorreoElectronicoTxt','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "El Correo Electronico del Usuario es Requerido";
            $a['success'] = false;
        }
        if($_REQUEST['segUsuariosEstatusHdn'] == ""){
            $e[] = array('id'=>'segUsuariosEstatusHdn','msg'=>"Este Campo es Requerido");
            $a['errorMessage'] = "El Estatus del Usuario es Requerido";
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            $ipRest = (empty($_REQUEST['segUsuariosIpHdn'])) ? 0 : 1;
            $horarioRest = empty($_REQUEST['segUsuariosHorariosDiasHdn']) ? 0 : 1;

            $sqlUpdUsuarioStr = "UPDATE segUsuariosTbl ".
                                "SET usuario = '" . $_REQUEST['segUsuariosUsuarioTxt'] . "'," .
                                "nombre      = '" . $_REQUEST['segUsuariosNombreTxt'] . "'," .
                                "estatus     = '" . $_REQUEST['segUsuariosEstatusHdn'] . "', " .
                                "tipoUsuario     = '" . $_REQUEST['segUsuariosTipoUsuarioCmb'] . "', " .
                                "restriccionPorIP = " . $ipRest . "," .
                                "restriccionPorHorario = " . $horarioRest . "," .
                                "correoElectronico     = '" . $_REQUEST['segUsuariosCorreoElectronicoTxt'] . "' " .
                                "WHERE idUsuario = " . $_REQUEST['segUsuariosIdUsuarioHdn'];

            $rs = fn_ejecuta_query($sqlUpdUsuarioStr);

            //IP
            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                if ($ipRest == 1) {
                    //Borra las restricciones
                    $sqlDeleteIpRestStr = "DELETE FROM segUsuariosIpTbl WHERE idUsuario = " . $_REQUEST['segUsuariosIdUsuarioHdn']; 

                    $rs = fn_ejecuta_query($sqlDeleteIpRestStr);

                    //Inserta las nuevas
                    $ipArr = explode('|', substr($_REQUEST['segUsuariosIpHdn'], 0, -1));
                    $ipFechaArr = explode('|', substr($_REQUEST['segUsuariosFechaIpHdn'], 0, -1));
                    
                    $sqlAddIpRestStr = "INSERT INTO segUsuariosIpTbl (idUsuario, fecha, ip, idUsuarioAct, ipAct) VALUES";

                    for ($iInt=0; $iInt < sizeof($ipArr); $iInt++) { 
                        if ($ipArr[$iInt] != "") {
                            if ($iInt != 0) {
                                $sqlAddIpRestStr .= ",";
                            }

                            $sqlAddIpRestStr .= "(".$_REQUEST['segUsuariosIdUsuarioHdn'].",".
                                                "'".$ipFechaArr[$iInt]."',".
                                                "'".$ipArr[$iInt]."',".
                                                $_SESSION['idUsuario'].",".
                                                "'".$_SERVER['REMOTE_ADDR']."')";
                        }
                    }
                    
                    $rs = fn_ejecuta_query($sqlAddIpRestStr);

                    $a['sql'] = $sqlAddIpRestStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $a['sql'];
            }
            
            //Horarios
            /*if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                if ($horarioRest == 1) {

                    //Borra las restricciones
                    $sqlDeleteIpRestStr = "DELETE FROM segUsuariosHorariosTbl WHERE idUsuario = " . $_REQUEST['segUsuariosIdUsuarioHdn']; 

                    $rs = fn_ejecuta_query($sqlDeleteIpRestStr);

                    //Inserta las nuevas
                    $diasArr = explode("|", substr($_REQUEST['segUsuariosHorariosDiasHdn'], 0, -1));
                    $entradasArr = explode("|", substr($_REQUEST['segUsuariosHorariosEntradasHdn'], 0, -1));
                    $salidasArr = explode("|", substr($_REQUEST['segUsuariosHorariosSalidasHdn'], 0, -1));
                    $diaCompArr = explode("|", substr($_REQUEST['segUsuariosHorariosDiaCompletoHdn'], 0, -1));

                    $sqlAddHorarioRestStr = "INSERT INTO segUsuariosHorariosTbl ".
                                            "(idUsuario, dia, entrada, salida, diaCompleto) VALUES";

                    for ($iInt=0; $iInt < sizeof($diasArr); $iInt++) { 
                        if ($diasArr[$iInt] != "") {
                            if ($iInt != 0) {
                                $sqlAddHorarioRestStr .= ",";
                            }

                            $sqlAddHorarioRestStr .= "(".$_REQUEST['segUsuariosIdUsuarioHdn'].",".
                                                    $diasArr[$iInt].",".
                                                    "'".$entradasArr[$iInt]."',".
                                                    "'".$salidasArr[$iInt]."',".
                                                    $diaCompArr[$iInt].")";
                        }
                    }

                    $rs = fn_ejecuta_query($sqlAddHorarioRestStr);

                    $a['sql'] = $sqlAddHorarioRestStr;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $a['sql'];
            }*/

            //Centros Distribucion
            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                //Borra los Centros de Distribución
                $sqlDeleteIpRestStr = "DELETE FROM genAsignacionUsuariosCompaniasTbl WHERE idUsuario = '" . $_REQUEST['segUsuariosIdUsuarioHdn'] . "'"; 

                $rs = fn_ejecuta_query($sqlDeleteIpRestStr);

                 //Inserta las nuevas
                if ($_REQUEST['segUsuariosCentroDistHdn'] != "") {
                    $centroDistArr = explode("|", substr($_REQUEST['segUsuariosCentroDistHdn'], 0, -1));

                    $sqlAddCentroDistStr = "INSERT INTO genAsignacionUsuariosCompaniasTbl (claveCompania, idUsuario, cambiarFecha) VALUES";

                    for ($iInt=0; $iInt < sizeof($centroDistArr); $iInt++) { 
                        if ($centroDistArr[$iInt] != "") {
                            if ($iInt != 0) {
                                $sqlAddCentroDistStr .= ",";
                            }

                            $sqlAddCentroDistStr .= "(" . $centroDistArr[$iInt] . ", " . $_REQUEST['segUsuariosIdUsuarioHdn'] . ", 'N')";
                        }
                    }

                    $rs = fn_ejecuta_query($sqlAddCentroDistStr);

                    $a['sql'] = $sqlAddCentroDistStr;

                    if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $a['successMessage'] = "Usuario Actualizado con Exito";
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddCentroDistStr;
                    }
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $a['sql'];
            }

            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] =  "Usuario Actualizado con Exito";
            }

        }
        $a['errors'] = $e;
        $a['successTitle'] = "Mantenimiento a Usuarios";
        echo json_encode($a);
    }

    function getCompaniasUsuario(){
		$idUsuario = "";
		if($_REQUEST['segUsuariosIdUsuarioHdn'] != "")
			$idUsuario = $_REQUEST['segUsuariosIdUsuarioHdn'];
		else
			$idUsuario = $_SESSION['idUsuario'];
			
        $sqlGetCentrosUsuarioStr = "SELECT uc.claveCompania, co.nombre ".
                                   "FROM genCatCompaniasTbl co, genAsignacionUsuariosCompaniasTbl uc ".
                                   "WHERE uc.claveCompania = co.claveCompania ".
                                   "AND   uc.idUsuario     = " . $idUsuario;

        $rs = fn_ejecuta_query($sqlGetCentrosUsuarioStr);
        $row = mysql_fetch_assoc($rs);
        $total = mysql_num_rows($rs);

        $i = 0;
        $response->success = true;
        $response->records = $total;
        do
        {
			if($row['claveCompania'] != '')
            {
                $row['nombreCombo'] = $row['claveCompania'] . " - " . $row['nombre'];
                $response->root[$i] = $row;
                $i++;
            }
        }while($row = mysql_fetch_assoc($rs));
            
        echo json_encode($response);    
    }

    function getCompaniasDisponibles(){

        $sqlGetCentrosDisponiblesStr = "SELECT co.* ".
                                       "FROM genCatCompaniasTbl co ";

        if ($_REQUEST['segUsuariosIdUsuarioHdn'] != "") {
            $sqlGetCentrosDisponiblesStr .= "WHERE co.claveCompania NOT IN ".
                                            "(SELECT su.claveCompania FROM genAsignacionUsuariosCompaniasTbl su ".
                                            " WHERE su.idUsuario = " . $_REQUEST['segUsuariosIdUsuarioHdn'] . ") ";
        }

        $rs = fn_ejecuta_query($sqlGetCentrosDisponiblesStr);
        $row = mysql_fetch_assoc($rs);
        $total = mysql_num_rows($rs);

        $i = 0;
        $response->success = true;
        $response->records = $total;
        do
        {
            if($row['claveCompania'] != '')
            {
                $row['nombreCombo'] = $row['claveCompania'] . " - " . $row['nombre'];
                $response->root[$i] = $row;
                $i++;
            }
        }while($row = mysql_fetch_assoc($rs));
            
        echo json_encode($response);    
    }

    function getIPUsuario(){
        $sqlGetCentrosUsuarioStr = "SELECT * ".
                                   "FROM segUsuariosIpTbl ".
                                   "WHERE idUsuario = ".$_REQUEST['segUsuariosIdUsuarioHdn'];

        $rs = fn_ejecuta_query($sqlGetCentrosUsuarioStr);
        $row = mysql_fetch_assoc($rs);
        $total = mysql_num_rows($rs);

        $i = 0;
        $response->success = true;
        $response->records = $total;
        do
        {
            if($row['idUsuario'] != '')
            {
                $response->root[$i] = $row;
                $i++;
            }
        }while($row = mysql_fetch_assoc($rs));
            
        echo json_encode($response);
    }

    function getHorariosUsuario(){
        $sqlGetHorariosUsuarioStr = "SELECT * ".
                                    "FROM segUsuariosHorariosTbl ".
                                    "WHERE idUsuario = ".$_REQUEST['segUsuariosIdUsuarioHdn'];

        $rs = fn_ejecuta_query($sqlGetHorariosUsuarioStr);
        $row = mysql_fetch_assoc($rs);
        $total = mysql_num_rows($rs);

        $i = 0;
        $response->success = true;
        $response->records = $total;
        do
        {
            $response->root[$i] = $row;
            $i++;
        }while($row = mysql_fetch_assoc($rs));
            
        echo json_encode($response);
    }

    function getUsuarioConfig(){
        $sqlGetUsuarioConfigStr = "SELECT module, name, iconCls " .
                                  "FROM segUsuariosDesktopTbl " . 
                                  "WHERE idUsuario = " . $_SESSION['idUsuario'];

        $rs = fn_ejecuta_query($sqlGetUsuarioConfigStr);
        $row = mysql_fetch_assoc($rs);
        $total = mysql_num_rows($rs);

        $i = 0;
        do{
            $response[$i] = $row;
            $i++;
        }while($row = mysql_fetch_assoc($rs));
            
        echo json_encode($response);
    }

    function generaPassword(){
        $pwd = "";
        $characteres = "abcdefghijklmnopqrstuvwxyz";
        $min = 0;
        $may = 0;
        $num = 0;

        $i = 1;
        while ($i <= 8) {
            $choice = rand(1,3);
            if ($i == 6) {
                if ($min == 0) {
                    $pwd .= $characteres[rand(0,strlen($characteres))];
                    $min++;
                } elseif ($may == 0) {
                    $pwd .= strtoupper($characteres[rand(0,strlen($characteres))]);
                    $may++;
                } elseif ($num == 0) {
                    $pwd .= rand(0,9);
                    $num++;
                }
            }
            if ($choice == 1) {
                $pwd .= $characteres[rand(0,strlen($characteres))];
                $min++;
            } elseif ($choice == 2) {
                $pwd .= strtoupper($characteres[rand(0,strlen($characteres))]);
                $may++;
            } else {
                $pwd .= rand(0,9);
                $num++;
            }
            $i++;
        }
        return $pwd;
    }
?>