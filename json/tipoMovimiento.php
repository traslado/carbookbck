<?php
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $sql="SELECT valor, nombre as tipoMovimiento FROM cageneralestbl ".
			"WHERE valor in ('PR','CO','UE','EO','EH','FC','IG','SP','UP','OM') ".
			"AND columna='claveMovimiento'";

	$rs=fn_ejecuta_query($sql);

    echo json_encode($rs);

?>