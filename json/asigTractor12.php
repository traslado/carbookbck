<?php
    session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require("C:/Servidores/apache/htdocs/carbookbck/procesos/i343_Asignaciones.php");

    $_REQUEST = trasformUppercase($_REQUEST);

   /* switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
            break;
    }*/

    switch($_REQUEST['tr12ActionHdn']){
        case 'getUnidadesTemporal':
            getUnidadesTemporal();
            break;
        case 'addUndtemporal':
            addUndtemporal();
            break;
        case 'dltTemporal':
            dltTemporal();
            break;
        case 'getImpresionTalon':
            getImpresionTalon();
            break;
        case 'addViajeTractor':
            addViajeTractor();
            break;               
        case 'addViajeOtroTrans':
            addViajeOtroTrans();
            break;                  
        default:
            echo '';
            break;
    }

    function getUnidadesTemporal(){
        //$RQusuario = 13;

        $getUnidadesTemporal = "SELECT aut.vin, ud.distribuidor dist,concat(ud.distribuidor,' - ',cd.descripcionCentro) as distribuidor, (SELECT 1 FROM alunidadesdetenidastbl dt WHERE aut.vin = dt.vin AND dt.claveMovimiento = 'UD') as unidadDetenida,pc.plaza as plazaOrigen, pd.plaza as plazaDestino, (SELECT MAX(viaje) FROM trviajestractorestbl WHERE idTractor = 256 ) AS numViaje, count(aut.vin) as totalUnidades, ".
               "pc.idPlaza as diPlazaOrigen, pd.idPlaza as idPlazaDestino, cd.distribuidorCentro, ud.centroDistribucion, ".
               "(SELECT ct.tipoTarifa FROM caTarifasTbl ct WHERE ct.idTarifa = ud.idTarifa) AS tipoTarifa ".
               "FROM alunidadestmp aut, alultimodetalletbl ud, caplazastbl pD, caplazastbl pC, cadistribuidorescentrostbl di, cadistribuidorescentrostbl cd ".
               "WHERE aut.vin = ud.vin ".
               "AND aut.vin = '".$_REQUEST['asigT12VinHdn']."' ".
               "AND ud.centroDistribucion = di.distribuidorCentro ".
               "AND ud.distribuidor = cd.distribuidorCentro ".
               "AND di.idPlaza = pc.idPlaza ".
               "AND cd.idPlaza = pd.idPlaza ".
               "AND aut.idUsuario = '".$_SESSION['idUsuario']."' ".
               "AND aut.modulo = 'Asig. tractor 12' ".
               "GROUP BY aut.vin";
        //echo "$getUnidadesTemporal<br>";
        $rsGetTmp = fn_ejecuta_query($getUnidadesTemporal);
        echo json_encode($rsGetTmp);
    }

    function addUndtemporal(){

        //$RQusuario = 13;
        $RQip = '10.1.2.42';

        $addUnidadTmp = "INSERT INTO alUnidadesTmp (vin, avanzada, modulo, idUsuario, ip, fecha) ".
        "VALUES( ".
        "'".$_REQUEST['asigT12VinTxt']."', ".
        "'".substr($_REQUEST['asigT12VinTxt'],9,8)."', ".
        "'Asig. tractor 12' ,".
        //" (SELECT 1 FROM alunidadesdetenidastbl WHERE vin = '".$_REQUEST['asigT12VinTxt']."' AND numeroMovimiento = 1) , ".
        "'".$_SESSION['idUsuario']."', ".
        "'".$RQip."', ".
        "NOW() ) ";

        $rsAddTmp = fn_ejecuta_Add($addUnidadTmp);
        echo json_encode($rsAddTmp); 
    }

    function dltTemporal(){

        //$RQusuario = 13;
        $whereStr = "";
        if (isset($_REQUEST['tr12VinHdn']) && $_REQUEST['tr12VinHdn'] != '') {
            $whereStr = "AND vin = '".$_REQUEST['tr12VinHdn']."'";
        }

        $dltUnidades = "DELETE FROM alUnidadesTmp ". 
                       "WHERE modulo = 'Asig. tractor 12' ".
                       "AND idUsuario = '".$_SESSION['idUsuario']."' ".
                       $whereStr;        
    
        $rsDltTmp = fn_ejecuta_Add($dltUnidades);
        echo json_encode($rsDltTmp);
    }

    function getImpresionTalon(){

        $getImpresion = "SELECT tl.idviajeTractor as numeroViaje, tl.folio as folio ".
                        "FROM trviajestractorestbl vt, trTalonesViajesTbl tl ".
                        "WHERE tl.idViajeTractor = vt.idViajeTractor ".
                        "AND idTractor = 256 ".
                        "AND viaje = (SELECT max(viaje) FROM trviajestractorestbl WHERE idTractor = 256) ";

        $rsGetImpresion = fn_ejecuta_query($getImpresion);
        echo json_encode($rsGetImpresion);
    }

    function addViajeTractor(){
        $a = array('success' => true, 'msjResponse' => 'Asignaci&oacuten efectuada.', 'numViaje' => '');
        $detalle = json_decode($_POST['detalle'],true);
        $cenDistribucion = $detalle[0]['Cdist'];

        //actualiza Folio de la compania Crymex y centroDistribucion y tipo de Documento
        $sql = "SELECT folio FROM trFoliosTbl WHERE centroDistribucion = '".$cenDistribucion."' AND compania = 'CR' AND tipoDocumento = 'TR'";
        //echo "$sql<br>";
        $rsActFolio = fn_ejecuta_query($sql);        

        $folioUpd = floatval($rsActFolio['root'][0]['folio']) + 1;

        $sql = "UPDATE trFoliosTbl ".
              "SET folio = '".$folioUpd."' ".
              "WHERE centroDistribucion = '".$cenDistribucion."' ".
              "AND compania = 'CR' ".
              "AND tipoDocumento = 'TR' ";
        //echo "$sql<br>";
        fn_ejecuta_Add($sql);
        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
            $a['success'] = false;
            $a['sql'] = $sql;
            $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten del Folio.";
            echo json_encode($a);
            die();
        }

        $sql = "SELECT aut.vin, ud.distribuidor, (SELECT 1 FROM alunidadesdetenidastbl dt WHERE aut.vin = dt.vin limit 1) as unidadDetenida,pc.plaza as plazaOrigen, pd.plaza as plazaDestino, (SELECT MAX(viaje) FROM trviajestractorestbl WHERE idTractor = 256 ) AS numViaje, count(aut.vin) as totalUnidades, ".
               "pc.idPlaza as diPlazaOrigen, pd.idPlaza as idPlazaDestino ".
               "FROM alunidadestmp aut, alultimodetalletbl ud, caplazastbl pD, caplazastbl pC, cadistribuidorescentrostbl di, cadistribuidorescentrostbl cd ".
               "WHERE aut.vin = ud.vin ".
               "AND ud.centroDistribucion = di.distribuidorCentro ".
               "AND ud.distribuidor = cd.distribuidorCentro ".
               "AND di.idPlaza = pc.idPlaza ".
               "AND cd.idPlaza = pd.idPlaza ".
               "AND aut.idUsuario = '".$_SESSION['idUsuario']."' ".
               "AND aut.modulo = 'Asig. tractor 12' ".
               "GROUP BY aut.vin";
        $rsGetTmp1 = fn_ejecuta_query($sql);
        //var_dump($rsGetTmp1);
        $numFinal = floatval($rsGetTmp1['root'][0]['numViaje']);
        for ($i=0; $i < $rsGetTmp1['records']; $i++) { 
            $row = $rsGetTmp1['root'][$i];
            $rowAux = obtenRow($detalle,$row['vin']);

            $cenDistribucion = $rowAux['Cdist'];
            $VTplazaOrigen = $row['diPlazaOrigen'];
            $VTplazaDestino = $row['idPlazaDestino'];
            $distribuidorTalon = $row['distribuidor'];
            if ($i == 0) {
                $numFinal++;
                $plazaOrigenStr = "SELECT idPlaza FROM cadistribuidorescentrostbl WHERE distribuidorCentro = '".$_SESSION['usuCompania']."'";
                $plazaOrigenRst = fn_ejecuta_Add($plazaOrigenStr);
                $plazaDestinoStr = "SELECT idPlaza FROM cadistribuidorescentrostbl WHERE distribuidorCentro = '".$_REQUEST['asigT12RemitenteHdn']."'";
                $plazaDestinoRst = fn_ejecuta_Add($plazaDestinoStr);
           
                /*$sql = "INSERT INTO trViajesTractoresTbl (idTractor,claveChofer,idPlazaOrigen,idPlazaDestino,".
                       "centroDistribucion,viaje,fechaEvento,kilometrosTabulados,kilometrosComprobados,".
                       "kilometrosSinUnidad,numeroUnidades,numeroRepartos,idViajePadre,claveMovimiento,usuario,ip) ".
                       "VALUES( ".
                       "'256', ".
                       "'0', ".
                       "".$plazaOrigenRst['root'][0]['idPlaza'].",".
                       "".$plazaDestinoRst['root'][0]['idPlaza'].",".
                       "'".$cenDistribucion."', ".
                       "'".$numFinal."', ".
                       "NOW(), ".
                       "(SELECT kilometros FROM cakilometrosplazatbl WHERE idPlazaOrigen = ".$VTplazaOrigen." AND idPlazaDestino = ".$VTplazaDestino."), ".
                       "'0', ".
                       "'0', ".
                       "(SELECT count(VIN) as numeroUnidades FROM alunidadestmp WHERE modulo = 'Asig. tractor 12' AND idUsuario = ".$_SESSION['idUsuario']."), ".
                       "'1', ".
                       "null, ".
                       "'VP', ".
                       "'".$_SESSION['idUsuario']."', ".
                       "'".$_SERVER['REMOTE_ADDR']."') ";
                //echo "$sql<br>";
                fn_ejecuta_Add($sql);*/

                 $sqlImportes="SELECT * FROM trunidadesembarcadastbl where vin='".$row['vin']."'";
                 $rsImportes=fn_ejecuta_query($sqlImportes);

                 $updViaje="UPDATE trViajesTractoresTbl SET claveMovimiento='VP' WHERE idViajeTractor=".$rsImportes['root'][0]['idViajeTractor'];
                 fn_ejecuta_query($updViaje);

                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['sql'] = $sql;
                    $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten del Viaje.";
                } else {
                    $sql = "SELECT idviajeTractor, (SELECT direccionEntrega FROM cadistribuidorescentrostbl WHERE distribuidorCentro = '".$distribuidorTalon."') AS direccionEntrega FROM trViajesTractoresTbl ".
                          "WHERE viaje ='".$numFinal."' ".
                          "AND idTractor = 256 ".
                          "AND centroDistribucion = '".$cenDistribucion."'";
                    //echo "$sql<br>";
                    $rsIdViaje = fn_ejecuta_query($sql);
            
                    $idViaje = $rsIdViaje['root'][0]['idviajeTractor'];
                    $direntrega = $rsIdViaje['root'][0]['direccionEntrega'];

                    if (!empty($a['numViaje'])) {
                        $a['numViaje'] .= ", ";
                    }
                    $a['numViaje'] .= $idViaje;
            
                    $sql = "SELECT 1 AS bandera FROM alUnidadesDetenidasTbl de WHERE de.vin = '".$row['vin']."' AND de.claveMovimiento = 'UD'";
                    //echo "$sql<br>";
                    $rsGetTalon = fn_ejecuta_query($sql);
            
                    $comentario = '  ENTR. REPRES.';
                    $claveUD ='ER';
                    if($rsGetTalon['root'][0]['bandera'] == 1){
                        $comentario = '  UNDS DET S.E.';
                        $claveUD ='DS';
                    }

                    $sqlImportes="SELECT * FROM trunidadesembarcadastbl where vin='".$row['vin']."'";
                    $rsImportes=fn_ejecuta_query($sqlImportes);

                    $numUnidades="SELECT count(VIN) as numeroUnidades FROM alUnidadesTmp WHERE modulo = 'Asig. tractor 12' AND idUsuario = '".$_SESSION['idUsuario']."'";
                    $rsNumUnidades=fn_ejecuta_query($numUnidades);

                    $totUnidades=$rsNumUnidades['root'][0]['numeroUnidades'];
                     $folioUpd = floatval($rsActFolio['root'][0]['folio']) + 1;

                     $sqlActualizarTalon = "UPDATE trTalonesViajesTbl SET claveMovimiento = 'TE',  tipoDocumento = 'TE',folio=".$folioUpd.", fechaEvento = now(),tipoTalon = 'TN' ".", fechaEntrega = '".$rsImportes['root'][0]['fechaEmbarque']."' "." WHERE idTalon = ".$rsImportes['root'][0]['idtalon'];

                    $rsUpdTalon = fn_ejecuta_Upd($sqlActualizarTalon);


                  $folioUpd = floatval($rsActFolio['root'][0]['folio']) + 1;

                  $sql = "UPDATE trFoliosTbl ".
                        "SET folio = '".$folioUpd."' ".
                        "WHERE centroDistribucion = '".$cenDistribucion."' ".
                        "AND compania = 'CR' ".
                        "AND tipoDocumento = 'TR' ";
                  //echo "$sql<br>";
                  fn_ejecuta_Add($sql);

                    /*$sql = "INSERT INTO trTalonesViajesTbl (distribuidor,folio,idViajeTractor,companiaRemitente,".
                          "idPlazaOrigen,idPlazaDestino,direccionEntrega,centroDistribucion,tipoTalon,".
                          "fechaEvento,observaciones,numeroUnidades,importe,seguro,tarifaCobrar,kilometrosCobrar,".
                          "impuesto,retencion,claveMovimiento,tipoDocumento,tipoDctoSica,folioSica,subTotal,IVA,retenciones,total,totalIva,totalRetenciones,claveCliente,nombre,referencia,observacionesCP,claveCsica) VALUES( ".
                          "'".$distribuidorTalon."', ".
                          "".$folioUpd.", ".
                          "'".$idViaje."', ".
                          "'".$_REQUEST['asigT12RemitenteHdn']."', ".
                          "(SELECT idPlaza FROM cadistribuidorescentrostbl WHERE distribuidorCentro = '".$_SESSION['usuCompania']."'),".
                          "(SELECT idPlaza FROM cadistribuidorescentrostbl WHERE distribuidorCentro = '".$_REQUEST['asigT12RemitenteHdn']."'), ".
                          "'".$direntrega."', ".
                          "'".$cenDistribucion."', ".
                          "'TN', ".
                          "NOW(), ".
                          "'*HORA: ".date ("H:i:s").$comentario."', ".
                          "(SELECT count(VIN) as numeroUnidades FROM alUnidadesTmp WHERE modulo = 'Asig. tractor 12' AND idUsuario = '".$_SESSION['idUsuario']."'), ".
                          "'0.00', ".
                          "'0.00', ".
                          "'0.00', ".
                          "'0.00', ".
                          "'0.00', ".
                          "'0.00', ".
                          "'TE', ".
                          "'TE','".$rsImportes['root'][0]['tipoDocumento']."','".
                          $rsImportes['root'][0]['folio']."','".
                          $rsImportes['root'][0]['subtotal']."','".
                          $rsImportes['root'][0]['IVA']."','".
                          $rsImportes['root'][0]['retenciones']."','".
                          $rsImportes['root'][0]['total']."','".
                          $rsImportes['root'][0]['totalIVA']."','".
                          $rsImportes['root'][0]['totalRetenciones']."','".
                          $rsImportes['root'][0]['claveCliente']."','".
                          $rsImportes['root'][0]['nombre']."','".
                          $rsImportes['root'][0]['referencia']."','".
                          $rsImportes['root'][0]['observaciones']."','".
                          $rsImportes['root'][0]['claveCompania']."') ";
                    //echo "$sql<br>";
                    fn_ejecuta_Add($sql);*/




                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                    {
                        $a['success'] = false;
                        $a['sql'] = $sql;
                        $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten del Tal&oacuten.";
                    }
                }
            }

            if($a['success'])
            {

              $sqlImportes="SELECT * FROM trunidadesembarcadastbl where vin='".$row['vin']."'";
                    $rsImportes=fn_ejecuta_query($sqlImportes);


                $sql =  "INSERT INTO trUnidadesDetallesTalonesTbl(idTalon,vin,estatus,imagen01,imagen02,imagen03,importeUnitario) ".
                        "SELECT (SELECT tl.idTalon FROM trTalonesViajesTbl tl WHERE tl.idViajeTractor = '".$rsImportes['root'][0]['idViajeTractor']."') AS idTalon, vin,'S',null,null,null ,null FROM alUnidadesTmp ".
                        "WHERE modulo = 'Asig. tractor 12' ".
                        "AND idUsuario = '".$_SESSION['idUsuario']."' ".
                        "AND vin = '".$row['vin']."'";
                //echo "$sql<br>";
                fn_ejecuta_Add($sql);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['sql'] = $sql;
                    $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten del Detalle del Tal&oacuten.";
                }
                 $sqlImportes="SELECT * FROM trunidadesembarcadastbl where vin='".$row['vin']."'";
                    $rsImportes3=fn_ejecuta_query($sqlImportes);


               /* $upd="UPDATE trUnidadesDetallesTalonesTbl SET importeUnitario=".$rsImportes3['root'][0]['importeUnitario']." WHERE vin='".$row['vin']."' AND idTalon='".$rsImportes3['root'][0]['idtalon']."'";
                fn_ejecuta_query($upd);*/

                $sqlImportes1="DELETE  FROM trunidadesembarcadastbl where vin='".$row['vin']."'";
                    $rsImportes1=fn_ejecuta_query($sqlImportes1);

                    

            }
             

            if($a['success'])
            {
                $fechaEvento_01 = date("Y-m-d H:i:s", time() + 1);
                $fechaEvento_02 = date("Y-m-d H:i:s", time() + 2);
                $fechaEvento_03 = date("Y-m-d H:i:s", time() + 3);
        
                $sql = "INSERT INTO alHistoricoUnidadesTbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                      "SELECT (SELECT au1.centroDistribucion FROM alUltimoDetalleTbl au1 WHERE au1.vin = tmp.vin) as centroDistribucion, tmp.vin, NOW() + INTERVAL 1 SECOND,'ER' AS claveMovimiento, (SELECT au2.distribuidor FROM alultimodetalletbl au2 WHERE au2.vin = tmp.vin) as distribuidor, ".
                      "(SELECT au3.idTarifa FROM alUltimoDetalleTbl au3 WHERE au3.vin = tmp.vin) as idTarifa,(SELECT au4.localizacionUnidad FROM alUltimoDetalleTbl au4 WHERE au4.vin = tmp.vin) as localizacionUnidad, '0' as claveChofer, null as descripcion, '".$_SESSION['idUsuario']."' as usuario,'".$_SERVER['REMOTE_ADDR']."' as ip ".
                      "FROM alUnidadesTmp tmp ".
                      "WHERE modulo = 'Asig. tractor 12' ".
                      "AND idUsuario = '".$_SESSION['idUsuario']."' ".
                      "AND vin = '".$row['vin']."'";
                //echo "$sql<br>";
                fn_ejecuta_Add($sql);

                $sqlDltLocalizacionPatiosStr = "UPDATE alLocalizacionPatiosTbl ".
                                           "SET vin = NULL, estatus='DI' WHERE vin= '".$row['vin']."' ";

                //i343a($row['vin'],'AM');


                /*if($patio != ''){
                    $sqlDltLocalizacionPatiosStr .= "AND patio = '".$patio."'";
                }*/

                $rs = fn_ejecuta_Upd($sqlDltLocalizacionPatiosStr);

                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['sql'] = $sql;
                    $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten del Hist&oacuterico AM.";
                }
            }     

            if($a['success'])
            {
                $sql = "INSERT INTO alHistoricoUnidadesTbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                        "SELECT (SELECT au1.centroDistribucion FROM alUltimoDetalleTbl au1 WHERE au1.vin = tmp.vin) as centroDistribucion, tmp.vin, NOW() + INTERVAL 120 SECOND,'OM' AS claveMovimiento, '".$distribuidorTalon."' as distribuidor, ".
                        "(SELECT au3.idTarifa FROM alUltimoDetalleTbl au3 WHERE au3.vin = tmp.vin) as idTarifa,'".$distribuidorTalon."', '0' as claveChofer, null as descripcion, '".$_SESSION['idUsuario']."' as usuario,'".$_SERVER['REMOTE_ADDR']."' as ip ".
                        "FROM alUnidadesTmp tmp ".
                        "WHERE modulo = 'Asig. tractor 12' ".
                        "AND idUsuario = '".$_SESSION['idUsuario']."' ".
                        "AND vin = '".$row['vin']."'";
                //echo "$sql<br>";
                // fn_ejecuta_Add($sql);

                $sqlDltLocalizacionPatiosStr = "UPDATE alLocalizacionPatiosTbl ".
                                           "SET vin = NULL, estatus='DI' WHERE vin= '".$row['vin']."' ";

                //i830_AM($row['vin'],'AM');
                //i343a($row['vin'],'AM');

                /*if($patio != ''){
                    $sqlDltLocalizacionPatiosStr .= "AND patio = '".$patio."'";
                }*/

                $rs = fn_ejecuta_Upd($sqlDltLocalizacionPatiosStr);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['sql'] = $sql;
                    $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten del Hist&oacuterico OM.";
                }
            }

            if($a['success'])
            {
                $sql= "INSERT INTO alHistoricoUnidadesTbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                        "SELECT dt.centroDistribucion,dt.vin,NOW() + INTERVAL 3 SECOND,'DS' as claveMovimiento ,dt.distribuidor,dt.idTarifa,dt.localizacionUnidad,dt.claveChofer, dt.observaciones,'".$_SESSION['idUsuario']."','".$_SERVER['REMOTE_ADDR']."' ".
                        "FROM alUnidadesTmp tmp, alUltimoDetalleTbl dt ".
                        "WHERE tmp.vin = dt.vin ".
                        "AND tmp.vin IN (SELECT dt.vin FROM alUnidadesDetenidasTbl dt WHERE dt.vin = tmp.vin AND dt.claveMovimiento = 'UD') ".
                        "AND tmp.modulo = 'Asig. tractor 12' ".
                        "AND tmp.idUsuario = '".$_SESSION['idUsuario']."' ".
                        "AND tmp.vin = '".$row['vin']."'";
                //echo "$sql<br>";
                //fn_ejecuta_Add($sql);

                $sqlDltLocalizacionPatiosStr = "UPDATE alLocalizacionPatiosTbl ".
                                           "SET vin = NULL, estatus='DI' WHERE vin= '".$row['vin']."' ";

                //i830_AM($row['vin'],'AM');
                  //  i343a($row['vin'],'AM');

                /*if($patio != ''){
                    $sqlDltLocalizacionPatiosStr .= "AND patio = '".$patio."'";
                }*/

                $rs = fn_ejecuta_Upd($sqlDltLocalizacionPatiosStr);
                
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
                {
                    $a['success'] = false;
                    $a['sql'] = $sql;
                    $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten del Hist&oacuterico DS.";
                }                               
            }
            if ($a['success'] == false) {
                $a['vin'] = $row['vin'];
                break;
            }

                        $selStr = "SELECT ud.vin, ud.idtarifa, ud.distribuidor, ud.localizacionUnidad, tl.centroDistribucion, vi.claveChofer, vi.idViajeTractor, tl.idTalon, tl.clavemovimiento, tl.numerounidades, dist.tipodistribuidor, ".
                        "CASE WHEN sim.marca IN ('AR','CD','DG','FI','JP', 'RA') THEN 'FCA' ".
                        "ELSE '' ".
                        "END  AS marca ".
                        "FROM trtalonesviajestbl tl, catractorestbl tt, trviajestractorestbl vi, trunidadesdetallestalonestbl ts, alultimodetalletbl ud, cadistribuidorescentrostbl dist, alunidadestbl un, casimbolosunidadestbl sim ".
                        "WHERE tl.centroDistribucion IN ('CDSLP','CMDAT','CDTOL','CDSAL','CDAGS','CDVER','CDSFE','CDLZC','CDANG','CDLCL') ".
                        "AND tl.distribuidor NOT IN ('CDTOL','CDSAL','CDAGS','CDVER','CDSFE','CDLZC','CDSLP','CMDAT','CDANG') ".
                        "AND tl.folio = '".$folioUpd."' ".
                        "AND tt.tractor = '12' ".
                        "AND vi.idTractor = tt.idTractor ".
                        "AND vi.idViajeTractor = tl.idViajeTractor ".
                        "AND ts.idTalon = tl.idTalon ".
                        "AND ud.centroDistribucion = tl.centroDistribucion ".
                        "AND ud.vin = ts.vin ".
                        "AND tl.clavemovimiento = 'TE' ".
                        "AND ud.idtarifa != 5 ".
                        "AND dist.distribuidorcentro = ud.distribuidor ".
                        "AND un.vin = ud.vin ".
                        "AND sim.simboloUnidad = un.simboloUnidad ".
                        "AND ud.vin not in (select vin from alTransportacionEstandarTrackingObtTbl where vin ='".$row['vin']."') ".
                        "AND ud.vin not in (select vin from alTransExportacionTrackingObtTbl where vin ='".$row['vin']."') ".
                        "AND ud.vin =  '".$row['vin']."' ";
                $alTransExpRst = fn_ejecuta_query($selStr);
                if ($alTransExpRst['records'] > 0) {
                    if ($alTransExpRst['root'][0]['marca'] == 'FCA') {
                        $insStr = "INSERT INTO ";
                        if ($alTransExpRst['root'][0]['tipodistribuidor'] == 'DI') {
                            $insStr .= "alTransportacionEstandarTrackingObtTbl";
                        } else {
                            $insStr .= "alTransExportacionTrackingObtTbl";
                        }
                        $insStr .= " (vin, fechaom, estatusHistorico) VALUES ('".$row['vin']."',NOW(),'OM')";
                        fn_ejecuta_query($insStr);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['errorMessage'] = $_SESSION['error_sql'];
                            $a['sql']         = $insStr;
                            break;
                        }
                    }
                }
        }
        if($a['success'])
        {
            $sql = "UPDATE alUltimoDetalleTbl ".
                 "SET claveMovimiento = '".$claveUD."', ".
                 "fechaEvento = NOW() + INTERVAL 3 SECOND ".
                 "WHERE vin IN(SELECT VIN FROM alUnidadesTmp WHERE modulo = 'Asig. tractor 12' AND idUsuario = '".$_SESSION['idUsuario']."') ";
            //echo "$sql<br>";
            fn_ejecuta_Add($sql);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
            {
                $a['success'] = false;
                $a['sql'] = $sql;
                $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten del Detalle.";
            }
        }

        $desbloqueoTr="DELETE FROM trviajestractorestmp where idTractor='256' and centroDistribucion='".$_SESSION['usuCompania']."'";
        fn_ejecuta_query($desbloqueoTr);

        $a['folio'] = $folioUpd;
            
        echo json_encode($a);
    }

     function addViajeOtroTrans(){
        $a = array('success' => true, 'msjResponse' => 'Asignaci&oacuten efectuada.', 'numViaje' => '');
        $detalle = json_decode($_POST['detalle'],true);
        $cenDistribucion = $detalle[0]['Cdist'];

        //actualiza Folio de la compania Crymex y centroDistribucion y tipo de Documento
        //$sql = "SELECT folio FROM trFoliosTbl WHERE centroDistribucion = '".$cenDistribucion."' AND compania = 'CR' AND tipoDocumento = 'TR'";
        //echo "$sql<br>";
        //$rsActFolio = fn_ejecuta_query($sql);        

        $folioUpd = floatval($rsActFolio['root'][0]['folio']) + 1;

        $sql = "UPDATE trFoliosTbl ".
              "SET folio = '".$folioUpd."' ".
              "WHERE centroDistribucion = '".$cenDistribucion."' ".
              "AND compania = 'CR' ".
              "AND tipoDocumento = 'TR' ";
        //echo "$sql<br>";
        //fn_ejecuta_Add($sql);
        /*if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
            $a['success'] = false;
            $a['sql'] = $sql;
            $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten del Folio.";
            echo json_encode($a);
            die();
        }*/


              /*          $selStr = "SELECT ud.vin, ud.idtarifa, ud.distribuidor, ud.localizacionUnidad, tl.centroDistribucion, vi.claveChofer, vi.idViajeTractor, tl.idTalon, tl.clavemovimiento, tl.numerounidades, dist.tipodistribuidor, ".
                        "CASE WHEN sim.marca IN ('AR','CD','DG','FI','JP', 'RA') THEN 'FCA' ".
                        "ELSE '' ".
                        "END  AS marca ".
                        "FROM trtalonesviajestbl tl, catractorestbl tt, trviajestractorestbl vi, trunidadesdetallestalonestbl ts, alultimodetalletbl ud, cadistribuidorescentrostbl dist, alunidadestbl un, casimbolosunidadestbl sim ".
                        "WHERE tl.centroDistribucion IN ('CDSLP','CMDAT','CDTOL','CDSAL','CDAGS','CDVER','CDSFE','CDLZC','CDANG','CDLCL') ".
                        "AND tl.distribuidor NOT IN ('CDTOL','CDSAL','CDAGS','CDVER','CDSFE','CDLZC','CDSLP','CMDAT','CDANG') ".
                        "AND tl.folio = '".$folioUpd."' ".
                        "AND tt.tractor = '12' ".
                        "AND vi.idTractor = tt.idTractor ".
                        "AND vi.idViajeTractor = tl.idViajeTractor ".
                        "AND ts.idTalon = tl.idTalon ".
                        "AND ud.centroDistribucion = tl.centroDistribucion ".
                        "AND ud.vin = ts.vin ".
                        "AND tl.clavemovimiento = 'TE' ".
                        "AND ud.idtarifa != 5 ".
                        "AND dist.distribuidorcentro = ud.distribuidor ".
                        "AND un.vin = ud.vin ".
                        "AND sim.simboloUnidad = un.simboloUnidad ".
                        "AND ud.vin not in (select vin from alTransportacionEstandarTrackingObtTbl where vin ='".$row['vin']."') ".
                        "AND ud.vin not in (select vin from alTransExportacionTrackingObtTbl where vin ='".$row['vin']."') ".
                        "AND ud.vin =  '".$row['vin']."' ";
                $alTransExpRst = fn_ejecuta_query($selStr);
                if ($alTransExpRst['records'] > 0) {
                    if ($alTransExpRst['root'][0]['marca'] == 'FCA') {
                        $insStr = "INSERT INTO ";
                        if ($alTransExpRst['root'][0]['tipodistribuidor'] == 'DI') {
                            $insStr .= "alTransportacionEstandarTrackingObtTbl";
                        } else {
                            $insStr .= "alTransExportacionTrackingObtTbl";
                        }
                        $insStr .= " (vin, fechaom, estatusHistorico) VALUES ('".$row['vin']."',NOW(),'OM')";
                        fn_ejecuta_query($insStr);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['errorMessage'] = $_SESSION['error_sql'];
                            $a['sql']         = $insStr;
                            break;
                        }
                    }
                }
        }*/

         $fechaEvento_01 = date("Y-m-d H:i:s", time() + 1);
                $fechaEvento_02 = date("Y-m-d H:i:s", time() + 2);
                $fechaEvento_03 = date("Y-m-d H:i:s", time() + 3);

                $sql = "SELECT aut.vin, ud.distribuidor, (SELECT 1 FROM alunidadesdetenidastbl dt WHERE aut.vin = dt.vin limit 1) as unidadDetenida,pc.plaza as plazaOrigen, pd.plaza as plazaDestino, (SELECT MAX(viaje) FROM trviajestractorestbl WHERE idTractor = 256 ) AS numViaje, count(aut.vin) as totalUnidades, ".
               "pc.idPlaza as diPlazaOrigen, pd.idPlaza as idPlazaDestino ".
               "FROM alunidadestmp aut, alultimodetalletbl ud, caplazastbl pD, caplazastbl pC, cadistribuidorescentrostbl di, cadistribuidorescentrostbl cd ".
               "WHERE aut.vin = ud.vin ".
               "AND ud.centroDistribucion = di.distribuidorCentro ".
               "AND ud.distribuidor = cd.distribuidorCentro ".
               "AND di.idPlaza = pc.idPlaza ".
               "AND cd.idPlaza = pd.idPlaza ".
               "AND aut.idUsuario = '".$_SESSION['idUsuario']."' ".
               "AND aut.modulo = 'Asig. tractor 12' ".
               "GROUP BY aut.vin";
       $rsGetTmp1 = fn_ejecuta_query($sql);
        //var_dump($rsGetTmp1);
        //$numFinal = floatval($rsGetTmp1['root'][0]['numViaje']);
        for ($i=0; $i < $rsGetTmp1['records']; $i++) { 
        
                $sql = "INSERT INTO alHistoricoUnidadesTbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                      "SELECT (SELECT au1.centroDistribucion FROM alUltimoDetalleTbl au1 WHERE au1.vin = tmp.vin) as centroDistribucion, tmp.vin, NOW() + INTERVAL 1 SECOND,'".$_REQUEST['salida']."' AS claveMovimiento, (SELECT au2.distribuidor FROM alultimodetalletbl au2 WHERE au2.vin = tmp.vin) as distribuidor, ".
                      "(SELECT au3.idTarifa FROM alUltimoDetalleTbl au3 WHERE au3.vin = tmp.vin) as idTarifa,(SELECT au4.localizacionUnidad FROM alUltimoDetalleTbl au4 WHERE au4.vin = tmp.vin) as localizacionUnidad, '0' as claveChofer, null as descripcion, '".$_SESSION['idUsuario']."' as usuario,'".$_SERVER['REMOTE_ADDR']."' as ip ".
                      "FROM alUnidadesTmp tmp ".
                      "WHERE modulo = 'Asig. tractor 12' ". 
                      "AND idUsuario = '".$_SESSION['idUsuario']."' ".
                      "AND vin = '".$rsGetTmp1['root'][$i]['vin']."'";
                //echo "$sql<br>";
                fn_ejecuta_Add($sql);

                if ($_REQUEST['salida']=='ER') {
                    genera530($rsGetTmp1['root'][$i]['vin']);
                }
                

                if ($_REQUEST['salida']=='TYTT' || $_REQUEST['salida']=='CPTT' || $_REQUEST['salida']=='IMGT' || $_REQUEST['salida']=='JCCT' || $_REQUEST['salida']=='LART' || $_REQUEST['salida']=='SWTT' || $_REQUEST['salida']=='TCT' || $_REQUEST['salida']=='TLET' || $_REQUEST['salida']=='TLMT' || $_REQUEST['salida']=='TPKT') {                    
                    $insertTransportacion="INSERT INTO altransportacionestandartrackingobttbl (`VIN`, `fechaOM`, `fechaAuditoria`, `estatusHistorico`, `fechaUpload`, `marca`) 
VALUES ('".$rsGetTmp1['root'][$i]['vin']."', NOW(), NOW(), '".$_REQUEST['salida']."', NOW(), 'FCA')";
                    $guarda=fn_ejecuta_Add($insertTransportacion);
                }

                $sql = "UPDATE alUltimoDetalleTbl ".
                 "SET claveMovimiento = '".$_REQUEST['salida']."', ".
                 "fechaEvento = NOW() + INTERVAL 3 SECOND ".
                 "WHERE vin IN(SELECT VIN FROM alUnidadesTmp WHERE modulo = 'Asig. tractor 12' AND idUsuario = '".$_SESSION['idUsuario']."') ";
            //echo "$sql<br>";
            fn_ejecuta_Add($sql);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "")
            {
                $a['success'] = false;
                $a['sql'] = $sql;
                $a['msjResponse'] = $_SESSION['error_sql']. " En la actualizaci&oacuten del Detalle.";
            }
        

        }
     
            
        echo json_encode($a);
    }

    function obtenRow($arrUnidades,$vinPrm) {
        $totUnidades = sizeof($arrUnidades);
        $row = array();
        for ($i=0; $i < $totUnidades; $i++) {
            $row = $arrUnidades[$i];
            if ($vinPrm == $row['vin']) {
                break;
            }
        }
        return $row;
    }

    function genera530($vin){

        $sql530="SELECT * FROM alhistoricounidadestbl
                where claveMovimiento='ER'
                and centroDistribucion in('CDTOL','CDSAL')
                and vin='".$vin."'";
        $rs530= fn_ejecuta_query($sql530);        

        
            $fechaArchivo=date("Ymd", strtotime("now"));
            $horaArchivo=date("Hi", strtotime("now"));
                      
            $fileDir = "E:\\carbook\\i343a\\HA530".substr($fechaArchivo,2,2).substr($fechaArchivo,4,4).$horaArchivo.".txt";
            
            
            $flReporte660 = fopen($fileDir, "a") or die("No se pudo generar ,interfaz");

            $fecha=date("Ymd", strtotime("now"));
            $hora=date("Hi", strtotime("now"));

            if ($_SESSION['usuCompania']=='CDSAL') {
                $splc='922786000';
            }else if ($_SESSION['usuCompania']=='CDTOL') {
                $splc='958770000';
            }

            $fechaUnidad=date("Ymd", strtotime($rs530['root'][0]['fechaEvento']));
            $horaUnidad=date("Hi", strtotime($rs530['root'][0]['fechaEvento']));

            fwrite($flReporte660,'ISA*03*HA530     *00*          *ZZ*XXDP           *ZZ*ADIMSDCC       *'.substr($fecha,2,2).substr($fecha,4,4).'*'.$hora.'*U*00200*000000001*0*P*<'.PHP_EOL);
            fwrite($flReporte660,'GS*VI*XXDP*INNI*'.substr($fecha,2,2).substr($fecha,4,4).'*'.$hora.'*1*T*1'.PHP_EOL);
            fwrite($flReporte660,'ST*530*10001'.PHP_EOL);
            fwrite($flReporte660,'BV1*XTRA*'.$splc.'*0001'.PHP_EOL);
            fwrite($flReporte660,'VI*'.$vin.'****'.$rs530['root'][0]['distribuidor'].PHP_EOL);
            fwrite($flReporte660,'P2**'.substr($fechaUnidad,2,2).substr($fechaUnidad,4,4).'*A*'.$horaUnidad.PHP_EOL);
            fwrite($flReporte660,'SE*04*10001'.PHP_EOL);

            $fechaFormato=date("YmdHis", strtotime("now"));
            fwrite($flReporte660,'GE*1*1'.PHP_EOL);
            fwrite($flReporte660,'IEA*01*000000001'.PHP_EOL);
            
            
            
     
          $insTra="INSERT INTO altransaccionunidadtbl (tipoTransaccion, centroDistribucion, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento,prodStatus) VALUES ('XXDP', '".$_SESSION['usuCompania']."', '".$vin."', NOW(), 'ER', NOW(),'HA530".substr($fechaArchivo,2,2).substr($fechaArchivo,4,4).$horaArchivo."')";
          $rsTr=fn_ejecuta_query($insTra);

                                    
            //copy($nombreFile, $nombreRespaldo);
            fclose($file);  
            //unlink($nombreFile);

    }
?>