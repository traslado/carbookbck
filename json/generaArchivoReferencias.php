<?php

  session_start();
    ///setlocale(LC_TIME, 'es_MX.utf8');
    $codigoTalonEspecial = 'TE';

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
  
generaArchivo();

  function generaArchivo(){  

  	$tipoTransmision=$_REQUEST['tipoTransmision'];
        $centroDistribucion=$_REQUEST['centroDistribucion'];
        $vin1= $_REQUEST['vines'];


        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");
        $vin=str_ireplace($buscar,$reemplazar,$vin1);

        $cadena = chunk_split($vin, 16,"','");
        
        $vines=substr($cadena,0,-2);
        

    $queryBusqueda="SELECT invoicenumber,
  status,
  processdate,
  
  SUM(
  CASE WHEN modeservice IN ('HL','SH','MVL') then
  amountpaid
  else
  0
  END) as subtotaltransportacion,
 
  SUM(
  CASE WHEN modeservice IN ('HL','SH','MVL') then
  amountpaid*.16
  else
  0
  END) as ivatransportacion, 
  
  
  SUM(
  CASE WHEN modeservice IN ('HL','SH','MVL') then
  amountpaid*.04
  else
  0
  END) as rettransportacion,
  
  
  SUM(
  CASE WHEN modeservice IN ('HL','SH','MVL') then
  amountpaid*1.16 - amountpaid*.04
  else
  0
 
  END) as totaltransportacion,

  SUM(
  CASE WHEN modeservice NOT IN ('HL','SH','MVL') then
  amountpaid
  else
  0
  END) as subtotalserv,
 
  SUM(
  CASE WHEN modeservice NOT IN ('HL','SH','MVL') then
  amountpaid*.16
  else
  0
  END) as ivaserv,
    
  
  SUM(
  CASE WHEN modeservice NOT IN ('HL','SH','MVL') then
  amountpaid*1.16
  else
  0
 
  END) as totalserv,
  SUM(
  CASE WHEN modeservice IN ('HL','SH','MVL') then
  amountpaid*1.16 - amountpaid*.04
  else
  amountpaid*1.16
 
  END) as totalgral

  FROM alreferenciaspaymenttbl a
  
 WHERE invoiceNumber IN ('".$vines.")
 AND modeservice IN ('HL','SH','MVL')
 AND substring(fechaCarga,1,10) = (
    SELECT MAX(substring(b.fechaCarga,1,10))
    FROM alreferenciaspaymenttbl AS b
    WHERE a.invoicenumber = b.invoicenumber
      AND a.status = b.status)
  GROUP BY 1,2";

  $data=fn_ejecuta_query($queryBusqueda);

  for ($i=0; $i <sizeof($data['root']) ; $i++) { 
    # code...

    $sqlFolio="SELECT folio+1 as folio FROM trfoliostbl
            WHERE centroDistribucion='TCO'
            AND tipoDocumento='RF'";
    $rsFolio=fn_ejecuta_query($sqlFolio);

    $folio=$rsFolio['root'][0]['folio'];
  
    $fileDir = "C:/cartaPorte/CONECTOR/TCO".$folio.".txt";
    
    
    $flReporte660 = fopen($fileDir, "a") or die("No se pudo generar ,interfaz");

    //A) ENCABEZADO
                         
    fwrite($flReporte660,'tipoDeComprobante=0'.PHP_EOL);                         
    fwrite($flReporte660,'lugarExpedicion=11520'.PHP_EOL);

    /*if ($data['companiaTractor']=='CRYMEX') {
      $regimen='601';
    }
    if ($data['companiaTractor']=='TRANSDRIZA') {
      $regimen='624';
    }*/

    $regimen='601';

    fwrite($flReporte660,"regimen=".$regimen.PHP_EOL);

    if($data['tipoTalon'] == $codigoTalonEspecial){
            $plaza=$especiales[0]['plaza'];
        } else {
            $plaza=$data['descPlazaDestino'];
        }

        $cliente='1';

  if($cliente =='1'){
            $RFC='CME720930GM9';
            $receptor='STELLANTIS MEXICO, S.A. DE C.V.';
            $plaza1='';

          }

    if ($cliente =='1') {
          $cliente='597';
          $pago='G03';
    }
     

    fwrite($flReporte660,"receptor=".$RFC."|".$receptor." ".$plaza1."|".$cliente."||||||||||correo@tracomex.com.mx|RESIDENCIA_FISCAL|NumRegIdTrib||||".$customer.$TLP.PHP_EOL);
    fwrite($flReporte660,"usoCFDI=".$pago."||||".PHP_EOL);
    fwrite($flReporte660,'serie=TCO'.PHP_EOL);
    //fwrite($flReporte660,'folio='.$data['folioSica']."-".substr($data['centroDistribucion'],2,3).$folio.PHP_EOL);
    fwrite($flReporte660,'folio=TCO'.$folio.PHP_EOL);
    fwrite($flReporte660,'formaDePago=99'.PHP_EOL);
    fwrite($flReporte660,'condicionesDePago='.PHP_EOL);
    //fwrite($flReporte660,'metodoDePago=PPD'.PHP_EOL);
    fwrite($flReporte660,"metodoDePago=PPD|F|TCO".$folio."||".PHP_EOL);
    fwrite($flReporte660,'confirmacion='.PHP_EOL);
    fwrite($flReporte660,'CFDIRelacionados='.PHP_EOL);

     if ($cliente !=='10004' && $data['companiaTractor']=='TRANSDRIZA') {
        $leyenda="TRACOMEX SA DE CV en la calidad de coordinador cumple las obligaciones fiscales de Transdriza SA de CV conforme al artículo 72 fracción V de la ley del impuesto sobre la renta";
     }

    fwrite($flReporte660,"observaciones=".$leyenda."|12|".$data['observacionesCP']."|REFERENCIA".PHP_EOL);

    fwrite($flReporte660,'concepto={78101803|ART-001|1|E48|SERVICIO|SERVICIO DE TRANSPORTACION');
    fwrite($flReporte660,"|".$data['root'][$i]['subtotaltransportacion']."|".$data['root'][$i]['subtotaltransportacion']."|||||Traslados[".$data['root'][$i]['subtotaltransportacion']."|002|Tasa|16.00000|".$data['root'][$i]['ivatransportacion']."|||||]Retenciones[|||||".$data['root'][$i]['subtotaltransportacion']."|002|Tasa|4.000|".$data['root'][$i]['rettransportacion']."]extradata[|||||||||]}".PHP_EOL);/////importes

    fwrite($flReporte660,"subtotal=".$data['root'][$i]['subtotaltransportacion'].PHP_EOL);
    fwrite($flReporte660,'descuento='.PHP_EOL);
    fwrite($flReporte660,'moneda=MXN|1.0'.PHP_EOL);
    fwrite($flReporte660,"totalImpuestosRetenidos=".$data['root'][$i]['rettransportacion'].PHP_EOL);
    fwrite($flReporte660,"totalImpuestosTrasladados=".$data['root'][$i]['ivatransportacion'].PHP_EOL);

    $total=$data['root'][$i]['subtotaltransportacion'] + $data['root'][$i]['ivatransportacion'];
    $total1=$total-$data['root'][$i]['rettransportacion'];

    fwrite($flReporte660,"total=".$total1.PHP_EOL);

    

    $fecha = $data['fechaEvento'];
        $fechaEntrega = strtotime ( "+".$data['diasEntrega']." day" , strtotime ($fecha ));
        $fechaEntrega = date ( 'd/m/Y' , $fechaEntrega );


    /*if ($cliente =='10004') {
          if ($data['companiaTractor']=='CRYMEX') {
            $companiaRFC='ATC900122NU0';
          }
          if ($data['companiaTractor']=='TRANSDRIZA') {
            $companiaRFC='TRA891031SXA';
          }
    }else{*/
       $companiaRFC='TRA8402225G0';
    //}


    
    //echo $data['companiaTractor'];

    fwrite($flReporte660,'emisor='.$companiaRFC.PHP_EOL.PHP_EOL); 



    fwrite($flReporte660,'// COMPLEMENTO DE CARTA PORTE. INICIA CON ESTA LEYENDA Y EL ORDEN DE LOS RENGLONES ES MUY IMPORTANTE RESPETARLO'.PHP_EOL);
    fwrite($flReporte660,'// esto es un comentario'.PHP_EOL);
    fwrite($flReporte660,'COMPLEMENTO_CARTA_PORTE'.PHP_EOL);
    fwrite($flReporte660,'//Header=Version|TranspInternac|EntradaSalidaMerc|ViaEntradaSalida|TotalDistRec'.PHP_EOL);

    fwrite($flReporte660,"Header=1.0|No|||900".PHP_EOL.PHP_EOL);

    //////UBICACIONES 

    fwrite($flReporte660,'// INICIO DE NODO UBICACIONES. CADA NODO UBICACIÓN SE COMPONE DE 4 RENGLONES. PUEDEN SER 1 O MAS NODOS UBICACIÓN. EN PRINCIPIO AL MENOS 2, ORIGEN Y DESTINO'.PHP_EOL);
    fwrite($flReporte660,'// SE REPORTA VACÍO SI NO APLICA EL NODO'.PHP_EOL);
    fwrite($flReporte660,'//Ubicacion=TipoEstacion|DistanciaRecorrida'.PHP_EOL);
    fwrite($flReporte660,'//Origen=idOrigen|RFCRemitente|NombreRemitente|NumRegIdTrib|ResidenciaFiscal|NumEstacion|NombreEstacion|NavegacionTrafico|FechaHoraSalida'.PHP_EOL);
    fwrite($flReporte660,'//Destino=IDDestino|RFCDestinatario|NombreDestinatario|NumRegIdTrib|ResidenciaFiscal|NumEstacion|NombreEstacion|NavegacionTrafico|FechaHoraProgLlegada'.PHP_EOL);
    fwrite($flReporte660,'//Domicilio=calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP'.PHP_EOL.PHP_EOL);      
   
    fwrite($flReporte660,"Ubicacion=01|900.00".PHP_EOL);
    fwrite($flReporte660,"Origen=|TRA8402225G0|CD SALTILLO||||||2022-03-28 16:20:24".PHP_EOL);
    fwrite($flReporte660,"Destino=".PHP_EOL);
    fwrite($flReporte660,"Domicilio=AV. SAN MARCOS S/N PARQUE IND STA FE| FE||||||COA|MEX|25300".PHP_EOL);
    fwrite($flReporte660,"Ubicacion=03|900.00".PHP_EOL);
    fwrite($flReporte660,"Origen=".PHP_EOL);
    fwrite($flReporte660,"Destino=|CME720930GM9|STELLANTIS MEXICO, S.A. DE C.V.||||||2022-03-28 16:20:24".PHP_EOL);
    fwrite($flReporte660,"Domicilio=Carr Toluca - México Km 60.5|Km 60.5||||||MEX|MEX|50100".PHP_EOL.PHP_EOL);
    //fwrite($flReporte660,"Domicilio=INTERIOR RECINTO PORTUARIO|2995||2024|06||007|MIC|MEX|60950".PHP_EOL.PHP_EOL);

    /////MERCANCIAS

    fwrite($flReporte660,'// Inicio de nodo Mercancias. Puede contener 1 o mas nodos mercancia. Cada nodo mercancia se compone de 3 renglones. Debe contener 1 nodo Autotransporte'.PHP_EOL);
    fwrite($flReporte660,'//Mercancias=PesoBrutoTotal|UnidadPeso|PesoNetoTotal|NumTotalMercancias|CargoPorTasacion'.PHP_EOL.PHP_EOL);
    fwrite($flReporte660,"Mercancias=999|Kgs||1|".PHP_EOL.PHP_EOL);

    fwrite($flReporte660,'//Mercancia=BienesTransp|ClaveSTCC|Descripcion|Cantidad|ClaveUnidad|Unidad|Dimensiones|MaterialPeligroso|CveMaterialPeligroso|Embalaje|DescripcionEmbalaje|PesoEnKg|ValorMercancia|Moneda|FraccionArancelaria|UUID'.PHP_EOL);
    fwrite($flReporte660,'//CantidadTransporta={Cantidad|IDOrigen|IDDestino|CvesTrasporte}, {},...{}'.PHP_EOL);
    fwrite($flReporte660,'//DetalleMercancia=UnidadPeso|PesoBruto|PesoNeto|PesoTara|NumPiezas'.PHP_EOL.PHP_EOL);

    
    fwrite($flReporte660,"Mercancia=25101503||TRANSPORTACION DE UNIDADES|1|H87|UNIDAD||||||0999|0|MXN||".PHP_EOL); 
                    
    /////TRANSPORTE

    fwrite($flReporte660,'//AutoTrasporteFederal=PermSCT|NumPermisoSCT|NombreAseg|NumPolizaSeguro'.PHP_EOL);
    fwrite($flReporte660,'//IdentificacionVehicular=ConfigVehicular|PlacaVM|AnioModeloVM'.PHP_EOL);
    fwrite($flReporte660,'//Remolques={SubTipoRem|Placa}{SubTipoRem|Placa}'.PHP_EOL.PHP_EOL);

    fwrite($flReporte660,"AutoTrasporteFederal=TPAF01|00001178|Aseguradora Patrimonial Danos, S.A.|926601".PHP_EOL);
    fwrite($flReporte660,"IdentificacionVehicular=C3R3|1234AC|1998|44.5|".PHP_EOL);    


    fwrite($flReporte660,"Remolques={CTR014|1234AB}".PHP_EOL.PHP_EOL);

    fwrite($flReporte660,'//NODO FIGURA TRANSPORTE, EN TEORÍA APLICA CUANDO EL PROPIETARIO DEL TRANSPORTE, ES DISTINTO AL EMISOR DEL CFDI CON COMPLEMENTO'.PHP_EOL);
    fwrite($flReporte660,'// PUEDEN EXISTIR VARIOS NODOS DE OPERADORES, PROPIETARIOS, ARRENDATARIOS Y NOTIFICADO'.PHP_EOL);
    fwrite($flReporte660,'//FiguraTransporte=CveTransporte'.PHP_EOL);
    fwrite($flReporte660,'//Operadores={RFCOperador|NumLicencia|NombreOperador|NumRegIdTribOperador|Reside|calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP}'.PHP_EOL);
    fwrite($flReporte660,'//Propietario={RFCPropietario|NombrePropietario|NumRegIdTribPropietario|ResidenciaFiscalPropietario|calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP}'.PHP_EOL);
    fwrite($flReporte660,'//Arrendatario={RFCArrendatario|NombreArrendatario|NumRegIdTribArrendatario|ResidenciaFiscalArrendatario|calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP}'.PHP_EOL);
    fwrite($flReporte660,'//Notificado={RFCNotificado|NombreNotificado|NumRegIdTribNotificado|ResidenciaFiscalNotificado|calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP}'.PHP_EOL.PHP_EOL);


    fwrite($flReporte660,'FiguraTransporte=01'.PHP_EOL);
    fwrite($flReporte660,"Operadores=MEGM740701P84|LIC12345|0 - 12 OPERADOR TRACTOR||||||||||||".PHP_EOL);
    fwrite($flReporte660,'Propietario='.PHP_EOL);
    fwrite($flReporte660,'Arrendatario='.PHP_EOL);
    fwrite($flReporte660,"Notificado=CME720930GM9|STELLANTIS MEXICO, S.A. DE C.V.|||1750||0145|03||048|HID|MEX|42083".PHP_EOL);
    
    $sqlFolio="update trfoliostbl set folio=".$folio.
            " WHERE centroDistribucion='TCO'
            AND tipoDocumento='RF'";
    $rsFolio=fn_ejecuta_query($sqlFolio);

      $sqlFolio="update alreferenciaspaymenttbl set folioTimbrado='TCO".$folio.
            "' WHERE invoiceNumber='".$data['root'][$i]['invoicenumber']."';";
    //$rsFolio=fn_ejecuta_query($sqlFolio);

  

     //echo "       INVOICE NUMBER       ".$data['root'][$i]['invoicenumber']."       FOLIO ASIGNADO   TCO".$folio." ";

   }

    
  }



?>