<?php

	/*********************************************
	Sólo necesita alUnidadesVinHdn para funcionar
	*********************************************/

	$savePath = "./../";

	require_once("../funciones/fpdf/fpdf.php");
	require_once("../funciones/barcode.php");
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");

	$success = true;

	$vinArr = explode('|', substr($_REQUEST['alUnidadesVinHdn'], 0, -1));

	$pdf = new FPDF('P', 'mm', array(101, 152));

	for ($nInt=0; $nInt < sizeof($vinArr); $nInt++) { 
		if($vinArr[$nInt] != ""){
			
			$sqlGetFiMe = "SELECT nomFac,dirEnt  FROM alinstruccionesmercedestbl WHERE vin ='".$vinArr[$nInt]."'";
			$rsSqlGetFiMe = fn_ejecuta_query($sqlGetFiMe);

			$sqlGetDataStr = "SELECT h.vin, h.distribuidor, lp.fila, lp.lugar, h.claveMovimiento, dc.tipoDistribuidor, ".
									 "dc.observaciones AS puerto, dc.descripcionCentro , p.pais, h.localizacionUnidad AS patio, h.fechaEvento, pl.plaza ".
									 "FROM caDistribuidoresCentrosTbl dc, caDireccionesTbl di, caColoniasTbl co, ".
									 "caMunicipiosTbl m, caEstadosTbl e, caPaisesTbl p, caPlazasTbl pl, alHistoricoUnidadesTbl h ".
									 "LEFT JOIN allocalizacionpatiostbl lp ON lp.vin = h.vin ".
									 "WHERE dc.distribuidorCentro = h.distribuidor ".
									 "AND pl.idPlaza = dc.idPlaza ".
									 "AND di.direccion = dc.direccionEntrega ".
									 "AND co.idColonia = di.idColonia ".
									 "AND m.idMunicipio = co.idMunicipio ".
									 "AND e.idEstado = m.idEstado ".
									 "AND p.idPais = e.idPais ".
									 "AND h.vin = '".$vinArr[$nInt]."' ".
									 "AND fechaEvento = (SELECT MAX(h1.fechaEvento) ".
									 "FROM alHistoricoUnidadesTbl h1 ". 
									 "WHERE h1.vin = h.vin AND (h1.claveMovimiento = 'L1' OR h1.claveMovimiento = 'L2' OR h1.claveMovimiento = 'L3')) ";

			$rs = fn_ejecuta_query($sqlGetDataStr);	
			$data = $rs['root'][0];
			if (sizeof($data) > 0) {
				$pdf = generarPdf($data, $pdf, $nInt,$rsSqlGetFiMe); 
			}
		}
	}

	$pdf->Output('Etiquetas.pdf', I);

	function generarPdf($data, $pdf, $n,$rsSqlGetFiMe){
		//Solo para pruebas, por defecto 0
		$border = 0;
		if($data['tipoDistribuidor'] == 'DX') {
			$paisPlaza = substr($data['pais'],0,6);
			$puertoNombre = substr($data['puerto'], 0, 9);
		} else {
			$paisPlaza = substr($data['plaza'],0,6);
			$puertoNombre = substr($data['descripcionCentro'],0, 9);
		}

		if(sizeof($data) > 0){
			$pdf->AddPage();
			$pdf->AddFont('skyline', '', 'skyline-reg.php');
			$pdf->SetMargins(0.2, 0.2, 0.2,0.2);
			if ($n > 0) {
				$pdf->SetY(10);
				$pdf->SetX(10);
			}

			//Código Distribuidor y País
			$pdf->SetY(6);
			$pdf->SetX(12);
			$pdf->SetFont('Arial','B',20);
			$pdf->Cell(20,7,$data['distribuidor'], 0,0, 'L');  
			
			$pdf->SetY(20);
			$pdf->SetX(12);
			$pdf->SetFont('skyline','',65);
			$pdf->Cell(25,13,$paisPlaza, 0,0, 'L');
			
			//Patio, Fila, Cajon
			$pdf->SetFont('Arial','B',15);
			$pdf->SetY(7);		
			$pdf->SetX(50);
			$pdf->Cell(33, 5, 'Patio: LC Logistic '.$data['patios'],0,1,'L');

			$pdf->SetY(14);
			$pdf->SetX(50);
			$pdf->Cell(33, 5, 'Fila: '.$rsSqlGetFiMe['root'][0]['nomFac'],0,1,'L');

			$pdf->SetY(21);
			$pdf->SetX(50);
			$pdf->Cell(33, 5, 'Mercado: '.$rsSqlGetFiMe['root'][0]['dirEnt'],0,1,'L');

			/*$pdf->SetY(28);
			$pdf->SetX(50);
			$pdf->Cell(33, 5, 'Mercado: '.substr($data['descripcion'],0,5),0,1,'L');*/


			//Puerto
			$pdf->setY(40);
			$pdf->SetX(9);
			$pdf->SetFont('skyline','',83);
			$pdf->Cell(70,30,$puertoNombre, 0,1, 'C');

			//Fecha y Hora
			if (!isset($data['fechaEvento'])) {
				$data['fechaEvento'] = date("Y-m-d H:i:s");
			}

			$pdf->SetFont('Arial','B',18);
			$pdf->setY(70);
			$pdf->SetX(20);
			$pdf->Cell(50,5,$data['fechaEvento'], 0,1, 'C'); 

			//Avanzada
			$pdf->SetFont('skyline','',75);
			$pdf->setY(85);
			$pdf->SetX(25);
			$pdf->Cell(40,23,substr($data['vin'], -8), 0,1, 'C'); 

			$vin=$data['vin'];

			//****************
			//CODIGO DE BARRAS
			//****************

			$fontSize = 100; // GD1 in px ; GD2 in point
			$marge = 10; // between barcode and hri in pixel
			$x = 400;  // barcode center x
			$y = 60;  // barcode center y
			$height = 180;  // barcode height
			$width = 3;  // barcode width
			$angle = 0; // rotation in degrees 
			$code = $data['vin']; // vin code for barcode
			$type = 'code128'; // barcode type
			$imageTitle = $code.'_barcodeTemp.gif';

			//Se crea la imagen para usar para el codigo de barras
			$im = imagecreatetruecolor(750, 120);
			$black = imagecolorallocate($im, 0x00, 0x00, 0x00);
			$white = imagecolorallocate($im,0xff,0xff,0xff);
			imagefilledrectangle($im, 0, 0, 800, 120, $white);
			//imagestring($im, 5, 335, 100, "*".$data['vin']."*", imagecolorallocate($im, 0, 0, 0));

			//Se genera el Codigo de barras
			$data = Barcode::gd($im, $black, $x, $y, $angle, $type,   array('code'=>$code), $width, $height);
			Header('Content-type: image/gif');
			imagegif($im, $imageTitle);

			//Código de Barras y vin

			$pdf->Image($imageTitle,9,110, 80);

			//VIN
			$pdf->SetFont('Arial','',14);
			$pdf->setY(125);
			$pdf->SetX(26);
			$pdf->Cell(40,5,"*".$vin."*", 0,1, 'C');
			// I para mostrar, F para salvar
			/*$saveFile = $savePath.$code.'.pdf';
			$pdf->Output($code.'.pdf', 'D');
			//$pdf->Output($saveFile, 'F');*/

			imagedestroy($im);
			
			if (file_exists($imageTitle)) {
				unlink($imageTitle);
			}

			return $pdf;

		} else {
			echo json_encode(array('success'=>false, 'errorMessage'=>$_SESSION['error_sql']." <br> ".$sqlGetDataStr));
		}
	}
?>