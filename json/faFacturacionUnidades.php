<?php
	session_start();
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("../funciones/Classes/PHPExcel.php");

    $_REQUEST = trasformUppercase($_REQUEST);
    
    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
    
    switch($_REQUEST['faFacturacionUnidadesActionHdn']){
        case 'facturarUnidades':
        	facturarUnidades();
        	break;
        case 'facturarChrysler':
            facturarChrysler();
            break;
        case 'cobrarUnidades':
        	cobrarUnidades();
        	break;
        case 'leerArchivoChrysler':
        	leerArchivoChrysler();
        	break;
        case 'loadUnidadesChryslerTemp':
            loadUnidadesChryslerTemp();
            break;
        case 'clearChryslerTemp':
            clearChryslerTemp();
            break;
        case 'updVinVision':
            updVinVision();
            break;
       	default:
            echo '';
 	}

 	function facturarUnidades(){
 		$a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturacionUnidadesSerieTxt'] == ""){
            $e[] = array('id'=>'faFacturacionUnidadesSerieTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionUnidadesNumeroTxt'] == ""){
            $e[] = array('id'=>'faFacturacionUnidadesNumeroTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionUnidadesFechaTxt'] == ""){
            $e[] = array('id'=>'faFacturacionUnidadesFechaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $vinArr = explode('|', substr($_REQUEST['faFacturacionUnidadesVinTxt'], 0, -1));
        if(in_array('', $vinArr)){
            $e[] = array('id'=>'faFacturacionUnidadesVinTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
        	foreach ($vinArr as $vin) {
        		//REVISAR SI EXISTE UN REGISTRO
        		$sqlCheckFacturacion = 	"SELECT 1 FROM faFacturacionTransportacionTbl ".
        								"WHERE vin = '".$vin."'";

        		$rs = fn_ejecuta_query($sqlCheckFacturacion);

        		if(sizeof($rs['root']) > 0){
        			$sqlAddUpdFacturacion = "UPDATE faFacturacionTransportacionTbl SET ".
        									"serFactura = '".$_REQUEST['faFacturacionUnidadesSerieTxt']."',".
        									"numeroFactura = ".$_REQUEST['faFacturacionUnidadesNumeroTxt'].",".
        									"fechaFactura = '".$_REQUEST['faFacturacionUnidadesFechaTxt']."' ".
        									"WHERE vin = '".$vin."'";
        		} else {
        			$sqlAddUpdFacturacion = "INSERT INTO faFacturacionTransportacionTbl ".
        									"(vin, avanzada, serFactura, numeroFactura, fechaFactura) VALUES (".
        										"'".$vin."',".
        										"'".substr($vin, -8)."',".
        										"'".$_REQUEST['faFacturacionUnidadesSerieTxt']."',".
        										$_REQUEST['faFacturacionUnidadesNumeroTxt'].",".
        										"'".$_REQUEST['faFacturacionUnidadesFechaTxt']."')";
        		}

        		fn_ejecuta_query($sqlAddUpdFacturacion);

        		if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
        			$a['successMessage'] = getFacturacionUnidadesSuccessMsg();
        		} else {
        			$a['success'] = false;
        			$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUpdFacturacion;
        		}
        	}
        }

        $a['errors'] = $e;
        echo json_encode($a);
 	}

    function facturarChrysler(){
        $a = array();
        $e = array();
        $a['success'] = true;

        //PARA AUMENTAR EL TIEMPO DE EJECUCION POR SI SON MUCHAS UNIDADES
        set_time_limit(60);

        if($_REQUEST['faFacturacionNombrePeriodoHdn'] == ""){
            $e[] = array('id'=>'faFacturacionNombrePeriodoHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionFechaInicioTxt'] == ""){
            $e[] = array('id'=>'faFacturacionFechaInicioTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionFechaFinalTxt'] == ""){
            $e[] = array('id'=>'faFacturacionFechaFinalTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            $sqlGetChrysler =   "SELECT mu.tipoFactura, mu.busUnit, mu.mercado, mu.simboloUnidad, mu.serie, mu.vin, ".
                                    "mu.distribuidor, mu.fechaDocumento, ".
                                    "(SELECT su.marca FROM caSimbolosUnidadesTbl su ".
                                        "WHERE su.simboloUnidad = mu.simboloUnidad) AS marca,".
                                    "(SELECT ma.descripcion FROM caMarcasUnidadesTbl ma ".
                                        "WHERE ma.marca = (SELECT su2.marca FROM caSimbolosUnidadesTbl su2 ".
                                        "WHERE su2.simboloUnidad = mu.simboloUnidad)) AS descripcionMarca, ".
                                    "(SELECT 1 FROM faFacturacionTransportacionTbl ft WHERE ft.vin = mu.vin) AS isUpdate ".
                                "FROM alMovimientoUnidadesDetalleTbl mu ".
                                "WHERE mu.vin NOT IN (SELECT mu2.vin FROM alMovimientoUnidadesDetalleTbl mu2 ".
                                    "WHERE mu2.estatus = 'B') ";

            $rs = fn_ejecuta_query($sqlGetChrysler);
            
            if(sizeof($rs['root']) > 0){
                $sqlAddFacturacion = "INSERT INTO faFacturacionTransportacionTbl ".
                                        "(vin, avanzada, tipoDocumento, busUnit, mercado, modelo, marca,".
                                            "descripcion, serie, distribuidor, fechaDocumento) VALUES ";

                $insertData = "";

                for ($i=0; $i < sizeof($rs['root']); $i++) {

                    if($rs['root'][$i]['isUpdate'] == "1"){
                        $sqlUpdFacturacion =    "UPDATE faFacturacionTransportacionTbl SET ".
                                                "tipoDocumento = '".$rs['root'][$i]['tipoFactura']."',".
                                                "busUnit = ".$rs['root'][$i]['busUnit'].",".
                                                "mercado = '".$rs['root'][$i]['mercado']."', ".
                                                "modelo = '".$rs['root'][$i]['simboloUnidad']."', ".
                                                "marca = '".$rs['root'][$i]['marca']."', ".
                                                "descripcion = '".$rs['root'][$i]['descripcionMarca']."', ".
                                                "serie = '".$rs['root'][$i]['serie']."', ".
                                                "distribuidor = '".$rs['root'][$i]['distribuidor']."', ".
                                                "fechaDocumento = '".$rs['root'][$i]['fechaDocumento']."' ".
                                                "WHERE vin = '".$rs['root'][$i]['vin']."'";

                        fn_ejecuta_query($sqlUpdFacturacion, false);
                    } else {
                        if($insertData != "")
                            $insertData .= ",";

                        $insertData .=   "('".$rs['root'][$i]['vin']."',".
                                        "'".substr($rs['root'][$i]['vin'], -8)."',".
                                        "'".$rs['root'][$i]['tipoFactura']."',".
                                        "'".$rs['root'][$i]['busUnit']."',".
                                        "'".$rs['root'][$i]['mercado']."',".
                                        "'".$rs['root'][$i]['simboloUnidad']."',".
                                        "'".$rs['root'][$i]['marca']."',".
                                        "'".$rs['root'][$i]['descripcionMarca']."',".
                                        "'".$rs['root'][$i]['serie']."',".
                                        "'".$rs['root'][$i]['distribuidor']."',".
                                        "'".$rs['root'][$i]['fechaDocumento']."')";
                    }
                }

                fn_ejecuta_query($sqlAddFacturacion.$insertData, false);

                if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){                    
                    $sqlAddPeriodo =    "INSERT INTO caPeriodosFacturacionTbl (fechaInicio, fechaFinal, nombre, estatus) ".
                                        "VALUES (".
                                            "'".$_REQUEST['faFacturacionFechaInicioTxt']."',".
                                            "'".$_REQUEST['faFacturacionFechaFinalTxt']."',".
                                            "'".$_REQUEST['faFacturacionNombrePeriodoHdn']."',".
                                            "'A')";

                    fn_ejecuta_query($sqlAddPeriodo);

                    if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        $a['successMessage'] = getFacturacionUnidadesSuccessMsg();
                    } else {
                        $a['success'] = false;
                        $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddFacturacion;
                    }

                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddFacturacion;
                }
            }
        }

        $a['errors'] = $e;
        echo json_encode($a);
    }

 	function cobrarUnidades(){
 		$a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['faFacturacionUnidadesSerieTxt'] == ""){
            $e[] = array('id'=>'faFacturacionUnidadesSerieTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionUnidadesNumeroTxt'] == ""){
            $e[] = array('id'=>'faFacturacionUnidadesNumeroTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['faFacturacionUnidadesFechaTxt'] == ""){
            $e[] = array('id'=>'faFacturacionUnidadesFechaTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
    		$sqlUpdFacturacion=	"UPDATE faFacturacionTransportacionTbl SET ".
								"fechaCobro = '".$_REQUEST['faFacturacionUnidadesFechaTxt']."' ".
								"WHERE serFactura='".$_REQUEST['faFacturacionUnidadesSerieTxt']."' ".
								"AND numeroFactura=".$_REQUEST['faFacturacionUnidadesNumeroTxt'];

    		fn_ejecuta_query($sqlUpdFacturacion);

    		if(!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
    			$a['successMessage'] = getFacturacionUnidadesCobroMsg();
    		} else {
    			$a['success'] = false;
    			$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdFacturacion;
    		}
        }

        $a['errors'] = $e;
        echo json_encode($a);
 	}

 	function leerArchivoChrysler(){
 		$a = array();
		$a['success'] = true;

		if(isset($_FILES)) {
			if($_FILES["movimientosUnidadesArchivoFile"]["error"] > 0){
				$a['success'] = false;
				$a['message'] = $_FILES["movimientosUnidadesArchivoFile"]["error"];
			} else {
				$tempFileName = $_FILES['movimientosUnidadesArchivoFile']['tmp_name']; 
				$originalFileName = $_FILES['movimientosUnidadesArchivoFile']['name'];
			}

            if (!file_exists($tempFileName)){
                $a['success'] = false;
                $a['errorMessage'] = "Error al procesar el archivo " . $tempFileName;                   
            } else {
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($tempFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($tempFileName);
                } catch(Exception $e) {
                    die('Error cargando archivo "'.pathinfo($tempFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                }

                //LIMPIAMOS LA TABLA
                fn_ejecuta_query("TRUNCATE TABLE alMovimientoUnidadesDetalleTbl");

                $sheet = $objPHPExcel->getSheet(0); 
                $highestRow = $sheet->getHighestRow();

                $highestColumn = $sheet->getHighestColumn();

                $sheetData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow);

                $fechaInicio = null;
                $fechaFinal = null;

                //FORMATO DE COMO SE LEE LA FECHA DEL ARCHIVO
                $dateFormat = "m-d-y";

                $unidades = array();

                $sqlAddVin =    "INSERT INTO alMovimientoUnidadesDetalleTbl (tipoFactura,busUnit,mercado,".
                                "simboloUnidad,serie,vin,distribuidor,fechaDocumento,estatus) VALUES ";

                $dataInsert = "";

                for ($i=0; $i < sizeof($sheetData); $i++) {
                    $tempDate = date_create_from_format($dateFormat, $sheetData[$i][7]);

                    if($fechaInicio == null){
                        $fechaInicio = $tempDate;
                    } else {
                        if($tempDate < $fechaInicio){
                            $fechaInicio = $tempDate;  
                        }
                    }

                    if($fechaFinal == null){
                        $fechaFinal = $tempDate;
                    } else {
                        if($tempDate > $fechaFinal){
                            $fechaFinal = $tempDate;  
                        }
                    }

                    $estatus = 'A';

                    if(in_array($sheetData[$i][5], $unidades)){
                        $estatus = 'B';
                    } else {
                        array_push($unidades, $sheetData[$i][5]);
                    }

                    if($dataInsert != "")
                        $dataInsert .= ",";

                    $dataInsert .=   "('".$sheetData[$i][0]."',".
                                    "'".$sheetData[$i][1]."',".
                                    "'".$sheetData[$i][2]."',".
                                    "'".$sheetData[$i][3]."',".
                                    "'".$sheetData[$i][4]."',".
                                    "'".$sheetData[$i][5]."',".
                                    "'".$sheetData[$i][6]."',".
                                    "'".date_format($tempDate, "Y-m-d")."',".
                                    "'".$estatus."')";

                }

                fn_ejecuta_query($sqlAddVin.$dataInsert, false);

                if(sizeof($sheetData) == 0){
                    $a['success'] = false;
                    $a['errorMessage'] = "No se encontraron unidades en el archivo ingresado";
                }

                $a['fechaInicio'] =  date_format($fechaInicio, 'd/m/Y');
                $a['fechaFinal'] = date_format($fechaFinal, 'd/m/Y');
                $a['records'] = $highestRow-1;
            }
		}

        echo json_encode($a);
 	}

    function loadUnidadesChryslerTemp(){

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesNomPeriodoTxt'], "MO.nombrePeriodo", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesFinicioCmb'], "MO.fechaInicial", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['movimientoUnidadesFfinalCmb'], "MO.fechaFinal", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetChrysler  =  "SELECT tipoFactura,busUnit,mercado,simboloUnidad,serie,vin,distribuidor,fechaDocumento ".
                            "FROM alMovimientoUnidadesDetalleTbl ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetChrysler);

        if($_REQUEST['start'] != '' && $_REQUEST['limit'] != ''){
            $rs['root'] = array_slice($rs['root'], $_REQUEST['start'], $_REQUEST['start']+$_REQUEST['limit']);
        }     

        echo json_encode($rs);
    }

    function clearChryslerTemp(){
        fn_ejecuta_query("TRUNCATE TABLE alMovimientoUnidadesDetalleTbl");
    }

    function updVinVision(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $vinArr = explode('|', substr($_REQUEST['faFacturacionUnidadesVinTxt'],0,-1));
        if(in_array("", $vinArr)){
            $e[] = array('id'=>'faFacturacionUnidadesVinTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if($a['success']){
            foreach ($vinArr as $vin) {
                $sqlUpdVv = "UPDATE faFacturacionTransportacionTbl SET ".
                            "omVv = 'OM' ".
                            "WHERE vin = '".$vin."'";

                fn_ejecuta_query($sqlUpdVv);

                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql']."<br>".$sqlUpdVv;
                }
            }
        }

        if($a['success'])
            $a['successMessage'] = "Unidades Actualizadas Correctamente";

        echo json_encode($a);
    }

?>