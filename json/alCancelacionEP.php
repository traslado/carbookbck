	<?php

	session_start();
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");

	switch($_REQUEST['alUnidadesActionHdn']){
	    case 'getBuscaVin':
	    	getBuscaVin();		
	    break;
	    case 'getValidaUD':
	    	getValidaUD();		
	    break;
	   	case 'getQuitaEstatusEP':
	    	getQuitaEstatusEP();		
	    break;
	    default:
	    echo '';
	}

	function getBuscaVin(){
		$sqlConsulta = "SELECT vin FROM alunidadesTbl ".
  						"WHERE avanzada = '".$_REQUEST['alUnidadesAvanzadaHdn']."' ";
	    
	    $rsSqlConsulta = fn_ejecuta_query($sqlConsulta);

		echo json_encode($rsSqlConsulta);  						
	}
	function getValidaUD(){
		$sqlConsulta = "SELECT * FROM alultimodetalletbl ".
							"WHERE vin = '".$_REQUEST['alUnidadesVinHdn']."' ";
	    
	    $rsSqlConsulta = fn_ejecuta_query($sqlConsulta);

		echo json_encode($rsSqlConsulta);  						
	}	

	function getQuitaEstatusEP(){
 		
 		$json = json_decode($_REQUEST["changes"],true);			

 		for ($i=0; $i < sizeof($json); $i++) { 


 		$sqlDeleteEP = " INSERT INTO alhistoricoUnidadesTbl (centroDistribucion,vin, fechaEvento,claveMovimiento, ".
 						"distribuidor,idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip) ".
 						"SELECT centroDistribucion,vin,now() as fechaEvento,'XZ' as claveMovimiento,distribuidor, ".
 					   "idTarifa,localizacionUnidad,claveChofer,observaciones,usuario,ip  FROM alhistoricoUnidadesTbl ".
 					   "WHERE vin ='".$json[$i]["vin"]."' AND CAST(fechaEvento AS DATE) ='".$_REQUEST["fechaCambios"]."' ".
 					   "AND claveMovimiento in ('EP') ";
	    
	    $rsSqlDeleteEP = fn_ejecuta_query($sqlDeleteEP);

 		$sqlDeleteEP = "DELETE FROM alhistoricoUnidadesTbl ".
 					   "WHERE vin ='".$json[$i]["vin"]."' AND CAST(fechaEvento AS DATE) ='".$_REQUEST["fechaCambios"]."' ".
 					   "AND claveMovimiento in ('EP','HO') ";

	    $rsSqlDeleteEP = fn_ejecuta_query($sqlDeleteEP);

 		$sqlSelUd ="SELECT vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
              "claveChofer, observaciones, usuario, ip ".
              "FROM alhistoricounidadestbl h1 ".
              "WHERE h1.vin = '".$json[$i]["vin"]."' ".
              "AND h1.fechaEvento = (SELECT MAX(hu2.fechaEvento) ".
                                    "FROM alhistoricounidadestbl hu2 ".
                                    "WHERE hu2.vin = h1.vin ".
                                    "AND hu2.claveMovimiento not in (SELECT ge.valor ". 
                                                                    "FROM cageneralestbl ge ". 
                                                                    "WHERE ge.tabla = 'alHistoricoUnidadesTbl' ".
                                                                    "AND ge.columna = 'nvalidos')) ";

                  $rssqlSelUd= fn_ejecuta_query($sqlSelUd);                                                            


                  $dltUdUnd = "SELECT *  FROM alultimodetalletbl ".
                              "WHERE vin = '".$json[$i]["vin"]."' ";

                  $rsVin = fn_ejecuta_query($dltUdUnd);


                  if (sizeof($rsVin['root']) == 1 ) {

                            if ($rssqlSelUd['root'][0]['claveChofer'] == null)
                             {

                                 $claveChofer= "null";
                              }
                            else 
                              {
                                 $claveChofer=$rssqlSelUd['root'][0]['claveChofer'];

                              }


                  $sqlUpdUnd = "UPDATE alultimodetalletbl ".
                        "SET centroDistribucion= '".$rssqlSelUd['root'][0]['centroDistribucion']."', ".
                        "fechaEvento = '".$rssqlSelUd['root'][0]['fechaEvento']."', ".
                        "claveMovimiento = '".$rssqlSelUd['root'][0]['claveMovimiento']."', ".
                        "distribuidor = '".$rssqlSelUd['root'][0]['distribuidor']."', ".
                        "idTarifa = '".$rssqlSelUd['root'][0]['idTarifa']."', ".
                        "localizacionUnidad = '".$rssqlSelUd['root'][0]['localizacionUnidad']."', ".
                        "claveChofer =".$claveChofer.",".
                        "usuario = '".$_SESSION['idUsuario']."', ".
                        "ip = '".getClientIP()."' ".
                        "WHERE vin = '".$rssqlSelUd['root'][0]['vin']."'";

                    fn_ejecuta_query($sqlUpdUnd);

                  }

                  else

                  {

                  $addUdUnd = "INSERT INTO alultimodetalletbl(vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, ".
                              "localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
                              "SELECT vin, centroDistribucion, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                              "claveChofer, observaciones, usuario, ip ".
                              "FROM alhistoricounidadestbl h1 ".
                              "WHERE h1.vin = '".$json[$i]["vin"]."' ".
                              "AND h1.fechaEvento = (SELECT MAX(hu2.fechaEvento) ".
                                                    "FROM alhistoricounidadestbl hu2 ".
                                                    "WHERE hu2.vin = h1.vin ".
                                                    "AND hu2.claveMovimiento not in (SELECT ge.valor ". 
                                                                                    "FROM cageneralestbl ge ". 
                                                                                    "WHERE ge.tabla = 'alHistoricoUnidadesTbl' ".
                                                                                    "AND ge.columna = 'nvalidos')) ";

                    fn_ejecuta_query($addUdUnd);

                  }

		echo json_encode($sqlDeleteEP);  

 		}			
	}	
?>