<?php
    session_start();
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");
    
    $a = array();
    $a['successTitle'] = "Manejador de Archivos";
    
    if(isset($_FILES)) {
        if($_FILES["alCargaDisponiblesFld"]["error"] > 0){
            $a['success'] = false;
            $a['message'] = $_FILES["alCargaDisponiblesFld"]["error"];

        } else {
            $temp_file_name = $_FILES['alCargaDisponiblesFld']['tmp_name']; 
            $original_file_name = $_FILES['alCargaDisponiblesFld']['name'];
          
            // Find file extention 
            $ext = explode ('.', $original_file_name); 
            $ext = $ext [count ($ext) - 1]; 
            
            // Remove the extention from the original file name 
            $file_name = str_replace ($ext, '', $original_file_name); 
            
            $new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;

            if (move_uploaded_file($temp_file_name, $new_name)){

                if (!file_exists($new_name)){
                    $a['success'] = false;
                    $a['errorMessage'] = "Error al procesar el archivo " . $new_name;
                } else {
                    $a['success'] = true;
                    $a['successMessage'] = "Archivo Cargado";
                    $a['archivo'] = $file_name . $ext;
                    $a['root'] = leerXLS($new_name);
                }
            } else { 
                $a['success'] = false;
                $a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
            }
        }
    } else {
        $a['success'] = false;
        $a['errorMessage'] = "Error FILES NOT SET";
    }
    
    echo json_encode($a); 

    function leerXLS($inputFileName) {
        /** Error reporting */
        error_reporting(E_ALL);
        
        date_default_timezone_set ("America/Mexico_City");
        
        //  Include PHPExcel_IOFactory
        include '../funciones/Classes/PHPExcel/IOFactory.php';
        
        //$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
        //echo $inputFileName;
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader =
             PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        
        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        //Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
        $rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

        
        //-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

        if (ord(strtoupper($highestColumn)) <= 5 && $highestRow <= 5 ) {
            $root[] = array('resultado' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
            return $root;
        }

        if (ord(strtoupper($highestColumn)) < 2) {
            $root[] = array('resultado' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
            return $root;
        }

    

        if (strlen($rowData[0][0]) < 17) {
            $root[] = array('resultado' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el orden correcto', 'fail'=>'Y');
            return $root;
        }

       
        $isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
                                //'Simbolo'=>array('val'=>'','size'=>true,'exist'=>true),
                                'Distribuidor' =>array('val'=>'','size'=>true,'exist'=>true),
                                //'Color' =>array('val'=>'','size'=>true,'exist'=>true),
                                'Avanzada' =>array('val'=>'','size'=>true,'exist'=>true)
                            );
        
        for($row = 0; $row<sizeof($rowData); $row++){
            //Validar si las unidades estan bien o no
            //Verifico si la longitud de loas campos sea la correcta...
            $isValidArr['VIN']['val'] = $rowData[$row][0];
            $isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
            $isValidArr['VINN']['val'] = $rowData[$row][0];
            $isValidArr['VINN']['size'] = strlen($rowData[$row][0]) == 17;
            /*$isValidArr['Simbolo']['val'] = $rowData[$row][1];
            $isValidArr['Simbolo']['size'] = strlen($rowData[$row][1]) > 0;*/
            $isValidArr['Distribuidor']['val'] = $rowData[$row][1];
            $isValidArr['Distribuidor']['size'] = strlen($rowData[$row][1]) <= 6;
            /*$isValidArr['Color']['val'] = $rowData[$row][3];
            $isValidArr['Color']['size'] = strlen($rowData[$row][3]) > 0;*/
            $isValidArr['Avanzada']['size'] = substr($rowData[$row][0],9,17);


            $errorMsg = '';
            $isTrue = true;

            foreach ($isValidArr as $key => $value) {
                if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
                    $errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
                    $isTrue = false;
                } else {// de lo contrario verifico que exista en la base
                    if ($key == 'VIN') {
                        
                        $isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
                        if (!$isValidArr[$key]['exist']) {
                                if ($key == 'VIN') {
                                    $siNo = 'Ya Existe';
                                }
                                /* if ($key == 'VINN') {
                                    $siNo = 'Falta cargar en Blue Report';
                                }*/
                            $errorMsg .= 'El VIN '.$siNo;
                            $isTrue = false; 
                        }
                    }
                }
            }

            if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
                //Busco el idTarifa
                $sqlidTarifa = "SELECT ct.idTarifa FROM caSimbolosUnidadesTbl su, caClasificacionTarifasTbl ct,".
                               " caTarifasTbl t WHERE su.clasificacion = ct.clasificacion ".
                               "AND ct.idTarifa = t.idTarifa ".
                               "AND su.simboloUnidad = 'SIPEN'";

                $rsTarifa = fn_ejecuta_query($sqlidTarifa);

                /*$sqlColor = "SELECT  1  as colorExistente FROM cacolorunidadestbl cu, casimbolosunidadestbl su ".
                                "WHERE  cu.marca = su.marca ".
                                "AND su.simboloUnidad = '".$rowData[$row][1]."' ".
                                "AND  cu.color = '".$rowData[$row][3]."'";

                $rsColorUnidades = fn_ejecuta_query($sqlColor);                             */
                
                $sqluD = "SELECT  1  as vin FROM alUltimoDetalleTbl ".
                                "WHERE  vin = '".$rowData[$row][0]."'";

                $rsSqlUd = fn_ejecuta_query($sqluD);        

                if (!$rsTarifa || sizeof($rsTarifa['root']) < 1) {
                    $errorMsg = 'Registro No Agregado. Verifica la Tarifa, S&iacute;mbolo o Clasificaci&oacute;n';
                    
                }/*elseif(sizeof($rsColorUnidades['root']) < 1){
                    $errorMsg = 'Registro No Agregado. Verifica el Color para la marca';
                }*/ else {

                    //Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
                    if (sizeof($rsSqlUd['root']) == 1) {

                        if ($_SESSION['usuCompania'] =='CDTOL') {
                            $clave='UT';
                        }else{
                            $clave='FH';
                        }



                    $addHist = "INSERT INTO alHistoricoUnidadesTbl (centroDistribucion ,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, Observaciones, usuario, ip)".
                    "VALUES( '".$_SESSION['usuCompania']."', ".
                        "'".$rowData[$row][0]."', ".
                        "NOW(), ".
                        "'".$clave."', ".
                        "'".$rowData[$row][1]."', ".
                        "'".$rsTarifa['root'][0]['idTarifa']."', ".
                        "'".$_SESSION['usuCompania']."', ".
                        replaceEmptyNull("'".$chofer."'").", ".             
                        replaceEmptyNull("'".$RQobservaciones."'").", ".                        
                         "'".$_SESSION['idUsuario']."', ".
                         "'".$_SERVER['REMOTE_ADDR']."') ";

                    fn_ejecuta_query($addHist);

                    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
                                            "SET centroDistribucion='".$_SESSION['usuCompania']."', ".
                                            "fechaEvento = NOW(), ".
                                            "claveMovimiento = '".$clave."', ".
                                            "distribuidor = '".$rowData[$row][1]."', ".
                                            "idTarifa = '".$rsTarifa['root'][0]['idTarifa']."', ".
                                            "localizacionUnidad = '".$_SESSION['usuCompania']."', ".
                                            "claveChofer = null, ". 
                                            "Observaciones = null, ".   
                                            "usuario = '".$_SESSION['idUsuario']."', ".
                                            "ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                            "WHERE vin = '".$rowData[$row][0]."' ";


                    fn_ejecuta_query($sqlUpdUltimoDetalleStr);


                $actualizaalUnidad =       "UPDATE alUnidadesTbl ".
                                            "SET distribuidor = '".$rowData[$row][1]."' ".
                                            "WHERE vin = '".$rowData[$row][0]."' ";


                    fn_ejecuta_query($actualizaalUnidad);


                    }
                    else {
                    $addTR = "INSERT INTO alUnidadesTbl (vin, avanzada, distribuidor,simboloUnidad,color,folioRepuve, descripcionUnidad)".
                    "VALUES( '".$rowData[$row][0]."' ,".
                        "'".substr($rowData[$row][0],9,17)."', ".
                        "'".$rowData[$row][1]."', ".
                        "'SIPEN', ".
                        "'PW7', ".                        
                        replaceEmptyNull("'".$repuve."'").", ".                 
                        replaceEmptyNull("'".$rowData[$row][5]."'").") ";

                    fn_ejecuta_query($addTR);

                    $addHist = "INSERT INTO alHistoricoUnidadesTbl (centroDistribucion ,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, Observaciones, usuario, ip)".
                    "VALUES( '".$_SESSION['usuCompania']."', ".
                        "'".$rowData[$row][0]."', ".
                        "NOW(), ".
                        "'".$clave."', ".
                        "'".$rowData[$row][1]."', ".
                        "'".$rsTarifa['root'][0]['idTarifa']."', ".
                        "'".$_SESSION['usuCompania']."', ".
                        replaceEmptyNull("'".$chofer."'").", ".             
                        replaceEmptyNull("'".$RQobservaciones."'").", ".                        
                         "'".$_SESSION['idUsuario']."', ".
                         "'".$_SERVER['REMOTE_ADDR']."') ";

                    fn_ejecuta_query($addHist);


                    $addUdet = "INSERT INTO alUltimoDetalleTbl (centroDistribucion ,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, Observaciones, usuario, ip)".
                    "VALUES( '".$_SESSION['usuCompania']."', ".
                        "'".$rowData[$row][0]."', ".
                        "NOW(), ".
                        "'".$clave."', ".
                        "'".$rowData[$row][1]."', ".
                        "'".$rsTarifa['root'][0]['idTarifa']."',".
                        "'".$_SESSION['usuCompania']."', ".                     
                        replaceEmptyNull("'".$chofer."'").", ".                 
                        replaceEmptyNull("'".$RQobservaciones."'").", ".
                         "'".$_SESSION['idUsuario']."', ".
                         "'".$_SERVER['REMOTE_ADDR']."') ";                 

                    fn_ejecuta_query($addUdet);

                    }/*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCompania'],'PR',
                              $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCompania'], $repuve, $chofer, $observaciones,$rowData[$row][4]);*/

                              

                    if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                        $errorMsg = 'Agregado Correctamente';
                    } else {
                        $errorMsg = 'Registro No Agregado';
                    }
                    
                }
            }
            
            $root[]= array('VIN'=>$rowData[$row][0], 'distribuidor'=>$rowData[$row][1],'descripcion'=>$rowData[$row][2],'buque'=>$rowData[$row][5],'fecha'=>$rowData[$row][8],'resultado'=>$errorMsg);        

            //echo json_encode($rowData[$row]);
        }
        return $root;   

    }


  function isFieldInDataBase($field, $value,$TLP) { //Funcion que checa que un valor exista en la base
        $tabla = 'tabla';
        $columna = 'columna';

        switch(strtoupper($field)) {
            case 'VIN':
                $tabla = 'alhistoricounidadestbl'; $columna = 'vin'; break;
            
        }
        $sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."' AND claveMovimiento in ('FH','UT');". $AND;
        $rs = fn_ejecuta_query($sqlExist);
        return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
    }




?>