<?php
    session_start();
	$_SESSION['modulo'] = "catTractores";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("trMantenimiento.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['catTractoresActionHdn']) {
        case 'getTractores':
            getTractores();
            break;
		case 'addTractor':
			addTractor();
            break;
		case 'updTractor':
			updTractor();
            break;
		case 'dltTractor':
			dltTractor();
        case 'bloquearTractor':
            bloquearTractor();
            break;
        case 'updBloqueoTractor':
            updBloqueoTractor();
            break;
        case 'liberarTractor':
            echo json_encode(liberarTractor($_REQUEST['catTractorIdTractorHdn']));
            break;
        case 'liberarTractorMasivo':
            liberarTractorMasivo();
            break;
        case 'getTractoresDisponibles':
            getTractoresDisponibles();
            break;
        case 'getTractoresBloqueados':
            getTractoresBloqueados();
            break;
        case 'getTractorUltimoMovimiento':
            getTractorUltimoMovimiento();
            break;    
        default:
            echo '';
    }

    function getTractores(){
    	$lsWhereStr = "";

		if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresIdTractorHdn'], "idTractor", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresCiaHdn'], "compania", 1, 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresTractorTxt'], "tractor", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresMarcaHdn'], "marca", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresModeloTxt'], "modelo", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresSerieTxt'], "serie", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresPlacaTxt'], "placas", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresRendimientoTxt'], "rendimiento", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresEjesTxt'], "ejes", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresObservacionesTxt'], "observaciones", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresEstatusHdn'], "estatus", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }
	    if ($gb_error_filtro == 0){
    		$ls_condicion = fn_construct($_REQUEST['catTractoresIaveTxt'], "tarjetaIave", 0);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $ls_condicion);
	    }

	    $sqlGetTractoresStr = "SELECT tc.*, ".
                              "(SELECT co.descripcion FROM caCompaniasTbl co WHERE co.compania=tc.compania) AS nombreCompania, ".
                              "(SELECT mu.descripcion FROM caMarcasUnidadesTbl mu WHERE mu.marca=tc.marca) AS nombreMarca, ".
                              "(SELECT cg.nombre FROM caGeneralesTbl cg ".
                                        "WHERE cg.valor=tc.estatus AND tabla='caTractoresTbl' ".
                                        "AND columna='estatus') AS nombreEstatus ".
                              "FROM caTractoresTbl tc ".$lsWhereStr;

		$rs = fn_ejecuta_query($sqlGetTractoresStr);

		echo json_encode($rs);
    }

	function addTractor(){

		$a = array();
        $e = array();
        $a['success'] = true;

		if($_REQUEST['catTractoresCiaHdn'] == ""){
            $e[] = array('id'=>'catTractoresCiaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresTractorTxt'] == ""){
            $e[] = array('id'=>'catTractoresTractorTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresMarcaHdn'] == ""){
            $e[] = array('id'=>'catTractoresMarcaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresModeloTxt'] == ""){
            $e[] = array('id'=>'catTractoresModeloTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresSerieTxt'] == ""){
            $e[] = array('id'=>'catTractoresSerieTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresPlacaTxt'] == ""){
            $e[] = array('id'=>'catTractoresPlacaTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresRendimientoTxt'] == ""){
            $e[] = array('id'=>'catTractoresRendimientoTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresEjesTxt'] == ""){
            $e[] = array('id'=>'catTractoresEjesTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresEstatusHdn'] == ""){
            $e[] = array('id'=>'catTractoresEstatusHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresIaveTxt'] == ""){
            $e[] = array('id'=>'catTractoresIaveTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresTipoCmb'] == ""){
            $e[] = array('id'=>'catTractoresTipoCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresKilometrosServicioTxt'] == ""){
            $e[] = array('id'=>'catTractoresKilometrosServicioTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true){
        	$sqlAddTractorStr = "INSERT INTO caTractoresTbl (".
            "tractor, tipoTractor, tarjetaIave, serie, rendimiento, placas, observaciones, modelo, marca, kilometrosservicio, ".
            " estatus, ejes, compania) ".
								"VALUES (".
                                "'".$_REQUEST['catTractoresTractorTxt']."', ".
                                "'".$_REQUEST['catTractoresTipoCmb']."', ".
								"'".$_REQUEST['catTractoresIaveTxt']."', ".
                                "'".$_REQUEST['catTractoresSerieTxt']."', ".
                                "'".$_REQUEST['catTractoresRendimientoTxt']."', ".
                                "'".$_REQUEST['catTractoresPlacaTxt']."', ".
                                "'".$_REQUEST['catTractoresObservacionesTxt']."', ".
                                "'".$_REQUEST['catTractoresModeloTxt']."', ".
								"'".$_REQUEST['catTractoresMarcaHdn']."', ".
                                $_REQUEST['catTractoresKilometrosServicioTxt'].", ".
                                "'".$_REQUEST['catTractoresEstatusHdn']."', ".
                                "'".$_REQUEST['catTractoresEjesTxt']."', ".
                                "'".$_REQUEST['catTractoresCiaHdn']."')";

			$rs = fn_ejecuta_Add($sqlAddTractorStr);


			if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
				$a['sql'] = $sqlAddTractorStr;
                $idTractor =  mysql_insert_id();
                darMantenimiento($idTractor, '0', '0');
                $a['successMessage'] = getTractoresSuccessMsg();
			} else {
            	$a['success'] = false;
            	$a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddTractorStr;

                $errorNoArr = explode(":", $_SESSION['error_sql']);
            	if($errorNoArr[0] == '1062'){
            		$e[] = array('id'=>'duplicate','msg'=>getTractoresDuplicateMsg());
            	}
        	}

		}
		$a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();
        echo json_encode($a);

        $selIdtr="SELECT * FROM caTractoresTbl where tractor='".$_REQUEST['catTractoresTractorTxt']."'";
        $rsId=fn_ejecuta_query($selIdtr);

        $insMant="INSERT INTO camantenimientotractorestbl (idTractor, movimiento, claveMovimiento, fechaInicial, odometro) 
                VALUES ('".$rsId['root'][0]['idTractor']."', 1, 'MP', now(), 1)";
            fn_ejecuta_Add($insMant);

        $instViaje="INSERT INTO trviajestractorestbl (idViajeTractor, idTractor, claveChofer, idPlazaOrigen, idPlazaDestino, centroDistribucion, viaje, fechaEvento, kilometrosTabulados, numeroUnidades, numeroRepartos, claveMovimiento, usuario, ip) VALUES (NULL, '".$rsId['root'][0]['idTractor']."', 0, 268, 268, 'CDTOL', 1, now(), 1.00, 0, 0, 'VX', 1, 1)";
            fn_ejecuta_Add($instViaje);

        $selIdVia="SELECT * FROM trViajesTractoresTbl where idTractor='".$rsId['root'][0]['idTractor']."'";
        $rsIdViaje=fn_ejecuta_query($selIdVia);

        $insDetalle="INSERT INTO camantenimientotractoresdetalletbl (idMantenimientoDetalle, idTractor, movimiento, idViajeTractor, claveMovimiento, fechaEvento, odometro) 
        VALUES (NULL, '".$rsId['root'][0]['idTractor']."', '1', '".$rsIdViaje['root'][0]['idViajeTractor']."', 'MP', now(), '1')
";
      fn_ejecuta_Add($insDetalle);

	}

	function updTractor(){
		$a = array();
        $e = array();
        $a['success'] = true;

		if($_REQUEST['catTractoresCiaHdn'] == ""){
            $e[] = array('id'=>'catTractoresCiaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresTractorTxt'] == ""){
            $e[] = array('id'=>'catTractoresTractorTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresMarcaHdn'] == ""){
            $e[] = array('id'=>'catTractoresMarcaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresModeloTxt'] == ""){
            $e[] = array('id'=>'catTractoresModeloTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresSerieTxt'] == ""){
            $e[] = array('id'=>'catTractoresSerieTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresPlacaTxt'] == ""){
            $e[] = array('id'=>'catTractoresPlacaTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresRendimientoTxt'] == ""){
            $e[] = array('id'=>'catTractoresRendimientoTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresEjesTxt'] == ""){
            $e[] = array('id'=>'catTractoresEjesTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresEstatusHdn'] == ""){
            $e[] = array('id'=>'catTractoresEstatusHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresIaveTxt'] == ""){
            $e[] = array('id'=>'catTractoresIaveTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresKilometrosServicioTxt'] == ""){
            $e[] = array('id'=>'catTractoresKilometrosServicioTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
           $sqlUpdTractoresStr = "UPDATE caTractoresTbl ".
                                 "SET compania= '".$_REQUEST['catTractoresCiaHdn']."', ".
                                 "tractor=".$_REQUEST['catTractoresTractorTxt'].", ".
                                 "marca= '".$_REQUEST['catTractoresMarcaHdn']."', ".
                                 "modelo= '".$_REQUEST['catTractoresModeloTxt']."', ".
                                 "serie= '".$_REQUEST['catTractoresSerieTxt']."', ".
                                 "placas= '".$_REQUEST['catTractoresPlacaTxt']."', ".
                                 "rendimiento=".$_REQUEST['catTractoresRendimientoTxt'].", ".
                                 "ejes=".$_REQUEST['catTractoresEjesTxt'].", ".
                                 "observaciones= '".$_REQUEST['catTractoresObservacionesTxt']."', ".
                                 "estatus= '".$_REQUEST['catTractoresEstatusHdn']."', ".
                                 "tarjetaIave= '".$_REQUEST['catTractoresIaveTxt']."', ".
                                 "kilometrosServicio= ".$_REQUEST['catTractoresKilometrosServicioTxt']." ".
                                 "WHERE idTractor=".$_REQUEST['catTractoresIdTractorHdn'];

            $rs = fn_ejecuta_Upd($sqlUpdTractoresStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlUpdTractoresStr;
                $a['successMessage'] = getTractoresUpdateMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdTractoresStr;

                $errorNoArr = explode(":", $_SESSION['error_sql']);
                if($errorNoArr[0] == '1062'){
                    $e[] = array('id'=>'duplicate','msg'=>getTractoresDuplicateMsg());
                }
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
	}

    function dltTractor(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $sqlDeleteTractorStr = "DELETE FROM caTractoresTbl WHERE idTractor=".$_REQUEST['catTractoresIdTractorHdn'];

        $rs = fn_ejecuta_query($sqlDeleteTractorStr);

        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
            $a['sql'] = $sqlDeleteTractorStr;
            $a['successMessage'] = getTractoresDeleteMsg();
            $a['id'] = $_REQUEST['catTractorIdTractorHdn'];
        } else {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDeleteTractorStr;
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function bloquearTractor(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catTractoresIdTractorHdn'] == ""){
            $e[] = array('id'=>'catTractoresIdTractorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresModuloTxt'] == ""){
            $e[] = array('id'=>'catTractoresModuloTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {

            $sqlBloquearTractorViajeStr = "INSERT INTO trViajesTractoresTmp (idViajeTractor, idTractor, claveChofer, ".
                                            "centroDistribucion, modulo, idUsuario, ip, fechaEvento) ".
                                            "VALUES(".
                                              replaceEmptyNull($_REQUEST['catTractoresIdViajeHdn']).",".
                                              $_REQUEST['catTractoresIdTractorHdn'].",".
                                              replaceEmptyNull($_REQUEST['catTractoresClaveChoferTxt']).",".
                                              replaceEmptyNull("'".$_SESSION['usuCompania']."'").",".
                                              "'".$_REQUEST['catTractoresModuloTxt']."',".
                                              replaceEmptyDec($_SESSION['idUsuario']).",".
                                              "'".$_SERVER['REMOTE_ADDR']."',".
                                              "'".date("Y-m-d H:i:s")."')";

            $rs = fn_ejecuta_Add($sqlBloquearTractorViajeStr);

            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] = getBloquearTractorMsg();
                $a['sql'] = $sqlBloquearTractorViajeStr;
                $a['root'][]['bloqueo'] = true;
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlBloquearTractorViajeStr;
                $a['errorMsg'] = 'Error al Bloquear el Tractor';
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function updBloqueoTractor(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if ($a['success'] == true) {
            $sqlUpdBloqueoStr = "UPDATE trViajesTractoresTmp SET ".
                                "idViajeTractor = ".replaceEmptyNull($_REQUEST['catTractoresIdViajeHdn']).",".
                                "claveChofer = ".replaceEmptyNull($_REQUEST['catTractoresClaveChoferHdn'])." ".
                                "WHERE idTractor =".$_REQUEST['catTractoresIdTractorHdn'];

            $rs = fn_ejecuta_Upd($sqlUpdBloqueoStr);

            if (!isset($_SESSION['error_sql']) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['successMessage'] = getBloquearTractorMsg();
                $a['sql'] = $sqlUpdBloqueoStr;
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdBloqueoStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function liberarTractor($idTractor){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($idTractor !='256'){

            if ($a['success'] == true) {
                $sqlLiberarTractorStr = "DELETE FROM trViajesTractoresTmp ".
                                        "WHERE idTractor= ".$idTractor;

                $rs = fn_ejecuta_query($sqlLiberarTractorStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlLiberarTractorStr;
                    $a['successMessage'] = getTractoresLiberarMsg();
                    $a['root'][]['bloqueo'] = true;
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlLiberarTractorStr;
                }
            }

            $a['errors'] = $e;
            $a['successTitle'] = getMsgTitulo();
            return $a;
        }
         return $a;
    }

    function liberarTractorMasivo(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $tractorArr = explode('|', substr($_REQUEST['catTractoresIdTractorHdn'], 0, -1));
        if(in_array('', $tractorArr)){
            $e[] = array('id'=>'idTractorLista','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        $sqlLiberarTractorStr = "DELETE FROM trViajesTractoresTmp ".
                                       "WHERE idTractor IN ('";

        if ($a['success'] == true) {
            for($i = 0; $i < sizeof($tractorArr); $i++){

                if($i == sizeof($tractorArr) -1){
                    $sqlLiberarTractorStr.= $tractorArr[$i]."')";
                    continue;
                }
                $sqlLiberarTractorStr.= $tractorArr[$i]."', '";
            }

                $rs = fn_ejecuta_query($sqlLiberarTractorStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlLiberarTractorStr;
                $a['successMessage'] = getTractoresLiberarMasivoMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlLiberarTractorStr;
            }
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getTractoresDisponibles(){
        $lsWhereStr = "WHERE idTractor NOT IN  ".
                      "(SELECT idTractor from trViajesTractoresTmp)";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTractoresIdTractorHdn'], "idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTractoresCompaniaHdn'], "compania", 1, 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTractoresEstatusHdn'], "estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetDisponiblesStr = "SELECT * FROM caTractoresTbl ".$lsWhereStr;

        $rs = fn_ejecuta_query($sqlGetDisponiblesStr);

        echo json_encode($rs);
    }

    function getTractoresBloqueados(){
        $lsWhereStr = "WHERE tb.idTractor = tr.idTractor ".
                        "AND tb.idUsuario = us.idUsuario ";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTractorIdTractorHdn'], "tb.idTractor", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catTractorClaveChoferHdn'], "tb.claveChofer", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetBloqueadasStr = "SELECT tb.*, vt.idPlazaDestino, tr.tractor, us.usuario, ".
                                    "ch.apellidoPaterno, ch.apellidoMaterno, ch.nombre, ".
                                    "(SELECT pl.plaza FROM caPlazasTbl pl WHERE vt.idPlazaDestino = pl.idPlaza) AS plaza ".
                                "FROM trViajesTractoresTmp tb ".
                                    "LEFT JOIN trViajesTractoresTbl vt ON tb.idViajeTractor = vt.idViajeTractor  ".
                                    "LEFT JOIN caChoferesTbl ch ON tb.claveChofer = ch.claveChofer, ".
                                    "caTractoresTbl tr, segUsuariosTbl us ".$lsWhereStr. " AND tb.centroDistribucion='".$_SESSION['usuCompania']."'";

        $rs = fn_ejecuta_query($sqlGetBloqueadasStr);

        for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
            $rs['root'][$nInt]['choferCompleto'] = $rs['root'][$nInt]['claveChofer']." - ".$rs['root'][$nInt]['nombre']." ".
                                                    $rs['root'][$nInt]['apellidoPaterno']." ".
                                                    $rs['root'][$nInt]['apellidoMaterno'];
        }

        echo json_encode($rs);
    }

    function getTractorUltimoMovimiento(){

        if($_REQUEST['catTractoresTractorHdn'] == ""){
            $e[] = array('id'=>'catTractoresIdTractorHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catTractoresCompaniaHdn'] == ""){
            $e[] = array('id'=>'catTractoresCompaniaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        else{
            $sqlGetTractorEstatus = "SELECT a.idTractor, a.tractor, b.claveMovimiento,  (SELECT concat(c.claveChofer,' - ',c.nombre,' ',c.apellidoPaterno,' ',c.apellidoMaterno) FROM caChoferestbl c WHERE b.claveChofer = c.claveChofer) as nombreCompleto, b.viaje, ".
                                    "(SELECT plaza FROM caplazastbl d WHERE d.idPlaza = b.idPlazaOrigen) as plazaOrigen, (SELECT plaza FROM caplazastbl d WHERE d.idPlaza = b.idPlazaDestino) as plazaDestino ".
                                    "FROM catractorestbl a, trviajestractorestbl b ".
                                    "WHERE a.idTractor = b.idTractor ".
                                    "AND b.idViajeTractor = (SELECT max(idViajeTractor) ".
                                    "FROM trviajestractorestbl c ".
                                    "WHERE b.idTractor = c.idTractor) ".
                                    "  AND a.idTractor = '".$_REQUEST['catTractoresIdTractorHdn']."' ".
                                    "  AND a.compania ='".$_REQUEST['catTractoresCompaniaHdn']."' ";

            $rs = fn_ejecuta_query($sqlGetTractorEstatus);
            echo json_encode($rs);
        }
    }
?>
