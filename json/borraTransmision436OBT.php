<?php
	$_REQUEST['zonaHoraria'] = date_default_timezone_get();
	require_once("../funciones/generales.php");


	switch($_REQUEST['actionHdn'])
	{
		case 'eliminar':
			eliminar();
			break;
		case 'corregir':
			//corregir();
			eliminar();
			break;		
		case 'buscar':
			buscar();
			break;				
		default:
			echo '';
			break;
	}

    function eliminar(){
        $a = array();
        $a['successTitle'] = "Manejador de Archivos";
        $a['msjResponse'] = "Eliminaci&oacuten de unidades correctas.";
        if($_REQUEST['actionHdn'] == 'corregir')
        {
        		$a['msjResponse'] = "Correcci&oacuten de rutas correctas.";
        }

        if(isset($_FILES)) {
            if($_FILES["archivo"]["error"] > 0){
                $a['success'] = false;
                $a['msjResponse'] = $_FILES["archivo"]["error"];

            } else {
                $temp_file_name = $_FILES['archivo']['tmp_name'];
                $original_file_name = $_FILES['archivo']['name'];

                // Find file extention
                $ext = explode ('.', $original_file_name);
                $ext = $ext [count ($ext) - 1];

                // Remove the extention from the original file name
                $file_name = str_replace ($ext, '', $original_file_name);

                $new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
              
                //$temp_file_name = '../../tmp/demo.xlsx';											//S�lo para probar por fuera
                //$new_name = '../../respaldos/demo.xlsx';											//S�lo para probar por fuera                
                //$a['root'] = leerXLS($new_name);															//S�lo para probar por fuera
                                    					  
               	//comentar para pruebas
                 if (move_uploaded_file($temp_file_name, $new_name)){
                    if (!file_exists($new_name)){
                        $a['success'] = false;
                        $a['msjResponse'] = "Error al procesar el archivo " . $new_name;
                    } else {
                        $a['archivo'] = $file_name . $ext;
                        if($_REQUEST['actionHdn'] == 'corregir')
                        {
                        		$x = corregirXLS($new_name);
                        }
                        else
                        {
                        		$x = leerXLS($new_name);
                        }                        
                        $a['success'] = $x['success'];
												$a['msjResponse'] = $x['msjResponse'];
												$a['records'] = $x['records'];
                    }
                } else {
                    $a['success'] = false;
                    $a['msjResponse'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
                }
            }
        } else {
            $a['success'] = false;
            $a['msjResponse'] = "Error en archivo."; 
        }

        echo json_encode($a);
    }

		function leerXLS($inputFileName) {
        /** Error reporting */
        error_reporting(E_ERROR);		//CHK

        //Establece la zona horaria
				date_default_timezone_set($_REQUEST['zonaHoraria']);
				
				$a = array('success' =>true, 'sql' =>'', 'msjResponse' => "<b>Transmisi&oacuten eliminada.</b>", 'root' => '', 'records' => 0);
				
				$fechaActual = date('Y-m-d');			
				$intervalo = '-10 days';
				$fechaAnt = date("Y-m-d",strtotime("$fechaActual ".$intervalo));
				//var_dump($fechaAnt);

        //  Include PHPExcel_IOFactory
        include '../funciones/Classes/PHPExcel/IOFactory.php';

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader =
             PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error cargando el archivo "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //Se obtiene el valor de cada registro de la columna "A"
        $rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);
        
        //var_dump('DATA',$rowData);

        //-------------Hacemos algunas validaci�nes propias de un archivo v�lido de Excel----------------

        if ($highestRow < 1 || (count($rowData) == 1 && empty($rowData[0][0]))) {
            $a['success'] = false;
            $a['msjResponse'] = '<b>Archivo incorrecto.</br> Ingrese un archivo con datos completos.</b>';
        }
        //------------------------------------------------------------------------------------------------

        if($a['success'])
        {
		        $totalIns = 0;
		        $in = '';
		        for($row = 0; $row<sizeof($rowData); $row++){
				        $vin = $rowData[$row][0];
				        //var_dump($vin);
				        if(!empty($vin) && strlen($vin) == 17)
				        {
		                $in .= "'".$vin."',";
		                $totalIns++;		               
				        }
		        }
		        if($totalIns > 0)
			      {
				        $in = '('.substr($in,0,strlen($in)-1).')';
				        //echo "<br>$in"; 			      		
			      }
        }
        
        if($totalIns > 0 && $in != '' && $a['success'])
        {
            $sql = "DELETE FROM alTransaccionUnidadTbl WHERE tipoTransaccion = '510' AND vin IN ".$in;
						//echo "$sql<br>"; 
						fn_ejecuta_query($sql);            
						if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
								$a['success'] = false;
								$a['sql'] = $sql;
								$a['msjResponse'] = $_SESSION['error_sql']." En el borrado de la transacci&oacuten.";
						}
						if($a['success'])
						{		            
		            $sql = "DELETE FROM al660Tbl WHERE vupdate BETWEEN '".$fechaAnt."' AND '".$fechaActual."' AND vin IN ".$in;
								//echo "<br>$sql<br>";
								fn_ejecuta_query($sql);								
								if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
										$a['success'] = false;
										$a['sql'] = $sql;
										$a['msjResponse'] = $_SESSION['error_sql']." En el borrado del registro 660.";
								}								
						}			
						$a['records'] = $totalIns;
        }
        //var_dump($a);
        
        return $a;
    }

		function corregirXLS($inputFileName) {
        /** Error reporting */
        error_reporting(E_ERROR);		//CHK

        //Establece la zona horaria
				date_default_timezone_set($_REQUEST['zonaHoraria']);
				
				$a = array('success' =>true, 'sql' =>'', 'msjResponse' => "<b>Correcci&oacuten de ruta correcta.</b>", 'root' => '', 'records' => 0);
				
				$fechaActual = date('Y-m-d');			
				$intervalo = '-10 days';
				$fechaAnt = date("Y-m-d",strtotime("$fechaActual ".$intervalo));
				//var_dump($fechaAnt);

        //  Include PHPExcel_IOFactory
        include '../funciones/Classes/PHPExcel/IOFactory.php';

        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader =
             PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error cargando el archivo "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        //Se obtiene el valor de cada registro de la columna "A"
        $rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);
        
        //var_dump('DATA',$rowData);

        //-------------Hacemos algunas validaci�nes propias de un archivo v�lido de Excel----------------

        if ($highestRow < 1 || (count($rowData) == 1 && empty($rowData[0][0]))) {
            $a['success'] = false;
            $a['msjResponse'] = '<b>Archivo incorrecto.</br> Ingrese un archivo con datos completos.</b>';
        }
        //------------------------------------------------------------------------------------------------

        if($a['success'])
        {
		        $totalIns = 0;
		        $in = '';
		        for($row = 0; $row<sizeof($rowData); $row++){
				        $vin = $rowData[$row][0];
				        //var_dump($vin);
				        if(!empty($vin) && strlen($vin) == 17)
				        {
		                $in .= "'".$vin."',";
		                $totalIns++;		               
				        }
		        }
		        if($totalIns > 0)
			      {
				        $in = '('.substr($in,0,strlen($in)-1).')';
				        //echo "<br>$in"; 			      		
			      }
        }
        
        if($totalIns > 0 && $in != '' && $a['success'])
        {
            
		        $sql = "UPDATE alCargaBlueReportTbl SET origendetramo = '".$_REQUEST['origen']."'".
		               " WHERE vin IN ".$in;            
						//echo "$sql<br>"; 
						fn_ejecuta_query($sql);            
						if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
								$a['success'] = false;
								$a['sql'] = $sql;
								$a['msjResponse'] = $_SESSION['error_sql']." En la correcci&oacuten de la ruta.";
						}			
						$a['records'] = $totalIns;
        }
        //var_dump($a);
        
        return $a;
    }

    function buscar(){
        $sql = "SELECT origendetramo FROM alCargaBlueReportTbl".
               " WHERE vin = '".$_REQUEST['vin']."'";
				//echo "<br>$sql<br>";
				$rstOrigen = fn_ejecuta_query($sql);
				
        echo json_encode($rstOrigen);   
    }
    
    function corregir(){
        $a = array();
        $a['success']  = true;
        $a['msjResponse']  = "Ruta corregida satisfactoriamente.";

        $sql = "UPDATE alCargaBlueReportTbl SET origendetramo = '".$_REQUEST['origen']."'".
               " WHERE vin = '".$_REQUEST['vin']."'";
				//echo "<br>$sql<br>";
				fn_ejecuta_query($sql);								
				if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
						$a['success'] = false;
						$a['sql'] = $sql;
						$a['msjResponse'] = $_SESSION['error_sql']." En la actualizaci&oacuten de la ruta.";
				}	 
				
        echo json_encode($a);   
    }    
        
?>