<?php

	session_start();
    ///setlocale(LC_TIME, 'es_MX.utf8');
    $codigoTalonEspecial = 'TE';

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    

	header("Content-type: application/pdf");
	header("Content-Disposition: inline; filename=documento.pdf");

    if(isset($_REQUEST['actionHdn']) && $_REQUEST['actionHdn'] == "buscaFolio"){
        buscaFolio();
    }
    else
    {
        if($_REQUEST['esTractor12Hdn'] != ""){
            imprimirTalon12($_REQUEST['trViajesTractoresIdViajeHdn']);
        } else{
            imprimirTalon();
        }       
    }

    function buscaFolio(){
        $sql = "SELECT tv.*, tr.tractor, vt.viaje, tv.numeroUnidades, tr.rendimiento, vt.numeroRepartos AS distribuidores, vt.kilometrosTabulados, ".
               "concat(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as operador, cast(tv.fechaEvento as date) as fechaEvento,".
               "(SELECT count(tl1.distribuidor) FROM trtalonesviajestbl tl1 WHERE tl1.idViajeTractor = vt.idViajeTractor) as talones, ".
               "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS plazaOrigen, ".
               "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaDestino) AS plazaDestino ".
               "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch ".
               "WHERE tv.idViajeTractor = vt.idViajeTractor ".
               "AND vt.idTractor = tr.idTractor ".
               "AND vt.claveChofer = ch.claveChofer ".
               "AND tr.compania = '".$_REQUEST['compania']."' ".
               "AND tv.folio = ".$_REQUEST['folio']." ".
               "AND tv.centroDistribucion in ('CDLZC','CDMTY','CDSAL','CDSFE','CDTOL','CDVER','CDAGS','CDSLP','CMDAT','CDSLO','CDLAR','CDKAN')".
               "AND tv.tipotalon = '".$_REQUEST['tipoTalon']."'";
         //echo "$sql<br>";
         $rsTalon = fn_ejecuta_query($sql);
         
         echo json_encode($rsTalon);
    }


    function imprimirTalon(){
        $a = array();
        $e = array();
        $a['success'] = true;
        $a['msjResponse'] = 'Reimpresi&oacuten generada correctamente.';
        global $codigoTalonEspecial;

        if($_REQUEST['trViajesTractoresIdViajeHdn'] != ""){

            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND vt.idViajeTractor = ".$_REQUEST['trViajesTractoresIdViajeHdn']." ".
                                     "AND tv.claveMovimiento != 'TX' ".
                                     "AND tv.centroDistribucion in ('CDLZC','CDMTY','CDSAL','CDSFE','CDTOL','CDVER','CDAGS','CDSLP','CMDAT','CDSLO','CDLAR','CDKAN')".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].")";
        } else {
            if ($_REQUEST['trViajesTractoresFoliosHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresFoliosHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if ($_REQUEST['trViajesTractoresCompaniaHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresCompaniaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            $tipoTalon = '';
            if(isset($_REQUEST['tipoTalon']))
            {
                $tipoTalon = " AND tv.tipotalon = '".$_REQUEST['tipoTalon']."'";
            }

            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].") ".
                                     "AND tv.centroDistribucion in ('CDLZC','CDMTY','CDSAL','CDSFE','CDTOL','CDVER','CDAGS','CDSLP','CMDAT','CDSLO','CDLAR','CDKAN')".
                                     $tipoTalon;

        }

        if ($a['success']) {

         // $pdf = new FPDF('L', 'mm','A4');


          //echo "$sqlGetTalonesViajeStr<br>";
          $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);
          //echo json_encode($sqlGetTalonesViajeStr);
          $varInc = 0;

          //echo json_encode($rsTalon);

          for ($nInt=1 ; $nInt <= sizeof($rsTalon['root']) ; $nInt++) {


            $sqlGetTalonStr = "SELECT tv.*, dc.descripcionCentro, tr.tractor, ".//co.descripcion AS nombreCiaRemitente, co.rfc AS rfcRemitente, co.direccion AS dirCompania, 
                              "vt.claveChofer, ch.*, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist,".
                              "(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$varInc]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                              "(SELECT pl.plaza FROM caPlazasTbl pl ".
                              "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                              "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                              "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                              "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                              "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                              "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".
                              "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caChoferesTbl ch, ".
                              "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                              "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".// caCompaniasTbl co,
                              "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                              "AND dc.distribuidorCentro = tv.distribuidor ".
                              // "AND co.compania = tv.companiaRemitente ".
                              "AND tr.idTractor = vt.idTractor ".
                              "AND ch.claveChofer = vt.claveChofer ".
                              "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                              "WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                              "AND col.idColonia = di.idColonia ".
                              "AND mu.idMunicipio = col.idMunicipio ".
                              "AND es.idEstado = mu.idEstado ".
                              "AND pa.idPais = es.idPais ".
                              "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                              "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                               "AND tv.centroDistribucion in ('CDLZC','CDMTY','CDSAL','CDSFE','CDTOL','CDVER','CDAGS','CDSLP','CMDAT','CDSLO','CDLAR','CDKAN')".
                              "AND tv.idTalon = ".$rsTalon['root'][$varInc]['idTalon']." ".
                              "AND tv.claveMovimiento != 'TX' ";

            $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

            //echo json_encode($sqlGetTalonStr);

            if(sizeof($dataTalon['root']) > 0){
              $selStr = "SELECT * FROM caCompaniasTbl WHERE compania = '".$dataTalon['root'][0]['companiaRemitente']."'";
              $ciaRst = fn_ejecuta_query($selStr);
              if ($ciaRst['records'] > 0) {
                $dataTalon['root'][0]['nombreCiaRemitente'] = $ciaRst['root'][0]['descripcion'];
                $dataTalon['root'][0]['rfcRemitente'] = $ciaRst['root'][0]['rfc'];
                $dataTalon['root'][0]['dirCompania'] = $ciaRst['root'][0]['direccion'];
              } else {
                $selStr = "SELECT * FROM caDistribuidoresCentrosTbl WHERE distribuidorCentro = '".$dataTalon['root'][0]['companiaRemitente']."'";
                $ciaRst = fn_ejecuta_query($selStr);
                $dataTalon['root'][0]['nombreCiaRemitente'] = $ciaRst['root'][0]['descripcionCentro'];
                $dataTalon['root'][0]['rfcRemitente'] = $ciaRst['root'][0]['rfc'];
                $dataTalon['root'][0]['dirCompania'] = $ciaRst['root'][0]['direccionEntrega'];
              }

              if($dataTalon['root'][0]['dirCompania'] != ''){

                $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                              "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                              "caEstadosTbl es, caPaisesTbl pa ".
                                              "WHERE pa.idPais = es.idPais ".
                                              "AND es.idEstado = mu.idEstado ".
                                              "AND mu.idMunicipio = co.idMunicipio ".
                                              "AND co.idColonia = dir.idColonia ".
                                              "AND dir.direccion = '".$dataTalon['root'][0]['dirCompania']."'";

                $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio']." ".$dirCompania['root'][0]['cp'];
                $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
              }

              $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ".
                                        "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                        "WHERE un.vin = ut.vin ".
                                        "AND su.simboloUnidad = un.simboloUnidad ".
                                        "AND ut.idTalon =".$rsTalon['root'][$varInc]['idTalon']." ".
                                        "AND ut.estatus != 'C' ";

              $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);


              if(sizeof($unidadesTalon['root']) > 0){

                $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                $dataTalon['root'][0]['apellidoPaterno']." ".
                $dataTalon['root'][0]['apellidoMaterno'];
                $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];

              if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                $sqlGetServEspStr = "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                    "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                    "WHERE dir.direccion = de.direccionDestino ".
                                    "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                    "AND dir.idColonia = co.idColonia ".
                                    "AND co.idMunicipio = mu.idMunicipio ".
                                    "AND mu.idEstado = es.idEstado ".
                                    "ANd es.idPais = pa.idPais ".
                                    "AND pl.idPlaza = dc.idPlaza ".
                                    "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                    "AND de.idDestinoEspecial =(SELECT MAX(idDestinoEspecial) FROM aldestinosespecialestbl where vin ='".$unidadesTalon['root'][0]['vin']."') ";

                $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);

                for ($nInt01=0; $nInt01 < sizeof($dataServEsp['root']); $nInt01++) {

                  $dataServEsp['root'][$nInt01]['descDist'] = $dataServEsp['root'][$nInt01]['distribuidor']." - ".
                  $dataServEsp['root'][$nInt01]['descripcionCentro'];

                  $dataServEsp['root'][$nInt01]['dirL1'] = $dataServEsp['root'][$nInt01]['calleNumero'];
                  $dataServEsp['root'][$nInt01]['dirL2'] = $dataServEsp['root'][$nInt01]['colonia'].", ".$dataServEsp['root'][$nInt01]['municipio']." ".$dataServEsp['root'][$nInt01]['cp'];
                  $dataServEsp['root'][$nInt01]['dirL3'] = $dataServEsp['root'][$nInt01]['estado'].", ".$dataServEsp['root'][$nInt01]['pais'];
                }
              }
                generaArchivo($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);
                


            } else {
              $pdf->AddPage();
              $pdf->SetY(92+(4*$nInt)+0);
              $pdf->SetX(25+0);
              $pdf->Cell(17,3, 'TALON #'.$rsTalon['root'][$varInc]['idTalon']. ' SIN UNIDADES',0, 'C');
            }
          }
            $varInc ++ ;
        }
      }
        //Output
        //$pdf->Output('../../talon.pdf', I);
    }
    function imprimirTalon12($idViaje){
        $a = array();
        $e = array();
        $a['success'] = true;

        //SOLO PARA EL CASO DEL FONDO CON EL TALON ESCANEADO
        $fondoPath = "../img/impTalon.jpg";

        if ($idViaje == "") {
            $e[] = array('id'=>'idViaje','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success']) {
           
            $sqlGetTalonesViajeStr = "SELECT idTalon FROM trTalonesViajesTbl WHERE idViajeTractor = ".$idViaje." AND claveMovimiento='TF' AND folio='".$_REQUEST['trViajesTractoresFoliosHdn']."'" ;

            $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);

            for ($nInt=0; $nInt < sizeof($rsTalon['root']); $nInt++) {



               $sqlGetTalonStr = "SELECT tv.*, co.descripcion AS nombreCiaRemitente, co.rfc AS rfcRemitente, co.direccion AS dirCompania, dc.descripcionCentro, tr.tractor, ".
                                  "vt.claveChofer, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist, ".
                                  "(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                                  "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                        "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                                  "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                        "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                                  "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                                        "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                                  "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".
                                  "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caCompaniasTbl co, ".
                                    "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                                    "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
                                  "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                  "AND dc.distribuidorCentro = tv.distribuidor ".
                                  "AND co.compania = tr.compania  ".
                                  "AND tr.idTractor = vt.idTractor ".
                                  "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                                  "WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                                  "AND col.idColonia = di.idColonia ".
                                  "AND mu.idMunicipio = col.idMunicipio ".
                                  "AND es.idEstado = mu.idEstado ".
                                  "AND pa.idPais = es.idPais ".
                                  "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                                  "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                                  "AND idTalon = ".$rsTalon['root'][$nInt]['idTalon'];

                $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

                if(sizeof($dataTalon['root']) > 0){
                    if($dataTalon['root'][0]['dirCompania'] != ''){
                        $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                        "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                                        "caEstadosTbl es, caPaisesTbl pa ".
                                                        "WHERE pa.idPais = es.idPais ".
                                                        "AND es.idEstado = mu.idEstado ".
                                                        "AND mu.idMunicipio = co.idMunicipio ".
                                                        "AND co.idColonia = dir.idColonia ".
                                                        "AND dir.direccion = ".$dataTalon['root'][0]['dirCompania'];

                        $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                        $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                        $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio']." ".$dirCompania['root'][0]['cp'];
                        $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
                    }

                    $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ,(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal ".
                                                "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                                "WHERE un.vin = ut.vin ".
                                                "AND su.simboloUnidad = un.simboloUnidad ".
                                                "AND ut.idTalon =".$rsTalon['root'][$nInt]['idTalon']." ".
                                                "AND ut.estatus != 'C' ";

                    $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);

                    echo $rSumaPeso[0]['totalPeso'];

                    if(sizeof($unidadesTalon['root']) > 0){
                        $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                        $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                        $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                                                                $dataTalon['root'][0]['apellidoPaterno']." ".
                                                                $dataTalon['root'][0]['apellidoMaterno'];
                        $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                        $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                        $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];

                        if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                            $sqlGetServEspStr = "SELECT de.distribuidorOrigen AS distribuidor, dc.descripcionCentro, de.direccionOrigen AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionOrigen ".
                                                "AND dc.distribuidorCentro = de.distribuidorOrigen ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                                "UNION ".
                                                "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionDestino ".
                                                "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."'";

                            $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);


                            for ($nInt=0; $nInt < sizeof($dataServEsp['root']); $nInt++) {
                                $dataServEsp['root'][$nInt]['descDist'] = $dataServEsp['root'][$nInt]['distribuidor']." - ".
                                                                            $dataServEsp['root'][$nInt]['descripcionCentro'];

                                $dataServEsp['root'][$nInt]['dirL1'] = $dataServEsp['root'][$nInt]['calleNumero'];
                                $dataServEsp['root'][$nInt]['dirL2'] = $dataServEsp['root'][$nInt]['colonia'].", ".$dataServEsp['root'][$nInt]['municipio']." ".$dataServEsp['root'][$nInt]['cp'];
                                $dataServEsp['root'][$nInt]['dirL3'] = $dataServEsp['root'][$nInt]['estado'].", ".$dataServEsp['root'][$nInt]['pais'];
                            }
                        }
                        //print_r($dirCompania['root'][0]);
                        $pdf = generaArchivo($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);
                    } else {
                        $a['errorMessage'] = "ESTE TALON NO TIENE UNIDADES";
                        return $a;
                    }
                } else {
                    $a['errorMessage'] = "ERROR AL INTENTAR CARGAR EL TALON ".$rsTalon['root'][$nInt]['idTalon'];
                    return $a;
                }
            }

            echo json_encode($a);
        }
    }
	
	function generaArchivo($pdf, $fondo, $data, $dirCompania, $unidades, $especiales){		
	
    $fileDir = "C:/CFDI_Traslado/CONECTOR/".substr($data['centroDistribucion'],2,3).$data['folio'].$data['companiaTractor'].".txt";
    $flReporte660 = fopen($fileDir, "a") or die("No se pudo generar ,interfaz");

	 	//A) ENCABEZADO
	 											 
		fwrite($flReporte660,'tipoDeComprobante=6'.PHP_EOL);												 
		fwrite($flReporte660,'lugarExpedicion=11520'.PHP_EOL);

		if ($data['companiaTractor']=='CRYMEX') {
			$regimen='601';
		}
		if ($data['companiaTractor']=='TRANSDRIZA') {
			$regimen='624';
		}

		fwrite($flReporte660,"regimen=".$regimen.PHP_EOL);

		if($data['tipoTalon'] == $codigoTalonEspecial){
            $plaza=$especiales[0]['plaza'];
        } else {
            $plaza=$data['descPlazaDestino'];
        }

        if($data['tipoTalon'] == $codigoTalonEspecial){
            $RFC=$especiales[0]['rfc'];
        } else {
            $RFC=$data['rfcDestinatario'];

        }if($RFC ==null){
            $RFC='XAXX010101000';
        } 

    if ($data['tipoTalon']=='TE') {
        $desc='PRIMER MOVIMIENTO SE  ';
    }
    else{
     $desc='TALON NORMAL  '; 
    }

     

		fwrite($flReporte660,"receptor=".$RFC."|".$data['descDistribuidor'].$data['dirL1Dest']." ".$plaza."|||||||||||correo@tracomex.com.mx|RESIDENCIA_FISCAL|NumRegIdTrib".PHP_EOL);
		fwrite($flReporte660,'usoCFDI=G03'.PHP_EOL);
		fwrite($flReporte660,'serie='.substr($data['centroDistribucion'],2,3).PHP_EOL);
		fwrite($flReporte660,'folio='.$data['folio'].PHP_EOL);
		fwrite($flReporte660,'formaDePago=02'.PHP_EOL);
		fwrite($flReporte660,'condicionesDePago='.PHP_EOL);
		fwrite($flReporte660,'metodoDePago=PPD'.PHP_EOL);
		fwrite($flReporte660,'confirmacion='.PHP_EOL);
		fwrite($flReporte660,'CFDIRelacionados='.PHP_EOL);

		fwrite($flReporte660,'concepto={01010101|ART-001|1|E48|TRANSPORTE|');


		for ($nInt=0; $nInt < sizeof($unidades); $nInt++) {

			fwrite($flReporte660,$unidades[$nInt]['vin']."   ".$unidades[$nInt]['simboloUnidad']."   ".$unidades[$nInt]['descSimbolo']."<enter>");
                        
        }

		fwrite($flReporte660,'|0.00|0.00|0.00|0.00|||||Traslados[0|002|Tasa|16.00000|0.00|||||]Retenciones[|||||||||]extradata[|||||||||]}'.PHP_EOL);

		fwrite($flReporte660,'subtotal=0.00'.PHP_EOL);
		fwrite($flReporte660,'descuento='.PHP_EOL);
		fwrite($flReporte660,'moneda=MXN|1.0'.PHP_EOL);
		fwrite($flReporte660,'totalImpuestosRetenidos='.PHP_EOL);
		fwrite($flReporte660,'totalImpuestosTrasladados=0.00'.PHP_EOL);
		fwrite($flReporte660,'total=0.00'.PHP_EOL);

		$fecha = $data['fechaEvento'];
        $fechaEntrega = strtotime ( "+".$data['diasEntrega']." day" , strtotime ($fecha ));
        $fechaEntrega = date ( 'd/m/Y' , $fechaEntrega );


		 if ($unidades[$nInt]['pesoAprox']== '0') {
              $peso = '0999';
              $peso1[] = '0999';
            }else{
              $peso = $unidades[$nInt]['pesoAprox'];
              $peso1[] = $unidades[$nInt]['pesoAprox'];
            }

            $pesoTotal= array_sum($peso1);


		fwrite($flReporte660,'CA=ORIGEN DE LA CARGA: '.$data['descPlazaOrigen']."  ".$data['nombreCiaRemitente']." ".$data['rfcRemitente']."  ".$dirCompania['dirL1Cia'].$dirCompania['dirL2Cia']."  ".$dirCompania['dirL3Cia']." |DESTINO DE LA CARGA: ".$data['descDistribuidor']." ".$data['rfcDestinatario']." ".$data['dirL1Dest']."  |EL MISMO|EL MISMO|".$fechaEntrega."||".$pesoTotal."|NO|$0,000.00|N/A|".$desc."*HORA:\t".date("H:i:s")."|TRACTOR ".$data['tractor']."|".$data['claveChofer']." - ".$data['nombreChofer']."|SI||||   ".PHP_EOL);

		if ($data['companiaTractor']=='CRYMEX') {
			$companiaRFC='ATC900122NU0';
		}
		if ($data['companiaTractor']=='TRANSDRIZA') {
			$companiaRFC='TRA891031SXA';
		}
		//echo $data['companiaTractor'];

		fwrite($flReporte660,'emisor='.$companiaRFC.PHP_EOL); 


// COMPLEMENTO DE CARTA PORTE. INICIA CON ESTA LEYENDA Y EL ORDEN DE LOS RENGLONES ES MUY IMPORTANTE RESPETARLO
// esto es un comentario
    //fwrite($flReporte660,'COMPLEMENTO_CARTA_PORTE'.PHP_EOL);
//Header=Version|TranspInternac|EntradaSalidaMerc|ViaEntradaSalida|TotalDistRec
   // fwrite($flReporte660,'Header=1.0|No|||'.PHP_EOL); ///300 kilometros ------------------------------------

// INICIO DE NODO UBICACIONES. CADA NODO UBICACIÓN SE COMPONE DE 4 RENGLONES. PUEDEN SER 1 O MAS NODOS UBICACIÓN. EN PRINCIPIO AL MENOS 2, ORIGEN Y DESTINO
// SE REPORTA VACÍO SI NO APLICA EL NODO
//Ubicacion=TipoEstacion|DistanciaRecorrida
//Origen=idOrigen|RFCRemitente|NombreRemitente|NumRegIdTrib|ResidenciaFiscal|NumEstacion|NombreEstacion|NavegacionTrafico|FechaHoraSalida
//Destino=IDDestino|RFCDestinatario|NombreDestinatario|NumRegIdTrib|ResidenciaFiscal|NumEstacion|NombreEstacion|NavegacionTrafico|FechaHoraProgLlegada
//Domicilio=calle|noExt|noInt|colonia|localidad|referencia|municipio|estado|pais|CP
   // fwrite($flReporte660,'Ubicacion=01|400'.PHP_EOL);



		/////impresion PDF


		// dormir durante 7 segundos
		sleep(15);


		setlocale(LC_TIME, "spanish");
		$dia=strftime("%d");
		$mes1=strftime("%B");
		$anio=strftime("%Y");

		$mes=substr($mes1, 0,3);

		

		//readfile("C:\MultiConectorCP\FACTURAS\abr2021\LZC\PDF\LZC1029777_XAXX010101000_06abr2021_134636.pdf");

		readfile("C:/CFDI_Traslado/FACTURAS/".$mes.$anio."/".substr($data['centroDistribucion'],2,3)."/PDF/".substr($data['centroDistribucion'],2,3).$data['folio']."_".$RFC."_".$dia.$mes.$anio.".pdf");

		//echo "C:/CFDI_Traslado/FACTURAS/".$mes.$anio."/".substr($data['centroDistribucion'],2,3)."/PDF/".substr($data['centroDistribucion'],2,3).$data['folio']."_".$RFC."_".$dia.$mes.$anio.".pdf";
		
	}
?>