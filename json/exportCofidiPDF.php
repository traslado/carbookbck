<?php
	// TRACOMEX19				[0000000019]
	// AUTOTRANSPORTESCHRYMEX20	[0000000020]
	// COMERCIALCRYMEX21		[0000000021]
	// TRANSDRIZA22				[0000000022]
	// 	YYYYMM
	// 		DD
	// 			01

	// ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
    ini_set('max_execution_time', 30000);
    /*session_start();
    require("../functions/generales.php");
    require("../functions/construct.php");*/

	//recibeArchivoFTP();
	function recibeArchivoFTP($IPServidorFTP='172.106.0.126', $usuarioFTP='COFIDI', $passwordFTP='Tracomex1', $directorioFTP='') {
		$a = array('msjResponse' => 'El archivo se ha descargado correctamente.', 'error' => 0, 'nombreArchivo' => '');
		$nombreArchivo = '';
		$nombreArchivoCompleto = '';
		$lb_successtrue = 0;
		$lb_successfalse = 0;
		$ls_msjError = "<br>Ha ocurrido un error al descargar el archivo.";

		$ga_cveciacfd = array('0000000019','0000000020','0000000021','0000000022');
		date_default_timezone_set('America/Mexico_City');
	    setlocale(LC_TIME , 'es_MX.UTF-8');
	    $gd_fechaProceso = strftime("%Y-%m-%d %H:%M:%S");
	    $gd_yyyymmdd = strftime("%Y%m%d");
	    $gd_yyyymm = strftime("%Y%m");
	    $gd_dd = strftime("%d");
	    $gs_01 = '01';

		// conexión
		$conexion = ftp_connect($IPServidorFTP);
		// acceso
		$login = ftp_login($conexion, $usuarioFTP, $passwordFTP);
		if ((!$conexion) || (!$login)) { 
	       	$a['msjResponse'] = "Conexion fallida al sitio FTP!";
	       	$a['error'] = 1;
		} else {
			# Conectado a $IPServidorFTP, por el usuario $usuarioFTP
			ftp_pasv($login, true); //Modo pasivo la conexion es iniciada por el usuario

			$ls_rootdir = $_SERVER['DOCUMENT_ROOT'];
			$reccia = sizeof($ga_cveciacfd);
			for ($icia=0; $icia < $reccia; $icia++) { 
				# Si no existe el directorio se crea por cia en local
				// $ls_cfdicia = "${ls_rootdir}cfdi/procesados/".$ga_cveciacfd[$icia];// Win
				$ls_cfdicia = "../cfdi/CFDI_PDF/";// Lin
				// echo $ls_cfdicia;
				if (!file_exists($ls_cfdicia)) {  
	                mkdir($ls_cfdicia, 0777,true);
	                # 0777 (Rwxrwxrwx) No hay restricciones en los permisos.
	            }
	            // Archivos del directorio a la cual el usuario tiene permisos
				$directorioFTP = "ExportCFD/".$ga_cveciacfd[$icia]."/${gd_yyyymm}/${gd_dd}/${gs_01}";
				// var_dump($directorioFTP);
				$files = ftp_nlist($conexion, $directorioFTP);
				foreach ($files as $nombreArchivoCompleto) {
					$explode_name = explode('.',$nombreArchivoCompleto);
					// echo "<br>$nombreArchivoCompleto";
					$tipoArchivo = end($explode_name);
					if (strtolower($tipoArchivo) == "pdf") {
						$explode_dir = explode('/',$nombreArchivoCompleto);
						$nombreArchivo = end($explode_dir);
						// $a['nombreArchivo'] .= $nombreArchivo."\n";
						# Si no existe el directorio se crea
						if (!file_exists($ls_cfdicia."/${gd_yyyymmdd}")) {  
			                mkdir($ls_cfdicia."/${gd_yyyymmdd}", 0777, true);
			            }

						$ls_existearchivo = $ls_cfdicia."/${gd_yyyymmdd}/$nombreArchivo";
						if (!file_exists($ls_existearchivo)) { 
							# Descarga el archivo si no existe
							# Cambiar get por lftp >> Revisar
							if (ftp_get($conexion, $ls_existearchivo, $nombreArchivoCompleto, FTP_BINARY)) {
								$lb_successtrue++;
							} else {
								$ls_msjError .= "<br>$nombreArchivoCompleto";
								$lb_successfalse++;
							}
						}
					}
				}
			}
			if ($lb_successtrue > 0 && $lb_successfalse > 0) {
				$a['msjResponse'] .= $ls_msjError;
			} else {
				if ($lb_successtrue == 0 && $lb_successfalse > 0) {
					$a['msjResponse'] = $ls_msjError;
					$a['error'] = 1;
				} else {
					if ($lb_successtrue == 0 && $lb_successfalse == 0) {
						$a['msjResponse'] = "No existe archivos para descargar.";
					}
				}
			}
			// ob_start();
			// ob_get_clean();
		}
		ftp_close($conexion);

		echo json_encode($a);
		die();
	}
?>