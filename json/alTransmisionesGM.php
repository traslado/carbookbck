<?php
	require_once("../funciones/generales.php");
	require_once("../funciones/construct.php");
	require_once("../funciones/utilidades.php");

	date_default_timezone_set('America/Mexico_City');

	switch($_REQUEST['alTransmisionesGMHdn']){
		case 'validaUnidades':
			validaUnidades();
		break;
	    default:
	        echo '';
	}

	function validaUnidades(){
		if (count($_REQUEST['interfacesGMUnidadesTxt']) != 0) {			

			$unidadesUpd= $_REQUEST['interfacesGMUnidadesTxt'];
			$unidades=trim($unidadesUpd);
			$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
			$reemplazar = array("", "", "", "");
			$vines = str_ireplace($buscar,$reemplazar,$unidades);

			$cadena = chunk_split($vines, 17,"','");

			$vinesUpd = strtoupper(substr($cadena,0,-2));

			//echo json_encode($vinesUpd);			    
		}

		switch ($_REQUEST['interfacesGMTipoTransmision']) {
			case 'Liberar':
				liberarS3($vinesUpd);
				break;
			case 'HOLD':
				HOLDS2($vinesUpd);
				break;	
			case 'Corte':
				CorteS5($vinesUpd);
				break;	
			default:
				# code...
				break;
		}
	}

	function liberarS3($vinesUpd){
		//echo "inicio GM S3";

		unlink("C:/servidores/DFYP301X");


		 $sqlUnidadesGM ="SELECT  case when substr(cveDisFac,-5) in('77831','77828','77827','77848','77829','77830','72148','31112') then 'G' else 'M' end as shipper, vin, 'S3' as eventCode, substring(now(),1,10) as  vehicleDate, 'LC' as portCode, ".
	   					"currency,'' as volume, substring(	cveDisFac,3) as ship, substring(now(),1,10) as dateVehicle, 'TRAM' as sender, concat('0000',(SELECT folio +1 FROM trfoliostbl WHERE centroDistribucion='LZC02' AND compania ='GM' AND tipoDocumento ='GM')) as sequence, ".
	   					 " '' as orderNumber,'  ' as dealer,'' as portDischarge, ".
							"concat(substring(now(),12,2),substring(now(),15,2),substring(now(),18,2)) as eventTime  ".
							"FROM alinstruccionesmercedestbl ".
								"where vin in ('".$vinesUpd.");";

			$rstSqlUnidadesGM= fn_ejecuta_query($sqlUnidadesGM);

			//echo json_encode($sqlUnidadesGM);

		if(sizeof($rstSqlUnidadesGM['root']) != NULL){

			for ($i=0; $i <sizeof($rstSqlUnidadesGM['root']) ; $i++) { 
				//echo json_encode($rstSqlUnidadesGM['root'][$i]['cveStatus']);
				/*if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S2') {				
					$arrS2[] = $rstSqlUnidadesGM['root'][$i];
				}
			/////////////////////////////////////////////////////////
				else */
				if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S3') {
					$arrS3[] = $rstSqlUnidadesGM['root'][$i];
				}
				else{
					//echo "string";
				}
			}/////////////////////////////////////////
		}

		/*if ($arrS2 != null) {
			generaS2($arrS2);			
		}*/

		//echo count($arrS3);
		if ($arrS3 != null) {
			generaS3($arrS3,$rstSqlUnidadesGM);			
		}
	}


	function generaS3($arrS3,$rstSqlUnidadesGM){	


		$fecha = date("YmdHis");
	    $fileDir = "C:/servidores/DFYP301X";



	    $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
	    $recordsPositionValue = array();

	            
	    //A) ENCABEZADO
	    
		fwrite($fileRec,"GESGEIS3.000  TRAM             DFYDETROIT$".$fecha.sprintf('%09s',$arrS3[0]['sequence'])."EST".PHP_EOL);
	        
	    //B) DETALLE UNIDADES
	   	for ($i=0; $i <sizeof($arrS3) ; $i++) {


				

			fwrite($fileRec,sprintf('%-1s',$arrS3[$i]['shipper']).sprintf('%-17s',$arrS3[$i]['vin']).sprintf('%-6s',$arrS3[$i]['orderNumber']).sprintf('%-2s',$arrS3[$i]['eventCode']).sprintf('%-10s',$arrS3[$i]['vehicleDate']).
							sprintf('%-9s',$arrS3[$i]['dealer']).sprintf('%-3s',$arrS3[$i]['portCode']).sprintf('%-5s',$arrS3[$i]['portDischarge']).sprintf('%-5s',$arrS3[$i]['currency']).sprintf('%-7s',$arrS3[$i]['volume']).
							sprintf('%-5s',$arrS3[$i]['ship']).sprintf('%-20s',$arrS3[$i]['engineNumber']).sprintf('%-10s',$arrS3[$i]['dateVehicle']).sprintf('%-59s',$arrS3[$i]['futureUse']).
							"EST".sprintf('%-6s',$arrS3[$i]['eventTime']).sprintf('%-32s',$arrS3[$i]['reservedForFutureUse']).PHP_EOL);

			$vin = $arrS3[$i]['vin'];
			$today = date("Y-m-d H:i:s");
			$fecha = substr($today,0,10);
			$hora=substr($today,11,8);
			$fecha1 = $arrS3[$i]['vehicleDate']." ".$hora;


			$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".			
								"VALUES ('IS3','LZC02','".$arrS3[$i]['sequence']."','".
								$arrS3[$i]['vin'].
								"','".$fecha1.
								"','S3','".
								$today."','".
								$varEstatus."','".
								$fecha."','".
								$hora."') ";    

			fn_ejecuta_query($sqlAddTransaccion);

			$sqlUpd ="UPDATE alinstruccionesmercedestbl set cveStatus='3G' WHERE vin ='".$arrS3[$i]['vin']."' ";
			fn_ejecuta_query($sqlUpd);

	    }
		//C) TRAILER
		$long=(sizeof($arrS3));
		fwrite($fileRec,"GEE".sprintf('%06d',($long)).sprintf('%09s',$arrS3[0]['sequence']));
	   	fclose($fileRec);
	   	//ftpArchivo ($nombreBusqueda);

	   	$fechaCopia = date("Y-m-d His");


		$updateFolioGM = "UPDATE trfoliostbl set folio=".$arrS3[0]['sequence']." where centroDistribucion='LZC02' and compania='GM' and tipoDocumento='GM'";
		fn_ejecuta_query($updateFolioGM);

		
		copy( "C:/servidores/DFYP301X", "E:/carbook/interfacesGM/respaldoS3/DFYP301X".$fechaCopia);

		exec("C:/Paso/ejecutaFtp.bat");		
		//echo json_encode("UNIDADES LIBERADAS CORRECTAMENTE");
		echo json_encode($rstSqlUnidadesGM);
	}

	function HOLDS2($vinesUpd){
		//echo "inicio GM S2";

	    unlink("C:/servidores/DFYP301X");


			 $sqlUnidadesGM= "SELECT  case when substr(cveDisFac,-5) in('77831','77828','77827','77848','77829','77830','72148','31112') then 'G' else 'M' end as shipper, vin, 'S2' as eventCode, substring(now(),1,10) as  vehicleDate, 'LC' as portCode, currency,'' as volume, ".
			     					"substring(	cveDisFac,3) as ship, substring(now(),1,10) as dateVehicle, ".
									"'TRAM' as sender, concat('0000',(SELECT folio +1 FROM trfoliostbl WHERE centroDistribucion='LZC02' AND compania ='GM' AND tipoDocumento ='GM')) as sequence,  '' as orderNumber,'  ' as dealer,'' as portDischarge, ".
									"concat(substring(now(),12,2),substring(now(),15,2),substring(now(),18,2)) as eventTime ".
									"FROM alinstruccionesmercedestbl ".
									"where vin  in ('".$vinesUpd.");";	
			$rstSqlUnidadesGM= fn_ejecuta_query($sqlUnidadesGM);



		if(sizeof($rstSqlUnidadesGM['root']) != NULL){

			for ($i=0; $i <sizeof($rstSqlUnidadesGM['root']) ; $i++) { 				
				if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S2') {				
					$arrS2[] = $rstSqlUnidadesGM['root'][$i];
				}
			}
		}

		if ($arrS2 != null) {
			generaS2($arrS2,$rstSqlUnidadesGM);			
		}	
	}

	function generaS2($arrS2,$rstSqlUnidadesGM){

		$fecha = date("YmdHis");
	    $fileDir = "C:/servidores/DFYP301X";


	    $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
	    $recordsPositionValue = array();
	    
	            
	    //A) ENCABEZADO	    
		fwrite($fileRec,"GESGEIS2.000  TRAM             DFYDETROIT$".$fecha.sprintf('%09s',$arrS2[0]['sequence'])."EST".PHP_EOL);
	        
	    //B) DETALLE UNIDADES
	   	for ($i=0; $i <sizeof($arrS2) ; $i++) {
				

			fwrite($fileRec,sprintf('%-1s',$arrS2[$i]['shipper']).sprintf('%-17s',$arrS2[$i]['vin']).sprintf('%-6s',$arrS2[$i]['orderNumber']).sprintf('%-2s',$arrS2[$i]['eventCode']).sprintf('%-10s',$arrS2[$i]['vehicleDate']).
							sprintf('%-9s',$arrS2[$i]['dealer']).sprintf('%-3s',$arrS2[$i]['portCode']).sprintf('%-5s',$arrS2[$i]['portDischarge']).sprintf('%-5s',$arrS2[$i]['currency']).sprintf('%-7s',$arrS2[$i]['volume']).
							sprintf('%-25s',$arrS2[$i]['reserved']).sprintf('%-10s',$arrS2[$i]['dateVehicle']).sprintf('%-30s',$arrS2[$i]['storageLocation']).sprintf('%-15s',$arrS2[$i]['storageNumber']).sprintf('%-14s',$arrS2[$i]['futureUse']).
							sprintf('%-3s',$arrS2[$i]['timerQ']).sprintf('%-6s',$arrS2[$i]['eventTime']).sprintf('%-32s',$arrS2[$i]['reservedForFutureUse']).sprintf('%-100s',$arrS2[$i]['holdAdditional']).PHP_EOL);

			$vin = $arrS2[$i]['vin'];
			$today = date("Y-m-d H:i:s");
			$fecha = substr($today,0,10);
			$hora=substr($today,11,8);
			$fecha1 = $arrS2[$i]['vehicleDate']." ".$hora;


			$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".			
								"VALUES ('IS2','LZC02','".$arrS2[$i]['sequence']."','".
								$arrS2[$i]['vin'].
								"','".$fecha1.
								"','S2','".
								$today."','".
								$varEstatus."','".
								$fecha."','".
								$hora."') ";    

			fn_ejecuta_query($sqlAddTransaccion);

			$sqlUpd ="UPDATE alinstruccionesmercedestbl set cveStatus='3G' WHERE vin ='".$arrS2[$i]['vin']."' ";
			fn_ejecuta_query($sqlUpd);

	    }
		//C) TRAILER
		$long=(sizeof($arrS2));
		fwrite($fileRec,"GEE".sprintf('%06d',($long)).sprintf('%09s',$arrS2[0]['sequence']));
	   	fclose($fileRec);	   	

	   	$fechaCopia = date("Y-m-d His");



		$updateFolioGM = "UPDATE trfoliostbl set folio=".$arrS2[0]['sequence']." where centroDistribucion='LZC02' and compania='GM' and tipoDocumento='GM'";
		fn_ejecuta_query($updateFolioGM);

		copy( "C:/servidores/DFYP301X", "E:/carbook/interfacesGM/respaldoS2/DFYP301X".$fechaCopia);		

		exec("C:/Paso/ejecutaFtp.bat");
		//echo "Termino IS2";		
		//echo json_encode("UNIDADES COLOCADAS EN HOLD CORRECTAMENTE");
		echo json_encode($rstSqlUnidadesGM);
	}

	function CorteS5($vinesUpd){

		unlink("C:/servidores/DFYP301X");

		 $sqlUnidadesGM= "SELECT  case when substr(cveDisFac,-5) in('77831','77828','77827','77848','77829','77830','72148','31112') then 'G' else 'M' end as shipper, vin, 'S5' as eventCode, substring(now(),1,10) as  vehicleDate, 'LC' as portCode, currency,'' as volume, ".
	 					"substring(cveDisFac,3) as ship, substring(now(),1,10) as dateVehicle, ".
						"'TRAM' as sender, concat('0000',(SELECT folio +1 FROM trfoliostbl WHERE centroDistribucion='LZC02' AND compania ='GM' AND tipoDocumento ='GM')) as sequence,  '' as orderNumber,'  ' as dealer,'' as portDischarge, ".
						"concat(substring(now(),12,2),substring(now(),15,2),substring(now(),18,2)) as eventTime ".
						"FROM alinstruccionesmercedestbl ".
						"where vin  in ('".$vinesUpd.");";
		  $rstSqlUnidadesGM= fn_ejecuta_query($sqlUnidadesGM);	

						  	//echo json_encode($rstSqlUnidadesGM);			

		if(sizeof($rstSqlUnidadesGM['root']) != NULL){

		for ($i=0; $i <sizeof($rstSqlUnidadesGM['root']) ; $i++) { 
			//echo json_encode($rstSqlUnidadesGM['root'][$i]['cveStatus']);
			if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S5') {				
				$arrS5[] = $rstSqlUnidadesGM['root'][$i];
			}
		/////////////////////////////////////////////////////////
			/*else if ($rstSqlUnidadesGM['root'][$i]['eventCode'] == 'S3') {
				$arrS3[] = $rstSqlUnidadesGM['root'][$i];
			}
			else{
				echo "string";
			}*/
			}/////////////////////////////////////////
		}

		//echo json_encode($arrS5);

		if ($arrS5 != null) {
			generaS5($arrS5,$rstSqlUnidadesGM);			
		}
		/*if ($arrS3 != null) {
			generaS3($arrS3);			
		}*/

	}


	function generaS5($arrS5,$rstSqlUnidadesGM){

		$fecha = date("YmdHis");
	    $fileDir = "C:/servidores/DFYP301X";


	    $fileRec = fopen($fileDir, "a") or die("No se pudo abrir archivo");
	    $recordsPositionValue = array();
	    

	    $nombreBusqueda = "DFYP301X";


	            
	    //A) ENCABEZADO
	    
		fwrite($fileRec,"GESGEIS5.000  TRAM             DFYDETROIT$".$fecha.sprintf('%09s',$arrS5[0]['sequence'])."EST".PHP_EOL);
	        
	    //B) DETALLE UNIDADES
	   	for ($i=0; $i <sizeof($arrS5) ; $i++) {
				

			fwrite($fileRec,sprintf('%-1s',$arrS5[$i]['shipper']).sprintf('%-17s',$arrS5[$i]['vin']).sprintf('%-6s',$arrS5[$i]['orderNumber']).sprintf('%-2s',$arrS5[$i]['eventCode']).sprintf('%-10s',$arrS5[$i]['vehicleDate']).
							sprintf('%-9s',$arrS5[$i]['dealer']).sprintf('%-3s',$arrS5[$i]['portCode']).sprintf('%-5s',$arrS5[$i]['portDischarge']).sprintf('%-12s',$arrS5[$i]['currency'])."PP".sprintf('%-92s',$arrS5[$i]['buque'])./*sprintf('%-7s',$arrS5[$i]['volume']).
							sprintf('%-25s',$arrS5[$i]['reserved']).sprintf('%-10s',$arrS5[$i]['dateVehicle']).sprintf('%-30s',$arrS5[$i]['storageLocation']).sprintf('%-15s',$arrS5[$i]['storageNumber']).sprintf('%-14s',$arrS5[$i]['futureUse']).
							sprintf('%-3s',$arrS5[$i]['timerQ']).*/"EST".sprintf('%-138s',$arrS5[$i]['eventTime'])/*.sprintf('%-32s',$arrS5[$i]['reservedForFutureUse']).sprintf('%-100s',$arrS5[$i]['holdAdditional'])*/.PHP_EOL);

			$vin = $arrS5[$i]['vin'];
			$today = date("Y-m-d H:i:s");
			$fecha = substr($today,0,10);
			$hora=substr($today,11,8);
			$fecha1 = $arrS5[$i]['vehicleDate']." ".$hora;


			$sqlAddTransaccion= "INSERT INTO altransaccionunidadtbl(tipoTransaccion,centroDistribucion,folio,vin,fechaGeneracionUnidad,claveMovimiento,fechaMovimiento,prodStatus,fecha,hora) ".					 					
								"VALUES ('IS5','LZC02','".$arrS5[$i]['sequence']."','".
								$arrS5[$i]['vin'].
								"','".$fecha1.
								"','S5','".
								$today."','".
								$varEstatus."','".
								$fecha."','".
								$hora."') ";    

			fn_ejecuta_query($sqlAddTransaccion);

			$sqlUpd ="UPDATE alinstruccionesmercedestbl set cveStatus='3G' WHERE vin ='".$arrS5[$i]['vin']."' ";
			fn_ejecuta_query($sqlUpd);

	    }
		//C) TRAILER
		$long=(sizeof($arrS5));
		fwrite($fileRec,"GEE".sprintf('%06d',($long)).sprintf('%09s',$arrS5[0]['sequence']));
	   	fclose($fileRec);
	   	//ft ($nombreBusqueda);
		$fechaCopia = date("Y-m-d His");

		$updateFolioGM = "UPDATE trfoliostbl set folio=".$arrS5[0]['sequence']." where centroDistribucion='LZC02' and compania='GM' and tipoDocumento='GM'";
		fn_ejecuta_query($updateFolioGM);

		copy( "C:/servidores/DFYP301X", "E:/carbook/interfacesGM/respaldoS5/DFYP301X".$fechaCopia);		

		exec("C:/Paso/ejecutaFtp.bat");

		//echo json_encode("UNIDADES CORTADAS DE MANIFIESTO CORRECTAMENTE");
		echo json_encode($rstSqlUnidadesGM);
	}
?>