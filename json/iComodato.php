<?php
	require("../funciones/generales.php");
	require("../funciones/utilidades.php");

	global $dirPath;
	global $filePath;
	global $fileName;
	date_default_timezone_set('America/Mexico_City');

	$ejecutaProceso = true;


	while(true) {
		if(date("i")%60 == 0) {
			if($ejecutaProceso == true) {
				echo "Inicio: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
				RA2V();
				RA3R();	
				echo "Termino: ".date("Y-m-d H:i", strtotime("now"))."\r\n";
				$ejecutaProceso = false;
			}
		}
		else
			$ejecutaProceso = true;
	}

	function RA2V(){
		$startDate = date('Y-m-d', strtotime("-30 days")); //- 5 días
	    $yesterday = date('Y-m-d', strtotime("-1 days"));
	    $today = date('Y-m-d');
	    $todayDateTime = date('Y-m-d H:i:s');


		$sqlGeneraDatos = "SELECT h.vin, h.fechaEvento ". 
	      "FROM alHistoricoUnidadesTbl h ".
	      "WHERE h.centroDistribucion IN ('CMDAT') ".  
	        " AND h.claveMovimiento IN ('IC') ".
	        " AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'R2V' AND rv.fechaMovimiento = h.fechaEvento) ". 
	        " AND h.fechaEvento >=  '".$startDate."' ";

	    $rsGeneraDatos = fn_ejecuta_query($sqlGeneraDatos);

		$sqlTotalUnidades = "SELECT COUNT(H.VIN) AS numeroUnidad ".
	    "  FROM alHistoricoUnidadesTbl h ".
	    "  WHERE h.centroDistribucion IN ('CMDAT') ". 
	    "    AND h.claveMovimiento IN ('IC') ".
	    "    AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'R2V' AND rv.fechaMovimiento = h.fechaEvento) ". 
	        " AND h.fechaEvento BETWEEN '".$startDate."' AND '".$today."' ";

	    $rsTotalUnidades = fn_ejecuta_query($sqlTotalUnidades);

	    $patioGroup = $rsTotalUnidades['root'][0]['numeroUnidad'];


    	if(sizeof($rsGeneraDatos['root']) != null){
			//$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
	        $fileDir = "E:/carbook/i340/RA2V.txt";
	        $rampaFija = '807';
	        $lw_ramp = 'ADIMSRAMPMX';
	        $flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
	        $recordsPositionValue = array();
	        //for ($i = 0; $i <= $patioGroup; i++) {       
	            //foreach ($patioGroup as $conteo => $patioGroup) {
	            //A) ENCABEZADO
	            fwrite($flReporte660, 'ISA*03*RA2VE     *00*          *ZZ*ADIMSRAMPMX991 *ZZ*ADIMS          *'.date_format(date_create($today), 'ymd').'*'.date_format(date_create(date('H:i:s')), 'H').''.date_format(date_create(date('H:i:s')), 'i').'*U*00300*'.sprintf('%09d', sizeof($rsGeneraDatos['root'])).'*0*P*'.PHP_EOL);
	                

	                //B) DETALLE UNIDADES
	                
	                for ($i=0; $i < sizeof($rsGeneraDatos['root']); $i++) { 
	                    fwrite($flReporte660,'2VCM'.date('mdy').$rsGeneraDatos['root'][$i]['vin'].'CMDAT'.out('s', 27).'A'.out('s', 14).date_format(date_create($rsGeneraDatos['root'][$i]['fechaEvento']),'Hi').'NC     '.PHP_EOL);
	                }
	                fwrite($flReporte660, 'IEA*01*'.sprintf('%09d', sizeof($rsGeneraDatos['root'])).PHP_EOL);
	                //C) TRAILER
	                /*$record = array(0 => 'IEA*01*', 7 => sprintf('%09d', count($patioGroup)));
	                $recordsPositionValue[] = $record;
	                $trailerTxt = getTxt($record, array_keys($record));
	                fwrite($flReporte660, $trailerTxt);*/
	            //}
	        //}
	        fclose($flReporte660);
	        //echo json_encode($recordsPositionValue);

		  	$sqlInsTransaccion =	"INSERT INTO altransaccionunidadtbl  (tipoTransaccion, centroDistribucion, folio, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus)".
		        					  "SELECT 'R2V','CMDAT' as centroDistribucion,null as folio,h.vin, now() as fechaMovimiento,h.claveMovimiento,h.fechaEvento,a660.prodstatus ".
		                              "FROM alHistoricoUnidadesTbl h, al660tbl a660 ".
		                              "WHERE h.vin = a660.vin ".
		                              "AND h.centroDistribucion IN ('CMDAT') ". 
		                              "AND h.claveMovimiento IN ('IC') ".
		                              "AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'R2V' AND rv.fechaMovimiento = h.fechaEvento) ".  
	        						  "AND h.fechaEvento >=  '".$startDate."' ";

			$rsAddTransaccion = fn_ejecuta_query($sqlInsTransaccion);
    	}else{
    		echo "No se genero archivo RA2VE  ";
    	}	        	        						  		
	}

	function RA3R(){
	    $startDate = date('Y-m-d', strtotime("-30 days")); //- 5 días
	    $yesterday = date('Y-m-d', strtotime("-1 days"));
	    $today = date('Y-m-d');
	    $todayDateTime = date('Y-m-d H:i:s');


		$sqlGeneraDatos = "SELECT h.vin, h.fechaEvento ". 
	      "FROM alHistoricoUnidadesTbl h ".
	      "WHERE h.centroDistribucion IN ('CMDAT') ".  
	        " AND h.claveMovimiento IN ('SV','ST','SP','SX') ".
	        " AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'RA3' AND rv.fechaMovimiento = h.fechaEvento) ". 
	        " AND h.fechaEvento >= '".$startDate."' ";

	    $rsGeneraDatos = fn_ejecuta_query($sqlGeneraDatos);

		$sqlTotalUnidades = "SELECT COUNT(H.VIN) AS numeroUnidad ".
	    "  FROM alHistoricoUnidadesTbl h ".
	    "  WHERE h.centroDistribucion IN ('SV','ST','SP','SX') ". 
	    "    AND h.claveMovimiento IN ('IC') ".
	    "    AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'R2V' AND rv.fechaMovimiento = h.fechaEvento) ". 
	        " AND h.fechaEvento BETWEEN '".$startDate."' AND '".$today."' ";

	    $rsTotalUnidades = fn_ejecuta_query($sqlTotalUnidades);

	    $patioGroup = $rsTotalUnidades['root'][0]['numeroUnidad'];


	    if(sizeof($rsGeneraDatos['root']) != null){
	    //$fileDir = $_SERVER['DOCUMENT_ROOT']."/RA2V.txt";
	        $fileDir = "E:/carbook/i341/RA3R.txt";
	        $rampaFija = '807';
	        $lw_ramp = 'ADIMSRAMPMX';
	        $flReporte660 = fopen($fileDir, "a") or die("No se pudo abrir Reporte");
	        $recordsPositionValue = array();
	        //for ($i = 0; $i <= $patioGroup; i++) {       
	            //foreach ($patioGroup as $conteo => $patioGroup) {
	            //A) ENCABEZADO
	            fwrite($flReporte660, 'ISA*03*RA3R      *00*          *ZZ*ADIMSRAMPMX991 *ZZ*ADIMS          *'.date_format(date_create($today), 'ymd').'*'.date_format(date_create(date('H:i:s')), 'H').''.date_format(date_create(date('H:i:s')), 'i').'*U*00300*'.sprintf('%09d', sizeof($rsGeneraDatos['root'])).'*0*P*'.PHP_EOL);
	                

	                //B) DETALLE UNIDADES
	                
	                for ($i=0; $i < sizeof($rsGeneraDatos['root']); $i++) {

	                    fwrite($flReporte660,'3RCM'.date('mdy').$rsGeneraDatos['root'][$i]['vin'].'CMDAT'.out('s', 17).date_format(date_create($rsGeneraDatos['root'][$i]['fechaEvento']),'Hi').out('s', 26).PHP_EOL);
	                }
	                fwrite($flReporte660, 'IEA*01*'.sprintf('%09d', sizeof($rsGeneraDatos['root'])).PHP_EOL);
	                //C) TRAILER
	                /*$record = array(0 => 'IEA*01*', 7 => sprintf('%09d', count($patioGroup)));
	                $recordsPositionValue[] = $record;
	                $trailerTxt = getTxt($record, array_keys($record));
	                fwrite($flReporte660, $trailerTxt);*/
	            //}
	        //}
	        fclose($flReporte660);
	        //echo json_encode($recordsPositionValue);

		  	$sqlInsTransaccion =	"INSERT INTO altransaccionunidadtbl  (tipoTransaccion, centroDistribucion, folio, vin, fechaGeneracionUnidad, claveMovimiento, fechaMovimiento, prodStatus)".
									"SELECT 'RA3','CMDAT' as centroDistribucion,null as folio,h.vin, now() as fechaMovimiento,h.claveMovimiento,h.fechaEvento,a660.prodstatus ".
									"FROM alHistoricoUnidadesTbl h, al660tbl a660 ".
									"WHERE h.vin = a660.vin ".
  									"AND h.centroDistribucion IN ('CMDAT')  ".
									"AND h.claveMovimiento IN ('SV','ST','SP','SX') ".
									"AND h.vin NOT IN (SELECT rv.vin FROM altransaccionunidadtbl rv WHERE h.vin = rv.vin AND rv.tipoTransaccion = 'RA3' AND rv.fechaMovimiento = h.fechaEvento)   ".
									"AND h.fechaEvento >=  '".$startDate."' ";

			$rsAddTransaccion = fn_ejecuta_query($sqlInsTransaccion);
	    }else{
	    	echo "No se genero Archivo RA3R  ";
	    }
	}
?>	