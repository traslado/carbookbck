<?php
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");

    function leerXLS($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) <= 67 && $highestRow <= 3 ) {
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) < 68) {
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17) {
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN-Simbolo-Distribuidor-Color]', 'fail'=>'Y');
			return $root;
		}
		//------------------------------------------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
								'Simbolo'=>array('val'=>'','size'=>true,'exist'=>true),
								'Distribuidor' =>array('val'=>'','size'=>true,'exist'=>true),								
								'Avanzada' =>array('val'=>'','size'=>true,'exist'=>true)
								//'ubicacion' =>array('val'=>'','size'=>true,'exist'=>true),
								//'tipoMercado' =>array('val'=>'','size'=>true,'exist'=>true)
							);
		
		for($row = 0; $row<sizeof($rowData); $row++){
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['Simbolo']['val'] = $rowData[$row][1];
			$isValidArr['Simbolo']['size'] = strlen($rowData[$row][1]) > 0;
			$isValidArr['Distribuidor']['val'] = $rowData[$row][2];
			$isValidArr['Distribuidor']['size'] = strlen($rowData[$row][2]) == 5;
			$isValidArr['Avanzada']['size'] = substr($rowData[$row][0],9,17);
			//$isValidArr['ubicacion']['val'] = $rowData[$row][3];
			//$isValidArr['ubicacion']['size'] = strlen($rowData[$row][3]) > 0;
			//$isValidArr['tipoMercado']['val'] = $rowData[$row][4];
			//$isValidArr['tipoMercado']['size'] = strlen($rowData[$row][4]) > 0;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {
				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				} else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						if (!$isValidArr[$key]['exist']) {
							$siNo = ($key == 'VIN') ? ' ya ' : ' no ';
							$errorMsg .= 'El '.$key.$siNo.'Existe!';
							$isTrue = false; 
						}
					}
				}
			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa
				$sqlidTarifa = "SELECT ct.idTarifa FROM caSimbolosUnidadesTbl su, caClasificacionTarifasTbl ct,".
							   " caTarifasTbl t WHERE su.clasificacion = ct.clasificacion ".
							   "AND ct.idTarifa = t.idTarifa ".
							   "AND su.simboloUnidad = '".$rowData[$row][1]."'";

				$rsTarifa = fn_ejecuta_query($sqlidTarifa);				

				/*$sqlColor = "SELECT  1  as colorExistente FROM cacolorunidadestbl cu, casimbolosunidadestbl su ".
								"WHERE  cu.marca = su.marca ".
								"AND su.simboloUnidad = '".$rowData[$row][1]."' ".
								"AND  cu.color = '".$rowData[$row][3]."'";

				$rsColorUnidades = fn_ejecuta_query($sqlColor);	*/							
				
				if (!$rsTarifa || sizeof($rsTarifa['root']) < 1) {
					$errorMsg = 'Registro No Agregado. Verifica la Tarifa, S&iacute;mbolo o Clasificaci&oacute;n';
					
				}/*elseif(sizeof($rsColorUnidades['root']) < 1){
					$errorMsg = 'Registro No Agregado. Verifica el Color para la marca';
				} */else {

					//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl

					$_SESSION['usuCompania']='LZC02';

					/*$addTR = "INSERT INTO alUnidadesTbl (vin, avanzada, distribuidor,simboloUnidad,color,folioRepuve, descripcionUnidad)".
					"VALUES( '".$rowData[$row][0]."' ,".
						"'".substr($rowData[$row][0],9,17)."', ".
						"'".$rowData[$row][2]."', ".
						"'".$rowData[$row][1]."', ".
						"'".$rowData[$row][3]."', ".						
						replaceEmptyNull("'".$repuve."'").", ".					
						replaceEmptyNull("'LZC02_ingreso01'").") ";

					fn_ejecuta_query($addTR);*/
					
					$today = date("Y-m-d H:i:s");
					$fecha = substr($today,0,10);
					$hora=substr($today,11,8);						

					$addHist = "INSERT INTO alInstruccionesMercedesTbl (cveDisFac,nomFac,cveDisEnt,dirEnt,fEvento,hEvento,fPed, vin,  natType,modelDesc,cveStatus)".
					"VALUES( '".$rowData[$row][2]."', ".
						"'".$rowData[$row][3]."', ".
						"'".$rowData[$row][2]."', ".
						"'".$rowData[$row][4]."', ".
						"'".$fecha."', ".
						"'".$hora."',".
						"'".$rowData[$row][5]."', ".
						"'".$rowData[$row][0]."', ".
						"'".substr($rowData[$row][0],9,17)."', ".
		                "'".$rowData[$row][1]."', ".
		                "'".$rowData[$row][6]."') ";

					fn_ejecuta_query($addHist);

					$addTmp = "INSERT INTO alInstruccionesMercedesTmp (cveDisFac,nomFac,cveDisEnt,dirEnt,fEvento,hEvento,fPed, vin,  natType,modelDesc,cveStatus)".
					"VALUES( '".$rowData[$row][2]."', ".
						"'".$rowData[$row][3]."', ".
						"'".$rowData[$row][2]."', ".
						"'".$rowData[$row][4]."', ".
						"'".$fecha."', ".
						"'".$hora."',".
						"'".$rowData[$row][5]."', ".
						"'".$rowData[$row][0]."', ".
						"'".substr($rowData[$row][0],9,17)."', ".
		                "'".$rowData[$row][1]."', ".
		               	"'".$rowData[$row][6]."') ";

					fn_ejecuta_query($addTmp);					


					/*$addUdet = "INSERT INTO alUltimoDetalleTbl (centroDistribucion ,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip)".
					"VALUES( '".$_SESSION['usuCompania']."', ".
						"'".$rowData[$row][0]."', ".
						"NOW(), ".
						"'L1', ".
						"'".$rowData[$row][2]."', ".
		                "'1',".
						"'".$_SESSION['usuCompania']."', ".						
						replaceEmptyNull("'".$chofer."'").", ".					
		                 "'UNIDAD INGRESO L1',".
		                 "'98',".
                         "'".$_SERVER['REMOTE_ADDR']."') ";					

					fn_ejecuta_query($addUdet);*/

					

					/*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCompania'],'PR',
							  $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCompania'], $repuve, $chofer, $observaciones,$rowData[$row][4]);*/

					if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
						$errorMsg = 'Agregado Correctamente';
					} else {
						$errorMsg = 'Registro No Agregado';
					}
					
				}
			}
			$root[]= array('vin'=>$rowData[$row][0],'simbolo'=>$rowData[$row][1], 'distribuidor'=>$rowData[$row][2],'ubicacion'=>$rowData[$row][3],'tipoMercado'=>$rowData[$row][4],'nose'=>$errorMsg);			

		}
		return $root;	
	}


	function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
		$tabla = 'tabla';
		$columna = 'columna';
		switch(strtoupper($field)) {
			case 'VIN':
				$tabla = 'alInstruccionesMercedesTbl'; $columna = 'vin'; break;
			case 'SIMBOLO':
				$tabla = 'caSimbolosUnidadesTbl'; $columna = 'simboloUnidad'; break;
			case 'DISTRIBUIDOR':
				$tabla = 'caDistribuidoresCentrosTbl'; $columna = 'distribuidorCentro'; break;

		}
		$sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
		$rs = fn_ejecuta_query($sqlExist);
		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
	}

			/*

			$vVin = strlen($rowData[$iInt][0]);
			$vSimbolo = strlen($rowData[$iInt][1]);
			$vDistribuidor = strlen($rowData[$iInt][2]);
			$vColor = strlen($rowData[$iInt][3]);
			$vAvanzada = substr($rowData[$iInt][0],9,17);


			$lsWhereStr_01 = "WHERE distribuidorCentro ='".$rowData[$iInt][2]."'";
			$sqlDistribuidoresStr = "SELECT distribuidorCentro FROM caDistribuidoresCentrosTbl ".$lsWhereStr_01;
			$rsDistribuidor = fn_ejecuta_query($sqlDistribuidoresStr);
			$cDistribuidor = $rsDistribuidor['root'][0]['distribuidorCentro'];

			$lsWhereStr_02 = "WHERE simboloUnidad ='".$rowData[$iInt][1]."'";
			$sqlSomboloStr = "SELECT simboloUnidad FROM caSimbolosUnidadesTbl ".$lsWhereStr_02;
			$rsSimbolo = fn_ejecuta_query($sqlSomboloStr);
			$cSimbolo = $rsSimbolo['root'][0]['simboloUnidad'];

			$lsWhereStr_03 = "WHERE color ='".$rowData[$iInt][3]."'";
			$sqlColorStr = "SELECT color FROM cacolorunidadestbl ".$lsWhereStr_03;
			$rsColor = fn_ejecuta_query($sqlColorStr);
			$cColor = $rsColor['root'][0]['color'];

			$lsWhereStr_04 = "WHERE vin ='".$rowData[$iInt][0]."'";
			$sqlVintr = "SELECT vin FROM alUnidadesTbl ".$lsWhereStr_04;
			$rsVin = fn_ejecuta_query($sqlVintr);
			$cVin = $rsVin['root'][0]['vin'];

			$lsWhereStr_05 = "WHERE A.clasificacion = B.clasificacion ".
							 "AND B.idTarifa = C.idTarifa ".
							 "AND C.tipoTarifa = 'N' ".
							 "AND A.simboloUnidad = '".$rowData[$iInt][1]."'";

			$sqlidTrarifatr = "SELECT B.idTarifa FROM casimbolosunidadestbl A, caclasificaciontarifastbl B, catarifastbl C ".$lsWhereStr_05;
			$rsTarifa = fn_ejecuta_query($sqlidTrarifatr);

			$vIdTarifa = $rsTarifa['root'][0]['idTarifa'];


	    
			if($rsColor > 0 and  $vColor > 0 )
			{
				if($cDistribuidor != '' and $vDistribuidor == '5')
				{
					if($cSimbolo != '' and $vSimbolo > 0)
					{
						if($cVin == '' and $vVin == '17')
						{							
							$root[$iInt] = array('VIN'=>$rowData[$iInt][0],'simbolo'=>$rowData[$iInt][1],'distribuidor'=>$rowData[$iInt][2],'color'=>$rowData[$iInt][3],'nose'=>'Unidad Cargada...');
							
							$sqlAddUnidadStr = "INSERT INTO alUnidadesTbl ".
	                           "VALUES(".
	                           "'".$rowData[$iInt][0]."',".
	                           "'".substr($rowData[$iInt][0], -8)."',".
	                           "'".$rowData[$iInt][2]."',".
	                           "'".$rowData[$iInt][1]."',".
	                           "'".$rowData[$iInt][3]."',".
	                           replaceEmptyNull("'".$repuve."'").")";
							$rs_01 = fn_ejecuta_query($sqlAddUnidadStr);
	        				
	        				$sqlAddHistoricoUnidadStr = "INSERT INTO alHistoricoUnidadesTbl ".
	                            "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, ".
	                            "localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
	                            "VALUES(".
	                            "'CDVER',".
	                            "'".$rowData[$iInt][0]."',".
	                            "(SELECT CURRENT_TIMESTAMP),".
	                            "'PR',".
	                            "'".$rowData[$iInt][2]."',".
	                            "'".$vIdTarifa."',".
	                            "'PVER1',".
	                            replaceEmptyNull("'".$chofer."'").",".
	                            "'ARCHIVO DE CARGA MASIVA',".
	                            "'".$_SESSION['usuario']."',".
	                            "'".$_SERVER['REMOTE_ADDR']."')";
	        				$rs_02 = fn_ejecuta_query($sqlAddHistoricoUnidadStr);

						}
						else{
							$root[$iInt] = array('VIN'=>$rowData[$iInt][0],'simbolo'=>$rowData[$iInt][1],'distribuidor'=>$rowData[$iInt][2],'color'=>$rowData[$iInt][3],'nose'=>'El VIN ya existe o no tiene los 17 caracteres');
						}
					}
					else{
						$root[$iInt] = array('VIN'=>$rowData[$iInt][0],'simbolo'=>$rowData[$iInt][1],'distribuidor'=>$rowData[$iInt][2],'color'=>$rowData[$iInt][3],'nose'=>'El Simbolo No existe en el Sistema');
					}
				}
				else{
					$root[$iInt] = array('VIN'=>$rowData[$iInt][0],'simbolo'=>$rowData[$iInt][1],'distribuidor'=>$rowData[$iInt][2],'color'=>$rowData[$iInt][3],'nose'=>'El Distribuidor No existe en el Sistema');	
				}		
			}
			else{				
				$root[$iInt] = array('VIN'=>$rowData[$iInt][0],'simbolo'=>$rowData[$iInt][1],'distribuidor'=>$rowData[$iInt][2],'color'=>$rowData[$iInt][3],'nose'=>'El Color No existe');	
			}
		}
		return $root;
	}*/

?>