<?php
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");


	function closeDeleteFiles($nombreRespaldo, $nombreLog, $logFile, $nombreError, $errorFile,$nombreFile){
		fclose($nombreRespaldo);
		fclose($logFile);
		fclose($errorFile);
		fclose($nombreFile);	

		unlink($nombreFile);	
		//echo $nombreFile;	
	
		if(filesize($nombreRespaldo) == 0)
			unlink($nombreRespaldo);
		if(filesize($nombreLog) == 0)
			unlink($nombreLog);
		if(filesize($nombreError) == 0)
			unlink($nombreError);
		if(filesize($nombreFile) == 0)
			unlink($nombreFile);

	}

?>	