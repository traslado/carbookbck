<?php
	session_start();
	//$_SESSION['modulo'] = "catColores";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

   // $_REQUEST = trasformUppercase($_REQUEST);
	
    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    } 
        switch($_REQUEST['trContinuidadSeHdn']){
        case 'getContinuidadUns':
        	getContinuidadUns();
        	break;   
        case 'getViajeContinuidad':
        	getViajeContinuidad();
        	break;
        case 'addContinuidadUns':
            addContinuidadUns();
            break;
        default:
            echo '';
            
    }

    function getContinuidadUns(){
    	
        $sqlGetContinuidad = "SELECT ue.vin, au.distribuidor, au.simboloUnidad, color, 0 AS seleccion ".
                            "FROM trunidadesdetallestalonestbl ts, trunidadesembarcadastbl ue,trviajestractorestbl vt, trtalonesviajestbl tl, aldestinosespecialestbl de, catractorestbl tr, alUnidadesTbl au ".
                            "WHERE ts.vin = ue.vin AND ts.idTalon = tl.idTalon AND tl.idViajeTractor = vt.idViajeTractor ".
                            "AND de.vin = ts.vin ".
                            "AND ue.vin = au.vin ".
                            "AND tr.idTractor = vt.idTractor ".
                            "AND tl.centroDistribucion = '".$_REQUEST['asignacionUnidadesContinuidadCdCmb']."' ".
                            "AND tl.folio = ".$_REQUEST['asignacionUnidadesContinuidadFolioTxt']." ".
                            "AND tr.compania = '".$_REQUEST['asignacionUnidadesContinuidadCompaniaCmb']."' ";		
		$rsContinuidad = fn_ejecuta_query($sqlGetContinuidad);
			
		echo json_encode($rsContinuidad);
    }

    function getViajeContinuidad(){
        $sqlGetViajes = "SELECT vt.viaje,vt.numeroUnidades,tr.tractor,tl.tipoTalon, ".
                        "(SELECT distinct(au.distribuidor) ". 
                            "FROM trunidadesdetallestalonestbl ts, trunidadesembarcadastbl ue,trviajestractorestbl vt, trtalonesviajestbl tl, aldestinosespecialestbl de, catractorestbl tr, alUnidadesTbl au ".
                            "WHERE ts.vin = ue.vin ".
                            "AND ts.idTalon = tl.idTalon ". 
                            "AND tl.idViajeTractor = vt.idViajeTractor ".
                            "AND de.vin = ts.vin ".
                            "AND ue.vin = au.vin ".
                            "AND tr.idTractor = vt.idTractor AND tl.centroDistribucion = '".$_REQUEST['asignacionUnidadesContinuidadCdCmb']."' ".
                            "AND tl.folio = '".$_REQUEST['asignacionUnidadesContinuidadFolioTxt']."' ". 
                            "AND tr.compania = '".$_REQUEST['asignacionUnidadesContinuidadCompaniaCmb']."'  limit 1) as distirbuidorUnidades ".                        
                        "FROM trviajestractorestbl vt,trtalonesviajestbl tl, catractorestbl tr ".
                        "WHERE vt.idViajeTractor = tl.idViajeTractor ".
                        "AND vt.idTractor = tr.idTractor ".
                        "AND tr.compania = '".$_REQUEST['asignacionUnidadesContinuidadCompaniaCmb']."' ".
                        "AND vt.centroDistribucion = '".$_REQUEST['asignacionUnidadesContinuidadCdCmb']."' ".
                        "AND tl.folio = '".$_REQUEST['asignacionUnidadesContinuidadFolioTxt']."' ";
        
        $rsViajeContinuidad = fn_ejecuta_query($sqlGetViajes);
            
        echo json_encode($rsViajeContinuidad);                        
    }

    function addContinuidadUns(){

        $a = array();
	      $a['success'] = true;
	      $a['msjResponse'] = 'Unidad agregada correctamente.';
        
        $arrUnidades = json_decode($_POST['arrUnidades'],true);						//CHK
        //$arrUnidades = json_decode($_GET['arrUnidades'],true);						//CHK
        
        //var_dump($arrUnidades);
        foreach($arrUnidades as $index => $row)
				{
		        $addUnsCon = "INSERT INTO trUnidadesDetallesTalonesTmp (numeroTalon, avanzada, vin, simbolo, color) ".
		                    "SELECT distinct  '".$_REQUEST['asignacionUnidadesContinuidadIdTalon']."' as numeroTalon, substr(ue.vin, 10,8) as avanzada,ue.vin, au.simboloUnidad, au.color ".
		                    "FROM trUnidadesDetallesTalonesTbl ts, trUnidadesEmbarcadasTbl ue,trViajesTractoresTbl vt, trTalonesViajesTbl tl, alDestinosEspecialesTbl de, caTractoresTbl tr, alUnidadesTbl au ".
		                    "WHERE ts.vin = ue.vin AND ts.idTalon = tl.idTalon AND tl.idViajeTractor = vt.idViajeTractor ".
		                    "AND de.vin = ts.vin ".
		                    "AND ue.vin = au.vin ".
		                    "AND tr.idTractor = vt.idTractor ".
		                    "AND tl.centroDistribucion = '".$_REQUEST['asignacionUnidadesContinuidadCdCmb']."' ".
		                    "AND tl.folio = ".$_REQUEST['asignacionUnidadesContinuidadFolioTxt']." ".
		                    "AND tr.compania = '".$_REQUEST['asignacionUnidadesContinuidadCompaniaCmb']."' ".
		                    "AND de.vin = '".$row['vin']."'";
					 //echo "$addUnsCon<br>";
		       fn_ejecuta_query($addUnsCon);
		       
			      if(isset($_SESSION['error_sql']) && $_SESSION['error_sql'] !="")
			      {
			         $a['success']		= false;
			         $a['msjResponse'] = $_SESSION['error_sql']. " En la inserci&oacuten de la unidad: ".$row['vin'].".";
			         $a['sql'] = $addUnsCon;
			         break;
			      }		       
				}
				echo json_encode($a);
    }    
?>