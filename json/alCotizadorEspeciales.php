<?php

    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("trGastosViajeTractor.php");


    switch($_REQUEST['cotizacionEspecialHdn']){
        case 'CONSULTACOTIZACION':
            CONSULTACOTIZACION();
            break; 
        case 'GUARDACOTIZACION':
            GUARDACOTIZACION();
            break; 
        case 'COMBOTIPOTRASLADO':
            COMBOTIPOTRASLADO();
            break;
        case 'COMBOCOMMODITY':
            COMBOCOMMODITY();
            break;
        case 'VALIDAVINES':
            VALIDAVINES();
            break;        
         case 'VALIDARC':
            VALIDARC();
            break;        
        default:
            echo '';

    }
    

    function CONSULTACOTIZACION(){ 
        
        $sqlCotizacion="SELECT  * from alcotizacionesespecialestbl ".
                    "where numCotizacion='".$_REQUEST['numCotizacion']."'".
                    "and estatus='CZ'";
        $rsCotizacion=fn_ejecuta_query($sqlCotizacion);

               
        $cadenaVin='';
        

        for ($i=0; $i <sizeof($rsCotizacion['root']) ; $i++) {
            $cadenaVin=$cadenaVin.$rsCotizacion['root'][$i]['VIN']."\n";
        }

        $rsCotizacion['data']['VIN']=$cadenaVin;
         echo json_encode($rsCotizacion);

        
    }

    function GUARDACOTIZACION(){   

        $vin1 = $_REQUEST['unidades'];    
        $buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar = array("", "", "", "");
        $vin = str_ireplace($buscar,$reemplazar,$vin1);
        $cadena = chunk_split($vin, 17,",");        
        $unidades= explode(",",substr($cadena,0,-1));

        //echo json_encode($unidades);

        for ($i=0; $i <sizeof($unidades) ; $i++) { 

             $sqlCotizacion="SELECT  * from alcotizacionesespecialestbl ".
                    "where numCotizacion='".$_REQUEST['numCotizacion']."'".
                    "and vin ='".$unidades[$i]."' ".
                    "and estatus='CZ'";
            $rsCotizacion=fn_ejecuta_query($sqlCotizacion);


            if (sizeof($rsCotizacion['root'])!='0' ) {

                $upd="UPDATE alcotizacionesespecialestbl SET numCotizacion=".replaceEmptyNull($_REQUEST['numCotizacion']).", tipoTraslado='".replaceEmptyNull($_REQUEST['tipoTraslado'])."', VIN='".$unidades[$i]."', commodity='".replaceEmptyNull($_REQUEST['comodity'])."', distribuidor='".replaceEmptyNull($_REQUEST['distribuidor'])."', ruta='".replaceEmptyNull($_REQUEST['ruta'])."', kmscargados=".replaceEmptyNull($_REQUEST['kmsCargados']).", kmsvacios=".replaceEmptyNull($_REQUEST['kmsVacios']).", factorcarga=".replaceEmptyNull($_REQUEST['factorcarga']).", tarifabase=".replaceEmptyNull($_REQUEST['tarifaBase']).", tarifacargado=".replaceEmptyNull($_REQUEST['tarifaCargado']).", tarifavacio=".replaceEmptyNull($_REQUEST['tarifaVacio']).", llaves=".replaceEmptyNull($_REQUEST['llaves']).", ferry=".replaceEmptyNull($_REQUEST['ferry']).", desvios=".replaceEmptyNull($_REQUEST['desvios']).", extras=".replaceEmptyNull($_REQUEST['extras']).", total=".replaceEmptyNull($_REQUEST['total']).", fechacotizacion= now(), estatus='CZ', idusuario='".$_SESSION['idUsuario']."' WHERE numCotizacion='".$_REQUEST['numCotizacion']."' and estatus='CZ' AND vin='".$unidades[$i]."'";

                fn_ejecuta_query($upd);  

                $numCotizacion=$_REQUEST['numCotizacion'];              

                
            }else{

                if ($_REQUEST['numCotizacion'] == '') {
                    $sqlFolio="SELECT folio+1 as folio from trfoliostbl
                        where centroDistribucion='CDTOL'
                        and tipoDocumento='SE'";
                    $rsFolio=fn_ejecuta_query($sqlFolio);

                          $updFolio="UPDATE trfoliostbl SET folio='".$rsFolio['root'][0]['folio']."' where centroDistribucion='CDTOL'
                        and tipoDocumento='SE'";
                fn_ejecuta_query($updFolio);

                    $numCotizacion=$rsFolio['root'][0]['folio'];
                }else{
                    $numCotizacion=$_REQUEST['numCotizacion'];
                }                



                  $sqlGuarda="INSERT INTO alcotizacionesespecialestbl (numCotizacion, tipoTraslado, VIN, commodity, distribuidor, ruta, kmscargados, kmsvacios, factorcarga, tarifabase, tarifacargado, tarifavacio, llaves, ferry, desvios, extras, total, fechacotizacion, estatus, idusuario) VALUES (".replaceEmptyNull($numCotizacion).", '".replaceEmptyNull($_REQUEST['tipoTraslado'])."', '".$unidades[$i]."', '".replaceEmptyNull($_REQUEST['comodity'])."', '".replaceEmptyNull($_REQUEST['distribuidor'])."', '".replaceEmptyNull($_REQUEST['ruta'])."', ".replaceEmptyNull($_REQUEST['kmsCargados']).", ".replaceEmptyNull($_REQUEST['kmsVacios']).", ".replaceEmptyNull($_REQUEST['factorCarga']).", ".replaceEmptyNull($_REQUEST['tarifaBase']).", ".replaceEmptyNull($_REQUEST['tarifaCargado']).", ".replaceEmptyNull($_REQUEST['tarifaVacio']).", ".replaceEmptyNull($_REQUEST['llaves']).", ".replaceEmptyNull($_REQUEST['ferry']).", ".replaceEmptyNull($_REQUEST['desvios']).", ".replaceEmptyNull($_REQUEST['extras']).", ".replaceEmptyNull($_REQUEST['total']).", now(), 'CZ', '".$_SESSION['idUsuario']."')";
                $rsguarda=fn_ejecuta_query($sqlGuarda);   

               
            }                  
            
        }

         /*$updFolio="UPDATE trfoliostbl SET folio='".$numCotizacion."' where centroDistribucion='CDTOL'
                        and tipoDocumento='SE'";
                fn_ejecuta_query($updFolio);*/

        $rs['data']['numCotizacion']=$numCotizacion;

          echo json_encode($rs);
        
    }
     

    function COMBOTIPOTRASLADO(){ 
        
        $sqlGenTipo="SELECT  nombre from cageneralestbl
                        WHERE TABLA='alcotizacionesespecialestbl'
                        AND columna='tipoTraslado'";
        $rsTipo=fn_ejecuta_query($sqlGenTipo);
        

        echo json_encode($rsTipo);
        
    }

    function COMBOCOMMODITY(){ 
        
        $sqlGenCom="SELECT valor, concat(valor,'-', nombre) as nombre from cageneralestbl
                    WHERE TABLA='alcotizacionesespecialestbl'
                    AND columna='commodity'";
        $rsCom=fn_ejecuta_query($sqlGenCom);
        

        echo json_encode($rsCom);
        
    }

    function VALIDAVINES(){

        $vin1= $_REQUEST['unidades'];


        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");
        $vin=str_ireplace($buscar,$reemplazar,$vin1);

        $cadena = chunk_split($vin, 17,"','");
        
        $vines=substr($cadena,0,-2);
        
        $sqlCotizacion="SELECT  * from alcotizacionesespecialestbl ".
                    "where vin in ('".$vines.") ".
                    "and estatus='CZ'";
        $rsCotizacion=fn_ejecuta_query($sqlCotizacion);

               
        $cadenaVin='';
        

        for ($i=0; $i <sizeof($rsCotizacion['root']) ; $i++) {
            $cadenaVin=$cadenaVin.$rsCotizacion['root'][$i]['VIN']."\n";
        }

        $rsCotizacion['data']['VIN']=$cadenaVin;
         echo json_encode($rsCotizacion);

        
    
    }


    function VALIDARC(){

        $vin1= $_REQUEST['unidades'];


        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array("", "", "", "");
        $vin=str_ireplace($buscar,$reemplazar,$vin1);

        $cadena = chunk_split($vin, 17,"','");
        
        $vines=substr($cadena,0,-2);
        
        $sqlCotizacion="SELECT  * from alultimodetalletbl ".
                    "where vin in ('".$vines.") ".
                    "and claveMovimiento!='RC'";
        $rsCotizacion=fn_ejecuta_query($sqlCotizacion);

               
        $cadenaVin='';
        

        for ($i=0; $i <sizeof($rsCotizacion['root']) ; $i++) {
            $cadenaVin=$cadenaVin.$rsCotizacion['root'][$i]['VIN']."\n";
        }

        $rsCotizacion['data']['VIN']=$cadenaVin;
         echo json_encode($rsCotizacion);

        
    
    }
    
    
?>
