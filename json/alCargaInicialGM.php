<?php
    session_start();
	
	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");
	
    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["alCinicialCargaFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["alCinicialCargaFld"]["error"];

		} else {
			$temp_file_name = $_FILES['alCinicialCargaFld']['tmp_name']; 
			$original_file_name = $_FILES['alCinicialCargaFld']['name'];
		  
			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;

			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLS($new_name);
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);
	

    function leerXLS($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) <= 67 && $highestRow <= 10 ) {
			$root[] = array('descripcion' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) < 2) {
			$root[] = array('descripcion' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17) {
			$root[] = array('descripcion' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN-Simbolo-Distribuidor-Color]', 'fail'=>'Y');
			return $root;
		}
		//------------------------------------------------------------------------------------------------
		
		//ECHO $_REQUEST['ceDis'];
		if($_REQUEST['ceDis'] == 'LZC02'){
			
				$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
							'Simbolo'=>array('val'=>'','size'=>true,'exist'=>true),
							'Distribuidor' =>array('val'=>'','size'=>true,'exist'=>true)							
						);

		}elseif($_REQUEST['ceDis'] == 'QRO02') {						
				$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true),
							'Simbolo'=>array('val'=>'','size'=>true,'exist'=>true),
							'Distribuidor' =>array('val'=>'','size'=>true,'exist'=>true),
							'color' =>array('val'=>'','size'=>true,'exist'=>true)
						);

		}

		//echo json_encode($isValidArr);

	
		
		for($row = 0; $row<sizeof($rowData); $row++){
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['Simbolo']['val'] = $rowData[$row][1];
			//$isValidArr['Simbolo']['size'] = strlen($rowData[$row][1]) > 0;
			$isValidArr['Distribuidor']['val'] = $rowData[$row][2];
			if($_REQUEST['ceDis'] == 'QRO02') {
				$isValidArr['color']['val'] = $rowData[$row][9];
			}
			//$isValidArr['Distribuidor']['size'] = strlen($rowData[$row][2]) == 5;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {
				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}/*if($rowData[$row][3] == null) {
					$errorMsg .='La '.$key.' Buque Erroneo ';
					echo json_encode("buque ".$rowData[$row][3]);
					$isTrue = false;
				}/*if($rowData[$row][4] == null) {
					$errorMsg .='El '.$key.' Tipo de Mercado no existe para esta Unidad';
					echo json_encode(" mercado ".$rowData[$row][4]);
					$isTrue = false;					
				}if($rowData[$row][5] == null) {
					$errorMsg .='El '.$key.' Nombre del Barco no existe para esta Unidad';
					echo json_encode(" brco ".$rowData[$row][5]);
					$isTrue = false;					
				}*/else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						if (!$isValidArr[$key]['exist']) {
							$siNo = ($key == 'VIN') ? ' ya ' : ' no ';
							$errorMsg .= 'El '.$key.$siNo.'Existe!';
							$isTrue = false; 

						}
					}					
				}
			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
				//$_SESSION['usuCompania'] = 'LZC02';
				


			
				$today = date("Y-m-d H:i:s");
				$fecha = substr($today,0,10);
				$hora=substr($today,11,8);

				$sqlGetSimbolo = "SELECT marca ".
								 "FROM casimbolosunidadestbl ".
								 "WHERE simboloUnidad = '".$rowData[$row][1]."' ";

				$rsGetSimbolo = fn_ejecuta_query($sqlGetSimbolo);
				

				$numVin = $rowData[$row][0];
				$numAvanzada = substr($rowData[$row][0],9,17);
				$numSimbolo = $rowData[$row][1];
				$numCveDisFac = $rowData[$row][2];
				$buque = $rowData[$row][3];
				$fechaArribo = $rowData[$row][4];
				$peso = $rowData[$row][5];
				$Origen = $rowData[$row][6];
				$dFinal = $rowData[$row][7];
				$madFrg = $rowData[$row][8];
				$area = $rowData[$row][9];
				$ciaSesVal=$_REQUEST['ceDis'];

				/*$numNomFac = $rowData[$row][3];
				$numDirEnt = $rowData[$row][4];
				$numFped = $rowData[$row][5];*/

				if($ciaSesVal == 'LZC02'){
					
					$tUnidad = 'DG';



				$addHist = "INSERT INTO alInstruccionesMercedesTbl (cveDisFac,nomFac,cveDisEnt,nomEnt,cveLoc,dirEnt,fEvento,hEvento,fPed, vin, natType,modelDesc,fechaMovimiento,trimCode,trimDesc,currency,cveStatus) ".
					"VALUES('".$numCveDisFac."', ".
						"'".$numNomFac."', ".
						"'".$numCveDisFac."', ".
						"'".$madFrg."', ".						
						"'".$ciaSesVal."', ".
						"'".$area."', ".
						"'".$fecha."', ".
						"'".$hora."',".
						"'".$buque."', ".
						"'".$numVin."', ".
						"'".$numAvanzada."', ".
			            "'".$numSimbolo."', ".
			            "'".$fechaArribo."', ".
			            "'".$Origen."', ".
			            "'".$dFinal."', ".
			            "'".$peso."', ".
			            "'".$tUnidad."') ";

					fn_ejecuta_query($addHist);			

				} elseif($ciaSesVal == 'QRO02') {						
					$tUnidad = 'QG';

				$addHist = "INSERT INTO alInstruccionesMercedesTbl (cveDisFac,nomFac,cveDisEnt,nomEnt,cveLoc,dirEnt,fEvento,hEvento,chipNum, vin, natType,modelDesc,fechaMovimiento,trimCode,trimDesc,currency,cveStatus) ".
					"VALUES('".$numCveDisFac."', ".
						"'".$numNomFac."', ".
						"'".$numCveDisFac."', ".
						"'".$madFrg."', ".						
						"'".$ciaSesVal."', ".
						"'".$area."', ".
						"'".$fecha."', ".
						"'".$hora."',".
						"'".$buque."', ".
						"'".$numVin."', ".
						"'".$numAvanzada."', ".
			            "'".$numSimbolo."', ".
			            "'".$fechaArribo."', ".
			            "'".$Origen."', ".
			            "'".$dFinal."', ".
			            "'".$peso."', ".
			            "'".$tUnidad."') ";


					fn_ejecuta_query($addHist);			

				}


		
				

				/*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCompania'],'PR',
						  $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCompania'], $repuve, $chofer, $observaciones,$rowData[$row][4]);*/

				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					$errorMsg = 'Agregado Correctamente';
				} else {
					$errorMsg = 'Registro No Agregado';
				}		
			}
			$root[]= array('vin'=>$rowData[$row][0],'simbolo'=>$rowData[$row][1], 'distribuidor'=>$rowData[$row][2],'buque'=>$rowData[$row][3],'fechaArribo'=>$rowData[$row][4],'peso'=>$rowData[$row][5],'Origen'=>$rowData[$row][6],'Destino Final'=>$rowData[$row][7],'Madrina o Furgon'=>$rowData[$row][8],'Demand Area'=>$rowData[$row][9],'descripcion'=>$errorMsg);			

			//echo json_encode($rowData[$row]);
		}
		return $root;	

	}


	function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
		$tabla = 'tabla';
		$columna = 'columna';

		switch(strtoupper($field)) {
			case 'VIN':
				$tabla = 'alInstruccionesMercedesTbl'; $columna = 'vin'; break;
			case 'SIMBOLO':
				$tabla = 'caSimbolosUnidadesTbl'; $columna = 'simboloUnidad'; break;
			case 'DISTRIBUIDOR':
				$tabla = 'caDistribuidoresCentrosTbl'; $columna = 'distribuidorCentro'; break;
			case 'COLOR':
				$tabla = 'cacolorunidadestbl'; $columna = 'color'; $AND='AND marca="GM" '; break;	
		}
		$sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'". $AND;
		$rs = fn_ejecuta_query($sqlExist);
		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
	}




?>