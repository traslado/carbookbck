<?php
	session_start();
	$_SESSION['modulo'] = "catClasificacionMarca";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']) {
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['catClasificacionMarcaActionHdn']) {
        case 'getClasificacionMarca':
            getClasificacionMarca();
            break;
        case 'getClasificacionesGroup':
            getClasificacionesGroup();
            break;
        case 'addClasificacionMarca':
        	addClasificacionMarca();
            break;
        case 'updClasificacionMarca':
            updClasificacionMarca();
            break;
        case 'dltClasificacionMarca':
            dltClasificacionMarca();
            break;
        case 'getClasificacionMarcaNoAsignada':
            getClasificacionMarcaNoAsignada();
            break;
        case 'getClasificacionMarcaAsignada':
            getClasificacionMarcaAsignada();
            break;
        case 'asignarClasificacion':
            asignarClasificacion();
            break;
        default:
            echo '';
    }

    function getClasificacionMarca(){
    	$lsWhereStr = "WHERE cm.clasificacion = ct.clasificacion ".
                      "AND cm.marca = ct.marca ".
                      "AND ct.idTarifa = tf.idTarifa ";

		if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaClasificacionTxt'], "cm.clasificacion", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
	    if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaMarcaHdn'], "cm.marca", 2);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
         if ($gb_error_filtro == 0)
        {
            $lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaSubMarcaHdn'], "cm.submarca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
	    if ($gb_error_filtro == 0)
	   	{
    		$lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaDescripcionTxt'], "cm.descripcion", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaTipoTarifaCmb'], "tf.tipoTarifa", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaIdTarifaCmb'], "tf.idTarifa", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }


	    $sqlGetClasificacionMarcaStr = "SELECT cm.*, ct.idTarifa, tf.tarifa, tf.tipoTarifa, tf.descripcion as descTarifa, concat(cm.marca,' - ',cm.descripcion) as mdescripcion, ".
                                       "(SELECT distinct(mu.descripcion) FROM caMarcasUnidadesTbl  mu WHERE mu.marca=cm.marca) AS nombreMarca, concat(cm.clasificacion,' - ', cm.descripcion) as desClasificacion, ".
                                       "(SELECT ge.nombre FROM caGeneralesTbl ge WHERE ge.tabla ='caClasificacionMarcaTbl' AND ge.columna = cm.marca AND ge.valor = cm.subMarca) AS nombreSubMarca ".
                                       "FROM caClasificacionMarcaTbl cm, caClasificacionTarifasTbl ct, caTarifasTbl tf " . $lsWhereStr.
                                       " ORDER BY clasificacion, marca, idTarifa, tipoTarifa";

        $rs = fn_ejecuta_query($sqlGetClasificacionMarcaStr);

        for($i= 0; $i < sizeof($rs['root']); $i++){
            $rs['root'][$i]['descTipoTarifa'] = $rs['root'][$i]['tipoTarifa']." - ".$rs['root'][$i]['descTarifa'];
            $rs['root'][$i]['descSubMarca'] = $rs['root'][$i]['subMarca']." - ".$rs['root'][$i]['nombreSubMarca'];
        }

		echo json_encode($rs);
    }

    function getClasificacionesGroup(){
        $lsWhereStr = "";

        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaClasificacionTxt'], "cm.clasificacion", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct("'".$_REQUEST['catClasificacionMarcaMarcaHdn']."'", "cm.marca", 0);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0)
        {
            $lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaDescripcionTxt'], "cm.descripcion", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0)
        {
            $lsCondicionStr = fn_construct($_REQUEST['catClasificacionMarcaSubMarcaHdn'], "cm.submarca", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetClasificacionMarcaStr = "SELECT cm.clasificacion, cm.descripcion ".
                                       "FROM caClasificacionMarcaTbl cm " . $lsWhereStr.
                                       "GROUP BY cm.clasificacion ";

        $rs = fn_ejecuta_query($sqlGetClasificacionMarcaStr);

        for($i= 0; $i < sizeof($rs['root']); $i++){
            $rs['root'][$i]['descSubMarca'] = $rs['root'][$i]['clasificacion']." - ".$rs['root'][$i]['descripcion'];
        }

        echo json_encode($rs);
    }

    function addClasificacionMarca(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catClasificacionMarcaClasificacionTxt'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaClasificacionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catClasificacionMarcaIdTarifaCmb'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaIdTarifaCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catClasificacionMarcaTipoTarifaCmb'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaTipoTarifaCmb','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catClasificacionMarcaMarcaHdn'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaMarcaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catClasificacionMarcaDescripcionTxt'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaDescripcionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catClasificacionMarcaSubMarcaHdn'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaSubMarcaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if ($a['success'] == true) {

            $marcaClasificacion = $_REQUEST['catClasificacionMarcaClasificacionTxt'];
            $idTarifa = $_REQUEST['catClasificacionMarcaIdTarifaCmb'];
            $marca = $_REQUEST['catClasificacionMarcaMarcaHdn'];

            $sqlAddClasificacionMarcaStr = "INSERT INTO caClasificacionMarcaTbl(clasificacion, marca, descripcion, subMarca) ".
                                       "VALUES(".
                                       "'".$_REQUEST['catClasificacionMarcaClasificacionTxt']."', ".
                                       "'".$_REQUEST['catClasificacionMarcaMarcaHdn']."', ".
                                       "'".$_REQUEST['catClasificacionMarcaDescripcionTxt']."', ".
                                       "'".$_REQUEST['catClasificacionMarcaSubMarcaHdn']."')";

            $rs = fn_ejecuta_Add($sqlAddClasificacionMarcaStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {


                if($_REQUEST['catClasificacionMarcaTarifaActionHdn'] == 'true'){//Si ya tiene Tarifa la actualiza

                    $sqlClasificacionTarifa =  "UPDATE caclasificaciontarifastbl SET ".
                                               "idTarifa = ".$idTarifa." ".
                                               "WHERE clasificacion = '".$marcaClasificacion."' ".
                                               "AND marca = '".$marca."' ".
                                               "AND idTarifa = ".$_REQUEST['catClasificacionMarcaIdTarifaOldHdn'];
                }
                else{//Si no tiene Tarifa la agrega
                    $sqlClasificacionTarifa =  "INSERT INTO caClasificacionTarifasTbl VALUES('".
                                                 $marcaClasificacion."', ".$idTarifa.", '".$marca."')";
                }

                $rs = fn_ejecuta_Add($sqlClasificacionTarifa);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlClasificacionTarifa;
                    $a['successMessage'] = getClasificacionMarcaSuccessMsg();
                }else{
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlClasificacionTarifa;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlClasificacionTarifa;
            }

        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function updClasificacionMarca(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if($_REQUEST['catClasificacionMarcaMarcaHdn'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaMarcaHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catClasificacionMarcaClasificacionTxt'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaClasificacionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catClasificacionMarcaDescripcionTxt'] == ""){
            $e[] = array('id'=>'catClasificacionMarcaDescripcionTxt','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if($_REQUEST['catClasificacionMarcaTarifaActionHdn'] == ""){//Campo que me dice si se agrega una clasificacionTarifa
            $e[] = array('id'=>'catClasificacionMarcaTarifaActionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        /*if($_REQUEST['catClasificacionMarcaIdTarifaOldHdn'] == ""){//Valor anterior de idTarifa
            $e[] = array('id'=>'catClasificacionMarcaIdTarifaOldHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }*/

        if ($a['success'] == true) {

            $sqlUpdClasificacionMarcaStr = "UPDATE caClasificacionMarcaTbl ".
                                           "SET descripcion='".$_REQUEST['catClasificacionMarcaDescripcionTxt']."', ".
                                           "subMarca = '".$_REQUEST['catClasificacionMarcaSubMarcaHdn']."' ".
                                           "WHERE marca='".$_REQUEST['catClasificacionMarcaMarcaHdn']."' ".
                                           "AND clasificacion ='".$_REQUEST['catClasificacionMarcaClasificacionTxt']."'";

            $rs = fn_ejecuta_Upd($sqlUpdClasificacionMarcaStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){

                $a['sql'] = $sqlUpdClasificacionTarifa;
                $a['successMessage'] = getClasificacionMarcaUpdateMsg();

                if($_REQUEST['catClasificacionMarcaIdTarifaOldHdn'] != ''){//Si ya tiene Tarifa la actualiza

                    $sqlClasificacionTarifa =  "UPDATE caclasificaciontarifastbl SET ".
                                               "idTarifa = ".$_REQUEST['catClasificacionMarcaIdTarifaCmb']." ".
                                               "WHERE clasificacion = '".$_REQUEST['catClasificacionMarcaClasificacionTxt']."' ".
                                               "AND marca = '".$_REQUEST['catClasificacionMarcaMarcaHdn']."' ".
                                               "AND idTarifa = ".$_REQUEST['catClasificacionMarcaIdTarifaOldHdn'];

                    $rs = fn_ejecuta_Upd($sqlClasificacionTarifa);
                }
                else{//Si no tiene Tarifa la agrega
                    $sqlClasificacionTarifa =  "INSERT INTO caClasificacionTarifasTbl VALUES('".
                                                 $_REQUEST['catClasificacionMarcaClasificacionTxt']."', ".
                                                 $_REQUEST['catClasificacionMarcaIdTarifaCmb'].", '".
                                                 $_REQUEST['catClasificacionMarcaMarcaHdn']."')";

                    $rs = fn_ejecuta_Add($sqlClasificacionTarifa);
                }


                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $a['sql'] = $sqlClasificacionTarifa;
                    $a['successMessage2'] = getClasificacionMarcaSuccessMsg();
                }else{
                    $a['sqlErrorNumber'] = mysql_errno();
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlClasificacionTarifa;
                }
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlUpdClasificacionMarcaStr;
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function dltClasificacionMarca(){
        $a = array();
        $e = array();
        $a['success'] = true;

        $sqlDeleteClasificacionMarcaStr = "DELETE FROM caClasificacionMarcaTbl ".
                                          "WHERE clasificacion='".$_REQUEST['catClasificacionMarcaClasificacionTxt']."' ".
                                          "AND marca='".$_REQUEST['catClasificacionMarcaMarcaHdn']."'";

        $rs = fn_ejecuta_query($sqlDeleteClasificacionMarcaStr);

        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
            $a['sql'] = $sqlDeleteClasificacionMarcaStr;
            $a['successMessage'] = getClasificacionMarcaDeleteMsg();
            $a['id'] = $_REQUEST['catClasificacionMarcaClasificacionTxt'];
        } else {
            $a['success'] = false;
            $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlDeleteClasificacionMarcaStr;
        }

        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }

    function getClasificacionMarcaNoAsignada(){
        $sqlGetNoAsignadaStr = "SELECT m.* FROM caclasificacionmarcatbl m ".
                                "WHERE m.clasificacion NOT IN ".
                                "(SELECT ct.clasificacion  ".
                                "FROM caclasificaciontarifastbl ct, catarifastbl c ".
                                "WHERE ct.idTarifa = c.idTarifa ".
                                "AND c.idTarifa = '".$_REQUEST['catTarifasActionHdn']."')";
        $rs = fn_ejecuta_query($sqlGetNoAsignadaStr);
        echo json_encode($rs);

    }

    function getClasificacionMarcaAsignada(){
        $sqlGetAsignadaStr = "SELECT ct.clasificacion, ct.marca ".
                                  "FROM caclasificaciontarifastbl ct, catarifastbl c ".
                                  "WHERE ct.idTarifa = c.idTarifa ".
                                  "AND c.idTarifa = '".$_REQUEST['catTarifasActionHdn']."'";

                                  echo $sqlGetAsignadasStr;
        $rs = fn_ejecuta_query($sqlGetAsignadaStr);
        echo json_encode($rs);
    }

function asignarClasificacion(){
        $a = array();
        $e = array();
        $a['success'] = true;

        if ($_REQUEST['catTarifasActionHdn'] == "") {
            $e[] = array('id'=>'catTarifasActionHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true) {
            $sqlBorrasAsignacionesStr = "DELETE FROM caclasificaciontarifastbl ".
                                        "WHERE idTarifa = '".$_REQUEST['catTarifasActionHdn']."'";

            $rs = fn_ejecuta_query($sqlBorrasAsignacionesStr);
        }

        if ($a['success'] == true) {
            if (($_REQUEST['catClasificacionTarifaAsignadasHdn'] != "|") && ($_REQUEST['catClasificacionTarifaClasificacionAsignadaHdn'] != "|")) {
                $marcasAsignadas = explode('|', substr($_REQUEST['catClasificacionTarifaAsignadasHdn'], 0, -1));
                $clasificacionAsignadas = explode('|', substr($_REQUEST['catClasificacionTarifaClasificacionAsignadaHdn'], 0, -1));
                $sqlAsignarMarcaStr = "INSERT INTO caclasificaciontarifastbl VALUES";

                for ($i = 0; $i<sizeof($marcasAsignadas);$i++) {
                    if($i != 0){
                        $sqlAsignarMarcaStr .= ",";
                    }
                    $sqlAsignarMarcaStr .= "('".$clasificacionAsignadas[$i]."','".$_REQUEST['catTarifasActionHdn']."','".$marcasAsignadas[$i]."')";

                }

                $rs = fn_ejecuta_Add($sqlAsignarMarcaStr);

                if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                    $a['sql'] = $sqlAsignarMarcaStr;
                    $a['successMessage'] = getAsignarMarcaSuccess();
                    $a['id'] = $_REQUEST['catTarifasActionHdn'];;
                } else {
                    $a['success'] = false;
                    $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAsignarMarcaStr;
                }
            } else {
                $a['sql'] = $sqlAsignarMarcaStr;
                $a['successMessage'] = getAsignarMarcaSuccess();
            }
        }
        $a['errors'] = $e;
        $a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
    }
?>
