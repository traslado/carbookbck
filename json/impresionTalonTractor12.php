<?php
    setlocale(LC_TIME, 'es_MX.utf8');
    $codigoTalonEspecial = 'TE';

    require_once("../funciones/fpdf/fpdf.php");
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    
    if($_REQUEST['esTractor12Hdn'] != ""){
        imprimirTalon12($_REQUEST['trViajesTractoresIdViajeHdn']);
    } else{
        imprimirTalon();
    }
    

    function imprimirTalon(){
        $a = array();
        $e = array();
        $a['success'] = true;
        global $codigoTalonEspecial;

        if($_REQUEST['trViajesTractoresIdViajeHdn'] != ""){
          
            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND vt.idViajeTractor IN (".$_REQUEST['trViajesTractoresIdViajeHdn'].") ".
                                     "AND tv.claveMovimiento != 'TX' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].")"; 
        } else {          
            if ($_REQUEST['trViajesTractoresFoliosHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresFoliosHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }
            if ($_REQUEST['trViajesTractoresCompaniaHdn'] == "") {
                $e[] = array('id'=>'trViajesTractoresCompaniaHdn','msg'=>getRequerido());
                $a['errorMessage'] = getErrorRequeridos();
                $a['success'] = false;
            }

            $sqlGetTalonesViajeStr = "SELECT tv.idTalon ".
                                     "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, catractorestbl tr ".
                                     "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                     "AND vt.idTractor = tr.idTractor ".
                                     "AND tr.compania = '".$_REQUEST['trViajesTractoresCompaniaHdn']."' ".
                                     "AND tv.folio IN (".$_REQUEST['trViajesTractoresFoliosHdn'].") ";
                                     
        }

        if ($a['success']) {

            $pdf = new FPDF('P', 'mm','Letter');

            $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);
            //echo json_encode($sqlGetTalonesViajeStr);
            $varInc = 0;     

            //echo json_encode($rsTalon);

            for ($nInt=1 ; $nInt <= sizeof($rsTalon['root']) ; $nInt++) {   

                $sqlGetTalonStr = "SELECT tv.*, co.descripcion AS nombreCiaRemitente, co.rfc AS rfcRemitente, co.direccion AS dirCompania, dc.descripcionCentro, tr.tractor, ".
                        "vt.claveChofer, ch.*, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist,".
                        "(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$varInc]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                        "(SELECT pl.plaza FROM caPlazasTbl pl ".
                        "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                        "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                        "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                        "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                        "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                        "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".  
                        "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caCompaniasTbl co, caChoferesTbl ch, ".
                        "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                        "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
                        "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                        "AND dc.distribuidorCentro = tv.distribuidor ".
                        //"AND co.compania = tv.companiaRemitente ".
                        "AND co.compania='TCO' ". 
                        "AND tr.idTractor = vt.idTractor ".
                        "AND ch.claveChofer = vt.claveChofer ".
                        /*"AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                        "WHERE dc4.distribuidorCentro = tv.distribuidor) ".*/
                        "AND di.direccion=dc.direccionEntrega ".
                        "AND col.idColonia = di.idColonia ".
                        "AND mu.idMunicipio = col.idMunicipio ".
                        "AND es.idEstado = mu.idEstado ".
                        "AND pa.idPais = es.idPais ".
                        "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                        "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                        "AND tv.idTalon = ".$rsTalon['root'][$varInc]['idTalon']." ".
                        "AND tv.claveMovimiento != 'TX' ";
            
                $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

                if(sizeof($dataTalon['root']) > 0){

                    if($dataTalon['root'][0]['dirCompania'] != ''){

                        $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                        "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                        "caEstadosTbl es, caPaisesTbl pa ".
                                        "WHERE pa.idPais = es.idPais ".
                                        "AND es.idEstado = mu.idEstado ".
                                        "AND mu.idMunicipio = co.idMunicipio ".
                                        "AND co.idColonia = dir.idColonia ".
                                        "AND dir.direccion = ".$dataTalon['root'][0]['dirCompania'];

                        $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                        $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                        $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio']." ".$dirCompania['root'][0]['cp'];
                        $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
                    }

                    $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ".
                                        "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                        "WHERE un.vin = ut.vin ".
                                        "AND su.simboloUnidad = un.simboloUnidad ".
                                        "AND ut.idTalon =".$rsTalon['root'][$varInc]['idTalon']." ".
                                        "AND ut.estatus != 'C' ";

                    $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);


                    if(sizeof($unidadesTalon['root']) > 0){
                
                        $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                        $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                        $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                        $dataTalon['root'][0]['apellidoPaterno']." ".
                        $dataTalon['root'][0]['apellidoMaterno'];     
                        $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                        $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                        $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];

                        if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                            $sqlGetServEspStr = "SELECT de.distribuidorOrigen AS distribuidor, dc.descripcionCentro, de.direccionOrigen AS direccion, ".
                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                    "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                    "WHERE dir.direccion = de.direccionOrigen ".
                                    "AND dc.distribuidorCentro = de.distribuidorOrigen ".
                                    "AND dir.idColonia = co.idColonia ".
                                    "AND co.idMunicipio = mu.idMunicipio ".
                                    "AND mu.idEstado = es.idEstado ".
                                    "ANd es.idPais = pa.idPais ".
                                    "AND pl.idPlaza = dc.idPlaza ".
                                    "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                    "UNION ".
                                    "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                    "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                    "WHERE dir.direccion = de.direccionDestino ".
                                    "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                    "AND dir.idColonia = co.idColonia ".
                                    "AND co.idMunicipio = mu.idMunicipio ".
                                    "AND mu.idEstado = es.idEstado ".
                                    "ANd es.idPais = pa.idPais ".
                                    "AND pl.idPlaza = dc.idPlaza ".
                                    "AND de.vin = '".$unidadesTalon['root'][0]['vin']."'";
                
                            $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);

                            for ($nInt01=0; $nInt01 < sizeof($dataServEsp['root']); $nInt01++) {
                  
                                $dataServEsp['root'][$nInt01]['descDist'] = $dataServEsp['root'][$nInt01]['distribuidor']." - ".
                                $dataServEsp['root'][$nInt01]['descripcionCentro'];

                                $dataServEsp['root'][$nInt01]['dirL1'] = $dataServEsp['root'][$nInt01]['calleNumero'];
                                $dataServEsp['root'][$nInt01]['dirL2'] = $dataServEsp['root'][$nInt01]['colonia'].", ".$dataServEsp['root'][$nInt01]['municipio']." ".$dataServEsp['root'][$nInt01]['cp'];
                                $dataServEsp['root'][$nInt01]['dirL3'] = $dataServEsp['root'][$nInt01]['estado'].", ".$dataServEsp['root'][$nInt01]['pais'];
                            }
                        }
                        $pdf = generarPdf($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);
                    } else {
                        $pdf->AddPage();
                        $pdf->SetY(92+(4*$nInt)+0);
                        $pdf->SetX(25+0);
                        $pdf->Cell(17,3, 'TALON #'.$rsTalon['root'][$varInc]['idTalon']. ' SIN UNIDADES',0, 'C');
                    }
                }
                $varInc ++ ;           
            }
        }
        //Output
        $pdf->Output('../../talon.pdf', 'I');
    }
    function imprimirTalon12($idViaje){
        $a = array();
        $e = array();
        $a['success'] = true;

        //SOLO PARA EL CASO DEL FONDO CON EL TALON ESCANEADO
        $fondoPath = "../img/impTalon.jpg";

        if ($idViaje == "") {
            $e[] = array('id'=>'idViaje','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success']) {
            $pdf = new FPDF('L', 'mm', 'A4');
            #Establecemos los márgenes izquierda, arriba y derecha: 
            $pdf->SetMargins(215, 215 , 215); 

            #Establecemos el margen inferior: 
            $pdf->SetAutoPageBreak(true,215);

            $sqlGetTalonesViajeStr = "SELECT idTalon FROM trTalonesViajesTbl WHERE idViajeTractor = ".$idViaje ;

            $rsTalon = fn_ejecuta_query($sqlGetTalonesViajeStr);
            
            for ($nInt=0; $nInt < sizeof($rsTalon['root']); $nInt++) { 
                /*
                $sqlGetTalonStr =  "SELECT   tv.*, dc.descripcionCentro AS nombreCiaRemitente, dc.rfc AS rfcRemitente, ".
                "dc.direccionFiscal AS dirCompania, dc.descripcionCentro, tr.tractor, ".
                "vt.claveChofer, ch.*, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, ".
                "kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist,(SELECT sum(su.pesoAproximado) ".
                "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                "WHERE un.vin = ut.vin AND su.simboloUnidad = ".
                "un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                "(SELECT pl.plaza FROM caPlazasTbl pl WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                "(SELECT pl2.plaza FROM ".
                "caPlazasTbl pl2 WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".   
                "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".
                "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caChoferesTbl ch, caDistribuidoresCentrosTbl dc, ".
                "caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, caMunicipiosTbl mu, ".
                "caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
                "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                "AND dc.distribuidorCentro = tv.distribuidor  ".
                "AND dc.distribuidorCentro = tv.companiaRemitente ".
                "AND tr.idTractor = vt.idTractor ".
                "AND ch.claveChofer = vt.claveChofer ".
                "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                "AND col.idColonia = di.idColonia ".
                "AND mu.idMunicipio = col.idMunicipio ". 
                "AND es.idEstado = mu.idEstado ".
                "AND pa.idPais = es.idPais ".
                "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                "AND kp.idPlazaDestino = tv.idPlazaDestino ". 
                "AND tv.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." ".
                "AND tv.claveMovimiento != 'TX' limit 1 " ;
                $dataTalon = fn_ejecuta_query($sqlGetTalonStr);
                */
                $sqlGetTalonStr = "SELECT tv.*, co.descripcion AS nombreCiaRemitente, co.rfc AS rfcRemitente, co.direccion AS dirCompania, dc.descripcionCentro, tr.tractor, ".
                                  "vt.claveChofer, di.*, col.colonia, col.cp, mu.municipio, es.estado, pa.pais, kp.diasEntrega, dc.contacto as contactoDist, dc.telefono as telefonoDist, ".
                                  "(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal, ".
                                  "(SELECT pl.plaza FROM caPlazasTbl pl ".
                                        "WHERE pl.idPlaza = tv.idPlazaOrigen) AS descPlazaOrigen, ".
                                  "(SELECT pl2.plaza FROM caPlazasTbl pl2 ".
                                        "WHERE pl2.idPlaza = tv.idPlazaDestino) AS descPlazaDestino, ".
                                  "(SELECT dc3.rfc FROM caDistribuidoresCentrosTbl dc3 ".
                                        "WHERE dc3.distribuidorCentro = tv.distribuidor) AS rfcDestinatario, ".
                                  "(SELECT co2.descripcion FROM caCompaniasTbl co2 WHERE co2.compania = tr.compania) AS companiaTractor ".  
                                  "FROM trTalonesViajesTbl tv, trviajestractorestbl vt, caCompaniasTbl co, ".
                                    "caDistribuidoresCentrosTbl dc, caTractoresTbl tr, caDireccionesTbl di, caColoniasTbl col, ".
                                    "caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caKilometrosPlazaTbl kp ".
                                  "WHERE tv.idViajeTractor = vt.idViajeTractor ".
                                  "AND dc.distribuidorCentro = tv.distribuidor ".
                                  "AND co.compania = tv.companiaRemitente ".
                                  "AND tr.idTractor = vt.idTractor ".
                                  "AND di.direccion = (SELECT dc4.direccionEntrega FROM caDistribuidoresCentrosTbl dc4 ".
                                  "WHERE dc4.distribuidorCentro = tv.distribuidor) ".
                                  "AND col.idColonia = di.idColonia ".
                                  "AND mu.idMunicipio = col.idMunicipio ".
                                  "AND es.idEstado = mu.idEstado ".
                                  "AND pa.idPais = es.idPais ".
                                  "AND kp.idPlazaOrigen = tv.idPlazaOrigen ".
                                  "AND kp.idPlazaDestino = tv.idPlazaDestino ".
                                  "AND idTalon = ".$rsTalon['root'][$nInt]['idTalon'];

                $dataTalon = fn_ejecuta_query($sqlGetTalonStr);

                if(sizeof($dataTalon['root']) > 0){
                    if($dataTalon['root'][0]['dirCompania'] != ''){
                        $sqlGetDireccionCompaniaStr = "SELECT dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                        "FROM caDireccionesTbl dir, caColoniasTbl co, caMunicipiosTbl mu, ".
                                                        "caEstadosTbl es, caPaisesTbl pa ".
                                                        "WHERE pa.idPais = es.idPais ".
                                                        "AND es.idEstado = mu.idEstado ".
                                                        "AND mu.idMunicipio = co.idMunicipio ".
                                                        "AND co.idColonia = dir.idColonia ".
                                                        "AND dir.direccion = ".$dataTalon['root'][0]['dirCompania'];

                        $dirCompania = fn_ejecuta_query($sqlGetDireccionCompaniaStr);

                        $dirCompania['root'][0]['dirL1Cia'] = $dirCompania['root'][0]['calleNumero'];
                        $dirCompania['root'][0]['dirL2Cia'] = $dirCompania['root'][0]['colonia'].", ".$dirCompania['root'][0]['municipio']." ".$dirCompania['root'][0]['cp'];
                        $dirCompania['root'][0]['dirL3Cia'] = $dirCompania['root'][0]['estado'].", ".$dirCompania['root'][0]['pais'];
                    }

                    $sqlGetUnidadesTalonStr = "SELECT ut.*, un.vin, un.simboloUnidad, su.descripcion AS descSimbolo, su.pesoAproximado as pesoAprox ,(SELECT sum(su.pesoAproximado) FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su WHERE un.vin = ut.vin AND su.simboloUnidad = un.simboloUnidad AND ut.idTalon = ".$rsTalon['root'][$nInt]['idTalon']." AND ut.estatus != 'C' ) as pesoTotal ".
                                                "FROM trUnidadesDetallesTalonesTbl ut, alUnidadesTbl un, caSimbolosUnidadesTbl su ".
                                                "WHERE un.vin = ut.vin ".
                                                "AND su.simboloUnidad = un.simboloUnidad ".
                                                "AND ut.idTalon =".$rsTalon['root'][$nInt]['idTalon']." ".
                                                "AND ut.estatus != 'C' ";

                    $unidadesTalon = fn_ejecuta_query($sqlGetUnidadesTalonStr);                           

                    echo $rSumaPeso[0]['totalPeso'];

                    if(sizeof($unidadesTalon['root']) > 0){
                        $dataTalon['root'][0]['descDistribuidor'] = $dataTalon['root'][0]['distribuidor']." - ".$dataTalon['root'][0]['descripcionCentro'];
                        $dataTalon['root'][0]['remitenteDesc'] = $dataTalon['root'][0]['companiaRemitente']." - ".$dataTalon['root'][0]['nombreCiaRemitente'];
                        $dataTalon['root'][0]['nombreChofer'] = $dataTalon['root'][0]['nombre']." ".
                                                                $dataTalon['root'][0]['apellidoPaterno']." ".
                                                                $dataTalon['root'][0]['apellidoMaterno'];     
                        $dataTalon['root'][0]['dirL1Dest'] = $dataTalon['root'][0]['calleNumero'];
                        $dataTalon['root'][0]['dirL2Dest'] = $dataTalon['root'][0]['colonia'].", ".$dataTalon['root'][0]['municipio']." ".$dataTalon['root'][0]['cp'];
                        $dataTalon['root'][0]['dirL3Dest'] = $dataTalon['root'][0]['estado'].", ".$dataTalon['root'][0]['pais'];

                        if($dataTalon['root'][0]['tipoTalon'] == $codigoTalonEspecial){

                            $sqlGetServEspStr = "SELECT de.distribuidorOrigen AS distribuidor, dc.descripcionCentro, de.direccionOrigen AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                    "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionOrigen ".
                                                "AND dc.distribuidorCentro = de.distribuidorOrigen ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."' ".
                                                "UNION ".
                                                "SELECT de.distribuidorDestino AS distribuidor, dc.descripcionCentro, de.direccionDestino AS direccion, ".
                                                    "dc.rfc, pl.plaza, dir.calleNumero, co.cp, co.colonia, mu.municipio, es.estado, pa.pais ".
                                                "FROM alDestinosEspecialesTbl de, caDistribuidoresCentrosTbl dc, caDireccionesTbl dir, ".
                                                "caColoniasTbl co, caMunicipiosTbl mu, caEstadosTbl es, caPaisesTbl pa, caPlazasTbl pl ".
                                                "WHERE dir.direccion = de.direccionDestino ".
                                                "AND dc.distribuidorCentro = de.distribuidorDestino ".
                                                "AND dir.idColonia = co.idColonia ".
                                                "AND co.idMunicipio = mu.idMunicipio ".
                                                "AND mu.idEstado = es.idEstado ".
                                                "ANd es.idPais = pa.idPais ".
                                                "AND pl.idPlaza = dc.idPlaza ".
                                                "AND de.vin = '".$unidadesTalon['root'][0]['vin']."'";

                            $dataServEsp = fn_ejecuta_query($sqlGetServEspStr);

                            for ($nInt=0; $nInt < sizeof($dataServEsp['root']); $nInt++) { 
                                $dataServEsp['root'][$nInt]['descDist'] = $dataServEsp['root'][$nInt]['distribuidor']." - ".
                                                                            $dataServEsp['root'][$nInt]['descripcionCentro'];

                                $dataServEsp['root'][$nInt]['dirL1'] = $dataServEsp['root'][$nInt]['calleNumero'];
                                $dataServEsp['root'][$nInt]['dirL2'] = $dataServEsp['root'][$nInt]['colonia'].", ".$dataServEsp['root'][$nInt]['municipio']." ".$dataServEsp['root'][$nInt]['cp'];
                                $dataServEsp['root'][$nInt]['dirL3'] = $dataServEsp['root'][$nInt]['estado'].", ".$dataServEsp['root'][$nInt]['pais'];
                            }
                        }
                        //print_r($dirCompania['root'][0]);
                        $pdf = generarPdf($pdf, $fondoPath, $dataTalon['root'][0], $dirCompania['root'][0], $unidadesTalon['root'], $dataServEsp['root']);
                    } else {
                        $a['errorMessage'] = "ESTE TALON NO TIENE UNIDADES";
                        return $a;
                    }
                } else {
                    $a['errorMessage'] = "ERROR AL INTENTAR CARGAR EL TALON ".$rsTalon['root'][$nInt]['idTalon'];
                    return $a;
                }
            }

            echo json_encode($a);
        }
    }

    function generarPdf($pdf, $fondo, $data, $dirCompania, $unidades, $especiales){

        global $codigoTalonEspecial;
        $border = 0;
        $font = 'Courier';
        $offsetX = 0;
        $offsetY = 0;

        $pdf->AddPage();
        $pdf->SetFont($font,'B',8);
        $pdf->SetAutoPageBreak(false);

        //FOLIO DEL TALON
        $pdf->SetY(14+$offsetY);
        $pdf->SetX(168+$offsetX);
        $pdf->Cell(35,5, $data['centroDistribucion']." ".$data['folio'], $border,0, 'L'); 

        ///////Fecha
        date_default_timezone_set('America/Mexico_City');
        //echo date('d-M-Y');
        $fecha = setlocale(LC_TIME,"spanish");
        //echo strftime("%d %B %Y");
        $fecha = explode('-', strftime("%d-%B-%Y"));       
       // $fecha = date("Y-m-d H:i:s" );
       //echo json_encode($fecha);
        $pdf->SetY(20+$offsetY);
        //Dia
        $pdf->SetX(116+$offsetX);
        $pdf->Cell(6,5, $fecha[0], $border,0, 'C'); 
        //Mes
        $pdf->SetX(136+$offsetX);
        $pdf->Cell(20,5, strtoupper($fecha[1]), $border,0, 'C');
        //Año
        $pdf->SetX(186+$offsetX);
        $pdf->Cell(10,5, $fecha[2], $border,0, 'C');
        ////////////

        /////////Origen
        //Cod y Nombre Origen
        $pdf->SetY(24+$offsetY);
        $pdf->SetX(22+$offsetX);
        $pdf->Cell(87,3, $data['descPlazaOrigen'], $border,0, 'L');
        //Remitente
        $pdf->SetY(27+$offsetY);
        $pdf->SetX(22+$offsetX);
        $pdf->Cell(83,3, $data['nombreCiaRemitente'], $border,0, 'L');
        //RFC
        $pdf->SetY(30+$offsetY);
        $pdf->SetX(22+$offsetX);
        $pdf->Cell(30,3, $data['rfcRemitente'], $border,0, 'L');
        //DireccionL1
        $pdf->SetY(33+$offsetY);
        $pdf->SetX(22+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL1Cia'], $border,0, 'L');
        //DireccionL2
        $pdf->SetY(36+$offsetY);
        $pdf->SetX(22+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL2Cia'], $border,0, 'L');
        //DireccionL3
        $pdf->SetY(39+$offsetY);
        $pdf->SetX(22+$offsetX);
        $pdf->Cell(82,3, $dirCompania['dirL3Cia'], $border,0, 'L');

        ////////Destino
        //Cod y Nombre Destino
        $pdf->SetY(25+$offsetY);
        $pdf->SetX(124+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[1]['plaza'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['descPlazaDestino'], $border,0, 'L');
        }
        //Destinatario
        $pdf->SetY(27+$offsetY);
        $pdf->SetX(124+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(79,3, $especiales[1]['descDist'], $border,0, 'L');
        } else {
            $pdf->Cell(79,3, $data['descDistribuidor'], $border,0, 'L');
        }
        //RFC
        $pdf->SetY(30+$offsetY);
        $pdf->SetX(124+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(30,3, $especiales[1]['rfc'], $border,0, 'L');
        } else {
            $pdf->Cell(30,3, $data['rfcDestinatario'], $border,0, 'L');
        }
        //DireccionL1
        $pdf->SetY(33+$offsetY);
        $pdf->SetX(124+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[1]['dirL1'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['dirL1Dest'], $border,0, 'L');
        }
        $pdf->SetY(37+$offsetY);
        $pdf->SetX(124+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[1]['dirL2'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['dirL2Dest'], $border,0, 'L');
        }
        //DireccionL3
        $pdf->SetY(40+$offsetY);
        $pdf->SetX(124+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(82,3, $especiales[1]['dirL3'], $border,0, 'L');
        } else {
            $pdf->Cell(82,3, $data['dirL3Dest'], $border,0, 'L');
        }

        //Se recogera
        $pdf->SetY(44+$offsetY);
        $pdf->SetX(5+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(93,3, $especiales[0]['descDist'], $border, 0, 'C');
            $pdf->SetY(55+$offsetY);
            $pdf->SetX(35+$offsetX);
            $pdf->Cell(93,3, $especiales[0]['dirL1'], $border, 0, 'C');
        } else {
            $pdf->Cell(93,3,"EL MISMO", $border, 0, 'C');
        }
        //Se entregara
        $pdf->SetY(44+$offsetY);
        $pdf->SetX(110+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            $pdf->Cell(93,3, $especiales[1]['dirL1'], $border, 0, 'C');

        } else {
            $pdf->Cell(93,3,"EL MISMO", $border, 0, 'C');
        }

        //Valor Unitario
        $pdf->SetY(48+$offsetY);
        $pdf->SetX(18+$offsetX);
        $pdf->Cell(45,3,"0.00", $border, 0, 'C');
        if($data['tipoTalon'] !== $codigoTalonEspecial){
            //$pdf->Cell(45,3,"0.00", $border, 0, 'C');
        }
        //Valor Declarado
        $pdf->SetY(48+$offsetY);
        $pdf->SetX(88+$offsetX);
        $pdf->Cell(45,3,"N/D", $border, 0, 'C');
        if($data['tipoTalon'] !== $codigoTalonEspecial){
            //$pdf->Cell(45,3,"N/D", $border, 0, 'C');
        }
        //Condiciones de Pago
        $pdf->SetY(48+$offsetY);
        $pdf->SetX(165+$offsetX);
        $pdf->Cell(45,3,"CREDITO", $border, 0, 'C');
        if($data['tipoTalon'] !== $codigoTalonEspecial){
            //$pdf->Cell(45,3,"CREDITO", $border, 0, 'C');
        }

        ///////Unidades
        for ($nInt=0; $nInt < sizeof($unidades); $nInt++) { 
            //Simbolo
            $pdf->SetY(56+(3*$nInt)+$offsetY);
            $pdf->SetX(25+$offsetX);
            $pdf->Cell(17,3, $unidades[$nInt]['simboloUnidad'], $border,0, 'C');
            //vin
            $pdf->SetY(56+(3*$nInt)+$offsetY);
            $pdf->SetX(46+$offsetX);
            $pdf->Cell(22,3, $unidades[$nInt]['vin'], $border,0, 'C');
            //Descripcion Simbolo
            $pdf->SetY(56+(3*$nInt)+$offsetY);
            $pdf->SetX(82+$offsetX);
            $pdf->Cell(72,3, $unidades[$nInt]['descSimbolo'], $border,0, 'L');
            //Peso Aproximado
            $pdf->SetY(56+(3*$nInt)+$offsetY);
            $pdf->SetX(120+$offsetX);
            $pdf->Cell(72,3, $unidades[$nInt]['pesoAprox'], $border,0, 'L'); 
        }
            
        //PESO APROXIMADO 
        $pdf->SetY(73+(4*$nInt)+$offsetY);
        $pdf->SetX(107+$offsetX);
        $pdf->Cell(72,3,"PESO APROXIMADO: ... ",$border,0, 'L'); 

        ////////Importes
        //Flete
        $pdf->SetY(56+$offsetY);
        $pdf->SetX(165+$offsetX);
        $pdf->Cell(26,3,"$.00", $border, 0, 'R');
        //Cargo por Seguro
        $pdf->SetY(60+$offsetY);
        $pdf->SetX(165+$offsetX);
        $pdf->Cell(26,3,"$.00", $border, 0, 'R');
        //SubTotal
        $pdf->SetY(84+$offsetY);
        $pdf->SetX(165+$offsetX);
        $pdf->Cell(26,3,"$.00", $border, 0, 'R');
        //Retencion IVA
        $pdf->SetY(88+$offsetY);
        $pdf->SetX(165+$offsetX);
        $pdf->Cell(26,3,"$.00", $border, 0, 'R');

        /////////Chofer
        //Clave
        $pdf->SetY(97+$offsetY);
        $pdf->SetX(28+$offsetX);
        $pdf->Cell(10,3,$data['claveChofer'], $border, 0, 'L');
        //Nombre
        $pdf->SetY(97+$offsetY);
        $pdf->SetX(22+$offsetX);
        $pdf->Cell(95,3,$data['nombreChofer'], $border, 0, 'C');
        //Tractor
        $pdf->SetY(97+$offsetY);
        $pdf->SetX(124+$offsetX);
        $pdf->Cell(20,3,$data['tractor'], $border, 0, 'C');

        //Importe Total en Letra
        $pdf->SetY(103+$offsetY);
        $pdf->SetX(38+$offsetX);
        $pdf->Cell(100,3,"CERO PESOS  00  M.N.", $border, 0, 'L');

        //Fecha Estimada Entrega
        $pdf->SetY(109+$offsetY);
        $pdf->SetX(70+$offsetX);
        $pdf->Cell(100,3,"FECHA ESTIMADA DE ENTREGA:  ".date("d/m/Y", strtotime(date("m/d/Y").' + '.$data['diasEntrega'].' days')), $border, 0, 'L');            

        ///////Observaciones
        //Hora
        $pdf->SetY(117+$offsetY);
        $pdf->SetX(70+$offsetX);
        if($data['tipoTalon'] == $codigoTalonEspecial){
            //FALTA AGREGAR LO DE PRIMER SERVICIO O LO DE CONTINUIDAD
            $pdf->Cell(130,3,substr($data['centroDistribucion'],0,3)." - ESP:\t"."*HORA:\t".date("H:i:s"), $border, 0, 'L');
        } else {
            $pdf->Cell(130,3,"*HORA:\t".date("H:i:s"), $border, 0, 'L');
        }
        //Compania
        $pdf->SetY(122+$offsetY);
        $pdf->SetX(38+$offsetX);
        $pdf->Cell(130,3,$data['observaciones'], $border, 0, 'L');

        //Peso Total
        $pdf->SetY(117+$offsetY);
        $pdf->SetX(100+$offsetX);
        $pdf->Cell(130,3,"PESO NETO: ".$data['pesoTotal'], $border, 0, 'L');

        //CONTACTO
        $pdf->SetY(119+$offsetY);
        $pdf->SetX(70+$offsetX);
        $pdf->Cell(130,3,"CONTACTO: ".$data['contactoDist'], $border, 0, 'L');

        //telefono
        $pdf->SetY(119+$offsetY);
        $pdf->SetX(140+$offsetX);
        $pdf->Cell(130,3,"Telefono: ".$data['telefonoDist'], $border, 0, 'L');        

        return $pdf;
    }
?>