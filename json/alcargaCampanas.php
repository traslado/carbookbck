<?php
    session_start();
    
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alUnidades.php");
    
    $a = array();
    $a['successTitle'] = "Manejador de Archivos";
    
    if(isset($_FILES)) {
        if($_FILES["alCargaCampanasFld"]["error"] > 0){
            $a['success'] = false;
            $a['message'] = $_FILES["alCargaCampanasFld"]["error"];

        } else {
            $temp_file_name = $_FILES['alCargaCampanasFld']['tmp_name']; 
            $original_file_name = $_FILES['alCargaCampanasFld']['name'];
          
            // Find file extention 
            $ext = explode ('.', $original_file_name); 
            $ext = $ext [count ($ext) - 1]; 
            
            // Remove the extention from the original file name 
            $file_name = str_replace ($ext, '', $original_file_name); 
            
            $new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;

            if (move_uploaded_file($temp_file_name, $new_name)){

                if (!file_exists($new_name)){
                    $a['success'] = false;
                    $a['errorMessage'] = "Error al procesar el archivo " . $new_name;
                } else {
                    $a['success'] = true;
                    $a['successMessage'] = "Archivo Cargado";
                    $a['archivo'] = $file_name . $ext;
                    $a['root'] = leerXLS($new_name);
                }
            } else { 
                $a['success'] = false;
                $a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
            }
        }
    } else {
        $a['success'] = false;
        $a['errorMessage'] = "Error FILES NOT SET";
    }
    
    echo json_encode($a); 

    function leerXLS($inputFileName) {
        /** Error reporting */
        error_reporting(E_ALL);
        
        date_default_timezone_set ("America/Mexico_City");
        
        //  Include PHPExcel_IOFactory
        include '../funciones/Classes/PHPExcel/IOFactory.php';
        
        //$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
        //echo $inputFileName;
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader =
             PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        
        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();

        //Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
        $rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

        
        //-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

        if (ord(strtoupper($highestColumn)) <= 5 && $highestRow <= 5 ) {
            $root[] = array('descripcion' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
            return $root;
        }

        if (ord(strtoupper($highestColumn)) < 2) {
            $root[] = array('descripcion' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
            return $root;
        }

        if (strlen($rowData[0][0]) < 17) {
            $root[] = array('descripcion' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN-Current Status]', 'fail'=>'Y');
            return $root;
        }

        $isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true));

        

    
        
        for($row = 0; $row<sizeof($rowData); $row++){
            //Validar si las unidades estan bien o no
            //Verifico si la longitud de los campos sea la correcta...
            $isValidArr['VIN']['val'] = $rowData[$row][0];
            $isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;


            $errorMsg = '';
            $isTrue = true;

            foreach ($isValidArr as $key => $value) {
                if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
                    $errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
                    $isTrue = false;
                }else {// de lo contrario verifico que exista en la base
                    if ($key != 'Avanzada') {
                        
                        $isValidArr[$key]['exist'] = isFieldInDataBase($key, $isValidArr[$key]['val'],$_REQUEST['alCargaCampanasNombreTxt']);//Funcion que verifica si esta en la base 
                        if (!$isValidArr[$key]['exist']) {
                            $siNo = ($key == 'VIN') ? ' ya ' : ' no ';
                            $errorMsg .= 'El '.$key.$siNo.'Existe!';
                            $isTrue = false; 

                        }
                    }                   
                }
            }

            if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
                //Busco el idTarifa

            
                $today = date("Y-m-d H:i:s");
                $fecha = substr($today,0,10);
                $hora=substr($today,11,8);

                        

                $numVin = $rowData[$row][0];                
                $status = $rowData[$row][1];
                $fechaHold=$rowData[$row][2];
                $nombreCampana=strtoupper($_REQUEST['alCargaCampanasNombreTxt']);
                //$ciaSesVal='CDSAL';
                $ciaSesVal=$_SESSION['usuCompania'];
                $cveStatus = 'UC';

                /*$numNomFac = $rowData[$row][3];
                $numDirEnt = $rowData[$row][4];
                $numFped = $rowData[$row][5];*/


                $addHist = "INSERT INTO alcargacampanastbl (centroDistribucion, nombreCampana, vin,fechaHold, currentStatus, fechaCarga, status) ".
                                        "VALUES('".$ciaSesVal."', ".
                                                "'".$nombreCampana."', ".
                                                "'".$numVin."', ".
                                                "'".$fechaHold."', ".
                                                "'".$status."', ".
                                                "'".$today."', ".                                                                      
                                                "'".$cveStatus."') ";

                    fn_ejecuta_query($addHist);     
                

                if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                    $errorMsg = 'Agregado Correctamente';
                } else {
                    $errorMsg = 'Registro No Agregado';
                }       
            }
            $root[]= array('vin'=>$rowData[$row][0],'current Status'=>$rowData[$row][1],'fecha carga'=>$today,'fecha hold'=>$rowData[$row][2],'descripcion'=>$errorMsg);          

            //echo json_encode($rowData[$row]);
        }
        return $root;   

    }


    function isFieldInDataBase($field, $value,$campana) { //Funcion que checa que un valor exista en la base
        $tabla = 'tabla';
        $columna = 'columna';

        switch(strtoupper($field)) {
            case 'VIN':
                $tabla = 'alcargacampanastbl'; $columna = 'vin'; break;
            
        }
        $sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."' AND nombreCampana='".$campana."'". $AND;
        $rs = fn_ejecuta_query($sqlExist);
        return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
    }




?>