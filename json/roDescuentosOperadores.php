    <?php

        session_start();
        require_once("../funciones/generales.php");
        require_once("../funciones/construct.php");
        require_once("../funciones/utilidades.php");

        switch($_REQUEST['roConceptosDescuentosChoferesHdn']){

            case 'getDescuentosOperadores':
                getDescuentosOperadores();      
            break;
            case 'getConceptosValidos':
                getConceptosValidos();      
            break;
            case 'getTipoConcepto':
                getTipoConcepto();      
            break;
            case 'getEstatus':
                getEstatus();       
            break;
            case 'getDatosGen':
                getDatosGen();
            break;
            case 'addDescuentos':
                addDescuentos();        
            break;
            default:
                echo '';
        }
  
    function getDescuentosOperadores(){

        $sqlDanosUnidad = "SELECT cd.concepto, cd.tipoConcepto, cd.tipoImporte, cd.importe, cd.saldo, cd.estatus, cd.idDescuento, cd.idObservaciones, cd.periodo, 1 AS banObservaciones, ".
                          "IFNULL((SELECT observaciones FROM roObservacionesDescuentosTbl WHERE idObservaciones = cd.idObservaciones),'') AS observaciones ".
                          "FROM roconceptosdescuentoschofertbl cd ".
                          "WHERE cd.claveChofer='".$_REQUEST['claveChoferHdn']."' ".
                          "AND cd.estatus='P' ";

       $rssqlDanosUnidad= fn_ejecuta_query($sqlDanosUnidad);

       echo json_encode($rssqlDanosUnidad);  
     }  

    function getConceptosValidos(){
        $sqlConcepto = "SELECT ge.valor as concepto , CONCAT(ge.valor,'-',co.nombre) as nombreConcepto ".
                        "FROM cageneralestbl ge,caconceptostbl co, caconceptoscentrostbl cc ";
        $lsWhereStr = "WHERE ge.tabla = 'trPolizaGastosTbl' ". 
                        "AND ge.columna = 'deducciones' ". 
                        "AND ge.valor=co.concepto ".
                        "AND co.concepto=cc.concepto ".
                        "AND cc.centroDistribucion='CDTOL' ".
                        "AND co.tipoConcepto='D' ";
        if (isset($_REQUEST['concepto']) && $_REQUEST['concepto'] != '') {
            $lsCondicionStr = fn_construct($_REQUEST['concepto'], "co.concepto", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

       $rsSqlConcepto = fn_ejecuta_query($sqlConcepto.$lsWhereStr);

       echo json_encode($rsSqlConcepto);  
   }

         function getTipoConcepto(){

        $sqlTipoConcepto =  "SELECT valor as tipoConcepto , CONCAT(valor,' - ',nombre) as nombreTipoConcepto ".
                            "FROM cageneralestbl WHERE tabla='roconceptosdescuentoschofertbl' ".
                            "AND columna='tipoConcepto' ";
       $rsSqlTipoConcepto= fn_ejecuta_query($sqlTipoConcepto);

       echo json_encode($rsSqlTipoConcepto);  
     }  

       function getEstatus(){

        $sqlEstatus =  "SELECT valor as estatus , CONCAT(valor,' - ',nombre) as nombreEstatus ".
                            "FROM cageneralestbl WHERE tabla='roconceptosdescuentoschofertbl' ".
                            "AND columna='estatus' ";
       $rsSqlEstatus= fn_ejecuta_query($sqlEstatus);

       echo json_encode($rsSqlEstatus);  
     }

    function getDatosGen() {
        $whereStr = "WHERE tabla = '".$_REQUEST['tabla']."' ".
                    "AND columna = '".$_REQUEST['columna']."'";
        if (isset($_REQUEST['valor']) && $_REQUEST['valor'] != '') {
            $lsCondicionStr = fn_construct($_REQUEST['valor'], "valor", 1);
            $whereStr = fn_concatena_condicion($whereStr, $lsCondicionStr);
        }

        $sqlStr = "SELECT *, CONCAT(valor,' - ',nombre) as descValor ".
                  "FROM cageneralestbl ${whereStr}";
        $genRst = fn_ejecuta_query($sqlStr);

        echo json_encode($genRst);  
    }


    function addDescuentos(){
        $a = array('success' => true, 'msjResponse' => '');

        $json = json_decode($_REQUEST["changes"],true);
        $totRec = sizeof($json);

        for ($i=0; $i < $totRec; $i++) {
            $idDescuento = $json[$i]["idDescuento"];
            $tipoImporte = $json[$i]["tipoImporte"];
            $importe = $json[$i]["importe"];
            $saldo = $json[$i]["saldo"];
            $estatus = $json[$i]["estatus"];
            $periodo = empty($json[$i]["periodo"])?'NULL':$json[$i]["periodo"];
            if ($idDescuento === "") {
                $selectSecuencia = "SELECT IFNULL((SELECT MAX(secuencia) + 1 ".
                                   "FROM roconceptosdescuentoschofertbl ".
                                   "WHERE claveChofer = '".$_REQUEST["claveChofer"]."' ".
                                   "AND concepto = '".$json[$i]["concepto"]."'),1) AS secuencia ";
                $rsSelectSecuencia = fn_ejecuta_sql($selectSecuencia);

                $insertDescuento = "INSERT INTO roconceptosdescuentoschofertbl (claveChofer, concepto, centroDistribucion, secuencia, fechaEvento, pagadoCobro, tipoConcepto, tipoImporte, importe, saldo, estatus, periodo) ".
                        "VALUES ('".$_REQUEST["claveChofer"]."', '".$json[$i]["concepto"]."','".$_REQUEST["ciasesval"]."', '".floatval($rsSelectSecuencia['root'][0]['secuencia'])."', CAST(now() AS date),'C','".$json[$i]["tipoConcepto"]."', '${tipoImporte}', '${importe}', '${saldo}', '${estatus}', ${periodo}) ";
                fn_ejecuta_sql($insertDescuento);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql']         = $insertDescuento;
                    break;
                }
            } else {
                $observaciones = strtoupper($json[$i]["observaciones"]);
                $idUsuario = $_SESSION['idUsuario'];
                $insStr = "INSERT INTO roObservacionesDescuentosTbl (idDescuento, observaciones, parametros, fechaEvento, idUsuario) VALUES ".
                          "(${idDescuento}, '${observaciones}', ".
                          "(SELECT CONCAT(IFNULL(tipoImporte,''),'|',IFNULL(importe,''),'|',IFNULL(saldo,''),'|',IFNULL(estatus,''),'|',IFNULL(periodo,'')) FROM roconceptosdescuentoschofertbl WHERE idDescuento = ${idDescuento} AND estatus = 'P'), ".
                          "NOW(), ${idUsuario})";
                $insRst = fn_ejecuta_sql($insStr);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql']         = $insStr;
                    break;
                } else {
                    $idObs = $insRst['id'];
                    $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                            "SET tipoImporte = '${tipoImporte}', ".
                            "importe = '${importe}', ".
                            "saldo = '${saldo}', ".
                            "estatus = '${estatus}', ".
                            "idObservaciones = ".$idObs.", ".
                            "periodo = ${periodo} ".
                            "WHERE idDescuento = '".$idDescuento."' ".
                            "AND estatus = 'P'";
                    fn_ejecuta_sql($updStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $updStr;
                        break;
                    }
                }
            }
        }
        fn_ejecuta_sql($a['success']);
        if ($a['success']) {
            //$exeproc = new programaexterno();
            //$exeproc->runprogram("proDescuentosPersonales.php");
        }

        echo json_encode($a);
    }
?>