	<?php

		session_start();
		require_once("../funciones/generales.php");
		require_once("../funciones/construct.php");
		require_once("../funciones/utilidades.php");

		switch($_REQUEST['alIngresoUnidadesLzcHdn']){
			case 'addLazaroL1':
				addLazaroL1();
				break;        
		    case 'addLazaroL2':
		        addLazaroL2(); 
		        break;
		    case 'addLazaroL3':
		        addLazaroL3(); 
		        break;
		    case 'getLogin':
		        getLogin();
		       	break;   
		    case 'comboVines':
		        comboVines();
		       	break;  
		    case 'PDI':
		        PDI();
		       	break;   
		     case 'comboVinesL2':
		        comboVinesL2();
		       	break; 
		     case 'comboVinesL3':
		        comboVinesL3();
		       	break; 
		     case 'comboPDI':
		        comboPDI();
		       	break;
		     case 'comboTipo':
		        comboTipo();
		       	break;
		    case 'verificacionEstatus':
		    	verificacionEstatus();    	 
		    	break;
		    case 'addMovimientoLzc':
		    	addMovimientoLzc();
		    	break;
		    case 'addPlataformas':
		    	addPlataformas();
		    	break;
		    case 'addSalidasMadrina':
		    	addSalidasMadrina();
		    	break;
		    case 'UpdPlataformas':
		    	UpdPlataformas();
		    	break;
		    case 'UpdMadrinasIM':
		    	UpdMadrinasIM();
		    	break;
		    case 'delPlataformas':
				delPlataformas();
		    	break;
		    case 'gridPlataformas':
		    	gridPlataformas();
		    	break;
		    case 'gridSalidasMadrina':
		    	gridSalidasMadrina();
		    	break;
		    case 'addMovimientoLzc2':
		    	addMovimientoLzc2();
		    	break;
		    case 'consultaRango':
		    	consultaRango();		
		    	break;
		 	case 'delSalidaMadrina':
		    	delSalidaMadrina();		
		    	break;
		    case 'comboDistribuidor':
		    	comboDistribuidor();		
		    	break;
		    case 'eliminaTlTmp':
		    	eliminaTlTmp();		
		    	break;
		    default:
		        echo '';
		}

		function addLazaroL1(){

			$sqlGetUnidad = "SELECT vin FROM alUltimoDetalleTbl WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ";
			$rsSqlGetUnidad= fn_ejecuta_Add($sqlGetUnidad);


			if($rsSqlGetUnidad['records'] != '0'){
				
				//echo '0|Unidad Ya Existente|';

			}
			else{

				$getConsultaIM = "SELECT modelDesc,cveDisFac ".
								"FROM alinstruccionesmercedestbl ".
								"WHERE VIN = '".$_REQUEST['alIngresoVinHdn']."' ";

				$rsgetConsultaIM = fn_ejecuta_Add($getConsultaIM);

				$disIM = $rsgetConsultaIM['root'][0]['cveDisFac'];
				$simIM = $rsgetConsultaIM['root'][0]['modelDesc'];

				$sqlAddUnidadStr =  "INSERT INTO alUnidadesTbl ".
				                                    "(vin, avanzada, distribuidor, simboloUnidad, color, folioRepuve, descripcionUnidad) ".
				                                    "VALUES(".
				                                    "'".$_REQUEST['alIngresoVinHdn']."',".
				                                    "'".substr($_REQUEST['alIngresoVinHdn'], -8)."',".
				                                    "'".$disIM."',".
				                                    "'".$simIM."',".
				                                    "'PBR',".
				                                    "(SELECT folioRepuve FROM alRepuveTbl WHERE vin = '".$_REQUEST['alIngresoVinHdn']."'),".
				                                    replaceEmptyNull("'LZC02_ingreso01'").")";

		        $rs_01 = fn_ejecuta_Add($sqlAddUnidadStr);


			    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
			                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
			                                "claveChofer, observaciones, usuario, ip) ".
			                                "VALUES(".
			                                "'LZC02',".
			                                "'".$_REQUEST['alIngresoVinHdn']."',".
			                                "NOW(),".
			                                "'L1',".
			                                "'".$disIM."',".
			                                "'1',".
			                                "'LZC02',".
			                                replaceEmptyNull($RQchofer).",".
			                                "'UNIDAD INGRESO L1',".
			                                "'98',".
			                                "'".getClientIP()."')";
			    
			    $rs_02 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);     

			    $sqlAddUpdUltimoDetalleStr = "INSERT INTO alUltimoDetalleTbl ".
			                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
			                                "claveChofer, observaciones, usuario, ip) ".
			                                "VALUES(".
			                                "'LZC02',".
			                                "'".$_REQUEST['alIngresoVinHdn']."',".
			                                "NOW(),".
			                                "'L1',".
			                                "'".$disIM."',".
			                                "'1',".
			                                "'LZC02',".
			                                replaceEmptyNull($RQchofer).",".
			                                "'UNIDAD INGRESO L1',".
			                                "'98',".
			                                "'".getClientIP()."')";
			    
			    $rs_03 = fn_ejecuta_Add($sqlAddUpdUltimoDetalleStr);

			}	     	
			echo json_encode($sqlGetUnidad);
		}

		function addMovimientoLzc(){


		    
			/*$sqlGetUnidad = "SELECT vin FROM alinstruccionesmercedestbl dy ".
								"WHERE dy.cveStatus='DK' ".
								"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ".
								"AND dy.vin in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin ".
								"AND claveMovimiento ='".$_REQUEST['cmAnterior']."')";
			$rsSqlGetUnidad= fn_ejecuta_Add($sqlGetUnidad);*/
			
			$getConsultaIM = "SELECT distribuidor ".
								"FROM alUnidadesTbl ".
								"WHERE 	vin = '".$_REQUEST['alIngresoVinHdn']."' ";

				$rsgetConsultaIM = fn_ejecuta_Add($getConsultaIM);

				//echo json_encode($rsgetConsultaIM);

				$disIM = $rsgetConsultaIM['root'][0]['distribuidor'];
				

			    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
			                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
			                                "claveChofer, observaciones, usuario, ip) ".
			                                "VALUES(".
			                                "'LZC02',".
			                                "'".$_REQUEST['alIngresoVinHdn']."',".
			                                "NOW(),".
			                                "'".$_REQUEST['cMovimiento']."',".
			                                "'".$disIM."',".
			                                "'1',".
			                                "'LZC02',".
			                                replaceEmptyNull($RQchofer).",".
			                                "'UNIDAD INGRESO ".$_REQUEST['cMovimiento']."',".
			                                "'98',".
			                                "'".getClientIP()."')";
			    
			    $rs_01 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);

			    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
			    							"SET fechaEvento = NOW(), ".
			    							"claveMovimiento = '".$_REQUEST['cMovimiento']."', ".
			    							"observaciones= 'UNIDAD INGRESO ".$_REQUEST['cMovimiento']."' ".
			    							"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
			    							"AND claveMovimiento = '".$_REQUEST['cmAnterior']."' ";

			    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);



				echo json_encode($rsgetConsultaIM);  	
		}

		function addPlataformas(){

			$vin1= $_REQUEST['vines'];
			$vin2=trim($vin1);
			$SNespacios = str_replace(' ', '', $vin2);
			$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
			$reemplazar = array("", "", "", "");
			$vin = str_ireplace($buscar,$reemplazar,$SNespacios);

			$cadena = chunk_split($vin, 17,"','");

			$vines = substr($cadena,0,-2);

			$WaybillDate= substr($_REQUEST['WaybillDate'],0,10)." 00:00:00" ;

			$SalidaDate= substr($_REQUEST['fechaSalida'],0,10) ;

		    $sqlselectExistIM="SELECT  vin,cveStatus from alinstruccionesmercedestbl ".
							"WHERE vin IN ('".$vines.") ".
							"AND  cveStatus in ('SO','DH','DK') ".
							"AND  cveLoc='LZC02' ";

		    $rssqlselectExistIM=fn_ejecuta_query($sqlselectExistIM);

		    echo json_encode($rssqlselectExistIM);

				$sqlInsertTmp="INSERT INTO alinstruccionesmercedestmp (cveDisFac,nomFac,cveDisEnt,dirEnt,cveLoc,fEvento,hEvento,fPed, vin, natType,modelDesc,descColor,trimCode,trimDesc,invPrice,fechaMovimiento,currency,especial,cveStatus)  ".
							"SELECT  cveDisFac,nomFac,cveDisEnt,dirEnt,cveLoc,fEvento,hEvento,fPed, vin, natType,modelDesc,'".$SalidaDate."".$_REQUEST['horaSalida']."' as descColor,'".$_REQUEST['plataforma']."' as trimCode, ".
							"'".$_REQUEST['Waybill']."' as trimDesc, 'P' as invPrice, '".$WaybillDate."' as fechaMovimiento, '".$_REQUEST['tipo']."' as currency,  especial,cveStatus from alinstruccionesmercedestbl ".
							"WHERE vin IN('".$vines.") ".
							"AND  cveLoc='LZC02' ".
							"AND  cveStatus in ('TK','TY') ".
					    	"AND trimCode is null ".	
					    	"AND trimDesc is null ".
					    	"AND invPrice is null ".
					    	"AND descColor is null ".
					    	"AND fechaMovimiento is null ".
					    	"AND SUBSTR(nomFac,-1) != 'M' ";
					    	"AND currency is null " ;


					  $primero=fn_ejecuta_Add($sqlInsertTmp);

			}

		function addSalidasMadrina(){

			$vin1= $_REQUEST['vines'];
			$vin2=trim($vin1);
			$SNespacios = str_replace(' ', '', $vin2);
			$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
			$reemplazar = array("", "", "", "");
			$vin = str_ireplace($buscar,$reemplazar,$SNespacios);

			$cadena = chunk_split($vin, 17,"','");

			$vines = substr($cadena,0,-2);

			$SalidaDate= substr($_REQUEST['fechaSalida'],0,10);

			$sqlselectExistIM="SELECT  vin,cveStatus from alinstruccionesmercedestbl ".
							"WHERE vin IN ('".$vines.") ".
							"AND  cveStatus in ('SO','DH','DK') ".
							"AND  cveLoc='LZC02' ";

		    $rssqlselectExistIM=fn_ejecuta_query($sqlselectExistIM);

		     echo json_encode($rssqlselectExistIM);

			

				$sqlInsertTmp="INSERT INTO alinstruccionesmercedestmp (cveDisFac,nomFac,cveDisEnt,dirEnt,cveLoc,fEvento,hEvento,fPed, vin, natType,modelDesc,descColor,trimCode,trimDesc,invPrice,fechaMovimiento,currency,especial,cveStatus)  ".
							"SELECT  cveDisFac,nomFac,cveDisEnt,dirEnt,cveLoc,fEvento,hEvento,fPed, vin, natType,modelDesc,descColor,trimCode, trimDesc, invPrice, '".$SalidaDate."".$_REQUEST['horaSalida']."' as fechaMovimiento,  currency,  especial,cveStatus from alinstruccionesmercedestbl ".
							"WHERE vin IN ('".$vines.") ".
							"AND  cveLoc='LZC02' ".
							"AND  cveStatus in ('TK','TY') ".
							"AND SUBSTR(nomFac,-1) = 'M' ".
					    	"AND fechaMovimiento is null ";

					 fn_ejecuta_query($sqlInsertTmp);
			    

		}

		function UpdPlataformas(){

			$sqlUnidadesPlataforma ="SELECT vin,trimCode,trimDesc,descColor,invPrice,fechaMovimiento,currency from alinstruccionesmercedestmp where cveLoc='LZC02' AND SUBSTR(nomFac,-1) != 'M' "; 

			$RssqlUnidadesPlataforma=fn_ejecuta_query($sqlUnidadesPlataforma);


			if($RssqlUnidadesPlataforma['root'] != null ){


				for ($i=0; $i < sizeof($RssqlUnidadesPlataforma['root']) ; $i++) { 


					$UpdatePlataformas = "UPDATE alinstruccionesmercedestbl ".
										"SET trimCode='".$RssqlUnidadesPlataforma['root'][$i]['trimCode']."', ".
										"trimDesc='".$RssqlUnidadesPlataforma['root'][$i]['trimDesc']."', ".
										"invPrice='".$RssqlUnidadesPlataforma['root'][$i]['invPrice']."' ,".
										"descColor='".$RssqlUnidadesPlataforma['root'][$i]['descColor']."' ,".
										"fechaMovimiento='".$RssqlUnidadesPlataforma['root'][$i]['fechaMovimiento']."' ,".
										"currency='".$RssqlUnidadesPlataforma['root'][$i]['currency']."' ".
										"WHERE vin='".$RssqlUnidadesPlataforma['root'][$i]['vin']."' ".
										"AND cveLoc='LZC02' ".
										"AND cveStatus in ('TK','TY') ";

						$rsUpdatePlataformas=fn_ejecuta_query($UpdatePlataformas);




					$dlvinTmp =  "DELETE FROM alinstruccionesmercedestmp ".
			                        "WHERE vin = '".$RssqlUnidadesPlataforma['root'][$i]['vin']."' ".
			                        "AND cveLoc='LZC02' ".
			                        "AND SUBSTR(nomFac,-1) != 'M' ";

			        $RsdlvinTmp = fn_ejecuta_query($dlvinTmp);   

				}
			}
			else 
			{

				echo json_encode($RssqlUnidadesPlataforma);
			}

		}

		function UpdMadrinasIM(){

			$sqlUnidadesPlataforma ="SELECT vin,fechaMovimiento from alinstruccionesmercedestmp where cveLoc='LZC02' AND SUBSTR(nomFac,-1) = 'M' "; 

			$RssqlUnidadesPlataforma=fn_ejecuta_query($sqlUnidadesPlataforma);


			if($RssqlUnidadesPlataforma['root'] != null ){


				for ($i=0; $i < sizeof($RssqlUnidadesPlataforma['root']) ; $i++) { 


					$UpdatePlataformas = "UPDATE alinstruccionesmercedestbl ".
										"SET fechaMovimiento='".$RssqlUnidadesPlataforma['root'][$i]['fechaMovimiento']."' ".
										"WHERE vin='".$RssqlUnidadesPlataforma['root'][$i]['vin']."' ".
										"AND cveLoc='LZC02' ".
										"AND cveStatus in ('TK','TY') ";

						$rsUpdatePlataformas=fn_ejecuta_query($UpdatePlataformas);



					$dlvinTmp =  "DELETE FROM alinstruccionesmercedestmp ".
			                        "WHERE vin = '".$RssqlUnidadesPlataforma['root'][$i]['vin']."' ".
			                        "AND cveLoc='LZC02' ".
			                        "AND SUBSTR(nomFac,-1) = 'M' ";

			        $RsdlvinTmp = fn_ejecuta_query($dlvinTmp);   

				}
			}
			else 
			{

				echo json_encode($RssqlUnidadesPlataforma);
			}

		}


			function delPlataformas(){

				$deleteTemPlataformas = " DELETE from alinstruccionesmercedestmp where  cveLoc='LZC02' ".
										"AND  cveStatus in ('TK','TY') AND SUBSTR(nomFac,-1) != 'M' ";

					$rsdeleteTemPlataformas=fn_ejecuta_query($deleteTemPlataformas);
		
		}

			function delSalidaMadrina(){

				$deleteTemPlataformas = " DELETE from alinstruccionesmercedestmp where cveLoc='LZC02' ".
										"AND cveStatus in ('TK','TY') AND SUBSTR(nomFac,-1) = 'M' ";

				$rsdeleteTemPlataformas=fn_ejecuta_query($deleteTemPlataformas);
		
		}


function addMovimientoLzc2(){

		
	if (count($_REQUEST['vines']) != 0) {



		$vin1= $_REQUEST['vines'];
		$vin2=trim($vin1);
		$SNespacios = str_replace(' ', '', $vin2);
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$SNespacios);

		$cadena = chunk_split($vin, 17,"','");

		$vines1 = substr($cadena,0,-2);

			
		$selAlinstruccionesMercedes= "SELECT vin,nomFac from alinstruccionesmercedestbl where cveLoc='LZC02' ".
									"AND vin IN('".$vines1.") ";

		$rsSel= fn_ejecuta_query($selAlinstruccionesMercedes);

		if($rsSel['root'] != null ){


			for ($i=0; $i < sizeof($rsSel['root']) ; $i++) { 

				if(substr($rsSel['root'][$i]['nomFac'], -1) != 'M')
				{

					$nomfac=$rsSel['root'][$i]['nomFac']."M";

						 $UpdAlinstruccionesmercedesK =   "UPDATE alinstruccionesmercedestbl ".
			    							"SET cveStatus = 'TK', dirEnt ='".$_REQUEST['alIngresoVinHdn']."', nomFac='".$nomfac."', ".
			    							"cveDisFac='".$_REQUEST['distribuidor']."' , cveDisEnt='".$_REQUEST['distribuidor']."' ".
			    							"WHERE vin IN('".$rsSel['root'][$i]['vin']."') ".
			    							"AND cveLoc='LZC02' ".
			    							"AND cveStatus = 'DK' ";

						  $segundoK=fn_ejecuta_query($UpdAlinstruccionesmercedesK);

						$UpdAlinstruccionesmercedesH =   "UPDATE alinstruccionesmercedestbl ".
			    							"SET cveStatus = 'TY', dirEnt ='".$_REQUEST['alIngresoVinHdn']."', nomFac='".$nomfac."', ".
			    							"cveDisFac='".$_REQUEST['distribuidor']."' , cveDisEnt='".$_REQUEST['distribuidor']."' ".
			    							"WHERE vin IN('".$rsSel['root'][$i]['vin']."') ".
			    							"AND cveLoc='LZC02' ".
			    							"AND cveStatus='DH' ";

						  $segundoH=fn_ejecuta_query($UpdAlinstruccionesmercedesH);
				}
				else
				{

					 $UpdAlinstruccionesmercedesK =   "UPDATE alinstruccionesmercedestbl ".
			    							"SET cveStatus = 'TK', dirEnt ='".$_REQUEST['alIngresoVinHdn']."', ".
			    							"cveDisFac='".$_REQUEST['distribuidor']."' , cveDisEnt='".$_REQUEST['distribuidor']."' ".
			    							"WHERE vin IN('".$rsSel['root'][$i]['vin']."') ".
			    							"AND cveLoc='LZC02' ".
			    							"AND cveStatus ='DK' ";

						$segundoK=fn_ejecuta_query($UpdAlinstruccionesmercedesK);

						 $UpdAlinstruccionesmercedesH =   "UPDATE alinstruccionesmercedestbl ".
			    							"SET cveStatus = 'TY', dirEnt ='".$_REQUEST['alIngresoVinHdn']."', ".
			    							"cveDisFac='".$_REQUEST['distribuidor']."' , cveDisEnt='".$_REQUEST['distribuidor']."' ".
			    							"WHERE vin IN('".$rsSel['root'][$i]['vin']."') ".
			    							"AND cveLoc='LZC02' ".
			    							"AND cveStatus ='DH' ";

						$segundoH=fn_ejecuta_query($UpdAlinstruccionesmercedesH);
				}
			}
		}				   

	}

	else if (count($_REQUEST['vines1']) != 0) {
			
		$vin1= $_REQUEST['vines1'];
		$vin2=trim($vin1);
		$SNespacios = str_replace(' ', '', $vin2);
		$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
		$reemplazar = array("", "", "", "");
		$vin = str_ireplace($buscar,$reemplazar,$SNespacios);

		$cadena = chunk_split($vin, 17,"','");

		$vines2 = substr($cadena,0,-2);


		$selAlinstruccionesMercedes= "SELECT vin,nomFac from alinstruccionesmercedestbl where cveLoc='LZC02' ".
									"AND vin IN('".$vines2.") ";

		$rsSelAlinstruccionesMercedes= fn_ejecuta_query($selAlinstruccionesMercedes);

		if($rsSelAlinstruccionesMercedes['root'] != null ){

			for ($i=0; $i < sizeof($rsSelAlinstruccionesMercedes['root']) ; $i++) { 

				if(substr($rsSelAlinstruccionesMercedes['root'][$i]['nomFac'], -1) == 'M')

				{
					$nomfac=rtrim($rsSelAlinstruccionesMercedes['root'][$i]['nomFac'],'M');


				  $sqlUpdAlinstruccionesmercedesFerroK =   "UPDATE alinstruccionesmercedestbl ".
			    							"SET cveStatus = 'TK', dirEnt='KCSM', nomFac='".$nomfac."' ".
			    							"WHERE vin IN('".$rsSelAlinstruccionesMercedes['root'][$i]['vin']."') ".
			    							"AND cveStatus='DK' " ;

			    	  $primero=fn_ejecuta_query($sqlUpdAlinstruccionesmercedesFerroK);

			    	  $sqlUpdAlinstruccionesmercedesFerroH =   "UPDATE alinstruccionesmercedestbl ".
			    							"SET cveStatus ='TY', dirEnt='KCSM', nomFac='".$nomfac."' ".
			    							"WHERE vin IN('".$rsSelAlinstruccionesMercedes['root'][$i]['vin']."') ".
			    							"AND cveStatus='DH' " ;

			    	  $segundo=fn_ejecuta_query($sqlUpdAlinstruccionesmercedesFerroH);
				}
				else
				{
						 $sqlUpdAlinstruccionesmercedesFerro =   "UPDATE alinstruccionesmercedestbl ".
			    							"SET cveStatus = 'TK', dirEnt='KCSM' ".
			    							"WHERE vin IN('".$rsSelAlinstruccionesMercedes['root'][$i]['vin']."') ".
			    							"AND cveStatus='DK' " ;

			    	  $primero=fn_ejecuta_query($sqlUpdAlinstruccionesmercedesFerro);

			    	  $sqlUpdAlinstruccionesmercedesFerro =   "UPDATE alinstruccionesmercedestbl ".
			    							"SET cveStatus = 'TY', dirEnt='KCSM' ".
			    							"WHERE vin IN('".$rsSelAlinstruccionesMercedes['root'][$i]['vin']."') ".
			    							"AND cveStatus='DH' " ;

			    	  $segundo=fn_ejecuta_query($sqlUpdAlinstruccionesmercedesFerro);

				}

			}
		}
	}
}

	/*function addLazaroL3(){
		    
			$sqlGetUnidad = "SELECT vin FROM alinstruccionesmercedestbl dy ".
								"WHERE dy.cveStatus='DK' ".
								"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ".
								"AND dy.vin in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='PI')";
			$rsSqlGetUnidad= fn_ejecuta_Add($sqlGetUnidad);

			if($rsSqlGetUnidad['records'] == '0'){
				
				echo json_encode($rsSqlGetUnidad);

			}else{


				$getConsultaIM = "SELECT distribuidor ".
								"FROM alUnidadesTbl ".
								"WHERE 	vin = '".$_REQUEST['alIngresoVinHdn']."' ";

				$rsgetConsultaIM = fn_ejecuta_Add($getConsultaIM);

				$disIM = $rsgetConsultaIM['root'][0]['distribuidor'];
				
				
			    $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
			                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
			                                "claveChofer, observaciones, usuario, ip) ".
			                                "VALUES(".
			                                "'LZC02',".
			                                "'".$_REQUEST['alIngresoVinHdn']."',".
			                                "NOW(),".
			                                "'L3',".
			                                "'".$disIM."',".
			                                "'1',".
			                                "'LZC02',".
			                                replaceEmptyNull($RQchofer).",".
			                                "'UNIDAD INGRESO L3',".
			                                "'98',".
			                                "'".getClientIP()."')";
			    
			    $rs_01 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);

			    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
			    							"SET fechaEvento = NOW(), ".
			    							"claveMovimiento = 'L3' , ".
			    							"observaciones= 'UNIDAD INGRESO L3' ".
			    							"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
			    							"AND claveMovimiento = 'L2' ";

			    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);



			echo json_encode($rsSqlGetUnidad);

			}    	
		}*/

		
		function getLogin(){

			$psw = md5($_REQUEST['lzcLoginPwdTxt']);

			$getUsr = "SELECT 1 as valor FROM segusuariostbl ".
						"WHERE usuario ='".$_REQUEST['lzcLoginUsrTxt']."' ".
						"AND password ='".$psw."'";
			$rsUsr =fn_ejecuta_query($getUsr);
			echo json_encode($rsUsr);	   

		}

		function comboVines(){
			$sqlInstrucciones ="SELECT concat(dy.natType,"."  '  vin: ' " .",dy.vin) as descripcion, vin   FROM alinstruccionesmercedestbl dy ".
								"WHERE vin not in (SELECT ud.vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin)  ".
								"AND (dy.cveStatus='DK' OR dy.cveStatus='DH') ";

			$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
			echo json_encode($rsInstrucciones);
		}


		function comboDistribuidor(){
			
			$sqlInstrucciones ="SELECT  distribuidorCentro as distribuidor  FROM cadistribuidorescentrostbl  ".
								"WHERE tipoDistribuidor != 'DX' ";

			$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);

			echo json_encode($rsInstrucciones);
		}


		function comboVinesL2(){
			$sqlInstrucciones ="SELECT concat(dy.natType,"."  '  vin: ' " .",dy.vin) as descripcion, vin   FROM alinstruccionesmercedestbl dy ".
								"WHERE vin in (SELECT ud.vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin and ud.claveMovimiento = 'L1') ".
								"AND (dy.cveStatus='DK' OR dy.cveStatus='DH') ";
			$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
			echo json_encode($rsInstrucciones);
		}

		function comboVinesL3(){
			$sqlInstrucciones = "SELECT concat(compania,"."  ' - ' " .",descripcion) as descripcion, compania   FROM cacompaniastbl ".
								"WHERE tipoCompania='E' ";
		
			$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
			echo json_encode($rsInstrucciones);
		}
		
		function comboPDI(){
			$sqlInstrucciones ="SELECT concat(dy.natType,"."  '  vin: ' " .",dy.vin) as descripcion, vin   FROM alinstruccionesmercedestbl dy ".
								"WHERE dy.vin in (SELECT ud.vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='L2') ".
								"AND (dy.cveStatus='DK' OR dy.cveStatus='DH') ";
			$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
			echo json_encode($rsInstrucciones);
		}
		
		function comboTipo(){

			$sqlInstrucciones = "SELECT  nombre,valor  FROM cageneralestbl ".
								"WHERE tabla='alinstruccionesmercedestbl' and columna='currency' and estatus='1' ";
			
			$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
			echo json_encode($rsInstrucciones);
		}

			function gridSalidasMadrina(){

			$sqlInstrucciones = " SELECT  tmp.vin, tmp.dirEnt as transportista,cveDisFac as distribuidor, tmp.fechaMovimiento as fechaSalida ".
								"FROM alinstruccionesmercedestmp tmp ".
								"WHERE tmp.cveLoc='LZC02' ".
								"AND SUBSTR(tmp.nomFac,-1) = 'M' "; 
										
			$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
			echo json_encode($rsInstrucciones);
		}

	

	function gridPlataformas(){

			$sqlInstrucciones = " SELECT  tmp.Vin, tmp.trimCode as NoPlataforma, (SELECT  ca.nombre  FROM cageneralestbl ca ".
								"WHERE ca.tabla='alinstruccionesmercedestbl' and ca.columna='currency' and ca.estatus='1' ".
								"and tmp.currency = ca.valor) as Tipo, ".
								"tmp.trimDesc as NoWaybill, SUBSTR(tmp.fechaMovimiento,1,10) as FechaWaybill, tmp.descColor as fechaSalida ".
								"FROM alinstruccionesmercedestmp tmp ".
								"WHERE tmp.cveLoc='LZC02' ".
								"AND SUBSTR(tmp.nomFac,-1) != 'M' "; 
										
			$rsInstrucciones = fn_ejecuta_query($sqlInstrucciones);
			echo json_encode($rsInstrucciones);
		}

	   function eliminaTlTmp(){

	        $dlvinTmp =  "DELETE FROM alinstruccionesmercedestmp ".
	                        "WHERE vin = '".$_REQUEST['vin']."' ".
	                        "AND cveLoc='LZC02' ";

	        $RsdlvinTmp = fn_ejecuta_query($dlvinTmp);      
	    }


		/*function PDI(){
			$sqlGetUnidad = "SELECT vin FROM alinstruccionesmercedestbl dy ".
								"WHERE dy.cveStatus='DK' ".
								"AND dy.vin ='".$_REQUEST['alIngresoVinHdn']."' ".
								"AND dy.vin in (SELECT vin FROM alultimodetalletbl ud WHERE dy.vin = ud.vin AND claveMovimiento ='L2')";
			$rsSqlGetUnidad= fn_ejecuta_Add($sqlGetUnidad);

			if($rsSqlGetUnidad['records'] == '0'){
				
				echo json_encode($rsSqlGetUnidad);

			}else{
				 $sqlAddPDI = "INSERT INTO alHistoricoUnidadesTbl ".
				                                "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
				                                "claveChofer, observaciones, usuario, ip) ".
				                                "VALUES(".
				                                "'LZC02',".
				                                "'".$_REQUEST['alIngresoVinHdn']."',".
				                                "NOW(),".
				                                "'PI',".
				                                "'DIPRU',".
				                                "'1',".
				                                "'LZC02',".
				                                replaceEmptyNull($RQchofer).",".
				                                "'UNIDAD INGRESO PDI',".
				                                "'98',".
				                                "'".getClientIP()."')";
				    
				$rs = fn_ejecuta_Add($sqlAddPDI);


				 $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
			    							"SET fechaEvento = NOW(), ".
			    							"claveMovimiento = 'PI', ".
			    							"observaciones= 'UNIDAD INGRESO PDI' ".
			    							"WHERE vin = '".$_REQUEST['alIngresoVinHdn']."' ".
			    							"AND claveMovimiento = 'L2' ";

			    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);



				echo json_encode($rsSqlGetUnidad);

			}
		}*/

		function verificacionEstatus(){
		    
			$vin1= $_REQUEST['vines'];
			$vin2=trim($vin1);
			$SNespacios = str_replace(' ', '', $vin2);
			$buscar = array(chr(13).chr(10), "\r\n", "\n", "\r");
			$reemplazar = array("", "", "", "");
			$vin = str_ireplace($buscar,$reemplazar,$SNespacios);

			$cadena = chunk_split($vin, 17,"','");

			$vines = substr($cadena,0,-2);


		    $sqlUpdUltimoDetalleStr =   "UPDATE alUltimoDetalleTbl ".
		    							"SET fechaEvento = NOW(), ".
		    							"claveMovimiento = '".$_REQUEST['cMovimiento']."', ".
		    							"observaciones= 'UNIDAD INGRESO ".$_REQUEST['cMovimiento']."' ".
		    							"WHERE vin IN('".$vines.") ". 
		    							"AND claveMovimiento = '".$_REQUEST['cmAnterior']."' ".
		    							"AND vin not in (SELECT vin from alHistoricoUnidadesTbl where claveMovimiento='".$_REQUEST['cMovimiento']."') ";

		    $rs_02 = fn_ejecuta_Add($sqlUpdUltimoDetalleStr);
		    

		    $sqlAddCambioHistoricoStr = "INSERT INTO alhistoricounidadestbl (centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip) ".
										"SELECT centroDistribucion, vin, NOW() as fechaEvento,'".$_REQUEST['cMovimiento']."' as claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer,'UNIDAD INGRESO ".$_REQUEST['cMovimiento']."' as observaciones, usuario, ip ".
										"FROM alhistoricounidadestbl ".
										"WHERE centroDistribucion = 'LZC02' ".
										"AND claveMovimiento = '".$_REQUEST['cmAnterior']."' ".
										"AND vin IN('".$vines.")".
										"AND vin not in (SELECT vin from alHistoricoUnidadesTbl where claveMovimiento='".$_REQUEST['cMovimiento']."') ";
		    
		    $rs_01 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);
		    


			echo json_encode($sqlUpdUltimoDetalleStr);		
	  
			//echo json_encode($vines);

		}
		function consultaRango(){
			$sqlConsulta = "SELECT vin FROM alinstruccionesmercedestbl ".
	  						"WHERE nomFac IN(".$_REQUEST['reImpresionEtiquetasLazaroLocTxt'].") ";
		    
		    $rsSqlConsulta = fn_ejecuta_query($sqlConsulta);

			echo json_encode($rsSqlConsulta);  						
		}	
	?>