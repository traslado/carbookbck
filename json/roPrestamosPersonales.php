<?php
    session_start();
    date_default_timezone_set('America/Mexico_City');
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    switch ($_REQUEST['ActionHdn']) {
        case 'getCatChoferes':
            getCatChoferes();
            break;
        case 'getPrestamosPersonales':
            getPrestamosPersonales();
            break;
        case 'updPrestamosPersonales':
            updPrestamosPersonales();
            break;
        default:
            # code...
            break;
    }

    function getCatChoferes() {
        $selChoferesStr = "SELECT ch.*, '' AS nombreChofer ".
                          "FROM caChoferesTbl ch, roconceptosdescuentoschofertbl cd ".
                          "WHERE ch.claveChofer = cd.claveChofer ".
                          "AND cd.concepto = '2228' ".
                          "AND cd.tipoConcepto = 'A' ".
                          "AND cd.estatus = 'P'";
        $Rst = fn_ejecuta_query($selChoferesStr);

        for ($iInt=0; $iInt < $Rst['records']; $iInt++) {
            $Rst['root'][$iInt]['nombreChofer'] = $Rst['root'][$iInt]['claveChofer']." - ".
                                                  $Rst['root'][$iInt]['nombre']." ".
                                                  $Rst['root'][$iInt]['apellidoPaterno']." ".
                                                  $Rst['root'][$iInt]['apellidoMaterno'];
        }

        echo json_encode($Rst);
    }

    function getPrestamosPersonales() {
        $a = array('success' => true, 'root' => array(), 'records' => 0, 'existeConcepto' => 0);

        $selStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                  "WHERE claveChofer = '".$_REQUEST['claveChofer']."' ".
                  "AND concepto = '2228' ".
                  "AND tipoConcepto = 'A' ".
                  "AND estatus = 'P'";
        $recRst = fn_ejecuta_query($selStr);

        if ($recRst['records'] > 0) {
            $a['existeConcepto'] = 1;
            $a['idDescuento'] = $recRst['root'][0]['idDescuento'];

            $selStr = "SELECT a.* ".
                      "FROM roPrestamosPersonalesTbl a ".
                      "WHERE a.idDescuento = ".$a['idDescuento']." ".
                      "AND a.idEstatus IN (0,1) ".
                      "ORDER BY a.secuencia ASC";
            $Rst = fn_ejecuta_query($selStr);
            $a['root'] = $Rst['root'];
            $a['records'] = $Rst['records'];
        }

        echo json_encode($a);
    }

    function updPrestamosPersonales() {
        $a = array('success'=>true,'msjResponse'=>'');

        $arrRemove = json_decode($_POST['arrRemove'],true);
        $totRemove = sizeof($arrRemove);
        $arrDetalle = json_decode($_POST['arrDetalle'],true);
        $totDetalle = sizeof($arrDetalle);

        for ($i=0; $i < $totDetalle; $i++) {
            if (empty($arrDetalle[$i]['idPrestamo'])) {
                $arrDetalle[$i]['idPrestamo'] = '99999999999';
            }
        }
        foreach($arrDetalle as $clave => $rec){
            $idEstatusArr[$clave] = $rec['idEstatus'];
            $idPrestamoArr[$clave] = $rec['idPrestamo'];
        }
        array_multisort($idEstatusArr,SORT_DESC,$idPrestamoArr,SORT_ASC,$arrDetalle);

        for ($i=0; $i < $totRemove; $i++) {
            $delStr = "DELETE FROM roPrestamosPersonalesTbl ".
                      "WHERE idPrestamo = ".$arrRemove[$i]['idPrestamo']." ".
                      "AND idEstatus = '0'";
            fn_ejecuta_sql($delStr);
            if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                $a['success']     = false;
                $a['msjResponse'] = $_SESSION['error_sql'];
                $a['sql_error']   = $delStr;
                break;
            }
        }

        $secuencia = 1;
        for ($i=0; $i < $totDetalle; $i++) {
            $row = $arrDetalle[$i];
            $idEstatus = '0';
            if ($row['idEstatus'] == '0') {
                // $ld_fechaActual = date('Y-m-d');
                // if (strtotime($row['fechaInicio']) == strtotime($ld_fechaActual)) {
                //     $idEstatus = '1';
                // }
                if ($row['idPrestamo'] != '99999999999') {
                    $updStr = "UPDATE roPrestamosPersonalesTbl ".
                              "SET secuencia = ".$secuencia.", ".
                              "porcentaje = ".$row['porcentaje'].", ".
                              "deudaTotal = ".$row['deudaTotal'].", ".
                              "mesesPago = ".$row['mesesPago'].", ".
                              "periodo = ".$row['periodo'].", ".
                              "pagoPeriodo = ".$row['pagoPeriodo'].", ".
                              "fechaInicio = '".$row['fechaInicio']."', ".
                              "idEstatus = ".$idEstatus.", ".
                              "fechaEvento = NOW(), ".
                              "idUsuario = ".$_SESSION['idUsuario']." ".
                              "WHERE idPrestamo = ".$row['idPrestamo'];
                    fn_ejecuta_sql($updStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql_error']   = $updStr;
                        break;
                    }
                } else {
                    $insStr = "INSERT INTO roPrestamosPersonalesTbl (idDescuento, secuencia, porcentaje, deudaTotal, mesesPago, periodo, pagoPeriodo, fechaInicio, idEstatus, fechaEvento, idUsuario) VALUES (".
                              "".$_REQUEST['idDescuento'].", " .
                              "".$secuencia.", " .
                              "".$row['porcentaje'].", " .
                              "".$row['deudaTotal'].", " .
                              "".$row['mesesPago'].", " .
                              "".$row['periodo'].", " .
                              "".$row['pagoPeriodo'].", " .
                              "'".$row['fechaInicio']."', " .
                              "".$idEstatus.", ".
                              "NOW(), ".
                              "".$_SESSION['idUsuario'].")";
                    fn_ejecuta_sql($insStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql']!="") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql_error']   = $insStr;
                        break;
                    }
                }
            }
            $secuencia++;
        }
        // fn_ejecuta_sql(false);
        fn_ejecuta_sql($a['success']);
        if ($a['success']) {
            $exeproc = new programaexterno();
            $exeproc->runprogram("proDescuentosPersonales.php");
        }

        echo json_encode($a);
    }
?>