<?php
    /**
    * Modif: ~/04/2020
    */
    session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['trPolizaCancelActionHdn']) {
        case 'obtenPolizaInterface':
            obtenPolizaInterface();
            break;
        case 'cancPoliza':
            cancPoliza();
            break;
        default:
            # code...
            break;
    }

    function obtenPolizaInterface() {
        $a = array('success' => true, 'root' => array(), 'msjResponse' => '');

        $fechaPolStr = "";
        $selPendStr = "SELECT DISTINCT CONCAT(co.compania,' - ', co.descripcion) AS compania, tr.tractor, gt.folio, vt.viaje, ch.claveChofer, CONCAT(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) AS operador, tr.rendimiento, ".
                        "vt.numeroUnidades, vt.idViajeTractor, ".
                        "SUBSTRING(gt.fechaEvento,1,10) AS fechaEvento, ".
                        "(SELECT DISTINCT claveMovimiento FROM trPolizaInterfaceTbl pi WHERE pi.folio = gt.folio AND pi.idViajeTractor = gt.idViajeTractor) AS claveMovimiento, 0 AS litros, '' AS concepto ".
                        "FROM trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trgastosviajetractortbl gt, cacompaniastbl co ".
                        "WHERE vt.idViajeTractor = gt.idViajeTractor ".
                        "AND vt.idTractor = tr.idTractor ".
                        "AND vt.claveChofer = ch.claveChofer ".
                        "AND vt.claveMovimiento = 'VP' ".
                        "AND gt.claveMovimiento IN ('GP','GS','GC') ".
                        "AND tr.compania = co.compania ".
                        "AND gt.folio IN (SELECT pi.folio FROM trPolizaInterfaceTbl pi WHERE pi.folio = gt.folio AND pi.idViajeTractor = gt.idViajeTractor) ".
                        "AND ch.claveChofer = '".$_REQUEST['claveChoferHdn']."' ".
                        "ORDER BY gt.fechaEvento DESC LIMIT 1";
        $baseRst = fn_ejecuta_sql($selPendStr);
        if ($baseRst['records'] > 0) {
            $a['recBase'] = $baseRst['root'][0];
            $fechaPolStr = "AND SUBSTRING(gt.fechaEvento,1,10) >= '".$a['recBase']['fechaEvento']."' ";
            $selDieselStr = "SELECT * FROM trCombustiblePendienteTbl ".
                          "WHERE idViajeTractor = ".$a['recBase']['idViajeTractor']." ".
                          "AND claveChofer = ".$_REQUEST['claveChoferHdn']." ".
                          "ORDER BY idCombustible DESC LIMIT 1";
            $combPendRst = fn_ejecuta_sql($selDieselStr);
            if ($combPendRst['records'] > 0) {
                $a['recBase']['litros'] = $combPendRst['root'][0]['litros'];
                $a['recBase']['concepto'] = $combPendRst['root'][0]['concepto'];
                $a['recBase']['estatusMov'] = $combPendRst['root'][0]['estatusMov'];
            }
            if ($a['recBase']['claveMovimiento'] == 'T') {
                $a['recBase']['descMovimiento'] = 'Transmitida a contabilidad.';
            } elseif ($a['recBase']['claveMovimiento'] == 'C') {
                $a['recBase']['descMovimiento'] = 'Pagada.';
            }
        }
        $a['recBase']['litros'] = floatval($a['recBase']['litros']);

        $selStr = "SELECT CONCAT(co.compania,' - ', co.descripcion) AS compania, tr.tractor, gt.folio, gt.centroDistribucion, vt.viaje, ch.claveChofer, CONCAT(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) AS operador, tr.rendimiento, vt.numeroUnidades, vt.idViajeTractor, SUBSTRING(gt.fechaEvento,1,10) AS fechaEvento, ".
                "(SELECT DISTINCT a.claveMovimiento FROM trgastosviajetractortbl a, caGeneralesTbl b WHERE a.concepto = b.valor AND b.tabla = 'comprobacionPoliza' AND b.columna = gt.centroDistribucion AND a.folio = gt.folio AND a.fechaEvento = gt.fechaEvento AND a.centroDistribucion = gt.centroDistribucion) AS claveMovimiento, ".
                "0 AS idCombustible, 0 AS litros, 0 AS litrosPost, '' AS concepto, 0 AS gastosNoComprobados, '' AS complementos, '' AS nodeFol ".
                "FROM trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trgastosviajetractortbl gt, cacompaniastbl co ".
                "WHERE vt.idViajeTractor = gt.idViajeTractor ".
                "AND vt.idTractor = tr.idTractor ".
                "AND vt.claveChofer = ch.claveChofer ".
                "AND vt.claveMovimiento = 'VP' ".
                "AND gt.claveMovimiento IN ('GP','GC') ".
                "AND tr.compania = co.compania ".
                "AND gt.folio NOT IN (SELECT pi.folio FROM trPolizaInterfaceTbl pi WHERE pi.folio = gt.folio AND pi.idViajeTractor = gt.idViajeTractor) ".
                "AND ch.claveChofer = ".$_REQUEST['claveChoferHdn']." ". 
                $fechaPolStr.
                "GROUP BY gt.folio ".
                "ORDER BY gt.folio DESC, gt.claveMovimiento DESC, gt.fechaEvento ASC";
        $polizasRst = fn_ejecuta_sql($selStr);
        if ($polizasRst['records'] > 0) {
            $a['root'] = $polizasRst['root'];
            $lb_idCombustible = false; $li_idCombustible = 0; $idxAux = 0;
            foreach ((array) $polizasRst['root'] as $key => $row) {
                $a['root'][$key]['cancelaChk'] = false;

                if ($idxAux == ($polizasRst['records']-1)) {
                    $selLtsStr = "SELECT * FROM trGastosViajeTractorTbl ".
                                 "WHERE idViajeTractor = ".$row['idViajeTractor']." ".
                                 "AND folio = '".$row['folio']."' ".
                                 "AND concepto IN ('9018','9019') ".
                                 "AND centroDistribucion = '".$row['centroDistribucion']."' ".
                                 "AND claveMovimiento = 'GD'";
                    $combPendRst = fn_ejecuta_sql($selLtsStr);
                    if ($combPendRst['records'] > 0) {
                        $a['recBase']['litros'] = $combPendRst['root'][0]['litros'];
                        $a['recBase']['concepto'] = $combPendRst['root'][0]['concepto'];
                        $a['recBase']['estatusMov'] = $combPendRst['root'][0]['estatusMov'];
                    }
                }

                if ($row['claveMovimiento'] == 'GC') {
                    $a['root'][$key]['tipoPoliza'] = 'C';
                    $selGVTStr = "SELECT * FROM trgastosviajetractortbl ".
                            "WHERE idViajeTractor = ".$row['idViajeTractor']." ".
                            "AND folio = '".$row['folio']."' ".
                            "AND concepto IN ('9012','9013') ".
                            "AND claveMovimiento = 'GC'";
                    $Rst = fn_ejecuta_sql($selGVTStr);
                    if ($Rst['records'] > 0) {
                        $lb_idCombustible = true;
                        $combusPendArr = explode('|', $Rst['root'][0]['observaciones']);
                        $li_idCombustible = $combusPendArr[0];
                        $sqlStr = "SELECT * FROM trCombustiblePendienteTbl " . 
                                  "WHERE idCombustible = " . $combusPendArr[1];
                        $complRst = fn_ejecuta_sql($sqlStr);
                        if ($complRst['records'] > 0) {
                            $a['root'][$key]['idCombustible'] = $complRst['root'][0]['idCombustible'];
                            $a['root'][$key]['litros'] = floatval($complRst['root'][0]['litros']);
                            $a['root'][$key]['litrosPost'] = 0;
                            $a['root'][$key]['concepto'] = $complRst['root'][0]['concepto'];
                            $a['root'][$key]['claveMovimiento'] = $complRst['root'][0]['claveMovimiento'];
                        }
                    }
                } else {
                    $a['root'][$key]['tipoPoliza'] = 'P';
                    $sel144Str = "SELECT * FROM trgastosviajetractortbl ".
                                 "WHERE idViajeTractor = ".$row['idViajeTractor']." ".
                                 "AND folio = '".$row['folio']."' ".
                                 "AND concepto = '144' ".
                                 "AND claveMovimiento = 'GS'";
                    $conc144Rst = fn_ejecuta_sql($sel144Str);
                    if ($conc144Rst['records'] > 0) {
                        $a['root'][$key]['gastosNoComprobados'] = floatval($conc144Rst['root'][0]['importe']);
                    }

                    $whereStr = "";
                    if ($lb_idCombustible) {
                        $whereStr = "WHERE idCombustible = ".$li_idCombustible;
                        $lb_idCombustible = false;
                        $li_idCombustible = 0;
                    } else {
                        $whereStr = "WHERE idViajeTractor = ".$row['idViajeTractor']." ".
                                    "AND claveChofer = ".$_REQUEST['claveChoferHdn']." ".
                                    "ORDER BY idCombustible DESC LIMIT 1";
                    }
                    $sqlStr = "SELECT * FROM trCombustiblePendienteTbl ".$whereStr;
                    $complRst = fn_ejecuta_sql($sqlStr);
                    if ($complRst['records'] > 0) {
                        $a['root'][$key]['idCombustible'] = $complRst['root'][0]['idCombustible'];
                        $a['root'][$key]['litros'] = floatval($complRst['root'][0]['litros']);
                        $a['root'][$key]['litrosPost'] = 0;
                        $a['root'][$key]['concepto'] = $complRst['root'][0]['concepto'];
                        $a['root'][$key]['claveMovimiento'] = $complRst['root'][0]['claveMovimiento'];
                    }
                    $complementosStr = '';
                    $selStr = "SELECT DISTINCT a.folio, CAST(a.fechaEvento AS DATE) AS fechaEvento ".
                              "FROM trgastosviajetractortbl a, trviajestractorestbl b ".
                              "WHERE a.centroDistribucion = '".$row['centroDistribucion']."' ".
                              "AND b.claveMovimiento = 'VP' ".
                              "AND a.idViajeTractor = b.idViajeTractor ".
                              "AND b.idViajeTractor = ".$row['idViajeTractor']." ".
                              "AND a.folio != 0 ".
                              "AND a.claveMovimiento = 'GC' ".
                              "AND a.claveMovimiento not in ('XP','XC') ".
                              "ORDER BY a.centroDistribucion, a.fechaEvento DESC";
                    $compRst = fn_ejecuta_sql($selStr);
                    for ($i=0; $i < $compRst['records']; $i++) { 
                        foreach ((array) $a['root'] as $j => $rowAux) {
                            if ($rowAux['folio'] == $compRst['root'][$i]['folio']) {
                                $a['root'][$j]['nodeFol'] = $row['folio'];
                                break;
                            }
                        }
                    }
                }
                if ($idxAux == 0) {
                    $a['recBase']['litrosPend'] = $a['root'][$key]['litros'];
                    $a['recBase']['conceptoPend'] = $a['root'][$key]['concepto'];
                }
                $idxAux++;
            }

            foreach ($a['root'] as $idx => $row) {
                $folioArr[$idx]     = $row['folio'];
                $claveMvtoArr[$idx] = $row['claveMovimiento'];
                $fechaEvtoArr[$idx] = $row['fechaEvento'];
            }
            array_multisort($folioArr,SORT_ASC,$claveMvtoArr,SORT_ASC,$fechaEvtoArr,SORT_ASC,$a['root']);//SORT_DESC
        }
        $a['records'] = sizeof($a['root']);

        echo json_encode($a);
    }

    function cancPoliza() {
        $a = array('success' => true);
        // 1 = POLIZA DE GASTOS
        // 2 = COMPLEMENTO DE POLIZA
        $actionPrm = $_REQUEST['actionHdn'];
        $claveChoferPrm = $_REQUEST['claveChofer'];
        $idViajeTractorPrm = $_REQUEST['idViajeTractor'];
        $litrosPendientePrm = $_REQUEST['litrosPendiente'];
        $conceptoPrm = $_REQUEST['concepto'];
        $arrPolizas = json_decode($_POST['arrPolizas'],true);
        $totPolizas = sizeof($arrPolizas);
        if ($totPolizas > 0) {
            foreach ($arrPolizas as $idx => $row) {
                $folioArr[$idx]     = $row['folio'];
                $claveMvtoArr[$idx] = $row['claveMovimiento'];
                $fechaEvtoArr[$idx] = $row['fechaEvento'];
            }
            array_multisort($folioArr,SORT_DESC,$claveMvtoArr,SORT_DESC,$fechaEvtoArr,SORT_ASC,$arrPolizas);
        }
        for ($z=0; $z < $totPolizas; $z++) { 
            $polRow = $arrPolizas[$z];
            $claveMovimientoPrm = ($polRow['tipoPoliza'] == 'P')?'GP':'GC';
            $folioPrm = $polRow['folio'];
            $idViajeTractorPrm = $polRow['idViajeTractor'];
            $centroDistribucionPrm = $polRow['centroDistribucion'];
            $observacionesPrm = strtoupper($polRow['observaciones']);

            if (isset($claveMovimientoPrm) && $claveMovimientoPrm == 'GP') {
                $folioStr = ""; $folioComp = "";
                if ($actionPrm == 2) {
                    $folioStr = "AND folio = ".$folioPrm." ";
                    $folioComp = "AND a.folio = ".$folioPrm." ";
                }

                // Cancela Poliza de Gastos
                if($a['success'] && ($centroDistribucionPrm == 'CDLZC' || $centroDistribucionPrm == 'CDVER')){
                    $sqlNumMto = "SELECT max(m2.idMantenimientoDetalle) as idMantenimiento ".
                                "FROM camantenimientotractoresdetalletbl m2 ".
                                "WHERE m2.idViajeTractor = '".$idViajeTractorPrm."';";
                    $rsNumMto = fn_ejecuta_sql($sqlNumMto);

                    $sqlDltMto = "DELETE FROM camantenimientotractoresdetalletbl ".
                                "WHERE idViajeTractor = '".$idViajeTractorPrm."' ".
                                "AND idMantenimientoDetalle = '".$rsNumMto['root'][0]['idMantenimiento']."'; ";
                    fn_ejecuta_sql($sqlDltMto);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $sqlDltMto;
                    }
                }
                if ($a['success']) {
                    //----- CANCELACION CONCEPTOS GS Y GP
                    $sqlUpdXP = "UPDATE trgastosviajetractortbl ".
                                "SET claveMovimiento = 'XP' ".
                                "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                $folioStr.
                                "AND claveMovimiento = 'GP';";
                    fn_ejecuta_sql($sqlUpdXP);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $sqlUpdXP;
                    } else {
                        $sqlUpdXS = "UPDATE trgastosviajetractortbl ".
                                    "SET claveMovimiento = 'XS' ".
                                    "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                    $folioStr.
                                    "AND claveMovimiento = 'GS';";
                        fn_ejecuta_sql($sqlUpdXS);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql'];
                            $a['sql']         = $sqlUpdXS;
                        } else {
                            //----- CANCELACION CONCEPTOS COMBUSTIBLE 9000
                            $sqlUpdXD = "UPDATE trgastosviajetractortbl ".
                                        "SET claveMovimiento = 'XD' ".
                                        "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                        $folioStr.
                                        "AND concepto like '9%';";
                            fn_ejecuta_sql($sqlUpdXD);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['msjResponse'] = $_SESSION['error_sql'];
                                $a['sql']         = $sqlUpdXD;
                            } else {
                                //----- CANCELACION VIAJE A VU
                                $sqlUpdViaje = "UPDATE trviajestractorestbl ".
                                                "SET claveMovimiento = 'VU' ".
                                                "WHERE idViajeTractor = ".$idViajeTractorPrm;
                                fn_ejecuta_sql($sqlUpdViaje);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success']     = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'];
                                    $a['sql']         = $sqlUpdViaje;
                                } else {
                                    //----- SE BORRA LAS FACTURAS DE LAS POLIZAS
                                    $sqlDltFac = "DELETE FROM trpolizafacturastbl ".
                                                "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                                "AND folio = '".$folioPrm."'";
                                    fn_ejecuta_sql($sqlDltFac);
                                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                        $a['success']     = false;
                                        $a['msjResponse'] = $_SESSION['error_sql'];
                                        $a['sql']         = $sqlDltFac;
                                    } else {
                                        $delStr = "DELETE FROM trComplementosConceptoTbl ".
                                                  "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                                  "AND concepto = '6002' ".
                                                  "AND centroDistribucion = '".$centroDistribucionPrm."' ".
                                                  "AND folio = '".$folioPrm."'";
                                        fn_ejecuta_sql($delStr);
                                        if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                                            $a['success'] = false;
                                            $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $delStr;
                                        } else {
                                            //----- SE BORRAN LAS OBSERVACIONES DE LA POLIZA
                                            $sqlDltObs = "DELETE FROM trobservacionesviajetbl ".
                                                        "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                                        "AND tipo = 'G';";
                                            fn_ejecuta_sql($sqlDltObs);
                                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                                $a['success']     = false;
                                                $a['msjResponse'] = $_SESSION['error_sql'];
                                                $a['sql']         = $sqlDltObs;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ($a['success']) {
                    // COMBUSTIBLE PENDIENTE
                    $fechaEvento = date("Y-m-d H:i:s");
                    $sqlAddObservGastos = "INSERT INTO trObservacionesViajeTbl (idViajeTractor, folio, fechaEvento, tipo, observaciones) VALUES (".
                                            $idViajeTractorPrm.",".
                                            "'".$folioPrm."',".
                                            "'".$fechaEvento."',".
                                            "'C',".
                                            "'".$observacionesPrm."')";
                    fn_ejecuta_sql($sqlAddObservGastos);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $sqlAddObservGastos;
                    } else {
                        $sqlGtosTr = "SELECT * FROM trGastosViajeTractorTbl ".
                                    "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                    $folioStr.
                                    "AND concepto IN ('9018','9019') ".
                                    "AND centroDistribucion = '".$centroDistribucionPrm."' ".
                                    "AND claveMovimiento = 'XD'";
                        $gastosVRs = fn_ejecuta_sql($sqlGtosTr);
                        if ($gastosVRs['records'] > 0) {
                            $litros = $gastosVRs['root'][0]['observaciones'];
                            if (!empty($litros) && $litros != 0) {
                                $sqlUpdPdt = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CP' ".
                                             "WHERE idCombustible = ".$litros;
                                fn_ejecuta_sql($sqlUpdPdt);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success']     = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'];
                                    $a['sql']         = $sqlUpdPdt;
                                } else {
                                    $sqlUpdPdtN = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CL' ".
                                                  "WHERE idCombustible != ".$litros;
                                    fn_ejecuta_sql($sqlUpdPdtN);
                                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                        $a['success']     = false;
                                        $a['msjResponse'] = $_SESSION['error_sql'];
                                        $a['sql']         = $sqlUpdPdtN;
                                    }
                                }
                            }
                        } else {
                            $sqlUpdPdtN = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CL' ".
                                          "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                          "AND claveChofer = ".$claveChoferPrm;
                            fn_ejecuta_sql($sqlUpdPdtN);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['msjResponse'] = $_SESSION['error_sql'];
                                $a['sql']         = $sqlUpdPdtN;
                            }
                        }
                    }

                    // TICKET CARD
                    if ($a['success']) {
                        $updStr = "UPDATE trpolizastckettbl ".
                                  "SET estatus = 1, folio = NULL, idViajeTractor = NULL ".
                                  "WHERE folio = ".$folioPrm." ".
                                  "AND idViajeTractor = ".$idViajeTractorPrm;
                        fn_ejecuta_sql($updStr);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql'];
                            $a['sql']         = $updStr;
                        } else {
                            // GASTO NO COMPROBADO
                            $selGtosStr = "SELECT * FROM trGastosViajeTractorTbl ".
                                        "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                        "AND folio = ".$folioPrm." ".
                                        "AND concepto = '144' ".
                                        "AND centroDistribucion = '".$centroDistribucionPrm."'";
                            $gastosRst = fn_ejecuta_sql($selGtosStr);
                            if ($gastosRst['records'] > 0) {
                                $selStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                                          "WHERE claveChofer = ".$claveChoferPrm." ".
                                          "AND concepto = '2231' ".
                                          "AND estatus = 'P'";
                                $descRst = fn_ejecuta_sql($selStr);
                                $gastosRst['root'][0]['importe'] = str_replace('-', '', $gastosRst['root'][0]['importe']);
                                $saldo = floatval($descRst['root'][0]['saldo']) - floatval($gastosRst['root'][0]['importe']);
                                if ($saldo > 0) {
                                    if ($descRst['root'][0]['tipoConcepto'] == 'I') {
                                        $ls_importe = "tipoImporte = ".$saldo.", importe = ".$saldo.", saldo = " . $saldo;
                                    } else {
                                        $ls_importe = "saldo = " . $saldo;
                                    }
                                    $setStr = "SET saldo = ${ls_importe} ";
                                } else {
                                    $setStr = "SET estatus = 'L' ";
                                }
                                $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                                          $setStr.
                                          "WHERE claveChofer = ".$claveChoferPrm." ".
                                          "AND concepto = '2231'";
                                fn_ejecuta_sql($updStr);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success']     = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'];
                                    $a['sql']         = $updStr;
                                }
                            }
                        }
                        if ($a['success']) {
                            // DESCUENTOS PERSONALES
                            $selStr = "SELECT * FROM rohistoricodescuentoschoferestbl ".
                                      "WHERE viaje = ".$idViajeTractorPrm." ".
                                      "AND avanzada = ".$folioPrm." ".
                                      "AND claveChofer = ".$claveChoferPrm." ".
                                      "AND centroDistribucion = '".$centroDistribucionPrm."'";
                            $historicoRst = fn_ejecuta_sql($selStr);
                            for ($i=0; $i < $historicoRst['records']; $i++) { 
                                $selStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                                          "WHERE claveChofer = ".$claveChoferPrm." ".
                                          "AND concepto = '".$historicoRst['root'][$i]['concepto']."' ".
                                          "ORDER BY idDescuento DESC LIMIT 1";
                                          // "AND estatus = 'P'";
                                $concDescRst = fn_ejecuta_sql($selStr);
                                if ($concDescRst['records'] > 0) {
                                    $tipoConcepto = $concDescRst['root'][0]['tipoConcepto'];
                                    $estatus = $concDescRst['root'][0]['estatus'];
                                    $saldo = floatval($historicoRst['root'][$i]['importe']);
                                    if ($estatus == 'P') {
                                        if ($tipoConcepto == 'P' || $tipoConcepto == 'I') {
                                            $selDescStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                                                      "WHERE claveChofer = ".$claveChoferPrm." ".
                                                      "AND concepto = '".$historicoRst['root'][$i]['concepto']."' ".
                                                      "AND tipoConcepto IN ('P','I') ".
                                                      "AND estatus = 'P'";
                                            $rodescRst = fn_ejecuta_sql($selDescStr);
                                            $saldoAct = floatval($rodescRst['root'][0]['saldo']) + $saldo;
                                            if ($rodescRst['root'][0]['tipoConcepto'] == 'I') {
                                                $ls_importe = "tipoImporte = ${saldoAct}, importe = ${saldoAct}, saldo = ${saldoAct}";
                                            } else {
                                                $ls_importe = "saldo = ${saldoAct}";
                                            }
                                            $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                                                      "SET ${ls_importe} ".
                                                      "WHERE claveChofer = ".$claveChoferPrm." ".
                                                      "AND concepto = '".$historicoRst['root'][$i]['concepto']."' ".
                                                      "AND tipoConcepto IN ('P','I') ".
                                                      "AND estatus = 'P'";
                                            fn_ejecuta_sql($updStr);
                                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                                $a['success']     = false;
                                                $a['msjResponse'] = $_SESSION['error_sql'];
                                                $a['sql']         = $updStr;
                                                break;
                                            }
                                        }
                                    } else {
                                        $tipoImporte = "tipoImporte";
                                        $importe = "importe";
                                        if ($tipoConcepto == 'I') {
                                            $tipoImporte = $saldo;
                                            $importe = $saldo;
                                        }
                                        $insStr = "INSERT INTO roconceptosdescuentoschofertbl (claveChofer, concepto, centroDistribucion, secuencia, fechaEvento, pagadoCobro, tipoConcepto, tipoImporte, importe, saldo, estatus) ".
                                                "SELECT claveChofer, concepto, centroDistribucion, secuencia +1 AS secuencia, CAST(NOW() AS DATE) AS fechaEvento, pagadoCobro, tipoConcepto, ${tipoImporte} AS tipoImporte, ${importe} AS importe, ${saldo} AS saldo, 'P' AS estatus ".
                                                "FROM roconceptosdescuentoschofertbl ".
                                                "WHERE claveChofer = ".$claveChoferPrm." ".
                                                "AND concepto = '".$historicoRst['root'][$i]['concepto']."' ".
                                                "ORDER BY secuencia DESC LIMIT 1";
                                        fn_ejecuta_sql($insStr);
                                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                            $a['success']     = false;
                                            $a['msjResponse'] = $_SESSION['error_sql'];
                                            $a['sql']         = $insStr;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if ($a['success']) {
                        $updStr = "UPDATE tralimentospendientetbl ".
                                  "SET claveMovimiento = 'PX' ".
                                  "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                                  "AND concepto = '2342' ".
                                  "AND centroDistribucion = '".$centroDistribucionPrm."' ".
                                  "AND folio = '".$folioPrm."' ".
                                  "AND claveMovimiento = 'PA'";
                        fn_ejecuta_sql($updStr);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql'];
                            $a['sql']         = $updStr;
                        }
                    }
                }
            } else {
                // Cancela Complemento de Poliza
                $a = canComplementoPoliza($idViajeTractorPrm, $folioPrm, $centroDistribucionPrm);
            }
            if ($a['success'] == false) {
                break;
            }
        }
        if ($a['success']) {
            //Actualiza combustible pendiente
            if ($totPolizas > 1) {
                $updStr = "UPDATE trCombustiblePendienteTbl ".
                          "SET claveMovimiento = 'CL' ".
                          "WHERE claveChofer = ${claveChoferPrm} ".
                          "AND claveMovimiento = 'CP'";
                fn_ejecuta_sql($updStr);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql']         = $updStr;
                }

                if ($a['success'] && $litrosPendientePrm != 0) {
                    $insStr = "INSERT INTO trCombustiblePendienteTbl (idViajeTractor, claveChofer, litros, concepto, claveMovimiento) VALUES (".
                        "${idViajeTractorPrm}, ${claveChoferPrm}, ${litrosPendientePrm}, '${conceptoPrm}', 'CP')";
                    fn_ejecuta_sql($insStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $insStr;
                    }
                }
            }
        }

        // fn_ejecuta_sql($a['success']);
        fn_ejecuta_sql(false);echo ">";
        echo json_encode($a);
    }

    function canComplementoPoliza($idViajeTractorPrm, $folioPrm, $centroDistribucionPrm) {
        $a = array('success' => true);

        $sqlUpdXC = "UPDATE trgastosviajetractortbl ".
                    "SET claveMovimiento = 'XC' ".
                    "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                    "AND folio = '".$folioPrm."' ".
                    "AND claveMovimiento = 'GC';";
        fn_ejecuta_sql($sqlUpdXC);
        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
            $a['success']     = false;
            $a['msjResponse'] = $_SESSION['error_sql'];
            $a['sql']         = $sqlUpdXC;
        } else {
            $sqlUpdXS = "UPDATE trgastosviajetractortbl ".
                        "SET claveMovimiento = 'XP' ".
                        "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                        "AND folio = '".$folioPrm."' ".
                        "AND claveMovimiento = 'GP';";
            fn_ejecuta_sql($sqlUpdXS);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success']     = false;
                $a['msjResponse'] = $_SESSION['error_sql'];
                $a['sql']         = $sqlUpdXS;
            }
        }
        if ($a['success']) {
            $sqlDltFac = "DELETE FROM trpolizafacturastbl ".
                        "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                        "AND folio = '".$folioPrm."'";
            fn_ejecuta_sql($sqlDltFac);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success']     = false;
                $a['msjResponse'] = $_SESSION['error_sql'];
                $a['sql']         = $sqlDltFac;
            } else {
                $delStr = "DELETE FROM trComplementosConceptoTbl ".
                          "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                          "AND concepto = '6002' ".
                          "AND centroDistribucion = '".$centroDistribucionPrm."' ".
                          "AND folio = '".$folioPrm."'";
                fn_ejecuta_sql($delStr);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $delStr;
                } else {
                    $updStr = "UPDATE tralimentospendientetbl ".
                              "SET claveMovimiento = 'CX' ".
                              "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                              "AND concepto = '2342' ".
                              "AND centroDistribucion = '".$centroDistribucionPrm."' ".
                              "AND folio = '".$folioPrm."' ".
                              "AND claveMovimiento = 'CA'";
                    fn_ejecuta_sql($updStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $updStr;
                    }
                }
            }
        }
        if ($a['success']) {
            // COMBUSTIBLE PENDIENTE
            $selXC = "SELECT * FROM trgastosviajetractortbl ".
                    "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                    "AND folio = '".$folioPrm."' ".
                    "AND concepto IN ('9012','9013') ".
                    "AND claveMovimiento = 'XC';";
            $Rst = fn_ejecuta_sql($selXC);
            if ($Rst) {
                $combusPendArr = explode('|', $Rst['root'][0]['observaciones']);
                $sqlDel = "DELETE FROM trCombustiblePendienteTbl " . 
                          "WHERE idCombustible = " . $combusPendArr[1] . " ".
                          "AND claveMovimiento = 'CP'";
                fn_ejecuta_sql($sqlDel);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql'] = $sqlDel;
                } else {
                    if (empty($combusPendArr[0])) {
                        $combusPendArr[0] = 0;
                    }
                    $sqlUpdPdt = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CP' " . 
                                 "WHERE idCombustible = " . $combusPendArr[0] . " ";
                    fn_ejecuta_sql($sqlUpdPdt);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success'] = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql'] = $sqlUpdPdt;
                    }
                }
            }
        }
        return $a;
    }
?>