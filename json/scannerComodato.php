<?php
    //ESTE PHP SE USA PARA CONSULTAS ESPECIFICAS PARA LAS TERMINALES (SCANNERES)
    session_start();
    $_SESSION['modulo'] = "teConsultas";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
    require_once("alDanosVics.php");
    require_once("alUnidades.php");
    require_once("trViajesTractores.php");
    require_once("actividadesPmp.php");

    //SPECIAL CHARACTER
    //SE USA PARA DIFERENCIAR LOS DIFERENTES QUERYS EN LA MISMA LINEA
    $spChar = '~';

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['teConsultasActionHdn']){
    	case 'checkLogin':
    		checkLogin();
    		break;
        case 'isPresentadaUnidad':
            isPresentadaUnidad();
            break;
        case 'getDatosPresentadas':
            getDatosPresentadas();
            break;
        case 'getColores':
            getColores();
            break;
        case 'presentarUnidad':
            presentarUnidad();
            break;
        case 'getUnidades':
            getUnidadesScanner();
            break;
        case 'validarEntradaPatio':
            validarEntradaPatio();
            break;
        case 'generarEntradaPatio':
            generarEntradaPatio();
            break;
        case 'validarCambioDistribuidor':
            validarCambioDistribuidor();
            break;
        case 'getDistribuidoresCombo':
            getDistribuidoresCombo();
            break;
        case 'cambiarDistribuidor':
            cambiarDistribuidor();
            break;
        case 'validarCapturaDanos':
            validarCapturaDanos();
            break;
        case 'getOrigenDanos':
            getOrigenDanos();
            break;
        case 'capturarDano':
            capturarDano();
            break;
        case 'getDatosRecepcionViaje':
            getDatosRecepcionViaje();
            break;
        case 'validarRecepcionViaje':
            validarRecepcionViaje();
            break;
        case 'recepcionViaje':
            recepcionViaje();
            break;
        case 'getTractoresEmbarcadas':
            getTractoresEmbarcadas();
            break;
        case 'validarEmbarcada':
            validarEmbarcada();
            break;
        case 'actualizarEmbarque':
            actualizarEmbarque();
            break;
        case 'validarUnidadSalidasGen':
            validarUnidadSalidasGen();
            break;
        case 'getTAT':
            getTAT();
            break;
        case 'generarTAT':
            generarTAT();
            break;
        case 'getPatios':
            getPatios();
            break;
        case 'getDatosFila':
            getDatosFila();
            break;
        case 'reordenarFila':
            reordenarFila();
            break;
        case 'getCampaniasCentrosDistribucion':
            getCampaniasCentrosDistribucion();
            break;
        case 'entradaCampania':
            entradaCampania();
            break;
        case 'setSalidaGeneral':
            setSalidaGeneral();
            break;
        case 'validarPatio':
            validarPatio();
            break;
        case 'generarEntradaSalida':
            generarEntradaSalida();
            break;
        case 'entradaContencionDB':
            entradaContencionDB();
            break;
        case 'validaVinEntradaContencion':
            validaVinEntradaContencion();
            break;
        case 'vinExisteContencion':
            vinExisteContencion();
            break;
        case 'vinNoExisteContencion':
            vinNoExisteContencion();
            break;
        case 'validaSimboloEntradaContencion':
            validaSimboloEntradaContencion();
            break;
        case 'getDatosContencion':
            getDatosContencion();
            break;
        case 'entradaContencionDB':
            entradaContencionDB();
            break;
        case 'consultarUnidad':
            consultarUnidad();
            break;
        case 'getActividadesPmp':
            getActividadesPmp();
            break;
        case 'validaVin':
            validaVin();
            break;
        case 'validaExistaVin':
            validaExistaVin();
            break;
         case 'tiposSalidas':
            tiposSalidas();
            break;
        case 'getMercado':
            getMercado();
            break;
        case 'tMercado';
            tMercado();
            break;
        case 'updTipoMercado';
            updTipoMercado();
            break;
        case 'getTipoSalida';
            getTipoSalida();
            break;
        default:
            echo '';
    }

    function checkLogin(){

        $response = "";

        $sqlCheckLogin = "SELECT us.idUsuario, uc.distribuidorCentro, ".
                            "(SELECT 1 AS conPatio FROM cageneralestbl WHERE tabla = 'trViajesTractoresTbl' ".
                                "AND columna = 'cdValidos' AND valor = uc.distribuidorCentro) AS conPatio ".
                            "FROM segusuariostbl us, segusuarioscentrostbl uc ".
                         "WHERE us.idUsuario = uc.idUsuario ".
                         "AND us.usuario = '".$_REQUEST['teConsultasUsuarioTxt']."' ".
                         "AND us.password = '".$_REQUEST['teConsultasPasswordTxt']."' ";

        $rs = fn_ejecuta_query($sqlCheckLogin);

        if(sizeof($rs['root']) > 0){
            $response = createResponse($rs['root']);
        }

        echo $response;
    }

    function createResponse($root){
        $response = "";
        $keys = array_keys($root[0]);

        foreach ($root as $row) {
            foreach ($keys as $key) {
                $response .= $row[$key]."|";
            }

            $response .= ";";
        }


        return $response;
    }

    //SE USAN COMO ALTERNATIVA A LOS COMBOS PARA VALIDAR LOS DATOS DEL FORMULARIO
   /* function createResponseCombos($root){
        $response = "";
        global $spChar;
        $keys = array_keys($root[0]);

        foreach ($root as $row) {
            foreach ($keys as $key) {
                if($row[$key] == $spChar){
                    $response .= ";";
                } else {
                    $response .= $row[$key]."|";
                }
            }
        }


        return $response;
    }
*/
    function getUnidadesScanner(){
        $response = "";

        $sqlGetUnidades = "SELECT u.vin, u.simboloUnidad, u.color, h.distribuidor, h.claveMovimiento, t.tarifa, h.localizacionUnidad ".
                            "FROM alUnidadesTbl u, alHistoricoUnidadesTbl h, caTarifasTbl t ".
                            "WHERE u.vin = h.vin ".
                            "AND h.fechaEvento=(".
                              "SELECT MAX(h1.fechaEvento) ".
                              "FROM alHistoricoUnidadesTbl h1 ".
                              "WHERE h1.vin = u.vin) ".
                            "AND t.idTarifa = h.idTarifa ";

        if($_REQUEST['teConsultasAvanzadaHdn'] != "")
            $sqlGetUnidades .= "AND u.avanzada = '".$_REQUEST['teConsultasAvanzadaHdn']."'";
        elseif ($_REQUEST['teConsultasVinHdn'] != "") {
            $sqlGetUnidades .= "AND u.vin = '".$_REQUEST['teConsultasVinHdn']."'";
        }

        $rs = fn_ejecuta_query($sqlGetUnidades);

        if(sizeof($rs['root']) > 0){
            $response = createResponse($rs['root']);
        }

        echo $response;
    }

    /*******************************************************************
    *
    *                           PRESENTADAS
    *
    ********************************************************************/
    function isPresentadaUnidad(){

        $sqlHistorico = "select * from alultimodetalletbl ".
                        " where vin = '".$_REQUEST['teConsultasVinHdn']."'";
        $rsHist = fn_ejecuta_query($sqlHistorico);

        /*$sqlHistorico = "SELECT u.vin FROM alUnidadesTbl u, alHistoricoUnidadesTbl hu ".
                        "WHERE u.vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                        "AND u.vin = hu.vin;";

        $rsHist = fn_ejecuta_query($sqlHistorico);*/

        if (!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
            if ($rsHist['root'][0]['claveMovimiento'] == 'IC') {
                echo '0|Unidad Con Ingreso|';
            } else { //Si no Existe se puede presentar
                //echo '1|Unidad Valida';

                $sql660 =   "SELECT a6.vin, a6.scaccode, a6.prodstatus, a6.dealerid, a6.model, ".
                            "a6.colorcode, a6.vupdate, a6.vuptime FROM al660tbl a6 ".
                            "WHERE a6.vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                            "AND (a6.dealerid IS NOT NULL OR a6.dealerid <> '') ".
                            "AND a6.von NOT LIKE '1111%' ".
                            "AND a6.von NOT LIKE '2222%' ".
                            "AND a6.von NOT LIKE '1.111%' ".
                            "AND a6.von NOT LIKE '2.222%' ".
                            "AND a6.vupdate =  (select max(a6_2.vupdate) from al660tbl a6_2 ".
                                                        "where a6_2.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                                        "and a6_2.scacCode IN ('MITS', 'XTRA') ".
                                                        "and a6_2.von NOT LIKE '1111%' AND a6_2.von NOT LIKE '2222%' AND a6_2.von NOT LIKE '1.111%' AND a6_2.von NOT LIKE '2.222%' ".
                                                        "group by a6_2.vin) ".
                            "AND a6.vuptime = (select max(a6_3.vuptime) from al660tbl a6_3 ".
                                              "where a6_3.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                              "and a6_3.von NOT LIKE '1111%' AND a6_3.von NOT LIKE '2222%' AND a6_3.von NOT LIKE '1.111%' AND a6_3.von NOT LIKE '2.222%' ".
                                              "and a6_3.vupdate = (select max(a6_4.vupdate) from al660tbl a6_4 ".
                                                                  "where a6_4.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                                                  "and a6_4.scacCode IN ('MITS', 'XTRA') ".
                                                                  "and a6_4.von NOT LIKE '1111%' AND a6_4.von NOT LIKE '2222%' AND a6_4.von NOT LIKE '1.111%' AND a6_4.von NOT LIKE '2.222%' ".
                                                                  "group by a6_4.vin) ".
                                              "and a6_3.scacCode = 'XTRA' ".
                                              "group by a6_3.vin) ".
                            "GROUP BY a6.vin ";

                $rs660 = fn_ejecuta_query($sql660);

                if($rs660['records'] > 0)
                    echo "1|".$rs660['root'][0]['model']."|".$rs660['root'][0]['dealerid']."|".$rs660['root'][0]['colorcode']."|";
                else
                    echo "1||||";

            }
        } else {

            echo '0|ERROR-NO HUBO RESPUESTA';
        }

    }


    function getDatosPresentadas(){
        $response = "";
        global $spChar;

        $sqlGetDatosPresentada = "SELECT simboloUnidad AS codigo FROM caSimbolosUnidadesTbl WHERE marca IN('FI','CD','JP','RA','DG','MI') ".
                                    "UNION ALL SELECT '".$spChar."' AS codigo UNION ALL ".
                                    "SELECT distribuidorCentro AS codigo FROM caDistribuidoresCentrosTbl ".
                                        "WHERE tipoDistribuidor in('DI','DX')  ".
                                    "UNION ALL SELECT '".$spChar."' AS codigo UNION ALL ".
                                    "SELECT tarifa AS codigo FROM caTarifasTbl ".
                                    "UNION ALL SELECT '".$spChar."' AS codigo ";

                                    //UNION ALL AL FINAL PORQUE LA FUNCION DE LA PANTALLA REMUEVE LOS DOS ULTIMOS CARACTERES
        $rs = fn_ejecuta_query($sqlGetDatosPresentada);

        if(sizeof($rs['root']) > 0){
            $response = createResponseCombos($rs['root']);
        }

        echo $response;
    }

    function getColores(){
        $response = "";

        $sqlGetColores = "  SELECT CONCAT(valor,' - ',nombre) as colorDesc ".
                        "FROM CAGENERALESTBL ".
                        "WHERE tabla = 'al660tbl' ".
                        "AND columna = 'prodStatus';";

        $rs = fn_ejecuta_query($sqlGetColores);

        if(sizeof($rs['root']) > 0){
            $response = createResponseCombos($rs['root']);
        }

        echo $response;
    }

    function presentarUnidad(){
        $response = "";
        $RQchofer="";

        $sqlCheckUnidad = "SELECT 1 FROM alUnidadesTbl WHERE vin = '".$_REQUEST['teConsultasVinHdn']."'";

        $rs = fn_ejecuta_query($sqlCheckUnidad);


        if(sizeof($rs['root']) < 0){
            $response = '0' . "|" . "Unidad ya presentada" . "|";
        } else {
            //Obtiene el id de la Tarifa
            $sqlTarifa = "SELECT idTarifa FROM caTarifasTbl WHERE tarifa = '".$_REQUEST['teConsultasTarifaTxt']."';";
            $rsTarifa = fn_ejecuta_query($sqlTarifa);
            $rsTarifa = $rsTarifa['root'][0]['idTarifa'];

    		/*$data = addUnidad($_REQUEST['teConsultasVinHdn'], $_REQUEST['teConsultasDistribuidorTxt'],
	    					  $_REQUEST['teConsultasSimboloUnidadHdn'],'PW7',
	    					  $_REQUEST['teConsultasCentroDistHdn'],'IC',$rsTarifa['idTarifa'],
	    					  $_REQUEST['teConsultasLocalizacionUnidadHdn'],'', '','UNIDAD DE COMODATO','', 0,
	    					  $_REQUEST['teConsultasIdUsuarioHdn']);*/




                  $sqlAddUnidadStr =  "INSERT INTO alUnidadesTbl ".
                                                "(vin, avanzada, distribuidor, simboloUnidad, color, folioRepuve, descripcionUnidad) ".
                                                "VALUES(".
                                                "'".$_REQUEST['teConsultasVinHdn']."',".
                                                "'".substr($_REQUEST['teConsultasVinHdn'], -8)."',".
                                                "'".$_REQUEST['teConsultasDistribuidorTxt']."',".
                                                "'".$_REQUEST['teConsultasSimboloUnidadHdn']."',".
                                                "'PW7',".
                                                "(SELECT folioRepuve FROM alRepuveTbl WHERE vin = '".$_REQUEST['teConsultasVinHdn']."'),".
                                                replaceEmptyNull("'cmDat_capturada'").")";

                    $rs_01 = fn_ejecuta_Add($sqlAddUnidadStr);


                 $sqlAddCambioHistoricoStr = "INSERT INTO alHistoricoUnidadesTbl ".
                                            "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                            "claveChofer, observaciones, usuario, ip) ".
                                            "VALUES(".
                                            "'CMDAT',".
                                            "'".$_REQUEST['teConsultasVinHdn']."',".
                                            "NOW(),".
                                            "'IC',".
                                            "'".$_REQUEST['teConsultasDistribuidorTxt']."',".
                                            "'".$rsTarifa."',".
                                            "'CMDAT',".
                                            replaceEmptyNull($RQchofer).",".
                                            "'UNIDAD DE COMODATO',".
                                            "'99',".
                                            "'".getClientIP()."')";

                $rs_04 = fn_ejecuta_Add($sqlAddCambioHistoricoStr);

                $sqlAddUpdUltimoDetalleStr = "INSERT INTO alUltimoDetalleTbl ".
                                            "(centroDistribucion, vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, ".
                                            "claveChofer, observaciones, usuario, ip) ".
                                            "VALUES(".
                                            "'CMDAT',".
                                            "'".$_REQUEST['teConsultasVinHdn']."',".
                                            "NOW(),".
                                            "'IC',".
                                            "'".$_REQUEST['teConsultasDistribuidorTxt']."',".
                                            "'".$rsTarifa."',".
                                            "'CMDAT',".
                                            replaceEmptyNull($RQchofer).",".
                                            "'UNIDAD DE COMODATO',".
                                            "'99',".
                                            "'".getClientIP()."')";

                $rs_03 = fn_ejecuta_Add($sqlAddUpdUltimoDetalleStr);

                  $updCentroUD = "  UPDATE alUltimoDetalleTbl ".
                                " SET centroDistribucion = 'CMDAT' , fechaEvento = NOW() ,claveMovimiento='IC', idTarifa= '".$rsTarifa."'".
                                " WHERE vin='".$_REQUEST['teConsultasVinHdn']."' ";
                                //"AND centroDistribucion = 'CDTOL' ";
                                //"AND IDTARIFA IN (8,9,10) ";

                  $rsUpdCentroUD = fn_ejecuta_query($updCentroUD);

                   $updUndUn = "UPDATE alUnidadesTbl ".
                            " SET distribuidor='".$_REQUEST['teConsultasDistribuidorTxt'].
                            "',descripcionUnidad ='cmDat_capturada', simboloUnidad='".$_REQUEST['teConsultasSimboloUnidadHdn']."'".
                            " WHERE `vin`='".$_REQUEST['teConsultasVinHdn']."'";

                    $rsAddUndHistorico = fn_ejecuta_query($updUndUn);


                 $sql660tbl = "SELECT * FROM al660tbl WHERE vin ='".$_REQUEST['teConsultasVinHdn']."'".
                                " AND scaccode='COMO'";
                 $rst660 = fn_ejecuta_query($sql660tbl);

                 if ($rst660['records'] == 0) {
                       $ins660Tbl = "INSERT INTO al660tbl (vin, numavanzada, scacCode,prodstatus, routeori,von,model,city,state,colorcode,tipoOrigen) ".
                             " VALUES ('".$_REQUEST['teConsultasVinHdn'].
                            "', '".substr($_REQUEST['teConsultasVinHdn'],9,17).
                            "','COMO' ".
                            ", '".$_REQUEST['teConsultasColorHdn']."' ".
                            ",'routeori' ".
                            ",'von' ".
                            ", '".$_REQUEST['teConsultasSimboloUnidadHdn']."' ".
                            ",'city' ".
                            ",'state' ".
                            ",'PW7' ".
                            ",'tipoOrigen' )";

                      fn_ejecuta_query($ins660Tbl);
                 }else{
                    $upd660 = "UPDATE al660tbl SET prodstatus='".$_REQUEST['teConsultasColorHdn']."' WHERE vin='".$_REQUEST['teConsultasVinHdn']."'";
                    $rstupd660 = fn_ejecuta_query($upd660);
                 }

                 insertActividadesPmp($_REQUEST['teConsultasVinHdn'],"COMODATO");


                   /* $updUndUd = "UPDATE alultimodetalletbl ".
                                "SET claveMovimiento = 'AM', ".
                                       "fechaEvento = NOW() ".
                                "WHERE vin IN (SELECT tmp.VIN".
                                                     "FROM alunidadestmp tmp ".
                                                     "WHERE tmp.ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                                     "AND tmp.idUsuario = '".$_SESSION['idUsuario']."') ";

                    $rsAddUndHistorico = fn_ejecuta_query($updUndUd);*/

                   /* $updUndUn = "UPDATE alUnidades ".
                                "SET distribuidor = '".$_REQUEST['teConsultasDistribuidorTxt']."', ".
                                       "fechaEvento = NOW() ".
                                "WHERE vin IN (SELECT tmp.VIN ".
                                                     "FROM alunidadestmp tmp ".
                                                     "WHERE tmp.ip = '".$_SERVER['REMOTE_ADDR']."' ".
                                                     "AND tmp.idUsuario = '".$_SESSION['idUsuario']."') ";

                    $rsAddUndHistorico = fn_ejecuta_query($updUndUd);  */





                    $data['success']=true;






            if($data['success']){
                $response = "1|" . $data['successMessage'] . "|";
            } else {
                $response = "0|" . $data['errorMessage'] . "|";
            }

            $updCentroHU = "  UPDATE alHistoricoUnidadesTbl ".
                            "SET centroDistribucion = 'CMDAT' ".
                            "WHERE OBSERVACIONES = 'UNIDAD DE COMODATO' ".
                            "AND centroDistribucion = 'CDTOL' ".
                            "AND IDTARIFA IN (8,9,10) ";


            $rsUpdCentroHU = fn_ejecuta_query($updCentroHU);

            /*$updCentroUD = "  UPDATE alUltimoDetalleTbl ".
                "SET centroDistribucion = 'CMDAT' , idTarifa= '".$rsTarifa['idTarifa']."'".
                "WHERE vin='".$_REQUEST['teConsultasVinHdn']."' ";
                //"AND centroDistribucion = 'CDTOL' ";
                //"AND IDTARIFA IN (8,9,10) ";

            $rsUpdCentroUD = fn_ejecuta_query($updCentroUD);*/

            //$_REQUEST['teConsultasColorHdn']
            /* $ins660Tbl = "INSERT INTO al660tbl (vin, numavanzada, scacCode,prodstatus, routeori,von,model,city,state,colorcode,tipoOrigen) ".
                          "VALUES ('".$_REQUEST['teConsultasVinHdn'].
                    "', '".substr($_REQUEST['teConsultasVinHdn'],9,17).
                    "','scacCode' ".
                    ", '".$_REQUEST['teConsultasColorHdn']."' ".
                    ",'routeori' ".
                    ",'von' ".
                    ", '".$_REQUEST['teConsultasSimboloUnidadHdn']."' ".
                    ",'city' ".
                    ",'state' ".
                    ",'PW7' ".
                    ",'tipoOrigen' )";

            fn_ejecuta_query($ins660Tbl);  */

$sqlIns660 = "";

        }

        echo $response;
    }

    /*******************************************************************
    *
    *                           ENTRADA A PATIO
    *
    ********************************************************************/

    function validarEntradaPatio(){
        $response = "";

        //USA alUnidadesVinHdn en vez de teConsultasVinHdn
        $data = getUnidadesEntradaPatio();

        if(sizeof($data['root']) > 0){
            for($i = 0; $i < sizeof($data['root']); $i++){
                $response .= $data['root'][$i]['vin']."|";
                $response .= $data['root'][$i]['simboloUnidadES']."|";
                $response .= $data['root'][$i]['tarifa']."|";
                $response .= $data['root'][$i]['distribuidor']."|";
                $response .= $data['root'][$i]['retrabajoSimbolo']."|";
                $response .= $data['root'][$i]['error']."|";
                $response .= ";";
            }
        }

        echo $response;
    }

    function generarEntradaPatio(){
        $response = "";

        $data = insertarEntradaPatio($_REQUEST['teConsultasCentroDistHdn'], $_REQUEST['teConsultasConLocalizacionHdn'], $_REQUEST['teConsultasFlotillaHdn'],
                                    $_REQUEST['teConsultasVinHdn'], $_REQUEST['teConsultasTarifaHdn'], $_REQUEST['teConsultasDistribuidorTxt'],
                                    $_REQUEST['teConsultasSimboloHdn'], $_REQUEST['teConsultasRetrabajoHdn'], $_REQUEST['teConsultasIdUsuarioHdn']);


        if($data['success']){
            $response = "1|".$data['successMessage']."|";
        } else {
            $response = "0|".$data['errorMessage']."|";
        }

        echo $response;
    }


    /*******************************************************************
    *
    *                       CAMBIO DE DISTRIBUIDOR
    *
    ********************************************************************/

    function validarCambioDistribuidor(){
        $response = "";

        $sqlValidarCambioDist = "SELECT ".
                                    "(SELECT 1 FROM alhistoricounidadestbl ".
                                        "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                                        "AND claveMovimiento = 'EP' ".
                                        "AND centroDistribucion = '".$_REQUEST['teConsultasCentroDistHdn']."') AS entradaPatio";

        $rs = fn_ejecuta_query($sqlValidarCambioDist);

		if($rs['records'] > 0){
    		$response = createResponse($rs['root']);
    	}

    }

    function getDistribuidoresCombo(){
        $response = "";
        global $spChar;

       $sqlGetDistribuidores = "SELECT distribuidorCentro AS codigo FROM caDistribuidoresCentrosTbl ".
                                "WHERE tipoDistribuidor = 'DI' ".
                                "UNION ALL SELECT '".$spChar."' AS codigo ";;


        $rs = fn_ejecuta_query($sqlGetDistribuidores);

        if(sizeof($rs['root']) > 0){
            $response = createResponseCombos($rs['root']);
        }

        echo $response;
    }

    function cambiarDistribuidor(){
        $response = "";

        $data = addHistoricoUnidad($_REQUEST['teConsultasCentroDistHdn'], $_REQUEST['teConsultasVinHdn'], $_REQUEST['teConsultasClaveMovimientoHdn'],
                                    $_REQUEST['teConsultasDistribuidorTxt'], $_REQUEST['teConsultasTarifaTxt'], $_REQUEST['teConsultasLocalizacionUnidadHdn'],
                                    $_REQUEST['teConsultasChoferHdn'], $_REQUEST['teConsultasObservacionesTxt'], $_REQUEST['teConsultasIdUsuarioHdn']);

        if($data['success']){
            $response = "1|"."Cambio de Distribuidor Realizado Correctamente"."|";
        } else {
            $response = "0|".$data['errorMessage']."|";
        }

        echo $response;
    }

    /*******************************************************************
    *
    *                       CAPTURA DE DAÑOS
    *
    ********************************************************************/

    function validarCapturaDanos(){
        $response = "";
        $codigo = "";
        $operador = "";

        if(isset($_REQUEST['teConsultasVinHdn'])){
            $operador = "=";
            $codigo = $_REQUEST['teConsultasVinHdn'];
        } else if(isset($_REQUEST['teConsultasAvanzadaHdn'])){
            $operador = "LIKE";
            $codigo = "%".$_REQUEST['teConsultasAvanzadaHdn'];
        }

        $sqlValidarCapturaDano = "SELECT u.vin, ".
                                "(SELECT 1 FROM alhistoricounidadestbl h WHERE h.vin = u.vin AND h.claveMovimiento = 'AM') AS asignada, ".
                                "(SELECT 1 FROM alhistoricounidadestbl h2 WHERE h2.vin = u.vin AND h2.claveMovimiento = 'DI') AS dano, ".
                                "(SELECT COUNT(*)+1 FROM alDanosVicsTbl d WHERE d.vin = u.vin) AS numeroDanos ".
                                "FROM alUnidadesTbl u ".
                                "WHERE u.vin ".$operador." '".$codigo."'";

        $rs = fn_ejecuta_query($sqlValidarCapturaDano);

        if(sizeof($rs['root']) > 0){
            $response = createResponse($rs['root']);
        }

        echo $response;
    }

    function getOrigenDanos(){
        $response = "";
        global $spChar;

        $sqlGetOrigenDanos =    "SELECT valor ".
                                "FROM cageneralestbl ".
                                "WHERE tabla = 'alDanosVicsTbl' ".
                                "AND columna = 'tipoDano' ".
                                "UNION ALL ".
                                "SELECT '".$spChar."' as valor ".
                                "UNION ALL ".
                                "SELECT DISTINCT areaDano AS valor FROM cadanostbl ".
                                "UNION ALL ".
                                "SELECT '".$spChar."' as valor ".
                                "UNION ALL ".
                                "SELECT DISTINCT tipoDano FROM cadanostbl ".
                                "UNION ALL ".
                                "SELECT '".$spChar."' as valor ".
                                "UNION ALL ".
                                "SELECT DISTINCT severidadDano FROM cadanostbl ".
                                "UNION ALL ".
                                "SELECT '".$spChar."' as valor ";

        $rs = fn_ejecuta_query($sqlGetOrigenDanos);

        if(sizeof($rs['root']) > 0){
            $response = createResponseCombos($rs['root']);
        }

        echo $response;
    }

    function capturarDano(){
        $response = "";
        $idDano = "";

        $sqlCheckCombinacionDanos = "SELECT idDano FROM caDanosTbl ".
                                    "WHERE areaDano = '".$_REQUEST['teConsultasAreaDanoTxt']."' ".
                                    "AND tipoDano = '".$_REQUEST['teConsultasTipoDanoTxt']."' ".
                                    "AND severidadDano = '".$_REQUEST['teConsultasSeveridadDanoTxt']."'";

        $rs = fn_ejecuta_query($sqlCheckCombinacionDanos);

        if ($rs['records'] > 0) {
            $idDano = $rs['root'][0]['idDano'];

            $data = addDanosVics($_REQUEST['teConsultasDanoOrigenHdn'], $_REQUEST['teConsultasVinHdn'], $_REQUEST['teConsultasNumDanosHdn'],
                                $idDano, $_REQUEST['teConsultasCentroDistHdn']);

            if ($data['success']) {
                $response = "1|".$data['successMessage']."|";
            } else {
                $response = "0|".$data['errorMessage']."|";
            }
        } else {
            $response = "0|No existe esta combinacion de daños|";
        }

        echo $response;
    }

    /*******************************************************************
    *
    *                       RECEPCION DE VIAJE
    *
    ********************************************************************/

    function getDatosRecepcionViaje(){
        $response = "";

        global $spChar;

        $sqlGetOrigenDanos =    "SELECT distribuidorCentro AS valor ".
                                "FROM caDistribuidoresCentrosTbl WHERE tipoDistribuidor = 'CD' ".
                                "UNION ALL ".
                                "SELECT '".$spChar."' ".
                                "UNION ALL ".
                                "SELECT compania FROM caCompaniasTbl ".
                                "UNION ALL ".
                                "SELECT '".$spChar."' ";

        $rs = fn_ejecuta_query($sqlGetOrigenDanos);

        if(sizeof($rs['root']) > 0){
            $response = createResponseCombos($rs['root']);
        }

        echo $response;
    }

    function validarRecepcionViaje(){
        $response = "";

        $_REQUEST['trViajesTractoresCompaniaHdn'] = $_REQUEST['teConsultasCompaniaHdn'];
        $_REQUEST['trViajesTractoresFolioTxt'] = $_REQUEST['teConsultasNumTalonHdn'];
        $_REQUEST['trViajesTractoresCentroDistHdn'] = $_REQUEST['teConsultasOrigenHdn'];
        $rsTalonViaje = getTalonesViaje();

        if ($rsTalonViaje['records'] > 0) {

            if ($rsTalonViaje['root'][0]['tipoDistribuidor'] == 'CD') { //Solo pueden recibir a PTO. Transferencia

                $rsTalon['root'][0]['idTalon'] = $rsTalonViaje['root'][0]['idTalon'];
                $rsTalon['root'][0]['distribuidor'] = $rsTalonViaje['root'][0]['distribuidor'];
                $rsTalon['root'][0]['numeroUnidades'] = $rsTalonViaje['root'][0]['numeroUnidades'];
                $rsTalon['root'][0]['fechaEvento'] = $rsTalonViaje['root'][0]['fechaEvento'];

                $response = createResponse($rsTalon['root']);

                $response = "1|".$response;

            } else { // DI

                echo "0|Solo se puede recibir en Distribuidor|";
            }
        }

        echo $response;


        /*$sqlValidarRecepcion = "SELECT idTalon, distribuidor, numeroUnidades, fechaEvento FROM trTalonesViajesTbl ".
                               "WHERE centroDistribucion = '".$_REQUEST['teConsultasOrigenHdn']."' ".
                               "AND companiaRemitente = '".$_REQUEST['teConsultasCompaniaHdn']."' ".
                               "AND folio = '".$_REQUEST['teConsultasNumTalonHdn']."' ";

        $rs = fn_ejecuta_query($sqlValidarRecepcion);

        if(sizeof($rs['root']) > 0){
            $response = createResponse($rs['root']);
        }

        echo $response;*/
    }

    function recepcionViaje(){
        $response = "";
        $success = true;
        $requeridos = array();
        $unidadesError = array();

        if($_REQUEST['teConsultasIdTalonHdn'] == ""){
            array_push($requeridos, 'teConsultasIdTalonHdn');
            $success = false;
        }
        if($_REQUEST['teConsultasCentroDistHdn'] == ""){
            array_push($requeridos, 'teConsultasCentroDistHdn');
            $success = false;
        }
        if($_REQUEST['teConsultasIdUsuarioHdn'] == ""){
            array_push($requeridos, 'teConsultasIdUsuarioHdn');
            $success = false;
        }

        if ($success) {
            //Se actualiza el estatus del talon
            $sqlUpdEstatusTalonStr = "UPDATE trTalonesViajesTbl ".
                                     "SET claveMovimiento = 'TT' ".
                                     "WHERE idTalon = ".$_REQUEST['teConsultasIdTalonHdn'];

            $rs = fn_ejecuta_query($sqlUpdEstatusTalonStr);

            $sqlCheckEstatusTalonesStr = "SELECT claveMovimiento FROM trTalonesViajesTbl ".
                                         "WHERE idViajeTractor = (SELECT idViajeTractor FROM trTalonesViajesTbl ".
                                            "WHERE idTalon = ".$_REQUEST['teConsultasIdTalonHdn'].")";

            $rs = fn_ejecuta_query($sqlCheckEstatusTalonesStr);
            $isConcluido = true;

            for ($nInt=0; $nInt < sizeof($rs['root']); $nInt++) {
                if($rs['root'][$nInt]['claveMovimiento'] != 'TD' && $rs['root'][$nInt]['claveMovimiento'] != 'TT'){
                    $isConcluido = false;
                    break;
                }
            }

            //Si todos los talones estan entregados se actualiza el estatus del viaje
            if($isConcluido){
                $sqlUpdEstatusViajeStr = "UPDATE trViajesTractoresTbl ".
                                         "SET claveMovimiento = 'VE' ".
                                         "WHERE idViajeTractor = (SELECT idViajeTractor FROM trTalonesViajesTbl ".
                                            "WHERE idTalon = ".$_REQUEST['teConsultasIdTalonHdn'].")";

                $rs = fn_ejecuta_query($sqlUpdEstatusViajeStr);

            }

            //Se insertan los estatus de entregada y de recibida para cada unidad del talon
            $sqlGetUnidadesTalon = "SELECT udt.vin, tv.centroDistribucion AS centroOrigen, hu.distribuidor, idTarifa, claveChofer ".
                                    "FROM trunidadesdetallestalonestbl udt ".
                                    "INNER JOIN alhistoricounidadestbl hu ".
                                        "ON hu.vin = udt.vin ".
                                        "AND hu.fechaEvento = ( ".
                                            "SELECT MAX(hu2.fechaEvento) ".
                                            "FROM alHistoricoUnidadesTbl hu2 ".
                                            "WHERE hu2.vin = hu.vin ".
                                        ") ".
                                    "INNER JOIN trtalonesviajestbl tv ON udt.idTalon = tv.idTalon ".
                                    "WHERE udt.idTalon = ".$_REQUEST['teConsultasIdTalonHdn'];

            $rs = fn_ejecuta_query($sqlGetUnidadesTalon);

            foreach ($rs['root'] as $unidad) {
                //ENTREGADA EN PUNTO DE TRASNFERENCIA
                $data = addHistoricoUnidad($unidad["centroOrigen"], $unidad["vin"], 'OP',
                                        $unidad["distribuidor"], $unidad["idTarifa"], $_REQUEST['teConsultasCentroDistHdn'],
                                        replaceEmptyNull($unidad["chofer"]), '', $_REQUEST['teConsultasIdUsuarioHdn']);

                if($data['success']){
                    //RECIBIDA EN PUNTO DE TRASNFERENCIA
                    $data = addHistoricoUnidad($_REQUEST['teConsultasCentroDistHdn'], $unidad["vin"], 'RP',
                                            $unidad["distribuidor"], $unidad["idTarifa"], $_REQUEST['teConsultasCentroDistHdn'],
                                            replaceEmptyNull($unidad["chofer"]), '', $_REQUEST['teConsultasIdUsuarioHdn']);

                    if(!$data['success']){
                        array_push($unidadesError, $unidad["vin"]);
                        $success = false;
                    }
                } else {
                    array_push($unidadesError, $unidad["vin"]);
                    $success = false;
                }

            }

            if($success){
                $response = "1|Viaje Recibido Correctamente|";
            } else {
                $response = "0|Unidades con error al recibir viaje:\n";

                foreach ($unidadesError as $unidad) {
                    $response .= $unidad."\n";
                }

                $response .= "|";
            }
        } else {
            $response = "0|Campos Requeridos:\n";

            foreach ($requeridos as $requerido) {
                $response .= $requerido."\n";
            }

            $response .= "|";
        }

        echo $response;
    }

    /*******************************************************************
    *
    *                           EMBARCADAS
    *
    ********************************************************************/

    function getTractoresEmbarcadas(){
        $response = "";

        $sqlGetTractores = "SELECT  tr.idTractor, tr.tractor, tr.compania, vt.idViajeTractor, ".
                            "(SELECT COUNT(*) FROM trUnidadesEmbarcadasTbl ue ".
                                "WHERE vt.idViajeTractor = ue.idViajeTractor) AS unidades ".
                            "FROM    caTractoresTbl tr, trViajesTractoresTbl vt ".
                            "WHERE   tr.idTractor = vt.idTractor ".
                            "AND vt.idViajeTractor = ( ".
                                "SELECT MAX(vt2.idViajeTractor) ".
                                "FROM trViajesTractoresTbl vt2 ".
                                "WHERE vt2.idTractor = tr.idTractor ".
                            ") ".
                            "AND vt.claveMovimiento = 'VV' ".
                            "AND vt.centroDistribucion = '".$_REQUEST['teConsultasCentroDistHdn']."' ".
                            "AND ( SELECT COUNT(*) FROM trUnidadesEmbarcadasTbl ue ".
                                "WHERE vt.idViajeTractor = ue.idViajeTractor ".
                            ") > 0 ";

        $rs = fn_ejecuta_query($sqlGetTractores);

        if(sizeof($rs['root']) > 0){
            $response = createResponse($rs['root']);
        }

        echo $response;
    }

    function validarEmbarcada(){
        $response = "";
        $success = true;

        if($_REQUEST['teConsultasVinHdn'] == ""){
            $success = false;
        }
        if($_REQUEST['teConsultasIdViajeHdn'] == ""){
            $success = false;
        }

        if($success){
            $sqlValidarEmbarcada = "SELECT ue.idUnidadEmbarque FROM trUnidadesEmbarcadasTbl ue ".
                                    "WHERE   ue.idViajeTractor = ".$_REQUEST['teConsultasIdViajeHdn']." ".
                                    "AND ue.vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                                    "AND ue.claveMovimiento = 'VV' ";

            $rs = fn_ejecuta_query($sqlValidarEmbarcada);

            if(sizeof($rs['root']) > 0){
                $response = "1|".$rs['root'][0]['idUnidadEmbarque']."|";
            } else {
                $response = "0|Unidad no registrada en Previaje|";
            }
        } else {
            $response = "0|Faltan Campos Requeridos|";
        }


        echo $response;
    }

    function actualizarEmbarque(){
        $response = "";

        $embArr = explode('|', substr($_REQUEST['teConsultasEmbarquesHdn'], 0, -1));

        if(sizeof($embArr) > 0){
            $embarques = "";
            foreach ($embArr as $embarque) {
                $embarques .= $embarque.",";
            }

            $embarques = substr($embarques, 0, -1);

            $sqlUpdEmbarques = "UPDATE trUnidadesEmbarcadasTbl SET claveMovimiento = 'VC' ".
                                "WHERE idUnidadEmbarque IN (".$embarques.")";

            fn_ejecuta_query($sqlUpdEmbarques);

            $response = "1|Embarque Validado Correctamente|";
        } else {
            $response = "0|No hay unidades a actualizar|";
        }

        echo $response;
    }

    /*******************************************************************
    *
    *                         SALIDAS GENERALES
    *
    ********************************************************************/

    function validarUnidadSalidasGen(){
        $response = "";
       /* $alHistoricoUnidades = "SELECT * FROM alhistoricounidadestbl ".
                                " WHERE vin ='".$_REQUEST['teConsultasVinHdn']."'";
        $rsHist = fn_ejecuta_query($alHistoricoUnidades);

        if ($rsHist['root'][0]['claveMovimiento'] == 'IC') {
                echo '0|Unidad con Salida|';
            } else {*/

                $sqlValidarSalida = "SELECT ".
                                    "(SELECT ud2.claveMovimiento ".
                                    "FROM alUltimoDetalleTbl ud2 ".
                                        "WHERE ud2.vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                                        "AND ud2.centroDistribucion = 'CMDAT' ".
                                    ") AS claveMovimiento, ".
                                    "(SELECT 1 FROM alUltimoDetalleTbl ud ".
                                        "WHERE ud.vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                                        "AND ud.centroDistribucion = 'CMDAT' ".
                                        "AND ud.claveMovimiento IN ( ".
                                            "SELECT valor FROM cageneralestbl ".
                                                "WHERE tabla = 'salidasGenerales' ".
                                                "AND columna = 'validos') ".
                                    ") AS estatusValido, ".
                                    "(SELECT '1' ".
                                    "FROM alUnidadesTmp ".
                                    "WHERE vin ='".$_REQUEST['teConsultasVinHdn']."') AS unidadBloqueada ;";

                $rs = fn_ejecuta_query($sqlValidarSalida);

                if (count($rs['root']) > 0) {
                    if ($rs['root'][0]['estatusValido'] == '1' && $rs['root'][0]['unidadBloqueada'] != '1') {
                        bloquearUnidadEscaner($_REQUEST['teConsultasVinHdn'], 'ESCANER',
                                              $_REQUEST['teConsultasIdUsuarioHdn'], $_REQUEST['teConsultasUsuarioIpHdn']);
                    }

                    $response = createResponse($rs['root']);
                }

                echo $response;
           // }

    }

    //TAT
    function getTAT(){
        $response = "";

        $sqlGetDatosPresentada = "SELECT dc.distribuidorCentro ".
                                 "FROM caDistribuidoresCentrosTbl dc ".
                                 "WHERE dc.tipoDistribuidor IN ('DI', 'DX');";

        $rs = fn_ejecuta_query($sqlGetDatosPresentada);

        if(sizeof($rs['root']) > 0){
            $response = createResponseCombos($rs['root']);
        }

        //echo $response;
    }

    function generarTAT(){
        $response = "";

        $sqlValidarUnidad = "SELECT 1 FROM alUnidadesDetenidasTbl ".
                            "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                            "AND fechaFinal IS NULL ";

        $rs = fn_ejecuta_query($sqlValidarUnidad);

        if(sizeof($rs['root']) == 0){
            $sqlGetUltimoHistorico = "SELECT * FROM alhistoricounidadestbl ".
                                        "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                                        "ORDER BY fechaEvento DESC LIMIT 1 ";

            $rs = fn_ejecuta_query($sqlGetUltimoHistorico);
            if(sizeof($rs['root']) > 0){
                $datosUnidad = $rs['root'][0];

                $data = addHistoricoUnidad('CMDAT',$_REQUEST['teConsultasVinHdn'],'ST',
                                $_REQUEST['teConsultasDistribuidorTxt'],$datosUnidad['idTarifa'],$datosUnidad['localizacionUnidad'],
                                $datosUnidad['claveChofer'],'',$_REQUEST['teConsultasIdUsuarioHdn']);

                if($data['success']){
                    $response = "1|Salida a TAT Generada Correctamente|";
                } else {
                    $response = "0|".$data['errorMessage']."|";
                }
            }
        } else {
            $response = "0|Unidad Invalida - Unidad Detenida|";
        }
    }


    function getCampaniasCentrosDistribucion() {

        $sqlCampanias = "SELECT dc.distribuidorCentro, dc.descripcionCentro ".
                        "FROM caDistribuidoresCentrosTbl dc ".
                        "WHERE dc.tipoDistribuidor = 'CA' ".
                        "AND dc.sucursalDe = '".$_REQUEST['teConsultasCentroDistHdn']."';";

        $rs = fn_ejecuta_query($sqlCampanias);

        if ($rs['records'] > 0)
            $response = createResponse($rs['root']);
        else
            $response = '0|NO EXISTEN CAMPAÑAS|';

        echo $response;
    }


    function entradaCampania() {
        $vin = $_REQUEST['teConsultasVinHdn'] = '1C4RJEBG4FC860809';
        //Valida que no exista cve movimiento EO en la unidad;
        $sqlValidaUnidad = "SELECT ((SELECT 1 FROM alHistoricoUnidadesTbl WHERE vin = '$vin' AND claveMovimiento = 'EO') is not null) as existeEO";

        $rs = fn_ejecuta_query($sqlValidaUnidad);

        if ($rs['root'][0]['existeEO'] == '1') {
            echo '1|valido';

        } else {
            echo '0|invalido';
        }
    }

    function setSalidaGeneral() {


        $campania = $_REQUEST['teConsultasCampaniaHdn'];
        $numPatio = $_REQUEST['teConsultasPatioNumHdn'];
        $usuario = $_REQUEST['teConsultasIdUsuarioHdn'];
        $centroDist = $_REQUEST['teConsultasCentroDistHdn'];
        $tipoSalida = $_REQUEST['teConsultasTipoSalidaHdn'];
        $distribuidor = $_REQUEST['teConsultasDistribuidorHdn'];
        $vin = $_REQUEST['teConsultasVinHdn'];
        $cves = array();

        //Obtiene el ultimo estatus de la unidad
        $sqlGetUnidad = "SELECT hu1.* FROM alHistoricoUnidadesTbl hu1 ".
                        "WHERE hu1.vin = '$vin' ".
                        "AND hu1.fechaEvento = ".
                        "(SELECT MAX(hu2.fechaEvento) FROM alHistoricoUnidadesTbl hu2 ".
                        "WHERE hu2.vin = '$vin' ".
                        "AND hu2.centroDistribucion = 'CMDAT');";

        $rsUnidad = fn_ejecuta_query($sqlGetUnidad);
        $rsUnidad = $rsUnidad['root'][0];

        if ($centroDist == 'CDSAL') {

            //Valida no exista la campaña a elegir para esa unidad
            $sqlValidaUnidad = "SELECT claveMovimiento FROM alHistoricoUnidadesTbl ".
                                "WHERE vin = '$_REQUEST[teConsultasVinHdn]' ".
                                "AND claveMovimiento = 'EO' ".
                                "AND distribuidor = '$campania' ".
                                "AND centroDistribucion = '$_REQUEST[teConsultasCentroDistHdn]';";

            switch ($tipoSalida) {
                case 'A'://CAMPA A CAMPA

                    $rsEO = fn_ejecuta_query($sqlValidaUnidad);

                    if (count($rsEO['root']) < 1) { //Si no existe campaña para esa unidad la genera

                        $data = addHistoricoUnidad($centroDist, $vin, 'SI', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);

                        $data = addHistoricoUnidad($centroDist, $vin, 'EO', $campania, $rsUnidad['idTarifa'],
                                                   'PSAL'.$numPatio, $rsUnidad['claveChofer'],
                                                   '', $usuario, 1);
                    } else {
                        $data = array('success' => false, 'errorMessage' => 'CAMPAÑA YA EXISTENTE PARA ESTA UNIDAD');
                    }

                    break;
                case 'E'://CAM-CONT E/UPFIT
                        if ($rsUnidad['claveMovimiento'] == 'EO')
                            $cves = array('SI', 'UE');
                        else if ($rsUnidad['claveMovimiento'] == 'CO')
                            $cves = array('SO', 'UE');
                        for ($i=0; $i < count($cves); $i++) {
                            $data = addHistoricoUnidad('CMDAT', $vin, $cves[$i], $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                       'CMDAT', $rsUnidad['claveChofer'],
                                                       'UNIDAD DE COMODATO', $usuario, $i);
                        }
                    break;
                case 'P'://PLANTA
                    $data = addHistoricoUnidad('CMDAT', $vin, 'SP', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                               $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                               'UNIDAD DE COMODATO', $usuario, 0);
                    break;
                case 'B'://PLANTA
                    $data = addHistoricoUnidad('CMDAT', $vin, 'PB', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                               $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                               'UNIDAD DE COMODATO', $usuario, 0);
                    break;
                case 'I'://PLANTA
                    $data = addHistoricoUnidad('CMDAT', $vin, 'SI', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                               $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                               'UNIDAD DE COMODATO', $usuario, 0);
                    break;
                case 'R':// DE CAMPAÑA
                    $data = addHistoricoUnidad('CMDAT', $vin, 'SI', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                               'CMDAT', $rsUnidad['claveChofer'],
                                               'UNIDAD DE COMODATO', $usuario, 0);

                    $data = addHistoricoUnidad('CMDAT', $vin, 'CO', "CMD", $rsUnidad['idTarifa'],
                                               $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                               'UNIDAD DE COMODATO', $usuario, 1);
                    break;
                case 'N'://SAL-CONT/ENT-CAM
                    $rsEO = fn_ejecuta_query($sqlValidaUnidad);
                    if (count($rsEO['root']) < 1) { //Si no existe campaña para esa unidad la genera
                        $data = addHistoricoUnidad('CMDAT', $vin, 'SO', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   'CMDAT', $rsUnidad['claveChofer'],
                                                   'UNIDAD DE COMODATO', $usuario, 0);

                        $data = addHistoricoUnidad('CMDAT', $vin, 'EO', "CMD", $rsUnidad['idTarifa'],
                                                   'CMDAT'.$numPatio, $rsUnidad['claveChofer'],
                                                   'UNIDAD DE COMODATO', $usuario, 1);
                    } else {
                        $data = array('success' => false, 'errorMessage' => 'CAMPAÑA YA EXISTENTE PARA ESTA UNIDAD');
                    }
                    break;
                case 'K'://CAMBIO PATIO FAC
                    $data = addHistoricoUnidad('CMDAT', $vin, 'PS', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                               'CMDAT', $rsUnidad['claveChofer'],
                                               'UNIDAD DE COMODATO', $usuario, 0);

                    if ($rsUnidad['claveMovimiento'] == 'SL')
                        $cves = array('DP', 'SL');
                    else if ($rsUnidad['claveMovimiento'] == 'DT')
                        $cves = array('DP');
                    else if ($rsUnidad['claveMovimiento'] == 'HD')
                        $cves = array('DP', 'HD');
                    else
                        $cves = array('CP');
                    for ($i=0; $i < count($cves) ; $i++) {
                        if ($i > 0)
                            $localidad = $rsUnidad['localizacionUnidad'];
                        else
                            $localidad = 'PSAL'.$numPatio;

                        $data = addHistoricoUnidad($centroDist, $vin, $cves[$i], $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $localidad, $rsUnidad['claveChofer'],
                                                   '', $usuario, $i + 1);
                    }
                    break;
                case 'U'://UPFIT S/PL, CA, CO
                    $opc = $_REQUEST['teConsultasUpfitOpcionHdn'];
                    $rsEO = fn_ejecuta_query($sqlValidaUnidad);
                    if (count($rsEO['root']) < 1) { //Si no existe campaña para esa unidad la genera
                        if ($opc == 'A') {//PLANTA
                            $data = addHistoricoUnidad($centroDist, $vin, 'UP', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                       $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                       '', $usuario, 0);
                        } else if ($opc == 'B') {//CAMPAÑA
                            $data = addHistoricoUnidad($centroDist, $vin, 'US', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                       $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                       '', $usuario, 0);

                            $data = addHistoricoUnidad($centroDist, $vin, 'EO', $campania, $rsUnidad['idTarifa'],
                                                       'PSAL'.$numPatio, $rsUnidad['claveChofer'],
                                                       '', $usuario, 1);
                        } else { // CONTENCION
                            $data = addHistoricoUnidad($centroDist, $vin, 'US', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                       $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                       '', $usuario, 0);

                            $data = addHistoricoUnidad($centroDist, $vin, 'UP', 'CMD', $rsUnidad['idTarifa'],
                                                       'PSAL'.$numPatio, $rsUnidad['claveChofer'],
                                                       '', $usuario, 1);
                        }
                    } else {
                        $data = array('success' => false, 'errorMessage' => 'CAMPAÑA YA EXISTENTE PARA ESTA UNIDAD');
                    }
                    break;
                default:
                    $data =  array('success' => false, 'errorMessage' => 'Opción Inválida');
            }
        } else { //CDTOL

            $sqlValidaUnidad = "SELECT * FROM alHistoricoUnidadesTbl ".
                                "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' ";
            $rsClaveMov =fn_ejecuta_query($sqlValidaUnidad);

            if ($rsClaveMov == 'IC') {
                $conHold =
                 1;
            }else{
                $sqlCountHold ="SELECT count(claveMovimiento) as count FROM alHistoricoUnidadesTbl ".
                                "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                                " AND observaciones = 'UNIDAD CON HOLD'";
                $rsCountHold =fn_ejecuta_query($sqlCountHold);

                $sqlCountLiberada ="SELECT count(claveMovimiento) as count FROM alHistoricoUnidadesTbl ".
                                "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                                    "AND observaciones = 'LIBERADA DE HOLD'";
                $rstCountLiberada = fn_ejecuta_query($sqlCountLiberada);

                if ($rsCountHold['root'][0]['count'] == $rstCountLiberada['root'][0]['count']) {
                    $conHold = 1;
                }else{
                    $conHold = 0;
                }


            }


            if ($conHold == 1) {
                switch ($tipoSalida) {
                    case 'T':

                        $data = addHistoricoUnidad('CMDAT', $vin, 'ST', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);
                        break;
                    case 'E':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'ET', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);
                        break;
                      case 'A':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'SA', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);
                        break;
                     case 'B':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'PB', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);
                        break;
                     case 'I':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'SI', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);
                        break;
                    case 'P':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'SP', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);
                        break;
                    case 'X':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'SX', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                           $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                           '', $usuario, 0);
                        break;
                    case 'O':
                        //Válida que no haya un EP
                        $sqlEP = "SELECT claveMovimiento FROM alHistoricoUnidadesTbl WHERE vin = '$vin' AND claveMovimiento = 'EP'";
                        $rsEP = fn_ejecuta_query($sqlEP);

                        if (count($rs['root']) < 1) { //Valida que no tenga ya EP Entrada a Patio
                            $data = addHistoricoUnidad('CMDAT', $vin, 'SO', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                       $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                       '', $usuario, 0);

                            $data = addHistoricoUnidad('CMDAT', $vin, 'EO', "CMD", $rsUnidad['idTarifa'],
                                                       'PTOL'.$numPatio, $rsUnidad['claveChofer'],
                                                       '', $usuario, 1);
                        } else {
                            $data = array('success'=> false, 'errorMessage'=>'LA UNIDAD YA TIENE ENTRADA A PATIO');
                        }
                        break;
                    case 'V':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'SV', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);
                        break;
                    case 'M':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'SM', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);
                        break;
                    case 'K':
                        $data = addHistoricoUnidad('CMDAT', $vin, 'PS', $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                   $rsUnidad['localizacionUnidad'], $rsUnidad['claveChofer'],
                                                   '', $usuario, 0);

                        if ($rsUnidad['claveMovimiento'] == 'SL')
                            $cves = array('DP', 'SL');
                        else if ($rsUnidad['claveMovimiento'] == 'DT')
                            $cves = array('DP');
                        else if ($rsUnidad['claveMovimiento'] == 'HD')
                            $cves = array('DP', 'HD');
                        else
                            $cves = array('CP');
                        for ($i=0; $i < count($cves) ; $i++) {
                            if ($i > 0)
                                $localidad = $rsUnidad['localizacionUnidad'];
                            else
                                $localidad = 'PTOL'.$numPatio;

                            $data = addHistoricoUnidad($centroDist, $vin, $cves[$i], $rsUnidad['distribuidor'], $rsUnidad['idTarifa'],
                                                       $localidad, $rsUnidad['claveChofer'],
                                                       '', $usuario, $i + 1);
                        }
                        break;
                    default:
                        $data =  array('success' => false, 'errorMessage' => 'Opción Inválida');
                }
            }
            else{
                echo '0|Salida No Permitida|';
            }
        }

        if ($data['success'] == true) {
            //Desbloquear Unidad
            $sqlDesbloquearUnidad = "DELETE FROM alUnidadesTmp WHERE vin = '$vin'";
            fn_ejecuta_query($sqlDesbloquearUnidad);

            $sqlDltLocalizacion =   "UPDATE alLocalizacionPatiosTbl SET vin = NULL , estatus ='DI' ".
                                 "WHERE vin =  '".$vin."'";
            fn_ejecuta_query($sqlDltLocalizacion);

            $delPmp = "DELETE FROM alactividadespmptbl WHERE centroDistribucion ='CMDAT' and vin='".$vin."' and claveEstatus ='PE'";
            fn_ejecuta_query($delPmp);

            echo '1|Salida realizada Correctamente|';
        } else {
            echo '0|'.$data['errorMessage'].'|';
        }
    }


    /*******************************************************************
    *
    *                    ACTUALIZACION DE UBICACION
    *
    ********************************************************************/

    function getPatios(){
        $response = "";

        $sqlGetDatosPresentada =    "SELECT  dc.distribuidorCentro ".
                                    "FROM    caDistribuidoresCentrosTbl dc ".
                                    "WHERE   dc.tipoDistribuidor = 'PA' ".
                                    "AND     dc.sucursalDe = '".$_REQUEST['teConsultasCentroDistHdn']."'";

        $rs = fn_ejecuta_query($sqlGetDatosPresentada);

        if(sizeof($rs['root']) > 0){
            $response = createResponseCombos($rs['root']);
        }

        echo $response;
    }

    function getDatosFila(){
        $response = "";

        $sqlGetDatosFila = "SELECT  COUNT(lp.idlocalizacion) AS ocupados, ".
                                    "(SELECT COUNT(lp2.idlocalizacion) FROM alLocalizacionPatiosTbl lp2 ".
                                        "WHERE   lp2.patio = '".$_REQUEST['teConsultasPatioHdn']."' ".
                                        "AND     lp2.fila = '".$_REQUEST['teConsultasFilaHdn']."' ".
                                        "AND     lp2.vin IS NULL ".
                                    ") AS libres, ".
                                    "(SELECT COUNT(lp2.idlocalizacion) FROM alLocalizacionPatiosTbl lp2 ".
                                        "WHERE   lp2.patio = '".$_REQUEST['teConsultasPatioHdn']."' ".
                                        "AND     lp2.fila = '".$_REQUEST['teConsultasFilaHdn']."' ".
                                        "AND     lp2.estatus = 'B' ".
                                    ") AS bloqueados ".
                            "FROM    alLocalizacionPatiosTbl lp ".
                            "WHERE   lp.patio = '".$_REQUEST['teConsultasPatioHdn']."' ".
                            "AND     lp.fila = '".$_REQUEST['teConsultasFilaHdn']."' ".
                            "AND     lp.vin IS NOT NULL ";

        $rs = fn_ejecuta_query($sqlGetDatosFila);

        if (sizeof($rs['root']) > 0) {
            $response = createResponse($rs['root']);
        }

        echo $response;
    }

    function reordenarFila(){
        $response = "";

        $sqlGetOcupados = "SELECT   lp.idlocalizacion, lp.vin, lp.orden ".
                            "FROM    alLocalizacionPatiosTbl lp ".
                            "WHERE   lp.patio = '".$_REQUEST['teConsultasPatioHdn']."' ".
                            "AND     lp.fila = '".$_REQUEST['teConsultasFilaHdn']."' ".
                            "AND     vin IS NOT NULL ".
                            "ORDER BY orden ";

        $rs = fn_ejecuta_query($sqlGetOcupados);

        if(sizeof($rs['root']) > 0){
            foreach ($rs['root'] as $unidad) {
                //OBTIENE EL LUGAR PRIMERO
                $sqlGetPrimerLugar = "SELECT idlocalizacion, orden FROM alLocalizacionPatiosTbl lp ".
                                     "WHERE lp.patio = '".$_REQUEST['teConsultasPatioHdn']."' ".
                                     "AND lp.fila = '".$_REQUEST['teConsultasFilaHdn']."' ".
                                     "AND vin IS NULL ORDER BY orden LIMIT 1 ";

                $rs = fn_ejecuta_query($sqlGetPrimerLugar);

                if(sizeof($rs['root']) > 0){
                    $primerLugar = $rs['root'][0]['idlocalizacion'];
                    $orden = $rs['root'][0]['orden'];
                } else {
                    $response = "0|No se encontro un lugar vacio|";
                    break;
                }

                if(intval($unidad['orden']) > intval($orden)) {
                    //INSERTA EN EL PRIMER LUGAR VACIO
                    $sqlInsert = "UPDATE alLocalizacionPatiosTbl ".
                                 "SET vin = '".$unidad['vin']."' ".
                                 "WHERE idlocalizacion = ".$primerLugar;

                    fn_ejecuta_query($sqlInsert);


                    //SE ELIMINA DE DONDE ESTABA ANTES
                    $sqlDelete = "UPDATE alLocalizacionPatiosTbl ".
                                 "SET vin = NULL ".
                                 "WHERE idlocalizacion = ".$unidad['idlocalizacion'];


                    fn_ejecuta_query($sqlDelete);
                }

            }

            $response = "1|Reorden Generado Correctamente|";
        } else {
            $response = "0|No se encontraron unidades en esta fila|";
        }


        echo $response;
    }
    function bloquearUnidadEscaner($vin, $modulo, $idUsuario, $ip) {
        $a = array();
        $e = array();
        $a['success'] = true;

        if($vin == ""){
            $e[] = array('id'=>'VinHdn','msg'=>getRequerido());
            $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
        if ($a['success'] == true) {
/////////
            $alHistoricoUnidades = "SELECT * FROM alUltimoDetalleTbl ".
                                " WHERE vin ='".$_REQUEST['teConsultasVinHdn']."'";
            $rsHist = fn_ejecuta_query($alHistoricoUnidades);

            if ($rsHist['root'][0]['claveMovimiento'] == 'IC') {
                $sqlBloquearUnidadStr = "INSERT INTO alUnidadesTmp ".
                                   "VALUES(".
                                   "'".$vin."',".
                                   "'".substr($vin, -8)."',".
                                   "'".$modulo."',".
                                   "'".$idUsuario."',".
                                   "'".$ip."',".
                                   "'".date("Y-m-d H:i:s")."')";

            $rs = fn_ejecuta_query($sqlBloquearUnidadStr);
            } else {
                                echo '0|Unidad con Salida|';


        }

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                $a['sql'] = $sqlBloquearUnidadStr;
                $a['successMessage'] = getUnidadesBloquearMsg();
                $a['root'][0]['bloqueo'] = true;
            } else {
                $a['errorMsg'] = getErrorBloquearUnidad();
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlBloquearUnidadStr;
            }
        }
    }

    /*******************************************************************
    *
    *                        ENTRADAS Y SALIDAS
    *
    ********************************************************************/

    function validarPatio(){
        $response = "";

        $sqlValidarPatio =  "SELECT patio FROM allocalizacionpatiostbl ".
                            "WHERE patio IN ( ".
                                "SELECT distribuidorCentro FROM caDistribuidoresCentrosTbl ".
                                "WHERE sucursalDe = '".$_REQUEST['teConsultasCentroDistHdn']."' ".
                            ") ".
                            "AND patio LIKE '%".$_REQUEST['teConsultasPatioNumHdn']."' ".
                            "GROUP BY patio";

        $rs = fn_ejecuta_query($sqlValidarPatio);

        if(sizeof($rs['root']) > 0){
            $response = createResponse($rs['root']);
        }

        echo $response;
    }

    function generarEntradaSalida(){
        $response = "";

        $vin = substr($_REQUEST['teConsultasVinHdn'],-17);

        $sqlCheckVinHistorico =  "SELECT h.distribuidor, h.idTarifa, h.claveChofer, ".
                                 "(SELECT u.simboloUnidad FROM alUnidadesTbl u ".
                                    "WHERE u.vin = h.vin) AS simboloUnidad ".
                                 "FROM alHistoricoUnidadesTbl h ".
                                 "WHERE h.vin = '".$vin."' ".
                                 "AND h.fechaEvento = (SELECT MAX(h2.fechaEvento) ".
                                    "FROM alHistoricoUnidadesTbl h2 ".
                                    "WHERE h2.vin = h.vin)";

        $rs = fn_ejecuta_query($sqlCheckVinHistorico);

        if(sizeof($rs['root']) > 0){
            $datos = $rs['root'][0];

            $sqlCheckPatio =    "SELECT idlocalizacion FROM alLocalizacionPatiosTbl ".
                                "WHERE patio = '".$_REQUEST['teConsultasPatioHdn']."' ".
                                "AND vin = '".$_REQUEST['teConsultasVinHdn']."' ";

            $rs = fn_ejecuta_query($sqlCheckPatio);

            if(sizeof($rs['root']) > 0){
                //SI SE ENCUENTRA EN EL PATIO INDICADO SE LE DA SALIDA
                $data = generarSalida($rs['root'][0]['idlocalizacion'], $vin, $datos, $_REQUEST['teConsultasIdUsuarioHdn']);

                if($data['success']){
                    $response = "1|".$data['successMessage']."|";
                } else {
                    $response = "0|".$data['errorMessage']."|";
                }
            } else {
                //REVISA SI SE ENCUENTRA EN OTRO PATIO
                $sqlCheckPatio =    "SELECT idlocalizacion, patio FROM alLocalizacionPatiosTbl ".
                                    "WHERE patio IN (".
                                        "SELECT distribuidorCentro FROM caDistribuidoresCentrosTbl ".
                                        "WHERE sucursalDe = '".$_REQUEST['teConsultasCentroDistHdn']."' ".
                                    ") ".
                                    "AND vin = '".$vin."' ";

                $rs = fn_ejecuta_query($sqlCheckPatio);

                if(sizeof($rs['root']) > 0){
                    $data = generarSalida($rs['root'][0]['idlocalizacion'], $_REQUEST['teConsultasCentroDistHdn'], $vin, $datos, $_REQUEST['teConsultasPatioHdn'], $_REQUEST['teConsultasIdUsuarioHdn']);

                    if($data['success']){
                        $data = generarEntrada($rs['root'][0]['idlocalizacion'], $_REQUEST['teConsultasCentroDistHdn'], $vin, $datos, $_REQUEST['teConsultasPatioHdn'], $_REQUEST['teConsultasIdUsuarioHdn']);

                        if($data['success']){
                            $response = "1|".$data['successMessage']."|";
                        } else {
                            $response = "0|".$data['errorMessage']."|";
                        }
                    } else {
                        $response = "0|".$data['errorMessage']."|";
                    }
                } else {
                    //SE LE DA ENTRADA SOLAMENTE
                    $data = generarEntrada($rs['root'][0]['idlocalizacion'], $_REQUEST['teConsultasCentroDistHdn'], $vin, $datos, $_REQUEST['teConsultasIdUsuarioHdn']);

                    if($data['success']){
                        $response = "1|".$data['successMessage']."|";
                    } else {
                        $response = "0|".$data['errorMessage']."|";
                    }
                }
            }
        } else {
            //SI LA UNIDAD NO EXISTE SE CREA CON INGRESO A PATIO CON DATOS TEMPORALES
            $distribuidor = 'M8010';
            $simboloUnidad = 'SIMTEMP';
            $color = 'PAL';

            $data = addUnidad($vin,$distribuidor,$simboloUnidad,$color,$_REQUEST['teConsultasCentroDistHdn'],'IP',
                                1,$_REQUEST['teConsultasPatioHdn'],'','','',0,$_REQUEST['teConsultasIdUsuarioHdn']);

            if($data['success']){
                $_SESSION['usuCompania'] = $centroDist;
                $data = addLocalizacionPatios($simboloUnidad."|", $distribuidor."|", $vin."|");

                if($data['success']){
                    $response = "1|".$data['successMessage']."|";
                } else {
                    $response = "0|".$data['errorMessage']."|";
                }
            } else {
                $response = "0|".$data['errorMessage']."|";
            }
        }



        echo $response;
    }

    function generarSalida($idLocalizacion, $centroDist, $vin, $datos, $patio, $idUsuario){
        $sqlDltLocalizacion =   "UPDATE alLocalizacionPatiosTbl SET vin = NULL ".
                                "WHERE idLocalizacion = ".$idLocalizacion;

        fn_ejecuta_query($sqlDltLocalizacion);

        if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
            $data = addHistoricoUnidad($centroDist,$vin, 'ZP', $datos['distribuidor'], $datos['idTarifa'], $patio,
                                    $datos['claveChofer'], '', $idUsuario);
        } else {
            $data = array('success'=>false, 'errorMessage'=>"Error al buscar unidad en patio");
        }

        $data['successMessage'] = "Salida Generada Correctamente";
        return $data;
    }

    function generarEntrada($idLocalizacion, $centroDist, $vin, $datos, $patio, $idUsuario){
        $_SESSION['usuCompania'] = $centroDist;
        $data = addLocalizacionPatios($datos['simboloUnidad']."|", $datos['distribuidor']."|", $vin."|");

        if($data['success']){
            $data = addHistoricoUnidad($centroDist,$vin, 'IP', $datos['distribuidor'], $datos['idTarifa'], $patio,
                                    $datos['claveChofer'], '', $idUsuario);
        }

        return $data;

    }

    /*-----------------------------------------------
    *
    *  FUNCIONES DE ENTRADA A CONTENCION 23-11-15
    *
    *-----------------------------------------------*/

    function validaVinEntradaContencion() {

        $sqlUnidad = "SELECT vin FROM alUnidadesTbl ".
                     "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."';";

        $rsUnidad = fn_ejecuta_query($sqlUnidad);
        $result = '';
        //1.- Si la Unidad no existe en alUnidadesTbl
        if ($rsUnidad['records'] == 0) {
            $result = vinNoExisteContencion();
        } else {
            $result = vinExisteContencion();
        }

        echo $result;
    }

    function vinExisteContencion() {

        $_REQUEST['alUnidadesVinHdn'] = $_REQUEST['teConsultasVinHdn'];
        $rsUnidad = getUnidades();
        $rsUnidad = $rsUnidad['root'][0];
        $result = '';
        $error = array('EO' => 'campaña', 'UE' => 'contención');
        $validos = array('SO', 'SU', 'ST', 'UP', 'PM', 'SP');
        //Obtiene el ultimo movimiento de la unidad

        if (in_array($rsUnidad['cveMovHistorico'], array_keys($error))) {
            $result = "0|No se puede ingresar Unidad recibida en undercoating o ".$error[$rsUnidad['cveMovHistorico']]."|";
        } else {
            if($rsUnidad['cveMovHistorico'] == 'SP') {
                $result =  "1|Unidad se reingresa a Campaña";
            } else {
                if (!in_array($rsUnidad['cveMovHistorico'], $validos)) {
                    $result = '0|Unidad ya recibida con otro estatus';
                } else {
                    $result = '1|Continua|';
                }
            }
        }

        return $result;
    }

    function vinNoExisteContencion($vin) {
        $sqlRetrabajo = "SELECT * FROM alRetrabajoUnidadesTbl ".
                        "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."';";

        $rsRetrabajo = fn_ejecuta_query($sqlRetrabajo);
        $result = '';
        //Si existe Retrabajo se pregunta si desea continuar;
        if (count($rsRetrabajo['root']) > 0) {

            $result = '1|0|Unidad en Retrabajo \n ¿Desea Continuar?|';

        } else { //Si no está en retrabajo se obtiene de la 660

            $sql660 =   "SELECT a6.vin, a6.scaccode, a6.prodstatus, a6.dealerid, a6.model, ".
                        "a6.colorcode, a6.vupdate, a6.vuptime FROM al660tbl a6 ".
                        "WHERE a6.vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                        "AND a6.dealerid IS NOT NULL ".
                        "AND a6.dealerid <> '' ".
                        "AND a6.scaccode = 'XTRA' ".
                        "AND a6.orisplc IS NOT NULL ".
                        "AND a6.orisplc <> '' ".
                        "AND a6.dessplc IS NOT NULL ".
                        "AND a6.dessplc <> '' ".
                        "AND (a6.dealerid IS NOT NULL OR a6.dealerid <> '') ".
                        "AND a6.von NOT LIKE '1111%' ".
                        "AND a6.von NOT LIKE '2222%' ".
                        "AND a6.von NOT LIKE '1.111%' ".
                        "AND a6.von NOT LIKE '2.222%' ".
                        "AND a6.vupdate =  (select max(a6_2.vupdate) from al660tbl a6_2 ".
                                                    "where a6_2.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                                    "and a6_2.scacCode IN ('MITS', 'XTRA') ".
                                                    "and a6_2.von NOT LIKE '1111%' AND a6_2.von NOT LIKE '2222%' AND a6_2.von NOT LIKE '1.111%' AND a6_2.von NOT LIKE '2.222%' ".
                                                    "group by a6_2.vin) ".
                        "AND a6.vuptime = (select max(a6_3.vuptime) from al660tbl a6_3 ".
                                          "where a6_3.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                          "and a6_3.von NOT LIKE '1111%' AND a6_3.von NOT LIKE '2222%' AND a6_3.von NOT LIKE '1.111%' AND a6_3.von NOT LIKE '2.222%' ".
                                          "and a6_3.vupdate = (select max(a6_4.vupdate) from al660tbl a6_4 ".
                                                              "where a6_4.vin  = '".$_REQUEST['teConsultasVinHdn']."' ".
                                                              "and a6_4.scacCode IN ('MITS', 'XTRA') ".
                                                              "and a6_4.von NOT LIKE '1111%' AND a6_4.von NOT LIKE '2222%' AND a6_4.von NOT LIKE '1.111%' AND a6_4.von NOT LIKE '2.222%' ".
                                                              "group by a6_4.vin) ".
                                          "and a6_3.scacCode = 'XTRA' ".
                                          "group by a6_3.vin) ".
                        "GROUP BY a6.vin ";

            $rs660Unidad = fn_ejecuta_query($sql660);

            if ($rs660Unidad['records'] > 0)
                $result = "1|".$rs660Unidad['root'][0]['model']."|".$rs660Unidad['root'][0]['dealerid']."|";

        }
        return $result;
    }

    function validaSimboloEntradaContencion() {

        $sqlRetrabajo = "SELECT * FROM alRetrabajoUnidadesTbl ".
                        "WHERE simboloUnidad = '".$_REQUEST['teConsultasSimboloHdn']."';";

        $rsRetrabajo = fn_ejecuta_query($sqlRetrabajo);

        //Si existe Retrabajo se pregunta si desea continuar;
        if (count($rsRetrabajo['root']) > 0) {

            echo '1|0|Unidad en Retrabajo \n ¿Desea Continuar?|';

        } else { //Si no está en rertabajo se obtiene de la 660

            echo '1|Continua|';

        }
    }
    //Contencion
    function getDatosContencion() {
        $response = "";
        global $spChar;

        $sqlGetDatosPresentada = "SELECT simboloUnidad AS codigo FROM caSimbolosUnidadesTbl ".
                                    "UNION ALL SELECT '".$spChar."' AS codigo UNION ALL ".
                                    "SELECT distribuidorCentro AS codigo FROM caDistribuidoresCentrosTbl ".
                                        "WHERE tipoDistribuidor = 'DI' OR tipoDistribuidor = 'CA' ".
                                    "UNION ALL SELECT '".$spChar."' AS codigo UNION ALL ".
                                    "SELECT distribuidorCentro AS codigo FROM caDistribuidoresCentrosTbl ".
                                        "WHERE tipoDistribuidor = 'DX' ".
                                    "UNION ALL SELECT '".$spChar."' AS codigo UNION ALL ".
                                    "SELECT tarifa AS codigo FROM caTarifasTbl ".
                                    "UNION ALL SELECT '".$spChar."' AS codigo UNION ALL ".
                                    "SELECT valor AS codigo FROM caGeneralesTbl ".
                                    "WHERE tabla = 'PPCAMPA' AND columna = 'PPCAMPA' ".
                                    "UNION ALL SELECT '".$spChar."' AS codigo ";

                                    //UNION ALL AL FINAL PORQUE LA FUNCION DE LA PANTALLA REMUEVE LOS DOS ULTIMOS CARACTERES

        $rs = fn_ejecuta_query($sqlGetDatosPresentada);

        if(sizeof($rs['root']) > 0){
            $response = createResponseCombos($rs['root']);
        }

        echo $response;
    }

    function entradaContencionDB() {
        //Valida si existe en Cabecero la Unidad
        $a['success'] = true;
        $sqlUnidad = "SELECT vin FROM alUnidadesTbl ".
                     "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."';";
        $rsUnidad = fn_ejecuta_query($sqlUnidad);
        //1.-
        if ($rsUnidad['records'] == 0) {
            $sqlAddUnidadStr = "INSERT INTO alUnidadesTbl (vin, avanzada, distribuidor, ".
                                                          "simboloUnidad, color, folioRepuve) ".
                                   "VALUES(".
                                   "'".$_REQUEST['teConsultasVinHdn']."',".
                                   "'".substr($_REQUEST['teConsultasVinHdn'], -8)."',".
                                   "'".$_REQUEST['teConsultasDistribuidorHdn']."',".
                                   "'".$_REQUEST['teConsultasSimboloHdn']."',".
                                   "'".$_REQUEST['teConsultasColorHdn']."',".
                                   //replaceEmptyNull("'".$repuve."'").")";
                                   "(SELECT folioRepuve FROM alRepuveTbl WHERE vin = '".$_REQUEST['teConsultasVinHdn']."'));";

            $rs = fn_ejecuta_query($sqlAddUnidadStr);

            if (!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
                $a['sql'] = $sqlAddUnidadStr;
                $a['successMessage'] = getUnidadesSuccessMsg();
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddUnidadStr;
                //echo $a['errorMessage'];
            }
        }
        //2.-
        if ($a['success'] == true) {

            $localizacionUnidad = $_REQUEST['teConsultasPatioHdn'];

            if ($_REQUEST['teConsultasCentroDistHdn'] == "CDTOL") {
                $localizacionUnidad = "TRAC".$_REQUEST['teConsultasPatioHdn'];
            } else if ($_REQUEST['teConsultasCentroDistHdn'] == "CDSAL" &&
                       $_REQUEST['teConsultasPatioHdn'] == "4") {
                $localizacionUnidad = "UPFIT";
            } else if ($_REQUEST['teConsultasCentroDistHdn'] == "CDSAL" &&
                       $_REQUEST['teConsultasPatioHdn'] != "4") {
                $localizacionUnidad = "SALT".$_REQUEST['teConsultasPatioHdn'];
            }

            $claveMovimiento = "EO";
            if ($_REQUEST['teConsultasCentroDistHdn'] == "CDSAL" && $_REQUEST['teConsultasPatioHdn'] == "4") {
                $claveMovimiento = "UE";
            } else if ($_REQUEST['teConsultasCentroDistHdn'] == "CDSAL" &&
                        $_REQUEST['teConsultasPatioHdn'] != "3" &&  $_REQUEST['teConsultasPatioHdn'] != "4") {
                $claveMovimiento = "CO";
            }

            $a = addHistoricoUnidad($_REQUEST['teConsultasCentroDistHdn'], $_REQUEST['teConsultasVinHdn'],
                                    $claveMovimiento, $_REQUEST['teConsultasDistribuidorHdn'],
                                    $_REQUEST['teConsultasTarifaHdn'], $localizacionUnidad,
                                    "", "", $_REQUEST['teConsultasIdUsuarioHdn']);
        }

        if ($a['success'] == true) {

            $sqlTipoDist = "SELECT tipoDistribuidor FROM caDistribuidoresCentrosTbl ".
                           "WHERE distribuidorCentro = '".$_REQUEST['teConsultasDistribuidorHdn']."'";

            $rsCentroDist = fn_ejecuta_query($sqlTipoDist);

            if ($rsCentroDist['records'] > 0) {

                if ($rsCentroDist['root'][0]['tipoDistribuidor'] == 'DX' && $rsUnidad['records'] == '0') {
                    $claveMovimiento = 'DX'; //Detenida por Exportacion

                    $a = addHistoricoUnidad($_REQUEST['teConsultasCentroDistHdn'], $_REQUEST['teConsultasVinHdn'],
                                        $claveMovimiento, $_REQUEST['teConsultasDistribuidorHdn'],
                                        $_REQUEST['teConsultasTarifaHdn'], $localizacionUnidad,
                                        "", "", $_REQUEST['teConsultasIdUsuarioHdn'], 2);

                } else if ($rsCentroDist['root'][0]['tipoDistribuidor'] == 'DX' && $rsUnidad['records'] > 0) {
                    //Efectua Conteo de DX y LX si son Iguales inserta
                    $sqlConteo = "SELECT ".
                                 "(SELECT COUNT(claveHold) FROM alHoldsUnidadesTbl ".
                                        "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' AND claveHold = 'DX') AS DXCount, ".
                                 "(SELECT COUNT(claveHold) FROM alHoldsUnidadesTbl ".
                                        "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' AND claveHold = 'LX') AS LXCount;";

                    $rsConteo = fn_ejecuta_query($sqlConteo);
                    if ($rsConteo['root'][0]['DXCount'] == $rsConteo['root'][0]['LXCount']) {

                        $a = addHistoricoUnidad($_REQUEST['teConsultasCentroDistHdn'], $_REQUEST['teConsultasVinHdn'],
                                    $claveMovimiento, $_REQUEST['teConsultasDistribuidorHdn'],
                                    $_REQUEST['teConsultasTarifaHdn'], $localizacionUnidad,
                                    "", "", $_REQUEST['teConsultasIdUsuarioHdn'], 2);
                    }
                }
            }
            //4 Paso.-

            $sqlGetHolds = "SELECT * FROM alHoldsUnidadesTbl WHERE vin = '".$_REQUEST['teConsultasVinHdn']."';";

            $rsHolds = fn_ejecuta_query($sqlGetHolds);

            $i = 0;
            foreach ($rsHolds['root'] as $hold) {
                $claveMovimiento = $hold['claveHold'];
                $a = addHistoricoUnidad($_REQUEST['teConsultasCentroDistHdn'], $_REQUEST['teConsultasVinHdn'],
                                    $claveMovimiento, $_REQUEST['teConsultasDistribuidorHdn'],
                                    $_REQUEST['teConsultasTarifaHdn'], $localizacionUnidad,
                                    "", "", $_REQUEST['teConsultasIdUsuarioHdn'], $i*2);
                $i++;

            }
            $lastDate = date("d/m/Y");
            $lastTime =  date("H:i:s");
            //Se elimina el registro hold de tratft
            $sqlDeleteHold = "DELETE FROM alHoldsUnidadesTbl ".
                             "WHERE vin = '".$_REQUEST['teConsultasVinHdn']."' ".
                             "AND claveHold = '".$claveMovimiento."';";

            //5.-Paso Se inserta en 660 si no existe registro en la 660;
            $avanzada = substr($_REQUEST['teConsultasVinHdn'], -8);

            $sqlEn660 = "SELECT * FROM al660Tbl WHERE vin = '".$_REQUEST['teConsultasVinHdn']."';";

            $rs660 = fn_ejecuta_query($sqlEn660);

            if ($rs660['records'] == '0') {
                if ($_REQUEST['teConsultasCentroDistHdn'] == "CDTOL") {
                    $orisplc = "960000305";
                } else if ($_REQUEST['teConsultasCentroDistHdn'] == "CDSAL"){
                    $orisplc = "922786710";
                }
                /*"INSERT INTO al660Tbl(vin, numavanzada, scaccode, prodstatus, orisplc, dessplc, ".
                                "routeori, routedes, von, dealerid, startdate, enddate, dealerid2, ".
                                "vupdate, vuptime,  model, ".
                                "city, state, colorcode) VALUES (".*/

                $sqlInsert660 = "INSERT INTO al660Tbl(vin, numavanzada, scaccode, prodstatus, orisplc, dessplc, ".
                                "routeori, routedes, von, dealerid, startdate, enddate, dealerid2, ".
                                "vupdate, vuptime, estimdate, expedite, merlcode, model, ".
                                "city, state, colorcode, ladingdes, authoriza, weight, height, lenght, width, ".
                                "volume, tipoOrigen, estatus) ".
                                "VALUES (".
                                "'".$_REQUEST['teConsultasVinHdn']."', '".$avanzada."', ".
                                "'XTRA', 'KZX', '$orisplc', '', '".$avanzada[1]."', 'EXP', '2222222222', '".
                                $_REQUEST['teConsultasDistribuidorHdn']."', '".$lastDate."', '', '', '".$lastDate."', '".
                                $lastTime."', '', '', '', '".$_REQUEST['teConsultasSimboloHdn']."', 'EXP', 'EXP', '---', ".
                                "'', '', '', '', '', '', '', '', '');";

                fn_ejecuta_query($sqlInsert660);
            }
        }

        if (!isset($_SESSION['error_sql']) || $_SESSION['error_sql'] == "") {
            echo '1|Unidad Actualizada|';
        } else {
            echo  '0|'.$_SESSION['error_sql'].'|';
        }

    }

    function printEntradaContencion() {

        $opc = $_REQUEST['teConsultasImpresionHdn'];

        if ($opc == '1')
            printUbicacionContencion();
        if ($opc == '2')
            printPlacardContencion();
        if ($opc == '3') {
            printUbicacionContencion();
            printPlacardContencion();
        }
    }

    function printPlacardContencion() {
        //Imprime placard
    }

    function printUbicacionContencion() {
        //Imprime Ubicacion
    }
    /*
    *       CONSULTAR UNIDAD
    *
    */


    function consultarUnidad() {

        $sqlHist =  "SELECT hu.claveMovimiento, ge.nombre AS descClaveMovimiento, ".
                    "DATE(hu.fechaEvento) AS fecha, hu.distribuidor, localizacionUnidad ".
                    "FROM alHistoricoUnidadesTbl hu, caGeneralesTbl ge ".
                    "WHERE hu.claveMovimiento = ge.valor ".
                    "AND hu.centroDistribucion = '".$_REQUEST['teConsultasCentroDistHdn']."' ".
                    "AND ge.tabla = 'alHistoricoUnidadesTbl' ".
                    "AND ge.columna = 'claveMovimiento' ".
                    "AND hu.vin = '".$_REQUEST['teConsultasVinHdn']."';";

        $rsHist = fn_ejecuta_query($sqlHist);

        if (sizeof($rsHist['root']) > 0){
            $response = createResponse($rsHist['root']);
        }

        echo $response;
    }

    function tiposSalidas(){
        $sqlSalidas = "SELECT concat(valor, ' - ', nombre) AS salida ".
                        "FROM cageneralestbl ".
                        "WHERE tabla='alhistoricounidadestbl' ".
                        //"AND valor in ('ST','SP','SV','SX','EL','SI','PB','SA','MV','CS')";
                        "AND idioma='CMDAT' ";
        $rs = fn_ejecuta_query($sqlSalidas);
       // echo json_encode($rs);

        if(sizeof($rs['root']) > 0){
            //echo json_encode($rs);
          $response = createResponseCombos($rs['root']);
        }

        echo $response;

    }

    function getMercado(){

        /*$sqlGetMercado = "SELECT au.vin, concat(au.simboloUnidad,' - ',tu.descripcion) as simUnidad, concat(hu.distribuidor ,' - ',di.descripcionCentro) as distUnidad, ".
                        "concat(tm.tarifa,' - ',tm.descripcion) as tipoMercado,hu.claveMovimiento, CONCAT(hu.claveMovimiento,' - ',ge.nombre) as cveMovimeinto  ".
                            "FROM alunidadestbl au, alultimodetalletbl hu, casimbolosunidadestbl tu, cadistribuidorescentrostbl di, catarifastbl tm, cageneralestbl ge ".
                            "WHERE au.vin = hu.vin ".
                            "AND hu.idTarifa = tm.idTarifa ".
                            "AND hu.distribuidor = di.distribuidorCentro ".
                            "AND au.simboloUnidad = tu.simboloUnidad ".
                            "AND hu.claveMovimiento = ge.valor ".
                            "AND au.vin = '".$_REQUEST['numUnidades']."' ";*/

        $sqlGetMercado = "SELECT au.vin, concat(au.simboloUnidad,' - ',tu.descripcion) as simUnidad, concat(hu.distribuidor ,' - ',di.descripcionCentro) as distUnidad, ".
                        "concat(tm.tarifa,' - ',tm.descripcion) as tipoMercado,hu.claveMovimiento, CONCAT(hu.claveMovimiento,' - ',(SELECT ge.nombre FROM cageneralestbl ge WHERE ge.tabla = 'alHistoricoUnidadesTbl' AND ge.columna ='claveMovimiento' AND ge.valor = hu.claveMovimiento)) as cveMovimeinto  ".
                            "FROM alunidadestbl au, alultimodetalletbl hu, casimbolosunidadestbl tu, cadistribuidorescentrostbl di, catarifastbl tm ".
                            "WHERE au.vin = hu.vin ".
                            "AND hu.idTarifa = tm.idTarifa ".
                            "AND hu.distribuidor = di.distribuidorCentro ".
                            "AND au.simboloUnidad = tu.simboloUnidad ".                            
                            "AND au.vin = '".$_REQUEST['numUnidades']."'; ";

        $rsSqlGetMercado = fn_ejecuta_query($sqlGetMercado);
        echo json_encode($rsSqlGetMercado);

    }

    function tMercado(){
        $getTipoMercado ="SELECT idTarifa, concat(tarifa,' - ',descripcion) as tMercado ".
                        "FROM catarifastbl ".
                        "WHERE tarifa IN('03','04','07','09','10')";

        $rsGetTipoMercado = fn_ejecuta_query($getTipoMercado);
        echo json_encode($rsGetTipoMercado);
    }

    function updTipoMercado(){

        $updHuMercado = "UPDATE alhistoricounidadestbl ".
                        "SET idTarifa = '".$_REQUEST['idMercado']."' ".
                        "WHERE vin = '".$_REQUEST['numUnidades']."' ";

        $rsupdHuMercado = fn_ejecuta_query($updHuMercado);

        $updUdMercado = "UPDATE alultimodetalletbl ".
        "SET idTarifa = '".$_REQUEST['idMercado']."' ".
        "WHERE vin = '".$_REQUEST['numUnidades']."' ";

        $rsupdUdMercado = fn_ejecuta_query($updUdMercado);
        echo json_encode($rsupdUdMercado);

    }

    function getTipoSalida(){

        $sqlGetSalida = "SELECT valor, CONCAT(VALOR,' - ',NOMBRE) as tSalida ".
                        "FROM cageneralestbl ".
                        "WHERE tabla = 'alHistoricoUnidadesTbl' ".
                        "AND valor IN('ST','PB','SI','SP','TI','EL','SX','SV','SA','MV') ".
                        "AND idioma = 'CMDAT' ";

        $rsSqlGetSalida = fn_ejecuta_query($sqlGetSalida);
        echo json_encode($rsSqlGetSalida);
    }



?>
