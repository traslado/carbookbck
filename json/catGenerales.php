<?php
    session_start();
	$_SESSION['modulo'] = "catGenerales";
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");
	
	switch($_SESSION['idioma']){
        case 'ES':
            include_once("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include_once("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include_once("../funciones/idiomas/mensajesES.php");
    }
	
    switch($_REQUEST['catGeneralesActionHdn']){
        case 'getGeneralesGroup':
            getGeneralesGroup();
            break;
        case 'getGenerales':
            echo json_encode(getGenerales());
            break;
		case 'addGenerales':
            addGenerales();
            break;
        case 'getGeneraMeses':
            getGeneraMeses();
            break;
        case 'eliminaRegistro':
            eliminaRegistro();
            break;
        default:
            echo '';
    }
			
	function getGeneralesGroup() {
    	$lsWhereStr = "";
		
		if($_SESSION['idioma'] && $_SESSION['idioma'] != "")
			$lsWhereStr = "WHERE idioma = '" . $_SESSION['idioma'] . "' ";

	    if ($gb_error_filtro == 0){
    		$lsCondicionStr = fn_construct($_REQUEST['catGeneralesTablaTxt'], "tabla", 1);
		    $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
	    }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesColumnaTxt'], "columna", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        $lsCondicionStr = fn_construct($_REQUEST['estatus'], "estatus", 1);
        $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);

		$sqlGetGeneralesGroupStr = "SELECT tabla, columna " .
            		               "FROM caGeneralesTbl " . $lsWhereStr .
            		               " GROUP BY tabla, columna";      
		
		$rs = fn_ejecuta_query($sqlGetGeneralesGroupStr);
			
		echo json_encode($rs);
	}	

	   function getGenerales() {
        $lsWhereStr = "";
        //echo $_REQUEST['catGeneralesValorTxt1'];
        
        if($_SESSION['idioma'] && $_SESSION['idioma'] != ""){
            $lsWhereStr = "WHERE idioma = '" . $_SESSION['idioma'] . "' ";
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesTablaTxt'], "tabla", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesColumnaTxt'], "columna", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesValorTxt'], "valor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesValorTxt1'], "valor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesNombreTxt'], "nombre", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesEstatusHdn'], "estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesIdiomaHdn'], "idioma", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetGeneralesStr = "SELECT tabla, columna, valor, nombre, estatus, idioma, idGenerales " .
                              "FROM caGeneralesTbl " . $lsWhereStr." ORDER BY valor";
				//echo "$sqlGetGeneralesStr<br>";
        $rs = fn_ejecuta_query($sqlGetGeneralesStr);
        //echo json_encode($sqlGetGeneralesStr);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['nombreValor'] = $rs['root'][$iInt]['valor']." - ".$rs['root'][$iInt]['nombre'];
            if(isset($_REQUEST['interfase']) && $_REQUEST['interfase'] == 1)
            {
            		$rs['root'][$iInt]['server'] = 'hptol';
            		$rs['root'][$iInt]['numero'] = 141;
						    switch($rs['root'][$iInt]['columna']){
						        case '4':
						            $rs['root'][$iInt]['patio'] = 'TOL';
						            break;
						        case '8':
						            $rs['root'][$iInt]['patio'] = 'AGS';
						            break;
										case '9':
						            $rs['root'][$iInt]['patio'] = 'MAZ';
						            break;
						        case '10':
						            $rs['root'][$iInt]['patio'] = 'SAL';
						            $rs['root'][$iInt]['numero'] = 146;
						            break;
						        case '11':
						            $rs['root'][$iInt]['patio'] = 'VER';
						            break;
						        case '12':
						            $rs['root'][$iInt]['patio'] = 'LZC';
						            break;
						        default:
						            echo '';
						    }
						    if(isset($_REQUEST['diesel']) && $_REQUEST['diesel'] == 1)
            		{
            				$rs['root'][$iInt]['numero'] = 2314;
            		}
						    if(isset($_REQUEST['sueldos']) && $_REQUEST['sueldos'] == 1 && $rs['root'][$iInt]['nombre'] == '04 - REMESA TOLUCA' && $rs['root'][$iInt]['columna'] == 4)
            		{
            				$rs['root'][$iInt]['nombre'] 	= '07 - REMESA TOLUCA';
            				$rs['root'][$iInt]['columna'] = 7;
            		}
            }
        }
            
        return $rs; 
    }
	
	function addGenerales() {
        $a = array();
        $e = array();
        $a['success'] = true;
		
		if($_REQUEST['catGeneralesTablaTxt'] == "")
        {
            $e[] = array('id'=>'catGeneralesTablaTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catGeneralesColumnaTxt'] == "")
        {
            $e[] = array('id'=>'catGeneralesColumnaTxt','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catGeneralesEstatusHdn'] == "")
        {
            $e[] = array('id'=>'catGeneralesEstatusHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catGeneralesValorHdn'] == "")
        {
            $e[] = array('id'=>'catGeneralesValorHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catGeneralesNombreHdn'] == "")
        {
            $e[] = array('id'=>'catGeneralesNombreHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		if($_REQUEST['catGeneralesIdiomaHdn'] == "")
        {
            $e[] = array('id'=>'catGeneralesIdiomaHdn','msg'=>getRequerido());
		    $a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

		//Revisar que ningun dato del grid esté vacio
        $valorArr = explode('|', substr($_REQUEST['catGeneralesValorHdn'], 0, -1));
        if(in_array('', $valorArr)){
        	$e[] = array('id'=>'catGeneralesValorHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

		$estatusArr =  explode('|', substr($_REQUEST['catGeneralesEstatusHdn'], 0, -1));
		if(in_array('', $estatusArr)){
        	$e[] = array('id'=>'catGeneralesEstatusHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

		$nombreArr = explode('|', substr($_REQUEST['catGeneralesNombreHdn'], 0, -1));
		if(in_array('', $nombreArr)){
        	$e[] = array('id'=>'catGeneralesNombreHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }
		$idiomaArr = explode('|', substr($_REQUEST['catGeneralesIdiomaHdn'], 0, -1));
		if(in_array('', $idiomaArr)){
        	$e[] = array('id'=>'catGeneralesIdiomaHdn','msg'=>getRequerido());
			$a['errorMessage'] = getErrorRequeridos();
            $a['success'] = false;
        }

        if ($a['success'] == true){

            $sqlCleanGeneralesTblColStr = "DELETE FROM caGeneralesTbl ".
                                          "WHERE tabla = '".$_REQUEST['catGeneralesTablaTxt']."' ".
                                          "AND columna = '".$_REQUEST['catGeneralesColumnaTxt']."' ";
            $rs = fn_ejecuta_query($sqlCleanGeneralesTblColStr);

            if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                $a['errorMessage'] = "";
                for($nInt=0;$nInt<count($valorArr);$nInt++){
                    $sqlAddGeneralesStr = "INSERT INTO caGeneralesTbl (tabla, columna, valor, nombre, estatus, idioma) VALUES".
                                            "('".$_REQUEST['catGeneralesTablaTxt']."', ".
                                            "'".$_REQUEST['catGeneralesColumnaTxt']."', ".
                                            "'".$valorArr[$nInt]."', ".
                                            "'".strtoupper($nombreArr[$nInt])."', ".
                                            "'".$estatusArr[$nInt]."', ".
                                            "'".strtoupper($idiomaArr[$nInt])."')";

                    $rs = fn_ejecuta_Add($sqlAddGeneralesStr);

                if($_REQUEST['catGeneralesTablaTxt'] == 'conceptos' && $_REQUEST['catGeneralesColumnaTxt'] =='9000')
                    {

                        $delConcepto = "DELETE FROM caconceptostbl ".
                                       "WHERE concepto = '".$valorArr[$nInt]."' ";
                        $rs = fn_ejecuta_query($delConcepto);     

                        $sqlAddConceptos = "INSERT INTO caconceptostbl (concepto,nombre,tipoConcepto,estatus,tipoGasto) VALUES".
                                            "('".$valorArr[$nInt]."', ".
                                            "'".strtoupper($nombreArr[$nInt])."', ".
                                            "'G', ".
                                            "'".$estatusArr[$nInt]."', ".
                                            "'') ";

                        $rs = fn_ejecuta_Add($sqlAddConceptos);


                        $SelConcepto = "SELECT * FROM caconceptosCentrosTbl ".
                                       "WHERE concepto = '".$valorArr[$nInt]."' ";
                        $rsSelConcepto = fn_ejecuta_query($SelConcepto);

                        if(sizeof($rsSelConcepto['root']) == 0)
                        {
                        
                    $sqlAddGeneralesStr = "INSERT INTO caconceptosCentrosTbl (centroDistribucion,concepto,tipoCuenta,conceptoNomina,calculo,importe) VALUES".
                                            "('CDTOL', ".
                                            "'".$valorArr[$nInt]."', ".
                                            "'', ".
                                            "'', ".
                                            "'0', ".
                                            "'0' )";

                        $rs = fn_ejecuta_Add($sqlAddGeneralesStr);

                    $sqlAddGeneralesStr = "INSERT INTO caconceptosCentrosTbl (centroDistribucion,concepto,tipoCuenta,conceptoNomina,calculo,importe) VALUES".
                                            "('CDAGS', ".
                                            "'".$valorArr[$nInt]."', ".
                                            "'', ".
                                            "'', ".
                                            "'0', ".
                                            "'0' )";

                        $rs = fn_ejecuta_Add($sqlAddGeneralesStr);

                    $sqlAddGeneralesStr = "INSERT INTO caconceptosCentrosTbl (centroDistribucion,concepto,tipoCuenta,conceptoNomina,calculo,importe) VALUES".
                                            "('CDLZC', ".
                                            "'".$valorArr[$nInt]."', ".
                                            "'', ".
                                            "'', ".
                                            "'0', ".
                                            "'0' )";

                        $rs = fn_ejecuta_Add($sqlAddGeneralesStr);

                    $sqlAddGeneralesStr = "INSERT INTO caconceptosCentrosTbl (centroDistribucion,concepto,tipoCuenta,conceptoNomina,calculo,importe) VALUES".
                                            "('CDSAL', ".
                                            "'".$valorArr[$nInt]."', ".
                                            "'', ".
                                            "'', ".
                                            "'0', ".
                                            "'0' )";

                        $rs = fn_ejecuta_Add($sqlAddGeneralesStr);

                    $sqlAddGeneralesStr = "INSERT INTO caconceptosCentrosTbl (centroDistribucion,concepto,tipoCuenta,conceptoNomina,calculo,importe) VALUES".
                                            "('CDVER', ".
                                            "'".$valorArr[$nInt]."', ".
                                            "'', ".
                                            "'', ".
                                            "'0', ".
                                            "'0' )";

                        $rs = fn_ejecuta_Add($sqlAddGeneralesStr);
                    }

                 }

                    if((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")){
                        $a['sql'] = $sqlAddGeneralesStr;
                        $a['successMessage'] = getGeneralesSuccessMsg();
                        $a['id'] = $_REQUEST['catGeneralesTablaTxt'];
                    } else {
                        $a['success'] = false;
                        if ($a['errorMessage'] != "") {
                            $a['errorMessage'] .= ",";
                        }
                        $a['errorMessage'] .= $nInt;
                    }
                }
                $a['errorMessage'] = "Error al insertar registro(s): ".$a['errorMessage'];   
            } else {
                $a['success'] = false;
                $a['errorMessage'] = $_SESSION['error_sql'] . "<br>" . $sqlAddGeneralesStr;
            }
		}
        $a['errors'] = $e;
		$a['successTitle'] = getMsgTitulo();
        echo json_encode($a);
	}

    function eliminaRegistro(){
        // echo "si esta entrando";
        $sqlAddGeneralesStr = "DELETE FROM  caGeneralesTbl ".
                "WHERE tabla ='".$_REQUEST['catGeneralesTablaTxt']."' ".
                "AND columna ='".$_REQUEST['catGeneralesColumnaTxt']."' ".
                "AND valor='".$_REQUEST['catGeneralesValorHdn']."' ".
                "AND nombre='".$_REQUEST['catGeneralesNombreHdn']."' ".
                "AND estatus ='".$_REQUEST['catGeneralesEstatusHdn']."' ".
                "AND idioma = '".$_REQUEST['catGeneralesIdiomaHdn']."';";

            $rs = fn_ejecuta_Add($sqlAddGeneralesStr); 
            echo json_encode($rs);    
    }

    function getGeneraMeses() {
        $lsWhereStr = "";
        //echo $_REQUEST['catGeneralesValorTxt1'];
        
        if($_SESSION['idioma'] && $_SESSION['idioma'] != ""){
            $lsWhereStr = "WHERE idioma = '" . $_SESSION['idioma'] . "' ";
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesTablaTxt'], "tabla", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesColumnaTxt'], "columna", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesValorTxt'], "valor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesValorTxt1'], "valor", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesNombreTxt'], "nombre", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesEstatusHdn'], "estatus", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }
        if ($gb_error_filtro == 0){
            $lsCondicionStr = fn_construct($_REQUEST['catGeneralesIdiomaHdn'], "idioma", 1);
            $lsWhereStr = fn_concatena_condicion($lsWhereStr, $lsCondicionStr);
        }

        $sqlGetGeneralesStr = "SELECT tabla, columna, valor, nombre, estatus, idioma " .
                              "FROM caGeneralesTbl WHERE tabla ='polizaGastos' ".
                              "AND columna = 'meses' ".
                              "AND valor IN('".$_REQUEST['catGeneralesValorTxt']."','".$_REQUEST['catGeneralesValorTxt1']."')".
                              "ORDER BY valor DESC "; 

        $rs = fn_ejecuta_query($sqlGetGeneralesStr);
        echo json_encode($rs);

        for ($iInt=0; $iInt < sizeof($rs['root']); $iInt++) { 
            $rs['root'][$iInt]['nombreValor'] = $rs['root'][$iInt]['valor']." - ".$rs['root'][$iInt]['nombre'];
        }
            
        return $rs; 
    }    
?>