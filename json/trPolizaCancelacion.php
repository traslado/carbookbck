<?php
    /**
    * Modif: ~/02/2020
    */
    session_start();
    require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    $_REQUEST = trasformUppercase($_REQUEST);

    switch($_SESSION['idioma']){
        case 'ES':
            include("../funciones/idiomas/mensajesES.php");
            break;
        case 'EN':
            include("../funciones/idiomas/mensajesEN.php");
            break;
        default:
            include("../funciones/idiomas/mensajesES.php");
    }

    switch($_REQUEST['trPolizaCancelActionHdn']) {
        case 'tipoCancelacion':
            tipoCancelacion();
            break;
        case 'getDatosPoliza':
            getDatosPoliza();
            break;
        case 'cancPoliza':
            cancPoliza();
            break;
        default:
            # code...
            break;
    }

    function tipoCancelacion(){
        // 1 Total, 2 parcial
        $sqlTcancelacion = "SELECT COUNT(DISTINCT(mesAfectacion)) as tipoPoliza ".
                            "FROM trgastosviajetractortbl ".
                            "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                            "AND claveMovimiento = '".$_REQUEST['claveMovimiento']."';";
        $rsTcancelacion = fn_ejecuta_query($sqlTcancelacion);

        $selStr = "SELECT * FROM trviajestractorestbl ".
                  "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                  "AND claveMovimiento = 'VP' ".
                  "ORDER BY fechaEvento DESC LIMIT 1";
        $ultPolRst = fn_ejecuta_query($selStr);
        $rsTcancelacion['ultimoPoliza'] = 0;
        if ($ultPolRst['records'] > 0) {
            if ($ultPolRst['root'][0]['idViajeTractor'] == $_REQUEST['canPolizaIdViajeHdn']) {
                $rsTcancelacion['ultimoPoliza'] = 1;
            }
        }

        echo json_encode($rsTcancelacion);
    }

    function getDatosPoliza(){
        $claveMovimiento = '';
        $selStr = "SELECT IFNULL(SUM(subtotal),0) AS subtotal, gvt.claveMovimiento ".
                  "FROM trgastosviajetractortbl gvt, caGeneralesTbl gen ".
                  "WHERE gvt.concepto = gen.valor ".
                  "AND gen.tabla = 'comprobacionPoliza' AND gen.columna = '".$_SESSION['usuCompania']."' ".
                  "AND gvt.folio = '".$_REQUEST['canPolizaNumeroTxt']."' ".
                  "AND SUBSTRING(gvt.fechaEvento,1,10) = '".$_REQUEST['canPolizaFechaDt']."' ".
                  "AND gvt.centroDistribucion = '".$_SESSION['usuCompania']."'";
        $selRst = fn_ejecuta_query($selStr);
        $claveMovimiento = $selRst['root'][0]['claveMovimiento'];
        if ($claveMovimiento == 'XC') {
            $claveMovimiento = 'GC';
            $rsSqlViajePoliza['records'] = 0;
            $rsSqlViajePoliza['msjResponse'] = 'Complemento de poliza cancelada.';
            echo json_encode($rsSqlViajePoliza);
            die();
        } elseif ($claveMovimiento == 'XP') {
            $claveMovimiento = 'GP';
            $rsSqlViajePoliza['records'] = 0;
            $rsSqlViajePoliza['msjResponse'] = 'Poliza de gastos cancelada.';
            echo json_encode($rsSqlViajePoliza);
            die();
        }

        $sqlViajePoliza = "SELECT DISTINCT concat(co.compania,' - ', co.descripcion) as compania, tr.tractor, vt.viaje, ch.claveChofer, concat(ch.claveChofer,' - ',ch.nombre,' ',ch.apellidoPaterno,' ',ch.apellidoMaterno) as operador, tr.rendimiento, ".
                        "(SELECT pl.Plaza FROM caplazastbl pl WHERE pl.idPlaza = vt.idPlazaOrigen) as origen, (SELECT pl1.Plaza FROM caplazastbl pl1 WHERE pl1.idPlaza = vt.idPlazaDestino) as destino, vt.kilometrosTabulados, ".
                        "vt.numeroRepartos as distribuidores, ".
                        "(SELECT count(tl1.distribuidor) FROM trtalonesviajestbl tl1 WHERE tl1.idViajeTractor = vt.idViajeTractor ) as talones, ".
                        "vt.numeroUnidades, vt.fechaEvento, vt.idViajeTractor, ".
                        "(SELECT SUM(subtotal) FROM trgastosviajetractortbl gvt, caGeneralesTbl gen WHERE gvt.concepto = gen.valor and gen.tabla = 'comprobacionPoliza' and gen.columna = '".$_SESSION['usuCompania']."' AND gvt.idViajeTractor = gt.idViajeTractor AND gvt.folio = gt.folio AND gvt.claveMovimiento='".$claveMovimiento."') AS totCompGtos, ".
                        "(SELECT subtotal FROM  trgastosviajetractortbl WHERE concepto='7012' AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento='".$claveMovimiento."') AS totIVAAcreditable, ".
                        "(SELECT subtotal FROM trgastosviajetractortbl WHERE concepto='7025' AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento='".$claveMovimiento."') AS totOperadores, ".
                        "(SELECT subtotal FROM trgastosviajetractortbl  WHERE concepto='7013' AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento='".$claveMovimiento."') AS totCargoOpe, ".
                        "0 AS totComprobado, 0 AS totCargos,".
                        "(SELECT SUM(importe) FROM trGastosViajeTractorTbl WHERE concepto IN ('7009','7016','7015','7023','7024') AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento != 'XP') AS totAnticipo,".
                        "(SELECT subtotal FROM  trgastosviajetractortbl WHERE concepto='7014' AND idViajeTractor = gt.idViajeTractor AND folio=gt.folio AND claveMovimiento='GP') AS totPagoEfectivo, ".//".$claveMovimiento."
                        "0 AS totAbonos,".
                        "(SELECT DISTINCT claveMovimiento FROM trPolizaInterfaceTbl pi WHERE pi.centroDistribucion = '".$_SESSION['usuCompania']."' AND pi.folio = gt.folio AND pi.idViajeTractor = gt.idViajeTractor) AS claveMovimiento, ".
                        "(SELECT COUNT(*) FROM trPolizaInterfaceTbl pi WHERE pi.centroDistribucion = '".$_SESSION['usuCompania']."' AND pi.folio = gt.folio AND pi.idViajeTractor = gt.idViajeTractor) AS existeInterface ".
                        "FROM trviajestractorestbl vt, catractorestbl tr, cachoferestbl ch, trgastosviajetractortbl gt, cacompaniastbl co ".
                        "WHERE vt.idViajeTractor = gt.idViajeTractor ".
                        "AND vt.idTractor = tr.idTractor ".
                        "AND vt.claveChofer = ch.claveChofer ".
                        "AND vt.claveMovimiento = 'VP' ".
                        "AND tr.compania = co.compania ".
                        // "AND gt.folio NOT IN (SELECT pi.folio FROM trPolizaInterfaceTbl pi WHERE pi.folio = gt.folio AND pi.idViajeTractor = gt.idViajeTractor) ".
                        "AND gt.folio = '".$_REQUEST['canPolizaNumeroTxt']."' ".
                        "AND gt.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                        // "AND vt.centroDistribucion = gt.centroDistribucion ".
                        "AND SUBSTRING(gt.fechaEvento,1,10) = '".$_REQUEST['canPolizaFechaDt']."';";
        $rsSqlViajePoliza = fn_ejecuta_query($sqlViajePoliza);
        $rsSqlViajePoliza['msjResponse'] = '';
        if ($rsSqlViajePoliza['records'] > 0) {
            if ($rsSqlViajePoliza['root'][0]['existeInterface'] != 0) {
                if ($rsSqlViajePoliza['root'][0]['claveMovimiento'] == 'T') {
                    $rsSqlViajePoliza['msjResponse'] = 'La póliza ya fue transmitida a contabilidad, no se puede cancelar.';
                }
                if ($rsSqlViajePoliza['root'][0]['claveMovimiento'] == 'C') {
                    $rsSqlViajePoliza['msjResponse'] = 'La póliza ya está pagada.';
                }
            } else {
                $rsSqlViajePoliza['root'][0]['totComprobado'] = floatval($rsSqlViajePoliza['root'][0]['totCompGtos']) + floatval($rsSqlViajePoliza['root'][0]['totIVAAcreditable']);
                $rsSqlViajePoliza['root'][0]['totCargos'] = number_format(floatval($rsSqlViajePoliza['root'][0]['totComprobado']) + floatval($rsSqlViajePoliza['root'][0]['totCargoOpe']),2,'.',',');
                $rsSqlViajePoliza['root'][0]['totAbonos'] = number_format(floatval($rsSqlViajePoliza['root'][0]['totAnticipo']) + floatval($rsSqlViajePoliza['root'][0]['totPagoEfectivo']),2,'.',',');

                $rsSqlViajePoliza['root'][0]['totCompGtos'] = number_format(floatval($rsSqlViajePoliza['root'][0]['totCompGtos']),2,'.',',');
                $rsSqlViajePoliza['root'][0]['totIVAAcreditable'] = number_format(floatval($rsSqlViajePoliza['root'][0]['totIVAAcreditable']),2,'.',',');
                $rsSqlViajePoliza['root'][0]['totOperadores'] = number_format(floatval($rsSqlViajePoliza['root'][0]['totOperadores']),2,'.',',');
                $rsSqlViajePoliza['root'][0]['totCargoOpe'] = number_format(floatval($rsSqlViajePoliza['root'][0]['totCargoOpe']),2,'.',',');
                $rsSqlViajePoliza['root'][0]['totAnticipo'] = number_format(floatval($rsSqlViajePoliza['root'][0]['totAnticipo']),2,'.',',');
                $rsSqlViajePoliza['root'][0]['totPagoEfectivo'] = number_format(floatval($rsSqlViajePoliza['root'][0]['totPagoEfectivo']),2,'.',',');
                $rsSqlViajePoliza['root'][0]['totComprobado'] = number_format($rsSqlViajePoliza['root'][0]['totComprobado'],2,'.',',');
                $rsSqlViajePoliza['root'][0]['claveMovimiento'] = $claveMovimiento;

                $rsSqlViajePoliza['root'][0]['msgFolios'] = "";
                if ($claveMovimiento != 'GC') {
                    $selStr = "SELECT DISTINCT a.folio, CAST(a.fechaEvento AS DATE) AS fechaEvento ".
                              "FROM trgastosviajetractortbl a, trviajestractorestbl b ".
                              "WHERE a.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND b.claveMovimiento = 'VP' ".
                              "AND a.idViajeTractor = b.idViajeTractor ".
                              "AND b.idViajeTractor = ".$rsSqlViajePoliza['root'][0]['idViajeTractor']." ".
                              "AND a.folio != 0 ".
                              "AND a.claveMovimiento = 'GC' ".
                              "AND a.claveMovimiento not in ('XP','XC') ".
                              "ORDER BY a.centroDistribucion, a.fechaEvento DESC";
                    $compRst = fn_ejecuta_query($selStr);
                    for ($i=0; $i < $compRst['records']; $i++) { 
                        if ($i == 0) {
                            $rsSqlViajePoliza['root'][0]['msgFolios'] .= "    FOLIO         FECHA\n";
                        }
                        $rsSqlViajePoliza['root'][0]['msgFolios'] .= "    ".$compRst['root'][$i]['folio']."      ".date('d/m/Y', strtotime($compRst['root'][$i]['fechaEvento']))."\n";
                    }
                }
            }
        } else {
            $rsSqlViajePoliza['msjResponse'] = 'La póliza no se encuentra.';
        }

        echo json_encode($rsSqlViajePoliza);
    }

    function cancPoliza(){
        $a = array('success' => true);
        // 1 = POLIZA DE GASTOS
        // 2 = COMPLEMENTO DE POLIZA
        if (isset($_REQUEST['claveMovimiento']) && $_REQUEST['claveMovimiento'] == 'GP') {
            $folioStr = ""; $folioComp = "";
            if ($_REQUEST['actionHdn'] == 2) {
                $folioStr = "AND folio = '".$_REQUEST['canPolizaNumeroTxt']."' ";
                $folioComp = "AND a.folio = '".$_REQUEST['canPolizaNumeroTxt']."' ";
            }

            // Cancela Complemento de Poliza
            $selStr = "SELECT DISTINCT a.folio, CAST(a.fechaEvento AS DATE) AS fechaEvento ".
                      "FROM trgastosviajetractortbl a, trviajestractorestbl b ".
                      "WHERE a.centroDistribucion = '".$_SESSION['usuCompania']."' ".
                      "AND b.claveMovimiento = 'VP' ".
                      "AND a.idViajeTractor = b.idViajeTractor ".
                      "AND b.idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".//89596 -- 89596 89715
                      "AND a.folio != 0 ".
                      "AND a.claveMovimiento = 'GC' ".
                      "AND a.claveMovimiento not in ('XP','XC') ".
                      "ORDER BY a.centroDistribucion, a.fechaEvento DESC";
            $compRst = fn_ejecuta_sql($selStr);
            for ($i=0; $i < $compRst['records']; $i++) { 
                $resp = canComplementoPoliza($_REQUEST['canPolizaIdViajeHdn'], $compRst['root'][$i]['folio']);
                if ($resp['success'] == false) {
                    $a = $resp;
                    break;
                }
            }
            // Cancela Poliza de Gastos
            if($a['success'] && ($_REQUEST['canPolizaCentroHdn'] == 'CDLZC' || $_REQUEST['canPolizaCentroHdn'] == 'CDVER')){
                $sqlNumMto = "SELECT max(m2.idMantenimientoDetalle) as idMantenimiento ".
                            "FROM camantenimientotractoresdetalletbl m2 ".
                            "WHERE m2.idViajeTractor = '".$_REQUEST['canPolizaIdViajeHdn']."';";
                $rsNumMto = fn_ejecuta_sql($sqlNumMto);

                $sqlDltMto = "DELETE FROM camantenimientotractoresdetalletbl ".
                            "WHERE idViajeTractor = '".$_REQUEST['canPolizaIdViajeHdn']."' ".
                            "AND idMantenimientoDetalle = '".$rsNumMto['root'][0]['idMantenimiento']."'; ";
                fn_ejecuta_sql($sqlDltMto);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql']         = $sqlDltMto;
                }
            }
            if ($a['success']) {
                //----- CANCELACION CONCEPTOS GS Y GP
                $sqlUpdXP = "UPDATE trgastosviajetractortbl ".
                            "SET claveMovimiento = 'XP' ".
                            "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                            $folioStr.
                            "AND claveMovimiento IN ('GP');";
                fn_ejecuta_sql($sqlUpdXP);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql']         = $sqlUpdXP;
                } else {
                    $sqlUpdXS = "UPDATE trgastosviajetractortbl ".
                                "SET claveMovimiento = 'XS' ".
                                "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                $folioStr.
                                "AND claveMovimiento IN ('GS');";
                    fn_ejecuta_sql($sqlUpdXS);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $sqlUpdXS;
                    } else {
                        //----- CANCELACION CONCEPTOS COMBUSTIBLE 9000
                        $sqlUpdXD = "UPDATE trgastosviajetractortbl ".
                                    "SET claveMovimiento = 'XD' ".
                                    "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                    $folioStr.
                                    "AND concepto like '9%';";
                        fn_ejecuta_sql($sqlUpdXD);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql'];
                            $a['sql']         = $sqlUpdXD;
                        } else {
                            //----- CANCELACION VIAJE A VU
                            $sqlUpdViaje = "UPDATE trviajestractorestbl ".
                                            "SET claveMovimiento = 'VU' ".
                                            "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn'];
                            fn_ejecuta_sql($sqlUpdViaje);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['msjResponse'] = $_SESSION['error_sql'];
                                $a['sql']         = $sqlUpdViaje;
                            } else {
                                //----- SE BORRA LAS FACTURAS DE LAS POLIZAS
                                $sqlDltFac = "DELETE FROM trpolizafacturastbl ".
                                            "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                            "AND folio = '".$_REQUEST['canPolizaNumeroTxt']."'";
                                fn_ejecuta_sql($sqlDltFac);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success']     = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'];
                                    $a['sql']         = $sqlDltFac;
                                } else {
                                    $delStr = "DELETE FROM trComplementosConceptoTbl ".
                                              "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                              "AND concepto = '6002' ".
                                              "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                              "AND folio = '".$_REQUEST['canPolizaNumeroTxt']."'";
                                    fn_ejecuta_sql($delStr);
                                    if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                                        $a['success'] = false;
                                        $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $delStr;
                                    } else {
                                        //----- SE BORRAN LAS OBSERVACIONES DE LA POLIZA
                                        $sqlDltObs = "DELETE FROM trobservacionesviajetbl ".
                                                    "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                                    "AND tipo = 'G';";
                                        fn_ejecuta_sql($sqlDltObs);
                                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                            $a['success']     = false;
                                            $a['msjResponse'] = $_SESSION['error_sql'];
                                            $a['sql']         = $sqlDltObs;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if ($a['success']) {
                // COMBUSTIBLE PENDIENTE
                $fechaEvento = date("Y-m-d H:i:s");
                $sqlAddObservGastos = "INSERT INTO trObservacionesViajeTbl (idViajeTractor, folio, fechaEvento, tipo, observaciones) VALUES (".
                                        $_REQUEST['canPolizaIdViajeHdn'].",".
                                        "'".$_REQUEST['canPolizaNumeroTxt']."',".
                                        "'".$fechaEvento."',".
                                        "'C',".
                                        "'".$_REQUEST['canPolizaObservacionesTxa']."')";
                fn_ejecuta_sql($sqlAddObservGastos);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success']     = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql']         = $sqlAddObservGastos;
                } else {
                    $sqlGtosTr = "SELECT * FROM trGastosViajeTractorTbl ".
                                "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                $folioStr.
                                "AND concepto IN ('9018','9019') ".
                                "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                                "AND claveMovimiento = 'XD'";
                    $gastosVRs = fn_ejecuta_sql($sqlGtosTr);
                    if ($gastosVRs['records'] > 0) {
                        $litros = $gastosVRs['root'][0]['observaciones'];
                        if (!empty($litros) && $litros != 0) {
                            $sqlUpdPdt = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CP' ".
                                         "WHERE idCombustible = ".$litros;
                            fn_ejecuta_sql($sqlUpdPdt);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['msjResponse'] = $_SESSION['error_sql'];
                                $a['sql']         = $sqlUpdPdt;
                            } else {
                                $sqlUpdPdtN = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CL' ".
                                              "WHERE idCombustible != ".$litros;
                                fn_ejecuta_sql($sqlUpdPdtN);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success']     = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'];
                                    $a['sql']         = $sqlUpdPdtN;
                                }
                            }
                        }
                    } else {
                        $sqlUpdPdtN = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CL' ".
                                      "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                      "AND claveChofer = ".$_REQUEST['claveChofer'];
                        fn_ejecuta_sql($sqlUpdPdtN);
                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                            $a['success']     = false;
                            $a['msjResponse'] = $_SESSION['error_sql'];
                            $a['sql']         = $sqlUpdPdtN;
                        }
                    }
                }

                // TICKET CARD
                if ($a['success']) {
                    $updStr = "UPDATE trpolizastckettbl ".
                              "SET estatus = 1, folio = NULL, idViajeTractor = NULL ".
                              "WHERE folio = '".$_REQUEST['canPolizaNumeroTxt']."' ".
                              "AND idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn'];
                    fn_ejecuta_sql($updStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $updStr;
                    } else {
                        // GASTO NO COMPROBADO
                        $selGtosStr = "SELECT * FROM trGastosViajeTractorTbl ".
                                    "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                    "AND folio = ".$_REQUEST['canPolizaNumeroTxt']." ".
                                    "AND concepto = '144' ".
                                    "AND centroDistribucion = '".$_SESSION['usuCompania']."'";
                        $gastosRst = fn_ejecuta_sql($selGtosStr);
                        if ($gastosRst['records'] > 0) {
                            $selStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                                      "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                                      "AND concepto = '2231' ".
                                      "AND estatus = 'P'";
                            $descRst = fn_ejecuta_sql($selStr);
                            $gastosRst['root'][0]['importe'] = str_replace('-', '', $gastosRst['root'][0]['importe']);
                            $saldo = floatval($descRst['root'][0]['saldo']) - floatval($gastosRst['root'][0]['importe']);
                            if ($saldo > 0) {
                                if ($descRst['root'][0]['tipoConcepto'] == 'I') {
                                    $ls_importe = "tipoImporte = ".$saldo.", importe = ".$saldo.", saldo = " . $saldo;
                                } else {
                                    $ls_importe = "saldo = " . $saldo;
                                }
                                $setStr = "SET ${ls_importe} ";
                            } else {
                                $setStr = "SET estatus = 'L' ";
                            }
                            $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                                      $setStr.
                                      "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                                      "AND concepto = '2231' ".
                                      "AND estatus = 'P'";
                            fn_ejecuta_sql($updStr);
                            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                $a['success']     = false;
                                $a['msjResponse'] = $_SESSION['error_sql'];
                                $a['sql']         = $updStr;
                            }
                        }
                    }
                    if ($a['success']) {
                        // DESCUENTOS PERSONALES
                        $selStr = "SELECT * FROM rohistoricodescuentoschoferestbl ".
                                  "WHERE viaje = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                  "AND avanzada = ".$_REQUEST['canPolizaNumeroTxt']." ".
                                  "AND claveChofer = ".$_REQUEST['claveChofer']." ".
                                  "AND centroDistribucion = '".$_SESSION['usuCompania']."'";
                        $historicoRst = fn_ejecuta_sql($selStr);
                        for ($i=0; $i < $historicoRst['records']; $i++) { 
                            $selStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                                      "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                                      "AND concepto = '".$historicoRst['root'][$i]['concepto']."' ".
                                      "ORDER BY idDescuento DESC LIMIT 1";
                                      // "AND estatus = 'P'";
                            $concDescRst = fn_ejecuta_sql($selStr);
                            if ($concDescRst['records'] > 0) {
                                $tipoConcepto = $concDescRst['root'][0]['tipoConcepto'];
                                $estatus = $concDescRst['root'][0]['estatus'];
                                $saldo = floatval($historicoRst['root'][$i]['importe']);
                                if ($estatus == 'P') {
                                    if ($tipoConcepto == 'P' || $tipoConcepto == 'I') {
                                        $selDescStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                                                  "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                                                  "AND concepto = '".$historicoRst['root'][$i]['concepto']."' ".
                                                  "AND tipoConcepto IN ('P','I') ".
                                                  "AND estatus = 'P'";
                                        $rodescRst = fn_ejecuta_sql($selDescStr);
                                        $saldoAct = floatval($rodescRst['root'][0]['saldo']) + $saldo;
                                        if ($rodescRst['root'][0]['tipoConcepto'] == 'I') {
                                            $ls_importe = "tipoImporte = ${saldoAct}, importe = ${saldoAct}, saldo = ${saldoAct}";
                                        } else {
                                            $ls_importe = "saldo = ${saldoAct}";
                                        }
                                        $updStr = "UPDATE roconceptosdescuentoschofertbl ".
                                                  "SET ${ls_importe} ".
                                                  "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                                                  "AND concepto = '".$historicoRst['root'][$i]['concepto']."' ".
                                                  "AND tipoConcepto IN ('P','I') ".
                                                  "AND estatus = 'P'";
                                        fn_ejecuta_sql($updStr);
                                        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                            $a['success']     = false;
                                            $a['msjResponse'] = $_SESSION['error_sql'];
                                            $a['sql']         = $updStr;
                                            break;
                                        }
                                    }
                                } else {
                                    $tipoImporte = "tipoImporte";
                                    $importe = "importe";
                                    if ($tipoConcepto == 'I') {
                                        $tipoImporte = $saldo;
                                        $importe = $saldo;
                                    }
                                    $insStr = "INSERT INTO roconceptosdescuentoschofertbl (claveChofer, concepto, centroDistribucion, secuencia, fechaEvento, pagadoCobro, tipoConcepto, tipoImporte, importe, saldo, estatus) ".
                                            "SELECT claveChofer, concepto, centroDistribucion, secuencia +1 AS secuencia, CAST(NOW() AS DATE) AS fechaEvento, pagadoCobro, tipoConcepto, ${tipoImporte} AS tipoImporte, ${importe} AS importe, ${saldo} AS saldo, 'P' AS estatus ".
                                            "FROM roconceptosdescuentoschofertbl ".
                                            "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                                            "AND concepto = '".$historicoRst['root'][$i]['concepto']."' ".
                                            "ORDER BY secuencia DESC LIMIT 1";
                                    fn_ejecuta_sql($insStr);
                                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                        $a['success']     = false;
                                        $a['msjResponse'] = $_SESSION['error_sql'];
                                        $a['sql']         = $insStr;
                                        break;
                                    }
                                }
                            }
                        }
                        if ($a['success']) {
                            // FONACOT PRESTAMOS PERSONALES
                            $selStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                                      "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                                      "AND concepto = '2228' ".
                                      "AND tipoConcepto = 'A'";
                            $prestRst = fn_ejecuta_sql($selStr);
                            if ($prestRst['records'] > 0) {
                                $updStr = "UPDATE roPagosOperadorHistoricoTbl ".
                                          "SET claveMovimiento = 'PC' ".
                                          "WHERE folio = '".$_REQUEST['canPolizaNumeroTxt']."' ".
                                          "AND idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                          "AND concepto = '2228' ".
                                          "AND claveMovimiento = 'PA'";
                                fn_ejecuta_sql($updStr);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success']     = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'];
                                    $a['sql']         = $updStr;
                                }
                            }
                        }
                        if ($a['success']) {
                            // INFONAVIT
                            $selStr = "SELECT * FROM roconceptosdescuentoschofertbl ".
                                      "WHERE claveChofer = ".$_REQUEST['claveChofer']." ".
                                      "AND concepto = '2227' ".
                                      "AND tipoConcepto = 'A'";
                            $prestRst = fn_ejecuta_sql($selStr);
                            if ($prestRst['records'] > 0) {
                                $updStr = "UPDATE roPagosOperadorHistoricoTbl ".
                                          "SET claveMovimiento = 'PC' ".
                                          "WHERE folio = '".$_REQUEST['canPolizaNumeroTxt']."' ".
                                          "AND idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                                          "AND concepto = '2227' ".
                                          "AND claveMovimiento = 'PA'";
                                fn_ejecuta_sql($updStr);
                                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                                    $a['success']     = false;
                                    $a['msjResponse'] = $_SESSION['error_sql'];
                                    $a['sql']         = $updStr;
                                }
                            }
                        }
                    }
                }

                if ($a['success']) {
                    $updStr = "UPDATE tralimentospendientetbl ".
                              "SET claveMovimiento = 'PX' ".
                              "WHERE idViajeTractor = ".$_REQUEST['canPolizaIdViajeHdn']." ".
                              "AND concepto = '2342' ".
                              "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND folio = '".$_REQUEST['canPolizaNumeroTxt']."' ".
                              "AND claveMovimiento = 'PA'";
                    fn_ejecuta_sql($updStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $updStr;
                    }
                }
            }
        } else {
            // Cancela Complemento de Poliza
            $a = canComplementoPoliza($_REQUEST['canPolizaIdViajeHdn'], $_REQUEST['canPolizaNumeroTxt']);
        }
        // fn_ejecuta_sql(false);echo ">";
        fn_ejecuta_sql($a['success']);
        if ($a['success']) {
           ///$exeproc = new programaexterno();
            //$exeproc->runprogram("proDescuentosPersonales.php");
        }

        echo json_encode($a);
    }

    function canComplementoPoliza($idViajeTractorPrm, $folioPrm) {
        $a = array('success' => true);

        $sqlUpdXC = "UPDATE trgastosviajetractortbl ".
                    "SET claveMovimiento = 'XC' ".
                    "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                    "AND folio = '".$folioPrm."' ".
                    "AND claveMovimiento IN ('GC');";
        fn_ejecuta_sql($sqlUpdXC);
        if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
            $a['success']     = false;
            $a['msjResponse'] = $_SESSION['error_sql'];
            $a['sql']         = $sqlUpdXC;
        } else {
            $sqlUpdXS = "UPDATE trgastosviajetractortbl ".
                        "SET claveMovimiento = 'XP' ".
                        "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                        "AND folio = '".$folioPrm."' ".
                        "AND claveMovimiento IN ('GP');";
            fn_ejecuta_sql($sqlUpdXS);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success']     = false;
                $a['msjResponse'] = $_SESSION['error_sql'];
                $a['sql']         = $sqlUpdXS;
            }
        }
        if ($a['success']) {
            $sqlDltFac = "DELETE FROM trpolizafacturastbl ".
                        "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                        "AND folio = '".$folioPrm."'";
            fn_ejecuta_sql($sqlDltFac);
            if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                $a['success']     = false;
                $a['msjResponse'] = $_SESSION['error_sql'];
                $a['sql']         = $sqlDltFac;
            } else {
                $delStr = "DELETE FROM trComplementosConceptoTbl ".
                          "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                          "AND concepto = '6002' ".
                          "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                          "AND folio = '".$folioPrm."'";
                fn_ejecuta_sql($delStr);
                if ((isset($_SESSION['error_sql'])) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['msjResponse'] = $_SESSION['error_sql'] . "<br>" . $delStr;
                } else {
                    $updStr = "UPDATE tralimentospendientetbl ".
                              "SET claveMovimiento = 'CX' ".
                              "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                              "AND concepto = '2342' ".
                              "AND centroDistribucion = '".$_SESSION['usuCompania']."' ".
                              "AND folio = '".$folioPrm."' ".
                              "AND claveMovimiento = 'CA'";
                    fn_ejecuta_sql($updStr);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success']     = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql']         = $updStr;
                    }
                }
            }
        }
        if ($a['success']) {
            // COMBUSTIBLE PENDIENTE
            $selXC = "SELECT * FROM trgastosviajetractortbl ".
                    "WHERE idViajeTractor = ".$idViajeTractorPrm." ".
                    "AND folio = '".$folioPrm."' ".
                    "AND concepto IN ('9012','9013') ".
                    "AND claveMovimiento = 'XC';";
            $Rst = fn_ejecuta_sql($selXC);
            if ($Rst['records'] > 0) {
                $combusPendArr = explode('|', $Rst['root'][0]['observaciones']);
                $sqlDel = "DELETE FROM trCombustiblePendienteTbl " . 
                          "WHERE idCombustible = " . $combusPendArr[1] . " ".
                          "AND claveMovimiento = 'CP'";
                fn_ejecuta_sql($sqlDel);
                if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                    $a['success'] = false;
                    $a['msjResponse'] = $_SESSION['error_sql'];
                    $a['sql'] = $sqlDel;
                } else {
                    if (empty($combusPendArr[0])) {
                        $combusPendArr[0] = 0;
                    }
                    $sqlUpdPdt = "UPDATE trCombustiblePendienteTbl SET claveMovimiento = 'CP' " . 
                                 "WHERE idCombustible = " . $combusPendArr[0] . " ";
                    fn_ejecuta_sql($sqlUpdPdt);
                    if (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] != "") {
                        $a['success'] = false;
                        $a['msjResponse'] = $_SESSION['error_sql'];
                        $a['sql'] = $sqlUpdPdt;
                    }
                }
            }
        }
        return $a;
    }
?>