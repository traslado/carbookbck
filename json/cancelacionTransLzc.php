<?php
    session_start();

	require_once("../funciones/generales.php");
    require_once("../funciones/construct.php");
    require_once("../funciones/utilidades.php");

    	switch($_REQUEST['alFunctionHdn']){
			case 'leerXLSSO':
				leerXLSSO();
				break;        
		 	case 'leerXLSTE':
		    	leerXLSTE();		
		    	break;
		    case 'leerXLSRE':
		    	leerXLSRE();		
		    	break;
		    case 'leerXLSPI':
		    	leerXLSPI();		
		    	break;
		    default:
		        echo 'no muestra nada';
		}

function leerXLSSO(){

    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["cancelacionTransmisionesLzcArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["cancelacionTransmisionesLzcArchivoFld"]["error"];

		} else {
			$temp_file_name = $_FILES["cancelacionTransmisionesLzcArchivoFld"]["tmp_name"]; 
			$original_file_name = $_FILES["cancelacionTransmisionesLzcArchivoFld"]["name"];

			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
		 
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLSSO1($new_name);
					//echo $a['root'];
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

}
function leerXLSTE(){

    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["cancelacionTransmisionesLzcTenderArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["cancelacionTransmisionesLzcTenderArchivoFld"]["error"];

		} else {
			$temp_file_name = $_FILES["cancelacionTransmisionesLzcTenderArchivoFld"]["tmp_name"]; 
			$original_file_name = $_FILES["cancelacionTransmisionesLzcTenderArchivoFld"]["name"];

			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
		 
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLSTE1($new_name);
					//echo $a['root'];
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

}
function leerXLSRE(){

    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["cancelacionTransmisionesLzcRepuveArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["cancelacionTransmisionesLzcRepuveArchivoFld"]["error"];

		} else {
			$temp_file_name = $_FILES["cancelacionTransmisionesLzcRepuveArchivoFld"]["tmp_name"]; 
			$original_file_name = $_FILES["cancelacionTransmisionesLzcRepuveArchivoFld"]["name"];

			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
		 
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLSRE1($new_name);
					//echo $a['root'];
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

}
function leerXLSPI(){

    $a = array();
	$a['successTitle'] = "Manejador de Archivos";
	
	if(isset($_FILES)) {
		if($_FILES["cancelacionTransmisionesLzcPortInArchivoFld"]["error"] > 0){
			$a['success'] = false;
			$a['message'] = $_FILES["cancelacionTransmisionesLzcPortInArchivoFld"]["error"];

		} else {
			$temp_file_name = $_FILES["cancelacionTransmisionesLzcPortInArchivoFld"]["tmp_name"]; 
			$original_file_name = $_FILES["cancelacionTransmisionesLzcPortInArchivoFld"]["name"];

			// Find file extention 
			$ext = explode ('.', $original_file_name); 
			$ext = $ext [count ($ext) - 1]; 
		 	
			// Remove the extention from the original file name 
			$file_name = str_replace ($ext, '', $original_file_name); 
		 	
		  	$new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;
		 
			if (move_uploaded_file($temp_file_name, $new_name)){

				if (!file_exists($new_name)){
					$a['success'] = false;
					$a['errorMessage'] = "Error al procesar el archivo " . $new_name;
				} else {
					$a['success'] = true;
					$a['successMessage'] = "Archivo Cargado";
					$a['archivo'] = $file_name . $ext;
					$a['root'] = leerXLSPI1($new_name);
					//echo $a['root'];
				}
		 	} else { 
				$a['success'] = false;
				$a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
			}
		}
	} else {
		$a['success'] = false;
		$a['errorMessage'] = "Error FILES NOT SET";
	}
	
	echo json_encode($a);

}
   function leerXLSSO1($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) < 4 && $highestRow > 2 ) {
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) <= 2 ) {
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17 || strlen($rowData[0][0]) > 17) {
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN]', 'fail'=>'Y');
			return $root;
		}
		//-------------------------------a-----------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true));
		
		for($row = 0; $row<sizeof($rowData); $row++){
			
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
	

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {


				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}

				else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBaseSO($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						
						if (!$isValidArr[$key]['exist']) {

							 	if ($key == 'VIN') {

							 		$siNo = ' No cuenta con Ship Out Para Cancelar';
							 	}

							$errorMsg = 'El '.$key.$siNo;
							$isTrue = false; 
						
						}
					}
				
				}	

			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
				$_SESSION['usuCompania'] = 'LZC02';
				$numVin = $rowData[$row][0];
			
					$deleteTransaccion ="DELETE FROM altransaccionunidadtbl where tipoTransaccion in ('RRL','GTE') ".
										"AND vin ='".$numVin."' ";

					fn_ejecuta_query($deleteTransaccion);

				$selMarcaVin =" SELECT su.marca from alinstruccionesmercedestbl im, casimbolosunidadestbl su ".
				  						"where im.modelDesc=su.simboloUnidad and im.vin='".$numVin."' ";

				$rsSelMarcaVin=	fn_ejecuta_query($selMarcaVin);	

				if ($rsSelMarcaVin['root'][0]['marca'] == 'KI') {
					
					$estatusTender='TK';

					}	
			    else if ($rsSelMarcaVin['root'][0]['marca'] == 'HY') {

			    	$estatusTender='TY';
			    }

			       $updateIM ="UPDATE alInstruccionesMercedesTbl set cveStatus='".$estatusTender."' , ".
			       			  "trimCode= null , trimDesc=null, invPrice=null,fechaMovimiento=null,currency=null,descColor=null ".
			       			  "where cveLoc='LZC02' and vin ='".$numVin."' ";

					fn_ejecuta_query($updateIM);


				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					$errorMsg = 'Ship Out Cancelado';
				} else {
					$errorMsg = 'Registro No Agregado';
				}		
			}
			$root[]= array('vin'=>$rowData[$row][0],'nose'=>$errorMsg);	

		}
		//echo json_encode($root);
	return $root;	

	}
   
	function isFieldInDataBaseSO($field, $value) { //Funcion que checa que un valor exista en la base


		switch(strtoupper($field)) {
			case 'VIN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus = 'SO' and vin  in (select vin from altransaccionunidadtbl where tipoTransaccion in ('RRL','GTE') and vin ='".$value."')";
				$rs = fn_ejecuta_query($sqlExist);

			break;


		}

		return strtoupper($field) == 'VIN' ? (sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
//echo json_encode($field);
}

   function leerXLSTE1($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) < 4 && $highestRow > 2 ) {
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) <= 2 ) {
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17 || strlen($rowData[0][0]) > 17) {
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN]', 'fail'=>'Y');
			return $root;
		}
		//-------------------------------a-----------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true));
		
		for($row = 0; $row<sizeof($rowData); $row++){
			
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['VINN']['val'] = $rowData[$row][0];
			$isValidArr['VINN']['size'] = strlen($rowData[$row][0]) == 17;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {


				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}

				else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBaseTE($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						
						if (!$isValidArr[$key]['exist']) {

							 	if ($key == 'VIN') {

							 		$siNo = 'El vin no cuenta con Tender Para Cancelar';
							 	}
							   else if ($key == 'VINN') {

							 		$siNo = 'Vin con salida(Cancelar primero SHIP OUT) ';
							 	}

							$errorMsg = $siNo;
							$isTrue = false; 
						
						}
					}
				
				}	

			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
				$_SESSION['usuCompania'] = 'LZC02';
				$numVin = $rowData[$row][0];
			
				$deleteTransaccion ="DELETE FROM altransaccionunidadtbl where tipoTransaccion in ('RLS') ".
										"AND vin ='".$numVin."' ";

					fn_ejecuta_query($deleteTransaccion);

				$selMarcaVin =" SELECT su.marca from alinstruccionesmercedestbl im, casimbolosunidadestbl su ".
				  						"where im.modelDesc=su.simboloUnidad and im.vin='".$numVin."' ";

				$rsSelMarcaVin=	fn_ejecuta_query($selMarcaVin);	

				if ($rsSelMarcaVin['root'][0]['marca'] == 'KI') {
					
					$estatusTender='DK';

					}	
			    else if ($rsSelMarcaVin['root'][0]['marca'] == 'HY') {

			    	$estatusTender='DH';
			    }

			       $updateIM ="UPDATE alInstruccionesMercedesTbl set cveStatus='".$estatusTender."' , ".
			       			  "cveColor= null ".
			       			  "where cveLoc='LZC02' and vin ='".$numVin."' ";

					fn_ejecuta_query($updateIM);
			

				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					$errorMsg = 'Tender Cancelado';
				} else {
					$errorMsg = 'Registro No Agregado';
				}		
			}
			$root[]= array('vin'=>$rowData[$row][0],'nose'=>$errorMsg);	

		}
		//echo json_encode($root);
	return $root;	

	}
   
	function isFieldInDataBaseTE($field, $value) { //Funcion que checa que un valor exista en la base


		switch(strtoupper($field)) {
			case 'VIN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus in ('TK','TY') and vin in (select vin from altransaccionunidadtbl where tipoTransaccion ='RLS' and vin ='".$value."')";
				$rs = fn_ejecuta_query($sqlExist);

			break;
			case 'VINN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus = 'SO' and vin  in (select vin from altransaccionunidadtbl where tipoTransaccion in ('RRL','GTE') and vin ='".$value."')";
				$rs = fn_ejecuta_query($sqlExist);

			break;


		}

		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) == 0) : (sizeof($rs['root']) == 0);
//echo json_encode($field);
}

 function leerXLSRE1($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) < 4 && $highestRow > 2 ) {
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) <= 2 ) {
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17 || strlen($rowData[0][0]) > 17) {
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN]', 'fail'=>'Y');
			return $root;
		}
		//-------------------------------a-----------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true));
		
		for($row = 0; $row<sizeof($rowData); $row++){
			
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['VINN']['val'] = $rowData[$row][0];
			$isValidArr['VINN']['size'] = strlen($rowData[$row][0]) == 17;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {


				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}

				else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBaseRE($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						
						if (!$isValidArr[$key]['exist']) {

							 	if ($key == 'VIN') {

							 		$siNo = 'El vin no cuenta con Repuve Para Cancelar';
							 	}
							   else if ($key == 'VINN') {

							 		$siNo = 'El vin ya cuenta con Salida ';
							 	}

							$errorMsg = $siNo;
							$isTrue = false; 
						
						}
					}
				
				}	

			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
				$_SESSION['usuCompania'] = 'LZC02';
				$numVin = $rowData[$row][0];
			
				$deleteTransaccion ="DELETE FROM altransaccionunidadtbl where tipoTransaccion in ('REP') ".
										"AND vin ='".$numVin."' ";

				fn_ejecuta_query($deleteTransaccion);


				$deleteRepuve ="DELETE FROM alrepuvetbl where  vin ='".$numVin."' ";

				fn_ejecuta_query($deleteRepuve);
                  
                 $updatealunidades ="UPDATE alUnidadesTbl set folioRepuve = null ".
			       			  "where  vin ='".$numVin."' ";

				fn_ejecuta_query($updatealunidades);
			

				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					$errorMsg = 'Repuve Cancelado';
				} else {
					$errorMsg = 'Registro No Agregado';
				}		
			}
			$root[]= array('vin'=>$rowData[$row][0],'nose'=>$errorMsg);	

		}
		//echo json_encode($root);
	return $root;	

	}
   
	function isFieldInDataBaseRE($field, $value) { //Funcion que checa que un valor exista en la base


		switch(strtoupper($field)) {
			case 'VIN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus in ('TK','TY','DK','DH') and vin in (select vin from altransaccionunidadtbl where tipoTransaccion ='REP' and vin ='".$value."') and vin in (select vin from alrepuvetbl where centroDistribucion='LZC02')";
				$rs = fn_ejecuta_query($sqlExist);

			break;
			case 'VINN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus = 'SO' and vin  in (select vin from altransaccionunidadtbl where tipoTransaccion in ('RRL','GTE') and vin ='".$value."')";
				$rs = fn_ejecuta_query($sqlExist);

			break;


		}

		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) == 0) : (sizeof($rs['root']) == 0);
//echo json_encode($field);
}

 function leerXLSPI1($inputFileName) {
    	/** Error reporting */
	    error_reporting(E_ALL);
		
		date_default_timezone_set ("America/Mexico_City");
		
		//  Include PHPExcel_IOFactory
		include '../funciones/Classes/PHPExcel/IOFactory.php';
		
		//$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
		//echo $inputFileName;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader =
			 PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();

		//Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
		$rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

		
		//-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

		if (ord(strtoupper($highestColumn)) < 4 && $highestRow > 2 ) {
			$root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
			return $root;
		}

		if (ord(strtoupper($highestColumn)) <= 2 ) {
			$root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
			return $root;
		}

		if (strlen($rowData[0][0]) < 17 || strlen($rowData[0][0]) > 17) {
			$root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [VIN]', 'fail'=>'Y');
			return $root;
		}
		//-------------------------------a-----------------------------------------------------------------
		

		$isValidArr = array('VIN' =>array('val'=>'','size'=>true,'exist'=>true));
		
		for($row = 0; $row<sizeof($rowData); $row++){
			
			//Validar si las unidades estan bien o no
			//Verifico si la longitud de loas campos sea la correcta...
			
			$isValidArr['VIN']['val'] = $rowData[$row][0];
			$isValidArr['VIN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['VINN']['val'] = $rowData[$row][0];
			$isValidArr['VINN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['TEN']['val'] = $rowData[$row][0];
			$isValidArr['TEN']['size'] = strlen($rowData[$row][0]) == 17;
			$isValidArr['REP']['val'] = $rowData[$row][0];
			$isValidArr['REP']['size'] = strlen($rowData[$row][0]) == 17;

			$errorMsg = '';
			$isTrue = true;

			foreach ($isValidArr as $key => $value) {


				if (!$isValidArr[$key]['size']) { //Si no es del tamaño correcto 
					$errorMsg .='El '.$key.' no es del tama&ntilde;o correcto|';
					$isTrue = false;
				}

				else {// de lo contrario verifico que exista en la base
					if ($key != 'Avanzada') {
						
						$isValidArr[$key]['exist'] = isFieldInDataBasePI($key, $isValidArr[$key]['val']);//Funcion que verifica si esta en la base 
						
						if (!$isValidArr[$key]['exist']) {

							 	if ($key == 'VIN') {

							 		$siNo = 'El vin no cuenta con Port In Para Cancelar';
							 	}
							   if ($key == 'VINN') {

							 		$siNo = 'Vin con salida(Cancelar primero SHIP OUT) ';
							 	}
								if ($key == 'TEN') {

							 		$siNo = 'Vin con tender(Cancelar primero TENDER)';
							 	}
							    if ($key == 'REP') {

							 		$siNo = 'Vin con Repuve (Cancelar primero REPUVE)';
							 	}

							$errorMsg = $siNo;
							$isTrue = false; 
						
						}
					}
				
				}	

			}

			if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
				//Busco el idTarifa

				//Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
			    $_SESSION['usuCompania'] = 'LZC02';
				$numVin = $rowData[$row][0];
			
				$deleteTransaccion ="DELETE FROM altransaccionunidadtbl where tipoTransaccion in ('REC') ".
										"AND vin ='".$numVin."' ";

				fn_ejecuta_query($deleteTransaccion);

                  
                 $updatealunidades ="UPDATE alHistoricoUnidadesTbl set observaciones='UNIDAD INGRESO L1' ".
			       			  		"where  vin ='".$numVin."' and claveMovimiento='L1' ";

				fn_ejecuta_query($updatealunidades);

				  $updUldet="SELECT centroDistribucion,vin, fechaEvento, claveMovimiento, distribuidor, idTarifa, localizacionUnidad, claveChofer, observaciones, usuario, ip 
						FROM alhistoricounidadestbl hu 
						WHERE  hu.fechaEvento = (SELECT MAX(hu2.fechaEvento) 
                      							FROM alhistoricounidadestbl hu2 
                      							WHERE hu2.vin = hu.vin 
                      							AND hu2.claveMovimiento not in (SELECT ge.valor 
                                                  FROM cageneralestbl ge 
                                                  WHERE ge.tabla = 'alHistoricoUnidadesTbl' 
                                                  AND ge.columna = 'nvalidos')) 
												AND  hu.vin = '".$numVin."'  ";
				
				$rsupdUldet	= fn_ejecuta_query($updUldet);	

				if ($rsupdUldet['root'][$i]['claveChofer'] == null)
		{

			$claveChofer= "null";
		}
		else 
		{
			$claveChofer=$rsupdUldet['root'][$i]['claveChofer'];

		}

			$sqlUpdUnd = "UPDATE alultimodetalletbl ".
						"SET centroDistribucion= '".$rsupdUldet['root'][0]['centroDistribucion']."', ".
						"fechaEvento = '".$rsupdUldet['root'][0]['fechaEvento']."', ".
						"claveMovimiento = '".$rsupdUldet['root'][0]['claveMovimiento']."', ".
						"distribuidor = '".$rsupdUldet['root'][0]['distribuidor']."', ".
						"idTarifa = '".$rsupdUldet['root'][0]['idTarifa']."', ".
						"localizacionUnidad = '".$rsupdUldet['root'][0]['localizacionUnidad']."', ".
						"claveChofer =".$claveChofer.",".
						"usuario = '".$rsupdUldet['root'][0]['usuario']."', ".
						"observaciones = '".$rsupdUldet['root'][0]['observaciones']."', ".
						"ip = '".$rsupdUldet['root'][0]['ip']."' ".
						"WHERE vin = '".$rsupdUldet['root'][0]['vin']."'";

			fn_ejecuta_query($sqlUpdUnd);
            
			

				if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
					$errorMsg = 'Port In cancelado';
				} else {
					$errorMsg = 'Registro No Agregado';
				}		
			}
			$root[]= array('vin'=>$rowData[$row][0],'nose'=>$errorMsg);	
 
		}
		//echo json_encode($root);
	return $root;	

	}
   
	function isFieldInDataBasePI($field, $value) { //Funcion que checa que un valor exista en la base


		switch(strtoupper($field)) {
			case 'VIN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus in ('DK','DH') and vin in (select vin from altransaccionunidadtbl where tipoTransaccion ='REC' and vin ='".$value."') and vin in (select vin from alHistoricoUnidadesTbl where observaciones='UNIDAD INGRESO L1 LC' and vin ='".$value."')";
				$rs = fn_ejecuta_query($sqlExist);

			break;

			case 'VINN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus = 'SO' and vin  in (select vin from altransaccionunidadtbl where tipoTransaccion in ('RRL','GTE') and vin ='".$value."')";
				$rs = fn_ejecuta_query($sqlExist);

			break;
			case 'TEN':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus in ('TK','TY') and vin in (select vin from altransaccionunidadtbl where tipoTransaccion in ('RLS') and vin ='".$value."')";
				$rs = fn_ejecuta_query($sqlExist);

			break;
			case 'REP':
			
				$sqlExist = "SELECT VIN FROM alInstruccionesMercedesTbl WHERE VIN = '".$value."' AND cveLoc='LZC02' and cveStatus in ('TK','TY','DK','DH') and vin in (select vin from altransaccionunidadtbl where tipoTransaccion ='REP' and vin ='".$value."') and vin in (select vin from alrepuvetbl where centroDistribucion='LZC02')";
				$rs = fn_ejecuta_query($sqlExist);

			break;


		}

		return strtoupper($field) == 'VIN' ? !(sizeof($rs['root']) == 0) : (sizeof($rs['root']) == 0);
//echo json_encode($field);
}

?>