<?php
  session_start();
$_SESSION['modulo'] = "catCompanias";
  require_once("../funciones/generales.php");
  require_once("../funciones/construct.php");
  require_once("../funciones/utilidades.php");

switch($_SESSION['idioma']){
      case 'ES':
          include("../funciones/idiomas/mensajesES.php");
          break;
      case 'EN':
          include("../funciones/idiomas/mensajesEN.php");
          break;
      default:
          include("../funciones/idiomas/mensajesES.php");
  }

  switch($_REQUEST['cargaXmlHdn']){
      case 'getTractor':
              getTractor();
          break;
      case 'getCargaXml':
          getCargaXml();
          break;
      case 'getViajeTractor':
          getViajeTractor();
          break;
      default:
          echo 'no esta entrando a ningun proceso';
  }

  //$_REQUEST = trasformUppercase($_REQUEST);

  function getTractor(){

      $sqlGetTractores = "SELECT ch.tractor, vt.viaje, ch.idTractor ".
                          "FROM catractorestbl ch,trviajestractorestbl vt ".
                          "WHERE ch.idTractor = vt.idTractor ".
                          "AND vt.claveMovimiento = 'VU' ".
                          "AND ch.compania = '".$_REQUEST['cargaXmlCompaniaCmb']."';";

      $rsTractores = fn_ejecuta_query($sqlGetTractores);

      echo json_encode($rsTractores);
  }

  function getViajeTractor(){
      $sqlGetViaje = "SELECT ch.tractor, vt.viaje, ch.idTractor, vt.idViajeTractor ".
                      "FROM catractorestbl ch,trviajestractorestbl vt ".
                      "WHERE ch.idTractor = vt.idTractor ".
                      "AND vt.claveMovimiento = 'VU' ".
                      "AND ch.idTractor = '".$_REQUEST['cargaXmlractorCmb']."';";

      $rsViajes = fn_ejecuta_query($sqlGetViaje);

      echo json_encode($rsViajes);
  }


  function getCargaXml(){
      $a = array();
      $a['successTitle'] = "Manejador de Archivos";

      if(isset($_FILES)) {
          if($_FILES["cargaXmlArchivoFld"]["error"] > 0){
              $a['success'] = false;
              $a['message'] = $_FILES["cargaXmlArchivoFld"]["error"];

          } else {
              $temp_file_name = $_FILES['cargaXmlArchivoFld']['tmp_name'];
              $original_file_name = $_FILES['cargaXmlArchivoFld']['name'];

              // Find file extention
              $ext = explode ('.', $original_file_name);
              $ext = $ext [count ($ext) - 1];

              // Remove the extention from the original file name
              $file_name = str_replace ($ext, '', $original_file_name);

              $new_name = $_SERVER['DOCUMENT_ROOT'].'/respaldos/'. $file_name . $ext;

              if (move_uploaded_file($temp_file_name, $new_name)){

                  if (!file_exists($new_name)){
                      $a['success'] = false;
                      $a['errorMessage'] = "Error al procesar el archivo " . $new_name;
                  } else {
                      $a['success'] = true;
                      $a['successMessage'] = "Archivo Cargado";
                      $a['archivo'] = $file_name . $ext;
                      $a['root'] = leerXLS($new_name);
                  }
              } else {
                  $a['success'] = false;
                  $a['errorMessage'] = "No se pudo mover el archivo " . $temp_file_name. '</br> a '.$new_name;
              }
          }
      } else {
          $a['success'] = false;
          $a['errorMessage'] = "Error FILES NOT SET";
      }

      echo json_encode($a);
  }

function leerXLS($inputFileName) {
      /** Error reporting */
      error_reporting(E_ALL);

      date_default_timezone_set ("America/Mexico_City");

      //  Include PHPExcel_IOFactory
      include '../funciones/Classes/PHPExcel/IOFactory.php';

      //$inputFileName = str_replace('json', 'Temp', str_replace('alUnidadesLeeArchivo.php', 'avanzadas.xls', __FILE__));
      //echo $inputFileName;
      //  Read your Excel workbook
      try {
          $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
          $objReader =
           PHPExcel_IOFactory::createReader($inputFileType);
          $objPHPExcel = $objReader->load($inputFileName);
      } catch(Exception $e) {
          die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
      }

      //  Get worksheet dimensions
      $sheet = $objPHPExcel->getSheet(0);
      $highestRow = $sheet->getHighestRow();
      $highestColumn = $sheet->getHighestColumn();

      //Obtengo un arreglo bidimensional(Tabla) de los datos del archivo de Excel
      $rowData = $sheet->rangeToArray('A1:' . $highestColumn . $highestRow);

      //-------------Hacemos algunas validaciónes propias de un archivo válido de Excel----------------

      if (ord(strtoupper($highestColumn)) <= 67 && $highestRow <= 10 ) {
          $root[] = array('nose' => 'Archivo Vac&iacute;o.</br> Ingrese un archivo con Datos.', 'fail'=>'Y');
          return $root;
      }if (ord(strtoupper($highestColumn)) < 2) {
          $root[] = array('nose' => 'Archivo con menos columnas </br> a las esperadas.', 'fail'=>'Y', 'row'=>$highestRow, 'col'=>$highestColumn);
          return $root;
      }if (strlen($rowData[0][0]) < 1) {
          $root[] = array('nose' => 'Archivo inv&aacute;lido. </br> Verifique que las columnas </br> se encuentren en el siguiente orden </br> [No.Comprobante-fechaEvento-litros-importe,placas]', 'fail'=>'Y');
          return $root;
      }
      //------------------------------------------------------------------------------------------------


      $isValidArr = array(    '01' =>array('val'=>'','size'=>true,'exist'=>true),
                              '02' =>array('val'=>'','size'=>true,'exist'=>true),
                              '03' =>array('val'=>'','size'=>true,'exist'=>true),
                              '03'=>array('val'=>'','size'=>true,'exist'=>true),
                              '04' =>array('val'=>'','size'=>true,'exist'=>true),
                              '05' =>array('val'=>'','size'=>true,'exist'=>true),
                              '06' =>array('val'=>'','size'=>true,'exist'=>true),
                              '07' =>array('val'=>'','size'=>true,'exist'=>true),
                              '08' =>array('val'=>'','size'=>true,'exist'=>true),
                              '09' =>array('val'=>'','size'=>true,'exist'=>true),
                              '10' =>array('val'=>'','size'=>true,'exist'=>true),
                              '11' =>array('val'=>'','size'=>true,'exist'=>true),
                              '12' =>array('val'=>'','size'=>true,'exist'=>true),
                              '13' =>array('val'=>'','size'=>true,'exist'=>true),
                              '14' =>array('val'=>'','size'=>true,'exist'=>true),
                              '15' =>array('val'=>'','size'=>true,'exist'=>true),
                              '16' =>array('val'=>'','size'=>true,'exist'=>true),
                              '17' =>array('val'=>'','size'=>true,'exist'=>true),
                              '18' =>array('val'=>'','size'=>true,'exist'=>true),
                              '19' =>array('val'=>'','size'=>true,'exist'=>true),
                              '20' =>array('val'=>'','size'=>true,'exist'=>true),
                              '21' =>array('val'=>'','size'=>true,'exist'=>true),
                              '22' =>array('val'=>'','size'=>true,'exist'=>true),
                              '23' =>array('val'=>'','size'=>true,'exist'=>true),
                              '24' =>array('val'=>'','size'=>true,'exist'=>true),
                              '25' =>array('val'=>'','size'=>true,'exist'=>true),
                              '26' =>array('val'=>'','size'=>true,'exist'=>true),
                              '27' =>array('val'=>'','size'=>true,'exist'=>true),
                              '28' =>array('val'=>'','size'=>true,'exist'=>true),
                              '29' =>array('val'=>'','size'=>true,'exist'=>true),
                              '30' =>array('val'=>'','size'=>true,'exist'=>true),
                              '31' =>array('val'=>'','size'=>true,'exist'=>true));

      for($row = 0; $row<sizeof($rowData); $row++){
          //Validar si las unidades estan bien o no
          //Verifico si la longitud de loas campos sea la correcta...
          $isValidArr['01']['val'] = $rowData[$row][0];
          $isValidArr['02']['val'] = $rowData[$row][1];
          $isValidArr['03']['val'] = $rowData[$row][2];
          $isValidArr['04']['val'] = $rowData[$row][3];
          $isValidArr['05']['val'] = $rowData[$row][4];
          $isValidArr['06']['val'] = $rowData[$row][5];
          $isValidArr['07']['val'] = $rowData[$row][6];
          $isValidArr['08']['val'] = $rowData[$row][7];
          $isValidArr['09']['val'] = $rowData[$row][8];
          $isValidArr['10']['val'] = $rowData[$row][9];
          $isValidArr['11']['val'] = $rowData[$row][10];
          $isValidArr['12']['val'] = $rowData[$row][11];
          $isValidArr['13']['val'] = $rowData[$row][12];
          $isValidArr['14']['val'] = $rowData[$row][13];
          $isValidArr['15']['val'] = $rowData[$row][14];
          $isValidArr['16']['val'] = $rowData[$row][15];
          $isValidArr['17']['val'] = $rowData[$row][16];
          $isValidArr['18']['val'] = $rowData[$row][17];
          $isValidArr['19']['val'] = $rowData[$row][18];
          $isValidArr['20']['val'] = $rowData[$row][19];
          $isValidArr['21']['val'] = $rowData[$row][20];
          $isValidArr['22']['val'] = $rowData[$row][21];
          $isValidArr['23']['val'] = $rowData[$row][22];
          $isValidArr['24']['val'] = $rowData[$row][23];
          $isValidArr['25']['val'] = $rowData[$row][24];
          $isValidArr['26']['val'] = $rowData[$row][25];
          $isValidArr['27']['val'] = $rowData[$row][26];
          $isValidArr['28']['val'] = $rowData[$row][27];
          $isValidArr['29']['val'] = $rowData[$row][28];
          $isValidArr['30']['val'] = $rowData[$row][29];
          $isValidArr['31']['val'] = $rowData[$row][30];
          //cargaXmlIdViajeHdn
          //$isValidArr['litros']['size'] = strlen($rowData[$row][2]) == 5;

          $errorMsg = '';
          $isTrue = true;



          foreach ($isValidArr as $key => $value) {
              /*if ($_REQUEST['cargaXmlIdViajeHdn']== NULL) { //Si no es del tamaño correcto
                  $errorMsg .='El '.$key.' no existe el viaje del tractor correcto|';
                  $isTrue = false;
              }*/if($isValidArr['compania']['size'] != 2) {
                  $errorMsg .='La Compania Tractor no tiene la Misma Longitud';
                  $isTrue = false;
              }/*if($rowData[$row][4] == null) {
                  $errorMsg .='El '.$key.' Tipo de Mercado no existe para esta Unidad';
                  echo json_encode(" mercado ".$rowData[$row][4]);
                  $isTrue = false;
              }if($rowData[$row][5] == null) {
                  $errorMsg .='El '.$key.' Nombre del Barco no existe para esta Unidad';
                  echo json_encode(" brco ".$rowData[$row][5]);
                  $isTrue = false;
              }*/
          }

          if ($isTrue) { //Si pasa la prueba del tamaño y la prueba de existencia en la Base lo inserto
              //Busco el idTarifa

              //Ahora inserto la unidad en alUnidadesTbl y alHistoricoUnidadesTbl
              //$_SESSION['usuCompania'] = 'LZC02';


              $today = date("Y-m-d H:i:s");
              $fecha = substr($today,0,10);
              $hora=substr($today,11,8);

              $numComprobante = $rowData[$row][2];
              $numfechaEvento = $rowData[$row][3];
              $numlitros = $rowData[$row][4];
              $numImporte = $rowData[$row][5];
              $numPlacas = $rowData[$row][6];


              /*$numNomFac = $rowData[$row][3];
              $numDirEnt = $rowData[$row][4];
              $numFped = $rowData[$row][5];*/



              $addXmlCard = "INSERT INTO trpolizaxmltmp (claveCompania,importe,metPago,numeroCuenta,rfc,nombre,retencionISR,retencionIVA,ieps,impuestoIEPS,tipoIML,tasaIML,impuestoIML,folioFiscal,fechaFactura,moneda,tipoCam,tipoCar,folioInterno,tipoPreg,tipoDoc,fechaPoliza,numPoliza,tipoPoliza,orden,usuario,status,subTotal,descuento,impLocTras) ".
                  "VALUES('".$isValidArr['01']['val']."', ".
                      "'".$isValidArr['02']['val']."', ".
                      "'".$isValidArr['03']['val']."', ".
                      "'".$isValidArr['04']['val']."', ".
                      "'".$isValidArr['05']['val']."', ".
                      "'".$isValidArr['06']['val']."', ".
                      "'".$isValidArr['06']['val']."', ".
                      "'".$isValidArr['07']['val']."', ".
                      "'".$isValidArr['08']['val']."', ".
                      "'".$isValidArr['09']['val']."', ".
                      "'".$isValidArr['10']['val']."', ".
                      "'".$isValidArr['11']['val']."', ".
                      "'".$isValidArr['12']['val']."', ".
                      "'".$isValidArr['13']['val']."', ".
                      "'".$isValidArr['14']['val']."', ".
                      "'".$isValidArr['15']['val']."', ".
                      "'".$isValidArr['16']['val']."', ".
                      "'".$isValidArr['17']['val']."', ".
                      "'".$isValidArr['18']['val']."', ".
                      "'".$isValidArr['19']['val']."', ".
                      "'".$isValidArr['20']['val']."', ".
                      "'".$isValidArr['21']['val']."', ".
                      "'".$isValidArr['22']['val']."', ".
                      "'".$isValidArr['23']['val']."', ".
                      "'".$isValidArr['24']['val']."', ".
                      "'".$isValidArr['25']['val']."', ".
                      "'".$isValidArr['26']['val']."', ".
                      "'".$isValidArr['27']['val']."', ".
                      "'".$isValidArr['28']['val']."', ".
                      "'".$isValidArr['29']['val']."', ".
                      "'".$isValidArr['30']['val']."', ".
                      "'".$isValidArr['31']['val']."') ";

              fn_ejecuta_query($addXmlCard);

              if ((!isset($_SESSION['error_sql'])) || (isset($_SESSION['error_sql']) && $_SESSION['error_sql'] == "")) {
                  $errorMsg = 'Agregado Correctamente';
              }else {
                  $errorMsg = 'Registro No Agregado';
              }

              /*$rs = addUnidad($rowData[$row][0],$rowData[$row][2],$rowData[$row][1],$rowData[$row][3], $_SESSION['usuCompania'],'PR',
                        $rsTarifa['root'][0]['idTarifa'],$_SESSION['usuCompania'], $repuve, $chofer, $observaciones,$rowData[$row][4]);*/
          }
          $root[]= array('compania'=>$rowData[$row][0],'tractor'=>$rowData[$row][1],'noComprobante'=>$rowData[$row][2],'fechaEvento'=>$rowData[$row][3], 'litros'=>$rowData[$row][4],'importe'=>$rowData[$row][5],'placas'=>$rowData[$row][6],'nose'=>$errorMsg);

          //echo json_encode($rowData[$row]);
      }
      return $root;
  }


  function isFieldInDataBase($field, $value) { //Funcion que checa que un valor exista en la base
      $tabla = 'tabla';
      $columna = 'columna';
      switch(strtoupper($field)) {
          case 'noComprobante':
              $tabla = 'alInstruccionesMercedesTbl'; $columna = 'noComprobante'; break;
          case 'fechaEvento':
              $tabla = 'cafechaEventosUnidadesTbl'; $columna = 'fechaEventoUnidad'; break;
          case 'litros':
              $tabla = 'calitrosesCentrosTbl'; $columna = 'litrosCentro'; break;
      }
      $sqlExist = "SELECT ".$columna." FROM ".$tabla." WHERE ".$columna." = '".$value."'";
      $rs = fn_ejecuta_query($sqlExist);
      return strtoupper($field) == 'noComprobante' ? !(sizeof($rs['root']) > 0) : (sizeof($rs['root']) > 0);
  }
?>
